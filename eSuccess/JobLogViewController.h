//
//  JobLogViewController.h
//  eSuccess
//
//  Created by admin on 5/11/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"

@interface JobLogViewController : BaseDetailTableViewController
@property(nonatomic, retain) NSMutableArray *jobArray;
@property(nonatomic, retain) NSMutableArray *jobNameArray;
@end
