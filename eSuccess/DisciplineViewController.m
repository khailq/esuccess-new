//
//  DisciplineViewController.m
//  eSuccess
//
//  Created by admin on 5/23/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "DisciplineViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "DayLeaveDetailCell.h"
#import "Employee Service/EmployeeServiceSvc.h"

@interface DisciplineViewController ()<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>
{
    NSMutableArray *disciplineViolationArr;
}
@end

@implementation DisciplineViewController
@synthesize disciplineArr;
@synthesize titleArr;
static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Kỷ luật";
    [self getData];
    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
    disciplineViolationArr = [NSMutableArray array];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 130;
//}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [titleArr count] > 0 ? [titleArr objectAtIndex:section]: nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [disciplineArr count] > 0 ? disciplineArr.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //empl_tns1_V_EmpProfileDiscipline *discipline = disciplineArr[section];
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (disciplineArr.count == 0) {
        return [self getDefaultEmptyCell];
    }
    
    DayLeaveDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
    if (cell == nil) {
        cell = [[DayLeaveDetailCell alloc]init];
    }
    cell.lbLeft.font = [UIFont systemFontOfSize:14];
    cell.lbLeft.backgroundColor = [UIColor clearColor];
    cell.lbRight.font = [UIFont boldSystemFontOfSize:16];
    cell.lbRight.backgroundColor = [UIColor clearColor];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    int section = indexPath.section;
    empl_tns1_V_EmpProfileDiscipline *discipline = [disciplineArr objectAtIndex:section];
    
    if (indexPath.row == 0) {
        cell.lbLeft.text = @"Quyết định";
        cell.lbRight.text = discipline.DecisionNo;
    }
    
    if (indexPath.row == 1) {
        cell.lbLeft.text = @"Lí do";
        cell.lbRight.text = @"";
    }
    
    if (indexPath.row == 2) {
        cell.lbLeft.text = @"Loại kỷ luật";
        cell.lbRight.text = @"";//discipline.OrgDisciplineViolationName;
    }
    if (indexPath.row == 3) {
        cell.lbLeft.text = @"Hình thức";
        cell.lbRight.text = @"";//discipline.OrgDisciplineMethodName;
    }
    
    if (indexPath.row == 4) {
        cell.lbLeft.text = @"Ngày ban hành";
        cell.lbRight.text = [formatter stringFromDate:discipline.DateOfIssue];
    }
    
//    if (indexPath.row == 5)
//    {
//        cell.lbLeft.text = @"Tiền mặt";
//        cell.lbRight.text = discipline.MonetaryDiscipline.stringValue;
//    }
    return cell;
}


#pragma get Emp Reward
-(void)getData
{
    disciplineArr = [[NSMutableArray alloc]init];
    titleArr = [[NSMutableArray alloc]init];
    
    [self getRewardById];
}
-(void)getRewardById
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Đang cập nhật";
    
    BasicHttpBinding_IEmpProfileLayerServiceBinding *binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
    EmpProfileLayerServiceSvc_GetViewEmpProfileDisciplineById *request = [[EmpProfileLayerServiceSvc_GetViewEmpProfileDisciplineById alloc]init];
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    if (app.otherEmpId != nil)
        request.employeeId = app.otherEmpId;
    else
        request.employeeId = app.sysUser.EmployeeId;
    [binding GetViewEmpProfileDisciplineByIdAsyncUsingParameters:request delegate:self];
}

-(void)getDisciplineViolationById:(NSNumber *)violationId
{
//    BasicHttpBinding_IEmployeeServiceBinding *binding = [EmployeeServiceSvc BasicHttpBinding_IEmployeeServiceBinding];
//    EmployeeServiceSvc_GetViewEmpProfileDisciplinesByEmployeeId *request = [[EmployeeServiceSvc_GetViewEmpProfileDisciplinesByEmployeeId alloc] init];
//    request.disciplineViolationId = violationId;
//    
//    [binding GetOrgDisciplineViolationByIdAsyncUsingParameters:request delegate:self];
}

- (void)operation:(BasicHttpBinding_IEmpProfileLayerServiceBindingOperation *)operation completedWithResponse:(id)response
{    
    if ([response isKindOfClass:[BasicHttpBinding_IEmpProfileLayerServiceBindingResponse class]])
    {
        BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *layerResponse = (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse*)response;
        
        for (id mine in layerResponse.bodyParts) {
            if ([mine isKindOfClass:[EmpProfileLayerServiceSvc_GetViewEmpProfileDisciplineByIdResponse class]]) {
                empl_tns1_ArrayOfV_EmpProfileDiscipline *result = [mine GetViewEmpProfileDisciplineByIdResult];
                disciplineArr = result.V_EmpProfileDiscipline;
                for (empl_tns1_V_EmpProfileDiscipline *discipline in disciplineArr) {
                    [titleArr addObject:discipline.Title];
                    
                }
            }
        }
    }
    else if ([response isKindOfClass:[BasicHttpBinding_IEmployeeServiceBindingResponse class]])
    {
//        BasicHttpBinding_IEmployeeServiceBindingResponse* profileResponse = (BasicHttpBinding_IEmployeeServiceBindingResponse*)response;
//        for (id mine in profileResponse.bodyParts) {
//            emp_tns1_OrgDisciplineViolation *disciplineViolation = [mine GetOrgDisciplineViolationByIdResult];
//            [disciplineViolationArr addObject:disciplineViolation];
//        }
    }
    
    if (self != nil && !self.viewDidDisappear)
    {
        [self.tableView reloadData];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }
}


@end
