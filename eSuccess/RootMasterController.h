//
//  RootMasterController.h
//  eSuccess
//
//  Created by Scott on 4/30/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>
#define MasterChangedNotification @"MasterNotification"

@interface RootMasterController : UINavigationController
{
    
}
-(void)replaceToDashboard;
//-(void)replaceToCareer;
-(void)replacePerformance;
-(void)replaceELearning;
-(void)replaceToProfile;
-(void)replaceToMyTask;
-(void)replaceToSelfService;
-(void)replaceToMyJob;
-(void)replaceToMyTeam;

@end
