//
//  InternalCourseViewController.m
//  eSuccess
//
//  Created by admin on 5/10/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "InternalCourseViewController.h"
#import "MBProgressHUD.h"
#import "DayLeaveDetailCell.h"
#import "AppDelegate.h" 
@interface InternalCourseViewController ()
{
    int i;
}
@end

@implementation InternalCourseViewController
@synthesize courseArray;
@synthesize courseInforArray;
static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Các khoá học nội bộ";
    courseArray = [[NSMutableArray alloc]init];
    courseInforArray = [[NSMutableArray alloc]init];
    [self getData];
    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([courseInforArray count] > 0) {
        return [[courseInforArray objectAtIndex:section]CoursesName];
    }
    return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if([courseArray count] > 0)
        return [courseArray count];
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //return [profileArray count];
    if([courseArray count] > 0){
        return 3;
    }
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (courseArray.count == 0)
        return [self getDefaultEmptyCell];
    
    DayLeaveDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
    if (cell == nil) {
        cell = [[DayLeaveDetailCell alloc]init];
    }
    cell.lbLeft.font = [UIFont systemFontOfSize:14];
    cell.lbLeft.backgroundColor = [UIColor clearColor];
    cell.lbRight.font = [UIFont boldSystemFontOfSize:16];
    cell.lbRight.backgroundColor = [UIColor clearColor];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    int section = indexPath.section;
    tns2_EmpProfileInternalCourse *course = [courseArray objectAtIndex:section];
    if (indexPath.row == 0) {
        cell.lbLeft.text = @"Thời gian";
        tns2_EmpInternalCourse *c = [courseInforArray objectAtIndex:section];
        cell.lbRight.text = [NSString stringWithFormat:@"%@ - %@",[formatter stringFromDate:c.StartDate],[formatter stringFromDate:c.EndDate] ];
    }
    if (indexPath.row == 1) {
        cell.lbLeft.text = @"Kết quả";
        cell.lbRight.text = course.Result;
    }
    if (indexPath.row == 2) {
        cell.lbLeft.text = @"Trạng thái";
        if ([course.Status integerValue] == 0) {
            cell.lbRight.text = @"Đang học";
        } else if([course.Status integerValue] == 1){
            cell.lbRight.text = @"Đã hoàn thành";
        } else{
            cell.lbRight.text = @"Chưa học";
        }
    }
    return cell;
}


#pragma getData
- (void)getData
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Đang cập nhật";
    
    
}


@end
