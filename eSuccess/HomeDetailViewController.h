//
//  HomeDetailViewController.h
//  Presidents
//
//  Created by HPTVIETNAM on 4/13/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseDetailTableViewController.h"
#import "EmpProfileLayerServiceSvc.h"
@interface HomeDetailViewController : BaseDetailTableViewController<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>


@end
