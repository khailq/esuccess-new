//
//  OrgCell.m
//  eSuccess
//
//  Created by HPTVIETNAM on 5/7/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "OrgCell.h"
#define LeftMargin 10

@implementation OrgCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(int)calculateSize
{
    int sizePerCol = PanelWidth + LeftMargin;
    int columns = ceil(self.bounds.size
    .width / sizePerCol);
    
    return columns;
}

-(void)setChildren:(NSArray *)children
{
    if (_children != children)
    {
        _children = children;
        if (_children.count > 0)
        {
            for (int i = 0; i < [self calculateSize]; ++i)
            {
                if (i < _children.count)
                {
                    CGRect rect = CGRectMake(i * (LeftMargin + PanelWidth) + LeftMargin, 0, PanelWidth, PanelHeight);
                    ProfilePanelView *panel = [[ProfilePanelView alloc] initWithFrame:rect];
                    
                    [self addSubview:panel];
                }
            }
        }
    }
}
@end
