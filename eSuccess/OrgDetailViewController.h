//
//  OrgDetailViewController.h
//  eSuccess
//
//  Created by HPTVIETNAM on 5/10/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailViewController.h"
//#import "GMGridView.h"

@interface OrgDetailViewController : BaseDetailViewController

@property (strong, nonatomic) IBOutlet UIImageView *userAvatar;
@property (strong, nonatomic) IBOutlet UILabel *lb_name;
@property (strong, nonatomic) IBOutlet UILabel *lb_position;
@property (strong, nonatomic) IBOutlet UILabel *lb_unitName;

@end
