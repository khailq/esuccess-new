//
//  RootMasterController.m
//  eSuccess
//
//  Created by Scott on 4/30/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//



#import "RootMasterController.h"
#import "AppDelegate.h"
#import "BaseNavigationViewController.h"

static NSString* ToPortraitDashboard = @"ToPortraitDashBoardSegue";
static NSString* ToLandscapeDashboard = @"ToLandscapeDashBoardSegue";
//static NSString * ToCareerMaster = @"ToCareerSegue";
static NSString * ToPerformanceMaster = @"ToMyPerformance";
static NSString * ToLearningMaster = @"ToLearningSegue";
static NSString * ToMyProfileMaster = @"ToMyProfileSegue";
static NSString * ToMyTaskMaster = @"ToMyTaskSegue";
static NSString * ToMyJobMaster = @"ToMyJobSegue";
static NSString * ToSelfServiceMaster = @"ToSelfServiceSegue";
static NSString * ToMyTeamMaster = @"ToMyTeamSegue";

@implementation RootMasterController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationBar.tintColor = [UIColor blackColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Replace methods

-(void)replaceToMyTeam
{
    [self performSegueWithIdentifier:ToMyTeamMaster sender:self];
}

-(void)replaceToProfile
{
    [self performSegueWithIdentifier:ToMyProfileMaster sender:self];
}

-(void)replaceToMyTask
{
    [self performSegueWithIdentifier:ToMyTaskMaster sender:self];
}

-(void)replaceToMyJob
{
    [self performSegueWithIdentifier:ToMyJobMaster sender:self];
}

-(void)replaceToSelfService
{
    [self performSegueWithIdentifier:ToSelfServiceMaster sender:self];
}


-(void)replaceToDashboard
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsPortrait(orientation))
        [self performSegueWithIdentifier:ToPortraitDashboard sender:self];
    else
        [self performSegueWithIdentifier:ToLandscapeDashboard sender:self];
}

-(void)replaceELearning
{
    [self performSegueWithIdentifier:ToLearningMaster sender:self];
}

-(void)replacePerformance
{
    [self performSegueWithIdentifier:ToPerformanceMaster sender:self];
}

#pragma mark - Segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"identifier %@",segue.identifier);
   
    if ([segue.destinationViewController respondsToSelector:@selector(receivedMasterNotification:)])
        [[NSNotificationCenter defaultCenter] addObserver:segue.destinationViewController selector:@selector(receivedMasterNotification:) name:MasterChangedNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:MasterChangedNotification object:self];

}

@end
