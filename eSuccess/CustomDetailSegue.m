//
//  CustomDetailSegue.m
//  eSuccess
//
//  Created by Scott on 5/2/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "CustomDetailSegue.h"
#import "BaseNavigationViewController.h"
#import "CustomSplitViewController.h"

@implementation CustomDetailSegue

-(void)perform
{
    CustomSplitViewController *splitViewController = (CustomSplitViewController*) [[[UIApplication sharedApplication] delegate] window].rootViewController;
    
    BaseNavigationViewController *baseNavigation = (BaseNavigationViewController*)splitViewController.detailViewController;
    
//    if (![self isClassName:NSStringFromClass([self.destinationViewController class]) existInStack:baseNavigation.viewControllers])
//    {
        baseNavigation.viewControllers = [NSArray array];
        [baseNavigation pushViewController:self.destinationViewController animated:YES];
//    }
}

-(BOOL)isClassName:(NSString*)name existInStack:(NSArray *)controllers
{
    for(id object in controllers)
    {
        NSString *className = NSStringFromClass([object class]);
        if ([name isEqualToString:className])
            return true;
    }
    
    return false;
}
@end
