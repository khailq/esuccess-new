//
//  OrgTopPanelView.m
//  eSuccess
//
//  Created by HPTVIETNAM on 5/11/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "OrgTopPanelView.h"

@implementation OrgTopPanelView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)drawRect:(CGRect)rect {
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextBeginPath(ctx);
    
    CGFloat maxY = CGRectGetMaxY(rect);
    CGFloat minY = CGRectGetMinY(rect);
    CGFloat minX = CGRectGetMinX(rect);
    CGFloat maxX = CGRectGetMaxX(rect);
    CGFloat midX = CGRectGetMidX(rect);
    
    CGContextMoveToPoint(ctx, 0, 0);
    CGContextAddLineToPoint(ctx, minX, maxY);
    CGContextAddLineToPoint(ctx, midX - 20, maxY);
    CGContextAddLineToPoint(ctx, midX, maxY - 15);
    CGContextAddLineToPoint(ctx, midX + 20, maxY);
    CGContextAddLineToPoint(ctx, maxX, maxY);
    CGContextAddLineToPoint(ctx, maxX, minY);

    CGContextClosePath(ctx);
    
    CGContextSetFillColorWithColor(ctx, [UIColor colorWithRed:225.0f/255.0f green:225.0f/255.0f blue:225.0f/255.0f alpha:1].CGColor);
    //CGContextSetFillColorWithColor(ctx, [UIColor blueColor].CGColor);
    
    CGContextFillPath(ctx);
}


@end
