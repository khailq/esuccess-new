//
//  FeedbackViewController.m
//  eSuccess
//
//  Created by admin on 5/31/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "FeedbackViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "FeedbackCell.h"
@interface FeedbackViewController ()

@end

@implementation FeedbackViewController
@synthesize feedbackArr;
@synthesize timeArr;
static NSString *feedbackCellIdentifier = @"FeedbackCell";
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Góp ý nhận xét";
    [self getData];
    UINib *nib = [UINib nibWithNibName:@"FeedbackCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:feedbackCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([timeArr count] > 0) {
        return [timeArr objectAtIndex:section];
    }
    return nil;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 130;
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [timeArr count] > 0 ? timeArr.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //return [profileArray count];
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (feedbackArr.count == 0)
        return [self getDefaultEmptyCell];
    
    FeedbackCell *cell = [tableView dequeueReusableCellWithIdentifier:feedbackCellIdentifier];
   
    return cell;
}

#pragma get Data Working Experience
-(void)getData
{
}

@end
