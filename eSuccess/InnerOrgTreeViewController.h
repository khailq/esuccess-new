//
//  InnerOrgTreeViewController.h
//  eSuccess
//
//  Created by HPTVIETNAM on 5/19/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrgUnitTree.h"

@interface InnerOrgTreeViewController : UITableViewController

@property (strong, nonatomic) OrgUnitTree *dataContext;

@end
