//
//  TableTitleCell.h
//  eSuccess
//
//  Created by HPTVIETNAM on 6/11/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableTitleCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lb_title;
@property (weak, nonatomic) IBOutlet UILabel *lb_additional;
@end
