//
//  SocialInsuranceSalaryViewController.h
//  eSuccess
//
//  Created by HPTVIETNAM on 6/25/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"
#import "EmpProfileLayerServiceSvc.h"
@interface SocialInsuranceSalaryViewController : BaseDetailTableViewController<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>
@property(nonatomic, strong) NSMutableArray *socialInsSalaryArr;
@property(nonatomic, strong) NSMutableArray *gradeArr;

@end
