#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class SystemServiceSvc_GetMenuBySysApplicationCode;
@class SystemServiceSvc_GetMenuBySysApplicationCodeResponse;
@class SystemServiceSvc_GetAllGroupMenus;
@class SystemServiceSvc_GetAllGroupMenusResponse;
@class SystemServiceSvc_GetAllMenus;
@class SystemServiceSvc_GetAllMenusResponse;
@class SystemServiceSvc_CreateGroupMenu;
@class SystemServiceSvc_CreateGroupMenuResponse;
@class SystemServiceSvc_CreateMenu;
@class SystemServiceSvc_CreateMenuResponse;
@class SystemServiceSvc_UpdateGroupMenu;
@class SystemServiceSvc_UpdateGroupMenuResponse;
@class SystemServiceSvc_UpdateMenu;
@class SystemServiceSvc_UpdateMenuResponse;
@class SystemServiceSvc_DeleteGroupMenu;
@class SystemServiceSvc_DeleteGroupMenuResponse;
@class SystemServiceSvc_DeleteMenu;
@class SystemServiceSvc_DeleteMenuResponse;
@class SystemServiceSvc_GetAllViewMenus;
@class SystemServiceSvc_GetAllViewMenusResponse;
@class SystemServiceSvc_GetViewMenuByApplicationCode;
@class SystemServiceSvc_GetViewMenuByApplicationCodeResponse;
@class SystemServiceSvc_GetGroupMenusByAplicationCode;
@class SystemServiceSvc_GetGroupMenusByAplicationCodeResponse;
@class SystemServiceSvc_GetGroupMenusByAplicationId;
@class SystemServiceSvc_GetGroupMenusByAplicationIdResponse;
@class SystemServiceSvc_GetGroupMenusById;
@class SystemServiceSvc_GetGroupMenusByIdResponse;
@class SystemServiceSvc_GetMenusById;
@class SystemServiceSvc_GetMenusByIdResponse;
@class SystemServiceSvc_GetSysApplicationByCompanyId;
@class SystemServiceSvc_GetSysApplicationByCompanyIdResponse;
@class SystemServiceSvc_GetSysApplicationByProductCodeAndCompanyId;
@class SystemServiceSvc_GetSysApplicationByProductCodeAndCompanyIdResponse;
@class SystemServiceSvc_GetMenuByUserPermisson;
@class SystemServiceSvc_GetMenuByUserPermissonResponse;
@class SystemServiceSvc_GetAllViewGroupMenus;
@class SystemServiceSvc_GetAllViewGroupMenusResponse;
@class SystemServiceSvc_GetAllSysApplications;
@class SystemServiceSvc_GetAllSysApplicationsResponse;
@class SystemServiceSvc_GetSysApplicationBySysApplicationId;
@class SystemServiceSvc_GetSysApplicationBySysApplicationIdResponse;
@class SystemServiceSvc_CreateSysModule;
@class SystemServiceSvc_CreateSysModuleResponse;
@class SystemServiceSvc_GetSysModuleBySysGroupMenuId;
@class SystemServiceSvc_GetSysModuleBySysGroupMenuIdResponse;
@class SystemServiceSvc_UpdateSysModule;
@class SystemServiceSvc_UpdateSysModuleResponse;
@class SystemServiceSvc_CreateFeature;
@class SystemServiceSvc_CreateFeatureResponse;
@class SystemServiceSvc_CreateAction;
@class SystemServiceSvc_CreateActionResponse;
@class SystemServiceSvc_CreateLink;
@class SystemServiceSvc_CreateLinkResponse;
@class SystemServiceSvc_GetSysFeatureBySysMenuId;
@class SystemServiceSvc_GetSysFeatureBySysMenuIdResponse;
@class SystemServiceSvc_GetSysActionBySysFeatureId;
@class SystemServiceSvc_GetSysActionBySysFeatureIdResponse;
@class SystemServiceSvc_GetSysLinkBySysActionId;
@class SystemServiceSvc_GetSysLinkBySysActionIdResponse;
@class SystemServiceSvc_UpdateSysFeature;
@class SystemServiceSvc_UpdateSysFeatureResponse;
@class SystemServiceSvc_UpdateSysLink;
@class SystemServiceSvc_UpdateSysLinkResponse;
@class SystemServiceSvc_GetModuleByGroupMenu;
@class SystemServiceSvc_GetModuleByGroupMenuResponse;
@class SystemServiceSvc_GetSysParamList;
@class SystemServiceSvc_GetSysParamListResponse;
@class SystemServiceSvc_GetSysParamById;
@class SystemServiceSvc_GetSysParamByIdResponse;
@class SystemServiceSvc_GetSysParamByKeyCodeType;
@class SystemServiceSvc_GetSysParamByKeyCodeTypeResponse;
@class SystemServiceSvc_GetSysParamListByKey;
@class SystemServiceSvc_GetSysParamListByKeyResponse;
@class SystemServiceSvc_GetSysParamListByKeyCode;
@class SystemServiceSvc_GetSysParamListByKeyCodeResponse;
@class SystemServiceSvc_GetSysParamListByKCT;
@class SystemServiceSvc_GetSysParamListByKCTResponse;
@class SystemServiceSvc_CreateSysParam;
@class SystemServiceSvc_CreateSysParamResponse;
@class SystemServiceSvc_UpdateSysParam;
@class SystemServiceSvc_UpdateSysParamResponse;
@class SystemServiceSvc_DeleteSysParam;
@class SystemServiceSvc_DeleteSysParamResponse;
@class SystemServiceSvc_GetAllTemplate;
@class SystemServiceSvc_GetAllTemplateResponse;
@class SystemServiceSvc_CreateTemplate;
@class SystemServiceSvc_CreateTemplateResponse;
@class SystemServiceSvc_UpdateTemplate;
@class SystemServiceSvc_UpdateTemplateResponse;
@class SystemServiceSvc_DeleteTemplate;
@class SystemServiceSvc_DeleteTemplateResponse;
@class SystemServiceSvc_GetAllSysProfiles;
@class SystemServiceSvc_GetAllSysProfilesResponse;
@class SystemServiceSvc_GetAllSysSubProfilesByProfileCode;
@class SystemServiceSvc_GetAllSysSubProfilesByProfileCodeResponse;
@class SystemServiceSvc_GetAllSysProfileLayer;
@class SystemServiceSvc_GetAllSysProfileLayerResponse;
@class SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE;
@class SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGEResponse;
@class SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_BySkipTake;
@class SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_BySkipTakeResponse;
@class SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLangSkipTake;
@class SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLangSkipTakeResponse;
@class SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLang;
@class SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLangResponse;
@class SystemServiceSvc_Get_TB_ESS_SYSTEM_LANGUAGE_ByContentCode;
@class SystemServiceSvc_Get_TB_ESS_SYSTEM_LANGUAGE_ByContentCodeResponse;
@class SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByConditions;
@class SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByConditionsResponse;
@class SystemServiceSvc_CreateSystemLanguage;
@class SystemServiceSvc_CreateSystemLanguageResponse;
@class SystemServiceSvc_DeleteSystemLanguageById;
@class SystemServiceSvc_DeleteSystemLanguageByIdResponse;
@class SystemServiceSvc_DeleteSystemLanguageByContentCode;
@class SystemServiceSvc_DeleteSystemLanguageByContentCodeResponse;
@class SystemServiceSvc_EditSystemLanguage;
@class SystemServiceSvc_EditSystemLanguageResponse;
@class SystemServiceSvc_GeLanguageContentByLangCodeAndContentCode;
@class SystemServiceSvc_GeLanguageContentByLangCodeAndContentCodeResponse;
#import "sys_tns1.h"
@interface SystemServiceSvc_GetMenuBySysApplicationCode : NSObject {
	
/* elements */
	NSString * sysApplicationCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetMenuBySysApplicationCode *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * sysApplicationCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetMenuBySysApplicationCodeResponse : NSObject {
	
/* elements */
	sys_tns1_ArrayOfV_SysMenu * GetMenuBySysApplicationCodeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetMenuBySysApplicationCodeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_ArrayOfV_SysMenu * GetMenuBySysApplicationCodeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAllGroupMenus : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAllGroupMenus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAllGroupMenusResponse : NSObject {
	
/* elements */
	sys_tns1_ArrayOfSysGroupMenu * GetAllGroupMenusResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAllGroupMenusResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_ArrayOfSysGroupMenu * GetAllGroupMenusResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAllMenus : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAllMenus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAllMenusResponse : NSObject {
	
/* elements */
	sys_tns1_ArrayOfSysMenu * GetAllMenusResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAllMenusResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_ArrayOfSysMenu * GetAllMenusResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_CreateGroupMenu : NSObject {
	
/* elements */
	sys_tns1_SysGroupMenu * groupMenu;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_CreateGroupMenu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_SysGroupMenu * groupMenu;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_CreateGroupMenuResponse : NSObject {
	
/* elements */
	NSNumber * CreateGroupMenuResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_CreateGroupMenuResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CreateGroupMenuResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_CreateMenu : NSObject {
	
/* elements */
	sys_tns1_SysMenu * menu;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_CreateMenu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_SysMenu * menu;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_CreateMenuResponse : NSObject {
	
/* elements */
	NSNumber * CreateMenuResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_CreateMenuResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CreateMenuResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_UpdateGroupMenu : NSObject {
	
/* elements */
	sys_tns1_SysGroupMenu * groupMenu;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_UpdateGroupMenu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_SysGroupMenu * groupMenu;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_UpdateGroupMenuResponse : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_UpdateGroupMenuResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_UpdateMenu : NSObject {
	
/* elements */
	sys_tns1_SysMenu * menu;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_UpdateMenu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_SysMenu * menu;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_UpdateMenuResponse : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_UpdateMenuResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_DeleteGroupMenu : NSObject {
	
/* elements */
	NSNumber * sysGroupMenuId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_DeleteGroupMenu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * sysGroupMenuId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_DeleteGroupMenuResponse : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_DeleteGroupMenuResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_DeleteMenu : NSObject {
	
/* elements */
	NSNumber * sysMenuId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_DeleteMenu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * sysMenuId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_DeleteMenuResponse : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_DeleteMenuResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAllViewMenus : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAllViewMenus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAllViewMenusResponse : NSObject {
	
/* elements */
	sys_tns1_ArrayOfV_SysMenu * GetAllViewMenusResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAllViewMenusResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_ArrayOfV_SysMenu * GetAllViewMenusResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetViewMenuByApplicationCode : NSObject {
	
/* elements */
	NSString * applicationCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetViewMenuByApplicationCode *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * applicationCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetViewMenuByApplicationCodeResponse : NSObject {
	
/* elements */
	sys_tns1_ArrayOfV_SysMenu * GetViewMenuByApplicationCodeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetViewMenuByApplicationCodeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_ArrayOfV_SysMenu * GetViewMenuByApplicationCodeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetGroupMenusByAplicationCode : NSObject {
	
/* elements */
	NSString * applicationCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetGroupMenusByAplicationCode *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * applicationCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetGroupMenusByAplicationCodeResponse : NSObject {
	
/* elements */
	sys_tns1_ArrayOfSysGroupMenu * GetGroupMenusByAplicationCodeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetGroupMenusByAplicationCodeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_ArrayOfSysGroupMenu * GetGroupMenusByAplicationCodeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetGroupMenusByAplicationId : NSObject {
	
/* elements */
	NSNumber * id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetGroupMenusByAplicationId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetGroupMenusByAplicationIdResponse : NSObject {
	
/* elements */
	sys_tns1_ArrayOfSysGroupMenu * GetGroupMenusByAplicationIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetGroupMenusByAplicationIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_ArrayOfSysGroupMenu * GetGroupMenusByAplicationIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetGroupMenusById : NSObject {
	
/* elements */
	NSNumber * id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetGroupMenusById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetGroupMenusByIdResponse : NSObject {
	
/* elements */
	sys_tns1_SysGroupMenu * GetGroupMenusByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetGroupMenusByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_SysGroupMenu * GetGroupMenusByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetMenusById : NSObject {
	
/* elements */
	NSNumber * id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetMenusById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetMenusByIdResponse : NSObject {
	
/* elements */
	sys_tns1_SysMenu * GetMenusByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetMenusByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_SysMenu * GetMenusByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysApplicationByCompanyId : NSObject {
	
/* elements */
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysApplicationByCompanyId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysApplicationByCompanyIdResponse : NSObject {
	
/* elements */
	sys_tns1_ArrayOfSysApplication * GetSysApplicationByCompanyIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysApplicationByCompanyIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_ArrayOfSysApplication * GetSysApplicationByCompanyIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysApplicationByProductCodeAndCompanyId : NSObject {
	
/* elements */
	NSString * ProductCode;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysApplicationByProductCodeAndCompanyId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ProductCode;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysApplicationByProductCodeAndCompanyIdResponse : NSObject {
	
/* elements */
	sys_tns1_ArrayOfSysApplication * GetSysApplicationByProductCodeAndCompanyIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysApplicationByProductCodeAndCompanyIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_ArrayOfSysApplication * GetSysApplicationByProductCodeAndCompanyIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetMenuByUserPermisson : NSObject {
	
/* elements */
	NSNumber * UserId;
	NSNumber * CompanyId;
	NSString * ModuleCode;
	NSString * ProductCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetMenuByUserPermisson *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * UserId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ModuleCode;
@property (retain) NSString * ProductCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetMenuByUserPermissonResponse : NSObject {
	
/* elements */
	sys_tns1_ArrayOfSP_GET_MENU_BY_USER_PERMISSION_Result * GetMenuByUserPermissonResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetMenuByUserPermissonResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_ArrayOfSP_GET_MENU_BY_USER_PERMISSION_Result * GetMenuByUserPermissonResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAllViewGroupMenus : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAllViewGroupMenus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAllViewGroupMenusResponse : NSObject {
	
/* elements */
	sys_tns1_ArrayOfV_SysGroupMenu * GetAllViewGroupMenusResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAllViewGroupMenusResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_ArrayOfV_SysGroupMenu * GetAllViewGroupMenusResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAllSysApplications : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAllSysApplications *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAllSysApplicationsResponse : NSObject {
	
/* elements */
	sys_tns1_ArrayOfSysApplication * GetAllSysApplicationsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAllSysApplicationsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_ArrayOfSysApplication * GetAllSysApplicationsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysApplicationBySysApplicationId : NSObject {
	
/* elements */
	NSNumber * Id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysApplicationBySysApplicationId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysApplicationBySysApplicationIdResponse : NSObject {
	
/* elements */
	sys_tns1_SysApplication * GetSysApplicationBySysApplicationIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysApplicationBySysApplicationIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_SysApplication * GetSysApplicationBySysApplicationIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_CreateSysModule : NSObject {
	
/* elements */
	sys_tns1_SysModule * module;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_CreateSysModule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_SysModule * module;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_CreateSysModuleResponse : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_CreateSysModuleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysModuleBySysGroupMenuId : NSObject {
	
/* elements */
	NSNumber * groupMenuId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysModuleBySysGroupMenuId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * groupMenuId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysModuleBySysGroupMenuIdResponse : NSObject {
	
/* elements */
	sys_tns1_SysModule * GetSysModuleBySysGroupMenuIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysModuleBySysGroupMenuIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_SysModule * GetSysModuleBySysGroupMenuIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_UpdateSysModule : NSObject {
	
/* elements */
	sys_tns1_SysModule * sysModule;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_UpdateSysModule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_SysModule * sysModule;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_UpdateSysModuleResponse : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_UpdateSysModuleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_CreateFeature : NSObject {
	
/* elements */
	sys_tns1_SysFeature * feature;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_CreateFeature *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_SysFeature * feature;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_CreateFeatureResponse : NSObject {
	
/* elements */
	NSNumber * CreateFeatureResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_CreateFeatureResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CreateFeatureResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_CreateAction : NSObject {
	
/* elements */
	sys_tns1_SysAction * action;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_CreateAction *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_SysAction * action;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_CreateActionResponse : NSObject {
	
/* elements */
	NSNumber * CreateActionResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_CreateActionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CreateActionResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_CreateLink : NSObject {
	
/* elements */
	sys_tns1_SysLink * link;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_CreateLink *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_SysLink * link;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_CreateLinkResponse : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_CreateLinkResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysFeatureBySysMenuId : NSObject {
	
/* elements */
	NSNumber * sysMenuId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysFeatureBySysMenuId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * sysMenuId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysFeatureBySysMenuIdResponse : NSObject {
	
/* elements */
	sys_tns1_SysFeature * GetSysFeatureBySysMenuIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysFeatureBySysMenuIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_SysFeature * GetSysFeatureBySysMenuIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysActionBySysFeatureId : NSObject {
	
/* elements */
	NSNumber * sysFeatureId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysActionBySysFeatureId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * sysFeatureId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysActionBySysFeatureIdResponse : NSObject {
	
/* elements */
	sys_tns1_SysAction * GetSysActionBySysFeatureIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysActionBySysFeatureIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_SysAction * GetSysActionBySysFeatureIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysLinkBySysActionId : NSObject {
	
/* elements */
	NSNumber * sysActionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysLinkBySysActionId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * sysActionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysLinkBySysActionIdResponse : NSObject {
	
/* elements */
	sys_tns1_SysLink * GetSysLinkBySysActionIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysLinkBySysActionIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_SysLink * GetSysLinkBySysActionIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_UpdateSysFeature : NSObject {
	
/* elements */
	sys_tns1_SysFeature * sysFeature;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_UpdateSysFeature *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_SysFeature * sysFeature;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_UpdateSysFeatureResponse : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_UpdateSysFeatureResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_UpdateSysLink : NSObject {
	
/* elements */
	sys_tns1_SysLink * link;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_UpdateSysLink *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_SysLink * link;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_UpdateSysLinkResponse : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_UpdateSysLinkResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetModuleByGroupMenu : NSObject {
	
/* elements */
	NSNumber * groupMenuId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetModuleByGroupMenu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * groupMenuId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetModuleByGroupMenuResponse : NSObject {
	
/* elements */
	sys_tns1_SysModule * GetModuleByGroupMenuResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetModuleByGroupMenuResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_SysModule * GetModuleByGroupMenuResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysParamList : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysParamList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysParamListResponse : NSObject {
	
/* elements */
	sys_tns1_ArrayOfTB_ESS_SYSTEM_PARAMETER * GetSysParamListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysParamListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_ArrayOfTB_ESS_SYSTEM_PARAMETER * GetSysParamListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysParamById : NSObject {
	
/* elements */
	NSNumber * intSysParamId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysParamById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * intSysParamId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysParamByIdResponse : NSObject {
	
/* elements */
	sys_tns1_TB_ESS_SYSTEM_PARAMETER * GetSysParamByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysParamByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_TB_ESS_SYSTEM_PARAMETER * GetSysParamByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysParamByKeyCodeType : NSObject {
	
/* elements */
	NSString * strKey;
	NSString * strCode;
	NSString * strType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysParamByKeyCodeType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * strKey;
@property (retain) NSString * strCode;
@property (retain) NSString * strType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysParamByKeyCodeTypeResponse : NSObject {
	
/* elements */
	sys_tns1_TB_ESS_SYSTEM_PARAMETER * GetSysParamByKeyCodeTypeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysParamByKeyCodeTypeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_TB_ESS_SYSTEM_PARAMETER * GetSysParamByKeyCodeTypeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysParamListByKey : NSObject {
	
/* elements */
	NSString * strKey;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysParamListByKey *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * strKey;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysParamListByKeyResponse : NSObject {
	
/* elements */
	sys_tns1_ArrayOfTB_ESS_SYSTEM_PARAMETER * GetSysParamListByKeyResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysParamListByKeyResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_ArrayOfTB_ESS_SYSTEM_PARAMETER * GetSysParamListByKeyResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysParamListByKeyCode : NSObject {
	
/* elements */
	NSString * strKey;
	NSString * strCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysParamListByKeyCode *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * strKey;
@property (retain) NSString * strCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysParamListByKeyCodeResponse : NSObject {
	
/* elements */
	sys_tns1_ArrayOfTB_ESS_SYSTEM_PARAMETER * GetSysParamListByKeyCodeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysParamListByKeyCodeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_ArrayOfTB_ESS_SYSTEM_PARAMETER * GetSysParamListByKeyCodeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysParamListByKCT : NSObject {
	
/* elements */
	NSString * strKey;
	NSString * strCode;
	NSString * strType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysParamListByKCT *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * strKey;
@property (retain) NSString * strCode;
@property (retain) NSString * strType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetSysParamListByKCTResponse : NSObject {
	
/* elements */
	sys_tns1_ArrayOfTB_ESS_SYSTEM_PARAMETER * GetSysParamListByKCTResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetSysParamListByKCTResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_ArrayOfTB_ESS_SYSTEM_PARAMETER * GetSysParamListByKCTResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_CreateSysParam : NSObject {
	
/* elements */
	sys_tns1_TB_ESS_SYSTEM_PARAMETER * entity;
	NSNumber * intUserId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_CreateSysParam *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_TB_ESS_SYSTEM_PARAMETER * entity;
@property (retain) NSNumber * intUserId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_CreateSysParamResponse : NSObject {
	
/* elements */
	USBoolean * CreateSysParamResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_CreateSysParamResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * CreateSysParamResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_UpdateSysParam : NSObject {
	
/* elements */
	sys_tns1_TB_ESS_SYSTEM_PARAMETER * entity;
	NSNumber * intUserId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_UpdateSysParam *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_TB_ESS_SYSTEM_PARAMETER * entity;
@property (retain) NSNumber * intUserId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_UpdateSysParamResponse : NSObject {
	
/* elements */
	USBoolean * UpdateSysParamResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_UpdateSysParamResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateSysParamResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_DeleteSysParam : NSObject {
	
/* elements */
	sys_tns1_TB_ESS_SYSTEM_PARAMETER * entity;
	NSNumber * intUserId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_DeleteSysParam *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_TB_ESS_SYSTEM_PARAMETER * entity;
@property (retain) NSNumber * intUserId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_DeleteSysParamResponse : NSObject {
	
/* elements */
	USBoolean * DeleteSysParamResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_DeleteSysParamResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteSysParamResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAllTemplate : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAllTemplate *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAllTemplateResponse : NSObject {
	
/* elements */
	sys_tns1_ArrayOfTB_ESS_FILE_TEMPLATE_MANAGEMENT * GetAllTemplateResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAllTemplateResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_ArrayOfTB_ESS_FILE_TEMPLATE_MANAGEMENT * GetAllTemplateResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_CreateTemplate : NSObject {
	
/* elements */
	sys_tns1_TB_ESS_FILE_TEMPLATE_MANAGEMENT * template;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_CreateTemplate *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_TB_ESS_FILE_TEMPLATE_MANAGEMENT * template;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_CreateTemplateResponse : NSObject {
	
/* elements */
	NSNumber * CreateTemplateResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_CreateTemplateResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CreateTemplateResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_UpdateTemplate : NSObject {
	
/* elements */
	sys_tns1_TB_ESS_FILE_TEMPLATE_MANAGEMENT * template;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_UpdateTemplate *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_TB_ESS_FILE_TEMPLATE_MANAGEMENT * template;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_UpdateTemplateResponse : NSObject {
	
/* elements */
	USBoolean * UpdateTemplateResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_UpdateTemplateResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateTemplateResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_DeleteTemplate : NSObject {
	
/* elements */
	sys_tns1_TB_ESS_FILE_TEMPLATE_MANAGEMENT * template;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_DeleteTemplate *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_TB_ESS_FILE_TEMPLATE_MANAGEMENT * template;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_DeleteTemplateResponse : NSObject {
	
/* elements */
	USBoolean * DeleteTemplateResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_DeleteTemplateResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteTemplateResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAllSysProfiles : NSObject {
	
/* elements */
	NSNumber * CompanyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAllSysProfiles *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAllSysProfilesResponse : NSObject {
	
/* elements */
	sys_tns1_ArrayOfV_ESS_SYS_PROFILES * GetAllSysProfilesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAllSysProfilesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_ArrayOfV_ESS_SYS_PROFILES * GetAllSysProfilesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAllSysSubProfilesByProfileCode : NSObject {
	
/* elements */
	NSString * profileCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAllSysSubProfilesByProfileCode *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * profileCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAllSysSubProfilesByProfileCodeResponse : NSObject {
	
/* elements */
	sys_tns1_ArrayOfV_ESS_SYS_PROFILE_SUB_MAPPING * GetAllSysSubProfilesByProfileCodeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAllSysSubProfilesByProfileCodeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_ArrayOfV_ESS_SYS_PROFILE_SUB_MAPPING * GetAllSysSubProfilesByProfileCodeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAllSysProfileLayer : NSObject {
	
/* elements */
	NSNumber * CompanyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAllSysProfileLayer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAllSysProfileLayerResponse : NSObject {
	
/* elements */
	sys_tns1_ArrayOfV_ESS_SYS_PROFILE_LAYER * GetAllSysProfileLayerResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAllSysProfileLayerResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_ArrayOfV_ESS_SYS_PROFILE_LAYER * GetAllSysProfileLayerResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE : NSObject {
	
/* elements */
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGEResponse : NSObject {
	
/* elements */
	sys_tns1_ArrayOfTB_ESS_SYSTEM_LANGUAGE * GetAll_TB_ESS_SYSTEM_LANGUAGEResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGEResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_ArrayOfTB_ESS_SYSTEM_LANGUAGE * GetAll_TB_ESS_SYSTEM_LANGUAGEResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_BySkipTake : NSObject {
	
/* elements */
	NSNumber * skip;
	NSNumber * take;
	NSNumber * total;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_BySkipTake *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * total;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_BySkipTakeResponse : NSObject {
	
/* elements */
	sys_tns1_ArrayOfTB_ESS_SYSTEM_LANGUAGE * GetAll_TB_ESS_SYSTEM_LANGUAGE_BySkipTakeResult;
	NSNumber * total;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_BySkipTakeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_ArrayOfTB_ESS_SYSTEM_LANGUAGE * GetAll_TB_ESS_SYSTEM_LANGUAGE_BySkipTakeResult;
@property (retain) NSNumber * total;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLangSkipTake : NSObject {
	
/* elements */
	NSNumber * skip;
	NSNumber * take;
	NSNumber * total;
	NSString * langCode_;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLangSkipTake *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * total;
@property (retain) NSString * langCode_;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLangSkipTakeResponse : NSObject {
	
/* elements */
	sys_tns1_ArrayOfTB_ESS_SYSTEM_LANGUAGE * GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLangSkipTakeResult;
	NSNumber * total;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLangSkipTakeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_ArrayOfTB_ESS_SYSTEM_LANGUAGE * GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLangSkipTakeResult;
@property (retain) NSNumber * total;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLang : NSObject {
	
/* elements */
	NSString * langCode_;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLang *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * langCode_;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLangResponse : NSObject {
	
/* elements */
	sys_tns1_ArrayOfTB_ESS_SYSTEM_LANGUAGE * GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLangResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLangResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_ArrayOfTB_ESS_SYSTEM_LANGUAGE * GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLangResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_Get_TB_ESS_SYSTEM_LANGUAGE_ByContentCode : NSObject {
	
/* elements */
	NSString * contentCode;
	NSString * langCode_;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_Get_TB_ESS_SYSTEM_LANGUAGE_ByContentCode *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * contentCode;
@property (retain) NSString * langCode_;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_Get_TB_ESS_SYSTEM_LANGUAGE_ByContentCodeResponse : NSObject {
	
/* elements */
	sys_tns1_TB_ESS_SYSTEM_LANGUAGE * Get_TB_ESS_SYSTEM_LANGUAGE_ByContentCodeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_Get_TB_ESS_SYSTEM_LANGUAGE_ByContentCodeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_TB_ESS_SYSTEM_LANGUAGE * Get_TB_ESS_SYSTEM_LANGUAGE_ByContentCodeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByConditions : NSObject {
	
/* elements */
	NSString * displayType;
	NSString * countryCode;
	NSString * productCode;
	NSString * moduleCode;
	NSString * formCode;
	NSString * strContentCode;
	NSString * strContent;
	NSNumber * take;
	NSNumber * skip;
	NSNumber * count;
	NSNumber * userId;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByConditions *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * displayType;
@property (retain) NSString * countryCode;
@property (retain) NSString * productCode;
@property (retain) NSString * moduleCode;
@property (retain) NSString * formCode;
@property (retain) NSString * strContentCode;
@property (retain) NSString * strContent;
@property (retain) NSNumber * take;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * count;
@property (retain) NSNumber * userId;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByConditionsResponse : NSObject {
	
/* elements */
	sys_tns1_ArrayOfTB_ESS_SYSTEM_LANGUAGE * GetAll_TB_ESS_SYSTEM_LANGUAGE_ByConditionsResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByConditionsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_ArrayOfTB_ESS_SYSTEM_LANGUAGE * GetAll_TB_ESS_SYSTEM_LANGUAGE_ByConditionsResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_CreateSystemLanguage : NSObject {
	
/* elements */
	sys_tns1_TB_ESS_SYSTEM_LANGUAGE * tb_ESS_SYSTEM_LANGUAGE;
	NSNumber * userId;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_CreateSystemLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_TB_ESS_SYSTEM_LANGUAGE * tb_ESS_SYSTEM_LANGUAGE;
@property (retain) NSNumber * userId;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_CreateSystemLanguageResponse : NSObject {
	
/* elements */
	USBoolean * CreateSystemLanguageResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_CreateSystemLanguageResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * CreateSystemLanguageResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_DeleteSystemLanguageById : NSObject {
	
/* elements */
	NSNumber * langId;
	NSNumber * userId;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_DeleteSystemLanguageById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * langId;
@property (retain) NSNumber * userId;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_DeleteSystemLanguageByIdResponse : NSObject {
	
/* elements */
	USBoolean * DeleteSystemLanguageByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_DeleteSystemLanguageByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteSystemLanguageByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_DeleteSystemLanguageByContentCode : NSObject {
	
/* elements */
	NSString * contentCode;
	NSNumber * companyId;
	NSNumber * userId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_DeleteSystemLanguageByContentCode *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * contentCode;
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * userId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_DeleteSystemLanguageByContentCodeResponse : NSObject {
	
/* elements */
	USBoolean * DeleteSystemLanguageByContentCodeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_DeleteSystemLanguageByContentCodeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteSystemLanguageByContentCodeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_EditSystemLanguage : NSObject {
	
/* elements */
	sys_tns1_TB_ESS_SYSTEM_LANGUAGE * tb_ESS_SYSTEM_LANGUAGE;
	NSNumber * userId;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_EditSystemLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) sys_tns1_TB_ESS_SYSTEM_LANGUAGE * tb_ESS_SYSTEM_LANGUAGE;
@property (retain) NSNumber * userId;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_EditSystemLanguageResponse : NSObject {
	
/* elements */
	USBoolean * EditSystemLanguageResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_EditSystemLanguageResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * EditSystemLanguageResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GeLanguageContentByLangCodeAndContentCode : NSObject {
	
/* elements */
	NSString * languageCode;
	NSString * contentCode;
	NSNumber * companyId;
	NSNumber * userId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GeLanguageContentByLangCodeAndContentCode *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * languageCode;
@property (retain) NSString * contentCode;
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * userId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SystemServiceSvc_GeLanguageContentByLangCodeAndContentCodeResponse : NSObject {
	
/* elements */
	NSString * GeLanguageContentByLangCodeAndContentCodeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SystemServiceSvc_GeLanguageContentByLangCodeAndContentCodeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * GeLanguageContentByLangCodeAndContentCodeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
/* Cookies handling provided by http://en.wikibooks.org/wiki/Programming:WebObjects/Web_Services/Web_Service_Provider */
#import <libxml/parser.h>
#import "xs.h"
#import "SystemServiceSvc.h"
#import "ns1.h"
#import "sys_tns1.h"
#import "sys_tns2.h"
@class BasicHttpBinding_ISystemServiceBinding;
@interface SystemServiceSvc : NSObject {
	
}
+ (BasicHttpBinding_ISystemServiceBinding *)BasicHttpBinding_ISystemServiceBinding;
@end
@class BasicHttpBinding_ISystemServiceBindingResponse;
@class BasicHttpBinding_ISystemServiceBindingOperation;
@protocol BasicHttpBinding_ISystemServiceBindingResponseDelegate <NSObject>
- (void) operation:(BasicHttpBinding_ISystemServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_ISystemServiceBindingResponse *)response;
@end
@interface BasicHttpBinding_ISystemServiceBinding : NSObject <BasicHttpBinding_ISystemServiceBindingResponseDelegate> {
	NSURL *address;
	NSTimeInterval defaultTimeout;
	NSMutableArray *cookies;
	BOOL logXMLInOut;
	BOOL synchronousOperationComplete;
	NSString *authUsername;
	NSString *authPassword;
}
@property (copy) NSURL *address;
@property (assign) BOOL logXMLInOut;
@property (assign) NSTimeInterval defaultTimeout;
@property (nonatomic, retain) NSMutableArray *cookies;
@property (nonatomic, retain) NSString *authUsername;
@property (nonatomic, retain) NSString *authPassword;
- (id)initWithAddress:(NSString *)anAddress;
- (void)sendHTTPCallUsingBody:(NSString *)body soapAction:(NSString *)soapAction forOperation:(BasicHttpBinding_ISystemServiceBindingOperation *)operation;
- (void)addCookie:(NSHTTPCookie *)toAdd;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetMenuBySysApplicationCodeUsingParameters:(SystemServiceSvc_GetMenuBySysApplicationCode *)aParameters ;
- (void)GetMenuBySysApplicationCodeAsyncUsingParameters:(SystemServiceSvc_GetMenuBySysApplicationCode *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetAllGroupMenusUsingParameters:(SystemServiceSvc_GetAllGroupMenus *)aParameters ;
- (void)GetAllGroupMenusAsyncUsingParameters:(SystemServiceSvc_GetAllGroupMenus *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetAllMenusUsingParameters:(SystemServiceSvc_GetAllMenus *)aParameters ;
- (void)GetAllMenusAsyncUsingParameters:(SystemServiceSvc_GetAllMenus *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)CreateGroupMenuUsingParameters:(SystemServiceSvc_CreateGroupMenu *)aParameters ;
- (void)CreateGroupMenuAsyncUsingParameters:(SystemServiceSvc_CreateGroupMenu *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)CreateMenuUsingParameters:(SystemServiceSvc_CreateMenu *)aParameters ;
- (void)CreateMenuAsyncUsingParameters:(SystemServiceSvc_CreateMenu *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)UpdateGroupMenuUsingParameters:(SystemServiceSvc_UpdateGroupMenu *)aParameters ;
- (void)UpdateGroupMenuAsyncUsingParameters:(SystemServiceSvc_UpdateGroupMenu *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)UpdateMenuUsingParameters:(SystemServiceSvc_UpdateMenu *)aParameters ;
- (void)UpdateMenuAsyncUsingParameters:(SystemServiceSvc_UpdateMenu *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)DeleteGroupMenuUsingParameters:(SystemServiceSvc_DeleteGroupMenu *)aParameters ;
- (void)DeleteGroupMenuAsyncUsingParameters:(SystemServiceSvc_DeleteGroupMenu *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)DeleteMenuUsingParameters:(SystemServiceSvc_DeleteMenu *)aParameters ;
- (void)DeleteMenuAsyncUsingParameters:(SystemServiceSvc_DeleteMenu *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetAllViewMenusUsingParameters:(SystemServiceSvc_GetAllViewMenus *)aParameters ;
- (void)GetAllViewMenusAsyncUsingParameters:(SystemServiceSvc_GetAllViewMenus *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetViewMenuByApplicationCodeUsingParameters:(SystemServiceSvc_GetViewMenuByApplicationCode *)aParameters ;
- (void)GetViewMenuByApplicationCodeAsyncUsingParameters:(SystemServiceSvc_GetViewMenuByApplicationCode *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetGroupMenusByAplicationCodeUsingParameters:(SystemServiceSvc_GetGroupMenusByAplicationCode *)aParameters ;
- (void)GetGroupMenusByAplicationCodeAsyncUsingParameters:(SystemServiceSvc_GetGroupMenusByAplicationCode *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetGroupMenusByAplicationIdUsingParameters:(SystemServiceSvc_GetGroupMenusByAplicationId *)aParameters ;
- (void)GetGroupMenusByAplicationIdAsyncUsingParameters:(SystemServiceSvc_GetGroupMenusByAplicationId *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetGroupMenusByIdUsingParameters:(SystemServiceSvc_GetGroupMenusById *)aParameters ;
- (void)GetGroupMenusByIdAsyncUsingParameters:(SystemServiceSvc_GetGroupMenusById *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetMenusByIdUsingParameters:(SystemServiceSvc_GetMenusById *)aParameters ;
- (void)GetMenusByIdAsyncUsingParameters:(SystemServiceSvc_GetMenusById *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetSysApplicationByCompanyIdUsingParameters:(SystemServiceSvc_GetSysApplicationByCompanyId *)aParameters ;
- (void)GetSysApplicationByCompanyIdAsyncUsingParameters:(SystemServiceSvc_GetSysApplicationByCompanyId *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetSysApplicationByProductCodeAndCompanyIdUsingParameters:(SystemServiceSvc_GetSysApplicationByProductCodeAndCompanyId *)aParameters ;
- (void)GetSysApplicationByProductCodeAndCompanyIdAsyncUsingParameters:(SystemServiceSvc_GetSysApplicationByProductCodeAndCompanyId *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetMenuByUserPermissonUsingParameters:(SystemServiceSvc_GetMenuByUserPermisson *)aParameters ;
- (void)GetMenuByUserPermissonAsyncUsingParameters:(SystemServiceSvc_GetMenuByUserPermisson *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetAllViewGroupMenusUsingParameters:(SystemServiceSvc_GetAllViewGroupMenus *)aParameters ;
- (void)GetAllViewGroupMenusAsyncUsingParameters:(SystemServiceSvc_GetAllViewGroupMenus *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetAllSysApplicationsUsingParameters:(SystemServiceSvc_GetAllSysApplications *)aParameters ;
- (void)GetAllSysApplicationsAsyncUsingParameters:(SystemServiceSvc_GetAllSysApplications *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetSysApplicationBySysApplicationIdUsingParameters:(SystemServiceSvc_GetSysApplicationBySysApplicationId *)aParameters ;
- (void)GetSysApplicationBySysApplicationIdAsyncUsingParameters:(SystemServiceSvc_GetSysApplicationBySysApplicationId *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)CreateSysModuleUsingParameters:(SystemServiceSvc_CreateSysModule *)aParameters ;
- (void)CreateSysModuleAsyncUsingParameters:(SystemServiceSvc_CreateSysModule *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetSysModuleBySysGroupMenuIdUsingParameters:(SystemServiceSvc_GetSysModuleBySysGroupMenuId *)aParameters ;
- (void)GetSysModuleBySysGroupMenuIdAsyncUsingParameters:(SystemServiceSvc_GetSysModuleBySysGroupMenuId *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)UpdateSysModuleUsingParameters:(SystemServiceSvc_UpdateSysModule *)aParameters ;
- (void)UpdateSysModuleAsyncUsingParameters:(SystemServiceSvc_UpdateSysModule *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)CreateFeatureUsingParameters:(SystemServiceSvc_CreateFeature *)aParameters ;
- (void)CreateFeatureAsyncUsingParameters:(SystemServiceSvc_CreateFeature *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)CreateActionUsingParameters:(SystemServiceSvc_CreateAction *)aParameters ;
- (void)CreateActionAsyncUsingParameters:(SystemServiceSvc_CreateAction *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)CreateLinkUsingParameters:(SystemServiceSvc_CreateLink *)aParameters ;
- (void)CreateLinkAsyncUsingParameters:(SystemServiceSvc_CreateLink *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetSysFeatureBySysMenuIdUsingParameters:(SystemServiceSvc_GetSysFeatureBySysMenuId *)aParameters ;
- (void)GetSysFeatureBySysMenuIdAsyncUsingParameters:(SystemServiceSvc_GetSysFeatureBySysMenuId *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetSysActionBySysFeatureIdUsingParameters:(SystemServiceSvc_GetSysActionBySysFeatureId *)aParameters ;
- (void)GetSysActionBySysFeatureIdAsyncUsingParameters:(SystemServiceSvc_GetSysActionBySysFeatureId *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetSysLinkBySysActionIdUsingParameters:(SystemServiceSvc_GetSysLinkBySysActionId *)aParameters ;
- (void)GetSysLinkBySysActionIdAsyncUsingParameters:(SystemServiceSvc_GetSysLinkBySysActionId *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)UpdateSysFeatureUsingParameters:(SystemServiceSvc_UpdateSysFeature *)aParameters ;
- (void)UpdateSysFeatureAsyncUsingParameters:(SystemServiceSvc_UpdateSysFeature *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)UpdateSysLinkUsingParameters:(SystemServiceSvc_UpdateSysLink *)aParameters ;
- (void)UpdateSysLinkAsyncUsingParameters:(SystemServiceSvc_UpdateSysLink *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetModuleByGroupMenuUsingParameters:(SystemServiceSvc_GetModuleByGroupMenu *)aParameters ;
- (void)GetModuleByGroupMenuAsyncUsingParameters:(SystemServiceSvc_GetModuleByGroupMenu *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetSysParamListUsingParameters:(SystemServiceSvc_GetSysParamList *)aParameters ;
- (void)GetSysParamListAsyncUsingParameters:(SystemServiceSvc_GetSysParamList *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetSysParamByIdUsingParameters:(SystemServiceSvc_GetSysParamById *)aParameters ;
- (void)GetSysParamByIdAsyncUsingParameters:(SystemServiceSvc_GetSysParamById *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetSysParamByKeyCodeTypeUsingParameters:(SystemServiceSvc_GetSysParamByKeyCodeType *)aParameters ;
- (void)GetSysParamByKeyCodeTypeAsyncUsingParameters:(SystemServiceSvc_GetSysParamByKeyCodeType *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetSysParamListByKeyUsingParameters:(SystemServiceSvc_GetSysParamListByKey *)aParameters ;
- (void)GetSysParamListByKeyAsyncUsingParameters:(SystemServiceSvc_GetSysParamListByKey *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetSysParamListByKeyCodeUsingParameters:(SystemServiceSvc_GetSysParamListByKeyCode *)aParameters ;
- (void)GetSysParamListByKeyCodeAsyncUsingParameters:(SystemServiceSvc_GetSysParamListByKeyCode *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetSysParamListByKCTUsingParameters:(SystemServiceSvc_GetSysParamListByKCT *)aParameters ;
- (void)GetSysParamListByKCTAsyncUsingParameters:(SystemServiceSvc_GetSysParamListByKCT *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)CreateSysParamUsingParameters:(SystemServiceSvc_CreateSysParam *)aParameters ;
- (void)CreateSysParamAsyncUsingParameters:(SystemServiceSvc_CreateSysParam *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)UpdateSysParamUsingParameters:(SystemServiceSvc_UpdateSysParam *)aParameters ;
- (void)UpdateSysParamAsyncUsingParameters:(SystemServiceSvc_UpdateSysParam *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)DeleteSysParamUsingParameters:(SystemServiceSvc_DeleteSysParam *)aParameters ;
- (void)DeleteSysParamAsyncUsingParameters:(SystemServiceSvc_DeleteSysParam *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetAllTemplateUsingParameters:(SystemServiceSvc_GetAllTemplate *)aParameters ;
- (void)GetAllTemplateAsyncUsingParameters:(SystemServiceSvc_GetAllTemplate *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)CreateTemplateUsingParameters:(SystemServiceSvc_CreateTemplate *)aParameters ;
- (void)CreateTemplateAsyncUsingParameters:(SystemServiceSvc_CreateTemplate *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)UpdateTemplateUsingParameters:(SystemServiceSvc_UpdateTemplate *)aParameters ;
- (void)UpdateTemplateAsyncUsingParameters:(SystemServiceSvc_UpdateTemplate *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)DeleteTemplateUsingParameters:(SystemServiceSvc_DeleteTemplate *)aParameters ;
- (void)DeleteTemplateAsyncUsingParameters:(SystemServiceSvc_DeleteTemplate *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetAllSysProfilesUsingParameters:(SystemServiceSvc_GetAllSysProfiles *)aParameters ;
- (void)GetAllSysProfilesAsyncUsingParameters:(SystemServiceSvc_GetAllSysProfiles *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetAllSysSubProfilesByProfileCodeUsingParameters:(SystemServiceSvc_GetAllSysSubProfilesByProfileCode *)aParameters ;
- (void)GetAllSysSubProfilesByProfileCodeAsyncUsingParameters:(SystemServiceSvc_GetAllSysSubProfilesByProfileCode *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetAllSysProfileLayerUsingParameters:(SystemServiceSvc_GetAllSysProfileLayer *)aParameters ;
- (void)GetAllSysProfileLayerAsyncUsingParameters:(SystemServiceSvc_GetAllSysProfileLayer *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetAll_TB_ESS_SYSTEM_LANGUAGEUsingParameters:(SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE *)aParameters ;
- (void)GetAll_TB_ESS_SYSTEM_LANGUAGEAsyncUsingParameters:(SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetAll_TB_ESS_SYSTEM_LANGUAGE_BySkipTakeUsingParameters:(SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_BySkipTake *)aParameters ;
- (void)GetAll_TB_ESS_SYSTEM_LANGUAGE_BySkipTakeAsyncUsingParameters:(SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_BySkipTake *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLangSkipTakeUsingParameters:(SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLangSkipTake *)aParameters ;
- (void)GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLangSkipTakeAsyncUsingParameters:(SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLangSkipTake *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLangUsingParameters:(SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLang *)aParameters ;
- (void)GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLangAsyncUsingParameters:(SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLang *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)Get_TB_ESS_SYSTEM_LANGUAGE_ByContentCodeUsingParameters:(SystemServiceSvc_Get_TB_ESS_SYSTEM_LANGUAGE_ByContentCode *)aParameters ;
- (void)Get_TB_ESS_SYSTEM_LANGUAGE_ByContentCodeAsyncUsingParameters:(SystemServiceSvc_Get_TB_ESS_SYSTEM_LANGUAGE_ByContentCode *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GetAll_TB_ESS_SYSTEM_LANGUAGE_ByConditionsUsingParameters:(SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByConditions *)aParameters ;
- (void)GetAll_TB_ESS_SYSTEM_LANGUAGE_ByConditionsAsyncUsingParameters:(SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByConditions *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)CreateSystemLanguageUsingParameters:(SystemServiceSvc_CreateSystemLanguage *)aParameters ;
- (void)CreateSystemLanguageAsyncUsingParameters:(SystemServiceSvc_CreateSystemLanguage *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)DeleteSystemLanguageByIdUsingParameters:(SystemServiceSvc_DeleteSystemLanguageById *)aParameters ;
- (void)DeleteSystemLanguageByIdAsyncUsingParameters:(SystemServiceSvc_DeleteSystemLanguageById *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)DeleteSystemLanguageByContentCodeUsingParameters:(SystemServiceSvc_DeleteSystemLanguageByContentCode *)aParameters ;
- (void)DeleteSystemLanguageByContentCodeAsyncUsingParameters:(SystemServiceSvc_DeleteSystemLanguageByContentCode *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)EditSystemLanguageUsingParameters:(SystemServiceSvc_EditSystemLanguage *)aParameters ;
- (void)EditSystemLanguageAsyncUsingParameters:(SystemServiceSvc_EditSystemLanguage *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISystemServiceBindingResponse *)GeLanguageContentByLangCodeAndContentCodeUsingParameters:(SystemServiceSvc_GeLanguageContentByLangCodeAndContentCode *)aParameters ;
- (void)GeLanguageContentByLangCodeAndContentCodeAsyncUsingParameters:(SystemServiceSvc_GeLanguageContentByLangCodeAndContentCode *)aParameters  delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)responseDelegate;
@end
@interface BasicHttpBinding_ISystemServiceBindingOperation : NSOperation {
	BasicHttpBinding_ISystemServiceBinding *binding;
	BasicHttpBinding_ISystemServiceBindingResponse *response;
	id<BasicHttpBinding_ISystemServiceBindingResponseDelegate> delegate;
	NSMutableData *responseData;
	NSURLConnection *urlConnection;
}
@property (retain) BasicHttpBinding_ISystemServiceBinding *binding;
@property (readonly) BasicHttpBinding_ISystemServiceBindingResponse *response;
@property (nonatomic, assign) id<BasicHttpBinding_ISystemServiceBindingResponseDelegate> delegate;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, retain) NSURLConnection *urlConnection;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetMenuBySysApplicationCode : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetMenuBySysApplicationCode * parameters;
}
@property (retain) SystemServiceSvc_GetMenuBySysApplicationCode * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetMenuBySysApplicationCode *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetAllGroupMenus : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetAllGroupMenus * parameters;
}
@property (retain) SystemServiceSvc_GetAllGroupMenus * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetAllGroupMenus *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetAllMenus : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetAllMenus * parameters;
}
@property (retain) SystemServiceSvc_GetAllMenus * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetAllMenus *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_CreateGroupMenu : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_CreateGroupMenu * parameters;
}
@property (retain) SystemServiceSvc_CreateGroupMenu * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_CreateGroupMenu *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_CreateMenu : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_CreateMenu * parameters;
}
@property (retain) SystemServiceSvc_CreateMenu * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_CreateMenu *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_UpdateGroupMenu : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_UpdateGroupMenu * parameters;
}
@property (retain) SystemServiceSvc_UpdateGroupMenu * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_UpdateGroupMenu *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_UpdateMenu : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_UpdateMenu * parameters;
}
@property (retain) SystemServiceSvc_UpdateMenu * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_UpdateMenu *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_DeleteGroupMenu : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_DeleteGroupMenu * parameters;
}
@property (retain) SystemServiceSvc_DeleteGroupMenu * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_DeleteGroupMenu *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_DeleteMenu : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_DeleteMenu * parameters;
}
@property (retain) SystemServiceSvc_DeleteMenu * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_DeleteMenu *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetAllViewMenus : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetAllViewMenus * parameters;
}
@property (retain) SystemServiceSvc_GetAllViewMenus * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetAllViewMenus *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetViewMenuByApplicationCode : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetViewMenuByApplicationCode * parameters;
}
@property (retain) SystemServiceSvc_GetViewMenuByApplicationCode * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetViewMenuByApplicationCode *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetGroupMenusByAplicationCode : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetGroupMenusByAplicationCode * parameters;
}
@property (retain) SystemServiceSvc_GetGroupMenusByAplicationCode * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetGroupMenusByAplicationCode *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetGroupMenusByAplicationId : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetGroupMenusByAplicationId * parameters;
}
@property (retain) SystemServiceSvc_GetGroupMenusByAplicationId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetGroupMenusByAplicationId *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetGroupMenusById : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetGroupMenusById * parameters;
}
@property (retain) SystemServiceSvc_GetGroupMenusById * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetGroupMenusById *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetMenusById : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetMenusById * parameters;
}
@property (retain) SystemServiceSvc_GetMenusById * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetMenusById *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetSysApplicationByCompanyId : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetSysApplicationByCompanyId * parameters;
}
@property (retain) SystemServiceSvc_GetSysApplicationByCompanyId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetSysApplicationByCompanyId *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetSysApplicationByProductCodeAndCompanyId : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetSysApplicationByProductCodeAndCompanyId * parameters;
}
@property (retain) SystemServiceSvc_GetSysApplicationByProductCodeAndCompanyId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetSysApplicationByProductCodeAndCompanyId *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetMenuByUserPermisson : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetMenuByUserPermisson * parameters;
}
@property (retain) SystemServiceSvc_GetMenuByUserPermisson * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetMenuByUserPermisson *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetAllViewGroupMenus : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetAllViewGroupMenus * parameters;
}
@property (retain) SystemServiceSvc_GetAllViewGroupMenus * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetAllViewGroupMenus *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetAllSysApplications : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetAllSysApplications * parameters;
}
@property (retain) SystemServiceSvc_GetAllSysApplications * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetAllSysApplications *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetSysApplicationBySysApplicationId : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetSysApplicationBySysApplicationId * parameters;
}
@property (retain) SystemServiceSvc_GetSysApplicationBySysApplicationId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetSysApplicationBySysApplicationId *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_CreateSysModule : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_CreateSysModule * parameters;
}
@property (retain) SystemServiceSvc_CreateSysModule * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_CreateSysModule *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetSysModuleBySysGroupMenuId : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetSysModuleBySysGroupMenuId * parameters;
}
@property (retain) SystemServiceSvc_GetSysModuleBySysGroupMenuId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetSysModuleBySysGroupMenuId *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_UpdateSysModule : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_UpdateSysModule * parameters;
}
@property (retain) SystemServiceSvc_UpdateSysModule * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_UpdateSysModule *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_CreateFeature : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_CreateFeature * parameters;
}
@property (retain) SystemServiceSvc_CreateFeature * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_CreateFeature *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_CreateAction : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_CreateAction * parameters;
}
@property (retain) SystemServiceSvc_CreateAction * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_CreateAction *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_CreateLink : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_CreateLink * parameters;
}
@property (retain) SystemServiceSvc_CreateLink * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_CreateLink *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetSysFeatureBySysMenuId : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetSysFeatureBySysMenuId * parameters;
}
@property (retain) SystemServiceSvc_GetSysFeatureBySysMenuId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetSysFeatureBySysMenuId *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetSysActionBySysFeatureId : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetSysActionBySysFeatureId * parameters;
}
@property (retain) SystemServiceSvc_GetSysActionBySysFeatureId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetSysActionBySysFeatureId *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetSysLinkBySysActionId : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetSysLinkBySysActionId * parameters;
}
@property (retain) SystemServiceSvc_GetSysLinkBySysActionId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetSysLinkBySysActionId *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_UpdateSysFeature : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_UpdateSysFeature * parameters;
}
@property (retain) SystemServiceSvc_UpdateSysFeature * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_UpdateSysFeature *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_UpdateSysLink : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_UpdateSysLink * parameters;
}
@property (retain) SystemServiceSvc_UpdateSysLink * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_UpdateSysLink *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetModuleByGroupMenu : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetModuleByGroupMenu * parameters;
}
@property (retain) SystemServiceSvc_GetModuleByGroupMenu * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetModuleByGroupMenu *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetSysParamList : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetSysParamList * parameters;
}
@property (retain) SystemServiceSvc_GetSysParamList * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetSysParamList *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetSysParamById : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetSysParamById * parameters;
}
@property (retain) SystemServiceSvc_GetSysParamById * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetSysParamById *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetSysParamByKeyCodeType : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetSysParamByKeyCodeType * parameters;
}
@property (retain) SystemServiceSvc_GetSysParamByKeyCodeType * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetSysParamByKeyCodeType *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetSysParamListByKey : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetSysParamListByKey * parameters;
}
@property (retain) SystemServiceSvc_GetSysParamListByKey * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetSysParamListByKey *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetSysParamListByKeyCode : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetSysParamListByKeyCode * parameters;
}
@property (retain) SystemServiceSvc_GetSysParamListByKeyCode * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetSysParamListByKeyCode *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetSysParamListByKCT : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetSysParamListByKCT * parameters;
}
@property (retain) SystemServiceSvc_GetSysParamListByKCT * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetSysParamListByKCT *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_CreateSysParam : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_CreateSysParam * parameters;
}
@property (retain) SystemServiceSvc_CreateSysParam * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_CreateSysParam *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_UpdateSysParam : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_UpdateSysParam * parameters;
}
@property (retain) SystemServiceSvc_UpdateSysParam * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_UpdateSysParam *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_DeleteSysParam : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_DeleteSysParam * parameters;
}
@property (retain) SystemServiceSvc_DeleteSysParam * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_DeleteSysParam *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetAllTemplate : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetAllTemplate * parameters;
}
@property (retain) SystemServiceSvc_GetAllTemplate * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetAllTemplate *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_CreateTemplate : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_CreateTemplate * parameters;
}
@property (retain) SystemServiceSvc_CreateTemplate * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_CreateTemplate *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_UpdateTemplate : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_UpdateTemplate * parameters;
}
@property (retain) SystemServiceSvc_UpdateTemplate * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_UpdateTemplate *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_DeleteTemplate : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_DeleteTemplate * parameters;
}
@property (retain) SystemServiceSvc_DeleteTemplate * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_DeleteTemplate *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetAllSysProfiles : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetAllSysProfiles * parameters;
}
@property (retain) SystemServiceSvc_GetAllSysProfiles * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetAllSysProfiles *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetAllSysSubProfilesByProfileCode : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetAllSysSubProfilesByProfileCode * parameters;
}
@property (retain) SystemServiceSvc_GetAllSysSubProfilesByProfileCode * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetAllSysSubProfilesByProfileCode *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetAllSysProfileLayer : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetAllSysProfileLayer * parameters;
}
@property (retain) SystemServiceSvc_GetAllSysProfileLayer * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetAllSysProfileLayer *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetAll_TB_ESS_SYSTEM_LANGUAGE : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE * parameters;
}
@property (retain) SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetAll_TB_ESS_SYSTEM_LANGUAGE_BySkipTake : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_BySkipTake * parameters;
}
@property (retain) SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_BySkipTake * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_BySkipTake *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLangSkipTake : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLangSkipTake * parameters;
}
@property (retain) SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLangSkipTake * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLangSkipTake *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLang : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLang * parameters;
}
@property (retain) SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLang * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByLang *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_Get_TB_ESS_SYSTEM_LANGUAGE_ByContentCode : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_Get_TB_ESS_SYSTEM_LANGUAGE_ByContentCode * parameters;
}
@property (retain) SystemServiceSvc_Get_TB_ESS_SYSTEM_LANGUAGE_ByContentCode * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_Get_TB_ESS_SYSTEM_LANGUAGE_ByContentCode *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByConditions : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByConditions * parameters;
}
@property (retain) SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByConditions * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GetAll_TB_ESS_SYSTEM_LANGUAGE_ByConditions *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_CreateSystemLanguage : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_CreateSystemLanguage * parameters;
}
@property (retain) SystemServiceSvc_CreateSystemLanguage * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_CreateSystemLanguage *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_DeleteSystemLanguageById : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_DeleteSystemLanguageById * parameters;
}
@property (retain) SystemServiceSvc_DeleteSystemLanguageById * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_DeleteSystemLanguageById *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_DeleteSystemLanguageByContentCode : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_DeleteSystemLanguageByContentCode * parameters;
}
@property (retain) SystemServiceSvc_DeleteSystemLanguageByContentCode * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_DeleteSystemLanguageByContentCode *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_EditSystemLanguage : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_EditSystemLanguage * parameters;
}
@property (retain) SystemServiceSvc_EditSystemLanguage * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_EditSystemLanguage *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_GeLanguageContentByLangCodeAndContentCode : BasicHttpBinding_ISystemServiceBindingOperation {
	SystemServiceSvc_GeLanguageContentByLangCodeAndContentCode * parameters;
}
@property (retain) SystemServiceSvc_GeLanguageContentByLangCodeAndContentCode * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISystemServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISystemServiceBindingResponseDelegate>)aDelegate
	parameters:(SystemServiceSvc_GeLanguageContentByLangCodeAndContentCode *)aParameters
;
@end
@interface BasicHttpBinding_ISystemServiceBinding_envelope : NSObject {
}
+ (BasicHttpBinding_ISystemServiceBinding_envelope *)sharedInstance;
- (NSString *)serializedFormUsingHeaderElements:(NSDictionary *)headerElements bodyElements:(NSDictionary *)bodyElements;
@end
@interface BasicHttpBinding_ISystemServiceBindingResponse : NSObject {
	NSArray *headers;
	NSArray *bodyParts;
	NSError *error;
}
@property (retain) NSArray *headers;
@property (retain) NSArray *bodyParts;
@property (retain) NSError *error;
@end
