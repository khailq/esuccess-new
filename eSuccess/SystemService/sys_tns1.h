#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class sys_tns1_ArrayOfV_SysMenu;
@class sys_tns1_V_SysMenu;
@class sys_tns1_ArrayOfSysGroupMenu;
@class sys_tns1_SysGroupMenu;
@class sys_tns1_SysApplication;
@class sys_tns1_ArrayOfSysMenu;
@class sys_tns1_SysMenu;
@class sys_tns1_ArrayOfSysGroupMenuPermission;
@class sys_tns1_ArrayOfSysRoleMenuPermission;
@class sys_tns1_ArrayOfSysUserMenuPermission;
@class sys_tns1_SysGroupMenuPermission;
@class sys_tns1_SysGroup;
@class sys_tns1_ArrayOfSysGroupEmpProfilePermission;
@class sys_tns1_SysGroupEmpProfilePermission;
@class sys_tns1_SysEmpProfileLayer;
@class sys_tns1_SysEmpProfileGroupLayer;
@class sys_tns1_ArrayOfSysEmpProfileLayerUrl;
@class sys_tns1_ArrayOfSysRoleEmpProfilePermission;
@class sys_tns1_ArrayOfSysUserEmpProfilePermission;
@class sys_tns1_ArrayOfSysEmpProfileLayer;
@class sys_tns1_SysEmpProfileLayerUrl;
@class sys_tns1_SysRoleEmpProfilePermission;
@class sys_tns1_SysRole;
@class sys_tns1_SysRoleMenuPermission;
@class sys_tns1_SysUserEmpProfilePermission;
@class sys_tns1_SysUser;
@class sys_tns1_SysUserMenuPermission;
@class sys_tns1_ArrayOfSysApplication;
@class sys_tns1_ArrayOfSP_GET_MENU_BY_USER_PERMISSION_Result;
@class sys_tns1_SP_GET_MENU_BY_USER_PERMISSION_Result;
@class sys_tns1_ArrayOfV_SysGroupMenu;
@class sys_tns1_V_SysGroupMenu;
@class sys_tns1_SysModule;
@class sys_tns1_SysFeature;
@class sys_tns1_SysAction;
@class sys_tns1_SysLink;
@class sys_tns1_ArrayOfTB_ESS_SYSTEM_PARAMETER;
@class sys_tns1_TB_ESS_SYSTEM_PARAMETER;
@class sys_tns1_ArrayOfTB_ESS_FILE_TEMPLATE_MANAGEMENT;
@class sys_tns1_TB_ESS_FILE_TEMPLATE_MANAGEMENT;
@class sys_tns1_ArrayOfV_ESS_SYS_PROFILES;
@class sys_tns1_V_ESS_SYS_PROFILES;
@class sys_tns1_ArrayOfV_ESS_SYS_PROFILE_SUB_MAPPING;
@class sys_tns1_V_ESS_SYS_PROFILE_SUB_MAPPING;
@class sys_tns1_ArrayOfV_ESS_SYS_PROFILE_LAYER;
@class sys_tns1_V_ESS_SYS_PROFILE_LAYER;
@class sys_tns1_ArrayOfTB_ESS_SYSTEM_LANGUAGE;
@class sys_tns1_TB_ESS_SYSTEM_LANGUAGE;
@interface sys_tns1_V_SysMenu : NSObject {
	
/* elements */
	NSString * Action;
	NSString * ApplicationCodeAction;
	NSString * Controller;
	USBoolean * IsDefault;
	USBoolean * IsDeleted;
	NSNumber * Priority;
	NSString * SysApplicationCode;
	NSNumber * SysApplicationId;
	NSNumber * SysGroupMenuId;
	NSString * SysGroupMenuName;
	NSNumber * SysMenuId;
	NSString * SysMenuName;
	NSString * Url;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_V_SysMenu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Action;
@property (retain) NSString * ApplicationCodeAction;
@property (retain) NSString * Controller;
@property (retain) USBoolean * IsDefault;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Priority;
@property (retain) NSString * SysApplicationCode;
@property (retain) NSNumber * SysApplicationId;
@property (retain) NSNumber * SysGroupMenuId;
@property (retain) NSString * SysGroupMenuName;
@property (retain) NSNumber * SysMenuId;
@property (retain) NSString * SysMenuName;
@property (retain) NSString * Url;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_ArrayOfV_SysMenu : NSObject {
	
/* elements */
	NSMutableArray *V_SysMenu;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_ArrayOfV_SysMenu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_SysMenu:(sys_tns1_V_SysMenu *)toAdd;
@property (readonly) NSMutableArray * V_SysMenu;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_SysApplication : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * Description;
	NSString * Icon;
	NSString * Name;
	NSString * ProductCode;
	NSString * SysApplicationCode;
	NSNumber * SysApplicationId;
	sys_tns1_ArrayOfSysGroupMenu * SysGroupMenus;
	NSString * Url;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_SysApplication *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Description;
@property (retain) NSString * Icon;
@property (retain) NSString * Name;
@property (retain) NSString * ProductCode;
@property (retain) NSString * SysApplicationCode;
@property (retain) NSNumber * SysApplicationId;
@property (retain) sys_tns1_ArrayOfSysGroupMenu * SysGroupMenus;
@property (retain) NSString * Url;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_SysGroupMenu : NSObject {
	
/* elements */
	NSString * Action;
	NSString * ApplicationCodeAction;
	NSString * Controller;
	NSString * Icon;
	USBoolean * IsDeleted;
	NSNumber * ParentId;
	NSNumber * Priority;
	sys_tns1_SysApplication * SysApplication;
	NSString * SysApplicationCode;
	NSNumber * SysApplicationId;
	NSNumber * SysGroupMenuId;
	NSString * SysGroupMenuName;
	NSString * Url;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_SysGroupMenu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Action;
@property (retain) NSString * ApplicationCodeAction;
@property (retain) NSString * Controller;
@property (retain) NSString * Icon;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ParentId;
@property (retain) NSNumber * Priority;
@property (retain) sys_tns1_SysApplication * SysApplication;
@property (retain) NSString * SysApplicationCode;
@property (retain) NSNumber * SysApplicationId;
@property (retain) NSNumber * SysGroupMenuId;
@property (retain) NSString * SysGroupMenuName;
@property (retain) NSString * Url;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_ArrayOfSysGroupMenu : NSObject {
	
/* elements */
	NSMutableArray *SysGroupMenu;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_ArrayOfSysGroupMenu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysGroupMenu:(sys_tns1_SysGroupMenu *)toAdd;
@property (readonly) NSMutableArray * SysGroupMenu;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_ArrayOfSysEmpProfileLayer : NSObject {
	
/* elements */
	NSMutableArray *SysEmpProfileLayer;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_ArrayOfSysEmpProfileLayer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysEmpProfileLayer:(sys_tns1_SysEmpProfileLayer *)toAdd;
@property (readonly) NSMutableArray * SysEmpProfileLayer;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_SysEmpProfileGroupLayer : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * Icon;
	USBoolean * IsDeleted;
	USBoolean * IsVisible;
	NSNumber * Priority;
	NSString * SysEmpProfileGroupLayerCode;
	NSNumber * SysEmpProfileGroupLayerId;
	NSString * SysEmpProfileGroupLayerName;
	sys_tns1_ArrayOfSysEmpProfileLayer * SysEmpProfileLayers;
	NSString * Url;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_SysEmpProfileGroupLayer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Icon;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsVisible;
@property (retain) NSNumber * Priority;
@property (retain) NSString * SysEmpProfileGroupLayerCode;
@property (retain) NSNumber * SysEmpProfileGroupLayerId;
@property (retain) NSString * SysEmpProfileGroupLayerName;
@property (retain) sys_tns1_ArrayOfSysEmpProfileLayer * SysEmpProfileLayers;
@property (retain) NSString * Url;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_SysEmpProfileLayerUrl : NSObject {
	
/* elements */
	NSString * Action;
	NSNumber * CompanyId;
	NSString * Controller;
	USBoolean * IsDeleted;
	sys_tns1_SysEmpProfileLayer * SysEmpProfileLayer;
	NSNumber * SysEmpProfileLayerId;
	NSNumber * SysEmpProfileLayerUrlId;
	NSString * Url;
	NSString * UrlType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_SysEmpProfileLayerUrl *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Action;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Controller;
@property (retain) USBoolean * IsDeleted;
@property (retain) sys_tns1_SysEmpProfileLayer * SysEmpProfileLayer;
@property (retain) NSNumber * SysEmpProfileLayerId;
@property (retain) NSNumber * SysEmpProfileLayerUrlId;
@property (retain) NSString * Url;
@property (retain) NSString * UrlType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_ArrayOfSysEmpProfileLayerUrl : NSObject {
	
/* elements */
	NSMutableArray *SysEmpProfileLayerUrl;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_ArrayOfSysEmpProfileLayerUrl *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysEmpProfileLayerUrl:(sys_tns1_SysEmpProfileLayerUrl *)toAdd;
@property (readonly) NSMutableArray * SysEmpProfileLayerUrl;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_SysRoleMenuPermission : NSObject {
	
/* elements */
	NSNumber * AssignedBy;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	sys_tns1_SysMenu * SysMenu;
	NSNumber * SysMenuId;
	sys_tns1_SysRole * SysRole;
	NSNumber * SysRoleId;
	NSNumber * SysRoleMenuPermissionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_SysRoleMenuPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AssignedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) sys_tns1_SysMenu * SysMenu;
@property (retain) NSNumber * SysMenuId;
@property (retain) sys_tns1_SysRole * SysRole;
@property (retain) NSNumber * SysRoleId;
@property (retain) NSNumber * SysRoleMenuPermissionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_ArrayOfSysRoleMenuPermission : NSObject {
	
/* elements */
	NSMutableArray *SysRoleMenuPermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_ArrayOfSysRoleMenuPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysRoleMenuPermission:(sys_tns1_SysRoleMenuPermission *)toAdd;
@property (readonly) NSMutableArray * SysRoleMenuPermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_SysRole : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * Description;
	NSString * Name;
	sys_tns1_ArrayOfSysRoleEmpProfilePermission * SysRoleEmpProfilePermissions;
	NSNumber * SysRoleId;
	sys_tns1_ArrayOfSysRoleMenuPermission * SysRoleMenuPermissions;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_SysRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Description;
@property (retain) NSString * Name;
@property (retain) sys_tns1_ArrayOfSysRoleEmpProfilePermission * SysRoleEmpProfilePermissions;
@property (retain) NSNumber * SysRoleId;
@property (retain) sys_tns1_ArrayOfSysRoleMenuPermission * SysRoleMenuPermissions;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_SysRoleEmpProfilePermission : NSObject {
	
/* elements */
	USBoolean * AddPermission;
	USBoolean * AprovePermisson;
	NSNumber * AssignedBy;
	NSDate * CreatedDate;
	USBoolean * DeletePermission;
	USBoolean * EditPermission;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	sys_tns1_SysEmpProfileLayer * SysEmpProfileLayer;
	NSNumber * SysEmpProfileLayerId;
	sys_tns1_SysRole * SysRole;
	NSNumber * SysRoleEmpProfilePermissionId;
	NSNumber * SysRoleId;
	USBoolean * ViewPermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_SysRoleEmpProfilePermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddPermission;
@property (retain) USBoolean * AprovePermisson;
@property (retain) NSNumber * AssignedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * DeletePermission;
@property (retain) USBoolean * EditPermission;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) sys_tns1_SysEmpProfileLayer * SysEmpProfileLayer;
@property (retain) NSNumber * SysEmpProfileLayerId;
@property (retain) sys_tns1_SysRole * SysRole;
@property (retain) NSNumber * SysRoleEmpProfilePermissionId;
@property (retain) NSNumber * SysRoleId;
@property (retain) USBoolean * ViewPermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_ArrayOfSysRoleEmpProfilePermission : NSObject {
	
/* elements */
	NSMutableArray *SysRoleEmpProfilePermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_ArrayOfSysRoleEmpProfilePermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysRoleEmpProfilePermission:(sys_tns1_SysRoleEmpProfilePermission *)toAdd;
@property (readonly) NSMutableArray * SysRoleEmpProfilePermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_SysUserMenuPermission : NSObject {
	
/* elements */
	NSNumber * AssignedBy;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	sys_tns1_SysMenu * SysMenu;
	NSNumber * SysMenuId;
	sys_tns1_SysUser * SysUser;
	NSNumber * SysUserId;
	NSNumber * SysUserMenuPermissionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_SysUserMenuPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AssignedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) sys_tns1_SysMenu * SysMenu;
@property (retain) NSNumber * SysMenuId;
@property (retain) sys_tns1_SysUser * SysUser;
@property (retain) NSNumber * SysUserId;
@property (retain) NSNumber * SysUserMenuPermissionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_ArrayOfSysUserMenuPermission : NSObject {
	
/* elements */
	NSMutableArray *SysUserMenuPermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_ArrayOfSysUserMenuPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysUserMenuPermission:(sys_tns1_SysUserMenuPermission *)toAdd;
@property (readonly) NSMutableArray * SysUserMenuPermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_SysUser : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * CurrentSessionId;
	NSNumber * EmployeeId;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSString * SelfServiceXML;
	sys_tns1_ArrayOfSysUserEmpProfilePermission * SysUserEmpProfilePermissions;
	NSNumber * SysUserId;
	sys_tns1_ArrayOfSysUserMenuPermission * SysUserMenuPermissions;
	NSString * Username;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_SysUser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CurrentSessionId;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * SelfServiceXML;
@property (retain) sys_tns1_ArrayOfSysUserEmpProfilePermission * SysUserEmpProfilePermissions;
@property (retain) NSNumber * SysUserId;
@property (retain) sys_tns1_ArrayOfSysUserMenuPermission * SysUserMenuPermissions;
@property (retain) NSString * Username;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_SysUserEmpProfilePermission : NSObject {
	
/* elements */
	USBoolean * AddPermission;
	USBoolean * AprovePermisson;
	NSNumber * AssignedBy;
	NSDate * CreatedDate;
	USBoolean * DeletePermission;
	USBoolean * EditPermission;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	sys_tns1_SysEmpProfileLayer * SysEmpProfileLayer;
	NSNumber * SysEmpProfileLayerId;
	sys_tns1_SysUser * SysUser;
	NSNumber * SysUserEmpProfilePermissionId;
	NSNumber * SysUserId;
	USBoolean * ViewPermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_SysUserEmpProfilePermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddPermission;
@property (retain) USBoolean * AprovePermisson;
@property (retain) NSNumber * AssignedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * DeletePermission;
@property (retain) USBoolean * EditPermission;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) sys_tns1_SysEmpProfileLayer * SysEmpProfileLayer;
@property (retain) NSNumber * SysEmpProfileLayerId;
@property (retain) sys_tns1_SysUser * SysUser;
@property (retain) NSNumber * SysUserEmpProfilePermissionId;
@property (retain) NSNumber * SysUserId;
@property (retain) USBoolean * ViewPermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_ArrayOfSysUserEmpProfilePermission : NSObject {
	
/* elements */
	NSMutableArray *SysUserEmpProfilePermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_ArrayOfSysUserEmpProfilePermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysUserEmpProfilePermission:(sys_tns1_SysUserEmpProfilePermission *)toAdd;
@property (readonly) NSMutableArray * SysUserEmpProfilePermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_SysEmpProfileLayer : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * Icon;
	USBoolean * IsDeleted;
	USBoolean * IsVisible;
	NSNumber * Priority;
	sys_tns1_SysEmpProfileGroupLayer * SysEmpProfileGroupLayer;
	NSNumber * SysEmpProfileGroupLayerId;
	NSString * SysEmpProfileLayerCode;
	NSNumber * SysEmpProfileLayerId;
	NSString * SysEmpProfileLayerName;
	sys_tns1_ArrayOfSysEmpProfileLayerUrl * SysEmpProfileLayerUrls;
	sys_tns1_ArrayOfSysGroupEmpProfilePermission * SysGroupEmpProfilePermissions;
	sys_tns1_ArrayOfSysRoleEmpProfilePermission * SysRoleEmpProfilePermissions;
	sys_tns1_ArrayOfSysUserEmpProfilePermission * SysUserEmpProfilePermissions;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_SysEmpProfileLayer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Icon;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsVisible;
@property (retain) NSNumber * Priority;
@property (retain) sys_tns1_SysEmpProfileGroupLayer * SysEmpProfileGroupLayer;
@property (retain) NSNumber * SysEmpProfileGroupLayerId;
@property (retain) NSString * SysEmpProfileLayerCode;
@property (retain) NSNumber * SysEmpProfileLayerId;
@property (retain) NSString * SysEmpProfileLayerName;
@property (retain) sys_tns1_ArrayOfSysEmpProfileLayerUrl * SysEmpProfileLayerUrls;
@property (retain) sys_tns1_ArrayOfSysGroupEmpProfilePermission * SysGroupEmpProfilePermissions;
@property (retain) sys_tns1_ArrayOfSysRoleEmpProfilePermission * SysRoleEmpProfilePermissions;
@property (retain) sys_tns1_ArrayOfSysUserEmpProfilePermission * SysUserEmpProfilePermissions;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_SysGroupEmpProfilePermission : NSObject {
	
/* elements */
	USBoolean * AddPermission;
	USBoolean * AprovePermisson;
	NSNumber * AssignedBy;
	NSDate * CreatedDate;
	USBoolean * DeletePermission;
	USBoolean * EditPermission;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	sys_tns1_SysEmpProfileLayer * SysEmpProfileLayer;
	NSNumber * SysEmpProfileLayerId;
	sys_tns1_SysGroup * SysGroup;
	NSNumber * SysGroupEmpProfilePermissionId;
	NSNumber * SysGroupId;
	USBoolean * ViewPermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_SysGroupEmpProfilePermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddPermission;
@property (retain) USBoolean * AprovePermisson;
@property (retain) NSNumber * AssignedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * DeletePermission;
@property (retain) USBoolean * EditPermission;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) sys_tns1_SysEmpProfileLayer * SysEmpProfileLayer;
@property (retain) NSNumber * SysEmpProfileLayerId;
@property (retain) sys_tns1_SysGroup * SysGroup;
@property (retain) NSNumber * SysGroupEmpProfilePermissionId;
@property (retain) NSNumber * SysGroupId;
@property (retain) USBoolean * ViewPermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_ArrayOfSysGroupEmpProfilePermission : NSObject {
	
/* elements */
	NSMutableArray *SysGroupEmpProfilePermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_ArrayOfSysGroupEmpProfilePermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysGroupEmpProfilePermission:(sys_tns1_SysGroupEmpProfilePermission *)toAdd;
@property (readonly) NSMutableArray * SysGroupEmpProfilePermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_SysGroup : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * Description;
	NSString * Name;
	sys_tns1_ArrayOfSysGroupEmpProfilePermission * SysGroupEmpProfilePermissions;
	NSNumber * SysGroupId;
	sys_tns1_ArrayOfSysGroupMenuPermission * SysGroupMenuPermissions;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_SysGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Description;
@property (retain) NSString * Name;
@property (retain) sys_tns1_ArrayOfSysGroupEmpProfilePermission * SysGroupEmpProfilePermissions;
@property (retain) NSNumber * SysGroupId;
@property (retain) sys_tns1_ArrayOfSysGroupMenuPermission * SysGroupMenuPermissions;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_SysGroupMenuPermission : NSObject {
	
/* elements */
	NSNumber * AssignedBy;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	sys_tns1_SysGroup * SysGroup;
	NSNumber * SysGroupId;
	NSNumber * SysGroupMenuPermissionId;
	sys_tns1_SysMenu * SysMenu;
	NSNumber * SysMenuId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_SysGroupMenuPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AssignedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) sys_tns1_SysGroup * SysGroup;
@property (retain) NSNumber * SysGroupId;
@property (retain) NSNumber * SysGroupMenuPermissionId;
@property (retain) sys_tns1_SysMenu * SysMenu;
@property (retain) NSNumber * SysMenuId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_ArrayOfSysGroupMenuPermission : NSObject {
	
/* elements */
	NSMutableArray *SysGroupMenuPermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_ArrayOfSysGroupMenuPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysGroupMenuPermission:(sys_tns1_SysGroupMenuPermission *)toAdd;
@property (readonly) NSMutableArray * SysGroupMenuPermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_SysMenu : NSObject {
	
/* elements */
	NSString * Action;
	NSString * ApplicationCodeAction;
	NSString * Controller;
	USBoolean * IsDefault;
	USBoolean * IsDeleted;
	NSNumber * Priority;
	NSString * ProductCode;
	NSNumber * SysGroupMenuId;
	sys_tns1_ArrayOfSysGroupMenuPermission * SysGroupMenuPermissions;
	NSNumber * SysMenuId;
	NSString * SysMenuName;
	sys_tns1_ArrayOfSysRoleMenuPermission * SysRoleMenuPermissions;
	sys_tns1_ArrayOfSysUserMenuPermission * SysUserMenuPermissions;
	NSString * Url;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_SysMenu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Action;
@property (retain) NSString * ApplicationCodeAction;
@property (retain) NSString * Controller;
@property (retain) USBoolean * IsDefault;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Priority;
@property (retain) NSString * ProductCode;
@property (retain) NSNumber * SysGroupMenuId;
@property (retain) sys_tns1_ArrayOfSysGroupMenuPermission * SysGroupMenuPermissions;
@property (retain) NSNumber * SysMenuId;
@property (retain) NSString * SysMenuName;
@property (retain) sys_tns1_ArrayOfSysRoleMenuPermission * SysRoleMenuPermissions;
@property (retain) sys_tns1_ArrayOfSysUserMenuPermission * SysUserMenuPermissions;
@property (retain) NSString * Url;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_ArrayOfSysMenu : NSObject {
	
/* elements */
	NSMutableArray *SysMenu;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_ArrayOfSysMenu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysMenu:(sys_tns1_SysMenu *)toAdd;
@property (readonly) NSMutableArray * SysMenu;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_ArrayOfSysApplication : NSObject {
	
/* elements */
	NSMutableArray *SysApplication;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_ArrayOfSysApplication *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysApplication:(sys_tns1_SysApplication *)toAdd;
@property (readonly) NSMutableArray * SysApplication;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_SP_GET_MENU_BY_USER_PERMISSION_Result : NSObject {
	
/* elements */
	NSString * GroupAction;
	NSString * GroupApplicationCodeAction;
	NSString * GroupController;
	NSString * GroupIcon;
	USBoolean * IsDefault;
	NSString * MenuAction;
	NSString * MenuApplicationCodeAction;
	NSString * MenuController;
	NSNumber * Priority;
	NSString * ProductCode;
	NSString * SysApplicationCode;
	NSNumber * SysGroupMenuId;
	NSString * SysGroupMenuName;
	NSNumber * SysMenuId;
	NSString * SysMenuName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_SP_GET_MENU_BY_USER_PERMISSION_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * GroupAction;
@property (retain) NSString * GroupApplicationCodeAction;
@property (retain) NSString * GroupController;
@property (retain) NSString * GroupIcon;
@property (retain) USBoolean * IsDefault;
@property (retain) NSString * MenuAction;
@property (retain) NSString * MenuApplicationCodeAction;
@property (retain) NSString * MenuController;
@property (retain) NSNumber * Priority;
@property (retain) NSString * ProductCode;
@property (retain) NSString * SysApplicationCode;
@property (retain) NSNumber * SysGroupMenuId;
@property (retain) NSString * SysGroupMenuName;
@property (retain) NSNumber * SysMenuId;
@property (retain) NSString * SysMenuName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_ArrayOfSP_GET_MENU_BY_USER_PERMISSION_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_GET_MENU_BY_USER_PERMISSION_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_ArrayOfSP_GET_MENU_BY_USER_PERMISSION_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_GET_MENU_BY_USER_PERMISSION_Result:(sys_tns1_SP_GET_MENU_BY_USER_PERMISSION_Result *)toAdd;
@property (readonly) NSMutableArray * SP_GET_MENU_BY_USER_PERMISSION_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_V_SysGroupMenu : NSObject {
	
/* elements */
	NSString * Action;
	NSString * ApplicationCodeAction;
	NSString * Controller;
	NSString * Icon;
	USBoolean * IsDeleted;
	NSNumber * ParentId;
	NSNumber * Priority;
	NSString * SysApplicationCode;
	NSNumber * SysApplicationId;
	NSString * SysApplicationName;
	NSNumber * SysGroupMenuId;
	NSString * SysGroupMenuName;
	NSString * Url;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_V_SysGroupMenu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Action;
@property (retain) NSString * ApplicationCodeAction;
@property (retain) NSString * Controller;
@property (retain) NSString * Icon;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ParentId;
@property (retain) NSNumber * Priority;
@property (retain) NSString * SysApplicationCode;
@property (retain) NSNumber * SysApplicationId;
@property (retain) NSString * SysApplicationName;
@property (retain) NSNumber * SysGroupMenuId;
@property (retain) NSString * SysGroupMenuName;
@property (retain) NSString * Url;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_ArrayOfV_SysGroupMenu : NSObject {
	
/* elements */
	NSMutableArray *V_SysGroupMenu;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_ArrayOfV_SysGroupMenu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_SysGroupMenu:(sys_tns1_V_SysGroupMenu *)toAdd;
@property (readonly) NSMutableArray * V_SysGroupMenu;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_SysModule : NSObject {
	
/* elements */
	NSString * Description;
	NSString * Name;
	NSNumber * SysApplicationId;
	NSNumber * SysGroupMenuId;
	NSString * SysModuleCode;
	NSNumber * SysModuleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_SysModule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Description;
@property (retain) NSString * Name;
@property (retain) NSNumber * SysApplicationId;
@property (retain) NSNumber * SysGroupMenuId;
@property (retain) NSString * SysModuleCode;
@property (retain) NSNumber * SysModuleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_SysFeature : NSObject {
	
/* elements */
	NSString * Description;
	NSString * Name;
	NSString * SysFeatureCode;
	NSNumber * SysFeatureId;
	NSNumber * SysMenuId;
	NSNumber * SysModuleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_SysFeature *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Description;
@property (retain) NSString * Name;
@property (retain) NSString * SysFeatureCode;
@property (retain) NSNumber * SysFeatureId;
@property (retain) NSNumber * SysMenuId;
@property (retain) NSNumber * SysModuleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_SysAction : NSObject {
	
/* elements */
	NSNumber * SysActionId;
	NSNumber * SysActionTypeId;
	NSNumber * SysFeatureId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_SysAction *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * SysActionId;
@property (retain) NSNumber * SysActionTypeId;
@property (retain) NSNumber * SysFeatureId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_SysLink : NSObject {
	
/* elements */
	NSString * Action;
	NSString * Controller;
	NSNumber * SysActionId;
	NSNumber * SysLinkId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_SysLink *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Action;
@property (retain) NSString * Controller;
@property (retain) NSNumber * SysActionId;
@property (retain) NSNumber * SysLinkId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_TB_ESS_SYSTEM_PARAMETER : NSObject {
	
/* elements */
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * Flex1;
	NSNumber * Flex10;
	USBoolean * Flex11;
	USBoolean * Flex12;
	USBoolean * Flex13;
	USBoolean * Flex14;
	NSNumber * Flex15;
	NSNumber * Flex16;
	NSNumber * Flex17;
	NSNumber * Flex18;
	NSString * Flex19;
	NSString * Flex2;
	NSString * Flex20;
	NSString * Flex3;
	NSDate * Flex4;
	NSDate * Flex5;
	NSDate * Flex6;
	NSNumber * Flex7;
	NSNumber * Flex8;
	NSNumber * Flex9;
	NSNumber * IntFlex1;
	NSNumber * IntFlex2;
	NSNumber * IntFlex3;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * SysParamCode;
	NSNumber * SysParamCompanyId;
	NSString * SysParamDesc;
	NSNumber * SysParamId;
	NSString * SysParamKey;
	NSString * SysParamNameEN;
	NSString * SysParamNameVN;
	NSNumber * SysParamOrder;
	NSString * SysParamType;
	NSString * SysParamValue;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_TB_ESS_SYSTEM_PARAMETER *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Flex1;
@property (retain) NSNumber * Flex10;
@property (retain) USBoolean * Flex11;
@property (retain) USBoolean * Flex12;
@property (retain) USBoolean * Flex13;
@property (retain) USBoolean * Flex14;
@property (retain) NSNumber * Flex15;
@property (retain) NSNumber * Flex16;
@property (retain) NSNumber * Flex17;
@property (retain) NSNumber * Flex18;
@property (retain) NSString * Flex19;
@property (retain) NSString * Flex2;
@property (retain) NSString * Flex20;
@property (retain) NSString * Flex3;
@property (retain) NSDate * Flex4;
@property (retain) NSDate * Flex5;
@property (retain) NSDate * Flex6;
@property (retain) NSNumber * Flex7;
@property (retain) NSNumber * Flex8;
@property (retain) NSNumber * Flex9;
@property (retain) NSNumber * IntFlex1;
@property (retain) NSNumber * IntFlex2;
@property (retain) NSNumber * IntFlex3;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * SysParamCode;
@property (retain) NSNumber * SysParamCompanyId;
@property (retain) NSString * SysParamDesc;
@property (retain) NSNumber * SysParamId;
@property (retain) NSString * SysParamKey;
@property (retain) NSString * SysParamNameEN;
@property (retain) NSString * SysParamNameVN;
@property (retain) NSNumber * SysParamOrder;
@property (retain) NSString * SysParamType;
@property (retain) NSString * SysParamValue;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_ArrayOfTB_ESS_SYSTEM_PARAMETER : NSObject {
	
/* elements */
	NSMutableArray *TB_ESS_SYSTEM_PARAMETER;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_ArrayOfTB_ESS_SYSTEM_PARAMETER *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTB_ESS_SYSTEM_PARAMETER:(sys_tns1_TB_ESS_SYSTEM_PARAMETER *)toAdd;
@property (readonly) NSMutableArray * TB_ESS_SYSTEM_PARAMETER;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_TB_ESS_FILE_TEMPLATE_MANAGEMENT : NSObject {
	
/* elements */
	NSNumber * CREATED_BY;
	NSDate * CREATED_DATE;
	NSString * FLEX1;
	NSNumber * FLEX10;
	NSNumber * FLEX11;
	NSNumber * FLEX12;
	USBoolean * FLEX13;
	USBoolean * FLEX14;
	USBoolean * FLEX15;
	USBoolean * FLEX16;
	NSString * FLEX2;
	NSString * FLEX3;
	NSString * FLEX4;
	NSDate * FLEX5;
	NSDate * FLEX6;
	NSDate * FLEX7;
	NSDate * FLEX8;
	NSNumber * FLEX9;
	NSString * FTM_CD;
	NSString * FTM_FOLDER_CD;
	NSNumber * FTM_ID;
	NSString * FTM_NM;
	NSString * FTM_TYPE;
	USBoolean * IS_DELETE;
	NSNumber * UPDATED_BY;
	NSDate * UPDATED_DATE;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_TB_ESS_FILE_TEMPLATE_MANAGEMENT *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CREATED_BY;
@property (retain) NSDate * CREATED_DATE;
@property (retain) NSString * FLEX1;
@property (retain) NSNumber * FLEX10;
@property (retain) NSNumber * FLEX11;
@property (retain) NSNumber * FLEX12;
@property (retain) USBoolean * FLEX13;
@property (retain) USBoolean * FLEX14;
@property (retain) USBoolean * FLEX15;
@property (retain) USBoolean * FLEX16;
@property (retain) NSString * FLEX2;
@property (retain) NSString * FLEX3;
@property (retain) NSString * FLEX4;
@property (retain) NSDate * FLEX5;
@property (retain) NSDate * FLEX6;
@property (retain) NSDate * FLEX7;
@property (retain) NSDate * FLEX8;
@property (retain) NSNumber * FLEX9;
@property (retain) NSString * FTM_CD;
@property (retain) NSString * FTM_FOLDER_CD;
@property (retain) NSNumber * FTM_ID;
@property (retain) NSString * FTM_NM;
@property (retain) NSString * FTM_TYPE;
@property (retain) USBoolean * IS_DELETE;
@property (retain) NSNumber * UPDATED_BY;
@property (retain) NSDate * UPDATED_DATE;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_ArrayOfTB_ESS_FILE_TEMPLATE_MANAGEMENT : NSObject {
	
/* elements */
	NSMutableArray *TB_ESS_FILE_TEMPLATE_MANAGEMENT;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_ArrayOfTB_ESS_FILE_TEMPLATE_MANAGEMENT *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTB_ESS_FILE_TEMPLATE_MANAGEMENT:(sys_tns1_TB_ESS_FILE_TEMPLATE_MANAGEMENT *)toAdd;
@property (readonly) NSMutableArray * TB_ESS_FILE_TEMPLATE_MANAGEMENT;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_V_ESS_SYS_PROFILES : NSObject {
	
/* elements */
	NSString * ACTION;
	NSString * CONTROLLER;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * PARAMETERS;
	NSString * PROFILE_NAME_EN;
	NSString * PROFILE_NAME_VN;
	NSNumber * PROFILE_PRIORITY;
	NSString * SYS_PROFILE_CODE;
	NSNumber * SYS_PROFILE_ID;
	NSNumber * SysParamCompanyId;
	NSString * SysParamDesc;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_V_ESS_SYS_PROFILES *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ACTION;
@property (retain) NSString * CONTROLLER;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * PARAMETERS;
@property (retain) NSString * PROFILE_NAME_EN;
@property (retain) NSString * PROFILE_NAME_VN;
@property (retain) NSNumber * PROFILE_PRIORITY;
@property (retain) NSString * SYS_PROFILE_CODE;
@property (retain) NSNumber * SYS_PROFILE_ID;
@property (retain) NSNumber * SysParamCompanyId;
@property (retain) NSString * SysParamDesc;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_ArrayOfV_ESS_SYS_PROFILES : NSObject {
	
/* elements */
	NSMutableArray *V_ESS_SYS_PROFILES;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_ArrayOfV_ESS_SYS_PROFILES *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ESS_SYS_PROFILES:(sys_tns1_V_ESS_SYS_PROFILES *)toAdd;
@property (readonly) NSMutableArray * V_ESS_SYS_PROFILES;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_V_ESS_SYS_PROFILE_SUB_MAPPING : NSObject {
	
/* elements */
	NSString * ACTION;
	NSString * CONTROLLER;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * PARAMETERS;
	NSNumber * PRIORITY;
	NSString * SUB_PROFILE_NAME_EN;
	NSString * SUB_PROFILE_NAME_VN;
	NSNumber * SUB_PROFILE_PRIORITY;
	NSString * SYS_PROFILE_CODE;
	NSNumber * SYS_PROFILE_DETAIL_ID;
	NSString * SYS_SUB_PROFILE_CODE;
	NSNumber * SYS_SUB_PROFILE_ID;
	NSNumber * SysParamCompanyId;
	NSString * SysParamDesc;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_V_ESS_SYS_PROFILE_SUB_MAPPING *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ACTION;
@property (retain) NSString * CONTROLLER;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * PARAMETERS;
@property (retain) NSNumber * PRIORITY;
@property (retain) NSString * SUB_PROFILE_NAME_EN;
@property (retain) NSString * SUB_PROFILE_NAME_VN;
@property (retain) NSNumber * SUB_PROFILE_PRIORITY;
@property (retain) NSString * SYS_PROFILE_CODE;
@property (retain) NSNumber * SYS_PROFILE_DETAIL_ID;
@property (retain) NSString * SYS_SUB_PROFILE_CODE;
@property (retain) NSNumber * SYS_SUB_PROFILE_ID;
@property (retain) NSNumber * SysParamCompanyId;
@property (retain) NSString * SysParamDesc;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_ArrayOfV_ESS_SYS_PROFILE_SUB_MAPPING : NSObject {
	
/* elements */
	NSMutableArray *V_ESS_SYS_PROFILE_SUB_MAPPING;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_ArrayOfV_ESS_SYS_PROFILE_SUB_MAPPING *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ESS_SYS_PROFILE_SUB_MAPPING:(sys_tns1_V_ESS_SYS_PROFILE_SUB_MAPPING *)toAdd;
@property (readonly) NSMutableArray * V_ESS_SYS_PROFILE_SUB_MAPPING;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_V_ESS_SYS_PROFILE_LAYER : NSObject {
	
/* elements */
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * PROFILE_LAYER_PRIORITY;
	NSString * SYS_PROFILE_LAYER_CODE;
	NSNumber * SYS_PROFILE_LAYER_ID;
	NSString * SYS_PROFILE_LAYER_NAME_EN;
	NSString * SYS_PROFILE_LAYER_NAME_VN;
	NSNumber * SysParamCompanyId;
	NSString * SysParamDesc;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_V_ESS_SYS_PROFILE_LAYER *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * PROFILE_LAYER_PRIORITY;
@property (retain) NSString * SYS_PROFILE_LAYER_CODE;
@property (retain) NSNumber * SYS_PROFILE_LAYER_ID;
@property (retain) NSString * SYS_PROFILE_LAYER_NAME_EN;
@property (retain) NSString * SYS_PROFILE_LAYER_NAME_VN;
@property (retain) NSNumber * SysParamCompanyId;
@property (retain) NSString * SysParamDesc;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_ArrayOfV_ESS_SYS_PROFILE_LAYER : NSObject {
	
/* elements */
	NSMutableArray *V_ESS_SYS_PROFILE_LAYER;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_ArrayOfV_ESS_SYS_PROFILE_LAYER *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ESS_SYS_PROFILE_LAYER:(sys_tns1_V_ESS_SYS_PROFILE_LAYER *)toAdd;
@property (readonly) NSMutableArray * V_ESS_SYS_PROFILE_LAYER;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_TB_ESS_SYSTEM_LANGUAGE : NSObject {
	
/* elements */
	NSNumber * CREATED_BY;
	NSDate * CREATED_DATE;
	USBoolean * IS_DELETED;
	NSNumber * MODIFIED_BY;
	NSDate * MODIFIED_DATE;
	NSString * SYS_LANG_CODE;
	NSString * SYS_LANG_COMPANY_CODE;
	NSNumber * SYS_LANG_COMPANY_ID;
	NSString * SYS_LANG_CONTENT;
	NSString * SYS_LANG_CONTENT_CODE;
	NSString * SYS_LANG_DESC;
	NSString * SYS_LANG_FORM_CODE;
	NSNumber * SYS_LANG_ID;
	NSString * SYS_LANG_MODULE_CODE;
	NSString * SYS_LANG_PRODUCT_CODE;
	NSString * SYS_LANG_TYPE;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_TB_ESS_SYSTEM_LANGUAGE *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CREATED_BY;
@property (retain) NSDate * CREATED_DATE;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSNumber * MODIFIED_BY;
@property (retain) NSDate * MODIFIED_DATE;
@property (retain) NSString * SYS_LANG_CODE;
@property (retain) NSString * SYS_LANG_COMPANY_CODE;
@property (retain) NSNumber * SYS_LANG_COMPANY_ID;
@property (retain) NSString * SYS_LANG_CONTENT;
@property (retain) NSString * SYS_LANG_CONTENT_CODE;
@property (retain) NSString * SYS_LANG_DESC;
@property (retain) NSString * SYS_LANG_FORM_CODE;
@property (retain) NSNumber * SYS_LANG_ID;
@property (retain) NSString * SYS_LANG_MODULE_CODE;
@property (retain) NSString * SYS_LANG_PRODUCT_CODE;
@property (retain) NSString * SYS_LANG_TYPE;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface sys_tns1_ArrayOfTB_ESS_SYSTEM_LANGUAGE : NSObject {
	
/* elements */
	NSMutableArray *TB_ESS_SYSTEM_LANGUAGE;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (sys_tns1_ArrayOfTB_ESS_SYSTEM_LANGUAGE *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTB_ESS_SYSTEM_LANGUAGE:(sys_tns1_TB_ESS_SYSTEM_LANGUAGE *)toAdd;
@property (readonly) NSMutableArray * TB_ESS_SYSTEM_LANGUAGE;
/* attributes */
- (NSDictionary *)attributes;
@end
