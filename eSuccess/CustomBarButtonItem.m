//
//  CustomBarButtonItem.m
//  eSuccess
//
//  Created by Scott on 4/30/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "CustomBarButtonItem.h"

#define ButtonWidth 42
#define ButtonHeight 42
#define Width 70
#define Height 70

@implementation CustomBarButtonItem

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithImageName:(NSString*)imageName title:(NSString*)labelContent
{
    self = [super init];
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
            
        self.frame = CGRectMake(0, 0, Width, Height);
        self.button = [UIButton buttonWithType:UIButtonTypeCustom];
        float btnX = (Width - ButtonWidth) / 2.0;
        self.button.frame = CGRectMake(btnX, 0, ButtonWidth, ButtonHeight);
        [self.button setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        [self addSubview:self.button];
        
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, ButtonHeight, Width, 26)];
        [self.label setTextColor:[UIColor whiteColor]];
        [self.label setTextAlignment:NSTextAlignmentCenter];
        [self.label setFont:[UIFont systemFontOfSize:11.5]];
        self.label.text = labelContent;
        self.label.backgroundColor = [UIColor clearColor];
        [self addSubview:self.label];
    }
    return self;
}

-(void)addTarget:(id)btnTarget forButtonAction:(SEL)action
{
    [self.button addTarget:btnTarget action:action forControlEvents:UIControlEventTouchUpInside];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
