//
//  LoginViewController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 4/19/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDelegate.h"
#import "HomeDetailViewController.h"
#import "CustomSplitViewController.h"
#import "BaseNavigationViewController.h"
#import "MBProgressHUD.h"
#import "BButton.h"
#import "NetworkResolver.h"

#define ToMainViewController @"ToMain"

@interface LoginViewController ()<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
@property (strong, nonatomic) UIBarButtonItem *barButtonItem;
@property (strong, nonatomic) UIPopoverController *popover;
@property (strong, nonatomic) IBOutlet BButton *btnLogin;
@end

@implementation LoginViewController
{
    int loginPhase;
    bool getTimeAbsenceTypeCompleted;
    bool getListApproverCompleted;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.title = @"Đăng nhập";
    CustomSplitViewController *splitViewController = (CustomSplitViewController*) [[[UIApplication sharedApplication] delegate] window].rootViewController;
    
    splitViewController.isLandscapeOK = YES;
    splitViewController.showsMasterInLandscape = NO;
    
    [splitViewController setMyToolBarHidden:YES animation:NO];
    
    
    [self.btnLogin setColor:[UIColor colorWithRed:201.0/255.0 green:3.0/255.0 blue:54.0/255.0 alpha:1.0]];
    [self.btnLogin addAwesomeIcon:FAIconUser beforeTitle:YES];
    
    UIColor *bg = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-login.jpg"]];
    self.view.backgroundColor = bg;
}

-(void)btnLoginClicked:(id)sender
{    
//    OrgViewController *controller = [[OrgViewController alloc] init];
//    UIPopoverController *pop = [[UIPopoverController alloc] initWithContentViewController:controller];
//    self.popover = pop;
//    controller.modalInPopover = NO;
//    [self.popover presentPopoverFromRect:self.btnLogin.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
    if (![self isValid])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi đăng nhập" message:@"Tên đăng nhập hoặc mật khẩu không được để trống" delegate:nil cancelButtonTitle:@"Nhập lại" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    [self doLogin];
}

-(BOOL)isValid
{
    if ([self.tb_username.text isEqualToString:@""] || [self.tb_password.text isEqualToString:@""])
        return NO;
    
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --
#pragma Employee service

-(void)doLogin
{    
    [self networkOperationIsPerforming];
    loginPhase = 0;
    [self doLoginWithPhaseNumber:loginPhase];
}

-(void)doLoginWithPhaseNumber:(int)phaseNumber
{
    switch (phaseNumber) {
        case 0:     // Verify username
        {
            [self validatedUser:self.tb_username.text pass:self.tb_password.text];
            break;
        }
        case 1:     // GetUserbyUserName
            [self getUserbyUserName:[self userName]];
            break;
//        
        default:
            //[self getAllApproverForEmployee:app.loginEmployee.EmployeeId];
            getListApproverCompleted = true;
            getTimeAbsenceTypeCompleted = true;
            //[self getListTimeAbsenceType];
            [self loginCompleted];
            break;
    }
}

-(NSString *)userName
{
    if ([self.tb_username.text rangeOfString:@"\\"].location != NSNotFound)
    {
        NSArray *arr = [self.tb_username.text componentsSeparatedByString:@"\\"];
        return arr[1];
    }
    
    return self.tb_username.text;
}
-(void)loginCompleted
{
    if (getListApproverCompleted && getTimeAbsenceTypeCompleted)
    {
        AppDelegate *app = [UIApplication sharedApplication].delegate;
        [self networkOperationCompleted];
        
        [app toMainPage];
    }
}


-(void)networkOperationIsPerforming
{
    [MBProgressHUD showHUDAddedTo:self.view animated:NO];
    [self.tb_password endEditing:YES];
    [self.tb_username endEditing:YES];
    
    id button = [self.view viewWithTag:1];
    if (button != nil)
        [button setEnabled:NO];
}

-(void)networkOperationCompleted
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    id button = [self.view viewWithTag:1];
    if (button != nil)
        [button setEnabled:YES];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    id destination = segue.destinationViewController;
    if ([destination isKindOfClass:[HomeDetailViewController class]])
    {
        HomeDetailViewController *controller = (HomeDetailViewController*) destination;
        CustomSplitViewController *splitViewController = (CustomSplitViewController*) [[[UIApplication sharedApplication] delegate] window].rootViewController;

        splitViewController.delegate = (id)controller;
        splitViewController.isLandscapeOK = YES;
    }
    
    CustomSplitViewController *splitViewController = (CustomSplitViewController*) [[[UIApplication sharedApplication] delegate] window].rootViewController;
    
    splitViewController.delegate = destination;
    //splitViewController.isLandscapeOK = YES;
}

- (BOOL)shouldAutorotate {
    return NO;
}


- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}


#pragma Security Service

- (void)validatedUser:(NSString *)userName pass:(NSString *)passWord{

    BasicHttpBinding_ISecurityServiceBinding *binding = [SecurityServiceSvc BasicHttpBinding_ISecurityServiceBinding];
    SecurityServiceSvc_SuccessLogin *request = [SecurityServiceSvc_SuccessLogin new];
    request.userName = userName;
    request.password = passWord;
    request.IPAddress = [NetworkResolver getIPAddress];
    request.HostName = @"iPad";
    request.SessionId = nil;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        BasicHttpBinding_ISecurityServiceBindingResponse *response = [binding SuccessLoginUsingParameters:request];
        for (id mine in response.bodyParts) {
            if([mine isKindOfClass:[SecurityServiceSvc_SuccessLoginResponse class]])
            {
                SecurityServiceSvc_SuccessLoginResponse *loginResponse = (SecurityServiceSvc_SuccessLoginResponse*)mine;
                NSString *error = loginResponse.error;
                 USBoolean *result = [loginResponse SuccessLoginResult];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([result boolValue]) {
                        loginPhase += 1;
                        [self doLoginWithPhaseNumber:loginPhase];
                    }else
                    {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi đăng nhập" message:error delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                        [alert show];
                        [self networkOperationCompleted];
                    }
                });
                
            }
        }
    });

    
}

- (void)getUserbyUserName:(NSString*)userName
{
    BasicHttpBinding_ISecurityServiceBinding *binding = [SecurityServiceSvc BasicHttpBinding_ISecurityServiceBinding];
    binding.logXMLInOut = YES;
    SecurityServiceSvc_GetUserByUserName *request = [[SecurityServiceSvc_GetUserByUserName alloc]init];
    request.userName = userName;
    
    [binding GetUserByUserNameAsyncUsingParameters:request delegate:self];
}

- (void)operation:(BasicHttpBinding_ISecurityServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_ISecurityServiceBindingResponse *)response
{
    if (response.bodyParts.count == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi đăng nhập" message:@"Không thể kết nối đến server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [self networkOperationCompleted];
    }
    
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    for(id mine in response.bodyParts)
    {
        if ([mine isKindOfClass:[SecurityServiceSvc_GetUserByUserNameResponse class]])
        {
            tns1_SysUser *sysUser = [mine GetUserByUserNameResult];
            if (sysUser != nil) {
                app.sysUser = sysUser;
                loginPhase += 1;
                [self doLoginWithPhaseNumber:loginPhase];
            }
            
        }
        
        if ([mine isKindOfClass:[SOAPFault class]])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Có lỗi trong quá trình đăng nhập" delegate:nil cancelButtonTitle:@"Thử lại" otherButtonTitles: nil];
            [alert show];
            
            [self networkOperationCompleted];
        }
    }
    
}


#pragma Employee Profile Service

- (void)getEmpBasicProfile:(NSNumber *)employeeId
{
    
}


@end
