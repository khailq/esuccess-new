//
//  JobPositionViewController.h
//  eSuccess
//
//  Created by admin on 6/14/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"
#import "EmpProfileLayerServiceSvc.h"
@interface JobPositionViewController : BaseDetailTableViewController<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>
@property(nonatomic, strong) NSMutableArray *jobPostionArr;
@property(nonatomic, strong) NSMutableArray *jobNameArr;
@end
