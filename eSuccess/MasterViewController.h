//
//  MasterViewController.h
//  Presidents
//
//  Created by HPTVIETNAM on 4/12/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
// test

#import <UIKit/UIKit.h>
#import "BaseMasterController.h"

@interface MasterViewController : BaseMasterController

@property (nonatomic, assign) BOOL viewProfileMode;

@end
