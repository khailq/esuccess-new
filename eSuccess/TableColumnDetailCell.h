//
//  TableColumnDetailCell.h
//  eSuccess
//
//  Created by HPTVIETNAM on 6/11/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableColumnDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *lb_describe;
@property (weak, nonatomic) IBOutlet UILabel *lb_measurement;
@property (weak, nonatomic) IBOutlet UILabel *lb_goalContent;
@property (weak, nonatomic) IBOutlet UILabel *lb_factContent;
@property (weak, nonatomic) IBOutlet UILabel *lb_percent;
@property (weak, nonatomic) IBOutlet UILabel *lb_creator;
@property (weak, nonatomic) IBOutlet UITextView *lb_report;

@end
