//
//  YearGoalsViewController.m
//  eSuccess
//
//  Created by admin on 6/7/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "YearGoalsViewController.h"
#import "AFJSONRequestOperation.h"
#import "GPPerspective.h"
#import "YearGoalCell.h"
#import "TableTitleCell.h"
#import "TableColumnDetailCell.h"

#define TableTitleCellIdentifier @"TableTitleCell"
#define TableColumnsDefinitionCellIdentifier @"TableColumnsDefinitionCell"
#define SeparateCellIdentifier @"SeparateCell"
#define TableColumnsDetailCellIdentifier @"TableColumnDetailCell"

@interface YearGoalsViewController ()

@property(nonatomic, strong) NSMutableArray *gpPerspectiveArr;

@end

@implementation YearGoalsViewController

@synthesize gpPerspectiveArr;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self getData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 130;
//}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([gpPerspectiveArr count] > 0) {
        GPPerspective *spec = [gpPerspectiveArr objectAtIndex:section];
        return spec.name;
    }
    return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([gpPerspectiveArr count] > 0) {
        return [gpPerspectiveArr count];
    } else
        return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (gpPerspectiveArr.count == 0)
        return 0;
    GPPerspective *perspective = gpPerspectiveArr[section];
    int rows = perspective.coYearArr.count * 3;
    for(GPCompanyYearObject *yearObject in perspective.coYearArr)
        rows += yearObject.gpScoreCardArr.count;
    
    NSLog(@"Section: %d. Row: %d", section, rows);
    return rows;
}

-(NSIndexPath *)getScoreCardForItemInIndexPath: (NSIndexPath *)indexPath
{
    GPPerspective *perspective = gpPerspectiveArr[indexPath.section];
    int count = 0;
    for (int index = 0; index < perspective.coYearArr.count; ++index)
    {
        GPCompanyYearObject *year = perspective.coYearArr[index];
        for (int row = 0; row < year.gpScoreCardArr.count + 3; ++row)
        {
            if (count++ == indexPath.row)
            {
                return [NSIndexPath indexPathForRow:row inSection:index];
            }
        }
    }
    
    return [NSIndexPath indexPathForRow:0 inSection:0];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 44)];
    UILabel *lb = [[UILabel alloc] initWithFrame:view.frame];
    lb.textAlignment = NSTextAlignmentCenter;
    lb.font = [UIFont boldSystemFontOfSize:17];
    lb.textColor = [UIColor whiteColor];
    view.backgroundColor = [UIColor colorWithRed:201.0/255.0 green:3.0/255.0 blue:54.0/255.0 alpha:1.0];
    if ([gpPerspectiveArr count] > 0) {
        GPPerspective *spec = [gpPerspectiveArr objectAtIndex:section];
        lb.text = spec.name;
    }
    return view;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if ([gpPerspectiveArr count] == 0) {
//        return [self getDefaultEmptyCell];
//    }
    
    GPPerspective *perspective = gpPerspectiveArr[indexPath.section];
    
    NSIndexPath *index = [self getScoreCardForItemInIndexPath:indexPath];
    GPCompanyYearObject *yearObject = perspective.coYearArr[index.section];
    
    NSLog(@"Row: %d. Section: %d", index.row, index.section);
    
    if (index.row >= yearObject.gpScoreCardArr.count + 2)
        return [tableView dequeueReusableCellWithIdentifier:SeparateCellIdentifier];
    
    if (index.row == 0)
    {
        GPCompanyYearObject *yearObject = perspective.coYearArr[index.section];
        TableTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:TableTitleCellIdentifier];
        cell.lb_title.text = yearObject.statement;
        cell.lb_additional.text = yearObject.smartTimeGoal;
        
        [cell.lb_title setTextColor:[UIColor whiteColor]];
        cell.lb_additional.textColor = [UIColor whiteColor];
        
        UIView *bgView = [[UIView alloc] initWithFrame:cell.frame];
        bgView.backgroundColor = [UIColor colorWithRed:55.0/255.0 green:130.0/255.0 blue:1.0 alpha:1.0];
        
        cell.backgroundView = bgView;
        
        return cell;
    }
    
    if (index.row == 1)
    {
        return [tableView dequeueReusableCellWithIdentifier:TableColumnsDefinitionCellIdentifier];
    }
    
    GPCompanyScoreCard *scoreCard = yearObject.gpScoreCardArr[index.row - 2];
    
    TableColumnDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:TableColumnsDetailCellIdentifier];
    cell.lb_describe.text = scoreCard.statement;
    cell.lb_creator.text = scoreCard.creatorFullName;
    cell.lb_factContent.text = [scoreCard.actual stringValue];
    cell.lb_goalContent.text = [scoreCard.target stringValue];
    cell.lb_measurement.text = scoreCard.metric;
    cell.lb_percent.text = [scoreCard.progress stringValue];
    
    return cell;
}

- (void)getData
{
    NSString *goalYearURL = @"http://10.0.18.118:6005/Goal/GetCompanyYearObjectiveViewModel";
    NSURL *url = [NSURL URLWithString:goalYearURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    gpPerspectiveArr = [[NSMutableArray alloc]init];

    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        for (id obj in JSON) {
            NSArray *gpCompanyObj = [obj objectForKey:@"GPCompanyYearlyObjectives"];
            NSMutableArray *companyObjArr = [[NSMutableArray alloc]init];
            for (id coObj in gpCompanyObj) {
                NSArray *gpScoreCardArr = [coObj objectForKey:@"GPCompanyScoreCards"];
                NSMutableArray *scoreCardArr = [[NSMutableArray alloc]init];
                
                GPCompanyYearObject *coYear = [[GPCompanyYearObject alloc]init];
                coYear.GPCompanyId = [[coObj objectForKey:@"GPCompanyYearlyObjectiveId"]intValue];
                coYear.statement = [coObj objectForKey:@"Statement"];
                coYear.smartTimeGoal = [coObj objectForKey:@"SmartTimeGoal"];

                //NSString *title = [NSString stringWithFormat:@"%@ - %@",coYear.statement,coYear.smartTimeGoal];
                
                if ([gpScoreCardArr count] > 0) {
                    for (id score in gpScoreCardArr) {
                        GPCompanyScoreCard *scoreCard = [GPCompanyScoreCard parseScoreCardFromDict:score];
                        [scoreCardArr addObject:scoreCard];
                    }
                }
                coYear.gpScoreCardArr = scoreCardArr;
                [companyObjArr addObject:coYear];
            }
            GPPerspective *gpPerspec = [[GPPerspective alloc]init];
            gpPerspec.gpPerspecId = [[obj objectForKey:@"GPPerspectiveId"]intValue];
            gpPerspec.name = [obj objectForKey:@"Name"];
            gpPerspec.coYearArr = companyObjArr;
            [gpPerspectiveArr addObject:gpPerspec];
            
            if (!self.viewDidDisappear)
                [self.tableView reloadData];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error Retrieving Data from Server"
                                                        message:[NSString stringWithFormat:@"%@",error]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }];
    
    [operation start];
    
}

@end
