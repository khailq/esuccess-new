//
//  ProfileCell.h
//  eSuccess
//
//  Created by admin on 5/4/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileCell : UITableViewCell
{
    IBOutlet UILabel *lblTime;
    IBOutlet UILabel *lblWork;
    IBOutlet UILabel *lblPlace;
    IBOutlet UILabel *lblNote;
}
@property(nonatomic, retain) IBOutlet UILabel *lblTime;
@property(nonatomic, retain) IBOutlet UILabel *lblWork;
@property(nonatomic, retain) IBOutlet UILabel *lblPlace;
@property(nonatomic, retain) IBOutlet UILabel *lblNote;
@end
