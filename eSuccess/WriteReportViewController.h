//
//  WriteReportViewController.h
//  eSuccess
//
//  Created by admin on 6/7/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"

@interface WriteReportViewController : BaseDetailTableViewController
@property(nonatomic, retain) IBOutlet UITextView *tvReport;
@end
