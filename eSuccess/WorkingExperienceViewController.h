//
//  WorkingExperienceViewController.h
//  eSuccess
//
//  Created by admin on 5/11/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"
#import "EmpProfileLayerServiceSvc.h"
@interface WorkingExperienceViewController : BaseDetailTableViewController<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>
@property(nonatomic, retain) NSMutableArray *workingArray;
@property(nonatomic, retain) NSMutableArray *projectArray;
@end
