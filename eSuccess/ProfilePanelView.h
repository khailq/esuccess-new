//
//  ProfilePanelView.h
//  eSuccess
//
//  Created by HPTVIETNAM on 5/7/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>

#define V_PanelWidth 90
#define V_PanelHeight 135
#define H_PanelWidth 195
#define H_PanelHeight 95

@class ProfilePanelView;
@protocol ProfilePanelProtocol <NSObject>

-(void)touchInPanel:(ProfilePanelView*)panel;

@end

@interface ProfilePanelView : UIView

@property (strong, nonatomic) UIImageView *avatar;

@property (strong, nonatomic) id dataContext;
@property (nonatomic, assign) BOOL selected;
@property (weak) id<ProfilePanelProtocol> delegate;

-(void)setChildName:(NSString *)name;
-(void)setPositionname:(NSString*)position;
-(void)setUnitName:(NSString*)unitName;

@end
