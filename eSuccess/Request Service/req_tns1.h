#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class req_tns1_LeaveRequestSubmitModel;
@class req_tns1_ArrayOfRequestDetail;
@class req_tns1_RequestDetail;
@class req_tns1_ResponseModel;
@class req_tns1_WorkingFormEmployeeModel;
#import "req_tns2.h"
@interface req_tns1_RequestDetail : NSObject {
	
/* elements */
	NSDate * DayOff;
	NSNumber * OffWorkType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (req_tns1_RequestDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * DayOff;
@property (retain) NSNumber * OffWorkType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface req_tns1_ArrayOfRequestDetail : NSObject {
	
/* elements */
	NSMutableArray *RequestDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (req_tns1_ArrayOfRequestDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRequestDetail:(req_tns1_RequestDetail *)toAdd;
@property (readonly) NSMutableArray * RequestDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface req_tns1_LeaveRequestSubmitModel : NSObject {
	
/* elements */
	NSString * Address;
	NSString * ApprovalStatus;
	USBoolean * ApprovedNow;
	NSNumber * ApproverId;
	NSString * CreatedDate;
	req_tns2_ArrayOfstring * DayLeave;
	NSString * EmpCode;
	NSNumber * EmpId;
	NSString * EndDate;
	NSNumber * Id_;
	req_tns1_ArrayOfRequestDetail * LeaveRequestDetail;
	req_tns2_ArrayOfstring * LeaveStatus;
	NSString * MinusAttendance;
	NSString * NotSalary;
	NSNumber * NumberDaysOff;
	NSString * Phone;
	NSString * Reason;
	NSString * ReturnUrl;
	NSString * StartDate;
	NSString * TimeAbsenceTypeId;
	NSNumber * UserId;
	USBoolean * isDraft;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (req_tns1_LeaveRequestSubmitModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSString * ApprovalStatus;
@property (retain) USBoolean * ApprovedNow;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * CreatedDate;
@property (retain) req_tns2_ArrayOfstring * DayLeave;
@property (retain) NSString * EmpCode;
@property (retain) NSNumber * EmpId;
@property (retain) NSString * EndDate;
@property (retain) NSNumber * Id_;
@property (retain) req_tns1_ArrayOfRequestDetail * LeaveRequestDetail;
@property (retain) req_tns2_ArrayOfstring * LeaveStatus;
@property (retain) NSString * MinusAttendance;
@property (retain) NSString * NotSalary;
@property (retain) NSNumber * NumberDaysOff;
@property (retain) NSString * Phone;
@property (retain) NSString * Reason;
@property (retain) NSString * ReturnUrl;
@property (retain) NSString * StartDate;
@property (retain) NSString * TimeAbsenceTypeId;
@property (retain) NSNumber * UserId;
@property (retain) USBoolean * isDraft;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface req_tns1_ResponseModel : NSObject {
	
/* elements */
	NSString * Message;
	USBoolean * Succeed;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (req_tns1_ResponseModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Message;
@property (retain) USBoolean * Succeed;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface req_tns1_WorkingFormEmployeeModel : NSObject {
	
/* elements */
	NSString * TimeIn;
	NSString * TimeOut;
	NSString * WorkingFormName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (req_tns1_WorkingFormEmployeeModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * TimeIn;
@property (retain) NSString * TimeOut;
@property (retain) NSString * WorkingFormName;
/* attributes */
- (NSDictionary *)attributes;
@end
