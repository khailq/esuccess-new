#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class RequestServiceSvc_CreateLeaveRequest;
@class RequestServiceSvc_CreateLeaveRequestResponse;
@class RequestServiceSvc_GetEmployeeWorkingForm;
@class RequestServiceSvc_GetEmployeeWorkingFormResponse;
#import "req_tns1.h"
@interface RequestServiceSvc_CreateLeaveRequest : NSObject {
	
/* elements */
	req_tns1_LeaveRequestSubmitModel * model;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (RequestServiceSvc_CreateLeaveRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) req_tns1_LeaveRequestSubmitModel * model;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface RequestServiceSvc_CreateLeaveRequestResponse : NSObject {
	
/* elements */
	req_tns1_ResponseModel * CreateLeaveRequestResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (RequestServiceSvc_CreateLeaveRequestResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) req_tns1_ResponseModel * CreateLeaveRequestResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface RequestServiceSvc_GetEmployeeWorkingForm : NSObject {
	
/* elements */
	NSNumber * empId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (RequestServiceSvc_GetEmployeeWorkingForm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * empId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface RequestServiceSvc_GetEmployeeWorkingFormResponse : NSObject {
	
/* elements */
	req_tns1_WorkingFormEmployeeModel * GetEmployeeWorkingFormResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (RequestServiceSvc_GetEmployeeWorkingFormResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) req_tns1_WorkingFormEmployeeModel * GetEmployeeWorkingFormResult;
/* attributes */
- (NSDictionary *)attributes;
@end
/* Cookies handling provided by http://en.wikibooks.org/wiki/Programming:WebObjects/Web_Services/Web_Service_Provider */
#import <libxml/parser.h>
#import "xs.h"
#import "RequestServiceSvc.h"
#import "ns1.h"
#import "req_tns1.h"
#import "req_tns2.h"
#import "req_tns3.h"
@class BasicHttpBinding_IRequestServiceBinding;
@interface RequestServiceSvc : NSObject {
	
}
+ (BasicHttpBinding_IRequestServiceBinding *)BasicHttpBinding_IRequestServiceBinding;
@end
@class BasicHttpBinding_IRequestServiceBindingResponse;
@class BasicHttpBinding_IRequestServiceBindingOperation;
@protocol BasicHttpBinding_IRequestServiceBindingResponseDelegate <NSObject>
- (void) operation:(BasicHttpBinding_IRequestServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IRequestServiceBindingResponse *)response;
@end
@interface BasicHttpBinding_IRequestServiceBinding : NSObject <BasicHttpBinding_IRequestServiceBindingResponseDelegate> {
	NSURL *address;
	NSTimeInterval defaultTimeout;
	NSMutableArray *cookies;
	BOOL logXMLInOut;
	BOOL synchronousOperationComplete;
	NSString *authUsername;
	NSString *authPassword;
}
@property (copy) NSURL *address;
@property (assign) BOOL logXMLInOut;
@property (assign) NSTimeInterval defaultTimeout;
@property (nonatomic, retain) NSMutableArray *cookies;
@property (nonatomic, retain) NSString *authUsername;
@property (nonatomic, retain) NSString *authPassword;
- (id)initWithAddress:(NSString *)anAddress;
- (void)sendHTTPCallUsingBody:(NSString *)body soapAction:(NSString *)soapAction forOperation:(BasicHttpBinding_IRequestServiceBindingOperation *)operation;
- (void)addCookie:(NSHTTPCookie *)toAdd;
- (BasicHttpBinding_IRequestServiceBindingResponse *)CreateLeaveRequestUsingParameters:(RequestServiceSvc_CreateLeaveRequest *)aParameters ;
- (void)CreateLeaveRequestAsyncUsingParameters:(RequestServiceSvc_CreateLeaveRequest *)aParameters  delegate:(id<BasicHttpBinding_IRequestServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IRequestServiceBindingResponse *)GetEmployeeWorkingFormUsingParameters:(RequestServiceSvc_GetEmployeeWorkingForm *)aParameters ;
- (void)GetEmployeeWorkingFormAsyncUsingParameters:(RequestServiceSvc_GetEmployeeWorkingForm *)aParameters  delegate:(id<BasicHttpBinding_IRequestServiceBindingResponseDelegate>)responseDelegate;
@end
@interface BasicHttpBinding_IRequestServiceBindingOperation : NSOperation {
	BasicHttpBinding_IRequestServiceBinding *binding;
	BasicHttpBinding_IRequestServiceBindingResponse *response;
	id<BasicHttpBinding_IRequestServiceBindingResponseDelegate> delegate;
	NSMutableData *responseData;
	NSURLConnection *urlConnection;
}
@property (retain) BasicHttpBinding_IRequestServiceBinding *binding;
@property (readonly) BasicHttpBinding_IRequestServiceBindingResponse *response;
@property (nonatomic, assign) id<BasicHttpBinding_IRequestServiceBindingResponseDelegate> delegate;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, retain) NSURLConnection *urlConnection;
- (id)initWithBinding:(BasicHttpBinding_IRequestServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IRequestServiceBindingResponseDelegate>)aDelegate;
@end
@interface BasicHttpBinding_IRequestServiceBinding_CreateLeaveRequest : BasicHttpBinding_IRequestServiceBindingOperation {
	RequestServiceSvc_CreateLeaveRequest * parameters;
}
@property (retain) RequestServiceSvc_CreateLeaveRequest * parameters;
- (id)initWithBinding:(BasicHttpBinding_IRequestServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IRequestServiceBindingResponseDelegate>)aDelegate
	parameters:(RequestServiceSvc_CreateLeaveRequest *)aParameters
;
@end
@interface BasicHttpBinding_IRequestServiceBinding_GetEmployeeWorkingForm : BasicHttpBinding_IRequestServiceBindingOperation {
	RequestServiceSvc_GetEmployeeWorkingForm * parameters;
}
@property (retain) RequestServiceSvc_GetEmployeeWorkingForm * parameters;
- (id)initWithBinding:(BasicHttpBinding_IRequestServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IRequestServiceBindingResponseDelegate>)aDelegate
	parameters:(RequestServiceSvc_GetEmployeeWorkingForm *)aParameters
;
@end
@interface BasicHttpBinding_IRequestServiceBinding_envelope : NSObject {
}
+ (BasicHttpBinding_IRequestServiceBinding_envelope *)sharedInstance;
- (NSString *)serializedFormUsingHeaderElements:(NSDictionary *)headerElements bodyElements:(NSDictionary *)bodyElements;
@end
@interface BasicHttpBinding_IRequestServiceBindingResponse : NSObject {
	NSArray *headers;
	NSArray *bodyParts;
	NSError *error;
}
@property (retain) NSArray *headers;
@property (retain) NSArray *bodyParts;
@property (retain) NSError *error;
@end
