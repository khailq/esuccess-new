//
//  ContractInforViewController.m
//  eSuccess
//
//  Created by admin on 5/18/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "ContractInforViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "DayLeaveDetailCell.h"
@interface ContractInforViewController ()

@end

@implementation ContractInforViewController
@synthesize contractArr;
@synthesize contractTypeArr;
static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Hợp đồng lao động";
    [self getData];
    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([contractTypeArr count] > 0) {
        return [contractTypeArr objectAtIndex:section];
    }
    return nil;
}
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 130;
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [contractArr count] > 0 ? contractArr.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //return [profileArray count];
    return [contractArr count] > 0 ? 19 : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (contractArr.count == 0)
        return [self getDefaultEmptyCell];
    
    DayLeaveDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
    if (cell == nil) {
        cell = [[DayLeaveDetailCell alloc]init];
    }
    cell.lbLeft.font = [UIFont systemFontOfSize:14];
    cell.lbLeft.backgroundColor = [UIColor clearColor];
    cell.lbRight.font = [UIFont boldSystemFontOfSize:16];
    cell.lbRight.backgroundColor = [UIColor clearColor];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    empl_tns1_V_EmpContract *contract = contractArr[indexPath.section];

    if (indexPath.row == 0) {
        cell.lbLeft.text = @"Công việc";
        cell.lbRight.text = contract.JobName;
    }
    if (indexPath.row == 1) {
        cell.lbLeft.text = @"Vị trí";
        cell.lbRight.text = contract.JobPositionName;
    }
    if (indexPath.row == 2) {
        cell.lbLeft.text = @"Nơi làm việc";
        cell.lbRight.text = contract.WorkingAddress;
    }
    if (indexPath.row == 3) {
        cell.lbLeft.text = @"Hình thức";
        cell.lbRight.text = contract.WorkingTimePolicy;
    }
    if (indexPath.row == 4) {
        cell.lbLeft.text = @"Ngày bắt đầu";
        if (contract.StartDate != nil) {
            cell.lbRight.text = [formatter stringFromDate:contract.StartDate];
        } else
            cell.lbRight.text = @"Đang cập nhật";
    }
    if (indexPath.row == 5) {
        cell.lbLeft.text = @"Ngày kết thúc";
        if (contract.EndDate != nil) {
            cell.lbRight.text = [formatter stringFromDate:contract.EndDate];
        } else
            cell.lbRight.text = @"Đang cập nhật";
    }
    if (indexPath.row == 6) {
        cell.lbLeft.text = @"Thời gian làm";
        cell.lbRight.text = contract.WorkingTime;
    }
    if (indexPath.row == 7) {
        cell.lbLeft.text = @"Phương tiện";
        cell.lbRight.text = contract.Transportation;
    }
    if (indexPath.row == 8) {
        cell.lbLeft.text = @"Lương cơ bản";
        cell.lbRight.text = [NSString stringWithFormat:@"%d",[contract.BaseWage integerValue] ];
    }
    if (indexPath.row == 9) {
        cell.lbLeft.text = @"Hình thức trả";
        cell.lbRight.text = contract.PayingMethod;
    }
    if (indexPath.row == 10) {
        cell.lbLeft.text = @"Số ngày trả";
        cell.lbRight.text = contract.PayDay;
    }
    if (indexPath.row == 11) {
        cell.lbLeft.text = @"Phụ cấp";
        cell.lbRight.text = contract.Allowances;
    }
    if (indexPath.row == 12) {
        cell.lbLeft.text = @"Tiền thưởng";
        cell.lbRight.text = contract.Bonuses;
    }
    if (indexPath.row == 13) {
        cell.lbLeft.text = @"Nghỉ mát";
        cell.lbRight.text = contract.HolIdayPolicy;
    }
    if (indexPath.row == 14) {
        cell.lbLeft.text = @"BHYT";
        cell.lbRight.text = contract.MedicalInsurance;
    }
    if (indexPath.row == 15) {
        cell.lbLeft.text = @"BHXH";
        cell.lbRight.text = contract.SocialInsurance;
    }
    if (indexPath.row == 16) {
        cell.lbLeft.text = @"Training";
        cell.lbRight.text = contract.TrainingPolicy;
    }
    if (indexPath.row == 17) {
        cell.lbLeft.text = @"Đã ký";
        if ([contract.IsSigned boolValue] == YES) {
            cell.lbRight.text = @"Rồi";
        } else
            cell.lbRight.text = @"Chưa";
    }
    if (indexPath.row == 18) {
        cell.lbLeft.text = @"Còn hiệu lực";
        if ([contract.IsDeleted boolValue] == YES) {
            cell.lbRight.text = @"Còn";
        } else
            cell.lbRight.text = @"Hết hạn";
    }
    return cell;
}

#pragma get Emp Contract
-(void)getData
{
    contractArr = [[NSMutableArray alloc]init];
    contractTypeArr = [[NSMutableArray alloc]init];
    [self getEmpContractById];
}

- (void)getEmpContractById
{
//    BasicHttpBinding_IEmpProfileLayerServiceBinding * binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
//     EmpProfileLayerServiceSvc_GetViewEmpProfileContractById *request = [[EmpProfileLayerServiceSvc_GetViewEmpProfileContractById alloc] init];
//    
//    AppDelegate *app = [UIApplication sharedApplication].delegate;
//    if (app.otherEmpId != nil) {
//        request.EmployeeId = app.otherEmpId;
//    } else{
//        request.EmployeeId = app.loginEmployeeInfo.EmployeeId;
//    }
//    [binding GetViewEmpProfileContractByIdAsyncUsingParameters:request delegate:self];
}

- (void)operation:(BasicHttpBinding_IEmpProfileLayerServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)response
{
    if ([response.bodyParts count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi kết nối" message:@"Không thể kết nối đến server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    for (id mine in response.bodyParts) {
        if ([mine isKindOfClass:[EmpProfileLayerServiceSvc_GetViewEmpProfileContractByIdResponse class]]) {
            empl_tns1_ArrayOfV_EmpContract *result = [mine GetViewEmpProfileContractByIdResult];
            contractArr = result.V_EmpContract;
            for (empl_tns1_V_EmpContract *contract in contractArr)  {
                [contractTypeArr addObject:contract.ContractCode];
            }
        }
        
        //        if ([mine isKindOfClass:[SOAPFault class]])
        //        {
        //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Server error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //            [alert show];
        //        }
    }
    
    if (!self.viewDidDisappear)
        [self.tableView reloadData];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
}


@end
