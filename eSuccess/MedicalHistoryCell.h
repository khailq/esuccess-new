//
//  MedicalHistoryCell.h
//  eSuccess
//
//  Created by admin on 5/4/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedicalHistoryCell : UITableViewCell
{
    IBOutlet UILabel *lblDiseaseName;
    IBOutlet UILabel *lblTime;
    IBOutlet UILabel *lblBenefit;
    
}
@property(nonatomic, retain) IBOutlet UILabel *lblDiseaseName;
@property(nonatomic, retain) IBOutlet UILabel *lblTime;
@property(nonatomic, retain) IBOutlet UILabel *lblBenefit;
@end
