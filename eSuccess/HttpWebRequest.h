//
//  HttpWebRequest.h
//  ePass
//
//  Created by Scott on 4/10/13.
//  Copyright (c) 2013 Scott. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HttpWebRequest : NSObject

+(void)getDataAsyncWithURL:(NSString*)url completionHandler:(void (^)(NSData* returnedData, NSError* error))onCompletedBlock;

+(void)postDataAsyncToURL:(NSString*)url postBody:(NSData*)data completionHandler:(void (^)(NSData *returnData, NSError *error))onCompletedBlock;
@end
