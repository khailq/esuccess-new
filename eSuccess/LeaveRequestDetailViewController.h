//
//  LeaveRequestDetailViewController.h
//  eSuccess
//
//  Created by admin on 5/28/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"

@interface LeaveRequestDetailViewController : BaseDetailTableViewController

@property(nonatomic, retain) NSMutableArray *leaveRequestArr;
@property(nonatomic, retain) NSMutableArray *timeArr;

@end
