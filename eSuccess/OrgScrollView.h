//
//  OrgScrollView.h
//  eSuccess
//
//  Created by HPTVIETNAM on 8/12/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfilePanelView.h"

@interface OrgScrollView : UIView
-(id)initWithDirectReportList:(NSArray *)directReport frame:(CGRect)frame parent:(id<ProfilePanelProtocol>)parent;
@property (assign) CGFloat contentWidth;
@property (weak) id<ProfilePanelProtocol> parentDataContext;
@end
