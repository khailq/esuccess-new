#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class wf_tns3_ArrayOfWorkflowCatalog;
@class wf_tns3_WorkflowCatalog;
@class wf_tns3_WorkflowProcessFile;
@class wf_tns3_V_Process;
@class wf_tns3_ArrayOfV_Process;
@class wf_tns3_WorkflowProcess;
#import "wf_tns2.h"
@interface wf_tns3_WorkflowCatalog : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * ProcessGroup;
	NSString * ProcessName;
	NSString * WorkflowCatalogCode;
	NSNumber * WorkflowCatalogId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (wf_tns3_WorkflowCatalog *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * ProcessGroup;
@property (retain) NSString * ProcessName;
@property (retain) NSString * WorkflowCatalogCode;
@property (retain) NSNumber * WorkflowCatalogId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface wf_tns3_ArrayOfWorkflowCatalog : NSObject {
	
/* elements */
	NSMutableArray *WorkflowCatalog;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (wf_tns3_ArrayOfWorkflowCatalog *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addWorkflowCatalog:(wf_tns3_WorkflowCatalog *)toAdd;
@property (readonly) NSMutableArray * WorkflowCatalog;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface wf_tns3_WorkflowProcessFile : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreateBy;
	NSDate * CreateDate;
	NSString * Description;
	NSString * FileName;
	USBoolean * IsActived;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * RealFileName;
	NSNumber * TotalStep;
	NSNumber * WorkflowCatalogId;
	NSNumber * WorkflowProcessFileId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (wf_tns3_WorkflowProcessFile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreateBy;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) NSString * FileName;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * RealFileName;
@property (retain) NSNumber * TotalStep;
@property (retain) NSNumber * WorkflowCatalogId;
@property (retain) NSNumber * WorkflowProcessFileId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface wf_tns3_V_Process : NSObject {
	
/* elements */
	NSString * CreateDate;
	NSString * Description;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * FileName;
	USBoolean * IsActived;
	NSString * ProcessGroup;
	NSString * ProcessName;
	NSString * RealFileName;
	NSNumber * TotalStep;
	NSString * WorkflowCatalogCode;
	NSNumber * WorkflowCatalogId;
	NSNumber * WorkflowProcessFileId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (wf_tns3_V_Process *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CreateDate;
@property (retain) NSString * Description;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * FileName;
@property (retain) USBoolean * IsActived;
@property (retain) NSString * ProcessGroup;
@property (retain) NSString * ProcessName;
@property (retain) NSString * RealFileName;
@property (retain) NSNumber * TotalStep;
@property (retain) NSString * WorkflowCatalogCode;
@property (retain) NSNumber * WorkflowCatalogId;
@property (retain) NSNumber * WorkflowProcessFileId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface wf_tns3_ArrayOfV_Process : NSObject {
	
/* elements */
	NSMutableArray *V_Process;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (wf_tns3_ArrayOfV_Process *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_Process:(wf_tns3_V_Process *)toAdd;
@property (readonly) NSMutableArray * V_Process;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface wf_tns3_WorkflowProcess : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	USBoolean * IsFinished;
	NSDate * ModifiedDate;
	NSNumber * WorkflowCatalogId;
	NSString * WorkflowProcessGuid;
	NSNumber * WorkflowProcessId;
	NSNumber * WorkflowStepTrackingId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (wf_tns3_WorkflowProcess *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsFinished;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * WorkflowCatalogId;
@property (retain) NSString * WorkflowProcessGuid;
@property (retain) NSNumber * WorkflowProcessId;
@property (retain) NSNumber * WorkflowStepTrackingId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
