#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class WorkflowServicesSvc_Start;
@class WorkflowServicesSvc_StartResponse;
@class WorkflowServicesSvc_Unload;
@class WorkflowServicesSvc_UnloadResponse;
@class WorkflowServicesSvc_GetBookmark;
@class WorkflowServicesSvc_GetBookmarkResponse;
@class WorkflowServicesSvc_Load;
@class WorkflowServicesSvc_LoadResponse;
@class WorkflowServicesSvc_FileUpload;
@class WorkflowServicesSvc_ResultUpload;
@class WorkflowServicesSvc_GetListFile;
@class WorkflowServicesSvc_GetListFileResponse;
@class WorkflowServicesSvc_CheckFileExist;
@class WorkflowServicesSvc_CheckFileExistResponse;
@class WorkflowServicesSvc_GetWorkflowCatalog;
@class WorkflowServicesSvc_GetWorkflowCatalogResponse;
@class WorkflowServicesSvc_AddProcessFile;
@class WorkflowServicesSvc_AddProcessFileResponse;
@class WorkflowServicesSvc_EditProcessFile;
@class WorkflowServicesSvc_EditProcessFileResponse;
@class WorkflowServicesSvc_DeleteProcessFile;
@class WorkflowServicesSvc_DeleteProcessFileResponse;
@class WorkflowServicesSvc_AddWorkflowCatalog;
@class WorkflowServicesSvc_AddWorkflowCatalogResponse;
@class WorkflowServicesSvc_GetProcessByCatalogId;
@class WorkflowServicesSvc_GetProcessByCatalogIdResponse;
@class WorkflowServicesSvc_CheckExistWorkflowCatalog;
@class WorkflowServicesSvc_CheckExistWorkflowCatalogResponse;
@class WorkflowServicesSvc_GetProcessFileById;
@class WorkflowServicesSvc_GetProcessFileByIdResponse;
@class WorkflowServicesSvc_GetGroupsOfWorkflowCatalog;
@class WorkflowServicesSvc_GetGroupsOfWorkflowCatalogResponse;
@class WorkflowServicesSvc_GetWorkflowCatalogByID;
@class WorkflowServicesSvc_GetWorkflowCatalogByIDResponse;
@class WorkflowServicesSvc_WriteLog;
@class WorkflowServicesSvc_WriteLogResponse;
@class WorkflowServicesSvc_DownloadFile;
@class WorkflowServicesSvc_DownloadFileResponse;
@class WorkflowServicesSvc_GetLastRecordInProcessFile;
@class WorkflowServicesSvc_GetLastRecordInProcessFileResponse;
@class WorkflowServicesSvc_EditWorkflowCatalog;
@class WorkflowServicesSvc_EditWorkflowCatalogResponse;
@class WorkflowServicesSvc_DeleteWorkflowCatalog;
@class WorkflowServicesSvc_DeleteWorkflowCatalogResponse;
@class WorkflowServicesSvc_CheckExistWFCode;
@class WorkflowServicesSvc_CheckExistWFCodeResponse;
@class WorkflowServicesSvc_GetListFileByWorkflowCatalog;
@class WorkflowServicesSvc_GetListFileByWorkflowCatalogResponse;
@class WorkflowServicesSvc_GetTotalProcessHistoryFile;
@class WorkflowServicesSvc_GetTotalProcessHistoryFileResponse;
@class WorkflowServicesSvc_ActiveProcess;
@class WorkflowServicesSvc_ActiveProcessResponse;
@class WorkflowServicesSvc_AddWorkflowProcess;
@class WorkflowServicesSvc_AddWorkflowProcessResponse;
@class WorkflowServicesSvc_CheckWorkflowFinish;
@class WorkflowServicesSvc_CheckWorkflowFinishResponse;
@class WorkflowServicesSvc_GetTotalTrackingRecords;
@class WorkflowServicesSvc_GetTotalTrackingRecordsResponse;
@class WorkflowServicesSvc_GetCurrentLastStep;
@class WorkflowServicesSvc_GetCurrentLastStepResponse;
@class WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalog;
@class WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalogResponse;
@class WorkflowServicesSvc_GetTotalWorkflowTrackingByWFCatAndOrg;
@class WorkflowServicesSvc_GetTotalWorkflowTrackingByWFCatAndOrgResponse;
@class WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalogAndStatus;
@class WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalogAndStatusResponse;
@class WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalogAndOrgAndStatus;
@class WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalogAndOrgAndStatusResponse;
@class WorkflowServicesSvc_GetTotalAllWorkflowTracking;
@class WorkflowServicesSvc_GetTotalAllWorkflowTrackingResponse;
@class WorkflowServicesSvc_GetTotalAllWorkflowTrackingByOrg;
@class WorkflowServicesSvc_GetTotalAllWorkflowTrackingByOrgResponse;
@class WorkflowServicesSvc_GetTotalAllWorkflowTrackingByStatus;
@class WorkflowServicesSvc_GetTotalAllWorkflowTrackingByStatusResponse;
@class WorkflowServicesSvc_GetTotalAllWorkflowTrackingByOrgAndStatus;
@class WorkflowServicesSvc_GetTotalAllWorkflowTrackingByOrgAndStatusResponse;
@class WorkflowServicesSvc_StartProcess;
@class WorkflowServicesSvc_StartProcessResponse;
@class WorkflowServicesSvc_ResumeBookmarkAfterStart;
@class WorkflowServicesSvc_ResumeBookmarkAfterStartResponse;
@class WorkflowServicesSvc_ResumeBookmarkWithFunction;
@class WorkflowServicesSvc_ResumeBookmarkWithFunctionResponse;
@class WorkflowServicesSvc_ResumeWorkflow;
@class WorkflowServicesSvc_ResumeWorkflowResponse;
@class WorkflowServicesSvc_ResumeBookmark;
@class WorkflowServicesSvc_ResumeBookmarkResponse;
@class WorkflowServicesSvc_ResumeBookmarkWithDictionary;
@class WorkflowServicesSvc_ResumeBookmarkWithDictionaryResponse;
@class WorkflowServicesSvc_GetProcessByFunction;
@class WorkflowServicesSvc_GetProcessByFunctionResponse;
@class WorkflowServicesSvc_GetProcessByFunctionAndWorkflowCode;
@class WorkflowServicesSvc_GetProcessByFunctionAndWorkflowCodeResponse;
#import "wf_tns1.h"
#import "wf_tns2.h"
#import "wf_tns3.h"
@interface WorkflowServicesSvc_Start : NSObject {
	
/* elements */
	NSString * bussinessProcessName;
	wf_tns1_ArrayOfint * employeeIDJoinSteps;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_Start *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * bussinessProcessName;
@property (retain) wf_tns1_ArrayOfint * employeeIDJoinSteps;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_StartResponse : NSObject {
	
/* elements */
	NSString * StartResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_StartResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * StartResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_Unload : NSObject {
	
/* elements */
	NSString * bussinessProcessName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_Unload *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * bussinessProcessName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_UnloadResponse : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_UnloadResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetBookmark : NSObject {
	
/* elements */
	NSString * id_;
	NSString * bussinessProcessName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetBookmark *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * id_;
@property (retain) NSString * bussinessProcessName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetBookmarkResponse : NSObject {
	
/* elements */
	NSString * GetBookmarkResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetBookmarkResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * GetBookmarkResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_Load : NSObject {
	
/* elements */
	NSString * id_;
	NSString * bussinessProcessName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_Load *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * id_;
@property (retain) NSString * bussinessProcessName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_LoadResponse : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_LoadResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_FileUpload : NSObject {
	
/* elements */
	NSData * Buffer;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_FileUpload *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSData * Buffer;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_ResultUpload : NSObject {
	
/* elements */
	NSString * Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_ResultUpload *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetListFile : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetListFile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetListFileResponse : NSObject {
	
/* elements */
	wf_tns1_ArrayOfstring * GetListFileResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetListFileResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) wf_tns1_ArrayOfstring * GetListFileResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_CheckFileExist : NSObject {
	
/* elements */
	NSString * fileName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_CheckFileExist *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * fileName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_CheckFileExistResponse : NSObject {
	
/* elements */
	USBoolean * CheckFileExistResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_CheckFileExistResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * CheckFileExistResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetWorkflowCatalog : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetWorkflowCatalog *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetWorkflowCatalogResponse : NSObject {
	
/* elements */
	wf_tns3_ArrayOfWorkflowCatalog * GetWorkflowCatalogResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetWorkflowCatalogResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) wf_tns3_ArrayOfWorkflowCatalog * GetWorkflowCatalogResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_AddProcessFile : NSObject {
	
/* elements */
	wf_tns3_WorkflowProcessFile * processFile;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_AddProcessFile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) wf_tns3_WorkflowProcessFile * processFile;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_AddProcessFileResponse : NSObject {
	
/* elements */
	NSString * AddProcessFileResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_AddProcessFileResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AddProcessFileResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_EditProcessFile : NSObject {
	
/* elements */
	wf_tns3_WorkflowProcessFile * processFile;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_EditProcessFile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) wf_tns3_WorkflowProcessFile * processFile;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_EditProcessFileResponse : NSObject {
	
/* elements */
	NSString * EditProcessFileResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_EditProcessFileResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * EditProcessFileResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_DeleteProcessFile : NSObject {
	
/* elements */
	NSNumber * processFileId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_DeleteProcessFile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * processFileId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_DeleteProcessFileResponse : NSObject {
	
/* elements */
	NSString * DeleteProcessFileResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_DeleteProcessFileResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * DeleteProcessFileResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_AddWorkflowCatalog : NSObject {
	
/* elements */
	wf_tns3_WorkflowCatalog * wfCatalog;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_AddWorkflowCatalog *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) wf_tns3_WorkflowCatalog * wfCatalog;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_AddWorkflowCatalogResponse : NSObject {
	
/* elements */
	NSString * AddWorkflowCatalogResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_AddWorkflowCatalogResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AddWorkflowCatalogResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetProcessByCatalogId : NSObject {
	
/* elements */
	NSNumber * workflowCatalogId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetProcessByCatalogId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * workflowCatalogId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetProcessByCatalogIdResponse : NSObject {
	
/* elements */
	wf_tns3_V_Process * GetProcessByCatalogIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetProcessByCatalogIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) wf_tns3_V_Process * GetProcessByCatalogIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_CheckExistWorkflowCatalog : NSObject {
	
/* elements */
	NSNumber * workflowCatalogId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_CheckExistWorkflowCatalog *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * workflowCatalogId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_CheckExistWorkflowCatalogResponse : NSObject {
	
/* elements */
	USBoolean * CheckExistWorkflowCatalogResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_CheckExistWorkflowCatalogResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * CheckExistWorkflowCatalogResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetProcessFileById : NSObject {
	
/* elements */
	NSNumber * processFileId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetProcessFileById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * processFileId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetProcessFileByIdResponse : NSObject {
	
/* elements */
	wf_tns3_WorkflowProcessFile * GetProcessFileByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetProcessFileByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) wf_tns3_WorkflowProcessFile * GetProcessFileByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetGroupsOfWorkflowCatalog : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetGroupsOfWorkflowCatalog *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetGroupsOfWorkflowCatalogResponse : NSObject {
	
/* elements */
	wf_tns3_ArrayOfWorkflowCatalog * GetGroupsOfWorkflowCatalogResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetGroupsOfWorkflowCatalogResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) wf_tns3_ArrayOfWorkflowCatalog * GetGroupsOfWorkflowCatalogResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetWorkflowCatalogByID : NSObject {
	
/* elements */
	NSNumber * wfCatalogID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetWorkflowCatalogByID *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * wfCatalogID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetWorkflowCatalogByIDResponse : NSObject {
	
/* elements */
	wf_tns3_WorkflowCatalog * GetWorkflowCatalogByIDResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetWorkflowCatalogByIDResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) wf_tns3_WorkflowCatalog * GetWorkflowCatalogByIDResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_WriteLog : NSObject {
	
/* elements */
	NSString * name;
	NSString * message;
	NSString * type;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_WriteLog *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * name;
@property (retain) NSString * message;
@property (retain) NSString * type;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_WriteLogResponse : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_WriteLogResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_DownloadFile : NSObject {
	
/* elements */
	NSString * fileName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_DownloadFile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * fileName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_DownloadFileResponse : NSObject {
	
/* elements */
	NSData * DownloadFileResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_DownloadFileResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSData * DownloadFileResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetLastRecordInProcessFile : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetLastRecordInProcessFile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetLastRecordInProcessFileResponse : NSObject {
	
/* elements */
	NSNumber * GetLastRecordInProcessFileResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetLastRecordInProcessFileResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetLastRecordInProcessFileResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_EditWorkflowCatalog : NSObject {
	
/* elements */
	wf_tns3_WorkflowCatalog * wfCatalog;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_EditWorkflowCatalog *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) wf_tns3_WorkflowCatalog * wfCatalog;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_EditWorkflowCatalogResponse : NSObject {
	
/* elements */
	NSString * EditWorkflowCatalogResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_EditWorkflowCatalogResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * EditWorkflowCatalogResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_DeleteWorkflowCatalog : NSObject {
	
/* elements */
	NSNumber * workflowCatalogID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_DeleteWorkflowCatalog *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * workflowCatalogID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_DeleteWorkflowCatalogResponse : NSObject {
	
/* elements */
	NSString * DeleteWorkflowCatalogResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_DeleteWorkflowCatalogResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * DeleteWorkflowCatalogResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_CheckExistWFCode : NSObject {
	
/* elements */
	NSString * wfCatalogCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_CheckExistWFCode *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * wfCatalogCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_CheckExistWFCodeResponse : NSObject {
	
/* elements */
	USBoolean * CheckExistWFCodeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_CheckExistWFCodeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * CheckExistWFCodeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetListFileByWorkflowCatalog : NSObject {
	
/* elements */
	NSNumber * workflowCatalogID;
	NSNumber * skip;
	NSNumber * take;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetListFileByWorkflowCatalog *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * workflowCatalogID;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetListFileByWorkflowCatalogResponse : NSObject {
	
/* elements */
	wf_tns3_ArrayOfV_Process * GetListFileByWorkflowCatalogResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetListFileByWorkflowCatalogResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) wf_tns3_ArrayOfV_Process * GetListFileByWorkflowCatalogResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetTotalProcessHistoryFile : NSObject {
	
/* elements */
	NSNumber * workflowCatalogID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetTotalProcessHistoryFile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * workflowCatalogID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetTotalProcessHistoryFileResponse : NSObject {
	
/* elements */
	NSNumber * GetTotalProcessHistoryFileResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetTotalProcessHistoryFileResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetTotalProcessHistoryFileResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_ActiveProcess : NSObject {
	
/* elements */
	NSNumber * processFileID;
	NSNumber * workflowCatalogID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_ActiveProcess *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * processFileID;
@property (retain) NSNumber * workflowCatalogID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_ActiveProcessResponse : NSObject {
	
/* elements */
	NSString * ActiveProcessResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_ActiveProcessResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ActiveProcessResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_AddWorkflowProcess : NSObject {
	
/* elements */
	wf_tns3_WorkflowProcess * workflowProcess;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_AddWorkflowProcess *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) wf_tns3_WorkflowProcess * workflowProcess;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_AddWorkflowProcessResponse : NSObject {
	
/* elements */
	NSString * AddWorkflowProcessResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_AddWorkflowProcessResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AddWorkflowProcessResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_CheckWorkflowFinish : NSObject {
	
/* elements */
	NSString * processGuid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_CheckWorkflowFinish *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * processGuid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_CheckWorkflowFinishResponse : NSObject {
	
/* elements */
	USBoolean * CheckWorkflowFinishResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_CheckWorkflowFinishResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * CheckWorkflowFinishResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetTotalTrackingRecords : NSObject {
	
/* elements */
	NSString * processGuid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetTotalTrackingRecords *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * processGuid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetTotalTrackingRecordsResponse : NSObject {
	
/* elements */
	NSNumber * GetTotalTrackingRecordsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetTotalTrackingRecordsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetTotalTrackingRecordsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetCurrentLastStep : NSObject {
	
/* elements */
	NSString * processGuid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetCurrentLastStep *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * processGuid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetCurrentLastStepResponse : NSObject {
	
/* elements */
	NSNumber * GetCurrentLastStepResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetCurrentLastStepResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetCurrentLastStepResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalog : NSObject {
	
/* elements */
	NSNumber * workflowCatalogID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalog *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * workflowCatalogID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalogResponse : NSObject {
	
/* elements */
	NSNumber * GetTotalWorkflowTrackingByWorkflowCatalogResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalogResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetTotalWorkflowTrackingByWorkflowCatalogResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetTotalWorkflowTrackingByWFCatAndOrg : NSObject {
	
/* elements */
	NSNumber * workflowCatalogID;
	NSNumber * orgUnitId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetTotalWorkflowTrackingByWFCatAndOrg *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * workflowCatalogID;
@property (retain) NSNumber * orgUnitId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetTotalWorkflowTrackingByWFCatAndOrgResponse : NSObject {
	
/* elements */
	NSNumber * GetTotalWorkflowTrackingByWFCatAndOrgResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetTotalWorkflowTrackingByWFCatAndOrgResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetTotalWorkflowTrackingByWFCatAndOrgResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalogAndStatus : NSObject {
	
/* elements */
	NSNumber * workflowCatalogID;
	USBoolean * isFinished;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalogAndStatus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * workflowCatalogID;
@property (retain) USBoolean * isFinished;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalogAndStatusResponse : NSObject {
	
/* elements */
	NSNumber * GetTotalWorkflowTrackingByWorkflowCatalogAndStatusResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalogAndStatusResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetTotalWorkflowTrackingByWorkflowCatalogAndStatusResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalogAndOrgAndStatus : NSObject {
	
/* elements */
	NSNumber * workflowCatalogID;
	NSNumber * orgUnitID;
	USBoolean * isFinished;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalogAndOrgAndStatus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * workflowCatalogID;
@property (retain) NSNumber * orgUnitID;
@property (retain) USBoolean * isFinished;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalogAndOrgAndStatusResponse : NSObject {
	
/* elements */
	NSNumber * GetTotalWorkflowTrackingByWorkflowCatalogAndOrgAndStatusResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalogAndOrgAndStatusResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetTotalWorkflowTrackingByWorkflowCatalogAndOrgAndStatusResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetTotalAllWorkflowTracking : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetTotalAllWorkflowTracking *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetTotalAllWorkflowTrackingResponse : NSObject {
	
/* elements */
	NSNumber * GetTotalAllWorkflowTrackingResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetTotalAllWorkflowTrackingResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetTotalAllWorkflowTrackingResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetTotalAllWorkflowTrackingByOrg : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetTotalAllWorkflowTrackingByOrg *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetTotalAllWorkflowTrackingByOrgResponse : NSObject {
	
/* elements */
	NSNumber * GetTotalAllWorkflowTrackingByOrgResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetTotalAllWorkflowTrackingByOrgResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetTotalAllWorkflowTrackingByOrgResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetTotalAllWorkflowTrackingByStatus : NSObject {
	
/* elements */
	USBoolean * isFinished;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetTotalAllWorkflowTrackingByStatus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * isFinished;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetTotalAllWorkflowTrackingByStatusResponse : NSObject {
	
/* elements */
	NSNumber * GetTotalAllWorkflowTrackingByStatusResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetTotalAllWorkflowTrackingByStatusResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetTotalAllWorkflowTrackingByStatusResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetTotalAllWorkflowTrackingByOrgAndStatus : NSObject {
	
/* elements */
	NSNumber * orgUnitID;
	USBoolean * isFinished;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetTotalAllWorkflowTrackingByOrgAndStatus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitID;
@property (retain) USBoolean * isFinished;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetTotalAllWorkflowTrackingByOrgAndStatusResponse : NSObject {
	
/* elements */
	NSNumber * GetTotalAllWorkflowTrackingByOrgAndStatusResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetTotalAllWorkflowTrackingByOrgAndStatusResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetTotalAllWorkflowTrackingByOrgAndStatusResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_StartProcess : NSObject {
	
/* elements */
	NSString * bussinessProcessName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_StartProcess *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * bussinessProcessName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_StartProcessResponse : NSObject {
	
/* elements */
	NSString * StartProcessResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_StartProcessResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * StartProcessResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_ResumeBookmarkAfterStart : NSObject {
	
/* elements */
	NSString * id_;
	NSString * data;
	NSString * typeName;
	NSString * bussinessProcessName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_ResumeBookmarkAfterStart *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * id_;
@property (retain) NSString * data;
@property (retain) NSString * typeName;
@property (retain) NSString * bussinessProcessName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_ResumeBookmarkAfterStartResponse : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_ResumeBookmarkAfterStartResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_ResumeBookmarkWithFunction : NSObject {
	
/* elements */
	NSString * id_;
	NSString * data;
	NSString * typeName;
	NSString * bussinessProcessName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_ResumeBookmarkWithFunction *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * id_;
@property (retain) NSString * data;
@property (retain) NSString * typeName;
@property (retain) NSString * bussinessProcessName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_ResumeBookmarkWithFunctionResponse : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_ResumeBookmarkWithFunctionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_ResumeWorkflow : NSObject {
	
/* elements */
	NSString * id_;
	NSString * data;
	NSString * typeName;
	NSString * bussinessProcessName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_ResumeWorkflow *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * id_;
@property (retain) NSString * data;
@property (retain) NSString * typeName;
@property (retain) NSString * bussinessProcessName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_ResumeWorkflowResponse : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_ResumeWorkflowResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_ResumeBookmark : NSObject {
	
/* elements */
	NSString * id_;
	NSString * data;
	NSString * typeName;
	NSString * bussinessProcessName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_ResumeBookmark *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * id_;
@property (retain) NSString * data;
@property (retain) NSString * typeName;
@property (retain) NSString * bussinessProcessName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_ResumeBookmarkResponse : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_ResumeBookmarkResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_ResumeBookmarkWithDictionary : NSObject {
	
/* elements */
	NSString * id_;
	NSString * data;
	NSString * typeName;
	NSString * bussinessProcessName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_ResumeBookmarkWithDictionary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * id_;
@property (retain) NSString * data;
@property (retain) NSString * typeName;
@property (retain) NSString * bussinessProcessName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_ResumeBookmarkWithDictionaryResponse : NSObject {
	
/* elements */
	NSNumber * ResumeBookmarkWithDictionaryResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_ResumeBookmarkWithDictionaryResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ResumeBookmarkWithDictionaryResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetProcessByFunction : NSObject {
	
/* elements */
	NSNumber * functionId;
	NSString * functionName;
	NSNumber * workflowCatalogId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetProcessByFunction *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * functionId;
@property (retain) NSString * functionName;
@property (retain) NSNumber * workflowCatalogId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetProcessByFunctionResponse : NSObject {
	
/* elements */
	NSString * GetProcessByFunctionResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetProcessByFunctionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * GetProcessByFunctionResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetProcessByFunctionAndWorkflowCode : NSObject {
	
/* elements */
	NSNumber * functionId;
	NSString * functionName;
	NSString * workflowCatalogCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetProcessByFunctionAndWorkflowCode *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * functionId;
@property (retain) NSString * functionName;
@property (retain) NSString * workflowCatalogCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface WorkflowServicesSvc_GetProcessByFunctionAndWorkflowCodeResponse : NSObject {
	
/* elements */
	NSString * GetProcessByFunctionAndWorkflowCodeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (WorkflowServicesSvc_GetProcessByFunctionAndWorkflowCodeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * GetProcessByFunctionAndWorkflowCodeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
/* Cookies handling provided by http://en.wikibooks.org/wiki/Programming:WebObjects/Web_Services/Web_Service_Provider */
#import <libxml/parser.h>
#import "xs.h"
#import "WorkflowServicesSvc.h"
#import "ns1.h"
#import "wf_tns1.h"
#import "wf_tns2.h"
#import "wf_tns3.h"
@class BasicHttpBinding_IWorkflowServicesBinding;
@interface WorkflowServicesSvc : NSObject {
	
}
+ (BasicHttpBinding_IWorkflowServicesBinding *)BasicHttpBinding_IWorkflowServicesBinding;
@end
@class BasicHttpBinding_IWorkflowServicesBindingResponse;
@class BasicHttpBinding_IWorkflowServicesBindingOperation;
@protocol BasicHttpBinding_IWorkflowServicesBindingResponseDelegate <NSObject>
- (void) operation:(BasicHttpBinding_IWorkflowServicesBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IWorkflowServicesBindingResponse *)response;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding : NSObject <BasicHttpBinding_IWorkflowServicesBindingResponseDelegate> {
	NSURL *address;
	NSTimeInterval defaultTimeout;
	NSMutableArray *cookies;
	BOOL logXMLInOut;
	BOOL synchronousOperationComplete;
	NSString *authUsername;
	NSString *authPassword;
}
@property (copy) NSURL *address;
@property (assign) BOOL logXMLInOut;
@property (assign) NSTimeInterval defaultTimeout;
@property (nonatomic, retain) NSMutableArray *cookies;
@property (nonatomic, retain) NSString *authUsername;
@property (nonatomic, retain) NSString *authPassword;
- (id)initWithAddress:(NSString *)anAddress;
- (void)sendHTTPCallUsingBody:(NSString *)body soapAction:(NSString *)soapAction forOperation:(BasicHttpBinding_IWorkflowServicesBindingOperation *)operation;
- (void)addCookie:(NSHTTPCookie *)toAdd;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)StartUsingParameters:(WorkflowServicesSvc_Start *)aParameters ;
- (void)StartAsyncUsingParameters:(WorkflowServicesSvc_Start *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)UnloadUsingParameters:(WorkflowServicesSvc_Unload *)aParameters ;
- (void)UnloadAsyncUsingParameters:(WorkflowServicesSvc_Unload *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)GetBookmarkUsingParameters:(WorkflowServicesSvc_GetBookmark *)aParameters ;
- (void)GetBookmarkAsyncUsingParameters:(WorkflowServicesSvc_GetBookmark *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)LoadUsingParameters:(WorkflowServicesSvc_Load *)aParameters ;
- (void)LoadAsyncUsingParameters:(WorkflowServicesSvc_Load *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)UploadFileUsingParameters:(WorkflowServicesSvc_FileUpload *)aParameters FileName:(NSString *)aFileName ;
- (void)UploadFileAsyncUsingParameters:(WorkflowServicesSvc_FileUpload *)aParameters FileName:(NSString *)aFileName  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)GetListFileUsingParameters:(WorkflowServicesSvc_GetListFile *)aParameters ;
- (void)GetListFileAsyncUsingParameters:(WorkflowServicesSvc_GetListFile *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)CheckFileExistUsingParameters:(WorkflowServicesSvc_CheckFileExist *)aParameters ;
- (void)CheckFileExistAsyncUsingParameters:(WorkflowServicesSvc_CheckFileExist *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)GetWorkflowCatalogUsingParameters:(WorkflowServicesSvc_GetWorkflowCatalog *)aParameters ;
- (void)GetWorkflowCatalogAsyncUsingParameters:(WorkflowServicesSvc_GetWorkflowCatalog *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)AddProcessFileUsingParameters:(WorkflowServicesSvc_AddProcessFile *)aParameters ;
- (void)AddProcessFileAsyncUsingParameters:(WorkflowServicesSvc_AddProcessFile *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)EditProcessFileUsingParameters:(WorkflowServicesSvc_EditProcessFile *)aParameters ;
- (void)EditProcessFileAsyncUsingParameters:(WorkflowServicesSvc_EditProcessFile *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)DeleteProcessFileUsingParameters:(WorkflowServicesSvc_DeleteProcessFile *)aParameters ;
- (void)DeleteProcessFileAsyncUsingParameters:(WorkflowServicesSvc_DeleteProcessFile *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)AddWorkflowCatalogUsingParameters:(WorkflowServicesSvc_AddWorkflowCatalog *)aParameters ;
- (void)AddWorkflowCatalogAsyncUsingParameters:(WorkflowServicesSvc_AddWorkflowCatalog *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)GetProcessByCatalogIdUsingParameters:(WorkflowServicesSvc_GetProcessByCatalogId *)aParameters ;
- (void)GetProcessByCatalogIdAsyncUsingParameters:(WorkflowServicesSvc_GetProcessByCatalogId *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)CheckExistWorkflowCatalogUsingParameters:(WorkflowServicesSvc_CheckExistWorkflowCatalog *)aParameters ;
- (void)CheckExistWorkflowCatalogAsyncUsingParameters:(WorkflowServicesSvc_CheckExistWorkflowCatalog *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)GetProcessFileByIdUsingParameters:(WorkflowServicesSvc_GetProcessFileById *)aParameters ;
- (void)GetProcessFileByIdAsyncUsingParameters:(WorkflowServicesSvc_GetProcessFileById *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)GetGroupsOfWorkflowCatalogUsingParameters:(WorkflowServicesSvc_GetGroupsOfWorkflowCatalog *)aParameters ;
- (void)GetGroupsOfWorkflowCatalogAsyncUsingParameters:(WorkflowServicesSvc_GetGroupsOfWorkflowCatalog *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)GetWorkflowCatalogByIDUsingParameters:(WorkflowServicesSvc_GetWorkflowCatalogByID *)aParameters ;
- (void)GetWorkflowCatalogByIDAsyncUsingParameters:(WorkflowServicesSvc_GetWorkflowCatalogByID *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)WriteLogUsingParameters:(WorkflowServicesSvc_WriteLog *)aParameters ;
- (void)WriteLogAsyncUsingParameters:(WorkflowServicesSvc_WriteLog *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)DownloadFileUsingParameters:(WorkflowServicesSvc_DownloadFile *)aParameters ;
- (void)DownloadFileAsyncUsingParameters:(WorkflowServicesSvc_DownloadFile *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)GetLastRecordInProcessFileUsingParameters:(WorkflowServicesSvc_GetLastRecordInProcessFile *)aParameters ;
- (void)GetLastRecordInProcessFileAsyncUsingParameters:(WorkflowServicesSvc_GetLastRecordInProcessFile *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)EditWorkflowCatalogUsingParameters:(WorkflowServicesSvc_EditWorkflowCatalog *)aParameters ;
- (void)EditWorkflowCatalogAsyncUsingParameters:(WorkflowServicesSvc_EditWorkflowCatalog *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)DeleteWorkflowCatalogUsingParameters:(WorkflowServicesSvc_DeleteWorkflowCatalog *)aParameters ;
- (void)DeleteWorkflowCatalogAsyncUsingParameters:(WorkflowServicesSvc_DeleteWorkflowCatalog *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)CheckExistWFCodeUsingParameters:(WorkflowServicesSvc_CheckExistWFCode *)aParameters ;
- (void)CheckExistWFCodeAsyncUsingParameters:(WorkflowServicesSvc_CheckExistWFCode *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)GetListFileByWorkflowCatalogUsingParameters:(WorkflowServicesSvc_GetListFileByWorkflowCatalog *)aParameters ;
- (void)GetListFileByWorkflowCatalogAsyncUsingParameters:(WorkflowServicesSvc_GetListFileByWorkflowCatalog *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)GetTotalProcessHistoryFileUsingParameters:(WorkflowServicesSvc_GetTotalProcessHistoryFile *)aParameters ;
- (void)GetTotalProcessHistoryFileAsyncUsingParameters:(WorkflowServicesSvc_GetTotalProcessHistoryFile *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)ActiveProcessUsingParameters:(WorkflowServicesSvc_ActiveProcess *)aParameters ;
- (void)ActiveProcessAsyncUsingParameters:(WorkflowServicesSvc_ActiveProcess *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)AddWorkflowProcessUsingParameters:(WorkflowServicesSvc_AddWorkflowProcess *)aParameters ;
- (void)AddWorkflowProcessAsyncUsingParameters:(WorkflowServicesSvc_AddWorkflowProcess *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)CheckWorkflowFinishUsingParameters:(WorkflowServicesSvc_CheckWorkflowFinish *)aParameters ;
- (void)CheckWorkflowFinishAsyncUsingParameters:(WorkflowServicesSvc_CheckWorkflowFinish *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)GetTotalTrackingRecordsUsingParameters:(WorkflowServicesSvc_GetTotalTrackingRecords *)aParameters ;
- (void)GetTotalTrackingRecordsAsyncUsingParameters:(WorkflowServicesSvc_GetTotalTrackingRecords *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)GetCurrentLastStepUsingParameters:(WorkflowServicesSvc_GetCurrentLastStep *)aParameters ;
- (void)GetCurrentLastStepAsyncUsingParameters:(WorkflowServicesSvc_GetCurrentLastStep *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)GetTotalWorkflowTrackingByWorkflowCatalogUsingParameters:(WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalog *)aParameters ;
- (void)GetTotalWorkflowTrackingByWorkflowCatalogAsyncUsingParameters:(WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalog *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)GetTotalWorkflowTrackingByWFCatAndOrgUsingParameters:(WorkflowServicesSvc_GetTotalWorkflowTrackingByWFCatAndOrg *)aParameters ;
- (void)GetTotalWorkflowTrackingByWFCatAndOrgAsyncUsingParameters:(WorkflowServicesSvc_GetTotalWorkflowTrackingByWFCatAndOrg *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)GetTotalWorkflowTrackingByWorkflowCatalogAndStatusUsingParameters:(WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalogAndStatus *)aParameters ;
- (void)GetTotalWorkflowTrackingByWorkflowCatalogAndStatusAsyncUsingParameters:(WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalogAndStatus *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)GetTotalWorkflowTrackingByWorkflowCatalogAndOrgAndStatusUsingParameters:(WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalogAndOrgAndStatus *)aParameters ;
- (void)GetTotalWorkflowTrackingByWorkflowCatalogAndOrgAndStatusAsyncUsingParameters:(WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalogAndOrgAndStatus *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)GetTotalAllWorkflowTrackingUsingParameters:(WorkflowServicesSvc_GetTotalAllWorkflowTracking *)aParameters ;
- (void)GetTotalAllWorkflowTrackingAsyncUsingParameters:(WorkflowServicesSvc_GetTotalAllWorkflowTracking *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)GetTotalAllWorkflowTrackingByOrgUsingParameters:(WorkflowServicesSvc_GetTotalAllWorkflowTrackingByOrg *)aParameters ;
- (void)GetTotalAllWorkflowTrackingByOrgAsyncUsingParameters:(WorkflowServicesSvc_GetTotalAllWorkflowTrackingByOrg *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)GetTotalAllWorkflowTrackingByStatusUsingParameters:(WorkflowServicesSvc_GetTotalAllWorkflowTrackingByStatus *)aParameters ;
- (void)GetTotalAllWorkflowTrackingByStatusAsyncUsingParameters:(WorkflowServicesSvc_GetTotalAllWorkflowTrackingByStatus *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)GetTotalAllWorkflowTrackingByOrgAndStatusUsingParameters:(WorkflowServicesSvc_GetTotalAllWorkflowTrackingByOrgAndStatus *)aParameters ;
- (void)GetTotalAllWorkflowTrackingByOrgAndStatusAsyncUsingParameters:(WorkflowServicesSvc_GetTotalAllWorkflowTrackingByOrgAndStatus *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)StartProcessUsingParameters:(WorkflowServicesSvc_StartProcess *)aParameters ;
- (void)StartProcessAsyncUsingParameters:(WorkflowServicesSvc_StartProcess *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)ResumeBookmarkAfterStartUsingParameters:(WorkflowServicesSvc_ResumeBookmarkAfterStart *)aParameters ;
- (void)ResumeBookmarkAfterStartAsyncUsingParameters:(WorkflowServicesSvc_ResumeBookmarkAfterStart *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)ResumeBookmarkWithFunctionUsingParameters:(WorkflowServicesSvc_ResumeBookmarkWithFunction *)aParameters ;
- (void)ResumeBookmarkWithFunctionAsyncUsingParameters:(WorkflowServicesSvc_ResumeBookmarkWithFunction *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)ResumeWorkflowUsingParameters:(WorkflowServicesSvc_ResumeWorkflow *)aParameters ;
- (void)ResumeWorkflowAsyncUsingParameters:(WorkflowServicesSvc_ResumeWorkflow *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)ResumeBookmarkUsingParameters:(WorkflowServicesSvc_ResumeBookmark *)aParameters ;
- (void)ResumeBookmarkAsyncUsingParameters:(WorkflowServicesSvc_ResumeBookmark *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)ResumeBookmarkWithDictionaryUsingParameters:(WorkflowServicesSvc_ResumeBookmarkWithDictionary *)aParameters ;
- (void)ResumeBookmarkWithDictionaryAsyncUsingParameters:(WorkflowServicesSvc_ResumeBookmarkWithDictionary *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)GetProcessByFunctionUsingParameters:(WorkflowServicesSvc_GetProcessByFunction *)aParameters ;
- (void)GetProcessByFunctionAsyncUsingParameters:(WorkflowServicesSvc_GetProcessByFunction *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IWorkflowServicesBindingResponse *)GetProcessByFunctionAndWorkflowCodeUsingParameters:(WorkflowServicesSvc_GetProcessByFunctionAndWorkflowCode *)aParameters ;
- (void)GetProcessByFunctionAndWorkflowCodeAsyncUsingParameters:(WorkflowServicesSvc_GetProcessByFunctionAndWorkflowCode *)aParameters  delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)responseDelegate;
@end
@interface BasicHttpBinding_IWorkflowServicesBindingOperation : NSOperation {
	BasicHttpBinding_IWorkflowServicesBinding *binding;
	BasicHttpBinding_IWorkflowServicesBindingResponse *response;
	id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate> delegate;
	NSMutableData *responseData;
	NSURLConnection *urlConnection;
}
@property (retain) BasicHttpBinding_IWorkflowServicesBinding *binding;
@property (readonly) BasicHttpBinding_IWorkflowServicesBindingResponse *response;
@property (nonatomic, assign) id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate> delegate;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, retain) NSURLConnection *urlConnection;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_Start : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_Start * parameters;
}
@property (retain) WorkflowServicesSvc_Start * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_Start *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_Unload : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_Unload * parameters;
}
@property (retain) WorkflowServicesSvc_Unload * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_Unload *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_GetBookmark : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_GetBookmark * parameters;
}
@property (retain) WorkflowServicesSvc_GetBookmark * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_GetBookmark *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_Load : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_Load * parameters;
}
@property (retain) WorkflowServicesSvc_Load * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_Load *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_UploadFile : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_FileUpload * parameters;
	NSString * FileName;
}
@property (retain) WorkflowServicesSvc_FileUpload * parameters;
@property (retain) NSString * FileName;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_FileUpload *)aParameters
	FileName:(NSString *)aFileName
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_GetListFile : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_GetListFile * parameters;
}
@property (retain) WorkflowServicesSvc_GetListFile * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_GetListFile *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_CheckFileExist : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_CheckFileExist * parameters;
}
@property (retain) WorkflowServicesSvc_CheckFileExist * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_CheckFileExist *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_GetWorkflowCatalog : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_GetWorkflowCatalog * parameters;
}
@property (retain) WorkflowServicesSvc_GetWorkflowCatalog * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_GetWorkflowCatalog *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_AddProcessFile : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_AddProcessFile * parameters;
}
@property (retain) WorkflowServicesSvc_AddProcessFile * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_AddProcessFile *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_EditProcessFile : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_EditProcessFile * parameters;
}
@property (retain) WorkflowServicesSvc_EditProcessFile * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_EditProcessFile *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_DeleteProcessFile : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_DeleteProcessFile * parameters;
}
@property (retain) WorkflowServicesSvc_DeleteProcessFile * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_DeleteProcessFile *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_AddWorkflowCatalog : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_AddWorkflowCatalog * parameters;
}
@property (retain) WorkflowServicesSvc_AddWorkflowCatalog * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_AddWorkflowCatalog *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_GetProcessByCatalogId : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_GetProcessByCatalogId * parameters;
}
@property (retain) WorkflowServicesSvc_GetProcessByCatalogId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_GetProcessByCatalogId *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_CheckExistWorkflowCatalog : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_CheckExistWorkflowCatalog * parameters;
}
@property (retain) WorkflowServicesSvc_CheckExistWorkflowCatalog * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_CheckExistWorkflowCatalog *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_GetProcessFileById : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_GetProcessFileById * parameters;
}
@property (retain) WorkflowServicesSvc_GetProcessFileById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_GetProcessFileById *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_GetGroupsOfWorkflowCatalog : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_GetGroupsOfWorkflowCatalog * parameters;
}
@property (retain) WorkflowServicesSvc_GetGroupsOfWorkflowCatalog * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_GetGroupsOfWorkflowCatalog *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_GetWorkflowCatalogByID : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_GetWorkflowCatalogByID * parameters;
}
@property (retain) WorkflowServicesSvc_GetWorkflowCatalogByID * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_GetWorkflowCatalogByID *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_WriteLog : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_WriteLog * parameters;
}
@property (retain) WorkflowServicesSvc_WriteLog * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_WriteLog *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_DownloadFile : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_DownloadFile * parameters;
}
@property (retain) WorkflowServicesSvc_DownloadFile * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_DownloadFile *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_GetLastRecordInProcessFile : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_GetLastRecordInProcessFile * parameters;
}
@property (retain) WorkflowServicesSvc_GetLastRecordInProcessFile * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_GetLastRecordInProcessFile *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_EditWorkflowCatalog : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_EditWorkflowCatalog * parameters;
}
@property (retain) WorkflowServicesSvc_EditWorkflowCatalog * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_EditWorkflowCatalog *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_DeleteWorkflowCatalog : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_DeleteWorkflowCatalog * parameters;
}
@property (retain) WorkflowServicesSvc_DeleteWorkflowCatalog * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_DeleteWorkflowCatalog *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_CheckExistWFCode : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_CheckExistWFCode * parameters;
}
@property (retain) WorkflowServicesSvc_CheckExistWFCode * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_CheckExistWFCode *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_GetListFileByWorkflowCatalog : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_GetListFileByWorkflowCatalog * parameters;
}
@property (retain) WorkflowServicesSvc_GetListFileByWorkflowCatalog * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_GetListFileByWorkflowCatalog *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_GetTotalProcessHistoryFile : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_GetTotalProcessHistoryFile * parameters;
}
@property (retain) WorkflowServicesSvc_GetTotalProcessHistoryFile * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_GetTotalProcessHistoryFile *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_ActiveProcess : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_ActiveProcess * parameters;
}
@property (retain) WorkflowServicesSvc_ActiveProcess * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_ActiveProcess *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_AddWorkflowProcess : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_AddWorkflowProcess * parameters;
}
@property (retain) WorkflowServicesSvc_AddWorkflowProcess * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_AddWorkflowProcess *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_CheckWorkflowFinish : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_CheckWorkflowFinish * parameters;
}
@property (retain) WorkflowServicesSvc_CheckWorkflowFinish * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_CheckWorkflowFinish *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_GetTotalTrackingRecords : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_GetTotalTrackingRecords * parameters;
}
@property (retain) WorkflowServicesSvc_GetTotalTrackingRecords * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_GetTotalTrackingRecords *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_GetCurrentLastStep : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_GetCurrentLastStep * parameters;
}
@property (retain) WorkflowServicesSvc_GetCurrentLastStep * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_GetCurrentLastStep *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_GetTotalWorkflowTrackingByWorkflowCatalog : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalog * parameters;
}
@property (retain) WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalog * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalog *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_GetTotalWorkflowTrackingByWFCatAndOrg : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_GetTotalWorkflowTrackingByWFCatAndOrg * parameters;
}
@property (retain) WorkflowServicesSvc_GetTotalWorkflowTrackingByWFCatAndOrg * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_GetTotalWorkflowTrackingByWFCatAndOrg *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_GetTotalWorkflowTrackingByWorkflowCatalogAndStatus : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalogAndStatus * parameters;
}
@property (retain) WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalogAndStatus * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalogAndStatus *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_GetTotalWorkflowTrackingByWorkflowCatalogAndOrgAndStatus : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalogAndOrgAndStatus * parameters;
}
@property (retain) WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalogAndOrgAndStatus * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_GetTotalWorkflowTrackingByWorkflowCatalogAndOrgAndStatus *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_GetTotalAllWorkflowTracking : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_GetTotalAllWorkflowTracking * parameters;
}
@property (retain) WorkflowServicesSvc_GetTotalAllWorkflowTracking * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_GetTotalAllWorkflowTracking *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_GetTotalAllWorkflowTrackingByOrg : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_GetTotalAllWorkflowTrackingByOrg * parameters;
}
@property (retain) WorkflowServicesSvc_GetTotalAllWorkflowTrackingByOrg * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_GetTotalAllWorkflowTrackingByOrg *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_GetTotalAllWorkflowTrackingByStatus : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_GetTotalAllWorkflowTrackingByStatus * parameters;
}
@property (retain) WorkflowServicesSvc_GetTotalAllWorkflowTrackingByStatus * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_GetTotalAllWorkflowTrackingByStatus *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_GetTotalAllWorkflowTrackingByOrgAndStatus : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_GetTotalAllWorkflowTrackingByOrgAndStatus * parameters;
}
@property (retain) WorkflowServicesSvc_GetTotalAllWorkflowTrackingByOrgAndStatus * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_GetTotalAllWorkflowTrackingByOrgAndStatus *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_StartProcess : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_StartProcess * parameters;
}
@property (retain) WorkflowServicesSvc_StartProcess * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_StartProcess *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_ResumeBookmarkAfterStart : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_ResumeBookmarkAfterStart * parameters;
}
@property (retain) WorkflowServicesSvc_ResumeBookmarkAfterStart * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_ResumeBookmarkAfterStart *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_ResumeBookmarkWithFunction : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_ResumeBookmarkWithFunction * parameters;
}
@property (retain) WorkflowServicesSvc_ResumeBookmarkWithFunction * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_ResumeBookmarkWithFunction *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_ResumeWorkflow : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_ResumeWorkflow * parameters;
}
@property (retain) WorkflowServicesSvc_ResumeWorkflow * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_ResumeWorkflow *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_ResumeBookmark : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_ResumeBookmark * parameters;
}
@property (retain) WorkflowServicesSvc_ResumeBookmark * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_ResumeBookmark *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_ResumeBookmarkWithDictionary : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_ResumeBookmarkWithDictionary * parameters;
}
@property (retain) WorkflowServicesSvc_ResumeBookmarkWithDictionary * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_ResumeBookmarkWithDictionary *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_GetProcessByFunction : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_GetProcessByFunction * parameters;
}
@property (retain) WorkflowServicesSvc_GetProcessByFunction * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_GetProcessByFunction *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_GetProcessByFunctionAndWorkflowCode : BasicHttpBinding_IWorkflowServicesBindingOperation {
	WorkflowServicesSvc_GetProcessByFunctionAndWorkflowCode * parameters;
}
@property (retain) WorkflowServicesSvc_GetProcessByFunctionAndWorkflowCode * parameters;
- (id)initWithBinding:(BasicHttpBinding_IWorkflowServicesBinding *)aBinding delegate:(id<BasicHttpBinding_IWorkflowServicesBindingResponseDelegate>)aDelegate
	parameters:(WorkflowServicesSvc_GetProcessByFunctionAndWorkflowCode *)aParameters
;
@end
@interface BasicHttpBinding_IWorkflowServicesBinding_envelope : NSObject {
}
+ (BasicHttpBinding_IWorkflowServicesBinding_envelope *)sharedInstance;
- (NSString *)serializedFormUsingHeaderElements:(NSDictionary *)headerElements bodyElements:(NSDictionary *)bodyElements;
@end
@interface BasicHttpBinding_IWorkflowServicesBindingResponse : NSObject {
	NSArray *headers;
	NSArray *bodyParts;
	NSError *error;
}
@property (retain) NSArray *headers;
@property (retain) NSArray *bodyParts;
@property (retain) NSError *error;
@end
