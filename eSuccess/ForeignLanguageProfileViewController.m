//
//  ForeignLanguageProfileViewController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 6/26/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "ForeignLanguageProfileViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "DayLeaveDetailCell.h"
@interface ForeignLanguageProfileViewController ()

@end

@implementation ForeignLanguageProfileViewController
@synthesize foreginLanguageArr;
@synthesize degreeNameArr;
static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Trình độ ngoại ngữ";
    [self getData];
    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 130;
//}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([degreeNameArr count] > 0) {
        return [degreeNameArr objectAtIndex:section];
    }
    return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [foreginLanguageArr count] > 0 ? foreginLanguageArr.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [foreginLanguageArr count] > 0 ? 6 : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (foreginLanguageArr.count == 0)
        return [self getDefaultEmptyCell];
    
    DayLeaveDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
    if (cell == nil) {
        cell = [[DayLeaveDetailCell alloc]init];
    }
    cell.lbLeft.font = [UIFont systemFontOfSize:14];
    cell.lbLeft.backgroundColor = [UIColor clearColor];
    cell.lbRight.font = [UIFont boldSystemFontOfSize:16];
    cell.lbRight.backgroundColor = [UIColor clearColor];
    
    int section = indexPath.section;
    empl_tns1_V_EmpProfileForeignLanguage *language = [foreginLanguageArr objectAtIndex:section];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    if (indexPath.row == 0) {
        cell.lbLeft.text = @"Ngôn ngữ";
        cell.lbRight.text = language.LanguageName;
    }
    if (indexPath.row == 1) {
        cell.lbLeft.text = @"Ngày cấp";
        cell.lbRight.text = [formatter stringFromDate:language.DateOfIssue];
    }
    if (indexPath.row == 2) {
        cell.lbLeft.text = @"Xếp loại";
        cell.lbRight.text = language.OrgDegreeRankName;
    }
    if (indexPath.row == 3) {
        cell.lbLeft.text = @"Nơi đào tạo";
        cell.lbRight.text = language.TrainingCenter;
    }
    if (indexPath.row == 4) {
        cell.lbLeft.text = @"Nơi cấp";
        cell.lbRight.text = language.PlaceOfIssue;
    }
    if (indexPath.row == 5) {
        cell.lbLeft.text = @"Ghi chú";
        cell.lbRight.text = language.Note;
    }
    return cell;
}

#pragma get Data Degree
-(void)getData
{
    foreginLanguageArr = [[NSMutableArray alloc]init];
    degreeNameArr= [[NSMutableArray alloc]init];
    
    [self getEmpProfileForeignLanguageById];
}
-(void)getEmpProfileForeignLanguageById
{
    BasicHttpBinding_IEmpProfileLayerServiceBinding * binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
    EmpProfileLayerServiceSvc_GetViewEmpProfileForeignLanguageById *request = [[EmpProfileLayerServiceSvc_GetViewEmpProfileForeignLanguageById alloc] init];
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    if (app.otherEmpId != nil) {
        request.EmployeeId = app.otherEmpId;
    } else{
        request.employeeId = app.sysUser.EmployeeId ;
    }
    [binding GetViewEmpProfileForeignLanguageByIdAsyncUsingParameters:request delegate:self];
}


- (void)operation:(BasicHttpBinding_IEmpProfileLayerServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)response
{
    if ([response.bodyParts count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi kết nối" message:@"Không thể kết nối đến server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    for (id mine in response.bodyParts) {
        if ([mine isKindOfClass:[EmpProfileLayerServiceSvc_GetViewEmpProfileForeignLanguageByIdResponse class]]) {
            empl_tns1_ArrayOfV_EmpProfileForeignLanguage *result = [mine GetViewEmpProfileForeignLanguageByIdResult];
            foreginLanguageArr = result.V_EmpProfileForeignLanguage;
            for (empl_tns1_V_EmpProfileForeignLanguage *language in foreginLanguageArr) {
                [degreeNameArr addObject:language.DegreeName];
            }
            
            if (!self.viewDidDisappear)
                [self.tableView reloadData];
        }
        
        //        if ([mine isKindOfClass:[SOAPFault class]])
        //        {
        //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Server error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //            [alert show];
        //        }
    }
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}


@end
