//
//  GPPerspective.h
//  eSuccess
//
//  Created by admin on 6/6/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GPCompanyYearObject.h"
@interface GPPerspective : NSObject
@property(nonatomic) NSInteger gpPerspecId;
@property(nonatomic, retain) NSString *name;
@property(nonatomic, retain) NSMutableArray *coYearArr;

+(GPPerspective *)gpPerspectiveWithId:(NSInteger)ID name:(NSString *)name coYearArr:(NSMutableArray *)gpCoYearArr;
@end
