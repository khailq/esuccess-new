//
//  ProfilePanelView.m
//  eSuccess
//
//  Created by HPTVIETNAM on 5/7/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "ProfilePanelView.h"
#import <QuartzCore/QuartzCore.h>

@interface ProfilePanelView ()

@property (strong, nonatomic) UILabel *lbName;
@property (strong, nonatomic) UILabel *lbPosition;
@property (strong, nonatomic) UILabel *lbUnit;

@end

@implementation ProfilePanelView
{
    UIView *innerView;
    UIImageView *decorator;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
        self.backgroundColor = [UIColor clearColor];
        
        innerView = [[UIView alloc] initWithFrame:CGRectMake(6, 0, H_PanelWidth - 6, H_PanelHeight)];
        innerView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1.0];
        [self addSubview: innerView];
        
        _avatar = [[UIImageView alloc] initWithFrame:CGRectMake(3, 25, 67, 67)];
        _lbName = [[UILabel alloc] initWithFrame:CGRectMake(31, 0, 152, 21)];
        _lbPosition = [[UILabel alloc] initWithFrame:CGRectMake(73, 25, 113, 21)];
        _lbUnit = [[UILabel alloc] initWithFrame:CGRectMake(73, 54, 113, 21)];
        
        [innerView addSubview:_avatar];
        [innerView addSubview:_lbName];
        [innerView addSubview:_lbPosition];
        [innerView addSubview:_lbUnit];
        
        decorator = [[UIImageView alloc] initWithFrame:CGRectMake(0, 2, 34, 26)];
        [decorator setImage:[UIImage imageNamed:@"1.png"]];
        [self addSubview:decorator];
            
    
        [self.avatar setImage:[UIImage imageNamed:@"avatar.jpg"]];
        
        
        self.lbName.text = [self truncateName:@"Pham Minh Chien"];
        self.lbName.lineBreakMode = NSLineBreakByTruncatingMiddle;
        self.lbName.font = [UIFont boldSystemFontOfSize:15];
        self.lbName.backgroundColor = [UIColor clearColor];
        
        
        self.lbPosition.text = @"Developer";
        self.lbPosition.font = [UIFont systemFontOfSize:11];
        self.lbPosition.backgroundColor = [UIColor clearColor];
        self.lbPosition.lineBreakMode = NSLineBreakByWordWrapping;
        self.lbPosition.numberOfLines = 0;
        
        self.lbUnit.text  = @"Van phong HO";
        self.lbUnit.font = [UIFont systemFontOfSize:11];
        self.lbUnit.backgroundColor = [UIColor clearColor];
        self.lbUnit.lineBreakMode = NSLineBreakByWordWrapping;
        self.lbUnit.numberOfLines = 0;
        
        innerView.layer.borderWidth = 0.5f;
        innerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        
//        self.layer.borderColor = [UIColor whiteColor].CGColor;
//        self.layer.borderWidth = 0.5f;
//        self.layer.masksToBounds = NO;
//        self.layer.shadowOffset = CGSizeMake(7, 5);
//        self.layer.shadowRadius = 7;
//        self.layer.shadowOpacity = 0.5;
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]
                                                 initWithTarget:self action:@selector(respondToTapGesture:)];
        
        // Specify that the gesture must be a single tap
        tapRecognizer.numberOfTapsRequired = 1;
        
        // Add the tap gesture recognizer to the view
        [self addGestureRecognizer:tapRecognizer];
    }
    return self;
}

-(void)respondToTapGesture:(id)sender
{
    if (self.delegate != nil)
        [self.delegate touchInPanel:self];
}

-(void)setSelected:(BOOL)selected
{
    _selected = selected;
    if (_selected)
    {
        innerView.layer.borderWidth = 2.0f;
        innerView.layer.borderColor = [UIColor blueColor].CGColor;
    }
    else
    {
        innerView.layer.borderWidth = 0.5f;
        innerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }
}

-(NSString*)truncateName:(NSString*)name
{
    NSString *result = @"";
    NSArray *parts = [name componentsSeparatedByString:@" "];
    for (int i = 0; i < parts.count - 1; ++i) {
        char c = [parts[i] characterAtIndex:0];
        NSString *shorthern = [NSString stringWithFormat:@"%c", c];
        result = [result stringByAppendingFormat:@"%@.", shorthern];
    }
    
    result = [result stringByAppendingString:parts[parts.count - 1]];
    return result;
}

-(void)setChildName:(NSString *)name
{
    //self.lbName.text = [self truncateName:name];
    self.lbName.text = name;
}

-(void)setPositionname:(NSString *)position
{
    self.lbPosition.text = position;
    [self.lbPosition sizeToFit];
}

-(void)setUnitName:(NSString *)unitName
{
    self.lbUnit.text = unitName;
    [self.lbUnit sizeToFit];
}


@end
