//
//  OrgUnitView.h
//  eSuccess
//
//  Created by HPTVIETNAM on 5/18/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrgUnitView : UIView

@property (weak, nonatomic) IBOutlet UILabel *lbNumberOfEmployees;
@property (weak, nonatomic) IBOutlet UILabel *lbOrgName;

@end
