//
//  HealthStatusViewController.h
//  eSuccess
//
//  Created by admin on 5/4/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseDetailTableViewController.h"

@interface HealthStatusViewController : BaseDetailTableViewController

@property(nonatomic, retain) NSMutableArray *medicalArr;
@end
