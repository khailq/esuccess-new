//
//  UnitLogViewController.m
//  eSuccess
//
//  Created by admin on 5/11/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "UnitLogViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h" 
#import "DayLeaveDetailCell.h"
@interface UnitLogViewController ()

@end

@implementation UnitLogViewController
@synthesize processWorkArr;
@synthesize orgUnitNameArr;
static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Biến động phòng ban";
    [self getData];
    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 130;
//}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([orgUnitNameArr count] > 0) {
        return [orgUnitNameArr objectAtIndex:section];
    }
    return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [processWorkArr count] > 0 ? processWorkArr.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //return [profileArray count];
    if([processWorkArr count] > 0){
        return 7;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (processWorkArr.count == 0)
        return [self getDefaultEmptyCell];
    
    DayLeaveDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
    if (cell == nil) {
        cell = [[DayLeaveDetailCell alloc]init];
    }
    cell.lbLeft.font = [UIFont systemFontOfSize:14];
    cell.lbLeft.backgroundColor = [UIColor clearColor];
    cell.lbRight.font = [UIFont boldSystemFontOfSize:16];
    cell.lbRight.backgroundColor = [UIColor clearColor];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    int section = indexPath.section;

    empl_tns1_V_EmpProfileProcessOfWork *empProcess = processWorkArr[section];
    if (indexPath.row == 0) {
        cell.lbLeft.text = @"Công việc";
        cell.lbRight.text = empProcess.OrgJobName;
    }
    if (indexPath.row == 1) {
        cell.lbLeft.text = @"Vị trí";
        cell.lbRight.text = empProcess.OrgJobPositionName;
    }
    if (indexPath.row == 2) {
        cell.lbLeft.text = @"Loại nhân viên";
//        cell.lbRight.text = empProcess.EmployeeTypeName;
        cell.lbRight.text = @"Nhân viên chính thức";
    }
    if (indexPath.row == 3) {
        cell.lbLeft.text = @"Nơi làm việc";
        cell.lbRight.text = empProcess.OrgUnitAddress;
    }
    if (indexPath.row == 4) {
        cell.lbLeft.text = @"Ngày áp dụng";
        cell.lbRight.text = [formatter stringFromDate:empProcess.ImplementationDate];
    }
    if (indexPath.row == 5) {
        cell.lbLeft.text = @"Lý do";
        cell.lbRight.text = empProcess.Reason;
    }
    if (indexPath.row == 6) {
        cell.lbLeft.text = @"Quyết định";
        cell.lbRight.text = empProcess.DecisionName;
    }
    return cell;
}

#pragma getData
- (void)getData
{
    processWorkArr = [[NSMutableArray alloc]init];
    orgUnitNameArr = [[NSMutableArray alloc]init];
    
    [self getEmpProfileProcessOfWorkById];
}

- (void)getEmpProfileProcessOfWorkById
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Đang cập nhật";
    
    BasicHttpBinding_IEmpProfileLayerServiceBinding *binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
    EmpProfileLayerServiceSvc_GetViewEmpProfileProcessOfWorkById *request = [[EmpProfileLayerServiceSvc_GetViewEmpProfileProcessOfWorkById alloc] init];
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    if (app.otherEmpId != nil)
        request.employeeId = app.otherEmpId;
    else
        request.employeeId = app.sysUser.EmployeeId;
    //request.EmployeeId = [NSNumber numberWithInt:2];
    [binding GetViewEmpProfileProcessOfWorkByIdAsyncUsingParameters:request delegate:self];
}

- (void)operation:(BasicHttpBinding_IEmpProfileLayerServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)response
{
    if ([response.bodyParts count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi kết nối" message:@"Không thể kết nối đến server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    for (id mine in response.bodyParts){
        if ([mine isKindOfClass:[EmpProfileLayerServiceSvc_GetViewEmpProfileProcessOfWorkByIdResponse class]]) {
            empl_tns1_ArrayOfV_EmpProfileProcessOfWork *result = [mine GetViewEmpProfileProcessOfWorkByIdResult];
            processWorkArr = result.V_EmpProfileProcessOfWork;
            for (empl_tns1_EmpProfileProcessOfWork  *process in processWorkArr) {
                [orgUnitNameArr addObject:process.OrgUnitName];
            }
        }
        //        if ([mine isKindOfClass:[SOAPFault class]])
        //        {
        //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Server error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //            [alert show];
        //        }
    }
    
    if (!self.viewDidDisappear)
        [self.tableView reloadData];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}


@end
