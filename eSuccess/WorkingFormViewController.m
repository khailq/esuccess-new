//
//  WorkingFormViewController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 6/26/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "WorkingFormViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "DayLeaveDetailCell.h"
@interface WorkingFormViewController ()

@end

@implementation WorkingFormViewController
@synthesize workingFormArr;
@synthesize workingFormNameArr;
static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Hình thức làm việc";
    [self getData];
    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 130;
//}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([workingFormNameArr count] > 0) {
        return [workingFormNameArr objectAtIndex:section];
    }
    return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [workingFormArr count] > 0 ? workingFormArr.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //return [profileArray count];
    return [workingFormArr count] > 0? 5:1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (workingFormArr.count == 0)
        return [self getDefaultEmptyCell];
    
    DayLeaveDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
    if (cell == nil) {
        cell = [[DayLeaveDetailCell alloc]init];
    }
    cell.lbLeft.font = [UIFont systemFontOfSize:14];
    cell.lbLeft.backgroundColor = [UIColor clearColor];
    cell.lbRight.font = [UIFont boldSystemFontOfSize:16];
    cell.lbRight.backgroundColor = [UIColor clearColor];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    int section = indexPath.section;
    empl_tns1_V_EmpProfileWorkingForm *workingForm = [workingFormArr objectAtIndex:section];
    if (indexPath.row == 0) {
        cell.lbLeft.text = @"Ngày thực hiện";
        cell.lbRight.text = [formatter stringFromDate:workingForm.ImplementationDate];
    }
    if (indexPath.row == 1) {
        cell.lbLeft.text = @"Giờ bắt đầu";
//        cell.lbRight.text = [self processTimeString:workingForm.TimeWorkingFormStartTime];
        cell.lbRight.text = @"8:00";
    }
    if (indexPath.row == 2) {
        cell.lbLeft.text = @"Giờ kết thúc";
        //[formatter setDateFormat:@"HH:mm:ss"];
//        cell.lbRight.text = [self processTimeString:workingForm.TimeWorkingFormStartEndTime];
        cell.lbRight.text = @"16:00";
    }
    if (indexPath.row == 3) {
        cell.lbLeft.text = @"Ghi chú";
        cell.lbRight.text = workingForm.Note;
    }
    if (indexPath.row == 4) {
        cell.lbLeft.text = @"Trạng thái";
        if (workingForm.IsCurrent != nil) {
            if ([workingForm.IsCurrent boolValue] == YES) {
                cell.lbRight.text = @"Đang áp dụng";
            } else
                cell.lbRight.text = @"Chưa áp dụng";
        } else
            cell.lbRight.text = @"Đang cập nhật";
    }
    return cell;
}

-(NSString*)processTimeString:(NSString*)input
{
    NSString *output = [input stringByReplacingOccurrencesOfString:@"PT" withString:@""];
    output = [output stringByReplacingOccurrencesOfString:@"M" withString:@""];
    output = [output stringByReplacingOccurrencesOfString:@"H" withString:@":"];
    if ([output characterAtIndex:(output.length - 1)] == ':')
        output = [output stringByAppendingString:@"00"];
    
    return output;
}

#pragma getData
- (void)getData
{
    workingFormArr = [[NSMutableArray alloc]init];
    workingFormNameArr = [[NSMutableArray alloc]init];
    
    [self getEmpProfileWorkingFormById];
}

- (void)getEmpProfileWorkingFormById
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Đang cập nhật";
    
    BasicHttpBinding_IEmpProfileLayerServiceBinding *binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
    EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingFormById *request = [[EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingFormById alloc] init];
    binding.logXMLInOut = YES;
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    if (app.otherEmpId != nil)
        request.employeeId = app.otherEmpId;
    else
        request.employeeId = app.sysUser.EmployeeId;
    [binding GetViewEmpProfileWorkingFormByIdAsyncUsingParameters:request delegate:self];
}

- (void)operation:(BasicHttpBinding_IEmpProfileLayerServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)response
{
    if ([response.bodyParts count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi kết nối" message:@"Không thể kết nối đến server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    for (id mine in response.bodyParts){
        if ([mine isKindOfClass:[EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingFormByIdResponse class]]) {
            empl_tns1_ArrayOfV_EmpProfileWorkingForm *result = [mine GetViewEmpProfileWorkingFormByIdResult];
            workingFormArr = result.V_EmpProfileWorkingForm;
            for (empl_tns1_V_EmpProfileWorkingForm *workingForm in workingFormArr) {
                [workingFormNameArr addObject:workingForm.NameVN];
            }
        }
    }
    
    if (self != nil && !self.viewDidDisappear)
    {
        [self.tableView reloadData];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }
}

@end
