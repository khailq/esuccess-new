//
//  BasicSalaryViewController.h
//  eSuccess
//
//  Created by HPTVIETNAM on 6/22/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"
#import "EmpProfileLayerServiceSvc.h"
@interface BasicSalaryViewController : BaseDetailTableViewController<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>
@property(nonatomic, strong) NSMutableArray *salaryArr;
@property(nonatomic, strong) NSMutableArray *qoutaArr;
@end
