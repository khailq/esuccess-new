//
//  MedicalHistoryCell.m
//  eSuccess
//
//  Created by admin on 5/4/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "MedicalHistoryCell.h"

@implementation MedicalHistoryCell
@synthesize lblDiseaseName;
@synthesize lblTime;
@synthesize lblBenefit;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
