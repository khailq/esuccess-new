//
//  AllowanceViewController.h
//  eSuccess
//
//  Created by admin on 5/24/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"
#import "EmpProfileLayerServiceSvc.h"
@interface AllowanceViewController : BaseDetailTableViewController<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>
@property(nonatomic, retain) NSMutableArray *allowanceArr;
@property(nonatomic, retain) NSMutableArray *allowanceNameArr;
@end
