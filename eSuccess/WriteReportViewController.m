//
//  WriteReportViewController.m
//  eSuccess
//
//  Created by admin on 6/7/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "WriteReportViewController.h"

@interface WriteReportViewController ()

@end

@implementation WriteReportViewController
@synthesize tvReport;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Báo cáo" style:UIBarButtonItemStyleDone target:self action:@selector(doFinish)];
    
}

- (void)doFinish
{
    if ([tvReport.text length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Lỗi" message:@"Vui lòng nhập nội dung báo cáo" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
