//
//  LearningMasterController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 5/4/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "LearningMasterController.h"
#import "Course.h"
#import "AFNetworking.h"
#import "PopoverContentViewController.h"
#import "MBProgressHUD.h"
#import "CoursesViewController.h"

#define LearningODataServiceUrl @"http://10.0.18.118:5000/odata/odataservices.svc/V_LMSCourseKhaiLQ?$filter=EmployeeId eq %d&$format=json"

#define GroupByTopicTitle @"Chủ đề"
#define GroupByCourseStatusTitle @"Tình trạng"
#define GroupByClassType @"Loại lớp học"

@interface LearningMasterController ()
{
    NSMutableArray *courses;
    NSMutableDictionary *groupByTopicDict;
    NSMutableDictionary *groupByCourseStatusDict;
    NSMutableDictionary *groupByClassDict;
    UIPopoverController *groupByPopover;
    PopoverContentViewController *groupByContentController;
    
    NSMutableArray *groups;
    
    NSDictionary *currentGroupByDict;
}
@end

static NSString *cellIdentifier = @"CellIdentifier";

@implementation LearningMasterController



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.title = @"E-Learning";
    groups = [NSMutableArray array];
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemOrganize target:self action:@selector(onRightButtonClicked:)];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    [self getCoursesForLoggedInEmployee];
    self.clearsSelectionOnViewWillAppear = NO;
    
    //[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:cellIdentifier];
}

-(void)onRightButtonClicked:(id)sender
{
    [groupByPopover presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

-(void)onGroupBySelectionChanged:(id)selectedItem
{
    [groupByPopover dismissPopoverAnimated:YES];
    NSString *text = (NSString *)selectedItem;
    
    //int index = [groups indexOfObjectIdenticalTo:text];
    currentGroupByDict = [self dictCorrespodingToTitle:text];
    
    [self.tableView reloadData];
    [self selectFirstRow];
}
-(void)getCoursesForLoggedInEmployee
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    //AppDelegate *app = [UIApplication sharedApplication].delegate;
    //NSNumber *empId = app.loginEmployeeInfo.EmployeeId;
    
    //NSString* api = [NSString stringWithFormat: LearningODataServiceUrl, empId.intValue];
    NSString* api = [NSString stringWithFormat: LearningODataServiceUrl, 4];
    api = [api stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:api]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self onGettingCourseCompletedWithJSON:JSON];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"Loi server");
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
    [operation start];
}

-(void)onGettingCourseCompletedWithJSON:(id)json
{
    courses = [NSMutableArray array];
    groupByClassDict = [NSMutableDictionary dictionary];
    groupByCourseStatusDict = [NSMutableDictionary dictionary];
    groupByTopicDict = [NSMutableDictionary dictionary];
    
    NSDictionary *returnObject = (NSDictionary*)json;
    NSArray *coursesArr = returnObject[@"d"];
    
    for(NSDictionary *dict in coursesArr)
    {
        Course *course = [Course parseCourseFromJSON:dict];
        [courses addObject:course];
        
        if (groupByTopicDict[course.topicGroupId] == nil)
        {
            NSMutableArray *courseByTopic = [NSMutableArray arrayWithObject:course];
            [groupByTopicDict setObject:courseByTopic forKey:course.topicGroupId];
        }
        else
        {
            NSMutableArray *courseByTopic = groupByTopicDict[course.topicGroupId];
            [courseByTopic addObject:course];
        }
        
        if (groupByCourseStatusDict[course.courseStatusId] == nil)
        {
            NSMutableArray *coursesByStatus = [NSMutableArray arrayWithObject:course];
            [groupByCourseStatusDict setObject:coursesByStatus forKey:course.courseStatusId];
        }
        else
        {
            NSMutableArray *coursesByStatus = groupByCourseStatusDict[course.courseStatusId];
            [coursesByStatus addObject:course];
        }
        
        if (groupByClassDict[course.courseTypeId] == nil)
        {
            NSMutableArray *coursesByType = [NSMutableArray arrayWithObject:course];
            [groupByClassDict setObject:coursesByType forKey:course.courseTypeId];
        }
        else
        {
            NSMutableArray *coursesByType = groupByClassDict[course.courseTypeId];
            [coursesByType addObject:course];
        }
    }
    
    if (groupByClassDict.count > 0)
        [groups addObject:GroupByClassType];
    if (groupByCourseStatusDict > 0)
        [groups addObject:GroupByCourseStatusTitle];
    if (groupByTopicDict.count > 0)
        [groups addObject:GroupByTopicTitle];
    
    currentGroupByDict = groupByClassDict.count > 0 ? groupByClassDict : groupByCourseStatusDict.count > 0 ? groupByCourseStatusDict : groupByTopicDict.count > 0 ? groupByTopicDict : [NSDictionary dictionary] ;
    
    groupByContentController = [[PopoverContentViewController alloc] init];
    groupByContentController.data = groups;
    [groupByContentController addTarget:self forSelectionChanged:@selector(onGroupBySelectionChanged:)];
    
    groupByPopover = [[UIPopoverController alloc] initWithContentViewController:groupByContentController];
    
    [self.tableView reloadData];
    
    [self selectFirstRow];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSDictionary *)dictCorrespodingToTitle:(NSString*)title
{
    if ([title rangeOfString:GroupByCourseStatusTitle].location != NSNotFound)
        return groupByCourseStatusDict;
    if ([title rangeOfString:GroupByTopicTitle].location != NSNotFound)
        return groupByTopicDict;
    return groupByClassDict;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    //[self performSegueWithIdentifier:@"ViewCoursesSegue" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    id destination = segue.destinationViewController;
    NSIndexPath *selectedIndex = [self.tableView indexPathForSelectedRow];
    NSArray *coursesArr = currentGroupByDict[currentGroupByDict.allKeys[selectedIndex.row]];
    
    if ([destination respondsToSelector:@selector(setCourses:)])
    {
        [destination setValue:coursesArr forKey:@"courses"];
    }
}

-(void)selectFirstRow
{
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
    
    [self performSegueWithIdentifier:@"ViewCoursesSegue" sender:self];
    
    
}
#pragma mark - TableView Datasource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //return groups.count;
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (groups.count == 0)
        return 0;
    return currentGroupByDict.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    NSArray *courseInGroup = currentGroupByDict[currentGroupByDict.allKeys[indexPath.row]];
    if (courseInGroup == nil)
        return nil;
    Course *course = courseInGroup[0];
    
    if (currentGroupByDict == groupByCourseStatusDict)
    {
        cell.textLabel.text = course.courseStatus;
    }
    else if (currentGroupByDict == groupByTopicDict)
    {
        cell.textLabel.text = course.topicGroupName;
    }
    else
        cell.textLabel.text = course.courseTypeName;
    
    return cell;
}
@end
