//
//  BasicSkillViewController.h
//  eSuccess
//
//  Created by admin on 5/11/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"
@interface BasicSkillViewController : BaseDetailTableViewController
@property(nonatomic, retain) NSMutableArray *skillArray;
@property(nonatomic, retain) NSMutableArray *skillTypeArr;
@end
