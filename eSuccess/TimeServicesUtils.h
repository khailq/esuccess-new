//
//  TimeServicesUtils.h
//  eSuccess
//
//  Created by HPTVIETNAM on 9/23/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TimeServieSvc.h"
#import "EmployeeServiceSvc.h"
#import "SystemServiceSvc.h"
#import "RequestServiceSvc.h"

@interface TimeServicesUtils : NSObject

@property (strong, nonatomic) sys_tns1_ArrayOfTB_ESS_SYSTEM_PARAMETER *listOfAbsenceTypeResult;
@property (strong, nonatomic) emp_tns1_ArrayOfSP_GetAllParentEmployee_Result *listOfLeadersResult;
@property (strong, nonatomic) time_tns1_SP_ESS_GET_EMP_HOLIDAY_Result *employeeHolidayInfoResult;
@property (strong, nonatomic) time_tns1_ArrayOfTB_ESS_SYSTEM_PARAMETER *methodsOfLeaveResult;

-(req_tns1_ResponseModel *) createLeaveRequest:(req_tns1_LeaveRequestSubmitModel *)submitModel;

-(void) getMethodOfLeave;

-(void) getEmployeeHolidayInfo:(NSNumber *)empId;
-(void) getLeadersOfEmployee:(NSNumber *)empId;
-(NSString *)classifyPaymentMethodForAbsenceType:(sys_tns1_TB_ESS_SYSTEM_PARAMETER*) absenceType;
-(void) getListOfAbsenceType;

+(id)instance;

@end
