//
//  Constant.h
//  eSuccess
//
//  Created by HPTVIETNAM on 9/23/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <Foundation/Foundation.h>

// Key nhận biết đây là đơn
#define TM_DON @"TM_DON"  

// Code cho biết đơn vắng mặt
#define TM_DON_LEAVE_REQUEST @"TM_DON_LEAVE_REQUEST"

// Hằng số cho biết trạng thái nghỉ không trừ lương và ngày phép
#define NOT_PAY_LEAVE_REQUEST 1

// trạng thái nghỉ trừ ngày phép
#define PAY_LEAVE 2

// trạng thái nghỉ trừ lương
#define PAY_SALARY 3

#define TM_CT @"TM_CT"

// Code cho biết các loại buổi vắng mặt
#define TM_CT_METHODOFLEAVE @"TM_CT_METHODOFLEAVE"

//Code cho biết loại buổi vắng mặt là buổi sáng
#define TM_CT_METHODOFLEAVE_AM @"TM_CT_METHODOFLEAVE_AM"

// Code cho biết loại buổi vắng mặt là buổi chiều
#define TM_CT_METHODOFLEAVE_PM @"TM_CT_METHODOFLEAVE_AM"

// Code cho biết loại buổi vắng mặt là fullday
#define TM_CT_METHODOFLEAVE_FULL @"TM_CT_METHODOFLEAVE_FULL"


@interface Constant : NSObject

@end
