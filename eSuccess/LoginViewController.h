//
//  LoginViewController.h
//  eSuccess
//
//  Created by HPTVIETNAM on 4/19/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SecurityServiceSvc.h"
#import "EmpProfileLayerServiceSvc.h"
//#import "LeaveRequestServiceSvc.h"

@interface LoginViewController : UIViewController<UISplitViewControllerDelegate, BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>

@property (strong, nonatomic) IBOutlet UITextField *tb_username;
@property (strong, nonatomic) IBOutlet UITextField *tb_password;

-(IBAction)btnLoginClicked:(id)sender;

@end
