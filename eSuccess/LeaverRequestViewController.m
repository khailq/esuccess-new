//
//  LeaverRequestViewController.m
//  Presidents
//
//  Created by HPTVIETNAM on 4/13/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "PopoverContentViewController.h"
#import "AppDelegate.h"
#import "DayLeaveDetailCell.h"
#import "SecurityServiceSvc.h"
#import "AFHTTPClient.h"
#import "MBProgressHUD.h"
#import "AFJSONRequestOperation.h"
#import "HttpWebRequest.h"
#import "TimeServieSvc.h"
#import "Employee Service/EmployeeServiceSvc.h"


static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";

@interface LeaverRequestViewController ()<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate, BasicHttpBinding_ITimeServieBindingResponseDelegate>

@end

@implementation LeaverRequestViewController
{
    NSArray *timeAbsenceTypes;
    NSArray *approvers;
    NSDateFormatter *dateFormatter;
    NSMutableArray *absenceTypeStringList;
    NSMutableArray *approverStringList;
    NSArray *absenceTypes;
    NSMutableArray *daysLeave;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.rightItem = [[UIBarButtonItem alloc] initWithTitle:@"Gởi" style:UIBarButtonItemStyleBordered target:self action:@selector(onSubmitClicked:)];
    self.navigationItem.rightBarButtonItem = self.rightItem;
    
    [self populateArray];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    NSDate *date = [NSDate date];
    NSString *string = [dateFormatter stringFromDate:date];
    self.lb_fromDate.text = string;
    self.lb_toDate.text = string;
    self.lb_issueDate.text = string;
    
//    leaveStatuses = [NSArray arrayWithObjects:@"Cả ngày/Full", @"Sáng/AM", @"Chiều/PM", nil];
    
    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
    
    daysLeave = [self getListDatesFrom:[NSDate date] toDate:[NSDate date]];
    
//    AppDelegate *app = [UIApplication sharedApplication].delegate;
//    self.lb_empCode.text = app.basicEmpProfile.EmployeeCode;
//    self.lb_empName.text = app.basicEmpProfile.FullName;
//    self.lb_unitName.text = app.basicEmpProfile.o
//    self.lb_position.text = app.loginEmployeeInfo.PositionName;
//    self.lb_email.text = app.loginEmployeeInfo.Email;
//    
//    _editable = YES;
//    
//    if (_leaveRequest != nil)
//    {
//        UIBarButtonItem *deleteItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(onDeleteButtonClicked:)];
//        self.navigationItem.rightBarButtonItem = deleteItem;
//        
//        self.editable = NO;
//        
//        self.lb_issueDate.text = [dateFormatter stringFromDate:_leaveRequest.SelfLeaveRequestCreatedDate];
//        self.lb_fromDate.text = [dateFormatter stringFromDate:_leaveRequest.StartDate];
//        self.lb_toDate.text = [dateFormatter stringFromDate:_leaveRequest.EndDate];
//        self.lb_absenceType.text = _leaveRequest.TimeAbsenceTypeName;
//        
//        self.lb_approver.text = _leaveRequest.ApproverName;
//        
//        self.tb_reason.text = _leaveRequest.Reason;
//    }
}

-(void)populateArray
{
        

}
-(void)getAbsentTypes
{
    BasicHttpBinding_ITimeServieBinding *binding = [TimeServieSvc BasicHttpBinding_ITimeServieBinding];
    TimeServieSvc_GetMasterConfigByCodeType *request = [TimeServieSvc_GetMasterConfigByCodeType new];
    request.type = @"TM_DON_LEAVE_REQUEST";
    request.code = @"TM_DON";
    
    [binding GetMasterConfigByCodeTypeAsyncUsingParameters:request delegate:self];
}

-(void)getAbsentTypeCompleted:(NSArray *)types
{
    absenceTypes = types;
    absenceTypeStringList = [NSMutableArray arrayWithCapacity:types.count];
    for (time_tns1_TimeSysConfigParam *param in types) {
        [absenceTypeStringList addObject:param.Name];
    }
}

-(void)getLeaders
{
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    BasicHttpBinding_IEmployeeServiceBinding *binding = [EmployeeServiceSvc BasicHttpBinding_IEmployeeServiceBinding];
    EmployeeServiceSvc_GetAllLeader *request = [EmployeeServiceSvc_GetAllLeader new];
    request.employeeId = app.sysUser.EmployeeId;
    
    [binding GetAllLeaderAsyncUsingParameters:request delegate:self];
}

-(void)getLeaderCompleted:(NSArray *)json
{
    approvers = json;
    approverStringList = [NSMutableArray arrayWithCapacity:approvers.count];
    for (emp_tns1_SP_GetAllParentEmployee_Result *leader in approvers) {
        [approverStringList addObject:leader.ParentFullName];
    }
}

-(void)onDeleteButtonClicked:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Bạn thực sự muốn huỷ đơn này?" delegate:self cancelButtonTitle:@"Không" otherButtonTitles:@"Đồng ý", nil];
    alert.tag = 20;
    [alert show];
}
-(void)setEditable:(BOOL)editable
{
    if (_editable != editable)
        _editable = editable;
    

    [self.tb_reason setEditable:_editable];
}

-(NSMutableArray *)getListDatesFrom:(NSDate*)startDate toDate:(NSDate*)endDate
{
    NSMutableArray *array = [NSMutableArray array];
    NSDate *nextDate = nil;
    NSTimeInterval interval = 60 * 24 * 60;
    for (nextDate = startDate; [nextDate compare:endDate] <= 0; nextDate = [nextDate dateByAddingTimeInterval:interval]) {
        [array addObject:nextDate];
    }
    
    return array;
}

-(NSDate*)addDays:(int)days toDate:(NSDate*)date
{
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:days];
    
    // create a calendar
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDate *newDate2 = [gregorian dateByAddingComponents:components toDate:date options:0];
    
    return newDate2;
}

-(int)daysFromDate:(NSDate*)startDate toDate:(NSDate*)endDate
{
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    
    return components.day;
}

-(int)daysFromDate:(NSString*)startDate toDateString:(NSString*)endDate
{
    NSDate *sDate = [dateFormatter dateFromString:startDate];
    NSDate *edate = [dateFormatter dateFromString:endDate];
    
    return [self daysFromDate:sDate toDate:edate] + 1;
}

-(void)onDatePickerSelectionChanged:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    NSDate *selectedDate = [self.datePicker.picker date];
    NSString *date = [dateFormatter stringFromDate:selectedDate];
    if (indexPath.section == 1)
    {
        if (indexPath.row == 1)
        {
            self.lb_fromDate.text = date;
            NSDate *toDate = [dateFormatter dateFromString:self.lb_toDate.text];
            if ([selectedDate compare:toDate] > 0)
                self.lb_toDate.text = date;
        }
        if (indexPath.row == 2)
            self.lb_toDate.text = date;
    }
}

-(int)getIndexOfItem:(NSString*)item inArray:(NSArray *)array
{
    for (int i = 0; i < array.count; ++i)
    {
        if ([array[i] rangeOfString:item].location != NSNotFound)
            return i;
    }

    return -1;
}

//-(void)onSubmitClicked:(id)sender
//{
//    AppDelegate *app = [UIApplication sharedApplication].delegate;
//    
//    NSMutableArray *dates = [NSMutableArray array];
//    NSMutableArray *statuses = [NSMutableArray array];
//    
//    for(NSDate *date in daysLeave)
//    {
//        [dates addObject:[dateFormatter stringFromDate:date]];
//    }
//    
//    for(int i = 0; i < daysLeave.count; ++i)
//    {
//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:2];
//        DayLeaveDetailCell *cell = (DayLeaveDetailCell *)[self.tableView cellForRowAtIndexPath:indexPath];
//        NSArray *separate = [cell.lbRight.text componentsSeparatedByString:@"/"];
//        [statuses addObject:separate[1]];
//    }
//    
//    int approverIndex = [self getIndexOfItem:self.lb_approver.text inArray:approverStringList];
//    if (approverIndex == -1)
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Chưa chọn người duyệt đơn" delegate:nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
//        [alert show];
//        return;
//    }
//    
//    tns1_SP_GetAllParentEmployee_Result *approver = [approvers objectAtIndex:approverIndex];
//    
//    int absenceIndex = [self getIndexOfItem:self.lb_absenceType.text inArray:absenceTypeStringList];
//    if (absenceIndex == -1)
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Chưa chọn hình thức nghỉ" delegate:nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
//        [alert show];
//        return;
//    }
//    tns1_TimeAbsenceType* absence = timeAbsenceTypes[absenceIndex];
//    
//    if ([self.tb_reason.text isEqualToString:@""])
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Chưa nhập lí do" delegate:nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
//        [alert show];
//        return;
//    }
//    int leaveType = -1;
//    if (absence.IsLeaveWithoutPay)
//        leaveType = 1;
//    else if (absence.IsUnpaidLeave)
//        leaveType = 2;
//    else
//        leaveType = 3;
//    
//    NSString *leaveTypeString = [NSString stringWithFormat:@"%d", leaveType];
//    NSString *numberDaysOffString = [NSString stringWithFormat:@"%d", daysLeave.count];
//    
//    [self submitLeaveRequestForEmployeeId:[app.loginEmployeeInfo.EmployeeId stringValue] employeeCode:app.loginEmployeeInfo.EmployeeCode absenceTypeId:[absence.TimeAbsenceTypeId stringValue] startDate:self.lb_fromDate.text endDate:self.lb_toDate.text numberDaysOff:numberDaysOffString address:app.loginEmployeeInfo.Address phone:app.loginEmployeeInfo.Address1Phone approverId:[approver.ParentId stringValue] reason:self.tb_reason.text approvalStatus:@"1" leaveType:leaveTypeString daysLeave:dates leaveStatuses:statuses];
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initPicker:(BOOL)isToDateLabel
{
    CGRect screenRect = self.view.frame;
    
    CGFloat y = screenRect.size.height;
    y = y - MyDateTimePickerHeight - screenRect.origin.y;
    CGRect rect = CGRectMake(0, y, screenRect.size.width, MyDateTimePickerHeight);
    self.datePicker = [[DatePickerPopup alloc] initWithFrame:rect];
    
    if(isToDateLabel)
        [self.datePicker.picker setMinimumDate:[NSDate date]];
    else
        {
            NSDate *date = [dateFormatter dateFromString:self.lb_fromDate.text];
            [self.datePicker.picker setMinimumDate:date];
        }
    [self.datePicker setMode:UIDatePickerModeDate];
    [self.datePicker addTargetForDoneButton:self action:@selector(onDoneButtonClicked:)];
    [self.datePicker.picker addTarget:self action:@selector(onDatePickerSelectionChanged:) forControlEvents:UIControlEventValueChanged];
    [self.datePicker setHidden:YES animated:NO];
    [self.view insertSubview:self.datePicker atIndex:[self.view subviews].count];
}

-(void)onDoneButtonClicked:(id)sender
{
    [self.datePicker setHidden:YES animated:YES];
    self.datePicker = nil;
    NSDate *startDate = [dateFormatter dateFromString:self.lb_fromDate.text];
    NSDate *endDate = [dateFormatter dateFromString:self.lb_toDate.text];
    
    daysLeave = [self getListDatesFrom:startDate toDate:endDate];
    [self.tableView reloadData];
}


#pragma mark - Table view delegate

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleNone;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int section = indexPath.section;

    if (section == 2) {
        return 44; 
    } else {
        return [super tableView:tableView heightForRowAtIndexPath:indexPath];
    }
}

- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int section = indexPath.section;
    
    // if dynamic section make all rows the same indentation level as row 0
    if (section == 2) {
        return 1;
    } else {
        return [super tableView:tableView indentationLevelForRowAtIndexPath:indexPath];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 2 ) {
        return daysLeave.count;
    } else {
        return [super tableView:tableView numberOfRowsInSection:section];
    }
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    int section = indexPath.section;
    
    
    if (section == 2) {
        NSDate *date = daysLeave[indexPath.row];
        DayLeaveDetailCell *dCell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
        
        dCell.lbLeft.text = [dateFormatter stringFromDate:date];
//        dCell.lbRight.text = leaveStatuses[0];
        
        
        
        return dCell;
    }
    else
    {
        return [super tableView:tableView cellForRowAtIndexPath:indexPath];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!_editable)
        return;
    
    [self.tb_reason endEditing:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (indexPath.section == 1 && indexPath.row == 1)
    {
        if (self.datePicker == nil)
        {
            [self initPicker: YES];
            [self.datePicker setHidden:NO animated:YES];
        }
    }
    else if (indexPath.section == 1 && indexPath.row == 2)
    {
        if (self.datePicker == nil)
        {
            [self initPicker:NO];
            [self.datePicker setHidden:NO animated:YES];
        }
    }
    else if (indexPath.section == 1 && indexPath.row == 3)
    {
        PopoverContentViewController *content = [[PopoverContentViewController alloc] init];
        content.data = absenceTypeStringList;
        content.type  = 1;
        
        UIPopoverController *pop = [[UIPopoverController alloc] initWithContentViewController:content];
        self.popover = pop;
        
        [self.popover presentPopoverFromRect:cell.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
        content.parentViewController = self;
    }
    else if (indexPath.section == 1 && indexPath.row == 4)
    {
        PopoverContentViewController *content = [[PopoverContentViewController alloc] init];
        content.type = 2;
        content.data = approverStringList;
        
        self.popover = [[UIPopoverController alloc] initWithContentViewController:content];
        
        
        [self.popover presentPopoverFromRect:cell.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        content.parentViewController = self;
    }
    else if (indexPath.section == 2)
    {
        PopoverContentViewController *content = [[PopoverContentViewController alloc] init];
//        content.data = leaveStatuses;
        self.popover = [[UIPopoverController alloc] initWithContentViewController:content];
        [content addTarget:self forSelectionChanged:@selector(onSelectionChanged:)];
        [self.popover presentPopoverFromRect:cell.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
    }
}

-(void)onSelectionChanged:(NSString *)value
{
    NSIndexPath *index = [self.tableView indexPathForSelectedRow];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:index];
    if ([cell isKindOfClass:[DayLeaveDetailCell class]])
    {
        DayLeaveDetailCell *dCell = (DayLeaveDetailCell *)cell;
        dCell.lbRight.text = value;
    }
    
    [self.popover dismissPopoverAnimated:YES];
    self.popover = nil;
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.popover = nil;
    [self.datePicker setHidden:YES animated:YES];
    self.datePicker = nil;
}

#pragma Submit Leave request

-(void)deleteLeaveRequest:(NSNumber*)leaveRequestId
{
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    NSString *url = [DeleteLeaveRequestUrl stringByAppendingString:[leaveRequestId stringValue]];
//    [HttpWebRequest getDataAsyncWithURL:url completionHandler:^(NSData *returnedData, NSError *error) {
//        NSString *returnString = [[NSString alloc] initWithData:returnedData encoding:NSUTF8StringEncoding];
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
//        if ([returnString isEqualToString:@"Success"])
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Xoá đơn xin phép thành công" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//            alert.tag = 10;
//            [alert show];
//        }
//        else
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Không thành công, vui lòng thử lại" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//            [alert show];
//        }
//    }];
}

-(NSString *)submitLeaveRequestForEmployeeId:(NSString *)empId employeeCode:(NSString *)empCode absenceTypeId:(NSString *)timeAbsenceTypeId startDate:(NSString *)startDate endDate:(NSString *)endDate numberDaysOff:(NSString *)numberDayOff address:(NSString *)address phone:(NSString *)phone approverId:(NSString *)approverId reason:(NSString *)reason approvalStatus:(NSString *)approvalStatus leaveType:(NSString *)leaveType daysLeave:(NSArray *)_daysLeave leaveStatuses:(NSArray *)_leaveStatuses
{
    NSDictionary *dictData = @{@"empId" : empId, @"empCode": empCode, @"timeAbsenceTypeId" : timeAbsenceTypeId, @"startDate" : startDate,
                                      @"endDate" : endDate, @"NumberDaysOff": numberDayOff, @"address": address, @"phone" : phone, @"approverId" : approverId, @"reason" : reason, @"approvalStatus" : approvalStatus, @"leaveType" : leaveType, @"dayLeave" : _daysLeave, @"leaveStatus" : _leaveStatuses };
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictData options:kNilOptions error:nil];
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSLog(@"JSON String: %@",jsonString);
    
    [self.tb_reason endEditing:YES];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Đang gởi đơn";
    
    NSString *baseURL = BaseUrl;
    baseURL = [baseURL stringByAppendingString:@"LeaveTimeRequest/CreateLeaveRequest"];
    
    [HttpWebRequest postDataAsyncToURL:baseURL postBody:jsonData completionHandler:^(NSData *returnData, NSError *error) {
        static NSString *okString = @"Thêm đơn xin nghỉ phép mới thành công";
   
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        returnString = [returnString stringByReplacingOccurrencesOfString:@"\"" withString:@""
                        ];
        [hud hide:YES];
        if ([returnString rangeOfString:okString].location != NSNotFound)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Gởi đơn xin nghỉ thành công" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            alert.tag = 10;
            [alert show];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Có lỗi trong quá trình gởi đơn xin nghỉ" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    }];
    
    return nil;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 10)
        [self.navigationController popViewControllerAnimated:YES];
    if (alertView.tag == 20 && buttonIndex == 1)
    {
        [self deleteLeaveRequest:self.leaveRequest.SelfLeaveRequestId];
    }
}

#pragma custom setter
-(void)setConfirmerName:(NSString *)confirmerName
{
    if (![_confirmerName isEqualToString:confirmerName])
    {
        _confirmerName = confirmerName;
        self.lb_approver.text = _confirmerName;
    }
    
    [self.popover dismissPopoverAnimated:YES];
    self.popover = nil;
}

-(void)setLeaveTypeText:(NSString *)leaveTypeText
{
    if (![_leaveTypeText isEqualToString:leaveTypeText])
    {
        _leaveTypeText = leaveTypeText;
        self.lb_absenceType.text = _leaveTypeText;
    }
    
    [self.popover dismissPopoverAnimated:YES];
    self.popover = nil;
}

#pragma mark-- Operation
-(void)operation:(id)operation completedWithResponse:(id)response
{
    if ([response isKindOfClass:[BasicHttpBinding_IEmployeeServiceBindingResponse class]])
    {
        BasicHttpBinding_IEmployeeServiceBindingResponse *empResponse = (BasicHttpBinding_IEmployeeServiceBindingResponse*)response;
        for (id type in empResponse.bodyParts) {
            if ([type isKindOfClass:[EmployeeServiceSvc_GetAllLeaderResponse class]])
            {
                emp_tns1_ArrayOfSP_GetAllParentEmployee_Result *result = [type GetAllLeaderResult];
                [self getLeaderCompleted:result.SP_GetAllParentEmployee_Result];
            }
        }
    }
    else if ([response isKindOfClass:[BasicHttpBinding_ITimeServieBindingResponse class]])
{
    BasicHttpBinding_ITimeServieBindingResponse *timeResponse = (BasicHttpBinding_ITimeServieBindingResponse *)response;
    for (id type in timeResponse.bodyParts) {
        if ([type isKindOfClass:[TimeServieSvc_GetMasterConfigByCodeTypeResponse class]])
        {
            
        }
    }
}
}
@end