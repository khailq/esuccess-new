//
//  OrgDetailViewController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 5/10/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "OrgDetailViewController.h"
#import "OrgTopPanelView.h"
#import <QuartzCore/QuartzCore.h>
#import "ProfilePanelView.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "UIImageView+AFNetworking.h"

#define H_OrgHeight 477
#define V_OrgHeight 733


@interface OrgDetailViewController ()<GMGridViewDataSource, GMGridViewActionDelegate>
{
    UIPopoverController *popover;
    GMGridView * _gmGridView;
    NSArray *children;
    NSMutableArray *profilePanels;
    ProfilePanelView *selectedItem;
    NSInteger selectedIndex;
}

@property (strong, nonatomic) IBOutlet UIView *userView;
@property (strong, nonatomic) IBOutlet UIView *orgPanelView;
@property (strong, nonatomic) IBOutlet OrgTopPanelView *orgTopPanelView;

-(IBAction)viewTouchInside:(id)sender;

@end

@implementation OrgDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    selectedIndex = -1;
    profilePanels = [NSMutableArray array];
    
    
    //NSNumber *pId = [NSNumber numberWithInt:13];
    //[self getDirectReportByParentId:pId];

    self.userView.layer.borderWidth = 3.0f;
    [[self.userView layer] setMasksToBounds:YES];
    self.userView.layer.borderColor = [UIColor orangeColor].CGColor;
    
    CGRect rect;
    int spacing = 15;
    UIEdgeInsets edgeInset;
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation]))
    {
        rect = CGRectMake(0, 0, self.view.bounds.size.width, V_OrgHeight);
        edgeInset = UIEdgeInsetsMake(20, 45, 20, 45);
        spacing = 25;
    }
    else
    {
        rect = CGRectMake(0, 0, self.view.bounds.size.width, H_OrgHeight);
        edgeInset = UIEdgeInsetsMake(20, 25, 20, 25);
    }
    _gmGridView = [[GMGridView alloc] initWithFrame:rect];
    [_gmGridView setBackgroundColor:[UIColor darkGrayColor]];
    _gmGridView.dataSource = self;
    _gmGridView.actionDelegate = self;
    _gmGridView.centerGrid = NO;
    _gmGridView.itemSpacing = spacing;
    _gmGridView.minEdgeInsets = edgeInset;
    _gmGridView.mainSuperView = self.view;
    
    [self.orgPanelView addSubview:_gmGridView];
    
}




-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    CGRect rect;
    int spacing = 0;
    UIEdgeInsets edgeInset;
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation]))
    {
        rect = CGRectMake(0, 0, self.view.bounds.size.width, V_OrgHeight);
        spacing = 25;
        edgeInset = UIEdgeInsetsMake(20, 45, 20, 45);
    }
    else
    {
        rect = CGRectMake(0, 0, self.view.bounds.size.width, H_OrgHeight);
        spacing = 15;
        edgeInset = UIEdgeInsetsMake(20, 25, 20, 25);
    }
    _gmGridView.frame = rect;
    _gmGridView.minEdgeInsets = edgeInset;
    _gmGridView.itemSpacing = spacing;
}

-(void)viewProfileClicked:(id)sender
{
    if (selectedIndex > -1)
    {
    }
}

-(void)viewTouchInside:(id)sender
{
    
}

-(int)heightFromSeparatorToBottom
{
    CGFloat y = self.userView.frame.origin.y + self.userView.frame.size.height + 10;
    return self.view.bounds.size.height - y;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - GMGridView datasource

-(NSInteger)numberOfItemsInGMGridView:(GMGridView *)gridView
{
    return children.count;
}

-(GMGridViewCell *)GMGridView:(GMGridView *)gridView cellForItemAtIndex:(NSInteger)index
{
    GMGridViewCell *cell = [gridView dequeueReusableCell];
    if (cell == nil)
        cell = [[GMGridViewCell alloc] init];

    
    
    return cell;
}

-(CGSize)GMGridView:(GMGridView *)gridView sizeForItemsInInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    return CGSizeMake(H_PanelWidth, H_PanelHeight);
    //return CGSizeMake(V_PanelWidth, V_PanelHeight);
}

#pragma mark - GMGridView action delegate
-(void)GMGridView:(GMGridView *)gridView didTapOnItemAtIndex:(NSInteger)position
{
    ProfilePanelView *panel = [profilePanels objectAtIndex:position];
    if (selectedItem != nil)
        selectedItem.selected = NO;
    panel.selected = YES;
    selectedItem = panel;
    selectedIndex = position;
}

#pragma mark - Employee service binding

@end
