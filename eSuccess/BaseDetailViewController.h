//
//  BaseDetailViewController.h
//  eSuccess
//
//  Created by HPTVIETNAM on 5/4/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSplitViewController.h"

@interface BaseDetailViewController : UIViewController

@property (assign, nonatomic) BOOL viewDidDisappear;
@end
