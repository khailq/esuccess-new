//
//  GPCompanyYearObject.h
//  eSuccess
//
//  Created by admin on 6/6/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GPCompanyScoreCard.h"
@interface GPCompanyYearObject : NSObject
@property(nonatomic) NSInteger GPCompanyId;
@property(nonatomic, retain) NSMutableArray *gpScoreCardArr;
@property(nonatomic, retain) NSString *statement;
@property(nonatomic, retain) NSString *smartTimeGoal;

+(GPCompanyYearObject *)companyYearWithID:(NSInteger )ID statement:(NSString *)statement smartTimeGoal:(NSString *)smartTimeGoal scoreCardArr:(NSMutableArray *)scoreCardArr;
//+(id)initWithID:(NSInteger )ID statement:(NSString *)statement scoreCardArr:(NSMutableArray *)scoreCardArr;
@end
