//
//  CustomSplitViewController.m
//  eSuccess
//
//  Created by Scott on 4/21/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "CustomSplitViewController.h"
#import "CustomBarButtonItem.h"
#import "AppDelegate.h"
#import "BaseNavigationViewController.h"



@interface CustomSplitViewController ()
{
    UIToolbar *myToolbar;
    __weak AppDelegate *app;
    NSArray *itemArray;
    
//    CustomBarButtonItem *dashboard;
//    CustomBarButtonItem *myProfile;
//    CustomBarButtonItem *myJob;
//    CustomBarButtonItem *myTask;
//    CustomBarButtonItem *myTeam;
//    CustomBarButtonItem *myPerformance;
//    CustomBarButtonItem *selfService;
//    CustomBarButtonItem *eLearning;
}

@property (strong, nonatomic) CustomBarButtonItem *selectedItem;

@end

@implementation CustomSplitViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    app = [UIApplication sharedApplication].delegate;
    
    myToolbar = [[UIToolbar alloc] init];
    myToolbar.barStyle = UIBarStyleBlackOpaque;
    
    CustomBarButtonItem *dashboard = [[CustomBarButtonItem alloc] initWithImageName:@"dashboard.png" title:@"Dashboard"];
    [dashboard addTarget:self forButtonAction:@selector(onTabItemClicked:)];
    dashboard.tag = 0;
    
    CustomBarButtonItem *myProfile = [[CustomBarButtonItem alloc] initWithImageName:@"my-profile.png" title:@"Profile"];
    [myProfile addTarget:self forButtonAction:@selector(onTabItemClicked:)];
    myProfile.tag = 1;
    
    CustomBarButtonItem *myJob = [[CustomBarButtonItem alloc] initWithImageName:@"career.png" title:@"My Job"];
    [myJob addTarget:self forButtonAction:@selector(onTabItemClicked:)];
    myJob.tag = 2;
    
    CustomBarButtonItem *myTask = [[CustomBarButtonItem alloc] initWithImageName:@"my-task.png" title:@"My Task"];
    [myTask addTarget:self forButtonAction:@selector(onTabItemClicked:)];
    myTask.tag = 3;
    
    CustomBarButtonItem *myPerformance = [[CustomBarButtonItem alloc] initWithImageName:@"performance.png" title:@"My Performance"];
    [myPerformance addTarget:self forButtonAction:@selector(onTabItemClicked:)];
    myPerformance.tag = 4;
    
    CustomBarButtonItem *myTeam = [[CustomBarButtonItem alloc] initWithImageName:@"my-team.png" title:@"My Team"];
    [myTeam addTarget:self forButtonAction:@selector(onTabItemClicked:)];
    myTeam.tag = 5;
    
    CustomBarButtonItem *selfService = [[CustomBarButtonItem alloc] initWithImageName:@"self-service.png" title:@"Self Service"];
    [selfService addTarget:self forButtonAction:@selector(onTabItemClicked:)];
    selfService.tag = 6;
    
    CustomBarButtonItem *eLearning = [[CustomBarButtonItem alloc] initWithImageName:@"e-learning.png" title:@"E-Learning"];
    [eLearning addTarget:self forButtonAction:@selector(onTabItemClicked:)];
    eLearning.tag = 7;
    
    UIBarButtonItem *flex1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *fix = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fix.width = 15;
    
    UIBarButtonItem *dashboardItem = [[UIBarButtonItem alloc] initWithCustomView:dashboard];
    dashboardItem.tag = 0;
    UIBarButtonItem *profileItem = [[UIBarButtonItem alloc] initWithCustomView:myProfile];
    profileItem.tag = 1;
    UIBarButtonItem *myJobItem = [[UIBarButtonItem alloc] initWithCustomView:myJob];
    myJobItem.tag = 2;
    UIBarButtonItem *myTaskItem = [[UIBarButtonItem alloc] initWithCustomView:myTask];
    myTaskItem.tag = 3;
    UIBarButtonItem *myTeamItem = [[UIBarButtonItem alloc] initWithCustomView:myTeam];
    myTeamItem.tag = 4;
    UIBarButtonItem *myPerformanceItem = [[UIBarButtonItem alloc] initWithCustomView:myPerformance];
    myPerformanceItem.tag = 5;
    UIBarButtonItem *selfServiceItem = [[UIBarButtonItem alloc] initWithCustomView:selfService];
    selfServiceItem.tag = 6;
    UIBarButtonItem *eLearningItem = [[UIBarButtonItem alloc] initWithCustomView:eLearning];
    eLearningItem.tag = 7;
    
    NSArray *array = @[flex1, dashboardItem, fix, profileItem, fix, myJobItem, fix, /*myTaskItem, fix, myPerformanceItem, fix, myTeamItem, fix, */ selfServiceItem, /*fix, eLearningItem, */flex1];
    
    [myToolbar setItems:array];
    
    itemArray = @[dashboardItem, profileItem, myJobItem, myTaskItem, myTeamItem, myPerformanceItem, selfServiceItem, eLearningItem];
    
    [self addMyToolbar:myToolbar];
    
    CustomSplitViewController *root = (CustomSplitViewController *)app.window.rootViewController;
    root.isLandscapeOK = NO;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if (self.isLandscapeOK) {
        return UIInterfaceOrientationMaskAll;
    }
    return UIInterfaceOrientationMaskPortrait;
}

-(void)selectTab:(TabItem)tab
{
    int tabIndex = (int)tab;
    UIBarButtonItem *item = itemArray[tabIndex];
    
    [self onTabItemClicked:item];
}

-(void)setSelectedBackground:(CustomBarButtonItem*)item
{
    item.backgroundColor = [UIColor colorWithRed:209/255 green:209/255 blue:209/255 alpha:0.3];
}

-(void)unsetSelectedBackgrund:(CustomBarButtonItem*)item
{
    item.backgroundColor = [UIColor clearColor];
}


#pragma mark - mytoolbar methods
-(void)onTabItemClicked:(id)sender
{
    CustomBarButtonItem *item = nil;
    if ([sender isKindOfClass:[UIBarButtonItem class]])
        item = (CustomBarButtonItem *)((UIBarButtonItem *)sender).customView;
    else
    {
        UIButton *button = (UIButton *)sender;
        item = (CustomBarButtonItem*)button.superview;
    }
    
    if (_selectedItem != item)
    {        
        [self unsetSelectedBackgrund:_selectedItem];
        _selectedItem = item;
        [self setSelectedBackground:_selectedItem];
        
        switch (item.tag) {
            case Dashboard:
                if (!self.isLandscape && self.isShowingMaster)
                {
                    [self toggleMasterView:self];
                }
                [app.rootMaster replaceToDashboard];
                break;
            case Profile:
                [app.rootMaster replaceToProfile];
                break;
            case Job:
                [app.rootMaster replaceToMyJob];
                break;
            case Task:
                [app.rootMaster replaceToMyTask];
                break;
            case Team:
                [app.rootMaster replaceToMyTeam];
                break;
            case Performance:
                [app.rootMaster replacePerformance];
                break;
            case SelfService:
                [app.rootMaster replaceToSelfService];
                break;
            default:
                [app.rootMaster replaceELearning];
                break;
        }
        
        if (item.tag != Dashboard)
        {
            self.showsMasterInLandscape = YES;
            [self setMyToolBarHidden:NO animation:NO];
        }
        else
        {
            self.showsMasterInLandscape = NO;
            [self setMyToolBarHidden:YES animation:NO];
        }
        
        app.otherEmpId = nil;
        app.otherEmpName = nil;
    }
}

//
//-(void)myPerformanceClicked:(id)sender
//{
//    CustomBarButtonItem *item = (CustomBarButtonItem*)((UIButton*)sender).superview;
//    if (_selectedItem != item)
//    {
//        [self unsetSelectedBackgrund:_selectedItem];
//        _selectedItem = item;
//        [self setSelectedBackground:_selectedItem];
//        
//        self.showsMasterInLandscape = YES;
//        [app.rootMaster replacePerformance];
//    }
//}
//
//-(void)myProfileItemClicked:(id)sender
//{
//    CustomBarButtonItem *item = (CustomBarButtonItem*)((UIButton*)sender).superview;
//    if (_selectedItem != item)
//    {
//        [self unsetSelectedBackgrund:_selectedItem];
//        _selectedItem = item;
//        [self setSelectedBackground:_selectedItem];
//        
//        self.showsMasterInLandscape = YES;
//        [app.rootMaster replaceToProfile];
//    }
//}
//
//-(void)myTaskItemClicked:(id)sender
//{
//    CustomBarButtonItem *item = (CustomBarButtonItem*)((UIButton*)sender).superview;
//    if (_selectedItem != item)
//    {
//        [self unsetSelectedBackgrund:_selectedItem];
//        _selectedItem = item;
//        [self setSelectedBackground:_selectedItem];
//        
//        self.showsMasterInLandscape = YES;
//        [app.rootMaster replaceToMyTask];
//    }
//}
//
//-(void)myTeamItemClicked:(id)sender
//{
//    CustomBarButtonItem *item = (CustomBarButtonItem*)((UIButton*)sender).superview;
//    if (_selectedItem != item)
//    {
//        [self unsetSelectedBackgrund:_selectedItem];
//        _selectedItem = item;
//        [self setSelectedBackground:_selectedItem];
//        
//        self.showsMasterInLandscape = YES;
//        [app.rootMaster replaceToMyTeam];
//    }
//}
//
//-(void)selfServiceItemClicked:(id)sender
//{
//    CustomBarButtonItem *item = (CustomBarButtonItem*)((UIButton*)sender).superview;
//    if (_selectedItem != item)
//    {
//        [self unsetSelectedBackgrund:_selectedItem];
//        _selectedItem = item;
//        [self setSelectedBackground:_selectedItem];
//        
//        self.showsMasterInLandscape = YES;
//        [app.rootMaster replaceToSelfService];
//    }
//}
//
//
//
//-(void)dashboardItemClicked:(id)sender
//{
//    CustomBarButtonItem *item = (CustomBarButtonItem*)((UIButton*)sender).superview;
//    if (_selectedItem != item)
//    {
//        [self unsetSelectedBackgrund:_selectedItem];
//        _selectedItem = item;
//        [self setSelectedBackground:_selectedItem];
//        
//        CustomSplitViewController *splitVC = (CustomSplitViewController*)app.window.rootViewController;
//        BaseNavigationViewController *nav = (BaseNavigationViewController*) splitVC.detailViewController;
//        [nav performSegueWithIdentifier:@"ToDashBoard" sender:self];
//        self.showsMasterInLandscape = NO;
//    }
//}
//
//-(void)myJobItemClicked:(id)sender
//{
//    CustomBarButtonItem *item = (CustomBarButtonItem*)((UIButton*)sender).superview;
//    if (_selectedItem != item)
//    {
//        [self unsetSelectedBackgrund:_selectedItem];
//        _selectedItem = item;
//        [self setSelectedBackground:_selectedItem];
//        
//        self.showsMasterInLandscape = YES;
//        [app.rootMaster replaceToMyJob];
//    }
//}
//
//-(void)eLearningClicked:(id)sender
//{
//    CustomBarButtonItem *item = (CustomBarButtonItem*)((UIButton*)sender).superview;
//    if (_selectedItem != item)
//    {
//        [self unsetSelectedBackgrund:_selectedItem];
//        _selectedItem = item;
//        [self setSelectedBackground:_selectedItem];
//        
//        self.showsMasterInLandscape = YES;
//        [app.rootMaster replaceELearning];
//    }
//}

-(void)toMainPage
{
    UIBarButtonItem *dashboard = itemArray[Dashboard];
    [self onTabItemClicked:dashboard];
}


@end
