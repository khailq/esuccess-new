//
//  ApplicationSharedData.m
//  eSuccess
//
//  Created by HPTVIETNAM on 4/19/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "ApplicationSharedData.h"

static ApplicationSharedData *instance;

@implementation ApplicationSharedData

+(ApplicationSharedData *)getInstance
{
    if (instance == nil)
        instance = [[ApplicationSharedData alloc] init];

    return instance;
}

-(void)performSegueWithIdentifier:(NSString *)segueID
{
    if (![segueID isEqualToString:@""])
    {
        [self.router performSegueWithIdentifier:segueID sender:self.router];
    }
}
@end
