//
//  RewardViewController.h
//  eSuccess
//
//  Created by admin on 5/23/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"
#import "EmpProfileLayerServiceSvc.h"
@interface RewardViewController : BaseDetailTableViewController<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>
@property(nonatomic, retain) NSMutableArray *rewardArr;
@property(nonatomic, retain) NSMutableArray *titleArr;
@end
