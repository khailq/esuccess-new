//
//  DayLeaveDetailCell.h
//  eSuccess
//
//  Created by HPTVIETNAM on 4/23/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DayLeaveDetailCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lbLeft;
@property (strong, nonatomic) IBOutlet UILabel *lbRight;

@end
