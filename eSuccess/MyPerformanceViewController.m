//
//  MyPerformanceViewController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 5/28/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "MyPerformanceViewController.h"

@interface MyPerformanceViewController ()

@end

@implementation MyPerformanceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.name = MyPerformanceName;
    [super viewDidLoad];
    self.title = @"My Performance";
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
