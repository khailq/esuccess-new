//
//  AppDelegate.h
//  eSuccess
//
//  Created by HPTVIETNAM on 4/19/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//  test

#import <UIKit/UIKit.h>
#import "tns2.h"
#import "SecurityServiceSvc.h"
#import "RootMasterController.h"
#import "EmpProfileLayerServiceSvc.h"

#define ViewEmployeeModeNotification @"ViewEmployee"
#define EndViewEmployeeModeNotification @"StopViewEmployee"
#define NoDataString @"Không có dữ liệu"
#define BaseUrl @"http://10.0.18.105:1919/"
#define AvatarUrl @"http://10.0.18.105:5000/CDN/Content/Employees/"
#define BeginViewProfileNotification @"BeginViewProfle"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

//@property (strong, nonatomic) tns2_Employee *loginEmployee;

@property (strong, nonatomic) tns1_SysUser *sysUser;
@property (strong, nonatomic) empl_tns1_V_EmpBasicProfile *basicEmpProfile;

@property (strong, nonatomic) RootMasterController *rootMaster;

//@property (strong, nonatomic) UIPopoverController *masterPopoverController;
//@property (strong, nonatomic) UIBarButtonItem *leftBarButtonItem;
@property (strong, nonatomic) NSMutableDictionary *masterControllerDictionary;
@property (strong, nonatomic) NSMutableDictionary *selectedIndexDictionary;

// Id of another employee that her profile is being viewed.
@property (strong, nonatomic) NSNumber *otherEmpId;
@property (strong, nonatomic) NSString *otherEmpName;

-(void)setOtherEmployeeInfo:(NSNumber*)empId name:(NSString*)name;
-(void)toMainPage;

//@property(nonatomic, strong)



@end
