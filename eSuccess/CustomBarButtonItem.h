//
//  CustomBarButtonItem.h
//  eSuccess
//
//  Created by Scott on 4/30/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomBarButtonItem : UIView
@property (strong, nonatomic) UIButton *button;
@property (strong, nonatomic) UILabel *label;

-(void)addTarget:(id)btnTarget forButtonAction:(SEL)action;
- (id)initWithImageName:(NSString*)imageName title:(NSString*)labelContent;
@end
