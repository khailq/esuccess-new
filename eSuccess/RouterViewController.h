//
//  RouterViewController.h
//  eSuccess
//
//  Created by Scott on 4/22/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DetailViewProtocol <NSObject>

@property (strong, nonatomic) UIPopoverController *masterPopoverController;
@property (strong, nonatomic) UIBarButtonItem *leftBarButtonItem;

@end

@interface RouterViewController : UIViewController <DetailViewProtocol>


@end
