//
//  LeaveRequestSubmitModel.h
//  eSuccess
//
//  Created by HPTVIETNAM on 9/24/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LeaveRequestSubmitModel : NSObject

@property (strong, nonatomic) NSNumber * Id;
@property (strong, nonatomic) NSNumber * EmpId;
@property (strong, nonatomic) NSString * EmpCode;
@property (strong, nonatomic) NSNumber * TimeAbsenceTypeId;
@property (strong, nonatomic) NSString * StartDate;
@property (strong, nonatomic) NSString * EndDate;
@property (strong, nonatomic) NSNumber * NumberDayOff;
@property (strong, nonatomic) NSString * Address;
@property (strong, nonatomic) NSString * Phone;
@property (strong, nonatomic) NSNumber * ApproverId;
@property (strong, nonatomic) NSString * Reason;
@property (strong, nonatomic) NSString * CreatedDate;
@property (strong, nonatomic) NSArray * DayLeave;
@property (strong, nonatomic) NSArray * LeaveStatus;
@property (strong, nonatomic) NSString * NotSalary;
@property (strong, nonatomic) NSString * MinusAttendance;
@property (strong, nonatomic) NSNumber * UserId;
@property (assign, nonatomic) BOOL ApprovedNow;
@property (strong, nonatomic) NSString * ReturnUrl;
@property (assign, nonatomic) BOOL isDraft;
@property (strong, nonatomic) NSString * ApprovalStatus;
@property (strong, nonatomic) NSArray * LeaveRequestDetail;

@end
