//
//  DisciplineViewController.h
//  eSuccess
//
//  Created by admin on 5/23/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"
#import "EmpProfileLayerServiceSvc.h"
@interface DisciplineViewController : BaseDetailTableViewController<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>
@property(nonatomic, retain) NSMutableArray *disciplineArr;
@property(nonatomic, retain) NSMutableArray *titleArr;
@end
