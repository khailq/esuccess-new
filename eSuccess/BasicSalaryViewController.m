//
//  BasicSalaryViewController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 6/22/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BasicSalaryViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "DayLeaveDetailCell.h"
#import "M13Checkbox.h"
@interface BasicSalaryViewController ()

@end

@implementation BasicSalaryViewController
@synthesize salaryArr;
@synthesize qoutaArr;
static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Biến động lương";
    [self getData];
    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 130;
//}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (salaryArr.count == 0)
        return @"";
    
    empl_tns1_V_EmpProfileBaseSalary *salary = salaryArr[section];
    return salary.SalaryName;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [salaryArr count] > 0 ? salaryArr.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [salaryArr count] > 0 ? 5 : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (salaryArr.count == 0)
        return [self getDefaultEmptyCell];
    
    DayLeaveDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
    if (cell == nil) {
        cell = [[DayLeaveDetailCell alloc]init];
    }
    cell.lbLeft.font = [UIFont systemFontOfSize:14];
    cell.lbLeft.backgroundColor = [UIColor clearColor];
    cell.lbRight.font = [UIFont boldSystemFontOfSize:16];
    cell.lbRight.backgroundColor = [UIColor clearColor];
    
    int section = indexPath.section;
    empl_tns1_V_EmpProfileBaseSalary *salary = [salaryArr objectAtIndex:section];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    NSString *leftLabel, *rightContent;
    switch (indexPath.row) {
        case 0:
            leftLabel = @"Bậc";
            rightContent = salary.GradeCoefficient;
            break;
        case 1:
            leftLabel = @"Hệ số";
            rightContent = salary.Coefficient.stringValue;
            break;
        case 2:
            leftLabel = @"Mức lương";
            rightContent = salary.PayScaleSalaryValue.stringValue;
            break;
        case 3:
            leftLabel = @"Ngày áp dụng";
            rightContent = [formatter stringFromDate:salary.ActivedDate];
            break;
        default:
            leftLabel = @"Ghi chú";
            rightContent = salary.Note;
            break;
    }
    
    cell.lbLeft.text = leftLabel;
    cell.lbRight.text = rightContent;
    
    return cell;
}

#pragma getData
- (void)getData
{
    salaryArr = [[NSMutableArray alloc]init];
    qoutaArr = [[NSMutableArray alloc]init];
    
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    NSNumber *empId;
    if (app.otherEmpId != nil)
        empId = app.otherEmpId;
    else
        empId = app.sysUser.EmployeeId ;
    
    [self getEmpProfileBasicSalaryById:empId];
}

- (void) getEmpProfileBasicSalaryById:(NSNumber *)empId
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Đang cập nhật";
    
    BasicHttpBinding_IEmpProfileLayerServiceBinding *binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
    EmpProfileLayerServiceSvc_GetViewEmpProfileBaseSalaryByView *request = [EmpProfileLayerServiceSvc_GetViewEmpProfileBaseSalaryByView new];
    request.employeeId = empId;
    
    [binding GetViewEmpProfileBaseSalaryByViewAsyncUsingParameters:request delegate:self];
}

- (void)operation:(BasicHttpBinding_IEmpProfileLayerServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)response
{
    if ([response.bodyParts count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi kết nối" message:@"Không thể kết nối đến server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    for (id mine in response.bodyParts){
        if ([mine isKindOfClass:[EmpProfileLayerServiceSvc_GetViewEmpProfileBaseSalaryByViewResponse class]])
        {
            empl_tns1_ArrayOfV_EmpProfileBaseSalary *result = [mine GetViewEmpProfileBaseSalaryByViewResult];
            for(empl_tns1_V_EmpProfileBaseSalary *salary in [result V_EmpProfileBaseSalary])
            {
                [salaryArr addObject:salary];
            }
            if (!self.viewDidDisappear && self != nil)
                [self.tableView reloadData];
            
        }
     
        if ([mine isKindOfClass:[SOAPFault class]])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Server error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    }
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

@end
