//
//  LeaveRequestCell.h
//  eSuccess
//
//  Created by HPTVIETNAM on 4/26/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BButton.h"

@interface LeaveRequestCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lb_name;
@property (strong, nonatomic) IBOutlet UILabel *lb_unitName;
@property (strong, nonatomic) IBOutlet UILabel *lb_fromDate;
@property (strong, nonatomic) IBOutlet UILabel *lb_toDate;
@property (strong, nonatomic) IBOutlet UILabel *lb_absenceType;
@property (strong, nonatomic) IBOutlet UILabel *lb_reason;

@property (strong, nonatomic) IBOutlet BButton *btnAccept;
@property (strong, nonatomic) IBOutlet BButton *btnDeny;

@property (assign, nonatomic) BOOL isAcceptSelected;
@property (assign, nonatomic) BOOL isDenySelected;

@property (retain, nonatomic) NSNumber *leaveRequestId;

-(IBAction)btnAccept_clicked:(id)sender;
-(IBAction)btnDeny_clicked:(id)sender;

-(void)addTarget:(id)target forAcceptAction:(SEL)acceptSel denyAction:(SEL)denySel;
@end
