//
//  UnitLogViewController.h
//  eSuccess
//
//  Created by admin on 5/11/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"
#import "EmpProfileLayerServiceSvc.h"
@interface UnitLogViewController : BaseDetailTableViewController<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>
@property(nonatomic, strong) NSMutableArray *processWorkArr;
@property(nonatomic, strong) NSMutableArray *orgUnitNameArr;
@end
