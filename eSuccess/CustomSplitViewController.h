//
//  CustomSplitViewController.h
//  eSuccess
//
//  Created by Scott on 4/21/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSplitViewController.h"

typedef enum _TabItem
{
    Dashboard = 0,
    Profile = 1,
    Job = 2,
    Task = 3,
    Performance = 4,
    Team = 5,
    SelfService = 6,
    Learning = 7
} TabItem;

@interface CustomSplitViewController : MGSplitViewController

@property (assign) BOOL isLandscapeOK;
-(void)toMainPage;
-(void)selectTab:(TabItem)tab;
@end
