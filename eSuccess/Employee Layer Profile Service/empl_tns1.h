#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class empl_tns1_V_EmpProfileQualification;
@class empl_tns1_ArrayOfV_EmpProfileForeignLanguage;
@class empl_tns1_V_EmpProfileForeignLanguage;
@class empl_tns1_ArrayOfEmpProfileComputingSkill;
@class empl_tns1_EmpProfileComputingSkill;
@class empl_tns1_Employee;
@class empl_tns1_ArrayOfCBAccidentInsurance;
@class empl_tns1_ArrayOfCBCompensationEmployeeGroupDetail;
@class empl_tns1_ArrayOfCBConvalescence;
@class empl_tns1_ArrayOfCBDayOffSocialInsurance;
@class empl_tns1_ArrayOfCBFactorEmployeeMetaData;
@class empl_tns1_ArrayOfCBHealthInsuranceDetail;
@class empl_tns1_CLogCity;
@class empl_tns1_CLogEmployeeType;
@class empl_tns1_ArrayOfCLogTrainer;
@class empl_tns1_ArrayOfEmpBasicProfile;
@class empl_tns1_ArrayOfEmpCompetency;
@class empl_tns1_ArrayOfEmpCompetencyRating;
@class empl_tns1_ArrayOfEmpContract;
@class empl_tns1_ArrayOfEmpPerformanceAppraisal;
@class empl_tns1_ArrayOfEmpProfileAllowance;
@class empl_tns1_ArrayOfEmpProfileBaseSalary;
@class empl_tns1_ArrayOfEmpProfileBenefit;
@class empl_tns1_ArrayOfEmpProfileComment;
@class empl_tns1_ArrayOfEmpProfileContact;
@class empl_tns1_ArrayOfEmpProfileDegree;
@class empl_tns1_ArrayOfEmpProfileDiscipline;
@class empl_tns1_ArrayOfEmpProfileEducation;
@class empl_tns1_ArrayOfEmpProfileEquipment;
@class empl_tns1_ArrayOfEmpProfileExperience;
@class empl_tns1_ArrayOfEmpProfileFamilyRelationship;
@class empl_tns1_ArrayOfEmpProfileForeignLanguage;
@class empl_tns1_ArrayOfEmpProfileHealthInsurance;
@class empl_tns1_ArrayOfEmpProfileHealthy;
@class empl_tns1_ArrayOfEmpProfileHobby;
@class empl_tns1_ArrayOfEmpProfileInternalCourse;
@class empl_tns1_ArrayOfEmpProfileJobPosition;
@class empl_tns1_ArrayOfEmpProfileLeaveRegime;
@class empl_tns1_ArrayOfEmpProfileOtherBenefit;
@class empl_tns1_ArrayOfEmpProfileParticipation;
@class empl_tns1_ArrayOfEmpProfilePersonality;
@class empl_tns1_ArrayOfEmpProfileProcessOfWork;
@class empl_tns1_ArrayOfEmpProfileQualification;
@class empl_tns1_ArrayOfEmpProfileReward;
@class empl_tns1_ArrayOfEmpProfileSkill;
@class empl_tns1_ArrayOfEmpProfileTotalIncome;
@class empl_tns1_ArrayOfEmpProfileTraining;
@class empl_tns1_ArrayOfEmpProfileWageType;
@class empl_tns1_ArrayOfEmpProfileWorkingExperience;
@class empl_tns1_ArrayOfEmpProfileWorkingForm;
@class empl_tns1_ArrayOfEmpSocialInsuranceSalary;
@class empl_tns1_ArrayOfGPAdditionAppraisal;
@class empl_tns1_ArrayOfGPCompanyScoreCard;
@class empl_tns1_ArrayOfGPCompanyStrategicGoal;
@class empl_tns1_ArrayOfGPCompanyYearlyObjective;
@class empl_tns1_ArrayOfGPEmployeeScoreCard;
@class empl_tns1_ArrayOfGPIndividualYearlyObjective;
@class empl_tns1_ArrayOfGPObjectiveInitiative;
@class empl_tns1_ArrayOfGPPerformanceYearEndResult;
@class empl_tns1_ArrayOfLMSCourseAttendee;
@class empl_tns1_ArrayOfLMSCourseTranscript;
@class empl_tns1_ArrayOfNCClientConnection;
@class empl_tns1_ArrayOfNCMessage;
@class empl_tns1_ArrayOfProjectMember;
@class empl_tns1_ArrayOfProject;
@class empl_tns1_ArrayOfRecInterviewSchedule;
@class empl_tns1_ArrayOfRecInterviewer;
@class empl_tns1_ArrayOfRecPhaseEmpDisplaced;
@class empl_tns1_ArrayOfRecRecruitmentRequirement;
@class empl_tns1_ArrayOfRecRequirementEmpDisplaced;
@class empl_tns1_ReportEmployeeRequestedBook;
@class empl_tns1_ArrayOfSelfLeaveRequest;
@class empl_tns1_ArrayOfSysRecPlanApprover;
@class empl_tns1_ArrayOfSysRecRequirementApprover;
@class empl_tns1_ArrayOfSysTrainingApprover;
@class empl_tns1_ArrayOfTask;
@class empl_tns1_ArrayOfTrainingCourseEmployee;
@class empl_tns1_ArrayOfTrainingEmpPlan;
@class empl_tns1_ArrayOfTrainingPlanEmployee;
@class empl_tns1_CBAccidentInsurance;
@class empl_tns1_CBCompensationEmployeeGroupDetail;
@class empl_tns1_CBCompensationEmployeeGroup;
@class empl_tns1_CBFactorEmployeeMetaData;
@class empl_tns1_CLogCBFactor;
@class empl_tns1_ArrayOfCBCompensationTypeFactor;
@class empl_tns1_CLogCBCompensationCategory;
@class empl_tns1_CBCompensationTypeFactor;
@class empl_tns1_ArrayOfCLogCBFactor;
@class empl_tns1_CBConvalescence;
@class empl_tns1_CBDayOffSocialInsurance;
@class empl_tns1_CLogCBDisease;
@class empl_tns1_CLogHospital;
@class empl_tns1_ArrayOfCBHealthInsurance;
@class empl_tns1_CBHealthInsurance;
@class empl_tns1_CLogCountry;
@class empl_tns1_ArrayOfCLogDistrict;
@class empl_tns1_ArrayOfCLogHospital;
@class empl_tns1_ArrayOfEmployee;
@class empl_tns1_ArrayOfCLogCity;
@class empl_tns1_CLogDistrict;
@class empl_tns1_CBHealthInsuranceDetail;
@class empl_tns1_CLogTrainer;
@class empl_tns1_Address;
@class empl_tns1_ArrayOfTrainingCourse;
@class empl_tns1_ArrayOfOrgUnit;
@class empl_tns1_EmpContract;
@class empl_tns1_Employer;
@class empl_tns1_OrgJob;
@class empl_tns1_OrgJobPosition;
@class empl_tns1_ArrayOfOrgJobPosition;
@class empl_tns1_ArrayOfOrgJobSpecificCompetency;
@class empl_tns1_ArrayOfOrgUnitJob;
@class empl_tns1_ArrayOfOrgJobPositionRequiredProficency;
@class empl_tns1_ArrayOfOrgJobPositionSpecificCompetency;
@class empl_tns1_ArrayOfRecPlanJobPosition;
@class empl_tns1_ArrayOfRecProcessApplyForJobPosition;
@class empl_tns1_ArrayOfRecRecruitmentPhaseJobPosition;
@class empl_tns1_ArrayOfRecRequirementJobPosition;
@class empl_tns1_EmpProfileJobPosition;
@class empl_tns1_OrgJobPositionRequiredProficency;
@class empl_tns1_OrgJobPositionSpecificCompetency;
@class empl_tns1_OrgProficencyLevel;
@class empl_tns1_CLogRating;
@class empl_tns1_ArrayOfEmpProficencyLevelRating;
@class empl_tns1_OrgCompetency;
@class empl_tns1_ArrayOfOrgProficencyLevelDetail;
@class empl_tns1_ArrayOfRecCandidateProficencyLevelRating;
@class empl_tns1_ArrayOfEmpProficencyDetailRating;
@class empl_tns1_ArrayOfEmpProficencyLevelDetailRating;
@class empl_tns1_ArrayOfLMSCourseCompetency;
@class empl_tns1_ArrayOfLMSCourseRequiredCompetency;
@class empl_tns1_ArrayOfOrgCoreCompetency;
@class empl_tns1_ArrayOfOrgProficencyDetail;
@class empl_tns1_ArrayOfOrgProficencyLevel;
@class empl_tns1_ArrayOfRecCanProficencyDetailRating;
@class empl_tns1_ArrayOfRecCanProficencyLevelDetailRating;
@class empl_tns1_ArrayOfRecCandidateCompetencyRating;
@class empl_tns1_ArrayOfRecCandidateExperience;
@class empl_tns1_ArrayOfRecCandidateQualification;
@class empl_tns1_ArrayOfRecCandidateSkill;
@class empl_tns1_ArrayOfTrainingEmpProficency;
@class empl_tns1_ArrayOfTrainingProficencyExpected;
@class empl_tns1_ArrayOfTrainingProficencyRequire;
@class empl_tns1_EmpCompetency;
@class empl_tns1_OrgCoreCompetency;
@class empl_tns1_EmpProficencyDetailRating;
@class empl_tns1_EmpProficencyLevelDetailRating;
@class empl_tns1_OrgProficencyDetail;
@class empl_tns1_EmpProficencyLevelRating;
@class empl_tns1_OrgProficencyLevelDetail;
@class empl_tns1_EmpCompetencyRating;
@class empl_tns1_OrgProficencyType;
@class empl_tns1_RecCanProficencyDetailRating;
@class empl_tns1_RecCanProficencyLevelDetailRating;
@class empl_tns1_RecCandidateProficencyLevelRating;
@class empl_tns1_RecCandidateCompetencyRating;
@class empl_tns1_RecCandidate;
@class empl_tns1_ArrayOfRecCandidateApplication;
@class empl_tns1_ArrayOfRecCandidateDegree;
@class empl_tns1_ArrayOfRecCandidateFamilyRelationship;
@class empl_tns1_ArrayOfRecCandidateForeignLanguage;
@class empl_tns1_RecCandidateStatu;
@class empl_tns1_RecCandidateSupplier;
@class empl_tns1_ArrayOfRecProcessPhaseResult;
@class empl_tns1_RecCandidateApplication;
@class empl_tns1_RecCandidateProfileStatu;
@class empl_tns1_RecRecruitmentPhaseJobPosition;
@class empl_tns1_RecInterviewSchedule;
@class empl_tns1_RecGroupInterviewer;
@class empl_tns1_RecInterviewPhase;
@class empl_tns1_RecInterviewScheduleStatu;
@class empl_tns1_ArrayOfRecScheduleInterviewerResult;
@class empl_tns1_RecInterviewer;
@class empl_tns1_RecScheduleInterviewerResult;
@class empl_tns1_RecInterviewPhaseEvaluation;
@class empl_tns1_RecEvaluationCriterion;
@class empl_tns1_RecGroupEvaluationCriterion;
@class empl_tns1_ArrayOfRecInterviewPhaseEvaluation;
@class empl_tns1_ArrayOfRecEvaluationCriterion;
@class empl_tns1_RecRecruitmentProcessPhase;
@class empl_tns1_ArrayOfRecInterviewPhase;
@class empl_tns1_RecRecruitmentProcess;
@class empl_tns1_RecProcessPhaseResult;
@class empl_tns1_ArrayOfRecRecruitmentProcessPhase;
@class empl_tns1_RecProcessApplyForJobPosition;
@class empl_tns1_RecRecruitmentPhase;
@class empl_tns1_RecPhaseStatu;
@class empl_tns1_RecRecruitmentRequirement;
@class empl_tns1_RecPhaseEmpDisplaced;
@class empl_tns1_ArrayOfRecRecruitmentPhase;
@class empl_tns1_RecRecruitmentPlan;
@class empl_tns1_ArrayOfRecRequirementApproveHistory;
@class empl_tns1_RecRequirementStatu;
@class empl_tns1_ArrayOfRecPlanApproveHistory;
@class empl_tns1_RecPlanStatu;
@class empl_tns1_RecPlanApproveHistory;
@class empl_tns1_SysRecPlanApprover;
@class empl_tns1_RecPlanJobPosition;
@class empl_tns1_ArrayOfRecRecruitmentPlan;
@class empl_tns1_RecRequirementApproveHistory;
@class empl_tns1_SysRecRequirementApprover;
@class empl_tns1_RecRequirementEmpDisplaced;
@class empl_tns1_RecRequirementJobPosition;
@class empl_tns1_RecCandidateDegree;
@class empl_tns1_CLogDegree;
@class empl_tns1_RecCandidateExperience;
@class empl_tns1_OrgExperience;
@class empl_tns1_OrgProjectType;
@class empl_tns1_OrgTimeInCharge;
@class empl_tns1_ArrayOfOrgExperience;
@class empl_tns1_RecCandidateFamilyRelationship;
@class empl_tns1_CLogFamilyRelationship;
@class empl_tns1_EmpProfileFamilyRelationship;
@class empl_tns1_ArrayOfCBPITDependent;
@class empl_tns1_ArrayOfEmpBeneficiary;
@class empl_tns1_CBPITDependent;
@class empl_tns1_EmpBeneficiary;
@class empl_tns1_RecCandidateForeignLanguage;
@class empl_tns1_RecCandidateQualification;
@class empl_tns1_OrgQualification;
@class empl_tns1_EmpProfileQualification;
@class empl_tns1_RecCandidateSkill;
@class empl_tns1_OrgSkill;
@class empl_tns1_OrgSkillType;
@class empl_tns1_EmpProfileSkill;
@class empl_tns1_ArrayOfOrgSkill;
@class empl_tns1_TrainingEmpProficency;
@class empl_tns1_TrainingCourseEmployee;
@class empl_tns1_TrainingCourseSchedule;
@class empl_tns1_CLogCourseSchedule;
@class empl_tns1_TrainingCourse;
@class empl_tns1_ArrayOfTrainingCourseSchedule;
@class empl_tns1_CLogTrainingCenter;
@class empl_tns1_TrainingCategory;
@class empl_tns1_ArrayOfTrainingCourseChapter;
@class empl_tns1_TrainingCoursePeriod;
@class empl_tns1_TrainingCourseType;
@class empl_tns1_ArrayOfTrainingCourseUnit;
@class empl_tns1_ArrayOfTrainingPlanDegree;
@class empl_tns1_TrainingPlanRequest;
@class empl_tns1_TrainingCourseChapter;
@class empl_tns1_TrainingCourseUnit;
@class empl_tns1_TrainingPlanDegree;
@class empl_tns1_CLogMajor;
@class empl_tns1_ArrayOfTrainingPlanRequest;
@class empl_tns1_EmpProfileEducation;
@class empl_tns1_TrainingPlanEmployee;
@class empl_tns1_TrainingProficencyExpected;
@class empl_tns1_TrainingProficencyRequire;
@class empl_tns1_ArrayOfRecCandidate;
@class empl_tns1_RecCandidateTypeSupplier;
@class empl_tns1_ArrayOfRecCandidateSupplier;
@class empl_tns1_ArrayOfTrainingPlanProfiency;
@class empl_tns1_TrainingPlanProfiency;
@class empl_tns1_EmpProfileWorkingExperience;
@class empl_tns1_LMSCourseAttendee;
@class empl_tns1_LMSCourse;
@class empl_tns1_CLogCourseStatu;
@class empl_tns1_GPCompanyYearlyObjective;
@class empl_tns1_GPObjectiveInitiative;
@class empl_tns1_LMSContentTopic;
@class empl_tns1_ArrayOfLMSCourseContent;
@class empl_tns1_ArrayOfLMSCourseDetail;
@class empl_tns1_ArrayOfLMSCourseProgressUnit;
@class empl_tns1_LMSCourseType;
@class empl_tns1_ArrayOfLMSCourse;
@class empl_tns1_GPCompanyStrategicGoal;
@class empl_tns1_GPPerspective;
@class empl_tns1_GPStrategy;
@class empl_tns1_GPCompanyScoreCard;
@class empl_tns1_GPPerspectiveMeasure;
@class empl_tns1_GPEmployeeScoreCard;
@class empl_tns1_ArrayOfGPEmployeeScoreCardAppraisal;
@class empl_tns1_ArrayOfGPEmployeeScoreCardProgress;
@class empl_tns1_GPPerformanceExecutionReport;
@class empl_tns1_GPEmployeeScoreCardAppraisal;
@class empl_tns1_GPEmployeeScoreCardProgress;
@class empl_tns1_GPIndividualYearlyObjective;
@class empl_tns1_ArrayOfGPExecutionPlanDetail;
@class empl_tns1_ArrayOfGPIndividualYearlyObjectiveAppraisal;
@class empl_tns1_GPExecutionPlanDetail;
@class empl_tns1_GPExecutionPlan;
@class empl_tns1_ArrayOfGPExecutionPlanActivity;
@class empl_tns1_ArrayOfGPEmployeeWholePlanAppraisal;
@class empl_tns1_GPAdditionAppraisal;
@class empl_tns1_GPEmployeeWholePlanAppraisal;
@class empl_tns1_GPPerformanceYearEndResult;
@class empl_tns1_GPExecutionPlanActivity;
@class empl_tns1_ArrayOfGPExecutionPlanActivityAppraisal;
@class empl_tns1_ArrayOfGPPeriodicReport;
@class empl_tns1_GPExecutionPlanActivityAppraisal;
@class empl_tns1_GPPeriodicReport;
@class empl_tns1_GPIndividualYearlyObjectiveAppraisal;
@class empl_tns1_ArrayOfGPPerspectiveMeasure;
@class empl_tns1_ArrayOfGPPerspectiveValue;
@class empl_tns1_ArrayOfGPCompanyStrategicGoalDetail;
@class empl_tns1_GPStrategyMapObject;
@class empl_tns1_GPCompanyStrategicGoalDetail;
@class empl_tns1_GPPerspectiveValue;
@class empl_tns1_ArrayOfGPPerspectiveValueDetail;
@class empl_tns1_GPPerspectiveValueDetail;
@class empl_tns1_ArrayOfGPStrategyMapObject;
@class empl_tns1_ArrayOfLMSTopicCompetency;
@class empl_tns1_LMSTopicGroup;
@class empl_tns1_LMSTopicCompetency;
@class empl_tns1_OrgCompetencyGroup;
@class empl_tns1_LMSCourseCompetency;
@class empl_tns1_LMSCourseRequiredCompetency;
@class empl_tns1_ArrayOfOrgCompetency;
@class empl_tns1_OrgJobSpecificCompetency;
@class empl_tns1_ArrayOfLMSContentTopic;
@class empl_tns1_LMSCourseContent;
@class empl_tns1_CLogLMSContentType;
@class empl_tns1_LMSCourseDetail;
@class empl_tns1_LMSCourseProgressUnit;
@class empl_tns1_OrgUnit;
@class empl_tns1_LMSCourseTranscript;
@class empl_tns1_OrgUnitJob;
@class empl_tns1_EmpBasicProfile;
@class empl_tns1_CLogEmpWorkingStatu;
@class empl_tns1_EmpPerformanceAppraisal;
@class empl_tns1_EmpProfileAllowance;
@class empl_tns1_EmpProfileBaseSalary;
@class empl_tns1_EmpProfileBenefit;
@class empl_tns1_EmpProfileComment;
@class empl_tns1_EmpProfileContact;
@class empl_tns1_EmpProfileDegree;
@class empl_tns1_OrgDegree;
@class empl_tns1_EmpProfileDiscipline;
@class empl_tns1_OrgDisciplineMaster;
@class empl_tns1_ArrayOfOrgDisciplineForUnit;
@class empl_tns1_OrgDisciplineForUnit;
@class empl_tns1_EmpProfileEquipment;
@class empl_tns1_CLogEquipment;
@class empl_tns1_EmpProfileExperience;
@class empl_tns1_EmpProfileForeignLanguage;
@class empl_tns1_CLogLanguage;
@class empl_tns1_EmpProfileHealthInsurance;
@class empl_tns1_EmpProfileHealthy;
@class empl_tns1_EmpProfileHobby;
@class empl_tns1_CLogHobby;
@class empl_tns1_EmpProfileInternalCourse;
@class empl_tns1_EmpProfileLeaveRegime;
@class empl_tns1_EmpProfileOtherBenefit;
@class empl_tns1_EmpOtherBenefit;
@class empl_tns1_EmpProfileParticipation;
@class empl_tns1_EmpProfilePersonality;
@class empl_tns1_CLogPersonality;
@class empl_tns1_EmpProfileProcessOfWork;
@class empl_tns1_EmpProfileReward;
@class empl_tns1_OrgRewardMaster;
@class empl_tns1_ArrayOfOrgRewardForUnit;
@class empl_tns1_OrgRewardForUnit;
@class empl_tns1_EmpProfileTotalIncome;
@class empl_tns1_EmpProfileTraining;
@class empl_tns1_EmpProfileWageType;
@class empl_tns1_EmpProfileWorkingForm;
@class empl_tns1_EmpSocialInsuranceSalary;
@class empl_tns1_NCClientConnection;
@class empl_tns1_NCMessage;
@class empl_tns1_NCMessageType;
@class empl_tns1_ProjectMember;
@class empl_tns1_Project;
@class empl_tns1_SelfLeaveRequest;
@class empl_tns1_SysTrainingApprover;
@class empl_tns1_ArrayOfTrainingEmpPlanApproved;
@class empl_tns1_TrainingEmpPlanApproved;
@class empl_tns1_TrainingEmpPlan;
@class empl_tns1_Task;
@class empl_tns1_ArrayOfV_EmpProfileSkill;
@class empl_tns1_V_EmpProfileSkill;
@class empl_tns1_ArrayOfV_EmpProfileDegree;
@class empl_tns1_V_EmpProfileDegree;
@class empl_tns1_ArrayOfV_EmpProfileKnowledge;
@class empl_tns1_V_EmpProfileKnowledge;
@class empl_tns1_ArrayOfV_CBFactorEmployeeMetaDataWithGroup;
@class empl_tns1_V_CBFactorEmployeeMetaDataWithGroup;
@class empl_tns1_ArrayOfV_CBFactorEmployeeMetaData;
@class empl_tns1_V_CBFactorEmployeeMetaData;
@class empl_tns1_ArrayOfEmpProfileMaternityRegime;
@class empl_tns1_EmpProfileMaternityRegime;
@class empl_tns1_V_EmpBasicInformation;
@class empl_tns1_ArrayOfV_EmpProfileQualification;
@class empl_tns1_ArrayOfV_EmpProfileWorkingExperience;
@class empl_tns1_V_EmpProfileWorkingExperience;
@class empl_tns1_ArrayOfV_ESS_SYS_PROFILE_TREE;
@class empl_tns1_V_ESS_SYS_PROFILE_TREE;
@class empl_tns1_ArrayOfTB_ESS_EMPLOYEE_GROUP_MAPPING;
@class empl_tns1_TB_ESS_EMPLOYEE_GROUP_MAPPING;
@class empl_tns1_ArrayOfV_ESS_EMPLOYEE_GROUP_MAPPING;
@class empl_tns1_V_ESS_EMPLOYEE_GROUP_MAPPING;
@class empl_tns1_ArrayOfV_ESS_CLOG_EMPLOYEE_GROUP;
@class empl_tns1_V_ESS_CLOG_EMPLOYEE_GROUP;
@class empl_tns1_ArrayOfCBSalaryScale;
@class empl_tns1_CBSalaryScale;
@class empl_tns1_ArrayOfCBSalaryByCoefficient;
@class empl_tns1_CBSalaryByCoefficient;
@class empl_tns1_CBSalaryGrade;
@class empl_tns1_CBSalaryLevel;
@class empl_tns1_ArrayOfCBSalaryLevel;
@class empl_tns1_ArrayOfCBSalaryGrade;
@class empl_tns1_ArrayOfV_CBSummarization;
@class empl_tns1_V_CBSummarization;
@class empl_tns1_ArrayOfV_EmpProfileAllowance;
@class empl_tns1_V_EmpProfileAllowance;
@class empl_tns1_ArrayOfV_EmpProfileBaseSalary;
@class empl_tns1_V_EmpProfileBaseSalary;
@class empl_tns1_ArrayOfV_EmpProfileTotalIncome;
@class empl_tns1_V_EmpProfileTotalIncome;
@class empl_tns1_ArrayOfSP_GET_PERMIS_EMP_PROF_LAYER_Result;
@class empl_tns1_SP_GET_PERMIS_EMP_PROF_LAYER_Result;
@class empl_tns1_ArrayOfV_SysEmpProfileGroupAndLayer;
@class empl_tns1_V_SysEmpProfileGroupAndLayer;
@class empl_tns1_ArrayOfSP_GET_EMP_PROF_LAYER_PERMIS_Result;
@class empl_tns1_SP_GET_EMP_PROF_LAYER_PERMIS_Result;
@class empl_tns1_V_EmpBasicProfile;
@class empl_tns1_ArrayOfV_EmpProfilePersonality;
@class empl_tns1_V_EmpProfilePersonality;
@class empl_tns1_ArrayOfV_EmpProfileHobby;
@class empl_tns1_V_EmpProfileHobby;
@class empl_tns1_ArrayOfV_EmpSocialInsuranceSalary;
@class empl_tns1_V_EmpSocialInsuranceSalary;
@class empl_tns1_ArrayOfEmpProfileBiography;
@class empl_tns1_EmpProfileBiography;
@class empl_tns1_ArrayOfV_EmpProfileFamilyRelationship;
@class empl_tns1_V_EmpProfileFamilyRelationship;
@class empl_tns1_ArrayOfV_EmpProfileContact;
@class empl_tns1_V_EmpProfileContact;
@class empl_tns1_ArrayOfV_EmpProfileJobPosition;
@class empl_tns1_V_EmpProfileJobPosition;
@class empl_tns1_ArrayOfV_EmpProfileProcessOfWork;
@class empl_tns1_V_EmpProfileProcessOfWork;
@class empl_tns1_ArrayOfV_EmpContract;
@class empl_tns1_V_EmpContract;
@class empl_tns1_V_ESS_ORG_POSITION_ALL_DESC;
@class empl_tns1_ArrayOfV_ESS_REQ_EQUIPMENT_MASTER;
@class empl_tns1_V_ESS_REQ_EQUIPMENT_MASTER;
@class empl_tns1_ArrayOfV_ESS_REQ_EQUIPMENT_DETAIL;
@class empl_tns1_V_ESS_REQ_EQUIPMENT_DETAIL;
@class empl_tns1_ArrayOfV_EmpProfileEquipment;
@class empl_tns1_V_EmpProfileEquipment;
@class empl_tns1_ArrayOfV_EmpProfileAssets;
@class empl_tns1_V_EmpProfileAssets;
@class empl_tns1_ArrayOfV_EmpProfileWageType;
@class empl_tns1_V_EmpProfileWageType;
@class empl_tns1_ArrayOfV_EmpProfileWorkingForm;
@class empl_tns1_V_EmpProfileWorkingForm;
@class empl_tns1_ArrayOfV_EmpProfileComment;
@class empl_tns1_V_EmpProfileComment;
@class empl_tns1_ArrayOfV_EmpProfileDiscipline;
@class empl_tns1_V_EmpProfileDiscipline;
@class empl_tns1_ArrayOfV_EmpPerformanceAppraisal;
@class empl_tns1_V_EmpPerformanceAppraisal;
@class empl_tns1_ArrayOfEmpProfileSocialInsurance;
@class empl_tns1_EmpProfileSocialInsurance;
@class empl_tns1_ArrayOfV_EmpProfileHealthInsurance;
@class empl_tns1_V_EmpProfileHealthInsurance;
@class empl_tns1_V_EmpProfileOtherBenefit;
#import "empl_tns5.h"
@interface empl_tns1_V_EmpProfileQualification : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CLogEducationLevelId;
	NSString * CLogMajorCode;
	NSNumber * CLogMajorId;
	NSString * CLogMajorName;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	NSString * EducationLevelName;
	NSNumber * EmpProfileQualificationId;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsValId;
	NSDate * ModifiedDate;
	NSNumber * OrgDegreeRankId;
	NSString * OrgDegreeRankName;
	NSString * OrgQualificationCode;
	NSString * OrgQualificationDescription;
	NSNumber * OrgQualificationId;
	NSString * OrgQualificationName;
	NSString * Other;
	NSString * PlaceIssue;
	NSNumber * RelateRowId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpProfileQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CLogEducationLevelId;
@property (retain) NSString * CLogMajorCode;
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSString * CLogMajorName;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) NSString * EducationLevelName;
@property (retain) NSNumber * EmpProfileQualificationId;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgDegreeRankId;
@property (retain) NSString * OrgDegreeRankName;
@property (retain) NSString * OrgQualificationCode;
@property (retain) NSString * OrgQualificationDescription;
@property (retain) NSNumber * OrgQualificationId;
@property (retain) NSString * OrgQualificationName;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) NSNumber * RelateRowId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpProfileForeignLanguage : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * AttachmenttUrl;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DateOfIssue;
	NSString * DegreeName;
	NSNumber * EffectiveTime;
	NSNumber * EmpProfileForeignLanguageId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSString * LanguageCode;
	NSNumber * LanguageId;
	NSString * LanguageName;
	NSString * LanguageNote;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * OrgDegreeRankId;
	NSString * OrgDegreeRankName;
	NSNumber * OrgDegreeRankPriority;
	NSString * PlaceOfIssue;
	NSNumber * RelateRowId;
	NSString * TrainingCenter;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpProfileForeignLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * AttachmenttUrl;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateOfIssue;
@property (retain) NSString * DegreeName;
@property (retain) NSNumber * EffectiveTime;
@property (retain) NSNumber * EmpProfileForeignLanguageId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * LanguageCode;
@property (retain) NSNumber * LanguageId;
@property (retain) NSString * LanguageName;
@property (retain) NSString * LanguageNote;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * OrgDegreeRankId;
@property (retain) NSString * OrgDegreeRankName;
@property (retain) NSNumber * OrgDegreeRankPriority;
@property (retain) NSString * PlaceOfIssue;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * TrainingCenter;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_EmpProfileForeignLanguage : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileForeignLanguage;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_EmpProfileForeignLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileForeignLanguage:(empl_tns1_V_EmpProfileForeignLanguage *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileForeignLanguage;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CBAccidentInsurance : NSObject {
	
/* elements */
	NSString * AccidentInsuranceContractCode;
	NSString * AccidentInsuranceContractNumber;
	NSString * AccidentName;
	NSNumber * CBAccidentInsuranceId;
	NSNumber * Charge;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * EffectDate;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSDate * ExpireDate;
	NSString * InsuranceCompanyName;
	NSString * InsuranceKind;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CBAccidentInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AccidentInsuranceContractCode;
@property (retain) NSString * AccidentInsuranceContractNumber;
@property (retain) NSString * AccidentName;
@property (retain) NSNumber * CBAccidentInsuranceId;
@property (retain) NSNumber * Charge;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EffectDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpireDate;
@property (retain) NSString * InsuranceCompanyName;
@property (retain) NSString * InsuranceKind;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfCBAccidentInsurance : NSObject {
	
/* elements */
	NSMutableArray *CBAccidentInsurance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfCBAccidentInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBAccidentInsurance:(empl_tns1_CBAccidentInsurance *)toAdd;
@property (readonly) NSMutableArray * CBAccidentInsurance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CBCompensationTypeFactor : NSObject {
	
/* elements */
	NSNumber * CBCompensationTypeFactorId;
	NSNumber * CLogCBCompensationTypeId;
	empl_tns1_CLogCBFactor * CLogCBFactor;
	NSNumber * CLogCBFactorId;
	USBoolean * IsDeleted;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CBCompensationTypeFactor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBCompensationTypeFactorId;
@property (retain) NSNumber * CLogCBCompensationTypeId;
@property (retain) empl_tns1_CLogCBFactor * CLogCBFactor;
@property (retain) NSNumber * CLogCBFactorId;
@property (retain) USBoolean * IsDeleted;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfCBCompensationTypeFactor : NSObject {
	
/* elements */
	NSMutableArray *CBCompensationTypeFactor;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfCBCompensationTypeFactor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBCompensationTypeFactor:(empl_tns1_CBCompensationTypeFactor *)toAdd;
@property (readonly) NSMutableArray * CBCompensationTypeFactor;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfCLogCBFactor : NSObject {
	
/* elements */
	NSMutableArray *CLogCBFactor;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfCLogCBFactor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogCBFactor:(empl_tns1_CLogCBFactor *)toAdd;
@property (readonly) NSMutableArray * CLogCBFactor;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CLogCBCompensationCategory : NSObject {
	
/* elements */
	NSNumber * CLogCBCompensationCategoryId;
	empl_tns1_ArrayOfCLogCBFactor * CLogCBFactors;
	NSString * CLogCompensationCategoryCode;
	NSString * Description;
	USBoolean * IsDeleted;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CLogCBCompensationCategory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogCBCompensationCategoryId;
@property (retain) empl_tns1_ArrayOfCLogCBFactor * CLogCBFactors;
@property (retain) NSString * CLogCompensationCategoryCode;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CLogCBFactor : NSObject {
	
/* elements */
	empl_tns1_ArrayOfCBCompensationTypeFactor * CBCompensationTypeFactors;
	empl_tns1_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
	empl_tns1_CLogCBCompensationCategory * CLogCBCompensationCategory;
	NSNumber * CLogCBCompensationCategoryId;
	NSString * CLogCBFactorCode;
	NSNumber * CLogCBFactorGroupId;
	NSNumber * CLogCBFactorId;
	NSString * Description;
	NSString * FormulaExpression;
	USBoolean * IsActived;
	USBoolean * IsDeactived;
	USBoolean * IsDeleted;
	USBoolean * IsInitial;
	NSString * Name;
	NSNumber * Priority;
	NSString * ServiceReferenceEndPointAddress;
	NSString * ServiceReferenceEndPointOperationContract;
	NSString * TableReference;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CLogCBFactor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfCBCompensationTypeFactor * CBCompensationTypeFactors;
@property (retain) empl_tns1_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
@property (retain) empl_tns1_CLogCBCompensationCategory * CLogCBCompensationCategory;
@property (retain) NSNumber * CLogCBCompensationCategoryId;
@property (retain) NSString * CLogCBFactorCode;
@property (retain) NSNumber * CLogCBFactorGroupId;
@property (retain) NSNumber * CLogCBFactorId;
@property (retain) NSString * Description;
@property (retain) NSString * FormulaExpression;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeactived;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInitial;
@property (retain) NSString * Name;
@property (retain) NSNumber * Priority;
@property (retain) NSString * ServiceReferenceEndPointAddress;
@property (retain) NSString * ServiceReferenceEndPointOperationContract;
@property (retain) NSString * TableReference;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CBFactorEmployeeMetaData : NSObject {
	
/* elements */
	NSDate * BeginDate;
	empl_tns1_CBCompensationEmployeeGroup * CBCompensationEmployeeGroup;
	NSNumber * CBCompensationEmployeeGroupId;
	NSString * CBFactorEmployeeMetaDataDescription;
	NSNumber * CBFactorEmployeeMetaDataId;
	empl_tns1_CLogCBFactor * CLogCBFactor;
	NSNumber * CLogCBFactorId;
	NSString * ChangeReason;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	NSNumber * FactorValue;
	NSString * GroupGuid;
	USBoolean * IsActivated;
	USBoolean * IsAppliedForCompany;
	USBoolean * IsChanged;
	USBoolean * IsCurrent;
	USBoolean * IsDeactivated;
	USBoolean * IsDeleted;
	USBoolean * IsInitialed;
	NSDate * ModifiedDate;
	NSNumber * OldFactorValue;
	NSString * ReferenceColunmName;
	NSString * ReferenceRowGuid;
	NSString * ReferenceTableName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CBFactorEmployeeMetaData *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * BeginDate;
@property (retain) empl_tns1_CBCompensationEmployeeGroup * CBCompensationEmployeeGroup;
@property (retain) NSNumber * CBCompensationEmployeeGroupId;
@property (retain) NSString * CBFactorEmployeeMetaDataDescription;
@property (retain) NSNumber * CBFactorEmployeeMetaDataId;
@property (retain) empl_tns1_CLogCBFactor * CLogCBFactor;
@property (retain) NSNumber * CLogCBFactorId;
@property (retain) NSString * ChangeReason;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) NSNumber * FactorValue;
@property (retain) NSString * GroupGuid;
@property (retain) USBoolean * IsActivated;
@property (retain) USBoolean * IsAppliedForCompany;
@property (retain) USBoolean * IsChanged;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeactivated;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInitialed;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OldFactorValue;
@property (retain) NSString * ReferenceColunmName;
@property (retain) NSString * ReferenceRowGuid;
@property (retain) NSString * ReferenceTableName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfCBFactorEmployeeMetaData : NSObject {
	
/* elements */
	NSMutableArray *CBFactorEmployeeMetaData;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfCBFactorEmployeeMetaData *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBFactorEmployeeMetaData:(empl_tns1_CBFactorEmployeeMetaData *)toAdd;
@property (readonly) NSMutableArray * CBFactorEmployeeMetaData;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CBCompensationEmployeeGroup : NSObject {
	
/* elements */
	empl_tns1_ArrayOfCBCompensationEmployeeGroupDetail * CBCompensationEmployeeGroupDetails;
	NSNumber * CBCompensationEmployeeGroupId;
	empl_tns1_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
	NSNumber * CompanyId;
	NSString * Description;
	USBoolean * IsActived;
	USBoolean * IsDeactived;
	USBoolean * IsDeleted;
	USBoolean * IsInitial;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CBCompensationEmployeeGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfCBCompensationEmployeeGroupDetail * CBCompensationEmployeeGroupDetails;
@property (retain) NSNumber * CBCompensationEmployeeGroupId;
@property (retain) empl_tns1_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeactived;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInitial;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CBCompensationEmployeeGroupDetail : NSObject {
	
/* elements */
	empl_tns1_CBCompensationEmployeeGroup * CBCompensationEmployeeGroup;
	NSNumber * CBCompensationEmployeeGroupDetailId;
	NSNumber * CBCompensationEmployeeGroupId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CBCompensationEmployeeGroupDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_CBCompensationEmployeeGroup * CBCompensationEmployeeGroup;
@property (retain) NSNumber * CBCompensationEmployeeGroupDetailId;
@property (retain) NSNumber * CBCompensationEmployeeGroupId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfCBCompensationEmployeeGroupDetail : NSObject {
	
/* elements */
	NSMutableArray *CBCompensationEmployeeGroupDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfCBCompensationEmployeeGroupDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBCompensationEmployeeGroupDetail:(empl_tns1_CBCompensationEmployeeGroupDetail *)toAdd;
@property (readonly) NSMutableArray * CBCompensationEmployeeGroupDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CBConvalescence : NSObject {
	
/* elements */
	NSNumber * AllowanceAmount;
	NSNumber * CBConvalescenceId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSDate * FromDate;
	NSNumber * HealthLessLevel;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * NumberDayOff;
	NSDate * NumberOfMounthApplied;
	NSDate * ToDate;
	NSNumber * TypeId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CBConvalescence *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AllowanceAmount;
@property (retain) NSNumber * CBConvalescenceId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * FromDate;
@property (retain) NSNumber * HealthLessLevel;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * NumberDayOff;
@property (retain) NSDate * NumberOfMounthApplied;
@property (retain) NSDate * ToDate;
@property (retain) NSNumber * TypeId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfCBConvalescence : NSObject {
	
/* elements */
	NSMutableArray *CBConvalescence;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfCBConvalescence *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBConvalescence:(empl_tns1_CBConvalescence *)toAdd;
@property (readonly) NSMutableArray * CBConvalescence;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CLogCBDisease : NSObject {
	
/* elements */
	empl_tns1_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
	NSNumber * CLogCBDiseaseId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	NSString * DiseaseCode;
	NSString * DiseaseGroupCode;
	USBoolean * IsChronic;
	USBoolean * IsDeleted;
	USBoolean * IsOccupational;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CLogCBDisease *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
@property (retain) NSNumber * CLogCBDiseaseId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) NSString * DiseaseCode;
@property (retain) NSString * DiseaseGroupCode;
@property (retain) USBoolean * IsChronic;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOccupational;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CBHealthInsurance : NSObject {
	
/* elements */
	NSNumber * CBHealthInsuranceId;
	empl_tns1_CLogHospital * CLogHospital;
	NSNumber * CLogHospitalId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * EffectDate;
	NSDate * ExpireDate;
	NSNumber * GrossCharge;
	NSString * HealthInsuranceContractCode;
	NSString * HealthInsuranceContractNumber;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Remark;
	NSNumber * SignerId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CBHealthInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBHealthInsuranceId;
@property (retain) empl_tns1_CLogHospital * CLogHospital;
@property (retain) NSNumber * CLogHospitalId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EffectDate;
@property (retain) NSDate * ExpireDate;
@property (retain) NSNumber * GrossCharge;
@property (retain) NSString * HealthInsuranceContractCode;
@property (retain) NSString * HealthInsuranceContractNumber;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Remark;
@property (retain) NSNumber * SignerId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfCBHealthInsurance : NSObject {
	
/* elements */
	NSMutableArray *CBHealthInsurance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfCBHealthInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBHealthInsurance:(empl_tns1_CBHealthInsurance *)toAdd;
@property (readonly) NSMutableArray * CBHealthInsurance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfCLogCity : NSObject {
	
/* elements */
	NSMutableArray *CLogCity;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfCLogCity *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogCity:(empl_tns1_CLogCity *)toAdd;
@property (readonly) NSMutableArray * CLogCity;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CLogCountry : NSObject {
	
/* elements */
	empl_tns1_ArrayOfCLogCity * CLogCities;
	NSString * CLogCountryCode;
	NSNumber * CLogCountryId;
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CLogCountry *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfCLogCity * CLogCities;
@property (retain) NSString * CLogCountryCode;
@property (retain) NSNumber * CLogCountryId;
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CLogDistrict : NSObject {
	
/* elements */
	empl_tns1_CLogCity * CLogCity;
	NSString * CLogCityCode;
	NSNumber * CLogCityId;
	NSString * CLogDistrictCode;
	NSNumber * CLogDistrictId;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CLogDistrict *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_CLogCity * CLogCity;
@property (retain) NSString * CLogCityCode;
@property (retain) NSNumber * CLogCityId;
@property (retain) NSString * CLogDistrictCode;
@property (retain) NSNumber * CLogDistrictId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfCLogDistrict : NSObject {
	
/* elements */
	NSMutableArray *CLogDistrict;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfCLogDistrict *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogDistrict:(empl_tns1_CLogDistrict *)toAdd;
@property (readonly) NSMutableArray * CLogDistrict;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfCLogHospital : NSObject {
	
/* elements */
	NSMutableArray *CLogHospital;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfCLogHospital *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogHospital:(empl_tns1_CLogHospital *)toAdd;
@property (readonly) NSMutableArray * CLogHospital;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmployee : NSObject {
	
/* elements */
	NSMutableArray *Employee;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmployee:(empl_tns1_Employee *)toAdd;
@property (readonly) NSMutableArray * Employee;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CLogCity : NSObject {
	
/* elements */
	NSString * CLogCityCode;
	NSNumber * CLogCityId;
	NSString * CLogCityNoIns;
	empl_tns1_CLogCountry * CLogCountry;
	NSString * CLogCountryCode;
	NSNumber * CLogCountryId;
	empl_tns1_ArrayOfCLogDistrict * CLogDistricts;
	empl_tns1_ArrayOfCLogHospital * CLogHospitals;
	NSNumber * CompanyId;
	empl_tns1_ArrayOfEmployee * Employees;
	empl_tns1_ArrayOfEmployee * Employees1;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
	NSString * SysNo;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CLogCity *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogCityCode;
@property (retain) NSNumber * CLogCityId;
@property (retain) NSString * CLogCityNoIns;
@property (retain) empl_tns1_CLogCountry * CLogCountry;
@property (retain) NSString * CLogCountryCode;
@property (retain) NSNumber * CLogCountryId;
@property (retain) empl_tns1_ArrayOfCLogDistrict * CLogDistricts;
@property (retain) empl_tns1_ArrayOfCLogHospital * CLogHospitals;
@property (retain) NSNumber * CompanyId;
@property (retain) empl_tns1_ArrayOfEmployee * Employees;
@property (retain) empl_tns1_ArrayOfEmployee * Employees1;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
@property (retain) NSString * SysNo;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CLogHospital : NSObject {
	
/* elements */
	NSString * Address;
	empl_tns1_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
	empl_tns1_ArrayOfCBHealthInsurance * CBHealthInsurances;
	empl_tns1_CLogCity * CLogCity;
	NSNumber * CLogCityId;
	NSString * CLogCityNoIns;
	NSString * CLogHospitalCode;
	NSNumber * CLogHospitalId;
	NSNumber * CompanyId;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CLogHospital *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) empl_tns1_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
@property (retain) empl_tns1_ArrayOfCBHealthInsurance * CBHealthInsurances;
@property (retain) empl_tns1_CLogCity * CLogCity;
@property (retain) NSNumber * CLogCityId;
@property (retain) NSString * CLogCityNoIns;
@property (retain) NSString * CLogHospitalCode;
@property (retain) NSNumber * CLogHospitalId;
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CBDayOffSocialInsurance : NSObject {
	
/* elements */
	NSNumber * CBDayOffSocialInsuranceId;
	empl_tns1_CLogCBDisease * CLogCBDisease;
	NSNumber * CLogCBDiseaseId;
	empl_tns1_CLogHospital * CLogHospital;
	NSNumber * CLogHospitalId;
	NSNumber * ClogCBDayOffTypeId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * DoctorName;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSDate * FromDate;
	USBoolean * IsDeleted;
	USBoolean * IsPaid;
	USBoolean * IsWorkAccident;
	NSDate * ModifiedDate;
	NSNumber * NumberDayOff;
	NSNumber * NumberDayOffByDoctor;
	NSNumber * NumberPaidDay;
	NSString * Reason;
	NSDate * ToDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CBDayOffSocialInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBDayOffSocialInsuranceId;
@property (retain) empl_tns1_CLogCBDisease * CLogCBDisease;
@property (retain) NSNumber * CLogCBDiseaseId;
@property (retain) empl_tns1_CLogHospital * CLogHospital;
@property (retain) NSNumber * CLogHospitalId;
@property (retain) NSNumber * ClogCBDayOffTypeId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DoctorName;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * FromDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsPaid;
@property (retain) USBoolean * IsWorkAccident;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NumberDayOff;
@property (retain) NSNumber * NumberDayOffByDoctor;
@property (retain) NSNumber * NumberPaidDay;
@property (retain) NSString * Reason;
@property (retain) NSDate * ToDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfCBDayOffSocialInsurance : NSObject {
	
/* elements */
	NSMutableArray *CBDayOffSocialInsurance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfCBDayOffSocialInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBDayOffSocialInsurance:(empl_tns1_CBDayOffSocialInsurance *)toAdd;
@property (readonly) NSMutableArray * CBDayOffSocialInsurance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CBHealthInsuranceDetail : NSObject {
	
/* elements */
	NSNumber * CBHealthInsuranceDetailId;
	NSNumber * CBHealthInsuranceId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CBHealthInsuranceDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBHealthInsuranceDetailId;
@property (retain) NSNumber * CBHealthInsuranceId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfCBHealthInsuranceDetail : NSObject {
	
/* elements */
	NSMutableArray *CBHealthInsuranceDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfCBHealthInsuranceDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBHealthInsuranceDetail:(empl_tns1_CBHealthInsuranceDetail *)toAdd;
@property (readonly) NSMutableArray * CBHealthInsuranceDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CLogEmployeeType : NSObject {
	
/* elements */
	NSString * CLogEmployeeTypeCode;
	NSNumber * CLogEmployeeTypeId;
	empl_tns1_ArrayOfEmployee * Employees;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CLogEmployeeType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogEmployeeTypeCode;
@property (retain) NSNumber * CLogEmployeeTypeId;
@property (retain) empl_tns1_ArrayOfEmployee * Employees;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_Employer : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_ArrayOfEmpContract * EmpContracts;
	NSString * EmployerCode;
	NSNumber * EmployerId;
	NSString * FullName;
	USBoolean * IsDeleted;
	NSString * JobPositionName;
	NSDate * ModifiedDate;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_Employer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_ArrayOfEmpContract * EmpContracts;
@property (retain) NSString * EmployerCode;
@property (retain) NSNumber * EmployerId;
@property (retain) NSString * FullName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * JobPositionName;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * DecisionFile;
	NSString * DecisionName;
	NSString * DecisionNo;
	NSString * DirectReportToEmployeeCode;
	NSNumber * DirectReportToEmployeeId;
	NSString * EmpJobPositionCode;
	NSNumber * EmpProfileJobPositionId;
	empl_tns1_Employee * Employee;
	empl_tns1_Employee * Employee1;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ExpirationDate;
	NSDate * ImplementationDate;
	USBoolean * IsActive;
	USBoolean * IsConcurrentPost;
	USBoolean * IsDeleted;
	USBoolean * IsMainPost;
	USBoolean * IsPromoted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * OrgJobExternalTitleCode;
	empl_tns1_OrgJobPosition * OrgJobPosition;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSNumber * PercentParticipation;
	NSString * Reason;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DecisionFile;
@property (retain) NSString * DecisionName;
@property (retain) NSString * DecisionNo;
@property (retain) NSString * DirectReportToEmployeeCode;
@property (retain) NSNumber * DirectReportToEmployeeId;
@property (retain) NSString * EmpJobPositionCode;
@property (retain) NSNumber * EmpProfileJobPositionId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) empl_tns1_Employee * Employee1;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsConcurrentPost;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainPost;
@property (retain) USBoolean * IsPromoted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * OrgJobExternalTitleCode;
@property (retain) empl_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * PercentParticipation;
@property (retain) NSString * Reason;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileJobPosition : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileJobPosition:(empl_tns1_EmpProfileJobPosition *)toAdd;
@property (readonly) NSMutableArray * EmpProfileJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_OrgJobPositionRequiredProficency : NSObject {
	
/* elements */
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	empl_tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSNumber * OrgJobPositionRequiredProficencyId;
	NSString * ProficiencyTypeCode;
	NSNumber * RecordId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_OrgJobPositionRequiredProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * OrgJobPositionRequiredProficencyId;
@property (retain) NSString * ProficiencyTypeCode;
@property (retain) NSNumber * RecordId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfOrgJobPositionRequiredProficency : NSObject {
	
/* elements */
	NSMutableArray *OrgJobPositionRequiredProficency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfOrgJobPositionRequiredProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgJobPositionRequiredProficency:(empl_tns1_OrgJobPositionRequiredProficency *)toAdd;
@property (readonly) NSMutableArray * OrgJobPositionRequiredProficency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_OrgCoreCompetency : NSObject {
	
/* elements */
	empl_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompetencyLevel;
	NSDate * CreatedDate;
	NSString * Definition;
	empl_tns1_ArrayOfEmpCompetency * EmpCompetencies;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * OrgCoreCompetencyId;
	NSString * Statement;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_OrgCoreCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompetencyLevel;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Definition;
@property (retain) empl_tns1_ArrayOfEmpCompetency * EmpCompetencies;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * OrgCoreCompetencyId;
@property (retain) NSString * Statement;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpCompetency : NSObject {
	
/* elements */
	empl_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpCompetencyId;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	empl_tns1_OrgCoreCompetency * OrgCoreCompetency;
	NSNumber * OrgCoreCompetencyId;
	empl_tns1_OrgProficencyLevel * OrgProficencyLevel;
	NSNumber * OrgProficencyLevelId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpCompetencyId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_OrgCoreCompetency * OrgCoreCompetency;
@property (retain) NSNumber * OrgCoreCompetencyId;
@property (retain) empl_tns1_OrgProficencyLevel * OrgProficencyLevel;
@property (retain) NSNumber * OrgProficencyLevelId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpCompetency : NSObject {
	
/* elements */
	NSMutableArray *EmpCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpCompetency:(empl_tns1_EmpCompetency *)toAdd;
@property (readonly) NSMutableArray * EmpCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProficencyLevelRating : NSObject {
	
/* elements */
	NSMutableArray *EmpProficencyLevelRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProficencyLevelRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProficencyLevelRating:(empl_tns1_EmpProficencyLevelRating *)toAdd;
@property (readonly) NSMutableArray * EmpProficencyLevelRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpCompetencyRating : NSObject {
	
/* elements */
	NSDate * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpCompetencyRatingId;
	empl_tns1_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpCompetencyRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpCompetencyRatingId;
@property (retain) empl_tns1_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProficencyLevelDetailRating : NSObject {
	
/* elements */
	NSMutableArray *EmpProficencyLevelDetailRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProficencyLevelDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProficencyLevelDetailRating:(empl_tns1_EmpProficencyLevelDetailRating *)toAdd;
@property (readonly) NSMutableArray * EmpProficencyLevelDetailRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProficencyLevelRating : NSObject {
	
/* elements */
	NSDate * AppraisalDate;
	empl_tns1_CLogRating * CLogRating;
	NSDate * CreatedDate;
	empl_tns1_EmpCompetencyRating * EmpCompetencyRating;
	NSNumber * EmpCompetencyRatingId;
	empl_tns1_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
	NSNumber * EmpProficencyLevelRatingId;
	NSString * ManagerNote;
	NSNumber * ManagerRating;
	empl_tns1_OrgProficencyLevel * OrgProficencyLevel;
	NSNumber * OrgProficencyLevelId;
	NSNumber * PeriodicallyAssessment;
	NSNumber * Score;
	NSString * SelfNote;
	NSNumber * SelfRating;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProficencyLevelRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AppraisalDate;
@property (retain) empl_tns1_CLogRating * CLogRating;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_EmpCompetencyRating * EmpCompetencyRating;
@property (retain) NSNumber * EmpCompetencyRatingId;
@property (retain) empl_tns1_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
@property (retain) NSNumber * EmpProficencyLevelRatingId;
@property (retain) NSString * ManagerNote;
@property (retain) NSNumber * ManagerRating;
@property (retain) empl_tns1_OrgProficencyLevel * OrgProficencyLevel;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) NSNumber * PeriodicallyAssessment;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfNote;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecCanProficencyLevelDetailRating : NSObject {
	
/* elements */
	NSMutableArray *RecCanProficencyLevelDetailRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecCanProficencyLevelDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCanProficencyLevelDetailRating:(empl_tns1_RecCanProficencyLevelDetailRating *)toAdd;
@property (readonly) NSMutableArray * RecCanProficencyLevelDetailRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecCandidateProfileStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * Priority;
	NSString * ProfileStatusCode;
	NSString * ProfileStatusName;
	empl_tns1_ArrayOfRecCandidateApplication * RecCandidateApplications;
	NSNumber * RecCandidateProfileStatusId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecCandidateProfileStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Priority;
@property (retain) NSString * ProfileStatusCode;
@property (retain) NSString * ProfileStatusName;
@property (retain) empl_tns1_ArrayOfRecCandidateApplication * RecCandidateApplications;
@property (retain) NSNumber * RecCandidateProfileStatusId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecEvaluationCriterion : NSObject {
	
/* elements */
	NSMutableArray *RecEvaluationCriterion;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecEvaluationCriterion *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecEvaluationCriterion:(empl_tns1_RecEvaluationCriterion *)toAdd;
@property (readonly) NSMutableArray * RecEvaluationCriterion;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecGroupEvaluationCriterion : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * Priority;
	empl_tns1_ArrayOfRecEvaluationCriterion * RecEvaluationCriterions;
	NSString * RecGroupEvalDesc;
	NSString * RecGroupEvalName;
	NSNumber * RecGroupEvaluationCriterionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecGroupEvaluationCriterion *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Priority;
@property (retain) empl_tns1_ArrayOfRecEvaluationCriterion * RecEvaluationCriterions;
@property (retain) NSString * RecGroupEvalDesc;
@property (retain) NSString * RecGroupEvalName;
@property (retain) NSNumber * RecGroupEvaluationCriterionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecInterviewPhaseEvaluation : NSObject {
	
/* elements */
	NSMutableArray *RecInterviewPhaseEvaluation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecInterviewPhaseEvaluation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecInterviewPhaseEvaluation:(empl_tns1_RecInterviewPhaseEvaluation *)toAdd;
@property (readonly) NSMutableArray * RecInterviewPhaseEvaluation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecEvaluationCriterion : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * MaxPoint;
	NSNumber * Priority;
	NSString * RecEvalDesc;
	NSString * RecEvalName;
	NSNumber * RecEvaluationCriterionId;
	empl_tns1_RecGroupEvaluationCriterion * RecGroupEvaluationCriterion;
	NSNumber * RecGroupEvaluationCriterionId;
	empl_tns1_ArrayOfRecInterviewPhaseEvaluation * RecInterviewPhaseEvaluations;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecEvaluationCriterion *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * MaxPoint;
@property (retain) NSNumber * Priority;
@property (retain) NSString * RecEvalDesc;
@property (retain) NSString * RecEvalName;
@property (retain) NSNumber * RecEvaluationCriterionId;
@property (retain) empl_tns1_RecGroupEvaluationCriterion * RecGroupEvaluationCriterion;
@property (retain) NSNumber * RecGroupEvaluationCriterionId;
@property (retain) empl_tns1_ArrayOfRecInterviewPhaseEvaluation * RecInterviewPhaseEvaluations;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecInterviewPhase : NSObject {
	
/* elements */
	NSMutableArray *RecInterviewPhase;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecInterviewPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecInterviewPhase:(empl_tns1_RecInterviewPhase *)toAdd;
@property (readonly) NSMutableArray * RecInterviewPhase;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecProcessPhaseResult : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Point_;
	empl_tns1_RecCandidate * RecCandidate;
	empl_tns1_RecCandidateApplication * RecCandidateApplication;
	NSNumber * RecCandidateApplicationId;
	NSNumber * RecCandidateId;
	NSNumber * RecProcessPhaseResultId;
	empl_tns1_RecRecruitmentProcessPhase * RecRecruitmentProcessPhase;
	NSNumber * RecRecruitmentProcessPhaseId;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecProcessPhaseResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Point_;
@property (retain) empl_tns1_RecCandidate * RecCandidate;
@property (retain) empl_tns1_RecCandidateApplication * RecCandidateApplication;
@property (retain) NSNumber * RecCandidateApplicationId;
@property (retain) NSNumber * RecCandidateId;
@property (retain) NSNumber * RecProcessPhaseResultId;
@property (retain) empl_tns1_RecRecruitmentProcessPhase * RecRecruitmentProcessPhase;
@property (retain) NSNumber * RecRecruitmentProcessPhaseId;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecProcessPhaseResult : NSObject {
	
/* elements */
	NSMutableArray *RecProcessPhaseResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecProcessPhaseResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecProcessPhaseResult:(empl_tns1_RecProcessPhaseResult *)toAdd;
@property (readonly) NSMutableArray * RecProcessPhaseResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecProcessApplyForJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	empl_tns1_OrgJobPosition * OrgJobPosition;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSNumber * RecProcessApplyForJobPositionId;
	empl_tns1_RecRecruitmentProcess * RecRecruitmentProcess;
	NSNumber * RecRecruitmentProcessId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecProcessApplyForJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * RecProcessApplyForJobPositionId;
@property (retain) empl_tns1_RecRecruitmentProcess * RecRecruitmentProcess;
@property (retain) NSNumber * RecRecruitmentProcessId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecProcessApplyForJobPosition : NSObject {
	
/* elements */
	NSMutableArray *RecProcessApplyForJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecProcessApplyForJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecProcessApplyForJobPosition:(empl_tns1_RecProcessApplyForJobPosition *)toAdd;
@property (readonly) NSMutableArray * RecProcessApplyForJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecRecruitmentProcessPhase : NSObject {
	
/* elements */
	NSMutableArray *RecRecruitmentProcessPhase;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecRecruitmentProcessPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRecruitmentProcessPhase:(empl_tns1_RecRecruitmentProcessPhase *)toAdd;
@property (readonly) NSMutableArray * RecRecruitmentProcessPhase;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecRecruitmentProcess : NSObject {
	
/* elements */
	NSDate * ApplyDate;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	empl_tns1_ArrayOfRecProcessApplyForJobPosition * RecProcessApplyForJobPositions;
	NSString * RecProcessDescription;
	NSString * RecProcessName;
	NSNumber * RecRecruitmentProcessId;
	empl_tns1_ArrayOfRecRecruitmentProcessPhase * RecRecruitmentProcessPhases;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecRecruitmentProcess *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ApplyDate;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_ArrayOfRecProcessApplyForJobPosition * RecProcessApplyForJobPositions;
@property (retain) NSString * RecProcessDescription;
@property (retain) NSString * RecProcessName;
@property (retain) NSNumber * RecRecruitmentProcessId;
@property (retain) empl_tns1_ArrayOfRecRecruitmentProcessPhase * RecRecruitmentProcessPhases;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecRecruitmentProcessPhase : NSObject {
	
/* elements */
	NSNumber * CoefficientPoint;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Priority;
	empl_tns1_ArrayOfRecInterviewPhase * RecInterviewPhases;
	NSString * RecProcessPhaseName;
	empl_tns1_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
	empl_tns1_RecRecruitmentProcess * RecRecruitmentProcess;
	NSNumber * RecRecruitmentProcessId;
	NSNumber * RecRecruitmentProcessPhaseId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecRecruitmentProcessPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CoefficientPoint;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Priority;
@property (retain) empl_tns1_ArrayOfRecInterviewPhase * RecInterviewPhases;
@property (retain) NSString * RecProcessPhaseName;
@property (retain) empl_tns1_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
@property (retain) empl_tns1_RecRecruitmentProcess * RecRecruitmentProcess;
@property (retain) NSNumber * RecRecruitmentProcessId;
@property (retain) NSNumber * RecRecruitmentProcessPhaseId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecInterviewPhase : NSObject {
	
/* elements */
	NSNumber * CoefficientPoint;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * InterviewPhaseName;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Priority;
	empl_tns1_ArrayOfRecInterviewPhaseEvaluation * RecInterviewPhaseEvaluations;
	NSNumber * RecInterviewPhaseId;
	empl_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
	NSNumber * RecProcessPhaseId;
	empl_tns1_RecRecruitmentProcessPhase * RecRecruitmentProcessPhase;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecInterviewPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CoefficientPoint;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * InterviewPhaseName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Priority;
@property (retain) empl_tns1_ArrayOfRecInterviewPhaseEvaluation * RecInterviewPhaseEvaluations;
@property (retain) NSNumber * RecInterviewPhaseId;
@property (retain) empl_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
@property (retain) NSNumber * RecProcessPhaseId;
@property (retain) empl_tns1_RecRecruitmentProcessPhase * RecRecruitmentProcessPhase;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecInterviewPhaseEvaluation : NSObject {
	
/* elements */
	NSNumber * ComanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	empl_tns1_RecEvaluationCriterion * RecEvaluationCriterion;
	NSNumber * RecEvaluationCriterionId;
	empl_tns1_RecInterviewPhase * RecInterviewPhase;
	NSNumber * RecInterviewPhaseEvaluationId;
	NSNumber * RecInterviewPhaseId;
	empl_tns1_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecInterviewPhaseEvaluation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ComanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_RecEvaluationCriterion * RecEvaluationCriterion;
@property (retain) NSNumber * RecEvaluationCriterionId;
@property (retain) empl_tns1_RecInterviewPhase * RecInterviewPhase;
@property (retain) NSNumber * RecInterviewPhaseEvaluationId;
@property (retain) NSNumber * RecInterviewPhaseId;
@property (retain) empl_tns1_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecScheduleInterviewerResult : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Point_;
	empl_tns1_RecInterviewPhaseEvaluation * RecInterviewPhaseEvaluation;
	NSNumber * RecInterviewPhaseEvaluationId;
	empl_tns1_RecInterviewSchedule * RecInterviewSchedule;
	NSNumber * RecInterviewScheduleId;
	empl_tns1_RecInterviewer * RecInterviewer;
	NSNumber * RecInterviewerId;
	NSNumber * RecScheduleInterviewerResultId;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecScheduleInterviewerResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Point_;
@property (retain) empl_tns1_RecInterviewPhaseEvaluation * RecInterviewPhaseEvaluation;
@property (retain) NSNumber * RecInterviewPhaseEvaluationId;
@property (retain) empl_tns1_RecInterviewSchedule * RecInterviewSchedule;
@property (retain) NSNumber * RecInterviewScheduleId;
@property (retain) empl_tns1_RecInterviewer * RecInterviewer;
@property (retain) NSNumber * RecInterviewerId;
@property (retain) NSNumber * RecScheduleInterviewerResultId;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecScheduleInterviewerResult : NSObject {
	
/* elements */
	NSMutableArray *RecScheduleInterviewerResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecScheduleInterviewerResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecScheduleInterviewerResult:(empl_tns1_RecScheduleInterviewerResult *)toAdd;
@property (readonly) NSMutableArray * RecScheduleInterviewerResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecInterviewer : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsMainInterviewer;
	NSDate * ModifiedDate;
	empl_tns1_RecGroupInterviewer * RecGroupInterviewer;
	NSNumber * RecGroupInterviewerId;
	NSNumber * RecInterviewerId;
	empl_tns1_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecInterviewer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainInterviewer;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_RecGroupInterviewer * RecGroupInterviewer;
@property (retain) NSNumber * RecGroupInterviewerId;
@property (retain) NSNumber * RecInterviewerId;
@property (retain) empl_tns1_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecInterviewer : NSObject {
	
/* elements */
	NSMutableArray *RecInterviewer;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecInterviewer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecInterviewer:(empl_tns1_RecInterviewer *)toAdd;
@property (readonly) NSMutableArray * RecInterviewer;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecGroupInterviewer : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * GroupName;
	USBoolean * IsDeleted;
	NSNumber * RecGroupInterviewerId;
	empl_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
	empl_tns1_ArrayOfRecInterviewer * RecInterviewers;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecGroupInterviewer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * GroupName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecGroupInterviewerId;
@property (retain) empl_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
@property (retain) empl_tns1_ArrayOfRecInterviewer * RecInterviewers;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecInterviewScheduleStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * Priority;
	NSNumber * RecInterviewScheduleStatusId;
	empl_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
	NSString * StatusCode;
	NSString * StatusName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecInterviewScheduleStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Priority;
@property (retain) NSNumber * RecInterviewScheduleStatusId;
@property (retain) empl_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
@property (retain) NSString * StatusCode;
@property (retain) NSString * StatusName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecInterviewSchedule : NSObject {
	
/* elements */
	NSNumber * AveragePoint;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeCreateId;
	NSDate * InterviewDate;
	NSString * InterviewPlace;
	NSDate * InterviewTime;
	USBoolean * IsDeleted;
	USBoolean * IsIntervieweeNotified;
	USBoolean * IsInterviewerNotified;
	NSDate * ModifiedDate;
	empl_tns1_RecCandidateApplication * RecCandidateApplication;
	NSNumber * RecCandidateApplicationId;
	empl_tns1_RecGroupInterviewer * RecGroupInterviewer;
	NSNumber * RecGroupInterviewerId;
	empl_tns1_RecInterviewPhase * RecInterviewPhase;
	NSNumber * RecInterviewPhaseId;
	NSNumber * RecInterviewScheduleId;
	empl_tns1_RecInterviewScheduleStatu * RecInterviewScheduleStatu;
	NSNumber * RecInterviewScheduleStatusId;
	empl_tns1_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecInterviewSchedule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AveragePoint;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeCreateId;
@property (retain) NSDate * InterviewDate;
@property (retain) NSString * InterviewPlace;
@property (retain) NSDate * InterviewTime;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsIntervieweeNotified;
@property (retain) USBoolean * IsInterviewerNotified;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_RecCandidateApplication * RecCandidateApplication;
@property (retain) NSNumber * RecCandidateApplicationId;
@property (retain) empl_tns1_RecGroupInterviewer * RecGroupInterviewer;
@property (retain) NSNumber * RecGroupInterviewerId;
@property (retain) empl_tns1_RecInterviewPhase * RecInterviewPhase;
@property (retain) NSNumber * RecInterviewPhaseId;
@property (retain) NSNumber * RecInterviewScheduleId;
@property (retain) empl_tns1_RecInterviewScheduleStatu * RecInterviewScheduleStatu;
@property (retain) NSNumber * RecInterviewScheduleStatusId;
@property (retain) empl_tns1_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecInterviewSchedule : NSObject {
	
/* elements */
	NSMutableArray *RecInterviewSchedule;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecInterviewSchedule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecInterviewSchedule:(empl_tns1_RecInterviewSchedule *)toAdd;
@property (readonly) NSMutableArray * RecInterviewSchedule;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecPhaseEmpDisplaced : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	empl_tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSString * Reason;
	NSNumber * RecPhaseEmpDisplaced1;
	empl_tns1_RecRecruitmentPhase * RecRecruitmentPhase;
	NSNumber * RecRecruitmentPhaseId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecPhaseEmpDisplaced *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * Reason;
@property (retain) NSNumber * RecPhaseEmpDisplaced1;
@property (retain) empl_tns1_RecRecruitmentPhase * RecRecruitmentPhase;
@property (retain) NSNumber * RecRecruitmentPhaseId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecPhaseEmpDisplaced : NSObject {
	
/* elements */
	NSMutableArray *RecPhaseEmpDisplaced;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecPhaseEmpDisplaced *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecPhaseEmpDisplaced:(empl_tns1_RecPhaseEmpDisplaced *)toAdd;
@property (readonly) NSMutableArray * RecPhaseEmpDisplaced;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecRecruitmentPhase : NSObject {
	
/* elements */
	NSMutableArray *RecRecruitmentPhase;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecRecruitmentPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRecruitmentPhase:(empl_tns1_RecRecruitmentPhase *)toAdd;
@property (readonly) NSMutableArray * RecRecruitmentPhase;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecPhaseStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSString * PhaseStatusName;
	NSNumber * RecPhaseStatusId;
	empl_tns1_ArrayOfRecRecruitmentPhase * RecRecruitmentPhases;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecPhaseStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * PhaseStatusName;
@property (retain) NSNumber * RecPhaseStatusId;
@property (retain) empl_tns1_ArrayOfRecRecruitmentPhase * RecRecruitmentPhases;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecRecruitmentPhaseJobPosition : NSObject {
	
/* elements */
	NSMutableArray *RecRecruitmentPhaseJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecRecruitmentPhaseJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRecruitmentPhaseJobPosition:(empl_tns1_RecRecruitmentPhaseJobPosition *)toAdd;
@property (readonly) NSMutableArray * RecRecruitmentPhaseJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_SysRecPlanApprover : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsMainApprover;
	NSNumber * Priority;
	empl_tns1_ArrayOfRecPlanApproveHistory * RecPlanApproveHistories;
	NSNumber * SysRecPlanApproverId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_SysRecPlanApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainApprover;
@property (retain) NSNumber * Priority;
@property (retain) empl_tns1_ArrayOfRecPlanApproveHistory * RecPlanApproveHistories;
@property (retain) NSNumber * SysRecPlanApproverId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecPlanApproveHistory : NSObject {
	
/* elements */
	NSDate * Date;
	USBoolean * IsApproved;
	USBoolean * IsDeleted;
	NSNumber * RecPlanApproveHistoryId;
	empl_tns1_RecRecruitmentPlan * RecRecruitmentPlan;
	NSNumber * RecRecruitmentPlanId;
	NSString * Remark;
	empl_tns1_SysRecPlanApprover * SysRecPlanApprover;
	NSNumber * SysRecPlanApproverId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecPlanApproveHistory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * Date;
@property (retain) USBoolean * IsApproved;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecPlanApproveHistoryId;
@property (retain) empl_tns1_RecRecruitmentPlan * RecRecruitmentPlan;
@property (retain) NSNumber * RecRecruitmentPlanId;
@property (retain) NSString * Remark;
@property (retain) empl_tns1_SysRecPlanApprover * SysRecPlanApprover;
@property (retain) NSNumber * SysRecPlanApproverId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecPlanApproveHistory : NSObject {
	
/* elements */
	NSMutableArray *RecPlanApproveHistory;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecPlanApproveHistory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecPlanApproveHistory:(empl_tns1_RecPlanApproveHistory *)toAdd;
@property (readonly) NSMutableArray * RecPlanApproveHistory;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecPlanJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	empl_tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSNumber * RecPlanJobPositionId;
	NSString * RecReason;
	empl_tns1_RecRecruitmentPlan * RecRecruitmentPlan;
	NSNumber * RecRecruitmentPlanId;
	NSNumber * RecruitedQuantity;
	NSNumber * RequirementQuantity;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecPlanJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * RecPlanJobPositionId;
@property (retain) NSString * RecReason;
@property (retain) empl_tns1_RecRecruitmentPlan * RecRecruitmentPlan;
@property (retain) NSNumber * RecRecruitmentPlanId;
@property (retain) NSNumber * RecruitedQuantity;
@property (retain) NSNumber * RequirementQuantity;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecPlanJobPosition : NSObject {
	
/* elements */
	NSMutableArray *RecPlanJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecPlanJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecPlanJobPosition:(empl_tns1_RecPlanJobPosition *)toAdd;
@property (readonly) NSMutableArray * RecPlanJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecRecruitmentPlan : NSObject {
	
/* elements */
	NSMutableArray *RecRecruitmentPlan;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecRecruitmentPlan *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRecruitmentPlan:(empl_tns1_RecRecruitmentPlan *)toAdd;
@property (readonly) NSMutableArray * RecRecruitmentPlan;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecPlanStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * RecPlanStatusId;
	empl_tns1_ArrayOfRecRecruitmentPlan * RecRecruitmentPlans;
	NSString * StatusName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecPlanStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecPlanStatusId;
@property (retain) empl_tns1_ArrayOfRecRecruitmentPlan * RecRecruitmentPlans;
@property (retain) NSString * StatusName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecRecruitmentRequirement : NSObject {
	
/* elements */
	NSMutableArray *RecRecruitmentRequirement;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecRecruitmentRequirement *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRecruitmentRequirement:(empl_tns1_RecRecruitmentRequirement *)toAdd;
@property (readonly) NSMutableArray * RecRecruitmentRequirement;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecRecruitmentPlan : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * PlanDescription;
	empl_tns1_ArrayOfRecPlanApproveHistory * RecPlanApproveHistories;
	NSNumber * RecPlanApproveHistoryId;
	empl_tns1_ArrayOfRecPlanJobPosition * RecPlanJobPositions;
	empl_tns1_RecPlanStatu * RecPlanStatu;
	NSNumber * RecPlanStatusId;
	NSString * RecRecruitmentPlanCode;
	NSNumber * RecRecruitmentPlanId;
	empl_tns1_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
	NSDate * StartDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecRecruitmentPlan *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * PlanDescription;
@property (retain) empl_tns1_ArrayOfRecPlanApproveHistory * RecPlanApproveHistories;
@property (retain) NSNumber * RecPlanApproveHistoryId;
@property (retain) empl_tns1_ArrayOfRecPlanJobPosition * RecPlanJobPositions;
@property (retain) empl_tns1_RecPlanStatu * RecPlanStatu;
@property (retain) NSNumber * RecPlanStatusId;
@property (retain) NSString * RecRecruitmentPlanCode;
@property (retain) NSNumber * RecRecruitmentPlanId;
@property (retain) empl_tns1_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
@property (retain) NSDate * StartDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_SysRecRequirementApprover : NSObject {
	
/* elements */
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsMainApprover;
	NSNumber * Priority;
	empl_tns1_ArrayOfRecRequirementApproveHistory * RecRequirementApproveHistories;
	NSNumber * SysRecRecruitmentApproverId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_SysRecRequirementApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainApprover;
@property (retain) NSNumber * Priority;
@property (retain) empl_tns1_ArrayOfRecRequirementApproveHistory * RecRequirementApproveHistories;
@property (retain) NSNumber * SysRecRecruitmentApproverId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecRequirementApproveHistory : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * Date;
	USBoolean * IsApproved;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * RecRecruitmentApproveHistoryId;
	empl_tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
	NSNumber * RecRequirementId;
	NSString * Remark;
	NSNumber * SysRecRecruitmentApproverId;
	empl_tns1_SysRecRequirementApprover * SysRecRequirementApprover;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecRequirementApproveHistory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * Date;
@property (retain) USBoolean * IsApproved;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * RecRecruitmentApproveHistoryId;
@property (retain) empl_tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
@property (retain) NSNumber * RecRequirementId;
@property (retain) NSString * Remark;
@property (retain) NSNumber * SysRecRecruitmentApproverId;
@property (retain) empl_tns1_SysRecRequirementApprover * SysRecRequirementApprover;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecRequirementApproveHistory : NSObject {
	
/* elements */
	NSMutableArray *RecRequirementApproveHistory;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecRequirementApproveHistory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRequirementApproveHistory:(empl_tns1_RecRequirementApproveHistory *)toAdd;
@property (readonly) NSMutableArray * RecRequirementApproveHistory;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecRequirementEmpDisplaced : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	empl_tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSString * Reason;
	empl_tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
	NSNumber * RecRecruitmentRequirementId;
	NSNumber * RecRequirementEmpDisplaced1;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecRequirementEmpDisplaced *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * Reason;
@property (retain) empl_tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
@property (retain) NSNumber * RecRecruitmentRequirementId;
@property (retain) NSNumber * RecRequirementEmpDisplaced1;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecRequirementEmpDisplaced : NSObject {
	
/* elements */
	NSMutableArray *RecRequirementEmpDisplaced;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecRequirementEmpDisplaced *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRequirementEmpDisplaced:(empl_tns1_RecRequirementEmpDisplaced *)toAdd;
@property (readonly) NSMutableArray * RecRequirementEmpDisplaced;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecRequirementJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	USBoolean * IsDisplacement;
	NSString * JobDescription;
	NSNumber * JobDescriptionId;
	NSDate * ModifiedDate;
	empl_tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSString * RecReason;
	empl_tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
	NSNumber * RecRecruitmentRequirementId;
	NSNumber * RecRequirementJobPositionId;
	NSNumber * RecruitedQuantity;
	NSNumber * RequirementQuantity;
	NSString * WorkingLocation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecRequirementJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDisplacement;
@property (retain) NSString * JobDescription;
@property (retain) NSNumber * JobDescriptionId;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * RecReason;
@property (retain) empl_tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
@property (retain) NSNumber * RecRecruitmentRequirementId;
@property (retain) NSNumber * RecRequirementJobPositionId;
@property (retain) NSNumber * RecruitedQuantity;
@property (retain) NSNumber * RequirementQuantity;
@property (retain) NSString * WorkingLocation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecRequirementJobPosition : NSObject {
	
/* elements */
	NSMutableArray *RecRequirementJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecRequirementJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRequirementJobPosition:(empl_tns1_RecRequirementJobPosition *)toAdd;
@property (readonly) NSMutableArray * RecRequirementJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecRequirementStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	empl_tns1_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
	NSNumber * RecRequirementStatusId;
	NSString * StatusName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecRequirementStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) empl_tns1_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
@property (retain) NSNumber * RecRequirementStatusId;
@property (retain) NSString * StatusName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecRecruitmentRequirement : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgUnitId;
	NSDate * RecBeginDate;
	NSDate * RecEndDate;
	NSNumber * RecRecruitmentApproveHistoryId;
	empl_tns1_ArrayOfRecRecruitmentPhase * RecRecruitmentPhases;
	empl_tns1_RecRecruitmentPlan * RecRecruitmentPlan;
	NSNumber * RecRecruitmentPlanId;
	NSNumber * RecRecruitmentRequirementId;
	empl_tns1_ArrayOfRecRequirementApproveHistory * RecRequirementApproveHistories;
	empl_tns1_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
	empl_tns1_ArrayOfRecRequirementJobPosition * RecRequirementJobPositions;
	empl_tns1_RecRequirementStatu * RecRequirementStatu;
	NSNumber * RecRequirementStatusId;
	NSNumber * RecruitedQuantity;
	NSString * RecruitmentPurpose;
	NSString * Remark;
	NSString * RequirementCode;
	NSDate * RequirementDate;
	NSString * RequirementName;
	NSNumber * TotalQuantity;
	NSNumber * UrgentLevel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecRecruitmentRequirement *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSDate * RecBeginDate;
@property (retain) NSDate * RecEndDate;
@property (retain) NSNumber * RecRecruitmentApproveHistoryId;
@property (retain) empl_tns1_ArrayOfRecRecruitmentPhase * RecRecruitmentPhases;
@property (retain) empl_tns1_RecRecruitmentPlan * RecRecruitmentPlan;
@property (retain) NSNumber * RecRecruitmentPlanId;
@property (retain) NSNumber * RecRecruitmentRequirementId;
@property (retain) empl_tns1_ArrayOfRecRequirementApproveHistory * RecRequirementApproveHistories;
@property (retain) empl_tns1_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
@property (retain) empl_tns1_ArrayOfRecRequirementJobPosition * RecRequirementJobPositions;
@property (retain) empl_tns1_RecRequirementStatu * RecRequirementStatu;
@property (retain) NSNumber * RecRequirementStatusId;
@property (retain) NSNumber * RecruitedQuantity;
@property (retain) NSString * RecruitmentPurpose;
@property (retain) NSString * Remark;
@property (retain) NSString * RequirementCode;
@property (retain) NSDate * RequirementDate;
@property (retain) NSString * RequirementName;
@property (retain) NSNumber * TotalQuantity;
@property (retain) NSNumber * UrgentLevel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecRecruitmentPhase : NSObject {
	
/* elements */
	NSDate * ApplicationDate;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	USBoolean * IsNotified;
	NSDate * ModifiedDate;
	NSString * PhaseName;
	empl_tns1_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
	empl_tns1_RecPhaseStatu * RecPhaseStatu;
	NSNumber * RecPhaseStatusId;
	NSString * RecRecruitmentPhaseCode;
	NSNumber * RecRecruitmentPhaseId;
	empl_tns1_ArrayOfRecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPositions;
	empl_tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
	NSNumber * RecRecruitmentRequirementId;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecRecruitmentPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ApplicationDate;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsNotified;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * PhaseName;
@property (retain) empl_tns1_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
@property (retain) empl_tns1_RecPhaseStatu * RecPhaseStatu;
@property (retain) NSNumber * RecPhaseStatusId;
@property (retain) NSString * RecRecruitmentPhaseCode;
@property (retain) NSNumber * RecRecruitmentPhaseId;
@property (retain) empl_tns1_ArrayOfRecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPositions;
@property (retain) empl_tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
@property (retain) NSNumber * RecRecruitmentRequirementId;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecRecruitmentPhaseJobPosition : NSObject {
	
/* elements */
	NSString * AgeLimitation;
	NSString * BeginSalaryLevel;
	NSString * BenefitAllowance;
	NSNumber * CompanyId;
	NSString * ContractTerm;
	NSDate * CreatedDate;
	NSNumber * DirectLeader;
	NSDate * FromDate;
	NSString * GenderLimitation;
	USBoolean * HasProbation;
	USBoolean * IsDeleted;
	USBoolean * IsDisplacement;
	NSString * JobDescription;
	NSDate * ModifiedDate;
	empl_tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSString * ProbationTime;
	empl_tns1_ArrayOfRecCandidateApplication * RecCandidateApplications;
	empl_tns1_RecRecruitmentPhase * RecRecruitmentPhase;
	NSNumber * RecRecruitmentPhaseId;
	NSNumber * RecRecruitmentPhaseJobPositionId;
	NSNumber * RecruitedQuantity;
	NSNumber * RequirementQuantity;
	NSDate * StartWorkDate;
	NSDate * ToDate;
	NSString * WorkingLocation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecRecruitmentPhaseJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AgeLimitation;
@property (retain) NSString * BeginSalaryLevel;
@property (retain) NSString * BenefitAllowance;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ContractTerm;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * DirectLeader;
@property (retain) NSDate * FromDate;
@property (retain) NSString * GenderLimitation;
@property (retain) USBoolean * HasProbation;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDisplacement;
@property (retain) NSString * JobDescription;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * ProbationTime;
@property (retain) empl_tns1_ArrayOfRecCandidateApplication * RecCandidateApplications;
@property (retain) empl_tns1_RecRecruitmentPhase * RecRecruitmentPhase;
@property (retain) NSNumber * RecRecruitmentPhaseId;
@property (retain) NSNumber * RecRecruitmentPhaseJobPositionId;
@property (retain) NSNumber * RecruitedQuantity;
@property (retain) NSNumber * RequirementQuantity;
@property (retain) NSDate * StartWorkDate;
@property (retain) NSDate * ToDate;
@property (retain) NSString * WorkingLocation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecCandidateApplication : NSObject {
	
/* elements */
	NSNumber * AveragePoint;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * CurrentInterviewPhaseId;
	NSNumber * CurrentProcessPhaseId;
	NSString * HRRemark;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSDate * NegotiateDate;
	empl_tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateApplicationId;
	NSNumber * RecCandidateId;
	empl_tns1_RecCandidateProfileStatu * RecCandidateProfileStatu;
	NSNumber * RecCandidateProfileStatusId;
	empl_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
	empl_tns1_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
	empl_tns1_RecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPosition;
	NSNumber * RecRecruitmentPhaseJobPositionId;
	NSDate * StartWorkingDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecCandidateApplication *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AveragePoint;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * CurrentInterviewPhaseId;
@property (retain) NSNumber * CurrentProcessPhaseId;
@property (retain) NSString * HRRemark;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * NegotiateDate;
@property (retain) empl_tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateApplicationId;
@property (retain) NSNumber * RecCandidateId;
@property (retain) empl_tns1_RecCandidateProfileStatu * RecCandidateProfileStatu;
@property (retain) NSNumber * RecCandidateProfileStatusId;
@property (retain) empl_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
@property (retain) empl_tns1_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
@property (retain) empl_tns1_RecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPosition;
@property (retain) NSNumber * RecRecruitmentPhaseJobPositionId;
@property (retain) NSDate * StartWorkingDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecCandidateApplication : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateApplication;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecCandidateApplication *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateApplication:(empl_tns1_RecCandidateApplication *)toAdd;
@property (readonly) NSMutableArray * RecCandidateApplication;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecCandidateCompetencyRating : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateCompetencyRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecCandidateCompetencyRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateCompetencyRating:(empl_tns1_RecCandidateCompetencyRating *)toAdd;
@property (readonly) NSMutableArray * RecCandidateCompetencyRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CLogDegree : NSObject {
	
/* elements */
	NSNumber * CLogDegreeId;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * Note;
	empl_tns1_ArrayOfRecCandidateDegree * RecCandidateDegrees;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CLogDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogDegreeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) empl_tns1_ArrayOfRecCandidateDegree * RecCandidateDegrees;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecCandidateDegree : NSObject {
	
/* elements */
	empl_tns1_CLogDegree * CLogDegree;
	NSNumber * CLogDegreeId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	USBoolean * IsDeleted;
	USBoolean * IsValId;
	NSDate * ModifiedDate;
	NSString * Other;
	NSString * PlaceIssue;
	empl_tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateDegreeId;
	NSNumber * RecCandidateId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecCandidateDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_CLogDegree * CLogDegree;
@property (retain) NSNumber * CLogDegreeId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) empl_tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateDegreeId;
@property (retain) NSNumber * RecCandidateId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecCandidateDegree : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateDegree;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecCandidateDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateDegree:(empl_tns1_RecCandidateDegree *)toAdd;
@property (readonly) NSMutableArray * RecCandidateDegree;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfOrgExperience : NSObject {
	
/* elements */
	NSMutableArray *OrgExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfOrgExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgExperience:(empl_tns1_OrgExperience *)toAdd;
@property (readonly) NSMutableArray * OrgExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_OrgProjectType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	empl_tns1_ArrayOfOrgExperience * OrgExperiences;
	NSNumber * OrgProjectTypeId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_OrgProjectType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) empl_tns1_ArrayOfOrgExperience * OrgExperiences;
@property (retain) NSNumber * OrgProjectTypeId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_OrgTimeInCharge : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	empl_tns1_ArrayOfOrgExperience * OrgExperiences;
	NSNumber * OrgTimeInChargeId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_OrgTimeInCharge *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_ArrayOfOrgExperience * OrgExperiences;
@property (retain) NSNumber * OrgTimeInChargeId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_OrgExperience : NSObject {
	
/* elements */
	NSString * Achievement;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgExperienceId;
	empl_tns1_OrgProjectType * OrgProjectType;
	NSNumber * OrgProjectTypeId;
	empl_tns1_OrgTimeInCharge * OrgTimeInCharge;
	NSNumber * OrgTimeInChargeId;
	empl_tns1_ArrayOfRecCandidateExperience * RecCandidateExperiences;
	NSString * RoleDescription;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_OrgExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Achievement;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgExperienceId;
@property (retain) empl_tns1_OrgProjectType * OrgProjectType;
@property (retain) NSNumber * OrgProjectTypeId;
@property (retain) empl_tns1_OrgTimeInCharge * OrgTimeInCharge;
@property (retain) NSNumber * OrgTimeInChargeId;
@property (retain) empl_tns1_ArrayOfRecCandidateExperience * RecCandidateExperiences;
@property (retain) NSString * RoleDescription;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecCandidateExperience : NSObject {
	
/* elements */
	empl_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompanyName;
	NSDate * CreatedDate;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	USBoolean * IsInternal;
	NSString * JobPosition;
	NSDate * ModifiedDate;
	NSString * NotableAchievements;
	NSString * Note;
	empl_tns1_OrgExperience * OrgExperience;
	NSNumber * OrgExperienceId;
	empl_tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateExperienceId;
	NSNumber * RecCandidateId;
	NSString * Responsibilities;
	NSDate * StartDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecCandidateExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompanyName;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInternal;
@property (retain) NSString * JobPosition;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * NotableAchievements;
@property (retain) NSString * Note;
@property (retain) empl_tns1_OrgExperience * OrgExperience;
@property (retain) NSNumber * OrgExperienceId;
@property (retain) empl_tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateExperienceId;
@property (retain) NSNumber * RecCandidateId;
@property (retain) NSString * Responsibilities;
@property (retain) NSDate * StartDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecCandidateExperience : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecCandidateExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateExperience:(empl_tns1_RecCandidateExperience *)toAdd;
@property (readonly) NSMutableArray * RecCandidateExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CBPITDependent : NSObject {
	
/* elements */
	NSNumber * CBPITDependentId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DependentBeginDate;
	NSString * DependentDemonstrative;
	NSDate * DependentEndDate;
	empl_tns1_EmpProfileFamilyRelationship * EmpProfileFamilyRelationship;
	NSNumber * EmpProfileFamilyRelationshipId;
	USBoolean * IsDeleted;
	USBoolean * IsDependent;
	NSDate * ModifiedDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CBPITDependent *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBPITDependentId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DependentBeginDate;
@property (retain) NSString * DependentDemonstrative;
@property (retain) NSDate * DependentEndDate;
@property (retain) empl_tns1_EmpProfileFamilyRelationship * EmpProfileFamilyRelationship;
@property (retain) NSNumber * EmpProfileFamilyRelationshipId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDependent;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfCBPITDependent : NSObject {
	
/* elements */
	NSMutableArray *CBPITDependent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfCBPITDependent *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBPITDependent:(empl_tns1_CBPITDependent *)toAdd;
@property (readonly) NSMutableArray * CBPITDependent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpBeneficiary : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpBeneficiaryId;
	NSNumber * EmpFamilyRelationshipId;
	empl_tns1_EmpProfileFamilyRelationship * EmpProfileFamilyRelationship;
	NSDate * ImplementationDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpBeneficiary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpBeneficiaryId;
@property (retain) NSNumber * EmpFamilyRelationshipId;
@property (retain) empl_tns1_EmpProfileFamilyRelationship * EmpProfileFamilyRelationship;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpBeneficiary : NSObject {
	
/* elements */
	NSMutableArray *EmpBeneficiary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpBeneficiary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpBeneficiary:(empl_tns1_EmpBeneficiary *)toAdd;
@property (readonly) NSMutableArray * EmpBeneficiary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileFamilyRelationship : NSObject {
	
/* elements */
	NSString * Address;
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSDate * BirthDay;
	empl_tns1_ArrayOfCBPITDependent * CBPITDependents;
	empl_tns1_CLogFamilyRelationship * CLogFamilyRelationship;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DeathDate;
	empl_tns1_ArrayOfEmpBeneficiary * EmpBeneficiaries;
	NSNumber * EmpProfileFamilyRelationshipId;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * HomeTown;
	USBoolean * IsDeleted;
	USBoolean * IsEmergencyContact;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	USBoolean * Nourish;
	NSString * Occupation;
	NSString * OccupationPlace;
	NSString * PersonFirstName;
	NSString * PersonFullName;
	NSString * PersonLastName;
	NSNumber * PersonSameOccupationId;
	NSString * Phone;
	NSNumber * RelateRowId;
	NSNumber * RelationshipId;
	NSString * RelationshipName;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSDate * BirthDay;
@property (retain) empl_tns1_ArrayOfCBPITDependent * CBPITDependents;
@property (retain) empl_tns1_CLogFamilyRelationship * CLogFamilyRelationship;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DeathDate;
@property (retain) empl_tns1_ArrayOfEmpBeneficiary * EmpBeneficiaries;
@property (retain) NSNumber * EmpProfileFamilyRelationshipId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * HomeTown;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEmergencyContact;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) USBoolean * Nourish;
@property (retain) NSString * Occupation;
@property (retain) NSString * OccupationPlace;
@property (retain) NSString * PersonFirstName;
@property (retain) NSString * PersonFullName;
@property (retain) NSString * PersonLastName;
@property (retain) NSNumber * PersonSameOccupationId;
@property (retain) NSString * Phone;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSNumber * RelationshipId;
@property (retain) NSString * RelationshipName;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileFamilyRelationship : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileFamilyRelationship;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileFamilyRelationship:(empl_tns1_EmpProfileFamilyRelationship *)toAdd;
@property (readonly) NSMutableArray * EmpProfileFamilyRelationship;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CLogFamilyRelationship : NSObject {
	
/* elements */
	NSNumber * CLogFamilyRelationshipId;
	NSNumber * CompanyId;
	empl_tns1_ArrayOfEmpProfileFamilyRelationship * EmpProfileFamilyRelationships;
	NSString * FamilyRelationshipCode;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
	empl_tns1_ArrayOfRecCandidateFamilyRelationship * RecCandidateFamilyRelationships;
	USBoolean * Sex;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CLogFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogFamilyRelationshipId;
@property (retain) NSNumber * CompanyId;
@property (retain) empl_tns1_ArrayOfEmpProfileFamilyRelationship * EmpProfileFamilyRelationships;
@property (retain) NSString * FamilyRelationshipCode;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
@property (retain) empl_tns1_ArrayOfRecCandidateFamilyRelationship * RecCandidateFamilyRelationships;
@property (retain) USBoolean * Sex;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecCandidateFamilyRelationship : NSObject {
	
/* elements */
	NSString * Address;
	NSDate * BirthDay;
	empl_tns1_CLogFamilyRelationship * CLogFamilyRelationship;
	NSNumber * CLogFamilyRelationshipId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DeathDate;
	NSString * HomeTown;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	USBoolean * Nourish;
	NSString * Occupation;
	NSString * OccupationPlace;
	NSString * PersonFirstName;
	NSString * PersonFullName;
	NSString * PersonLastName;
	NSNumber * PersonSameOccupationId;
	NSString * Phone;
	empl_tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateFamilyRelationshipId;
	NSNumber * RecCandidateId;
	NSString * RelationshipName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecCandidateFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSDate * BirthDay;
@property (retain) empl_tns1_CLogFamilyRelationship * CLogFamilyRelationship;
@property (retain) NSNumber * CLogFamilyRelationshipId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DeathDate;
@property (retain) NSString * HomeTown;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) USBoolean * Nourish;
@property (retain) NSString * Occupation;
@property (retain) NSString * OccupationPlace;
@property (retain) NSString * PersonFirstName;
@property (retain) NSString * PersonFullName;
@property (retain) NSString * PersonLastName;
@property (retain) NSNumber * PersonSameOccupationId;
@property (retain) NSString * Phone;
@property (retain) empl_tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateFamilyRelationshipId;
@property (retain) NSNumber * RecCandidateId;
@property (retain) NSString * RelationshipName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecCandidateFamilyRelationship : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateFamilyRelationship;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecCandidateFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateFamilyRelationship:(empl_tns1_RecCandidateFamilyRelationship *)toAdd;
@property (readonly) NSMutableArray * RecCandidateFamilyRelationship;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecCandidateForeignLanguage : NSObject {
	
/* elements */
	NSString * AttachmenttUrl;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Degree;
	NSDate * Duration_;
	NSDate * EffectiveDate;
	USBoolean * IsDeleted;
	NSString * Language;
	NSString * LanguageSkill;
	NSDate * ModifiedDate;
	NSString * Note;
	empl_tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateForeignLanguageId;
	NSNumber * RecCandidateId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecCandidateForeignLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AttachmenttUrl;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Degree;
@property (retain) NSDate * Duration_;
@property (retain) NSDate * EffectiveDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Language;
@property (retain) NSString * LanguageSkill;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) empl_tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateForeignLanguageId;
@property (retain) NSNumber * RecCandidateId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecCandidateForeignLanguage : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateForeignLanguage;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecCandidateForeignLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateForeignLanguage:(empl_tns1_RecCandidateForeignLanguage *)toAdd;
@property (readonly) NSMutableArray * RecCandidateForeignLanguage;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileQualification : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	NSNumber * EmpProfileQualificationId;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsValId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * OrgDegreeRankId;
	empl_tns1_OrgQualification * OrgQualification;
	NSNumber * OrgQualificationId;
	NSString * Other;
	NSString * PlaceIssue;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) NSNumber * EmpProfileQualificationId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgDegreeRankId;
@property (retain) empl_tns1_OrgQualification * OrgQualification;
@property (retain) NSNumber * OrgQualificationId;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileQualification : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileQualification;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileQualification:(empl_tns1_EmpProfileQualification *)toAdd;
@property (readonly) NSMutableArray * EmpProfileQualification;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_OrgQualification : NSObject {
	
/* elements */
	NSNumber * CLogEducationLevelId;
	NSNumber * CLogMajorId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * Description;
	empl_tns1_ArrayOfEmpProfileQualification * EmpProfileQualifications;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * OrgQualificationCode;
	NSNumber * OrgQualificationId;
	NSString * OrgQualificationName;
	empl_tns1_ArrayOfRecCandidateQualification * RecCandidateQualifications;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_OrgQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogEducationLevelId;
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) empl_tns1_ArrayOfEmpProfileQualification * EmpProfileQualifications;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OrgQualificationCode;
@property (retain) NSNumber * OrgQualificationId;
@property (retain) NSString * OrgQualificationName;
@property (retain) empl_tns1_ArrayOfRecCandidateQualification * RecCandidateQualifications;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecCandidateQualification : NSObject {
	
/* elements */
	empl_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	USBoolean * IsDeleted;
	USBoolean * IsValId;
	NSDate * ModifiedDate;
	empl_tns1_OrgQualification * OrgQualification;
	NSNumber * OrgQualificationId;
	NSString * Other;
	NSString * PlaceIssue;
	empl_tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateId;
	NSNumber * RecCandidateQualificationId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecCandidateQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValId;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_OrgQualification * OrgQualification;
@property (retain) NSNumber * OrgQualificationId;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) empl_tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateId;
@property (retain) NSNumber * RecCandidateQualificationId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecCandidateQualification : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateQualification;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecCandidateQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateQualification:(empl_tns1_RecCandidateQualification *)toAdd;
@property (readonly) NSMutableArray * RecCandidateQualification;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileSkill : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	empl_tns1_CLogRating * CLogRating;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * DocumentRef;
	NSNumber * EmpProfileSkillId;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	empl_tns1_OrgSkill * OrgSkill;
	NSNumber * OrgSkillId;
	NSNumber * RatingId;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) empl_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DocumentRef;
@property (retain) NSNumber * EmpProfileSkillId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_OrgSkill * OrgSkill;
@property (retain) NSNumber * OrgSkillId;
@property (retain) NSNumber * RatingId;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileSkill : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileSkill:(empl_tns1_EmpProfileSkill *)toAdd;
@property (readonly) NSMutableArray * EmpProfileSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfOrgSkill : NSObject {
	
/* elements */
	NSMutableArray *OrgSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfOrgSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgSkill:(empl_tns1_OrgSkill *)toAdd;
@property (readonly) NSMutableArray * OrgSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_OrgSkillType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * OrgSkillTypeId;
	empl_tns1_ArrayOfOrgSkill * OrgSkills;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_OrgSkillType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * OrgSkillTypeId;
@property (retain) empl_tns1_ArrayOfOrgSkill * OrgSkills;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfTrainingCourseSchedule : NSObject {
	
/* elements */
	NSMutableArray *TrainingCourseSchedule;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfTrainingCourseSchedule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingCourseSchedule:(empl_tns1_TrainingCourseSchedule *)toAdd;
@property (readonly) NSMutableArray * TrainingCourseSchedule;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CLogCourseSchedule : NSObject {
	
/* elements */
	NSNumber * ClogCourseScheduleId;
	NSNumber * DayOfWeek;
	NSDate * EndTime;
	USBoolean * IsDeleted;
	NSNumber * Shift;
	NSDate * StartTime;
	empl_tns1_ArrayOfTrainingCourseSchedule * TrainingCourseSchedules;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CLogCourseSchedule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ClogCourseScheduleId;
@property (retain) NSNumber * DayOfWeek;
@property (retain) NSDate * EndTime;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Shift;
@property (retain) NSDate * StartTime;
@property (retain) empl_tns1_ArrayOfTrainingCourseSchedule * TrainingCourseSchedules;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfTrainingCourse : NSObject {
	
/* elements */
	NSMutableArray *TrainingCourse;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfTrainingCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingCourse:(empl_tns1_TrainingCourse *)toAdd;
@property (readonly) NSMutableArray * TrainingCourse;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CLogTrainingCenter : NSObject {
	
/* elements */
	NSNumber * CLogTrainingCenterId;
	USBoolean * IsDeleted;
	NSString * Name;
	empl_tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CLogTrainingCenter *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogTrainingCenterId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) empl_tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_TrainingCategory : NSObject {
	
/* elements */
	NSDate * CreateDate;
	NSString * Description;
	NSString * ImgUrl;
	USBoolean * IsDeleted;
	NSString * Name;
	NSNumber * TrainingCategoryId;
	empl_tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_TrainingCategory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) NSString * ImgUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSNumber * TrainingCategoryId;
@property (retain) empl_tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_TrainingCourseChapter : NSObject {
	
/* elements */
	NSNumber * Chapter;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * Session;
	empl_tns1_TrainingCourse * TrainingCourse;
	NSNumber * TrainingCourseChapterId;
	NSNumber * TrainingCourseId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_TrainingCourseChapter *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Chapter;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * Session;
@property (retain) empl_tns1_TrainingCourse * TrainingCourse;
@property (retain) NSNumber * TrainingCourseChapterId;
@property (retain) NSNumber * TrainingCourseId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfTrainingCourseChapter : NSObject {
	
/* elements */
	NSMutableArray *TrainingCourseChapter;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfTrainingCourseChapter *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingCourseChapter:(empl_tns1_TrainingCourseChapter *)toAdd;
@property (readonly) NSMutableArray * TrainingCourseChapter;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_TrainingCoursePeriod : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
	NSNumber * Session;
	NSString * Target;
	NSNumber * TrainingCoursePeriodId;
	empl_tns1_ArrayOfTrainingCourse * TrainingCourses;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_TrainingCoursePeriod *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) NSNumber * Session;
@property (retain) NSString * Target;
@property (retain) NSNumber * TrainingCoursePeriodId;
@property (retain) empl_tns1_ArrayOfTrainingCourse * TrainingCourses;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_TrainingCourseType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * Note;
	NSNumber * TrainingCourseTypeId;
	empl_tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_TrainingCourseType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) NSNumber * TrainingCourseTypeId;
@property (retain) empl_tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_TrainingCourseUnit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * OrgUnitId;
	empl_tns1_TrainingCourse * TrainingCourse;
	NSNumber * TrainingCourseId;
	NSNumber * TrainingCourseUnitId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_TrainingCourseUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * OrgUnitId;
@property (retain) empl_tns1_TrainingCourse * TrainingCourse;
@property (retain) NSNumber * TrainingCourseId;
@property (retain) NSNumber * TrainingCourseUnitId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfTrainingCourseUnit : NSObject {
	
/* elements */
	NSMutableArray *TrainingCourseUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfTrainingCourseUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingCourseUnit:(empl_tns1_TrainingCourseUnit *)toAdd;
@property (readonly) NSMutableArray * TrainingCourseUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_TrainingPlanDegree : NSObject {
	
/* elements */
	NSNumber * CLogDegreeId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	empl_tns1_TrainingCourse * TrainingCourse;
	NSNumber * TrainingPlanDegreeId;
	NSNumber * TraningCourseId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_TrainingPlanDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogDegreeId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_TrainingCourse * TrainingCourse;
@property (retain) NSNumber * TrainingPlanDegreeId;
@property (retain) NSNumber * TraningCourseId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfTrainingPlanDegree : NSObject {
	
/* elements */
	NSMutableArray *TrainingPlanDegree;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfTrainingPlanDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingPlanDegree:(empl_tns1_TrainingPlanDegree *)toAdd;
@property (readonly) NSMutableArray * TrainingPlanDegree;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileEducation : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	empl_tns1_CLogMajor * CLogMajor;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileEducationId;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EndYear;
	USBoolean * IsDeleted;
	NSString * Major;
	NSNumber * MajorId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * RelateRowId;
	NSNumber * StartYear;
	NSString * University;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileEducation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) empl_tns1_CLogMajor * CLogMajor;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileEducationId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EndYear;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Major;
@property (retain) NSNumber * MajorId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSNumber * StartYear;
@property (retain) NSString * University;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileEducation : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileEducation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileEducation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileEducation:(empl_tns1_EmpProfileEducation *)toAdd;
@property (readonly) NSMutableArray * EmpProfileEducation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfTrainingPlanRequest : NSObject {
	
/* elements */
	NSMutableArray *TrainingPlanRequest;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfTrainingPlanRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingPlanRequest:(empl_tns1_TrainingPlanRequest *)toAdd;
@property (readonly) NSMutableArray * TrainingPlanRequest;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CLogMajor : NSObject {
	
/* elements */
	NSNumber * CLogCareerId;
	NSString * CLogMajorCode;
	NSNumber * CLogMajorId;
	empl_tns1_ArrayOfEmpProfileEducation * EmpProfileEducations;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	empl_tns1_ArrayOfTrainingPlanRequest * TrainingPlanRequests;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CLogMajor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogCareerId;
@property (retain) NSString * CLogMajorCode;
@property (retain) NSNumber * CLogMajorId;
@property (retain) empl_tns1_ArrayOfEmpProfileEducation * EmpProfileEducations;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) empl_tns1_ArrayOfTrainingPlanRequest * TrainingPlanRequests;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_TrainingPlanEmployee : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSDate * CreatedDate;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * TrainingPlanEmployeeId;
	empl_tns1_TrainingPlanRequest * TrainingPlanRequest;
	NSNumber * TrainingPlanRequestId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_TrainingPlanEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * TrainingPlanEmployeeId;
@property (retain) empl_tns1_TrainingPlanRequest * TrainingPlanRequest;
@property (retain) NSNumber * TrainingPlanRequestId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfTrainingPlanEmployee : NSObject {
	
/* elements */
	NSMutableArray *TrainingPlanEmployee;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfTrainingPlanEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingPlanEmployee:(empl_tns1_TrainingPlanEmployee *)toAdd;
@property (readonly) NSMutableArray * TrainingPlanEmployee;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_TrainingPlanRequest : NSObject {
	
/* elements */
	NSNumber * ApproverId;
	empl_tns1_CLogMajor * CLogMajor;
	NSNumber * CLogMajorId;
	NSNumber * CompanyId;
	NSString * Content;
	NSNumber * Cost;
	NSDate * CreatedDate;
	NSDate * EndDate;
	USBoolean * IsApproved;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSDate * StartDate;
	NSNumber * StrategyGoalId;
	NSString * Target;
	NSString * Title;
	empl_tns1_ArrayOfTrainingCourse * TrainingCourses;
	empl_tns1_ArrayOfTrainingPlanEmployee * TrainingPlanEmployees;
	NSNumber * TrainingPlanRequestId;
	NSNumber * TrainingPlanRequestTypeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_TrainingPlanRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproverId;
@property (retain) empl_tns1_CLogMajor * CLogMajor;
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Content;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsApproved;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * StartDate;
@property (retain) NSNumber * StrategyGoalId;
@property (retain) NSString * Target;
@property (retain) NSString * Title;
@property (retain) empl_tns1_ArrayOfTrainingCourse * TrainingCourses;
@property (retain) empl_tns1_ArrayOfTrainingPlanEmployee * TrainingPlanEmployees;
@property (retain) NSNumber * TrainingPlanRequestId;
@property (retain) NSNumber * TrainingPlanRequestTypeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_TrainingProficencyExpected : NSObject {
	
/* elements */
	empl_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	USBoolean * IsDeleted;
	NSNumber * ProficencyId;
	NSNumber * ProficencyType;
	empl_tns1_TrainingCourse * TrainingCourse;
	NSNumber * TrainingCourseId;
	NSNumber * TrainingProficencyExpectedId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_TrainingProficencyExpected *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ProficencyId;
@property (retain) NSNumber * ProficencyType;
@property (retain) empl_tns1_TrainingCourse * TrainingCourse;
@property (retain) NSNumber * TrainingCourseId;
@property (retain) NSNumber * TrainingProficencyExpectedId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfTrainingProficencyExpected : NSObject {
	
/* elements */
	NSMutableArray *TrainingProficencyExpected;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfTrainingProficencyExpected *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingProficencyExpected:(empl_tns1_TrainingProficencyExpected *)toAdd;
@property (readonly) NSMutableArray * TrainingProficencyExpected;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_TrainingProficencyRequire : NSObject {
	
/* elements */
	empl_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	USBoolean * IsDeleted;
	NSNumber * ProficencyId;
	NSNumber * ProficencyType;
	empl_tns1_TrainingCourse * TrainingCourse;
	NSNumber * TrainingCourseId;
	NSNumber * TrainingProficencyRequireId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_TrainingProficencyRequire *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ProficencyId;
@property (retain) NSNumber * ProficencyType;
@property (retain) empl_tns1_TrainingCourse * TrainingCourse;
@property (retain) NSNumber * TrainingCourseId;
@property (retain) NSNumber * TrainingProficencyRequireId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfTrainingProficencyRequire : NSObject {
	
/* elements */
	NSMutableArray *TrainingProficencyRequire;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfTrainingProficencyRequire *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingProficencyRequire:(empl_tns1_TrainingProficencyRequire *)toAdd;
@property (readonly) NSMutableArray * TrainingProficencyRequire;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_TrainingCourse : NSObject {
	
/* elements */
	empl_tns1_CLogTrainer * CLogTrainer;
	NSNumber * CLogTrainerId;
	empl_tns1_CLogTrainingCenter * CLogTrainingCenter;
	NSNumber * CLogTrainingCenterId;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSDate * CreatedDate;
	NSString * Email;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	USBoolean * IsExpired;
	USBoolean * IsLongTrainingCourse;
	USBoolean * IsNecessitated;
	USBoolean * IsPublish;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * Number;
	NSString * PhoneNumber;
	NSString * Result;
	NSDate * StartDate;
	NSNumber * StrategyGoalId;
	empl_tns1_TrainingCategory * TrainingCategory;
	NSNumber * TrainingCategoryId;
	empl_tns1_ArrayOfTrainingCourseChapter * TrainingCourseChapters;
	NSNumber * TrainingCourseId;
	empl_tns1_TrainingCoursePeriod * TrainingCoursePeriod;
	NSNumber * TrainingCoursePeriodId;
	empl_tns1_ArrayOfTrainingCourseSchedule * TrainingCourseSchedules;
	empl_tns1_TrainingCourseType * TrainingCourseType;
	NSNumber * TrainingCourseTypeId;
	empl_tns1_ArrayOfTrainingCourseUnit * TrainingCourseUnits;
	empl_tns1_ArrayOfTrainingPlanDegree * TrainingPlanDegrees;
	empl_tns1_TrainingPlanRequest * TrainingPlanRequest;
	NSNumber * TrainingPlanRequestId;
	empl_tns1_ArrayOfTrainingProficencyExpected * TrainingProficencyExpecteds;
	empl_tns1_ArrayOfTrainingProficencyRequire * TrainingProficencyRequires;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_TrainingCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_CLogTrainer * CLogTrainer;
@property (retain) NSNumber * CLogTrainerId;
@property (retain) empl_tns1_CLogTrainingCenter * CLogTrainingCenter;
@property (retain) NSNumber * CLogTrainingCenterId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Email;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsExpired;
@property (retain) USBoolean * IsLongTrainingCourse;
@property (retain) USBoolean * IsNecessitated;
@property (retain) USBoolean * IsPublish;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * Number;
@property (retain) NSString * PhoneNumber;
@property (retain) NSString * Result;
@property (retain) NSDate * StartDate;
@property (retain) NSNumber * StrategyGoalId;
@property (retain) empl_tns1_TrainingCategory * TrainingCategory;
@property (retain) NSNumber * TrainingCategoryId;
@property (retain) empl_tns1_ArrayOfTrainingCourseChapter * TrainingCourseChapters;
@property (retain) NSNumber * TrainingCourseId;
@property (retain) empl_tns1_TrainingCoursePeriod * TrainingCoursePeriod;
@property (retain) NSNumber * TrainingCoursePeriodId;
@property (retain) empl_tns1_ArrayOfTrainingCourseSchedule * TrainingCourseSchedules;
@property (retain) empl_tns1_TrainingCourseType * TrainingCourseType;
@property (retain) NSNumber * TrainingCourseTypeId;
@property (retain) empl_tns1_ArrayOfTrainingCourseUnit * TrainingCourseUnits;
@property (retain) empl_tns1_ArrayOfTrainingPlanDegree * TrainingPlanDegrees;
@property (retain) empl_tns1_TrainingPlanRequest * TrainingPlanRequest;
@property (retain) NSNumber * TrainingPlanRequestId;
@property (retain) empl_tns1_ArrayOfTrainingProficencyExpected * TrainingProficencyExpecteds;
@property (retain) empl_tns1_ArrayOfTrainingProficencyRequire * TrainingProficencyRequires;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfTrainingCourseEmployee : NSObject {
	
/* elements */
	NSMutableArray *TrainingCourseEmployee;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfTrainingCourseEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingCourseEmployee:(empl_tns1_TrainingCourseEmployee *)toAdd;
@property (readonly) NSMutableArray * TrainingCourseEmployee;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_TrainingCourseSchedule : NSObject {
	
/* elements */
	empl_tns1_CLogCourseSchedule * CLogCourseSchedule;
	NSNumber * ClogCourseScheduleId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	empl_tns1_TrainingCourse * TrainingCourse;
	empl_tns1_ArrayOfTrainingCourseEmployee * TrainingCourseEmployees;
	NSNumber * TrainingCourseId;
	NSNumber * TrainingCourseScheduleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_TrainingCourseSchedule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_CLogCourseSchedule * CLogCourseSchedule;
@property (retain) NSNumber * ClogCourseScheduleId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_TrainingCourse * TrainingCourse;
@property (retain) empl_tns1_ArrayOfTrainingCourseEmployee * TrainingCourseEmployees;
@property (retain) NSNumber * TrainingCourseId;
@property (retain) NSNumber * TrainingCourseScheduleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_TrainingCourseEmployee : NSObject {
	
/* elements */
	NSNumber * Absence;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSDate * CreatedDate;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsGraduated;
	NSNumber * Mark;
	NSDate * ModifiedDate;
	NSNumber * TrainingCourseEmployeeId;
	empl_tns1_TrainingCourseSchedule * TrainingCourseSchedule;
	NSNumber * TrainingCourseScheduleId;
	empl_tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_TrainingCourseEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Absence;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsGraduated;
@property (retain) NSNumber * Mark;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * TrainingCourseEmployeeId;
@property (retain) empl_tns1_TrainingCourseSchedule * TrainingCourseSchedule;
@property (retain) NSNumber * TrainingCourseScheduleId;
@property (retain) empl_tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_TrainingEmpProficency : NSObject {
	
/* elements */
	NSNumber * CLogEmpRatingId;
	empl_tns1_CLogRating * CLogRating;
	empl_tns1_CLogRating * CLogRating1;
	NSNumber * ClogEmpManagerId;
	USBoolean * IsDeleted;
	NSNumber * ManagerId;
	empl_tns1_OrgSkill * OrgSkill;
	NSNumber * ProficencyId;
	NSNumber * ProficencyType;
	empl_tns1_TrainingCourseEmployee * TrainingCourseEmployee;
	NSNumber * TrainingCourseEmployeeId;
	NSNumber * TrainingEmpProficencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_TrainingEmpProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogEmpRatingId;
@property (retain) empl_tns1_CLogRating * CLogRating;
@property (retain) empl_tns1_CLogRating * CLogRating1;
@property (retain) NSNumber * ClogEmpManagerId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ManagerId;
@property (retain) empl_tns1_OrgSkill * OrgSkill;
@property (retain) NSNumber * ProficencyId;
@property (retain) NSNumber * ProficencyType;
@property (retain) empl_tns1_TrainingCourseEmployee * TrainingCourseEmployee;
@property (retain) NSNumber * TrainingCourseEmployeeId;
@property (retain) NSNumber * TrainingEmpProficencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfTrainingEmpProficency : NSObject {
	
/* elements */
	NSMutableArray *TrainingEmpProficency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfTrainingEmpProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingEmpProficency:(empl_tns1_TrainingEmpProficency *)toAdd;
@property (readonly) NSMutableArray * TrainingEmpProficency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_OrgSkill : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * Description;
	empl_tns1_ArrayOfEmpProfileSkill * EmpProfileSkills;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * OrgSkillCode;
	NSNumber * OrgSkillId;
	empl_tns1_OrgSkillType * OrgSkillType;
	NSNumber * OrgSkillTypeId;
	NSNumber * Priority;
	NSString * PriorityName;
	empl_tns1_ArrayOfRecCandidateSkill * RecCandidateSkills;
	empl_tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_OrgSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) empl_tns1_ArrayOfEmpProfileSkill * EmpProfileSkills;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * OrgSkillCode;
@property (retain) NSNumber * OrgSkillId;
@property (retain) empl_tns1_OrgSkillType * OrgSkillType;
@property (retain) NSNumber * OrgSkillTypeId;
@property (retain) NSNumber * Priority;
@property (retain) NSString * PriorityName;
@property (retain) empl_tns1_ArrayOfRecCandidateSkill * RecCandidateSkills;
@property (retain) empl_tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecCandidateSkill : NSObject {
	
/* elements */
	empl_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * DocumentRef;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	empl_tns1_OrgSkill * OrgSkill;
	NSNumber * OrgSkillId;
	empl_tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateId;
	NSNumber * RecCandidateSkillId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecCandidateSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DocumentRef;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_OrgSkill * OrgSkill;
@property (retain) NSNumber * OrgSkillId;
@property (retain) empl_tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateId;
@property (retain) NSNumber * RecCandidateSkillId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecCandidateSkill : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecCandidateSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateSkill:(empl_tns1_RecCandidateSkill *)toAdd;
@property (readonly) NSMutableArray * RecCandidateSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecCandidate : NSObject {
	
/* elements */
	NSMutableArray *RecCandidate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecCandidate *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidate:(empl_tns1_RecCandidate *)toAdd;
@property (readonly) NSMutableArray * RecCandidate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecCandidateStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * RecCandidateStatusId;
	empl_tns1_ArrayOfRecCandidate * RecCandidates;
	NSString * StatusName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecCandidateStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecCandidateStatusId;
@property (retain) empl_tns1_ArrayOfRecCandidate * RecCandidates;
@property (retain) NSString * StatusName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecCandidateSupplier : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateSupplier;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecCandidateSupplier *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateSupplier:(empl_tns1_RecCandidateSupplier *)toAdd;
@property (readonly) NSMutableArray * RecCandidateSupplier;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecCandidateTypeSupplier : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * RecCadidateTypeSupplierId;
	empl_tns1_ArrayOfRecCandidateSupplier * RecCandidateSuppliers;
	NSString * TypeSupplierName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecCandidateTypeSupplier *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecCadidateTypeSupplierId;
@property (retain) empl_tns1_ArrayOfRecCandidateSupplier * RecCandidateSuppliers;
@property (retain) NSString * TypeSupplierName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecCandidateSupplier : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * RecCadidateSupplierId;
	NSNumber * RecCadidateTypeSupplierId;
	empl_tns1_RecCandidateTypeSupplier * RecCandidateTypeSupplier;
	empl_tns1_ArrayOfRecCandidate * RecCandidates;
	NSString * SupplierAddress;
	NSString * SupplierEmail;
	NSString * SupplierName;
	NSString * SupplierPhone;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecCandidateSupplier *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecCadidateSupplierId;
@property (retain) NSNumber * RecCadidateTypeSupplierId;
@property (retain) empl_tns1_RecCandidateTypeSupplier * RecCandidateTypeSupplier;
@property (retain) empl_tns1_ArrayOfRecCandidate * RecCandidates;
@property (retain) NSString * SupplierAddress;
@property (retain) NSString * SupplierEmail;
@property (retain) NSString * SupplierName;
@property (retain) NSString * SupplierPhone;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecCandidate : NSObject {
	
/* elements */
	NSString * Address1;
	NSNumber * Address1Id;
	NSString * Address1Phone;
	NSString * Address2;
	NSNumber * Address2Id;
	NSString * Address2Phone;
	NSDate * ApplyDate;
	NSDate * BirthDay;
	NSString * BirthPlace;
	NSNumber * BirthPlaceId;
	NSString * CVFileType;
	NSString * CVUrl;
	USBoolean * CanBusinessTrip;
	USBoolean * CanOT;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * CulturalLevelId;
	NSString * CurrentSalary;
	NSNumber * EducationLevelId;
	NSString * Email;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * Ethnicity;
	NSNumber * EthnicityId;
	NSString * ExpectedSalary;
	NSString * FirstName;
	NSString * Forte;
	NSString * FullName;
	USBoolean * Gender;
	NSString * HomeTown;
	NSNumber * HomeTownId;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	NSString * ImageUrl;
	USBoolean * IsDeleted;
	USBoolean * IsManager;
	USBoolean * IsOnlyYearOfBirthday;
	NSString * LastName;
	NSString * MainLanguage;
	NSNumber * MainLanguageId;
	NSNumber * MajorId;
	NSNumber * MaritalStatusId;
	NSString * Mobile;
	NSDate * ModifiedDate;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSNumber * NumEmpManaged;
	NSString * OfficePhone;
	NSNumber * RecCadidateSupplierId;
	empl_tns1_ArrayOfRecCandidateApplication * RecCandidateApplications;
	NSString * RecCandidateCode;
	empl_tns1_ArrayOfRecCandidateCompetencyRating * RecCandidateCompetencyRatings;
	empl_tns1_ArrayOfRecCandidateDegree * RecCandidateDegrees;
	empl_tns1_ArrayOfRecCandidateExperience * RecCandidateExperiences;
	empl_tns1_ArrayOfRecCandidateFamilyRelationship * RecCandidateFamilyRelationships;
	empl_tns1_ArrayOfRecCandidateForeignLanguage * RecCandidateForeignLanguages;
	NSNumber * RecCandidateId;
	empl_tns1_ArrayOfRecCandidateQualification * RecCandidateQualifications;
	empl_tns1_ArrayOfRecCandidateSkill * RecCandidateSkills;
	empl_tns1_RecCandidateStatu * RecCandidateStatu;
	NSNumber * RecCandidateStatusId;
	empl_tns1_RecCandidateSupplier * RecCandidateSupplier;
	empl_tns1_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
	NSString * Religion;
	NSNumber * ReligionId;
	NSString * Weaknesses;
	NSNumber * YearsExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecCandidate *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address1;
@property (retain) NSNumber * Address1Id;
@property (retain) NSString * Address1Phone;
@property (retain) NSString * Address2;
@property (retain) NSNumber * Address2Id;
@property (retain) NSString * Address2Phone;
@property (retain) NSDate * ApplyDate;
@property (retain) NSDate * BirthDay;
@property (retain) NSString * BirthPlace;
@property (retain) NSNumber * BirthPlaceId;
@property (retain) NSString * CVFileType;
@property (retain) NSString * CVUrl;
@property (retain) USBoolean * CanBusinessTrip;
@property (retain) USBoolean * CanOT;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * CulturalLevelId;
@property (retain) NSString * CurrentSalary;
@property (retain) NSNumber * EducationLevelId;
@property (retain) NSString * Email;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * Ethnicity;
@property (retain) NSNumber * EthnicityId;
@property (retain) NSString * ExpectedSalary;
@property (retain) NSString * FirstName;
@property (retain) NSString * Forte;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * HomeTown;
@property (retain) NSNumber * HomeTownId;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsManager;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) NSString * LastName;
@property (retain) NSString * MainLanguage;
@property (retain) NSNumber * MainLanguageId;
@property (retain) NSNumber * MajorId;
@property (retain) NSNumber * MaritalStatusId;
@property (retain) NSString * Mobile;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSNumber * NumEmpManaged;
@property (retain) NSString * OfficePhone;
@property (retain) NSNumber * RecCadidateSupplierId;
@property (retain) empl_tns1_ArrayOfRecCandidateApplication * RecCandidateApplications;
@property (retain) NSString * RecCandidateCode;
@property (retain) empl_tns1_ArrayOfRecCandidateCompetencyRating * RecCandidateCompetencyRatings;
@property (retain) empl_tns1_ArrayOfRecCandidateDegree * RecCandidateDegrees;
@property (retain) empl_tns1_ArrayOfRecCandidateExperience * RecCandidateExperiences;
@property (retain) empl_tns1_ArrayOfRecCandidateFamilyRelationship * RecCandidateFamilyRelationships;
@property (retain) empl_tns1_ArrayOfRecCandidateForeignLanguage * RecCandidateForeignLanguages;
@property (retain) NSNumber * RecCandidateId;
@property (retain) empl_tns1_ArrayOfRecCandidateQualification * RecCandidateQualifications;
@property (retain) empl_tns1_ArrayOfRecCandidateSkill * RecCandidateSkills;
@property (retain) empl_tns1_RecCandidateStatu * RecCandidateStatu;
@property (retain) NSNumber * RecCandidateStatusId;
@property (retain) empl_tns1_RecCandidateSupplier * RecCandidateSupplier;
@property (retain) empl_tns1_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
@property (retain) NSString * Religion;
@property (retain) NSNumber * ReligionId;
@property (retain) NSString * Weaknesses;
@property (retain) NSNumber * YearsExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecCandidateProficencyLevelRating : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateProficencyLevelRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecCandidateProficencyLevelRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateProficencyLevelRating:(empl_tns1_RecCandidateProficencyLevelRating *)toAdd;
@property (readonly) NSMutableArray * RecCandidateProficencyLevelRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecCandidateCompetencyRating : NSObject {
	
/* elements */
	empl_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * Mark;
	NSDate * ModifiedDate;
	NSString * Note;
	empl_tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateCompetencyRatingId;
	NSNumber * RecCandidateId;
	empl_tns1_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecCandidateCompetencyRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Mark;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) empl_tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateCompetencyRatingId;
@property (retain) NSNumber * RecCandidateId;
@property (retain) empl_tns1_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecCandidateProficencyLevelRating : NSObject {
	
/* elements */
	empl_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * Mark;
	NSDate * ModifiedDate;
	NSString * Note;
	empl_tns1_OrgProficencyLevel * OrgProficencyLevel;
	NSNumber * OrgProficencyLevelId;
	empl_tns1_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
	empl_tns1_RecCandidateCompetencyRating * RecCandidateCompetencyRating;
	NSNumber * RecCandidateCompetencyRatingId;
	NSNumber * RecCandidateProficencyLevelRatingId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecCandidateProficencyLevelRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Mark;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) empl_tns1_OrgProficencyLevel * OrgProficencyLevel;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) empl_tns1_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
@property (retain) empl_tns1_RecCandidateCompetencyRating * RecCandidateCompetencyRating;
@property (retain) NSNumber * RecCandidateCompetencyRatingId;
@property (retain) NSNumber * RecCandidateProficencyLevelRatingId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecCanProficencyLevelDetailRating : NSObject {
	
/* elements */
	empl_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * Mark;
	NSDate * ModifiedDate;
	NSString * Note;
	empl_tns1_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
	NSNumber * OrgProficencyLevelDetailId;
	empl_tns1_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
	NSNumber * RecCanProficencyLevelDetailRatingId;
	empl_tns1_RecCandidateProficencyLevelRating * RecCandidateProficencyLevelRating;
	NSNumber * RecCandidateProficencyLevelRatingId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecCanProficencyLevelDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Mark;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) empl_tns1_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) empl_tns1_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
@property (retain) NSNumber * RecCanProficencyLevelDetailRatingId;
@property (retain) empl_tns1_RecCandidateProficencyLevelRating * RecCandidateProficencyLevelRating;
@property (retain) NSNumber * RecCandidateProficencyLevelRatingId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_RecCanProficencyDetailRating : NSObject {
	
/* elements */
	empl_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * Mark;
	NSDate * ModifiedDate;
	NSString * Note;
	empl_tns1_OrgProficencyDetail * OrgProficencyDetail;
	NSNumber * OrgProficencyDetailId;
	NSNumber * RecCanProficencyDetailRatingId;
	empl_tns1_RecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRating;
	NSNumber * RecCanProficencyLevelDetailRatingId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_RecCanProficencyDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Mark;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) empl_tns1_OrgProficencyDetail * OrgProficencyDetail;
@property (retain) NSNumber * OrgProficencyDetailId;
@property (retain) NSNumber * RecCanProficencyDetailRatingId;
@property (retain) empl_tns1_RecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRating;
@property (retain) NSNumber * RecCanProficencyLevelDetailRatingId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfRecCanProficencyDetailRating : NSObject {
	
/* elements */
	NSMutableArray *RecCanProficencyDetailRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfRecCanProficencyDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCanProficencyDetailRating:(empl_tns1_RecCanProficencyDetailRating *)toAdd;
@property (readonly) NSMutableArray * RecCanProficencyDetailRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_OrgProficencyDetail : NSObject {
	
/* elements */
	empl_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgCompetencyTypeId;
	NSNumber * OrgProficencyDetailId;
	empl_tns1_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
	NSNumber * OrgProficencyLevelDetailId;
	empl_tns1_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_OrgProficencyDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgCompetencyTypeId;
@property (retain) NSNumber * OrgProficencyDetailId;
@property (retain) empl_tns1_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) empl_tns1_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfOrgProficencyDetail : NSObject {
	
/* elements */
	NSMutableArray *OrgProficencyDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfOrgProficencyDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgProficencyDetail:(empl_tns1_OrgProficencyDetail *)toAdd;
@property (readonly) NSMutableArray * OrgProficencyDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfOrgProficencyLevelDetail : NSObject {
	
/* elements */
	NSMutableArray *OrgProficencyLevelDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfOrgProficencyLevelDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgProficencyLevelDetail:(empl_tns1_OrgProficencyLevelDetail *)toAdd;
@property (readonly) NSMutableArray * OrgProficencyLevelDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_TrainingPlanProfiency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	empl_tns1_OrgProficencyType * OrgProficencyType;
	NSNumber * OrgProficencyTypeId;
	NSNumber * ProficencyId;
	NSNumber * TrainingPlanProfiencyId;
	NSNumber * TrainingPlanRequestId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_TrainingPlanProfiency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_OrgProficencyType * OrgProficencyType;
@property (retain) NSNumber * OrgProficencyTypeId;
@property (retain) NSNumber * ProficencyId;
@property (retain) NSNumber * TrainingPlanProfiencyId;
@property (retain) NSNumber * TrainingPlanRequestId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfTrainingPlanProfiency : NSObject {
	
/* elements */
	NSMutableArray *TrainingPlanProfiency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfTrainingPlanProfiency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingPlanProfiency:(empl_tns1_TrainingPlanProfiency *)toAdd;
@property (readonly) NSMutableArray * TrainingPlanProfiency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_OrgProficencyType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	empl_tns1_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
	NSNumber * OrgProficencyTypeId;
	NSString * ProficencyTypeCode;
	NSString * ProficencyTypeName;
	empl_tns1_ArrayOfTrainingPlanProfiency * TrainingPlanProfiencies;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_OrgProficencyType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
@property (retain) NSNumber * OrgProficencyTypeId;
@property (retain) NSString * ProficencyTypeCode;
@property (retain) NSString * ProficencyTypeName;
@property (retain) empl_tns1_ArrayOfTrainingPlanProfiency * TrainingPlanProfiencies;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_OrgProficencyLevelDetail : NSObject {
	
/* elements */
	empl_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	empl_tns1_ArrayOfOrgProficencyDetail * OrgProficencyDetails;
	empl_tns1_OrgProficencyLevel * OrgProficencyLevel;
	NSNumber * OrgProficencyLevelDetailId;
	NSNumber * OrgProficencyLevelId;
	empl_tns1_OrgProficencyType * OrgProficencyType;
	NSNumber * OrgProficencyTypeId;
	empl_tns1_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
	NSString * Statement;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_OrgProficencyLevelDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_ArrayOfOrgProficencyDetail * OrgProficencyDetails;
@property (retain) empl_tns1_OrgProficencyLevel * OrgProficencyLevel;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) empl_tns1_OrgProficencyType * OrgProficencyType;
@property (retain) NSNumber * OrgProficencyTypeId;
@property (retain) empl_tns1_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
@property (retain) NSString * Statement;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProficencyLevelDetailRating : NSObject {
	
/* elements */
	NSDate * AppraisalDate;
	empl_tns1_CLogRating * CLogRating;
	NSDate * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
	NSNumber * EmpProficencyLevelDetailRatingId;
	empl_tns1_EmpProficencyLevelRating * EmpProficencyLevelRating;
	NSNumber * EmpProficencyLevelRatingId;
	NSString * ManagerNote;
	NSNumber * ManagerRating;
	empl_tns1_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
	NSNumber * OrgProficencyLevelDetailId;
	NSNumber * PeriodicallyAssessment;
	NSNumber * Score;
	NSString * SelfNote;
	NSNumber * SelfRatingId;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProficencyLevelDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AppraisalDate;
@property (retain) empl_tns1_CLogRating * CLogRating;
@property (retain) NSDate * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
@property (retain) NSNumber * EmpProficencyLevelDetailRatingId;
@property (retain) empl_tns1_EmpProficencyLevelRating * EmpProficencyLevelRating;
@property (retain) NSNumber * EmpProficencyLevelRatingId;
@property (retain) NSString * ManagerNote;
@property (retain) NSNumber * ManagerRating;
@property (retain) empl_tns1_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) NSNumber * PeriodicallyAssessment;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfNote;
@property (retain) NSNumber * SelfRatingId;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProficencyDetailRating : NSObject {
	
/* elements */
	NSDate * AppraisalDate;
	empl_tns1_CLogRating * CLogRating;
	NSDate * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpProficencyDetailRatingId;
	empl_tns1_EmpProficencyLevelDetailRating * EmpProficencyLevelDetailRating;
	NSNumber * EmpProficencyLevelDetailRatingId;
	NSString * ManagerNote;
	NSNumber * ManagerRating;
	empl_tns1_OrgProficencyDetail * OrgProficencyDetail;
	NSNumber * OrgProficencyDetailId;
	NSNumber * PeriodicallyAssessment;
	NSString * SelfNote;
	NSNumber * SelfRating;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProficencyDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AppraisalDate;
@property (retain) empl_tns1_CLogRating * CLogRating;
@property (retain) NSDate * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProficencyDetailRatingId;
@property (retain) empl_tns1_EmpProficencyLevelDetailRating * EmpProficencyLevelDetailRating;
@property (retain) NSNumber * EmpProficencyLevelDetailRatingId;
@property (retain) NSString * ManagerNote;
@property (retain) NSNumber * ManagerRating;
@property (retain) empl_tns1_OrgProficencyDetail * OrgProficencyDetail;
@property (retain) NSNumber * OrgProficencyDetailId;
@property (retain) NSNumber * PeriodicallyAssessment;
@property (retain) NSString * SelfNote;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProficencyDetailRating : NSObject {
	
/* elements */
	NSMutableArray *EmpProficencyDetailRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProficencyDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProficencyDetailRating:(empl_tns1_EmpProficencyDetailRating *)toAdd;
@property (readonly) NSMutableArray * EmpProficencyDetailRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileWorkingExperience : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	empl_tns1_CLogRating * CLogRating;
	NSNumber * CompanyId;
	NSString * CompanyName;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileWorkingExperienceId;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	USBoolean * IsInternal;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * NotableAchievements;
	NSString * Note;
	NSNumber * OrgProjectTypeId;
	NSNumber * OrgTimeInChargeId;
	NSString * ProjectName;
	NSNumber * RatingId;
	NSNumber * RelateRowId;
	NSString * Responsibilities;
	NSString * Role;
	NSDate * StartDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileWorkingExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) empl_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompanyName;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileWorkingExperienceId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInternal;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * NotableAchievements;
@property (retain) NSString * Note;
@property (retain) NSNumber * OrgProjectTypeId;
@property (retain) NSNumber * OrgTimeInChargeId;
@property (retain) NSString * ProjectName;
@property (retain) NSNumber * RatingId;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * Responsibilities;
@property (retain) NSString * Role;
@property (retain) NSDate * StartDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileWorkingExperience : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileWorkingExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileWorkingExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileWorkingExperience:(empl_tns1_EmpProfileWorkingExperience *)toAdd;
@property (readonly) NSMutableArray * EmpProfileWorkingExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfLMSCourse : NSObject {
	
/* elements */
	NSMutableArray *LMSCourse;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfLMSCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourse:(empl_tns1_LMSCourse *)toAdd;
@property (readonly) NSMutableArray * LMSCourse;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CLogCourseStatu : NSObject {
	
/* elements */
	NSNumber * CourseStatusId;
	USBoolean * IsDeleted;
	empl_tns1_ArrayOfLMSCourse * LMSCourses;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CLogCourseStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CourseStatusId;
@property (retain) USBoolean * IsDeleted;
@property (retain) empl_tns1_ArrayOfLMSCourse * LMSCourses;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_GPEmployeeScoreCardAppraisal : NSObject {
	
/* elements */
	NSNumber * AppraisalTime;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
	NSNumber * GPEmployeeScoreCardAppraisalId;
	NSNumber * GPEmployeeScoreCardId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * Score;
	NSString * SelfComment;
	NSNumber * SelfRating;
	NSNumber * SelfScore;
	NSNumber * Status;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_GPEmployeeScoreCardAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AppraisalTime;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
@property (retain) NSNumber * GPEmployeeScoreCardAppraisalId;
@property (retain) NSNumber * GPEmployeeScoreCardId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * SelfScore;
@property (retain) NSNumber * Status;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfGPEmployeeScoreCardAppraisal : NSObject {
	
/* elements */
	NSMutableArray *GPEmployeeScoreCardAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfGPEmployeeScoreCardAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPEmployeeScoreCardAppraisal:(empl_tns1_GPEmployeeScoreCardAppraisal *)toAdd;
@property (readonly) NSMutableArray * GPEmployeeScoreCardAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_GPEmployeeScoreCardProgress : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
	NSNumber * GPEmployeeScoreCardId;
	NSNumber * GPEmployeeScoreCardProgressId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * NewValue;
	NSNumber * OldValue;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_GPEmployeeScoreCardProgress *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
@property (retain) NSNumber * GPEmployeeScoreCardId;
@property (retain) NSNumber * GPEmployeeScoreCardProgressId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NewValue;
@property (retain) NSNumber * OldValue;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfGPEmployeeScoreCardProgress : NSObject {
	
/* elements */
	NSMutableArray *GPEmployeeScoreCardProgress;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfGPEmployeeScoreCardProgress *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPEmployeeScoreCardProgress:(empl_tns1_GPEmployeeScoreCardProgress *)toAdd;
@property (readonly) NSMutableArray * GPEmployeeScoreCardProgress;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_GPAdditionAppraisal : NSObject {
	
/* elements */
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_Employee * Employee;
	empl_tns1_Employee * Employee1;
	NSNumber * EmployeeId;
	NSNumber * GPAdditionAppraisalId;
	empl_tns1_GPExecutionPlan * GPExecutionPlan;
	NSNumber * GPExecutionPlanId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * SupervisorId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_GPAdditionAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) empl_tns1_Employee * Employee1;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * GPAdditionAppraisalId;
@property (retain) empl_tns1_GPExecutionPlan * GPExecutionPlan;
@property (retain) NSNumber * GPExecutionPlanId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * SupervisorId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfGPAdditionAppraisal : NSObject {
	
/* elements */
	NSMutableArray *GPAdditionAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfGPAdditionAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPAdditionAppraisal:(empl_tns1_GPAdditionAppraisal *)toAdd;
@property (readonly) NSMutableArray * GPAdditionAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_GPEmployeeWholePlanAppraisal : NSObject {
	
/* elements */
	NSNumber * AppraisalTime;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * GPEmployeeWholePlanAppraisalId;
	empl_tns1_GPExecutionPlan * GPExecutionPlan;
	NSNumber * GPExecutionPlanId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * Result;
	NSNumber * Score;
	NSString * SelfComment;
	NSNumber * SelfRating;
	NSNumber * SelfScore;
	NSNumber * Status;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_GPEmployeeWholePlanAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AppraisalTime;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * GPEmployeeWholePlanAppraisalId;
@property (retain) empl_tns1_GPExecutionPlan * GPExecutionPlan;
@property (retain) NSNumber * GPExecutionPlanId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * Result;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * SelfScore;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfGPEmployeeWholePlanAppraisal : NSObject {
	
/* elements */
	NSMutableArray *GPEmployeeWholePlanAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfGPEmployeeWholePlanAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPEmployeeWholePlanAppraisal:(empl_tns1_GPEmployeeWholePlanAppraisal *)toAdd;
@property (readonly) NSMutableArray * GPEmployeeWholePlanAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_GPPerformanceYearEndResult : NSObject {
	
/* elements */
	NSString * ActivityName;
	NSNumber * Actual;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	empl_tns1_GPExecutionPlan * GPExecutionPlan;
	NSNumber * GPPerformancePlanId;
	NSNumber * GPPerformanceYearEndResultId;
	USBoolean * HalfYearResult;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Progress;
	NSNumber * Rate;
	NSString * Result;
	NSNumber * Target;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_GPPerformanceYearEndResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ActivityName;
@property (retain) NSNumber * Actual;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) empl_tns1_GPExecutionPlan * GPExecutionPlan;
@property (retain) NSNumber * GPPerformancePlanId;
@property (retain) NSNumber * GPPerformanceYearEndResultId;
@property (retain) USBoolean * HalfYearResult;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Progress;
@property (retain) NSNumber * Rate;
@property (retain) NSString * Result;
@property (retain) NSNumber * Target;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfGPPerformanceYearEndResult : NSObject {
	
/* elements */
	NSMutableArray *GPPerformanceYearEndResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfGPPerformanceYearEndResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPPerformanceYearEndResult:(empl_tns1_GPPerformanceYearEndResult *)toAdd;
@property (readonly) NSMutableArray * GPPerformanceYearEndResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_GPExecutionPlan : NSObject {
	
/* elements */
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmployeeId;
	empl_tns1_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals;
	empl_tns1_ArrayOfGPEmployeeWholePlanAppraisal * GPEmployeeWholePlanAppraisals;
	empl_tns1_ArrayOfGPExecutionPlanDetail * GPExecutionPlanDetails;
	NSNumber * GPExecutionPlanId;
	empl_tns1_ArrayOfGPPerformanceYearEndResult * GPPerformanceYearEndResults;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * Period;
	NSNumber * Status;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_GPExecutionPlan *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmployeeId;
@property (retain) empl_tns1_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals;
@property (retain) empl_tns1_ArrayOfGPEmployeeWholePlanAppraisal * GPEmployeeWholePlanAppraisals;
@property (retain) empl_tns1_ArrayOfGPExecutionPlanDetail * GPExecutionPlanDetails;
@property (retain) NSNumber * GPExecutionPlanId;
@property (retain) empl_tns1_ArrayOfGPPerformanceYearEndResult * GPPerformanceYearEndResults;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * Period;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_GPExecutionPlanActivityAppraisal : NSObject {
	
/* elements */
	NSNumber * AppraisalTime;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_GPExecutionPlanActivity * GPExecutionPlanActivity;
	NSNumber * GPExecutionPlanActivityAppraisalId;
	NSNumber * GPExecutionPlanActivityId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * Score;
	NSString * SelfComment;
	NSNumber * SelfRating;
	NSNumber * SelfScore;
	NSNumber * Status;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_GPExecutionPlanActivityAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AppraisalTime;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_GPExecutionPlanActivity * GPExecutionPlanActivity;
@property (retain) NSNumber * GPExecutionPlanActivityAppraisalId;
@property (retain) NSNumber * GPExecutionPlanActivityId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * SelfScore;
@property (retain) NSNumber * Status;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfGPExecutionPlanActivityAppraisal : NSObject {
	
/* elements */
	NSMutableArray *GPExecutionPlanActivityAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfGPExecutionPlanActivityAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPExecutionPlanActivityAppraisal:(empl_tns1_GPExecutionPlanActivityAppraisal *)toAdd;
@property (readonly) NSMutableArray * GPExecutionPlanActivityAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_GPPeriodicReport : NSObject {
	
/* elements */
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_GPExecutionPlanActivity * GPExecutionPlanActivity;
	NSNumber * GPExecutionPlanActivityId;
	NSNumber * GPPeriodicReportId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSDate * ReportDateTime;
	NSNumber * ReportTimes;
	NSNumber * Status;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_GPPeriodicReport *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_GPExecutionPlanActivity * GPExecutionPlanActivity;
@property (retain) NSNumber * GPExecutionPlanActivityId;
@property (retain) NSNumber * GPPeriodicReportId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * ReportDateTime;
@property (retain) NSNumber * ReportTimes;
@property (retain) NSNumber * Status;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfGPPeriodicReport : NSObject {
	
/* elements */
	NSMutableArray *GPPeriodicReport;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfGPPeriodicReport *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPPeriodicReport:(empl_tns1_GPPeriodicReport *)toAdd;
@property (readonly) NSMutableArray * GPPeriodicReport;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_GPExecutionPlanActivity : NSObject {
	
/* elements */
	NSString * AchievementLevel;
	NSString * ActivityGroup;
	NSString * ActivityName;
	NSNumber * Budget;
	NSString * BudgetUnit;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * EndDate;
	empl_tns1_ArrayOfGPExecutionPlanActivityAppraisal * GPExecutionPlanActivityAppraisals;
	NSNumber * GPExecutionPlanActivityId;
	empl_tns1_GPExecutionPlanDetail * GPExecutionPlanDetail;
	NSNumber * GPExecutionPlanDetailId;
	empl_tns1_ArrayOfGPPeriodicReport * GPPeriodicReports;
	USBoolean * IsDeleted;
	NSNumber * Measure;
	NSString * MeasureUnit;
	NSDate * ModifiedDate;
	NSDate * StartDate;
	NSString * Status;
	NSNumber * Target;
	NSString * TargetUnit;
	NSNumber * Variance;
	NSString * VarianceUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_GPExecutionPlanActivity *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AchievementLevel;
@property (retain) NSString * ActivityGroup;
@property (retain) NSString * ActivityName;
@property (retain) NSNumber * Budget;
@property (retain) NSString * BudgetUnit;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) empl_tns1_ArrayOfGPExecutionPlanActivityAppraisal * GPExecutionPlanActivityAppraisals;
@property (retain) NSNumber * GPExecutionPlanActivityId;
@property (retain) empl_tns1_GPExecutionPlanDetail * GPExecutionPlanDetail;
@property (retain) NSNumber * GPExecutionPlanDetailId;
@property (retain) empl_tns1_ArrayOfGPPeriodicReport * GPPeriodicReports;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Measure;
@property (retain) NSString * MeasureUnit;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * StartDate;
@property (retain) NSString * Status;
@property (retain) NSNumber * Target;
@property (retain) NSString * TargetUnit;
@property (retain) NSNumber * Variance;
@property (retain) NSString * VarianceUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfGPExecutionPlanActivity : NSObject {
	
/* elements */
	NSMutableArray *GPExecutionPlanActivity;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfGPExecutionPlanActivity *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPExecutionPlanActivity:(empl_tns1_GPExecutionPlanActivity *)toAdd;
@property (readonly) NSMutableArray * GPExecutionPlanActivity;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_GPExecutionPlanDetail : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_GPExecutionPlan * GPExecutionPlan;
	empl_tns1_ArrayOfGPExecutionPlanActivity * GPExecutionPlanActivities;
	NSNumber * GPExecutionPlanDetailId;
	NSNumber * GPExecutionPlanId;
	empl_tns1_GPIndividualYearlyObjective * GPIndividualYearlyObjective;
	NSNumber * GPIndividualYearlyObjectiveId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_GPExecutionPlanDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_GPExecutionPlan * GPExecutionPlan;
@property (retain) empl_tns1_ArrayOfGPExecutionPlanActivity * GPExecutionPlanActivities;
@property (retain) NSNumber * GPExecutionPlanDetailId;
@property (retain) NSNumber * GPExecutionPlanId;
@property (retain) empl_tns1_GPIndividualYearlyObjective * GPIndividualYearlyObjective;
@property (retain) NSNumber * GPIndividualYearlyObjectiveId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfGPExecutionPlanDetail : NSObject {
	
/* elements */
	NSMutableArray *GPExecutionPlanDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfGPExecutionPlanDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPExecutionPlanDetail:(empl_tns1_GPExecutionPlanDetail *)toAdd;
@property (readonly) NSMutableArray * GPExecutionPlanDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_GPIndividualYearlyObjectiveAppraisal : NSObject {
	
/* elements */
	NSNumber * AppraisalTime;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_GPIndividualYearlyObjective * GPIndividualYearlyObjective;
	NSNumber * GPIndividualYearlyObjectiveAppraisalId;
	NSNumber * GPIndividualYearlyObjectiveId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * Score;
	NSString * SelfComment;
	NSNumber * SelfRating;
	NSNumber * SelfScore;
	NSNumber * Status;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_GPIndividualYearlyObjectiveAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AppraisalTime;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_GPIndividualYearlyObjective * GPIndividualYearlyObjective;
@property (retain) NSNumber * GPIndividualYearlyObjectiveAppraisalId;
@property (retain) NSNumber * GPIndividualYearlyObjectiveId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * SelfScore;
@property (retain) NSNumber * Status;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfGPIndividualYearlyObjectiveAppraisal : NSObject {
	
/* elements */
	NSMutableArray *GPIndividualYearlyObjectiveAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfGPIndividualYearlyObjectiveAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPIndividualYearlyObjectiveAppraisal:(empl_tns1_GPIndividualYearlyObjectiveAppraisal *)toAdd;
@property (readonly) NSMutableArray * GPIndividualYearlyObjectiveAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_GPPerspectiveValueDetail : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_GPPerspectiveValue * GPPerspectiveValue;
	empl_tns1_GPPerspectiveValue * GPPerspectiveValue1;
	NSNumber * GPPerspectiveValueDetailId;
	NSNumber * GPStrategyMapObjectId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * PerspectiveValueDestinationId;
	NSNumber * PerspectiveValueSourceId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_GPPerspectiveValueDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_GPPerspectiveValue * GPPerspectiveValue;
@property (retain) empl_tns1_GPPerspectiveValue * GPPerspectiveValue1;
@property (retain) NSNumber * GPPerspectiveValueDetailId;
@property (retain) NSNumber * GPStrategyMapObjectId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * PerspectiveValueDestinationId;
@property (retain) NSNumber * PerspectiveValueSourceId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfGPPerspectiveValueDetail : NSObject {
	
/* elements */
	NSMutableArray *GPPerspectiveValueDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfGPPerspectiveValueDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPPerspectiveValueDetail:(empl_tns1_GPPerspectiveValueDetail *)toAdd;
@property (readonly) NSMutableArray * GPPerspectiveValueDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_GPPerspectiveValue : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	empl_tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	empl_tns1_ArrayOfGPPerspectiveValueDetail * GPPerspectiveValueDetails;
	empl_tns1_ArrayOfGPPerspectiveValueDetail * GPPerspectiveValueDetails1;
	NSNumber * GPPerspectiveValueId;
	empl_tns1_GPStrategyMapObject * GPStrategyMapObject;
	NSNumber * GPStrategyMapObjectId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * PositionX;
	NSNumber * PositionY;
	NSString * ValueType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_GPPerspectiveValue *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) empl_tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) empl_tns1_ArrayOfGPPerspectiveValueDetail * GPPerspectiveValueDetails;
@property (retain) empl_tns1_ArrayOfGPPerspectiveValueDetail * GPPerspectiveValueDetails1;
@property (retain) NSNumber * GPPerspectiveValueId;
@property (retain) empl_tns1_GPStrategyMapObject * GPStrategyMapObject;
@property (retain) NSNumber * GPStrategyMapObjectId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * PositionX;
@property (retain) NSNumber * PositionY;
@property (retain) NSString * ValueType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfGPPerspectiveValue : NSObject {
	
/* elements */
	NSMutableArray *GPPerspectiveValue;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfGPPerspectiveValue *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPPerspectiveValue:(empl_tns1_GPPerspectiveValue *)toAdd;
@property (readonly) NSMutableArray * GPPerspectiveValue;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfGPCompanyYearlyObjective : NSObject {
	
/* elements */
	NSMutableArray *GPCompanyYearlyObjective;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfGPCompanyYearlyObjective *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPCompanyYearlyObjective:(empl_tns1_GPCompanyYearlyObjective *)toAdd;
@property (readonly) NSMutableArray * GPCompanyYearlyObjective;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfGPStrategyMapObject : NSObject {
	
/* elements */
	NSMutableArray *GPStrategyMapObject;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfGPStrategyMapObject *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPStrategyMapObject:(empl_tns1_GPStrategyMapObject *)toAdd;
@property (readonly) NSMutableArray * GPStrategyMapObject;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_GPStrategy : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EndYear;
	empl_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
	NSNumber * GPStrategyId;
	empl_tns1_ArrayOfGPStrategyMapObject * GPStrategyMapObjects;
	USBoolean * IsActived;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * StartYear;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_GPStrategy *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EndYear;
@property (retain) empl_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
@property (retain) NSNumber * GPStrategyId;
@property (retain) empl_tns1_ArrayOfGPStrategyMapObject * GPStrategyMapObjects;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * StartYear;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_GPStrategyMapObject : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	empl_tns1_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails;
	empl_tns1_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
	empl_tns1_ArrayOfGPPerspectiveValue * GPPerspectiveValues;
	empl_tns1_GPStrategy * GPStrategy;
	NSNumber * GPStrategyId;
	NSNumber * GPStrategyMapObjectId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_GPStrategyMapObject *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) empl_tns1_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails;
@property (retain) empl_tns1_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
@property (retain) empl_tns1_ArrayOfGPPerspectiveValue * GPPerspectiveValues;
@property (retain) empl_tns1_GPStrategy * GPStrategy;
@property (retain) NSNumber * GPStrategyId;
@property (retain) NSNumber * GPStrategyMapObjectId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_GPCompanyStrategicGoalDetail : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_GPCompanyStrategicGoal * GPCompanyStrategicGoal;
	empl_tns1_GPCompanyStrategicGoal * GPCompanyStrategicGoal1;
	NSNumber * GPCompanyStrategicGoalDestinationId;
	NSNumber * GPCompanyStrategicGoalDetailId;
	NSNumber * GPCompanyStrategicGoalSourceId;
	empl_tns1_GPStrategyMapObject * GPStrategyMapObject;
	NSNumber * GPStrategyMapObjectId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_GPCompanyStrategicGoalDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_GPCompanyStrategicGoal * GPCompanyStrategicGoal;
@property (retain) empl_tns1_GPCompanyStrategicGoal * GPCompanyStrategicGoal1;
@property (retain) NSNumber * GPCompanyStrategicGoalDestinationId;
@property (retain) NSNumber * GPCompanyStrategicGoalDetailId;
@property (retain) NSNumber * GPCompanyStrategicGoalSourceId;
@property (retain) empl_tns1_GPStrategyMapObject * GPStrategyMapObject;
@property (retain) NSNumber * GPStrategyMapObjectId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfGPCompanyStrategicGoalDetail : NSObject {
	
/* elements */
	NSMutableArray *GPCompanyStrategicGoalDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfGPCompanyStrategicGoalDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPCompanyStrategicGoalDetail:(empl_tns1_GPCompanyStrategicGoalDetail *)toAdd;
@property (readonly) NSMutableArray * GPCompanyStrategicGoalDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_GPCompanyStrategicGoal : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_Employee * Employee;
	empl_tns1_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails;
	empl_tns1_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails1;
	NSNumber * GPCompanyStrategicGoalId;
	empl_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
	empl_tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	NSNumber * GPStrategyId;
	empl_tns1_GPStrategyMapObject * GPStrategyMapObject;
	NSNumber * GPStrategyMapObjectId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OwnerId;
	NSString * PeriodStrategy;
	NSString * SmartAttainableGoal;
	NSString * SmartMeasureGoal;
	NSString * SmartRelevantGoal;
	NSString * SmartSpecificGoal;
	NSString * SmartTimeGoal;
	NSString * Statement;
	NSNumber * YearBegin;
	NSNumber * YearEnd;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_GPCompanyStrategicGoal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) empl_tns1_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails;
@property (retain) empl_tns1_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails1;
@property (retain) NSNumber * GPCompanyStrategicGoalId;
@property (retain) empl_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
@property (retain) empl_tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) NSNumber * GPStrategyId;
@property (retain) empl_tns1_GPStrategyMapObject * GPStrategyMapObject;
@property (retain) NSNumber * GPStrategyMapObjectId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OwnerId;
@property (retain) NSString * PeriodStrategy;
@property (retain) NSString * SmartAttainableGoal;
@property (retain) NSString * SmartMeasureGoal;
@property (retain) NSString * SmartRelevantGoal;
@property (retain) NSString * SmartSpecificGoal;
@property (retain) NSString * SmartTimeGoal;
@property (retain) NSString * Statement;
@property (retain) NSNumber * YearBegin;
@property (retain) NSNumber * YearEnd;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfGPCompanyStrategicGoal : NSObject {
	
/* elements */
	NSMutableArray *GPCompanyStrategicGoal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfGPCompanyStrategicGoal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPCompanyStrategicGoal:(empl_tns1_GPCompanyStrategicGoal *)toAdd;
@property (readonly) NSMutableArray * GPCompanyStrategicGoal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_GPObjectiveInitiative : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_Employee * Employee;
	empl_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
	NSNumber * GPObjectiveInitiativeId;
	empl_tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	USBoolean * IsDeleted;
	empl_tns1_ArrayOfLMSCourse * LMSCourses;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * OwnerId;
	NSNumber * Progress;
	NSString * Purpose;
	NSNumber * YearBegin;
	NSNumber * YearEnd;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_GPObjectiveInitiative *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) empl_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
@property (retain) NSNumber * GPObjectiveInitiativeId;
@property (retain) empl_tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) USBoolean * IsDeleted;
@property (retain) empl_tns1_ArrayOfLMSCourse * LMSCourses;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * OwnerId;
@property (retain) NSNumber * Progress;
@property (retain) NSString * Purpose;
@property (retain) NSNumber * YearBegin;
@property (retain) NSNumber * YearEnd;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfGPObjectiveInitiative : NSObject {
	
/* elements */
	NSMutableArray *GPObjectiveInitiative;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfGPObjectiveInitiative *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPObjectiveInitiative:(empl_tns1_GPObjectiveInitiative *)toAdd;
@property (readonly) NSMutableArray * GPObjectiveInitiative;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_GPPerspectiveMeasure : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
	empl_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
	empl_tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	NSNumber * GPPerspectiveMeasureId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_GPPerspectiveMeasure *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
@property (retain) empl_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
@property (retain) empl_tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) NSNumber * GPPerspectiveMeasureId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfGPPerspectiveMeasure : NSObject {
	
/* elements */
	NSMutableArray *GPPerspectiveMeasure;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfGPPerspectiveMeasure *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPPerspectiveMeasure:(empl_tns1_GPPerspectiveMeasure *)toAdd;
@property (readonly) NSMutableArray * GPPerspectiveMeasure;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_GPPerspective : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	empl_tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
	empl_tns1_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
	empl_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
	empl_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
	empl_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
	empl_tns1_ArrayOfGPObjectiveInitiative * GPObjectiveInitiatives;
	NSNumber * GPPerspectiveId;
	empl_tns1_ArrayOfGPPerspectiveMeasure * GPPerspectiveMeasures;
	empl_tns1_ArrayOfGPPerspectiveValue * GPPerspectiveValues;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_GPPerspective *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) empl_tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
@property (retain) empl_tns1_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
@property (retain) empl_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
@property (retain) empl_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
@property (retain) empl_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
@property (retain) empl_tns1_ArrayOfGPObjectiveInitiative * GPObjectiveInitiatives;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) empl_tns1_ArrayOfGPPerspectiveMeasure * GPPerspectiveMeasures;
@property (retain) empl_tns1_ArrayOfGPPerspectiveValue * GPPerspectiveValues;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_GPIndividualYearlyObjective : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_Employee * Employee;
	empl_tns1_Employee * Employee1;
	NSNumber * EmployeeId;
	empl_tns1_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
	NSNumber * GPCompanyYearlyObjectiveId;
	empl_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
	NSNumber * GPEmployeeScoreCardId;
	empl_tns1_ArrayOfGPExecutionPlanDetail * GPExecutionPlanDetails;
	empl_tns1_ArrayOfGPIndividualYearlyObjectiveAppraisal * GPIndividualYearlyObjectiveAppraisals;
	NSNumber * GPIndividualYearlyObjectiveId;
	empl_tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * ObjectiveType;
	NSNumber * Owner;
	NSString * SmartAttainableGoal;
	NSString * SmartMeasureGoal;
	NSString * SmartRelevantGoal;
	NSString * SmartSpecificGoal;
	NSString * SmartTimeGoal;
	NSString * Statement;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_GPIndividualYearlyObjective *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) empl_tns1_Employee * Employee1;
@property (retain) NSNumber * EmployeeId;
@property (retain) empl_tns1_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
@property (retain) NSNumber * GPCompanyYearlyObjectiveId;
@property (retain) empl_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
@property (retain) NSNumber * GPEmployeeScoreCardId;
@property (retain) empl_tns1_ArrayOfGPExecutionPlanDetail * GPExecutionPlanDetails;
@property (retain) empl_tns1_ArrayOfGPIndividualYearlyObjectiveAppraisal * GPIndividualYearlyObjectiveAppraisals;
@property (retain) NSNumber * GPIndividualYearlyObjectiveId;
@property (retain) empl_tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * ObjectiveType;
@property (retain) NSNumber * Owner;
@property (retain) NSString * SmartAttainableGoal;
@property (retain) NSString * SmartMeasureGoal;
@property (retain) NSString * SmartRelevantGoal;
@property (retain) NSString * SmartSpecificGoal;
@property (retain) NSString * SmartTimeGoal;
@property (retain) NSString * Statement;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfGPIndividualYearlyObjective : NSObject {
	
/* elements */
	NSMutableArray *GPIndividualYearlyObjective;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfGPIndividualYearlyObjective *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPIndividualYearlyObjective:(empl_tns1_GPIndividualYearlyObjective *)toAdd;
@property (readonly) NSMutableArray * GPIndividualYearlyObjective;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_GPPerformanceExecutionReport : NSObject {
	
/* elements */
	NSString * ActivityName;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
	NSNumber * GPEmployeeScoreCardId;
	NSNumber * GPPerformanceExecutionReportId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Progress;
	NSString * ReportNumber;
	NSDate * ReportTime;
	NSNumber * Reporter;
	NSNumber * Status;
	NSNumber * Target;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_GPPerformanceExecutionReport *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ActivityName;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
@property (retain) NSNumber * GPEmployeeScoreCardId;
@property (retain) NSNumber * GPPerformanceExecutionReportId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Progress;
@property (retain) NSString * ReportNumber;
@property (retain) NSDate * ReportTime;
@property (retain) NSNumber * Reporter;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * Target;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_GPEmployeeScoreCard : NSObject {
	
/* elements */
	NSNumber * Actual;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	empl_tns1_GPCompanyScoreCard * GPCompanyScoreCard;
	NSNumber * GPCompanyScoreCardId;
	empl_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCard1;
	empl_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard2;
	empl_tns1_ArrayOfGPEmployeeScoreCardAppraisal * GPEmployeeScoreCardAppraisals;
	NSNumber * GPEmployeeScoreCardId;
	empl_tns1_ArrayOfGPEmployeeScoreCardProgress * GPEmployeeScoreCardProgresses;
	empl_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
	empl_tns1_GPPerformanceExecutionReport * GPPerformanceExecutionReport;
	empl_tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	empl_tns1_GPPerspectiveMeasure * GPPerspectiveMeasure;
	NSNumber * GPPerspectiveMeasureId;
	USBoolean * IsDeleted;
	NSString * Metric;
	NSDate * ModifiedDate;
	NSNumber * ParentId;
	NSNumber * PercentageOwnership;
	NSNumber * Progress;
	NSString * ScoreCardGroup;
	NSString * ScoreCardSubGroup;
	NSString * Statement;
	NSNumber * Status;
	NSNumber * Target;
	NSNumber * Weight;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_GPEmployeeScoreCard *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Actual;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) empl_tns1_GPCompanyScoreCard * GPCompanyScoreCard;
@property (retain) NSNumber * GPCompanyScoreCardId;
@property (retain) empl_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCard1;
@property (retain) empl_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard2;
@property (retain) empl_tns1_ArrayOfGPEmployeeScoreCardAppraisal * GPEmployeeScoreCardAppraisals;
@property (retain) NSNumber * GPEmployeeScoreCardId;
@property (retain) empl_tns1_ArrayOfGPEmployeeScoreCardProgress * GPEmployeeScoreCardProgresses;
@property (retain) empl_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
@property (retain) empl_tns1_GPPerformanceExecutionReport * GPPerformanceExecutionReport;
@property (retain) empl_tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) empl_tns1_GPPerspectiveMeasure * GPPerspectiveMeasure;
@property (retain) NSNumber * GPPerspectiveMeasureId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Metric;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * ParentId;
@property (retain) NSNumber * PercentageOwnership;
@property (retain) NSNumber * Progress;
@property (retain) NSString * ScoreCardGroup;
@property (retain) NSString * ScoreCardSubGroup;
@property (retain) NSString * Statement;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * Target;
@property (retain) NSNumber * Weight;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfGPEmployeeScoreCard : NSObject {
	
/* elements */
	NSMutableArray *GPEmployeeScoreCard;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfGPEmployeeScoreCard *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPEmployeeScoreCard:(empl_tns1_GPEmployeeScoreCard *)toAdd;
@property (readonly) NSMutableArray * GPEmployeeScoreCard;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_GPCompanyScoreCard : NSObject {
	
/* elements */
	NSNumber * Actual;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_Employee * Employee;
	NSNumber * GPCompanyScoreCardId;
	empl_tns1_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
	NSNumber * GPCompanyYearlyObjectiveId;
	empl_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
	empl_tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	empl_tns1_GPPerspectiveMeasure * GPPerspectiveMeasure;
	NSNumber * GPPerspectiveMeasureId;
	USBoolean * IsDeleted;
	NSString * Metric;
	NSDate * ModifiedDate;
	NSNumber * Progress;
	NSString * ReportingRythm;
	NSNumber * ScoreCardCreator;
	NSString * Statement;
	NSNumber * Status;
	NSNumber * Target;
	NSNumber * Weight;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_GPCompanyScoreCard *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Actual;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * GPCompanyScoreCardId;
@property (retain) empl_tns1_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
@property (retain) NSNumber * GPCompanyYearlyObjectiveId;
@property (retain) empl_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
@property (retain) empl_tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) empl_tns1_GPPerspectiveMeasure * GPPerspectiveMeasure;
@property (retain) NSNumber * GPPerspectiveMeasureId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Metric;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Progress;
@property (retain) NSString * ReportingRythm;
@property (retain) NSNumber * ScoreCardCreator;
@property (retain) NSString * Statement;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * Target;
@property (retain) NSNumber * Weight;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfGPCompanyScoreCard : NSObject {
	
/* elements */
	NSMutableArray *GPCompanyScoreCard;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfGPCompanyScoreCard *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPCompanyScoreCard:(empl_tns1_GPCompanyScoreCard *)toAdd;
@property (readonly) NSMutableArray * GPCompanyScoreCard;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_GPCompanyYearlyObjective : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_Employee * Employee;
	empl_tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
	empl_tns1_GPCompanyStrategicGoal * GPCompanyStrategicGoal;
	NSNumber * GPCompanyStrategicGoalId;
	NSNumber * GPCompanyYearlyObjectiveId;
	empl_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
	empl_tns1_GPObjectiveInitiative * GPObjectiveInitiative;
	NSNumber * GPObjectiveInitiativeId;
	empl_tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	empl_tns1_GPStrategy * GPStrategy;
	NSNumber * GPStrategyId;
	USBoolean * IsDeleted;
	empl_tns1_ArrayOfLMSCourse * LMSCourses;
	NSDate * ModifiedDate;
	NSNumber * Owner;
	NSString * SmartAttainableGoal;
	NSString * SmartMeasureGoal;
	NSString * SmartRelevantGoal;
	NSString * SmartSpecificGoal;
	NSString * SmartTimeGoal;
	NSString * Statement;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_GPCompanyYearlyObjective *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) empl_tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
@property (retain) empl_tns1_GPCompanyStrategicGoal * GPCompanyStrategicGoal;
@property (retain) NSNumber * GPCompanyStrategicGoalId;
@property (retain) NSNumber * GPCompanyYearlyObjectiveId;
@property (retain) empl_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
@property (retain) empl_tns1_GPObjectiveInitiative * GPObjectiveInitiative;
@property (retain) NSNumber * GPObjectiveInitiativeId;
@property (retain) empl_tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) empl_tns1_GPStrategy * GPStrategy;
@property (retain) NSNumber * GPStrategyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) empl_tns1_ArrayOfLMSCourse * LMSCourses;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Owner;
@property (retain) NSString * SmartAttainableGoal;
@property (retain) NSString * SmartMeasureGoal;
@property (retain) NSString * SmartRelevantGoal;
@property (retain) NSString * SmartSpecificGoal;
@property (retain) NSString * SmartTimeGoal;
@property (retain) NSString * Statement;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_LMSCourseCompetency : NSObject {
	
/* elements */
	empl_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompetencyName;
	NSNumber * CompetencyTypeId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	empl_tns1_LMSCourse * LMSCourse;
	NSNumber * LMSCourseCompetencyId;
	NSNumber * LMSCourseId;
	NSDate * ModifiedDate;
	empl_tns1_OrgCompetency * OrgCompetency;
	NSNumber * OrgCompetencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_LMSCourseCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompetencyName;
@property (retain) NSNumber * CompetencyTypeId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) empl_tns1_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseCompetencyId;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_OrgCompetency * OrgCompetency;
@property (retain) NSNumber * OrgCompetencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfLMSCourseCompetency : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfLMSCourseCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseCompetency:(empl_tns1_LMSCourseCompetency *)toAdd;
@property (readonly) NSMutableArray * LMSCourseCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_LMSCourseRequiredCompetency : NSObject {
	
/* elements */
	empl_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompetencyName;
	NSString * CompetencyType;
	NSNumber * CourseRequiredCompetencyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	empl_tns1_LMSCourse * LMSCourse;
	NSNumber * LMSCourseId;
	NSDate * ModifiedDate;
	empl_tns1_OrgCompetency * OrgCompetency;
	NSNumber * OrgCompetencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_LMSCourseRequiredCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompetencyName;
@property (retain) NSString * CompetencyType;
@property (retain) NSNumber * CourseRequiredCompetencyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) empl_tns1_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_OrgCompetency * OrgCompetency;
@property (retain) NSNumber * OrgCompetencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfLMSCourseRequiredCompetency : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseRequiredCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfLMSCourseRequiredCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseRequiredCompetency:(empl_tns1_LMSCourseRequiredCompetency *)toAdd;
@property (readonly) NSMutableArray * LMSCourseRequiredCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfOrgCompetency : NSObject {
	
/* elements */
	NSMutableArray *OrgCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfOrgCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgCompetency:(empl_tns1_OrgCompetency *)toAdd;
@property (readonly) NSMutableArray * OrgCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_OrgCompetencyGroup : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * GroupName;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	empl_tns1_ArrayOfOrgCompetency * OrgCompetencies;
	NSNumber * OrgCompetencyGroupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_OrgCompetencyGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * GroupName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_ArrayOfOrgCompetency * OrgCompetencies;
@property (retain) NSNumber * OrgCompetencyGroupId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_OrgJobSpecificCompetency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	empl_tns1_OrgCompetency * OrgCompetency;
	NSNumber * OrgCompetencyId;
	empl_tns1_OrgJob * OrgJob;
	NSNumber * OrgJobId;
	NSNumber * OrgJobSpecificCompetencyId;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_OrgJobSpecificCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_OrgCompetency * OrgCompetency;
@property (retain) NSNumber * OrgCompetencyId;
@property (retain) empl_tns1_OrgJob * OrgJob;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSNumber * OrgJobSpecificCompetencyId;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfOrgJobSpecificCompetency : NSObject {
	
/* elements */
	NSMutableArray *OrgJobSpecificCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfOrgJobSpecificCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgJobSpecificCompetency:(empl_tns1_OrgJobSpecificCompetency *)toAdd;
@property (readonly) NSMutableArray * OrgJobSpecificCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfOrgProficencyLevel : NSObject {
	
/* elements */
	NSMutableArray *OrgProficencyLevel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfOrgProficencyLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgProficencyLevel:(empl_tns1_OrgProficencyLevel *)toAdd;
@property (readonly) NSMutableArray * OrgProficencyLevel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_OrgCompetency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Definition;
	USBoolean * IsCore;
	USBoolean * IsDeleted;
	empl_tns1_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
	empl_tns1_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
	empl_tns1_ArrayOfLMSTopicCompetency * LMSTopicCompetencies;
	NSDate * ModifiedDate;
	NSString * Name;
	empl_tns1_OrgCompetencyGroup * OrgCompetencyGroup;
	NSNumber * OrgCompetencyGroupId;
	NSNumber * OrgCompetencyId;
	empl_tns1_ArrayOfOrgJobSpecificCompetency * OrgJobSpecificCompetencies;
	empl_tns1_ArrayOfOrgProficencyLevel * OrgProficencyLevels;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_OrgCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Definition;
@property (retain) USBoolean * IsCore;
@property (retain) USBoolean * IsDeleted;
@property (retain) empl_tns1_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
@property (retain) empl_tns1_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
@property (retain) empl_tns1_ArrayOfLMSTopicCompetency * LMSTopicCompetencies;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) empl_tns1_OrgCompetencyGroup * OrgCompetencyGroup;
@property (retain) NSNumber * OrgCompetencyGroupId;
@property (retain) NSNumber * OrgCompetencyId;
@property (retain) empl_tns1_ArrayOfOrgJobSpecificCompetency * OrgJobSpecificCompetencies;
@property (retain) empl_tns1_ArrayOfOrgProficencyLevel * OrgProficencyLevels;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_LMSTopicCompetency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	empl_tns1_LMSContentTopic * LMSContentTopic;
	NSNumber * LMSContentTopicId;
	NSNumber * LMSTopicCompetencyId;
	NSDate * ModifiedDate;
	empl_tns1_OrgCompetency * OrgCompetency;
	NSNumber * OrgCompetencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_LMSTopicCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) empl_tns1_LMSContentTopic * LMSContentTopic;
@property (retain) NSNumber * LMSContentTopicId;
@property (retain) NSNumber * LMSTopicCompetencyId;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_OrgCompetency * OrgCompetency;
@property (retain) NSNumber * OrgCompetencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfLMSTopicCompetency : NSObject {
	
/* elements */
	NSMutableArray *LMSTopicCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfLMSTopicCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSTopicCompetency:(empl_tns1_LMSTopicCompetency *)toAdd;
@property (readonly) NSMutableArray * LMSTopicCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfLMSContentTopic : NSObject {
	
/* elements */
	NSMutableArray *LMSContentTopic;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfLMSContentTopic *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSContentTopic:(empl_tns1_LMSContentTopic *)toAdd;
@property (readonly) NSMutableArray * LMSContentTopic;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_LMSTopicGroup : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	empl_tns1_ArrayOfLMSContentTopic * LMSContentTopics;
	NSNumber * LMSTopicGroupId;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_LMSTopicGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) empl_tns1_ArrayOfLMSContentTopic * LMSContentTopics;
@property (retain) NSNumber * LMSTopicGroupId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_LMSContentTopic : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * LMSContentTopicId;
	empl_tns1_ArrayOfLMSCourse * LMSCourses;
	empl_tns1_ArrayOfLMSTopicCompetency * LMSTopicCompetencies;
	empl_tns1_LMSTopicGroup * LMSTopicGroup;
	NSNumber * LMSTopicGroupId;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_LMSContentTopic *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * LMSContentTopicId;
@property (retain) empl_tns1_ArrayOfLMSCourse * LMSCourses;
@property (retain) empl_tns1_ArrayOfLMSTopicCompetency * LMSTopicCompetencies;
@property (retain) empl_tns1_LMSTopicGroup * LMSTopicGroup;
@property (retain) NSNumber * LMSTopicGroupId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CLogLMSContentType : NSObject {
	
/* elements */
	NSNumber * CLogLMSContentTypeId;
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	empl_tns1_ArrayOfLMSCourseContent * LMSCourseContents;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CLogLMSContentType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogLMSContentTypeId;
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) empl_tns1_ArrayOfLMSCourseContent * LMSCourseContents;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_LMSCourseContent : NSObject {
	
/* elements */
	empl_tns1_CLogLMSContentType * CLogLMSContentType;
	NSNumber * CLogLMSContentType_CLogLMSContentTypeId;
	empl_tns1_LMSCourse * LMSCourse;
	empl_tns1_LMSCourseContent * LMSCourseContent1;
	NSNumber * LMSCourseContent1_LMSCourseContentId;
	NSNumber * LMSCourseContentId;
	empl_tns1_LMSCourseContent * LMSCourseContents1;
	NSNumber * LMSCourse_LMSCourseId;
	NSString * Name;
	NSString * Statement;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_LMSCourseContent *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_CLogLMSContentType * CLogLMSContentType;
@property (retain) NSNumber * CLogLMSContentType_CLogLMSContentTypeId;
@property (retain) empl_tns1_LMSCourse * LMSCourse;
@property (retain) empl_tns1_LMSCourseContent * LMSCourseContent1;
@property (retain) NSNumber * LMSCourseContent1_LMSCourseContentId;
@property (retain) NSNumber * LMSCourseContentId;
@property (retain) empl_tns1_LMSCourseContent * LMSCourseContents1;
@property (retain) NSNumber * LMSCourse_LMSCourseId;
@property (retain) NSString * Name;
@property (retain) NSString * Statement;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfLMSCourseContent : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseContent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfLMSCourseContent *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseContent:(empl_tns1_LMSCourseContent *)toAdd;
@property (readonly) NSMutableArray * LMSCourseContent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_LMSCourseDetail : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CourseContentTypeId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	empl_tns1_LMSCourse * LMSCourse;
	NSNumber * LMSCourseDetailId;
	NSNumber * LMSCourseId;
	NSNumber * Level;
	NSDate * ModifiedDate;
	NSString * Resource;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_LMSCourseDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CourseContentTypeId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) empl_tns1_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseDetailId;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSNumber * Level;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Resource;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfLMSCourseDetail : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfLMSCourseDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseDetail:(empl_tns1_LMSCourseDetail *)toAdd;
@property (readonly) NSMutableArray * LMSCourseDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfOrgUnit : NSObject {
	
/* elements */
	NSMutableArray *OrgUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfOrgUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgUnit:(empl_tns1_OrgUnit *)toAdd;
@property (readonly) NSMutableArray * OrgUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_OrgUnit : NSObject {
	
/* elements */
	NSString * Address;
	empl_tns1_Address * Address1;
	NSNumber * AddressId;
	NSString * CommissionDesc;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	NSString * FunctionDesc;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	empl_tns1_ArrayOfLMSCourseProgressUnit * LMSCourseProgressUnits;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
	empl_tns1_ArrayOfOrgUnit * OrgUnit1;
	empl_tns1_OrgUnit * OrgUnit2;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSNumber * OrgUnitLevelId;
	NSString * ParentCode;
	NSNumber * ParentId;
	NSNumber * TotalEmployees;
	NSString * WorkLocation;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_OrgUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) empl_tns1_Address * Address1;
@property (retain) NSNumber * AddressId;
@property (retain) NSString * CommissionDesc;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) NSString * FunctionDesc;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) empl_tns1_ArrayOfLMSCourseProgressUnit * LMSCourseProgressUnits;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) empl_tns1_ArrayOfOrgUnit * OrgUnit1;
@property (retain) empl_tns1_OrgUnit * OrgUnit2;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * OrgUnitLevelId;
@property (retain) NSString * ParentCode;
@property (retain) NSNumber * ParentId;
@property (retain) NSNumber * TotalEmployees;
@property (retain) NSString * WorkLocation;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_LMSCourseProgressUnit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	empl_tns1_LMSCourse * LMSCourse;
	NSNumber * LMSCourseId;
	NSNumber * LMSCourseProgressUnitId;
	NSDate * ModifiedDate;
	NSNumber * NumOfEmpFinish;
	empl_tns1_OrgUnit * OrgUnit;
	NSNumber * OrgUnitId;
	USBoolean * Status;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_LMSCourseProgressUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) empl_tns1_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSNumber * LMSCourseProgressUnitId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NumOfEmpFinish;
@property (retain) empl_tns1_OrgUnit * OrgUnit;
@property (retain) NSNumber * OrgUnitId;
@property (retain) USBoolean * Status;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfLMSCourseProgressUnit : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseProgressUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfLMSCourseProgressUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseProgressUnit:(empl_tns1_LMSCourseProgressUnit *)toAdd;
@property (readonly) NSMutableArray * LMSCourseProgressUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_LMSCourseTranscript : NSObject {
	
/* elements */
	empl_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSString * Comment;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	empl_tns1_Employee * Employee;
	empl_tns1_Employee * Employee1;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsPass;
	empl_tns1_LMSCourse * LMSCourse;
	NSNumber * LMSCourseId;
	NSNumber * LMSCourseTranscriptId;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_LMSCourseTranscript *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) empl_tns1_Employee * Employee1;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsPass;
@property (retain) empl_tns1_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSNumber * LMSCourseTranscriptId;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfLMSCourseTranscript : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseTranscript;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfLMSCourseTranscript *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseTranscript:(empl_tns1_LMSCourseTranscript *)toAdd;
@property (readonly) NSMutableArray * LMSCourseTranscript;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_LMSCourseType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * LMSCourseTypeId;
	NSString * LMSCourseTypeName;
	empl_tns1_ArrayOfLMSCourse * LMSCourses;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_LMSCourseType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * LMSCourseTypeId;
@property (retain) NSString * LMSCourseTypeName;
@property (retain) empl_tns1_ArrayOfLMSCourse * LMSCourses;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_LMSCourse : NSObject {
	
/* elements */
	NSNumber * ApprovedBy;
	empl_tns1_CLogCourseStatu * CLogCourseStatu;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSString * CourseIcon;
	NSNumber * CourseStatusId;
	NSNumber * CourseTypeId;
	NSDate * CreatedDate;
	NSDate * EndDate;
	empl_tns1_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
	empl_tns1_GPObjectiveInitiative * GPObjectiveInitiative;
	USBoolean * IsApproved;
	USBoolean * IsDeleted;
	USBoolean * IsRequired;
	NSString * Keyword;
	empl_tns1_LMSContentTopic * LMSContentTopic;
	empl_tns1_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
	NSString * LMSCourseCode;
	empl_tns1_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
	empl_tns1_ArrayOfLMSCourseContent * LMSCourseContents;
	empl_tns1_ArrayOfLMSCourseDetail * LMSCourseDetails;
	NSNumber * LMSCourseId;
	empl_tns1_ArrayOfLMSCourseProgressUnit * LMSCourseProgressUnits;
	empl_tns1_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
	empl_tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
	empl_tns1_LMSCourseType * LMSCourseType;
	NSNumber * LMSInitiativeObjectiveId;
	NSNumber * LMSTopicGroupId;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
	NSNumber * Requestedby;
	NSDate * StartDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_LMSCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApprovedBy;
@property (retain) empl_tns1_CLogCourseStatu * CLogCourseStatu;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSString * CourseIcon;
@property (retain) NSNumber * CourseStatusId;
@property (retain) NSNumber * CourseTypeId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) empl_tns1_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
@property (retain) empl_tns1_GPObjectiveInitiative * GPObjectiveInitiative;
@property (retain) USBoolean * IsApproved;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsRequired;
@property (retain) NSString * Keyword;
@property (retain) empl_tns1_LMSContentTopic * LMSContentTopic;
@property (retain) empl_tns1_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
@property (retain) NSString * LMSCourseCode;
@property (retain) empl_tns1_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
@property (retain) empl_tns1_ArrayOfLMSCourseContent * LMSCourseContents;
@property (retain) empl_tns1_ArrayOfLMSCourseDetail * LMSCourseDetails;
@property (retain) NSNumber * LMSCourseId;
@property (retain) empl_tns1_ArrayOfLMSCourseProgressUnit * LMSCourseProgressUnits;
@property (retain) empl_tns1_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
@property (retain) empl_tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
@property (retain) empl_tns1_LMSCourseType * LMSCourseType;
@property (retain) NSNumber * LMSInitiativeObjectiveId;
@property (retain) NSNumber * LMSTopicGroupId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) NSNumber * Requestedby;
@property (retain) NSDate * StartDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_LMSCourseAttendee : NSObject {
	
/* elements */
	empl_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsApproved;
	USBoolean * IsDeleted;
	USBoolean * IsPassed;
	empl_tns1_LMSCourse * LMSCourse;
	NSNumber * LMSCourseAttendeeId;
	NSNumber * LMSCourseId;
	NSNumber * LMSDevelopmentPlanId;
	NSDate * ModifiedDate;
	NSNumber * NumberOfWarning;
	NSNumber * OverallScore;
	NSNumber * ParticipationStatus;
	NSNumber * ProgressStatus;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_LMSCourseAttendee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsApproved;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsPassed;
@property (retain) empl_tns1_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseAttendeeId;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSNumber * LMSDevelopmentPlanId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NumberOfWarning;
@property (retain) NSNumber * OverallScore;
@property (retain) NSNumber * ParticipationStatus;
@property (retain) NSNumber * ProgressStatus;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfLMSCourseAttendee : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseAttendee;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfLMSCourseAttendee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseAttendee:(empl_tns1_LMSCourseAttendee *)toAdd;
@property (readonly) NSMutableArray * LMSCourseAttendee;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfOrgCoreCompetency : NSObject {
	
/* elements */
	NSMutableArray *OrgCoreCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfOrgCoreCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgCoreCompetency:(empl_tns1_OrgCoreCompetency *)toAdd;
@property (readonly) NSMutableArray * OrgCoreCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CLogRating : NSObject {
	
/* elements */
	NSNumber * CLogRatingId;
	NSString * Description;
	empl_tns1_ArrayOfEmpCompetency * EmpCompetencies;
	empl_tns1_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
	empl_tns1_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
	empl_tns1_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
	empl_tns1_ArrayOfEmpProfileSkill * EmpProfileSkills;
	empl_tns1_ArrayOfEmpProfileWorkingExperience * EmpProfileWorkingExperiences;
	USBoolean * IsDeleted;
	empl_tns1_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
	empl_tns1_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
	empl_tns1_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
	empl_tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
	empl_tns1_ArrayOfOrgCoreCompetency * OrgCoreCompetencies;
	empl_tns1_ArrayOfOrgProficencyDetail * OrgProficencyDetails;
	empl_tns1_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
	empl_tns1_ArrayOfOrgProficencyLevel * OrgProficencyLevels;
	empl_tns1_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
	empl_tns1_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
	empl_tns1_ArrayOfRecCandidateCompetencyRating * RecCandidateCompetencyRatings;
	empl_tns1_ArrayOfRecCandidateExperience * RecCandidateExperiences;
	empl_tns1_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
	empl_tns1_ArrayOfRecCandidateQualification * RecCandidateQualifications;
	empl_tns1_ArrayOfRecCandidateSkill * RecCandidateSkills;
	empl_tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
	empl_tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies1;
	empl_tns1_ArrayOfTrainingProficencyExpected * TrainingProficencyExpecteds;
	empl_tns1_ArrayOfTrainingProficencyRequire * TrainingProficencyRequires;
	NSNumber * Value;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CLogRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSString * Description;
@property (retain) empl_tns1_ArrayOfEmpCompetency * EmpCompetencies;
@property (retain) empl_tns1_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
@property (retain) empl_tns1_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
@property (retain) empl_tns1_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
@property (retain) empl_tns1_ArrayOfEmpProfileSkill * EmpProfileSkills;
@property (retain) empl_tns1_ArrayOfEmpProfileWorkingExperience * EmpProfileWorkingExperiences;
@property (retain) USBoolean * IsDeleted;
@property (retain) empl_tns1_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
@property (retain) empl_tns1_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
@property (retain) empl_tns1_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
@property (retain) empl_tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
@property (retain) empl_tns1_ArrayOfOrgCoreCompetency * OrgCoreCompetencies;
@property (retain) empl_tns1_ArrayOfOrgProficencyDetail * OrgProficencyDetails;
@property (retain) empl_tns1_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
@property (retain) empl_tns1_ArrayOfOrgProficencyLevel * OrgProficencyLevels;
@property (retain) empl_tns1_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
@property (retain) empl_tns1_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
@property (retain) empl_tns1_ArrayOfRecCandidateCompetencyRating * RecCandidateCompetencyRatings;
@property (retain) empl_tns1_ArrayOfRecCandidateExperience * RecCandidateExperiences;
@property (retain) empl_tns1_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
@property (retain) empl_tns1_ArrayOfRecCandidateQualification * RecCandidateQualifications;
@property (retain) empl_tns1_ArrayOfRecCandidateSkill * RecCandidateSkills;
@property (retain) empl_tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
@property (retain) empl_tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies1;
@property (retain) empl_tns1_ArrayOfTrainingProficencyExpected * TrainingProficencyExpecteds;
@property (retain) empl_tns1_ArrayOfTrainingProficencyRequire * TrainingProficencyRequires;
@property (retain) NSNumber * Value;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_OrgProficencyLevel : NSObject {
	
/* elements */
	empl_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompetencyLevelName;
	NSDate * CreatedDate;
	empl_tns1_ArrayOfEmpCompetency * EmpCompetencies;
	empl_tns1_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	empl_tns1_OrgCompetency * OrgCompetency;
	NSNumber * OrgCompetencyId;
	empl_tns1_ArrayOfOrgJobPositionSpecificCompetency * OrgJobPositionSpecificCompetencies;
	empl_tns1_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
	NSNumber * OrgProficencyLevelId;
	empl_tns1_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
	NSString * Statement;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_OrgProficencyLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompetencyLevelName;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_ArrayOfEmpCompetency * EmpCompetencies;
@property (retain) empl_tns1_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_OrgCompetency * OrgCompetency;
@property (retain) NSNumber * OrgCompetencyId;
@property (retain) empl_tns1_ArrayOfOrgJobPositionSpecificCompetency * OrgJobPositionSpecificCompetencies;
@property (retain) empl_tns1_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) empl_tns1_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
@property (retain) NSString * Statement;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_OrgJobPositionSpecificCompetency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	empl_tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSNumber * OrgJobPositionSpecificCompetencyId;
	empl_tns1_OrgProficencyLevel * OrgProficencyLevel;
	NSNumber * OrgProficencyLevelId;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_OrgJobPositionSpecificCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * OrgJobPositionSpecificCompetencyId;
@property (retain) empl_tns1_OrgProficencyLevel * OrgProficencyLevel;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfOrgJobPositionSpecificCompetency : NSObject {
	
/* elements */
	NSMutableArray *OrgJobPositionSpecificCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfOrgJobPositionSpecificCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgJobPositionSpecificCompetency:(empl_tns1_OrgJobPositionSpecificCompetency *)toAdd;
@property (readonly) NSMutableArray * OrgJobPositionSpecificCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_OrgJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	empl_tns1_ArrayOfEmpContract * EmpContracts;
	empl_tns1_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
	empl_tns1_OrgJob * OrgJob;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	empl_tns1_ArrayOfOrgJobPositionRequiredProficency * OrgJobPositionRequiredProficencies;
	empl_tns1_ArrayOfOrgJobPositionSpecificCompetency * OrgJobPositionSpecificCompetencies;
	NSString * OrgJobPositionTitle;
	NSString * OrgJobTitleCode;
	NSNumber * OrgJobTitleId;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgWorkLevelCode;
	NSNumber * OrgWorkLevelId;
	empl_tns1_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
	empl_tns1_ArrayOfRecPlanJobPosition * RecPlanJobPositions;
	empl_tns1_ArrayOfRecProcessApplyForJobPosition * RecProcessApplyForJobPositions;
	empl_tns1_ArrayOfRecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPositions;
	empl_tns1_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
	empl_tns1_ArrayOfRecRequirementJobPosition * RecRequirementJobPositions;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_OrgJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_ArrayOfEmpContract * EmpContracts;
@property (retain) empl_tns1_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) empl_tns1_OrgJob * OrgJob;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) empl_tns1_ArrayOfOrgJobPositionRequiredProficency * OrgJobPositionRequiredProficencies;
@property (retain) empl_tns1_ArrayOfOrgJobPositionSpecificCompetency * OrgJobPositionSpecificCompetencies;
@property (retain) NSString * OrgJobPositionTitle;
@property (retain) NSString * OrgJobTitleCode;
@property (retain) NSNumber * OrgJobTitleId;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) empl_tns1_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
@property (retain) empl_tns1_ArrayOfRecPlanJobPosition * RecPlanJobPositions;
@property (retain) empl_tns1_ArrayOfRecProcessApplyForJobPosition * RecProcessApplyForJobPositions;
@property (retain) empl_tns1_ArrayOfRecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPositions;
@property (retain) empl_tns1_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
@property (retain) empl_tns1_ArrayOfRecRequirementJobPosition * RecRequirementJobPositions;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfOrgJobPosition : NSObject {
	
/* elements */
	NSMutableArray *OrgJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfOrgJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgJobPosition:(empl_tns1_OrgJobPosition *)toAdd;
@property (readonly) NSMutableArray * OrgJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_OrgUnitJob : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	empl_tns1_OrgJob * OrgJob;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSNumber * OrgUnitJobId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_OrgUnitJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_OrgJob * OrgJob;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * OrgUnitJobId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfOrgUnitJob : NSObject {
	
/* elements */
	NSMutableArray *OrgUnitJob;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfOrgUnitJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgUnitJob:(empl_tns1_OrgUnitJob *)toAdd;
@property (readonly) NSMutableArray * OrgUnitJob;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_OrgJob : NSObject {
	
/* elements */
	NSString * CLogCareerCode;
	NSNumber * CLogCareerId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_ArrayOfEmpContract * EmpContracts;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	empl_tns1_ArrayOfOrgJobPosition * OrgJobPositions;
	empl_tns1_ArrayOfOrgJobSpecificCompetency * OrgJobSpecificCompetencies;
	empl_tns1_ArrayOfOrgUnitJob * OrgUnitJobs;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_OrgJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogCareerCode;
@property (retain) NSNumber * CLogCareerId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_ArrayOfEmpContract * EmpContracts;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) empl_tns1_ArrayOfOrgJobPosition * OrgJobPositions;
@property (retain) empl_tns1_ArrayOfOrgJobSpecificCompetency * OrgJobSpecificCompetencies;
@property (retain) empl_tns1_ArrayOfOrgUnitJob * OrgUnitJobs;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpContract : NSObject {
	
/* elements */
	empl_tns1_Address * Address;
	NSString * AllocationEquipmentDescription;
	NSString * Allowances;
	NSString * AttachFile;
	NSNumber * BaseWage;
	NSString * Bonuses;
	NSString * CLogContractTermCode;
	NSString * CLogContractTypeCode;
	NSNumber * CompanyId;
	NSString * CompensationDescription;
	NSString * ContractCode;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DateSigned;
	NSDate * EmpAppraiseDate;
	NSString * EmpAppraiseNote;
	NSNumber * EmpAppraiseRating;
	NSNumber * EmpContractId;
	empl_tns1_Employee * Employee;
	NSString * EmployeeRepresentativeAddress;
	NSNumber * EmployeeRepresentativeAddressId;
	NSDate * EmployeeRepresentativeBirthDay;
	NSString * EmployeeRepresentativeBirthPlace;
	NSString * EmployeeRepresentativeCode;
	NSString * EmployeeRepresentativeFullName;
	NSNumber * EmployeeRepresentativeId;
	NSString * EmployeeRepresentativeMajor;
	NSString * EmployeeRepresentativeNationality;
	empl_tns1_Employer * Employer;
	NSNumber * EmployerRepresentativeId;
	NSDate * EndDate;
	NSDate * ExpirationDate;
	NSDate * HRAppraiseDate;
	NSString * HRAppraiseNote;
	NSNumber * HRAppraiseRating;
	NSString * HolIdayPolicy;
	NSNumber * HoursOfOfficialWork;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	USBoolean * IsDeleted;
	USBoolean * IsEnd;
	USBoolean * IsProbation;
	USBoolean * IsSigned;
	NSString * JobResponsibilitiesDescription;
	NSString * LaborSupportEquipments;
	NSDate * LeaderAppraiseDate;
	NSString * LeaderAppraiseNote;
	NSNumber * LeaderAppraiseRating;
	NSString * MedicalInsurance;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSDate * OfficialWorkEndTime;
	NSDate * OfficialWorkStartTime;
	empl_tns1_OrgJob * OrgJob;
	NSNumber * OrgJobId;
	empl_tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSNumber * OrgUnitId;
	NSString * OrganizationAddress;
	NSNumber * OrganizationAddressId;
	NSString * OrganizationName;
	NSString * OrganizationPhone;
	NSString * OtherPolicy;
	NSString * PayDay;
	NSString * PayingMethod;
	NSString * PlaceSigned;
	NSDate * ProbationaryEndDate;
	NSDate * ProbationaryStartDate;
	NSNumber * ProbationaryTime;
	NSString * ProvidedEquipment;
	NSNumber * RelationEmpContractId;
	NSString * SalaryIncreasePolicy;
	NSString * SocialInsurance;
	NSDate * StartDate;
	NSString * TrainingPolicy;
	NSString * Transportation;
	NSString * WorkingAddress;
	NSNumber * WorkingAddressId;
	NSNumber * WorkingDay;
	NSString * WorkingTime;
	NSString * WorkingTimePolicy;
	NSString * cAllowance;
	NSString * cBaseSalary;
	NSString * cSocialInsuranceSalary;
	NSNumber * nAllowance;
	NSNumber * nSocialInsuranceSalary;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_Address * Address;
@property (retain) NSString * AllocationEquipmentDescription;
@property (retain) NSString * Allowances;
@property (retain) NSString * AttachFile;
@property (retain) NSNumber * BaseWage;
@property (retain) NSString * Bonuses;
@property (retain) NSString * CLogContractTermCode;
@property (retain) NSString * CLogContractTypeCode;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompensationDescription;
@property (retain) NSString * ContractCode;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateSigned;
@property (retain) NSDate * EmpAppraiseDate;
@property (retain) NSString * EmpAppraiseNote;
@property (retain) NSNumber * EmpAppraiseRating;
@property (retain) NSNumber * EmpContractId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeRepresentativeAddress;
@property (retain) NSNumber * EmployeeRepresentativeAddressId;
@property (retain) NSDate * EmployeeRepresentativeBirthDay;
@property (retain) NSString * EmployeeRepresentativeBirthPlace;
@property (retain) NSString * EmployeeRepresentativeCode;
@property (retain) NSString * EmployeeRepresentativeFullName;
@property (retain) NSNumber * EmployeeRepresentativeId;
@property (retain) NSString * EmployeeRepresentativeMajor;
@property (retain) NSString * EmployeeRepresentativeNationality;
@property (retain) empl_tns1_Employer * Employer;
@property (retain) NSNumber * EmployerRepresentativeId;
@property (retain) NSDate * EndDate;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSDate * HRAppraiseDate;
@property (retain) NSString * HRAppraiseNote;
@property (retain) NSNumber * HRAppraiseRating;
@property (retain) NSString * HolIdayPolicy;
@property (retain) NSNumber * HoursOfOfficialWork;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEnd;
@property (retain) USBoolean * IsProbation;
@property (retain) USBoolean * IsSigned;
@property (retain) NSString * JobResponsibilitiesDescription;
@property (retain) NSString * LaborSupportEquipments;
@property (retain) NSDate * LeaderAppraiseDate;
@property (retain) NSString * LeaderAppraiseNote;
@property (retain) NSNumber * LeaderAppraiseRating;
@property (retain) NSString * MedicalInsurance;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * OfficialWorkEndTime;
@property (retain) NSDate * OfficialWorkStartTime;
@property (retain) empl_tns1_OrgJob * OrgJob;
@property (retain) NSNumber * OrgJobId;
@property (retain) empl_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrganizationAddress;
@property (retain) NSNumber * OrganizationAddressId;
@property (retain) NSString * OrganizationName;
@property (retain) NSString * OrganizationPhone;
@property (retain) NSString * OtherPolicy;
@property (retain) NSString * PayDay;
@property (retain) NSString * PayingMethod;
@property (retain) NSString * PlaceSigned;
@property (retain) NSDate * ProbationaryEndDate;
@property (retain) NSDate * ProbationaryStartDate;
@property (retain) NSNumber * ProbationaryTime;
@property (retain) NSString * ProvidedEquipment;
@property (retain) NSNumber * RelationEmpContractId;
@property (retain) NSString * SalaryIncreasePolicy;
@property (retain) NSString * SocialInsurance;
@property (retain) NSDate * StartDate;
@property (retain) NSString * TrainingPolicy;
@property (retain) NSString * Transportation;
@property (retain) NSString * WorkingAddress;
@property (retain) NSNumber * WorkingAddressId;
@property (retain) NSNumber * WorkingDay;
@property (retain) NSString * WorkingTime;
@property (retain) NSString * WorkingTimePolicy;
@property (retain) NSString * cAllowance;
@property (retain) NSString * cBaseSalary;
@property (retain) NSString * cSocialInsuranceSalary;
@property (retain) NSNumber * nAllowance;
@property (retain) NSNumber * nSocialInsuranceSalary;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpContract : NSObject {
	
/* elements */
	NSMutableArray *EmpContract;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpContract:(empl_tns1_EmpContract *)toAdd;
@property (readonly) NSMutableArray * EmpContract;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_Address : NSObject {
	
/* elements */
	NSNumber * AddressId;
	NSString * AddressNumber;
	NSNumber * CLogCityId;
	NSNumber * CLogCountryId;
	NSNumber * CLogDistrictId;
	empl_tns1_ArrayOfCLogTrainer * CLogTrainers;
	empl_tns1_ArrayOfEmpContract * EmpContracts;
	NSString * FullAddress;
	empl_tns1_ArrayOfOrgUnit * OrgUnits;
	NSString * PostalCode;
	NSString * Street;
	NSString * Ward;
	NSString * ZipCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_Address *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AddressId;
@property (retain) NSString * AddressNumber;
@property (retain) NSNumber * CLogCityId;
@property (retain) NSNumber * CLogCountryId;
@property (retain) NSNumber * CLogDistrictId;
@property (retain) empl_tns1_ArrayOfCLogTrainer * CLogTrainers;
@property (retain) empl_tns1_ArrayOfEmpContract * EmpContracts;
@property (retain) NSString * FullAddress;
@property (retain) empl_tns1_ArrayOfOrgUnit * OrgUnits;
@property (retain) NSString * PostalCode;
@property (retain) NSString * Street;
@property (retain) NSString * Ward;
@property (retain) NSString * ZipCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CLogTrainer : NSObject {
	
/* elements */
	NSString * Address;
	empl_tns1_Address * Address1;
	NSNumber * AddressId;
	NSNumber * CLogTrainerId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * EducationUnit;
	NSString * Email;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSString * FirstName;
	NSString * FullName;
	USBoolean * IsDeleted;
	USBoolean * IsEmployee;
	NSString * LastName;
	NSDate * ModifiedDate;
	NSString * PhoneNumber;
	empl_tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CLogTrainer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) empl_tns1_Address * Address1;
@property (retain) NSNumber * AddressId;
@property (retain) NSNumber * CLogTrainerId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * EducationUnit;
@property (retain) NSString * Email;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEmployee;
@property (retain) NSString * LastName;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * PhoneNumber;
@property (retain) empl_tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfCLogTrainer : NSObject {
	
/* elements */
	NSMutableArray *CLogTrainer;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfCLogTrainer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogTrainer:(empl_tns1_CLogTrainer *)toAdd;
@property (readonly) NSMutableArray * CLogTrainer;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CLogEmpWorkingStatu : NSObject {
	
/* elements */
	NSNumber * CLogEmpWorkingStatusId;
	NSNumber * CompanyId;
	empl_tns1_ArrayOfEmpBasicProfile * EmpBasicProfiles;
	NSString * EmpWorkingStatusCode;
	NSString * EmpWorkingStatusName;
	NSString * EmpWorkingStatusNameEN;
	USBoolean * IsDeleted;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CLogEmpWorkingStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogEmpWorkingStatusId;
@property (retain) NSNumber * CompanyId;
@property (retain) empl_tns1_ArrayOfEmpBasicProfile * EmpBasicProfiles;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmpWorkingStatusNameEN;
@property (retain) USBoolean * IsDeleted;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpBasicProfile : NSObject {
	
/* elements */
	NSString * BankAccountNo;
	NSString * BankBranch;
	NSString * BankName;
	NSNumber * CLogBankBranchId;
	empl_tns1_CLogEmpWorkingStatu * CLogEmpWorkingStatu;
	NSString * CVUrl;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * CulturalLevel;
	NSNumber * CulturalLevelId;
	NSString * EducationLevel;
	NSNumber * EducationLevelId;
	NSNumber * EmpBasicProfileId;
	NSNumber * EmpWorkingStatusId;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardPlaceOfIssue;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	NSString * Major;
	NSNumber * MajorId;
	NSString * MaritalStatus;
	NSNumber * MaritalStatusId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSString * PITCode;
	NSString * PITIssuedBy;
	NSDate * PassportDateOfExpire;
	NSDate * PassportDateOfIssue;
	NSString * PassportNo;
	NSString * PassportPlaceOfIssue;
	NSNumber * ProbationarySalary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpBasicProfile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * BankAccountNo;
@property (retain) NSString * BankBranch;
@property (retain) NSString * BankName;
@property (retain) NSNumber * CLogBankBranchId;
@property (retain) empl_tns1_CLogEmpWorkingStatu * CLogEmpWorkingStatu;
@property (retain) NSString * CVUrl;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * CulturalLevel;
@property (retain) NSNumber * CulturalLevelId;
@property (retain) NSString * EducationLevel;
@property (retain) NSNumber * EducationLevelId;
@property (retain) NSNumber * EmpBasicProfileId;
@property (retain) NSNumber * EmpWorkingStatusId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Major;
@property (retain) NSNumber * MajorId;
@property (retain) NSString * MaritalStatus;
@property (retain) NSNumber * MaritalStatusId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSString * PITCode;
@property (retain) NSString * PITIssuedBy;
@property (retain) NSDate * PassportDateOfExpire;
@property (retain) NSDate * PassportDateOfIssue;
@property (retain) NSString * PassportNo;
@property (retain) NSString * PassportPlaceOfIssue;
@property (retain) NSNumber * ProbationarySalary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpBasicProfile : NSObject {
	
/* elements */
	NSMutableArray *EmpBasicProfile;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpBasicProfile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpBasicProfile:(empl_tns1_EmpBasicProfile *)toAdd;
@property (readonly) NSMutableArray * EmpBasicProfile;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpCompetencyRating : NSObject {
	
/* elements */
	NSMutableArray *EmpCompetencyRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpCompetencyRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpCompetencyRating:(empl_tns1_EmpCompetencyRating *)toAdd;
@property (readonly) NSMutableArray * EmpCompetencyRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpPerformanceAppraisal : NSObject {
	
/* elements */
	NSDate * AppraisalDate;
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * Comment;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpPerformanceAppraisalId;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsEndYear;
	USBoolean * IsHalfYear;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * RelateRowId;
	NSNumber * Score;
	NSString * SelfComment;
	NSNumber * SelfRating;
	NSNumber * SelfScore;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpPerformanceAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AppraisalDate;
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpPerformanceAppraisalId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEndYear;
@property (retain) USBoolean * IsHalfYear;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * SelfScore;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpPerformanceAppraisal : NSObject {
	
/* elements */
	NSMutableArray *EmpPerformanceAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpPerformanceAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpPerformanceAppraisal:(empl_tns1_EmpPerformanceAppraisal *)toAdd;
@property (readonly) NSMutableArray * EmpPerformanceAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileAllowance : NSObject {
	
/* elements */
	NSDate * ActivedDate;
	NSString * AllowanceCode;
	NSString * AllowanceName;
	NSNumber * AllowanceValue;
	NSNumber * CLogCurrency;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpProfileAllowanceId;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsActived;
	USBoolean * IsDeactived;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileAllowance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ActivedDate;
@property (retain) NSString * AllowanceCode;
@property (retain) NSString * AllowanceName;
@property (retain) NSNumber * AllowanceValue;
@property (retain) NSNumber * CLogCurrency;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileAllowanceId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeactived;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileAllowance : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileAllowance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileAllowance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileAllowance:(empl_tns1_EmpProfileAllowance *)toAdd;
@property (readonly) NSMutableArray * EmpProfileAllowance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileBaseSalary : NSObject {
	
/* elements */
	NSDate * ActivedDate;
	NSString * BasicPayCode;
	NSNumber * CBSalaryCofficientId;
	NSNumber * CBSalaryGradeId;
	NSNumber * CBSalaryLevelId;
	NSNumber * CBSalaryScaleId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpProfileBaseSalaryId;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsActived;
	USBoolean * IsDeactived;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * NonPayScaleSalaryValue;
	NSString * Note;
	NSNumber * PayScaleSalaryValue;
	NSString * SalaryCode;
	NSNumber * SalaryId;
	NSString * SalaryName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileBaseSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ActivedDate;
@property (retain) NSString * BasicPayCode;
@property (retain) NSNumber * CBSalaryCofficientId;
@property (retain) NSNumber * CBSalaryGradeId;
@property (retain) NSNumber * CBSalaryLevelId;
@property (retain) NSNumber * CBSalaryScaleId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileBaseSalaryId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeactived;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NonPayScaleSalaryValue;
@property (retain) NSString * Note;
@property (retain) NSNumber * PayScaleSalaryValue;
@property (retain) NSString * SalaryCode;
@property (retain) NSNumber * SalaryId;
@property (retain) NSString * SalaryName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileBaseSalary : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileBaseSalary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileBaseSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileBaseSalary:(empl_tns1_EmpProfileBaseSalary *)toAdd;
@property (readonly) NSMutableArray * EmpProfileBaseSalary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileBenefit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileBenefitId;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * HealthInsuranceDateExpire;
	NSDate * HealthInsuranceDateIssue;
	NSNumber * HealthInsuranceId;
	NSString * HealthInsuranceNumber;
	NSString * HealthInsuranceRegPlace;
	USBoolean * IsDeleted;
	NSDate * LabourBookDateOfIssue;
	NSString * LabourBookNo;
	NSString * LabourBookPlaceOfIssue;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * OtherInsuranceCompanyName;
	NSDate * OtherInsuranceDateExpire;
	NSDate * OtherInsuranceDateIssue;
	NSString * OtherInsuranceNumber;
	NSDate * SocialInsuranceDateIssue;
	NSNumber * SocialInsuranceId;
	NSNumber * SocialInsuranceJoiningBefore;
	NSString * SocialInsuranceNumber;
	NSDate * UnemploymentInsuranceDateIssue;
	NSNumber * UnemploymentInsuranceId;
	NSString * UnemploymentInsuranceNumber;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileBenefitId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * HealthInsuranceDateExpire;
@property (retain) NSDate * HealthInsuranceDateIssue;
@property (retain) NSNumber * HealthInsuranceId;
@property (retain) NSString * HealthInsuranceNumber;
@property (retain) NSString * HealthInsuranceRegPlace;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * LabourBookDateOfIssue;
@property (retain) NSString * LabourBookNo;
@property (retain) NSString * LabourBookPlaceOfIssue;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OtherInsuranceCompanyName;
@property (retain) NSDate * OtherInsuranceDateExpire;
@property (retain) NSDate * OtherInsuranceDateIssue;
@property (retain) NSString * OtherInsuranceNumber;
@property (retain) NSDate * SocialInsuranceDateIssue;
@property (retain) NSNumber * SocialInsuranceId;
@property (retain) NSNumber * SocialInsuranceJoiningBefore;
@property (retain) NSString * SocialInsuranceNumber;
@property (retain) NSDate * UnemploymentInsuranceDateIssue;
@property (retain) NSNumber * UnemploymentInsuranceId;
@property (retain) NSString * UnemploymentInsuranceNumber;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileBenefit : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileBenefit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileBenefit:(empl_tns1_EmpProfileBenefit *)toAdd;
@property (readonly) NSMutableArray * EmpProfileBenefit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileComment : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSString * Content;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfileCommentId;
	empl_tns1_Employee * Employee;
	empl_tns1_Employee * Employee1;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * RelateRowId;
	NSString * ReviewerCode;
	NSNumber * ReviewerId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileComment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Content;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfileCommentId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) empl_tns1_Employee * Employee1;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * ReviewerCode;
@property (retain) NSNumber * ReviewerId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileComment : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileComment;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileComment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileComment:(empl_tns1_EmpProfileComment *)toAdd;
@property (readonly) NSMutableArray * EmpProfileComment;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileContact : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSString * ContactAddress;
	NSNumber * ContactAddressId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * Email;
	NSNumber * EmpProfileContactId;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSString * Fax;
	NSString * HomePhone;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	NSString * MobileNumber;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * OfficePhone;
	NSString * PermanentAddress;
	NSNumber * PermanentAddressId;
	NSNumber * RelateRowId;
	NSString * TemporaryAddress;
	NSNumber * TemporaryAddressId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileContact *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ContactAddress;
@property (retain) NSNumber * ContactAddressId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Email;
@property (retain) NSNumber * EmpProfileContactId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * Fax;
@property (retain) NSString * HomePhone;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * MobileNumber;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OfficePhone;
@property (retain) NSString * PermanentAddress;
@property (retain) NSNumber * PermanentAddressId;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * TemporaryAddress;
@property (retain) NSNumber * TemporaryAddressId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileContact : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileContact;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileContact *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileContact:(empl_tns1_EmpProfileContact *)toAdd;
@property (readonly) NSMutableArray * EmpProfileContact;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_OrgDegree : NSObject {
	
/* elements */
	NSNumber * CLogMajorId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * Description;
	empl_tns1_ArrayOfEmpProfileDegree * EmpProfileDegrees;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * OrgDegreeId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_OrgDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) empl_tns1_ArrayOfEmpProfileDegree * EmpProfileDegrees;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * OrgDegreeId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileDegree : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	NSNumber * EmpProfileDegreeId;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsValId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	empl_tns1_OrgDegree * OrgDegree;
	NSNumber * OrgDegreeId;
	NSNumber * OrgDegreeRankId;
	NSString * Other;
	NSString * PlaceIssue;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) NSNumber * EmpProfileDegreeId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_OrgDegree * OrgDegree;
@property (retain) NSNumber * OrgDegreeId;
@property (retain) NSNumber * OrgDegreeRankId;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileDegree : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileDegree;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileDegree:(empl_tns1_EmpProfileDegree *)toAdd;
@property (readonly) NSMutableArray * EmpProfileDegree;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_OrgDisciplineForUnit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgDisciplineForUnitId;
	NSNumber * OrgDisciplineId;
	empl_tns1_OrgDisciplineMaster * OrgDisciplineMaster;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_OrgDisciplineForUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgDisciplineForUnitId;
@property (retain) NSNumber * OrgDisciplineId;
@property (retain) empl_tns1_OrgDisciplineMaster * OrgDisciplineMaster;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfOrgDisciplineForUnit : NSObject {
	
/* elements */
	NSMutableArray *OrgDisciplineForUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfOrgDisciplineForUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgDisciplineForUnit:(empl_tns1_OrgDisciplineForUnit *)toAdd;
@property (readonly) NSMutableArray * OrgDisciplineForUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_OrgDisciplineMaster : NSObject {
	
/* elements */
	NSNumber * ApproveId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSDate * DateOfIssue;
	NSString * DecisionNo;
	empl_tns1_ArrayOfEmpProfileDiscipline * EmpProfileDisciplines;
	USBoolean * IsDeleted;
	USBoolean * IsForEmployee;
	NSDate * ModifiedDate;
	NSNumber * MonetaryValue;
	empl_tns1_ArrayOfOrgDisciplineForUnit * OrgDisciplineForUnits;
	NSNumber * OrgDisciplineMasterId;
	NSString * OrgDisciplineTypeCode;
	USBoolean * Status;
	NSString * Title;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_OrgDisciplineMaster *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproveId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSDate * DateOfIssue;
@property (retain) NSString * DecisionNo;
@property (retain) empl_tns1_ArrayOfEmpProfileDiscipline * EmpProfileDisciplines;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsForEmployee;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * MonetaryValue;
@property (retain) empl_tns1_ArrayOfOrgDisciplineForUnit * OrgDisciplineForUnits;
@property (retain) NSNumber * OrgDisciplineMasterId;
@property (retain) NSString * OrgDisciplineTypeCode;
@property (retain) USBoolean * Status;
@property (retain) NSString * Title;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileDiscipline : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfileDisciplineId;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	empl_tns1_OrgDisciplineMaster * OrgDisciplineMaster;
	NSNumber * OrgDisciplineMasterId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileDiscipline *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfileDisciplineId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_OrgDisciplineMaster * OrgDisciplineMaster;
@property (retain) NSNumber * OrgDisciplineMasterId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileDiscipline : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileDiscipline;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileDiscipline *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileDiscipline:(empl_tns1_EmpProfileDiscipline *)toAdd;
@property (readonly) NSMutableArray * EmpProfileDiscipline;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CLogEquipment : NSObject {
	
/* elements */
	NSDate * AddDate;
	NSString * CLogAssetsTypeCode;
	NSNumber * CLogCurrencyId;
	NSNumber * CLogEquipmentId;
	NSString * CLogEquipmentTypeCode;
	NSString * Code;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * Depreciation;
	empl_tns1_ArrayOfEmpProfileEquipment * EmpProfileEquipments;
	USBoolean * IsBigValue;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
	NSNumber * Number;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CLogEquipment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AddDate;
@property (retain) NSString * CLogAssetsTypeCode;
@property (retain) NSNumber * CLogCurrencyId;
@property (retain) NSNumber * CLogEquipmentId;
@property (retain) NSString * CLogEquipmentTypeCode;
@property (retain) NSString * Code;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * Depreciation;
@property (retain) empl_tns1_ArrayOfEmpProfileEquipment * EmpProfileEquipments;
@property (retain) USBoolean * IsBigValue;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) NSNumber * Number;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileEquipment : NSObject {
	
/* elements */
	empl_tns1_CLogEquipment * CLogEquipment;
	NSNumber * CLogEquipmentId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DateHandover;
	NSDate * DateReceived;
	NSString * Description;
	NSNumber * EmpProfileEquipmentId;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * HandoverStatus;
	USBoolean * IsDeleted;
	USBoolean * IsHandover;
	USBoolean * IsReturnToComp;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	USBoolean * MustHandover;
	NSNumber * Number;
	NSString * ReceiverCode;
	NSNumber * ReceiverId;
	NSNumber * RequestDetailId;
	NSString * StateHandover;
	NSString * StateReceived;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileEquipment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_CLogEquipment * CLogEquipment;
@property (retain) NSNumber * CLogEquipmentId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateHandover;
@property (retain) NSDate * DateReceived;
@property (retain) NSString * Description;
@property (retain) NSNumber * EmpProfileEquipmentId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * HandoverStatus;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsHandover;
@property (retain) USBoolean * IsReturnToComp;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) USBoolean * MustHandover;
@property (retain) NSNumber * Number;
@property (retain) NSString * ReceiverCode;
@property (retain) NSNumber * ReceiverId;
@property (retain) NSNumber * RequestDetailId;
@property (retain) NSString * StateHandover;
@property (retain) NSString * StateReceived;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileEquipment : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileEquipment;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileEquipment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileEquipment:(empl_tns1_EmpProfileEquipment *)toAdd;
@property (readonly) NSMutableArray * EmpProfileEquipment;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileExperience : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * AttachedDocumentUrl;
	NSString * Company;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * Customer;
	NSNumber * EmpProfileExperienceId;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * ProjectName;
	NSString * ProjectResult;
	NSNumber * RelateRowId;
	NSString * Roles;
	NSDate * StartDate;
	NSString * TeamSize;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * AttachedDocumentUrl;
@property (retain) NSString * Company;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Customer;
@property (retain) NSNumber * EmpProfileExperienceId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * ProjectName;
@property (retain) NSString * ProjectResult;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * Roles;
@property (retain) NSDate * StartDate;
@property (retain) NSString * TeamSize;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileExperience : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileExperience:(empl_tns1_EmpProfileExperience *)toAdd;
@property (readonly) NSMutableArray * EmpProfileExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CLogLanguage : NSObject {
	
/* elements */
	NSNumber * CLogLanguageId;
	empl_tns1_ArrayOfEmpProfileForeignLanguage * EmpProfileForeignLanguages;
	USBoolean * IsDeleted;
	NSString * Language;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CLogLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogLanguageId;
@property (retain) empl_tns1_ArrayOfEmpProfileForeignLanguage * EmpProfileForeignLanguages;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Language;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileForeignLanguage : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * AttachmenttUrl;
	empl_tns1_CLogLanguage * CLogLanguage;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DateOfIssue;
	NSString * DegreeName;
	NSNumber * EffectiveTime;
	NSNumber * EmpProfileForeignLanguageId;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * LanguageId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * OrgDegreeRankId;
	NSString * PlaceOfIssue;
	NSNumber * RelateRowId;
	NSString * TrainingCenter;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileForeignLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * AttachmenttUrl;
@property (retain) empl_tns1_CLogLanguage * CLogLanguage;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateOfIssue;
@property (retain) NSString * DegreeName;
@property (retain) NSNumber * EffectiveTime;
@property (retain) NSNumber * EmpProfileForeignLanguageId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * LanguageId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * OrgDegreeRankId;
@property (retain) NSString * PlaceOfIssue;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * TrainingCenter;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileForeignLanguage : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileForeignLanguage;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileForeignLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileForeignLanguage:(empl_tns1_EmpProfileForeignLanguage *)toAdd;
@property (readonly) NSMutableArray * EmpProfileForeignLanguage;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileHealthInsurance : NSObject {
	
/* elements */
	NSNumber * CLogHospitalId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * HICompanyRate;
	NSNumber * HIEmployeeRate;
	NSDate * HealthInsuranceDateEffect;
	NSDate * HealthInsuranceDateExpire;
	NSNumber * HealthInsuranceId;
	NSString * HealthInsuranceNumber;
	NSDate * ImplementDate;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	USBoolean * IsDifference;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileHealthInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogHospitalId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * HICompanyRate;
@property (retain) NSNumber * HIEmployeeRate;
@property (retain) NSDate * HealthInsuranceDateEffect;
@property (retain) NSDate * HealthInsuranceDateExpire;
@property (retain) NSNumber * HealthInsuranceId;
@property (retain) NSString * HealthInsuranceNumber;
@property (retain) NSDate * ImplementDate;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDifference;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileHealthInsurance : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileHealthInsurance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileHealthInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileHealthInsurance:(empl_tns1_EmpProfileHealthInsurance *)toAdd;
@property (readonly) NSMutableArray * EmpProfileHealthInsurance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileHealthy : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * BloodGroup;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DiseaseAbsenceEndDay;
	NSDate * DiseaseAbsenceStartDay;
	NSNumber * DiseaseBenefit;
	NSString * DiseaseName;
	NSNumber * EmpProfileHealthyId;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	USBoolean * IsDiseaseAbsence;
	USBoolean * IsEnoughHealthToWork;
	USBoolean * IsGoodHealth;
	USBoolean * IsMaternityAbsence;
	USBoolean * IsMedicalHistory;
	USBoolean * IsPeopleWithDisability;
	NSDate * MaternityAbsenceEndDay;
	NSDate * MaternityAbsenceStartDay;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileHealthy *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * BloodGroup;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DiseaseAbsenceEndDay;
@property (retain) NSDate * DiseaseAbsenceStartDay;
@property (retain) NSNumber * DiseaseBenefit;
@property (retain) NSString * DiseaseName;
@property (retain) NSNumber * EmpProfileHealthyId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDiseaseAbsence;
@property (retain) USBoolean * IsEnoughHealthToWork;
@property (retain) USBoolean * IsGoodHealth;
@property (retain) USBoolean * IsMaternityAbsence;
@property (retain) USBoolean * IsMedicalHistory;
@property (retain) USBoolean * IsPeopleWithDisability;
@property (retain) NSDate * MaternityAbsenceEndDay;
@property (retain) NSDate * MaternityAbsenceStartDay;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileHealthy : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileHealthy;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileHealthy *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileHealthy:(empl_tns1_EmpProfileHealthy *)toAdd;
@property (readonly) NSMutableArray * EmpProfileHealthy;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CLogHobby : NSObject {
	
/* elements */
	NSNumber * CLogHobbyId;
	NSString * Description;
	empl_tns1_ArrayOfEmpProfileHobby * EmpProfileHobbies;
	USBoolean * IsDeleted;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CLogHobby *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogHobbyId;
@property (retain) NSString * Description;
@property (retain) empl_tns1_ArrayOfEmpProfileHobby * EmpProfileHobbies;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileHobby : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	empl_tns1_CLogHobby * CLogHobby;
	NSNumber * CLogHobbyId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfileHobbyId;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileHobby *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) empl_tns1_CLogHobby * CLogHobby;
@property (retain) NSNumber * CLogHobbyId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfileHobbyId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileHobby : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileHobby;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileHobby *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileHobby:(empl_tns1_EmpProfileHobby *)toAdd;
@property (readonly) NSMutableArray * EmpProfileHobby;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileInternalCourse : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpInternalCourseId;
	NSNumber * EmpProfileInternalCourseId;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Result;
	NSNumber * Status;
	NSString * Support;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileInternalCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpInternalCourseId;
@property (retain) NSNumber * EmpProfileInternalCourseId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Result;
@property (retain) NSNumber * Status;
@property (retain) NSString * Support;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileInternalCourse : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileInternalCourse;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileInternalCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileInternalCourse:(empl_tns1_EmpProfileInternalCourse *)toAdd;
@property (readonly) NSMutableArray * EmpProfileInternalCourse;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileLeaveRegime : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CompensatoryLeave;
	NSNumber * CompensatoryLeaveOfYear;
	NSNumber * CompensatoryRemainedLeave;
	NSNumber * CompensatoryUsedLeave;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileLeaveRegimeId;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * MonthlyLeave;
	NSNumber * PlusMinusLeave;
	NSNumber * PreviousLeave;
	NSNumber * RemainedLeave;
	NSNumber * SeniorityLeave;
	NSNumber * TotalUsedLeave;
	NSNumber * Year;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileLeaveRegime *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CompensatoryLeave;
@property (retain) NSNumber * CompensatoryLeaveOfYear;
@property (retain) NSNumber * CompensatoryRemainedLeave;
@property (retain) NSNumber * CompensatoryUsedLeave;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileLeaveRegimeId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * MonthlyLeave;
@property (retain) NSNumber * PlusMinusLeave;
@property (retain) NSNumber * PreviousLeave;
@property (retain) NSNumber * RemainedLeave;
@property (retain) NSNumber * SeniorityLeave;
@property (retain) NSNumber * TotalUsedLeave;
@property (retain) NSNumber * Year;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileLeaveRegime : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileLeaveRegime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileLeaveRegime *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileLeaveRegime:(empl_tns1_EmpProfileLeaveRegime *)toAdd;
@property (readonly) NSMutableArray * EmpProfileLeaveRegime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpOtherBenefit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	NSNumber * EmpOtherBenefitId;
	empl_tns1_ArrayOfEmpProfileOtherBenefit * EmpProfileOtherBenefits;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpOtherBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) NSNumber * EmpOtherBenefitId;
@property (retain) empl_tns1_ArrayOfEmpProfileOtherBenefit * EmpProfileOtherBenefits;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileOtherBenefit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	empl_tns1_EmpOtherBenefit * EmpOtherBenefit;
	NSNumber * EmpOtherBenefitId;
	NSNumber * EmpProfileOtherBenefitId;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileOtherBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_EmpOtherBenefit * EmpOtherBenefit;
@property (retain) NSNumber * EmpOtherBenefitId;
@property (retain) NSNumber * EmpProfileOtherBenefitId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileOtherBenefit : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileOtherBenefit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileOtherBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileOtherBenefit:(empl_tns1_EmpProfileOtherBenefit *)toAdd;
@property (readonly) NSMutableArray * EmpProfileOtherBenefit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileParticipation : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileParticipationId;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSDate * PartyFeeEndDate;
	NSNumber * PartyFeePercent;
	NSString * PartyFeeRole;
	NSDate * PartyFeeStartDate;
	NSNumber * RelateRowId;
	NSDate * UnionFeeEndDate;
	NSNumber * UnionFeePercent;
	NSString * UnionFeeRole;
	NSDate * UnionFeeStartDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileParticipation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileParticipationId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * PartyFeeEndDate;
@property (retain) NSNumber * PartyFeePercent;
@property (retain) NSString * PartyFeeRole;
@property (retain) NSDate * PartyFeeStartDate;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSDate * UnionFeeEndDate;
@property (retain) NSNumber * UnionFeePercent;
@property (retain) NSString * UnionFeeRole;
@property (retain) NSDate * UnionFeeStartDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileParticipation : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileParticipation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileParticipation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileParticipation:(empl_tns1_EmpProfileParticipation *)toAdd;
@property (readonly) NSMutableArray * EmpProfileParticipation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CLogPersonality : NSObject {
	
/* elements */
	NSNumber * CLogPersonalityId;
	NSString * Description;
	empl_tns1_ArrayOfEmpProfilePersonality * EmpProfilePersonalities;
	USBoolean * IsDeleted;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CLogPersonality *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogPersonalityId;
@property (retain) NSString * Description;
@property (retain) empl_tns1_ArrayOfEmpProfilePersonality * EmpProfilePersonalities;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfilePersonality : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	empl_tns1_CLogPersonality * CLogPersonality;
	NSNumber * CLogPersonalityId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfilePersonalityId;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfilePersonality *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) empl_tns1_CLogPersonality * CLogPersonality;
@property (retain) NSNumber * CLogPersonalityId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfilePersonalityId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfilePersonality : NSObject {
	
/* elements */
	NSMutableArray *EmpProfilePersonality;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfilePersonality *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfilePersonality:(empl_tns1_EmpProfilePersonality *)toAdd;
@property (readonly) NSMutableArray * EmpProfilePersonality;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileProcessOfWork : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * DecisionFile;
	NSString * DecisionName;
	NSString * DecisionNo;
	NSNumber * EmpProfileJobPositionId;
	NSNumber * EmpProfileProcessOfWorkId;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ExpirationDate;
	NSDate * ImplementationDate;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * OrgUnitAddress;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgUnitName;
	NSString * OrgUnitNameEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileProcessOfWork *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DecisionFile;
@property (retain) NSString * DecisionName;
@property (retain) NSString * DecisionNo;
@property (retain) NSNumber * EmpProfileJobPositionId;
@property (retain) NSNumber * EmpProfileProcessOfWorkId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OrgUnitAddress;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitName;
@property (retain) NSString * OrgUnitNameEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileProcessOfWork : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileProcessOfWork;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileProcessOfWork *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileProcessOfWork:(empl_tns1_EmpProfileProcessOfWork *)toAdd;
@property (readonly) NSMutableArray * EmpProfileProcessOfWork;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_OrgRewardForUnit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgRewardForUnitId;
	NSNumber * OrgRewardId;
	empl_tns1_OrgRewardMaster * OrgRewardMaster;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_OrgRewardForUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgRewardForUnitId;
@property (retain) NSNumber * OrgRewardId;
@property (retain) empl_tns1_OrgRewardMaster * OrgRewardMaster;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfOrgRewardForUnit : NSObject {
	
/* elements */
	NSMutableArray *OrgRewardForUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfOrgRewardForUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgRewardForUnit:(empl_tns1_OrgRewardForUnit *)toAdd;
@property (readonly) NSMutableArray * OrgRewardForUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_OrgRewardMaster : NSObject {
	
/* elements */
	NSNumber * ApproveId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSDate * DateOfIssue;
	NSString * DecisionNo;
	empl_tns1_ArrayOfEmpProfileReward * EmpProfileRewards;
	USBoolean * IsAllCompany;
	USBoolean * IsDeleted;
	USBoolean * IsForEmployee;
	NSDate * ModifiedDate;
	NSNumber * MonetaryValue;
	empl_tns1_ArrayOfOrgRewardForUnit * OrgRewardForUnits;
	NSNumber * OrgRewardMasterId;
	NSString * OrgRewardTypeCode;
	USBoolean * Status;
	NSString * Title;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_OrgRewardMaster *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproveId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSDate * DateOfIssue;
@property (retain) NSString * DecisionNo;
@property (retain) empl_tns1_ArrayOfEmpProfileReward * EmpProfileRewards;
@property (retain) USBoolean * IsAllCompany;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsForEmployee;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * MonetaryValue;
@property (retain) empl_tns1_ArrayOfOrgRewardForUnit * OrgRewardForUnits;
@property (retain) NSNumber * OrgRewardMasterId;
@property (retain) NSString * OrgRewardTypeCode;
@property (retain) USBoolean * Status;
@property (retain) NSString * Title;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileReward : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfileRewardId;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	empl_tns1_OrgRewardMaster * OrgRewardMaster;
	NSNumber * OrgRewardMasterId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileReward *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfileRewardId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_OrgRewardMaster * OrgRewardMaster;
@property (retain) NSNumber * OrgRewardMasterId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileReward : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileReward;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileReward *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileReward:(empl_tns1_EmpProfileReward *)toAdd;
@property (readonly) NSMutableArray * EmpProfileReward;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileTotalIncome : NSObject {
	
/* elements */
	NSDate * CompanyId;
	NSDate * CreatedDate;
	NSNumber * DependancyDeduction;
	NSNumber * EmpProfileTotalIncomeId;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSNumber * GrossPayment;
	NSDate * ModifiedDate;
	NSNumber * Month;
	NSNumber * NetPayment;
	NSNumber * PIT;
	NSNumber * TaxableAmount;
	NSNumber * TotalDeduction;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileTotalIncome *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * DependancyDeduction;
@property (retain) NSNumber * EmpProfileTotalIncomeId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * GrossPayment;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Month;
@property (retain) NSNumber * NetPayment;
@property (retain) NSNumber * PIT;
@property (retain) NSNumber * TaxableAmount;
@property (retain) NSNumber * TotalDeduction;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileTotalIncome : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileTotalIncome;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileTotalIncome *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileTotalIncome:(empl_tns1_EmpProfileTotalIncome *)toAdd;
@property (readonly) NSMutableArray * EmpProfileTotalIncome;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileTraining : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * AttachedDocumentUrl;
	NSString * Certifications;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileTrainingId;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	NSDate * ExpiredDate;
	USBoolean * IsDeleted;
	USBoolean * IsInternal;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * RelateRowId;
	NSString * Result;
	NSDate * StartDate;
	NSNumber * Status;
	NSString * TrainingProgram;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileTraining *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * AttachedDocumentUrl;
@property (retain) NSString * Certifications;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileTrainingId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) NSDate * ExpiredDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInternal;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * Result;
@property (retain) NSDate * StartDate;
@property (retain) NSNumber * Status;
@property (retain) NSString * TrainingProgram;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileTraining : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileTraining;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileTraining *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileTraining:(empl_tns1_EmpProfileTraining *)toAdd;
@property (readonly) NSMutableArray * EmpProfileTraining;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileWageType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileWageTypeId;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ImplementationDate;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * TimeWageTypeCode;
	NSNumber * TimeWageTypeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileWageType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileWageTypeId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * TimeWageTypeCode;
@property (retain) NSNumber * TimeWageTypeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileWageType : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileWageType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileWageType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileWageType:(empl_tns1_EmpProfileWageType *)toAdd;
@property (readonly) NSMutableArray * EmpProfileWageType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileWorkingForm : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileWorkingFormId;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ImplementationDate;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * TimeWorkingFormCode;
	NSNumber * TimeWorkingFormId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileWorkingForm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileWorkingFormId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * TimeWorkingFormCode;
@property (retain) NSNumber * TimeWorkingFormId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileWorkingForm : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileWorkingForm;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileWorkingForm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileWorkingForm:(empl_tns1_EmpProfileWorkingForm *)toAdd;
@property (readonly) NSMutableArray * EmpProfileWorkingForm;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpSocialInsuranceSalary : NSObject {
	
/* elements */
	NSNumber * CLogCurrencyId;
	NSNumber * CLogCurrencyRateId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * DecisionCode;
	NSString * DecisionName;
	NSNumber * EmpSocialInsuranceSalaryId;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSDate * ExpirationDate;
	NSString * FileDecisionUrl;
	NSDate * ImplementationDate;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	USBoolean * IsIncrease;
	USBoolean * IsRecall;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * Reason;
	NSDate * RecallMonth;
	NSDate * SocialInsuranceDate;
	NSString * cEmpSISalary;
	NSString * cEmpSISalaryCoeficient;
	NSString * cEmpSISalaryGrade;
	NSNumber * nEmpSISalary;
	NSNumber * nEmpSISalaryCoeficient;
	NSNumber * nEmpSISalaryGrade;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpSocialInsuranceSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogCurrencyId;
@property (retain) NSNumber * CLogCurrencyRateId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DecisionCode;
@property (retain) NSString * DecisionName;
@property (retain) NSNumber * EmpSocialInsuranceSalaryId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSString * FileDecisionUrl;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsIncrease;
@property (retain) USBoolean * IsRecall;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * Reason;
@property (retain) NSDate * RecallMonth;
@property (retain) NSDate * SocialInsuranceDate;
@property (retain) NSString * cEmpSISalary;
@property (retain) NSString * cEmpSISalaryCoeficient;
@property (retain) NSString * cEmpSISalaryGrade;
@property (retain) NSNumber * nEmpSISalary;
@property (retain) NSNumber * nEmpSISalaryCoeficient;
@property (retain) NSNumber * nEmpSISalaryGrade;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpSocialInsuranceSalary : NSObject {
	
/* elements */
	NSMutableArray *EmpSocialInsuranceSalary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpSocialInsuranceSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpSocialInsuranceSalary:(empl_tns1_EmpSocialInsuranceSalary *)toAdd;
@property (readonly) NSMutableArray * EmpSocialInsuranceSalary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_NCClientConnection : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * ConnectionId;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSNumber * NCClientConnectionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_NCClientConnection *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ConnectionId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * NCClientConnectionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfNCClientConnection : NSObject {
	
/* elements */
	NSMutableArray *NCClientConnection;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfNCClientConnection *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addNCClientConnection:(empl_tns1_NCClientConnection *)toAdd;
@property (readonly) NSMutableArray * NCClientConnection;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_NCMessageType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * NCMessageTypeId;
	empl_tns1_ArrayOfNCMessage * NCMessages;
	NSString * Name;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_NCMessageType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NCMessageTypeId;
@property (retain) empl_tns1_ArrayOfNCMessage * NCMessages;
@property (retain) NSString * Name;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_NCMessage : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_Employee * Employee;
	empl_tns1_Employee * Employee1;
	NSString * FullMessage;
	USBoolean * IsDeleted;
	USBoolean * IsNew;
	NSDate * ModifiedDate;
	NSNumber * NCMessageId;
	empl_tns1_NCMessageType * NCMessageType;
	NSNumber * NCMessageTypeId;
	NSNumber * ReceiverEmployeeId;
	NSNumber * SenderEmployeeId;
	NSString * ShortcutMessage;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_NCMessage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) empl_tns1_Employee * Employee1;
@property (retain) NSString * FullMessage;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsNew;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NCMessageId;
@property (retain) empl_tns1_NCMessageType * NCMessageType;
@property (retain) NSNumber * NCMessageTypeId;
@property (retain) NSNumber * ReceiverEmployeeId;
@property (retain) NSNumber * SenderEmployeeId;
@property (retain) NSString * ShortcutMessage;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfNCMessage : NSObject {
	
/* elements */
	NSMutableArray *NCMessage;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfNCMessage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addNCMessage:(empl_tns1_NCMessage *)toAdd;
@property (readonly) NSMutableArray * NCMessage;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_Project : NSObject {
	
/* elements */
	NSString * Description;
	empl_tns1_Employee * Employee;
	NSDate * EndDate;
	NSNumber * Id_;
	NSString * Name;
	NSNumber * OwnerId;
	NSNumber * Progress;
	empl_tns1_ArrayOfProjectMember * ProjectMembers;
	NSDate * StartDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_Project *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Description;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSDate * EndDate;
@property (retain) NSNumber * Id_;
@property (retain) NSString * Name;
@property (retain) NSNumber * OwnerId;
@property (retain) NSNumber * Progress;
@property (retain) empl_tns1_ArrayOfProjectMember * ProjectMembers;
@property (retain) NSDate * StartDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ProjectMember : NSObject {
	
/* elements */
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSNumber * Id_;
	empl_tns1_Project * Project;
	NSNumber * ProjectId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ProjectMember *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * Id_;
@property (retain) empl_tns1_Project * Project;
@property (retain) NSNumber * ProjectId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfProjectMember : NSObject {
	
/* elements */
	NSMutableArray *ProjectMember;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfProjectMember *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addProjectMember:(empl_tns1_ProjectMember *)toAdd;
@property (readonly) NSMutableArray * ProjectMember;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfProject : NSObject {
	
/* elements */
	NSMutableArray *Project;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfProject *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addProject:(empl_tns1_Project *)toAdd;
@property (readonly) NSMutableArray * Project;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ReportEmployeeRequestedBook : NSObject {
	
/* elements */
	NSString * Address1;
	NSString * Benefit;
	NSDate * BirthDay;
	NSString * CLogCityNoIns;
	NSString * CLogHospitalCode;
	empl_tns1_Employee * Employee;
	NSString * Ethnicity;
	NSString * FullName;
	NSString * Gender;
	USBoolean * HadSIBook;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	NSString * Note;
	NSString * OrgUnitName;
	NSNumber * ReportEmployeeRequestedBookId;
	NSDate * RequestedDay;
	NSString * SICode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ReportEmployeeRequestedBook *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address1;
@property (retain) NSString * Benefit;
@property (retain) NSDate * BirthDay;
@property (retain) NSString * CLogCityNoIns;
@property (retain) NSString * CLogHospitalCode;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * Ethnicity;
@property (retain) NSString * FullName;
@property (retain) NSString * Gender;
@property (retain) USBoolean * HadSIBook;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) NSString * Note;
@property (retain) NSString * OrgUnitName;
@property (retain) NSNumber * ReportEmployeeRequestedBookId;
@property (retain) NSDate * RequestedDay;
@property (retain) NSString * SICode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_SelfLeaveRequest : NSObject {
	
/* elements */
	NSString * Address;
	NSNumber * ApprovalStatus;
	NSString * ApproverCode;
	NSNumber * ApproverId;
	NSDate * CreatedDate;
	empl_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	USBoolean * IsCompensatoryLeave;
	USBoolean * IsDeleted;
	USBoolean * IsLeaveWithoutPay;
	USBoolean * IsPaidLeave;
	USBoolean * IsUnpaidLeave;
	NSDate * ModifiedDate;
	NSNumber * NumberDaysOff;
	NSNumber * OrganizationId;
	NSString * PhoneNumber;
	NSString * Reason;
	NSNumber * SalaryPenaltyValue;
	NSNumber * SelfLeaveRequestId;
	NSDate * StartDate;
	NSNumber * TimeAbsenceTypeId;
	NSString * WorkflowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_SelfLeaveRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSNumber * ApprovalStatus;
@property (retain) NSString * ApproverCode;
@property (retain) NSNumber * ApproverId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsCompensatoryLeave;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsLeaveWithoutPay;
@property (retain) USBoolean * IsPaidLeave;
@property (retain) USBoolean * IsUnpaidLeave;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NumberDaysOff;
@property (retain) NSNumber * OrganizationId;
@property (retain) NSString * PhoneNumber;
@property (retain) NSString * Reason;
@property (retain) NSNumber * SalaryPenaltyValue;
@property (retain) NSNumber * SelfLeaveRequestId;
@property (retain) NSDate * StartDate;
@property (retain) NSNumber * TimeAbsenceTypeId;
@property (retain) NSString * WorkflowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfSelfLeaveRequest : NSObject {
	
/* elements */
	NSMutableArray *SelfLeaveRequest;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfSelfLeaveRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSelfLeaveRequest:(empl_tns1_SelfLeaveRequest *)toAdd;
@property (readonly) NSMutableArray * SelfLeaveRequest;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfSysRecPlanApprover : NSObject {
	
/* elements */
	NSMutableArray *SysRecPlanApprover;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfSysRecPlanApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysRecPlanApprover:(empl_tns1_SysRecPlanApprover *)toAdd;
@property (readonly) NSMutableArray * SysRecPlanApprover;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfSysRecRequirementApprover : NSObject {
	
/* elements */
	NSMutableArray *SysRecRequirementApprover;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfSysRecRequirementApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysRecRequirementApprover:(empl_tns1_SysRecRequirementApprover *)toAdd;
@property (readonly) NSMutableArray * SysRecRequirementApprover;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_TrainingEmpPlan : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsSubmited;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * State;
	NSNumber * StrategyGoalId;
	NSString * Target;
	empl_tns1_ArrayOfTrainingEmpPlanApproved * TrainingEmpPlanApproveds;
	NSNumber * TrainingEmpPlanId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_TrainingEmpPlan *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsSubmited;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * State;
@property (retain) NSNumber * StrategyGoalId;
@property (retain) NSString * Target;
@property (retain) empl_tns1_ArrayOfTrainingEmpPlanApproved * TrainingEmpPlanApproveds;
@property (retain) NSNumber * TrainingEmpPlanId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_TrainingEmpPlanApproved : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	empl_tns1_SysTrainingApprover * SysTrainingApprover;
	NSNumber * SysTrainingApproverId;
	empl_tns1_TrainingEmpPlan * TrainingEmpPlan;
	NSNumber * TrainingEmpPlanApprovedId;
	NSNumber * TrainingEmpPlanId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_TrainingEmpPlanApproved *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_SysTrainingApprover * SysTrainingApprover;
@property (retain) NSNumber * SysTrainingApproverId;
@property (retain) empl_tns1_TrainingEmpPlan * TrainingEmpPlan;
@property (retain) NSNumber * TrainingEmpPlanApprovedId;
@property (retain) NSNumber * TrainingEmpPlanId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfTrainingEmpPlanApproved : NSObject {
	
/* elements */
	NSMutableArray *TrainingEmpPlanApproved;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfTrainingEmpPlanApproved *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingEmpPlanApproved:(empl_tns1_TrainingEmpPlanApproved *)toAdd;
@property (readonly) NSMutableArray * TrainingEmpPlanApproved;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_SysTrainingApprover : NSObject {
	
/* elements */
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	empl_tns1_Employee * Employee;
	USBoolean * IsDeleted;
	NSNumber * Number;
	NSNumber * SysTrainingApproverId;
	empl_tns1_ArrayOfTrainingEmpPlanApproved * TrainingEmpPlanApproveds;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_SysTrainingApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Number;
@property (retain) NSNumber * SysTrainingApproverId;
@property (retain) empl_tns1_ArrayOfTrainingEmpPlanApproved * TrainingEmpPlanApproveds;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfSysTrainingApprover : NSObject {
	
/* elements */
	NSMutableArray *SysTrainingApprover;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfSysTrainingApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysTrainingApprover:(empl_tns1_SysTrainingApprover *)toAdd;
@property (readonly) NSMutableArray * SysTrainingApprover;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_Task : NSObject {
	
/* elements */
	NSString * Description;
	empl_tns1_Employee * Employee;
	NSDate * FromDate;
	NSNumber * Id_;
	NSString * Name;
	NSNumber * Process;
	NSNumber * ProjectId;
	NSNumber * Status;
	NSNumber * TaskOwnerId;
	NSDate * ToDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_Task *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Description;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSDate * FromDate;
@property (retain) NSNumber * Id_;
@property (retain) NSString * Name;
@property (retain) NSNumber * Process;
@property (retain) NSNumber * ProjectId;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * TaskOwnerId;
@property (retain) NSDate * ToDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfTask : NSObject {
	
/* elements */
	NSMutableArray *Task;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfTask *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTask:(empl_tns1_Task *)toAdd;
@property (readonly) NSMutableArray * Task;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfTrainingEmpPlan : NSObject {
	
/* elements */
	NSMutableArray *TrainingEmpPlan;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfTrainingEmpPlan *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingEmpPlan:(empl_tns1_TrainingEmpPlan *)toAdd;
@property (readonly) NSMutableArray * TrainingEmpPlan;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_Employee : NSObject {
	
/* elements */
	NSDate * BirthDay;
	NSString * BirthPlace;
	NSNumber * BirthPlaceId;
	empl_tns1_ArrayOfCBAccidentInsurance * CBAccidentInsurances;
	empl_tns1_ArrayOfCBCompensationEmployeeGroupDetail * CBCompensationEmployeeGroupDetails;
	empl_tns1_ArrayOfCBConvalescence * CBConvalescences;
	empl_tns1_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
	empl_tns1_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
	empl_tns1_ArrayOfCBHealthInsuranceDetail * CBHealthInsuranceDetails;
	empl_tns1_CLogCity * CLogCity;
	empl_tns1_CLogCity * CLogCity1;
	empl_tns1_CLogEmployeeType * CLogEmployeeType;
	empl_tns1_ArrayOfCLogTrainer * CLogTrainers;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	empl_tns1_ArrayOfEmpBasicProfile * EmpBasicProfiles;
	empl_tns1_ArrayOfEmpCompetency * EmpCompetencies;
	empl_tns1_ArrayOfEmpCompetencyRating * EmpCompetencyRatings;
	empl_tns1_ArrayOfEmpContract * EmpContracts;
	empl_tns1_ArrayOfEmpPerformanceAppraisal * EmpPerformanceAppraisals;
	empl_tns1_ArrayOfEmpProfileAllowance * EmpProfileAllowances;
	empl_tns1_ArrayOfEmpProfileBaseSalary * EmpProfileBaseSalaries;
	empl_tns1_ArrayOfEmpProfileBenefit * EmpProfileBenefits;
	empl_tns1_ArrayOfEmpProfileComment * EmpProfileComments;
	empl_tns1_ArrayOfEmpProfileComment * EmpProfileComments1;
	empl_tns1_ArrayOfEmpProfileComputingSkill * EmpProfileComputingSkills;
	empl_tns1_ArrayOfEmpProfileContact * EmpProfileContacts;
	empl_tns1_ArrayOfEmpProfileDegree * EmpProfileDegrees;
	empl_tns1_ArrayOfEmpProfileDiscipline * EmpProfileDisciplines;
	empl_tns1_ArrayOfEmpProfileEducation * EmpProfileEducations;
	empl_tns1_ArrayOfEmpProfileEquipment * EmpProfileEquipments;
	empl_tns1_ArrayOfEmpProfileExperience * EmpProfileExperiences;
	empl_tns1_ArrayOfEmpProfileFamilyRelationship * EmpProfileFamilyRelationships;
	empl_tns1_ArrayOfEmpProfileForeignLanguage * EmpProfileForeignLanguages;
	empl_tns1_ArrayOfEmpProfileHealthInsurance * EmpProfileHealthInsurances;
	empl_tns1_ArrayOfEmpProfileHealthy * EmpProfileHealthies;
	empl_tns1_ArrayOfEmpProfileHobby * EmpProfileHobbies;
	empl_tns1_ArrayOfEmpProfileInternalCourse * EmpProfileInternalCourses;
	empl_tns1_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions;
	empl_tns1_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions1;
	empl_tns1_ArrayOfEmpProfileLeaveRegime * EmpProfileLeaveRegimes;
	empl_tns1_ArrayOfEmpProfileOtherBenefit * EmpProfileOtherBenefits;
	empl_tns1_ArrayOfEmpProfileParticipation * EmpProfileParticipations;
	empl_tns1_ArrayOfEmpProfilePersonality * EmpProfilePersonalities;
	empl_tns1_ArrayOfEmpProfileProcessOfWork * EmpProfileProcessOfWorks;
	empl_tns1_ArrayOfEmpProfileQualification * EmpProfileQualifications;
	empl_tns1_ArrayOfEmpProfileReward * EmpProfileRewards;
	empl_tns1_ArrayOfEmpProfileSkill * EmpProfileSkills;
	empl_tns1_ArrayOfEmpProfileTotalIncome * EmpProfileTotalIncomes;
	empl_tns1_ArrayOfEmpProfileTraining * EmpProfileTrainings;
	empl_tns1_ArrayOfEmpProfileWageType * EmpProfileWageTypes;
	empl_tns1_ArrayOfEmpProfileWorkingExperience * EmpProfileWorkingExperiences;
	empl_tns1_ArrayOfEmpProfileWorkingForm * EmpProfileWorkingForms;
	empl_tns1_ArrayOfEmpSocialInsuranceSalary * EmpSocialInsuranceSalaries;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * EmployeeTKCode;
	NSNumber * EmployeeTypeId;
	NSDate * EndWorkingDate;
	NSDate * EntryDate;
	NSString * Ethnicity;
	NSNumber * EthnicityId;
	NSString * FirstName;
	NSString * FullName;
	empl_tns1_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals;
	empl_tns1_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals1;
	empl_tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
	empl_tns1_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
	empl_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
	empl_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
	empl_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
	empl_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives1;
	empl_tns1_ArrayOfGPObjectiveInitiative * GPObjectiveInitiatives;
	empl_tns1_ArrayOfGPPerformanceYearEndResult * GPPerformanceYearEndResults;
	USBoolean * Gender;
	NSString * HomeTown;
	NSNumber * HomeTownId;
	NSString * IdentityCardNo;
	NSString * ImageUrl;
	USBoolean * IsDeleted;
	USBoolean * IsOnlyYearOfBirthday;
	empl_tns1_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
	empl_tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
	empl_tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts1;
	NSString * LastName;
	NSString * MainLanguage;
	NSNumber * MainLanguageId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	empl_tns1_ArrayOfNCClientConnection * NCClientConnections;
	empl_tns1_ArrayOfNCMessage * NCMessages;
	empl_tns1_ArrayOfNCMessage * NCMessages1;
	NSDate * OfficialDate;
	NSString * OrgGroupCd;
	NSNumber * OrgGroupId;
	NSDate * ProbationaryDate;
	NSDate * ProbationaryEndDate;
	empl_tns1_ArrayOfProjectMember * ProjectMembers;
	empl_tns1_ArrayOfProject * Projects;
	empl_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
	empl_tns1_ArrayOfRecInterviewer * RecInterviewers;
	empl_tns1_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
	empl_tns1_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
	empl_tns1_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
	NSString * Religion;
	NSNumber * ReligionId;
	empl_tns1_ReportEmployeeRequestedBook * ReportEmployeeRequestedBook;
	empl_tns1_ArrayOfSelfLeaveRequest * SelfLeaveRequests;
	empl_tns1_ArrayOfSysRecPlanApprover * SysRecPlanApprovers;
	empl_tns1_ArrayOfSysRecRequirementApprover * SysRecRequirementApprovers;
	empl_tns1_ArrayOfSysTrainingApprover * SysTrainingApprovers;
	empl_tns1_ArrayOfTask * Tasks;
	empl_tns1_ArrayOfTrainingCourseEmployee * TrainingCourseEmployees;
	empl_tns1_ArrayOfTrainingEmpPlan * TrainingEmpPlans;
	empl_tns1_ArrayOfTrainingPlanEmployee * TrainingPlanEmployees;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_Employee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * BirthDay;
@property (retain) NSString * BirthPlace;
@property (retain) NSNumber * BirthPlaceId;
@property (retain) empl_tns1_ArrayOfCBAccidentInsurance * CBAccidentInsurances;
@property (retain) empl_tns1_ArrayOfCBCompensationEmployeeGroupDetail * CBCompensationEmployeeGroupDetails;
@property (retain) empl_tns1_ArrayOfCBConvalescence * CBConvalescences;
@property (retain) empl_tns1_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
@property (retain) empl_tns1_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
@property (retain) empl_tns1_ArrayOfCBHealthInsuranceDetail * CBHealthInsuranceDetails;
@property (retain) empl_tns1_CLogCity * CLogCity;
@property (retain) empl_tns1_CLogCity * CLogCity1;
@property (retain) empl_tns1_CLogEmployeeType * CLogEmployeeType;
@property (retain) empl_tns1_ArrayOfCLogTrainer * CLogTrainers;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) empl_tns1_ArrayOfEmpBasicProfile * EmpBasicProfiles;
@property (retain) empl_tns1_ArrayOfEmpCompetency * EmpCompetencies;
@property (retain) empl_tns1_ArrayOfEmpCompetencyRating * EmpCompetencyRatings;
@property (retain) empl_tns1_ArrayOfEmpContract * EmpContracts;
@property (retain) empl_tns1_ArrayOfEmpPerformanceAppraisal * EmpPerformanceAppraisals;
@property (retain) empl_tns1_ArrayOfEmpProfileAllowance * EmpProfileAllowances;
@property (retain) empl_tns1_ArrayOfEmpProfileBaseSalary * EmpProfileBaseSalaries;
@property (retain) empl_tns1_ArrayOfEmpProfileBenefit * EmpProfileBenefits;
@property (retain) empl_tns1_ArrayOfEmpProfileComment * EmpProfileComments;
@property (retain) empl_tns1_ArrayOfEmpProfileComment * EmpProfileComments1;
@property (retain) empl_tns1_ArrayOfEmpProfileComputingSkill * EmpProfileComputingSkills;
@property (retain) empl_tns1_ArrayOfEmpProfileContact * EmpProfileContacts;
@property (retain) empl_tns1_ArrayOfEmpProfileDegree * EmpProfileDegrees;
@property (retain) empl_tns1_ArrayOfEmpProfileDiscipline * EmpProfileDisciplines;
@property (retain) empl_tns1_ArrayOfEmpProfileEducation * EmpProfileEducations;
@property (retain) empl_tns1_ArrayOfEmpProfileEquipment * EmpProfileEquipments;
@property (retain) empl_tns1_ArrayOfEmpProfileExperience * EmpProfileExperiences;
@property (retain) empl_tns1_ArrayOfEmpProfileFamilyRelationship * EmpProfileFamilyRelationships;
@property (retain) empl_tns1_ArrayOfEmpProfileForeignLanguage * EmpProfileForeignLanguages;
@property (retain) empl_tns1_ArrayOfEmpProfileHealthInsurance * EmpProfileHealthInsurances;
@property (retain) empl_tns1_ArrayOfEmpProfileHealthy * EmpProfileHealthies;
@property (retain) empl_tns1_ArrayOfEmpProfileHobby * EmpProfileHobbies;
@property (retain) empl_tns1_ArrayOfEmpProfileInternalCourse * EmpProfileInternalCourses;
@property (retain) empl_tns1_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions;
@property (retain) empl_tns1_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions1;
@property (retain) empl_tns1_ArrayOfEmpProfileLeaveRegime * EmpProfileLeaveRegimes;
@property (retain) empl_tns1_ArrayOfEmpProfileOtherBenefit * EmpProfileOtherBenefits;
@property (retain) empl_tns1_ArrayOfEmpProfileParticipation * EmpProfileParticipations;
@property (retain) empl_tns1_ArrayOfEmpProfilePersonality * EmpProfilePersonalities;
@property (retain) empl_tns1_ArrayOfEmpProfileProcessOfWork * EmpProfileProcessOfWorks;
@property (retain) empl_tns1_ArrayOfEmpProfileQualification * EmpProfileQualifications;
@property (retain) empl_tns1_ArrayOfEmpProfileReward * EmpProfileRewards;
@property (retain) empl_tns1_ArrayOfEmpProfileSkill * EmpProfileSkills;
@property (retain) empl_tns1_ArrayOfEmpProfileTotalIncome * EmpProfileTotalIncomes;
@property (retain) empl_tns1_ArrayOfEmpProfileTraining * EmpProfileTrainings;
@property (retain) empl_tns1_ArrayOfEmpProfileWageType * EmpProfileWageTypes;
@property (retain) empl_tns1_ArrayOfEmpProfileWorkingExperience * EmpProfileWorkingExperiences;
@property (retain) empl_tns1_ArrayOfEmpProfileWorkingForm * EmpProfileWorkingForms;
@property (retain) empl_tns1_ArrayOfEmpSocialInsuranceSalary * EmpSocialInsuranceSalaries;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * EmployeeTKCode;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSDate * EndWorkingDate;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * Ethnicity;
@property (retain) NSNumber * EthnicityId;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) empl_tns1_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals;
@property (retain) empl_tns1_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals1;
@property (retain) empl_tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
@property (retain) empl_tns1_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
@property (retain) empl_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
@property (retain) empl_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
@property (retain) empl_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
@property (retain) empl_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives1;
@property (retain) empl_tns1_ArrayOfGPObjectiveInitiative * GPObjectiveInitiatives;
@property (retain) empl_tns1_ArrayOfGPPerformanceYearEndResult * GPPerformanceYearEndResults;
@property (retain) USBoolean * Gender;
@property (retain) NSString * HomeTown;
@property (retain) NSNumber * HomeTownId;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) empl_tns1_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
@property (retain) empl_tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
@property (retain) empl_tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts1;
@property (retain) NSString * LastName;
@property (retain) NSString * MainLanguage;
@property (retain) NSNumber * MainLanguageId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) empl_tns1_ArrayOfNCClientConnection * NCClientConnections;
@property (retain) empl_tns1_ArrayOfNCMessage * NCMessages;
@property (retain) empl_tns1_ArrayOfNCMessage * NCMessages1;
@property (retain) NSDate * OfficialDate;
@property (retain) NSString * OrgGroupCd;
@property (retain) NSNumber * OrgGroupId;
@property (retain) NSDate * ProbationaryDate;
@property (retain) NSDate * ProbationaryEndDate;
@property (retain) empl_tns1_ArrayOfProjectMember * ProjectMembers;
@property (retain) empl_tns1_ArrayOfProject * Projects;
@property (retain) empl_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
@property (retain) empl_tns1_ArrayOfRecInterviewer * RecInterviewers;
@property (retain) empl_tns1_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
@property (retain) empl_tns1_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
@property (retain) empl_tns1_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
@property (retain) NSString * Religion;
@property (retain) NSNumber * ReligionId;
@property (retain) empl_tns1_ReportEmployeeRequestedBook * ReportEmployeeRequestedBook;
@property (retain) empl_tns1_ArrayOfSelfLeaveRequest * SelfLeaveRequests;
@property (retain) empl_tns1_ArrayOfSysRecPlanApprover * SysRecPlanApprovers;
@property (retain) empl_tns1_ArrayOfSysRecRequirementApprover * SysRecRequirementApprovers;
@property (retain) empl_tns1_ArrayOfSysTrainingApprover * SysTrainingApprovers;
@property (retain) empl_tns1_ArrayOfTask * Tasks;
@property (retain) empl_tns1_ArrayOfTrainingCourseEmployee * TrainingCourseEmployees;
@property (retain) empl_tns1_ArrayOfTrainingEmpPlan * TrainingEmpPlans;
@property (retain) empl_tns1_ArrayOfTrainingPlanEmployee * TrainingPlanEmployees;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileComputingSkill : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSString * ComputingSkillName;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	NSNumber * EmpProfileComputingSkillId;
	empl_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsValid;
	NSDate * ModifiedDate;
	NSString * Other;
	NSString * PlaceIssue;
	NSNumber * RatingId;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileComputingSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ComputingSkillName;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) NSNumber * EmpProfileComputingSkillId;
@property (retain) empl_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValid;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) NSNumber * RatingId;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileComputingSkill : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileComputingSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileComputingSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileComputingSkill:(empl_tns1_EmpProfileComputingSkill *)toAdd;
@property (readonly) NSMutableArray * EmpProfileComputingSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpProfileSkill : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * DocumentRef;
	NSNumber * EmpProfileSkillId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * OrgSkillCode;
	NSString * OrgSkillDescription;
	NSNumber * OrgSkillId;
	NSString * OrgSkillName;
	NSNumber * OrgSkillPriority;
	NSString * OrgSkillPriorityName;
	NSNumber * OrgSkillTypeId;
	NSString * OrgSkillTypeName;
	NSString * RatingDescription;
	NSNumber * RatingId;
	NSNumber * RatingValue;
	NSNumber * RelateRowId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpProfileSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DocumentRef;
@property (retain) NSNumber * EmpProfileSkillId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OrgSkillCode;
@property (retain) NSString * OrgSkillDescription;
@property (retain) NSNumber * OrgSkillId;
@property (retain) NSString * OrgSkillName;
@property (retain) NSNumber * OrgSkillPriority;
@property (retain) NSString * OrgSkillPriorityName;
@property (retain) NSNumber * OrgSkillTypeId;
@property (retain) NSString * OrgSkillTypeName;
@property (retain) NSString * RatingDescription;
@property (retain) NSNumber * RatingId;
@property (retain) NSNumber * RatingValue;
@property (retain) NSNumber * RelateRowId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_EmpProfileSkill : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_EmpProfileSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileSkill:(empl_tns1_V_EmpProfileSkill *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpProfileDegree : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	NSNumber * EmpProfileDegreeId;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsValId;
	NSDate * ModifiedDate;
	NSString * OrgDegreeDescription;
	NSNumber * OrgDegreeId;
	NSString * OrgDegreeName;
	NSNumber * OrgDegreeRankId;
	NSString * OrgDegreeRankName;
	NSString * Other;
	NSString * PlaceIssue;
	NSString * ROW_ID;
	NSNumber * RelateRowId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpProfileDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) NSNumber * EmpProfileDegreeId;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OrgDegreeDescription;
@property (retain) NSNumber * OrgDegreeId;
@property (retain) NSString * OrgDegreeName;
@property (retain) NSNumber * OrgDegreeRankId;
@property (retain) NSString * OrgDegreeRankName;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) NSString * ROW_ID;
@property (retain) NSNumber * RelateRowId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_EmpProfileDegree : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileDegree;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_EmpProfileDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileDegree:(empl_tns1_V_EmpProfileDegree *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileDegree;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpProfileKnowledge : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * CLogMajorCode;
	NSNumber * CLogMajorId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * Description;
	NSNumber * EmpProfileKnowledgeId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSString * MajorName;
	NSString * MajorNameEN;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * OrgKnowledgeCode;
	NSNumber * OrgKnowledgeId;
	NSString * OrgKnowledgeName;
	NSString * RatingDesc;
	NSNumber * RatingId;
	NSNumber * RatingValue;
	NSNumber * RelateRowId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpProfileKnowledge *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * CLogMajorCode;
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) NSNumber * EmpProfileKnowledgeId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * MajorName;
@property (retain) NSString * MajorNameEN;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * OrgKnowledgeCode;
@property (retain) NSNumber * OrgKnowledgeId;
@property (retain) NSString * OrgKnowledgeName;
@property (retain) NSString * RatingDesc;
@property (retain) NSNumber * RatingId;
@property (retain) NSNumber * RatingValue;
@property (retain) NSNumber * RelateRowId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_EmpProfileKnowledge : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileKnowledge;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_EmpProfileKnowledge *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileKnowledge:(empl_tns1_V_EmpProfileKnowledge *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileKnowledge;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_CBFactorEmployeeMetaDataWithGroup : NSObject {
	
/* elements */
	NSDate * BeginDate;
	NSString * CBCompensationEmployeeGroupDescription;
	NSNumber * CBCompensationEmployeeGroupId;
	NSString * CBCompensationEmployeeGroupName;
	NSString * CBFactorEmployeeMetaDataDescription;
	NSNumber * CBFactorEmployeeMetaDataId;
	NSNumber * CLogCBCompensationCategoryId;
	NSString * CLogCBFactorCode;
	NSString * CLogCBFactorDescription;
	NSNumber * CLogCBFactorId;
	NSString * CLogCBFactorName;
	NSString * ChangeReason;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EmployeeTypeId;
	NSDate * EndDate;
	NSNumber * FactorValue;
	NSString * FormulaExpression;
	NSString * FullName;
	USBoolean * IsChanged;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OldFactorValue;
	NSString * ReferenceColunmName;
	NSString * ReferenceRowGuid;
	NSString * ReferenceTableName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_CBFactorEmployeeMetaDataWithGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * BeginDate;
@property (retain) NSString * CBCompensationEmployeeGroupDescription;
@property (retain) NSNumber * CBCompensationEmployeeGroupId;
@property (retain) NSString * CBCompensationEmployeeGroupName;
@property (retain) NSString * CBFactorEmployeeMetaDataDescription;
@property (retain) NSNumber * CBFactorEmployeeMetaDataId;
@property (retain) NSNumber * CLogCBCompensationCategoryId;
@property (retain) NSString * CLogCBFactorCode;
@property (retain) NSString * CLogCBFactorDescription;
@property (retain) NSNumber * CLogCBFactorId;
@property (retain) NSString * CLogCBFactorName;
@property (retain) NSString * ChangeReason;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSDate * EndDate;
@property (retain) NSNumber * FactorValue;
@property (retain) NSString * FormulaExpression;
@property (retain) NSString * FullName;
@property (retain) USBoolean * IsChanged;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OldFactorValue;
@property (retain) NSString * ReferenceColunmName;
@property (retain) NSString * ReferenceRowGuid;
@property (retain) NSString * ReferenceTableName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_CBFactorEmployeeMetaDataWithGroup : NSObject {
	
/* elements */
	NSMutableArray *V_CBFactorEmployeeMetaDataWithGroup;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_CBFactorEmployeeMetaDataWithGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_CBFactorEmployeeMetaDataWithGroup:(empl_tns1_V_CBFactorEmployeeMetaDataWithGroup *)toAdd;
@property (readonly) NSMutableArray * V_CBFactorEmployeeMetaDataWithGroup;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_CBFactorEmployeeMetaData : NSObject {
	
/* elements */
	NSDate * BeginDate;
	NSNumber * CBCompensationEmployeeGroupId;
	NSString * CBCompensationEmployeeGroupName;
	NSString * CBFactorEmployeeMetaDataDescription;
	NSNumber * CBFactorEmployeeMetaDataId;
	NSNumber * CLogCBCompensationCategoryId;
	NSString * CLogCBCompensationCategoryName;
	NSString * CLogCBFactorCode;
	NSString * CLogCBFactorDescription;
	NSNumber * CLogCBFactorId;
	NSString * CLogCBFactorName;
	NSString * ChangeReason;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EmployeeTypeId;
	NSDate * EndDate;
	NSNumber * FactorValue;
	NSString * FormulaExpression;
	NSString * FullName;
	USBoolean * IsActivated;
	USBoolean * IsChanged;
	USBoolean * IsCurrent;
	USBoolean * IsDeactivated;
	USBoolean * IsDeleted;
	USBoolean * IsInitialed;
	NSDate * ModifiedDate;
	NSNumber * OldFactorValue;
	NSString * ReferenceColunmName;
	NSString * ReferenceRowGuid;
	NSString * ReferenceTableName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_CBFactorEmployeeMetaData *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * BeginDate;
@property (retain) NSNumber * CBCompensationEmployeeGroupId;
@property (retain) NSString * CBCompensationEmployeeGroupName;
@property (retain) NSString * CBFactorEmployeeMetaDataDescription;
@property (retain) NSNumber * CBFactorEmployeeMetaDataId;
@property (retain) NSNumber * CLogCBCompensationCategoryId;
@property (retain) NSString * CLogCBCompensationCategoryName;
@property (retain) NSString * CLogCBFactorCode;
@property (retain) NSString * CLogCBFactorDescription;
@property (retain) NSNumber * CLogCBFactorId;
@property (retain) NSString * CLogCBFactorName;
@property (retain) NSString * ChangeReason;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSDate * EndDate;
@property (retain) NSNumber * FactorValue;
@property (retain) NSString * FormulaExpression;
@property (retain) NSString * FullName;
@property (retain) USBoolean * IsActivated;
@property (retain) USBoolean * IsChanged;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeactivated;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInitialed;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OldFactorValue;
@property (retain) NSString * ReferenceColunmName;
@property (retain) NSString * ReferenceRowGuid;
@property (retain) NSString * ReferenceTableName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_CBFactorEmployeeMetaData : NSObject {
	
/* elements */
	NSMutableArray *V_CBFactorEmployeeMetaData;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_CBFactorEmployeeMetaData *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_CBFactorEmployeeMetaData:(empl_tns1_V_CBFactorEmployeeMetaData *)toAdd;
@property (readonly) NSMutableArray * V_CBFactorEmployeeMetaData;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileMaternityRegime : NSObject {
	
/* elements */
	NSDate * BornDate;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileMaternityRegimeId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsRaisingChild;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSDate * PregnancyMonth7thDate;
	NSDate * RaisingChildBeginDate;
	NSDate * RaisingChildEndDate;
	NSNumber * Times;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileMaternityRegime *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * BornDate;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileMaternityRegimeId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsRaisingChild;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSDate * PregnancyMonth7thDate;
@property (retain) NSDate * RaisingChildBeginDate;
@property (retain) NSDate * RaisingChildEndDate;
@property (retain) NSNumber * Times;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileMaternityRegime : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileMaternityRegime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileMaternityRegime *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileMaternityRegime:(empl_tns1_EmpProfileMaternityRegime *)toAdd;
@property (readonly) NSMutableArray * EmpProfileMaternityRegime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpBasicInformation : NSObject {
	
/* elements */
	NSString * Address;
	NSNumber * AddressId;
	NSString * BankAccountNo;
	NSString * BankBranch;
	NSString * BankName;
	NSDate * BirthDay;
	NSString * BirthPlace;
	NSNumber * BirthPlaceId;
	NSString * CVUrl;
	NSString * CodeAndFullName;
	NSNumber * CompanyId;
	NSString * ContactAddress;
	NSNumber * ContactAddressId;
	NSString * CulturalLevel;
	NSNumber * CulturalLevelId;
	NSNumber * EducationLevelId;
	NSString * EducationLevelName;
	NSString * EducationLevelNameEN;
	NSString * Email;
	NSNumber * EmpBasicProfileId;
	NSString * EmpWorkingStatusCode;
	NSString * EmpWorkingStatusName;
	NSString * EmpWorkingStatusNameEN;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EmployeeTypeId;
	NSString * EmployeeTypeName;
	NSString * EmployeeTypeNameEN;
	NSDate * EntryDate;
	NSString * Ethnicity;
	NSNumber * EthnicityId;
	NSString * FirstName;
	NSString * FullName;
	USBoolean * Gender;
	NSString * HomePhone;
	NSString * HomeTown;
	NSNumber * HomeTownId;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	NSString * ImageUrl;
	USBoolean * IsConcurrentPost;
	USBoolean * IsDeleted;
	USBoolean * IsOnlyYearOfBirthday;
	NSString * LastName;
	NSString * MainLanguage;
	NSNumber * MainLanguageId;
	NSString * Major;
	NSNumber * MajorId;
	NSNumber * MaritalStatusId;
	NSString * MaritalStatusValue;
	NSString * MobileNumber;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSString * OfficePhone;
	NSDate * OfficialDate;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobNameEN;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgJobPositionName;
	NSNumber * OrgJobTitleId;
	NSString * OrgJobTitleLevelCode;
	NSString * OrgJobTitleNameEN;
	NSString * OrgJobTitleNameVN;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgUnitName;
	NSString * OrgWorkLevelCode;
	NSString * OrgWorkLevelDescription;
	NSNumber * OrgWorkLevelId;
	NSString * OrgWorkLevelName;
	NSString * PITCode;
	NSString * PITDateOfIssue;
	NSString * PITIssuedBy;
	NSDate * PassportDateOfExpire;
	NSDate * PassportDateOfIssue;
	NSString * PassportNo;
	NSString * PassportPlaceOfIssue;
	NSString * PermanentAddress;
	NSNumber * PermanentAddressId;
	NSString * PositionName;
	NSString * PositionNameEN;
	NSDate * ProbationaryDate;
	NSDate * ProbationaryEndDate;
	NSNumber * ProbationarySalary;
	NSString * Religion;
	NSNumber * ReligionId;
	NSString * TemporaryAddress;
	NSNumber * TemporaryAddressId;
	NSString * TimeWorkingFormCode;
	NSNumber * TimeWorkingFormId;
	NSString * UnitName;
	NSString * UnitNameEN;
	NSString * WorkLevelDescription;
	NSString * WorkLevelName;
	NSString * WorkLevelNameEN;
	NSString * WorkLocation;
	NSDate * WorkingFormImplementationDate;
	NSString * WorkingFormTypeCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpBasicInformation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSNumber * AddressId;
@property (retain) NSString * BankAccountNo;
@property (retain) NSString * BankBranch;
@property (retain) NSString * BankName;
@property (retain) NSDate * BirthDay;
@property (retain) NSString * BirthPlace;
@property (retain) NSNumber * BirthPlaceId;
@property (retain) NSString * CVUrl;
@property (retain) NSString * CodeAndFullName;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ContactAddress;
@property (retain) NSNumber * ContactAddressId;
@property (retain) NSString * CulturalLevel;
@property (retain) NSNumber * CulturalLevelId;
@property (retain) NSNumber * EducationLevelId;
@property (retain) NSString * EducationLevelName;
@property (retain) NSString * EducationLevelNameEN;
@property (retain) NSString * Email;
@property (retain) NSNumber * EmpBasicProfileId;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmpWorkingStatusNameEN;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSString * EmployeeTypeName;
@property (retain) NSString * EmployeeTypeNameEN;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * Ethnicity;
@property (retain) NSNumber * EthnicityId;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * HomePhone;
@property (retain) NSString * HomeTown;
@property (retain) NSNumber * HomeTownId;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsConcurrentPost;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) NSString * LastName;
@property (retain) NSString * MainLanguage;
@property (retain) NSNumber * MainLanguageId;
@property (retain) NSString * Major;
@property (retain) NSNumber * MajorId;
@property (retain) NSNumber * MaritalStatusId;
@property (retain) NSString * MaritalStatusValue;
@property (retain) NSString * MobileNumber;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSString * OfficePhone;
@property (retain) NSDate * OfficialDate;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobNameEN;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSNumber * OrgJobTitleId;
@property (retain) NSString * OrgJobTitleLevelCode;
@property (retain) NSString * OrgJobTitleNameEN;
@property (retain) NSString * OrgJobTitleNameVN;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitName;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSString * OrgWorkLevelDescription;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * OrgWorkLevelName;
@property (retain) NSString * PITCode;
@property (retain) NSString * PITDateOfIssue;
@property (retain) NSString * PITIssuedBy;
@property (retain) NSDate * PassportDateOfExpire;
@property (retain) NSDate * PassportDateOfIssue;
@property (retain) NSString * PassportNo;
@property (retain) NSString * PassportPlaceOfIssue;
@property (retain) NSString * PermanentAddress;
@property (retain) NSNumber * PermanentAddressId;
@property (retain) NSString * PositionName;
@property (retain) NSString * PositionNameEN;
@property (retain) NSDate * ProbationaryDate;
@property (retain) NSDate * ProbationaryEndDate;
@property (retain) NSNumber * ProbationarySalary;
@property (retain) NSString * Religion;
@property (retain) NSNumber * ReligionId;
@property (retain) NSString * TemporaryAddress;
@property (retain) NSNumber * TemporaryAddressId;
@property (retain) NSString * TimeWorkingFormCode;
@property (retain) NSNumber * TimeWorkingFormId;
@property (retain) NSString * UnitName;
@property (retain) NSString * UnitNameEN;
@property (retain) NSString * WorkLevelDescription;
@property (retain) NSString * WorkLevelName;
@property (retain) NSString * WorkLevelNameEN;
@property (retain) NSString * WorkLocation;
@property (retain) NSDate * WorkingFormImplementationDate;
@property (retain) NSString * WorkingFormTypeCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_EmpProfileQualification : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileQualification;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_EmpProfileQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileQualification:(empl_tns1_V_EmpProfileQualification *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileQualification;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpProfileWorkingExperience : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompanyName;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileWorkingExperienceId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	USBoolean * IsInternal;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * NotableAchievements;
	NSString * Note;
	NSNumber * OrgProjectTypeId;
	NSString * OrgProjectTypeName;
	NSString * OrgTimeInChargeDescription;
	NSNumber * OrgTimeInChargeId;
	NSString * ProjectName;
	NSString * RatingDescription;
	NSNumber * RatingValue;
	NSNumber * RelateRowId;
	NSString * Responsibilities;
	NSString * Role;
	NSDate * StartDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpProfileWorkingExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompanyName;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileWorkingExperienceId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInternal;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * NotableAchievements;
@property (retain) NSString * Note;
@property (retain) NSNumber * OrgProjectTypeId;
@property (retain) NSString * OrgProjectTypeName;
@property (retain) NSString * OrgTimeInChargeDescription;
@property (retain) NSNumber * OrgTimeInChargeId;
@property (retain) NSString * ProjectName;
@property (retain) NSString * RatingDescription;
@property (retain) NSNumber * RatingValue;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * Responsibilities;
@property (retain) NSString * Role;
@property (retain) NSDate * StartDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_EmpProfileWorkingExperience : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileWorkingExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_EmpProfileWorkingExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileWorkingExperience:(empl_tns1_V_EmpProfileWorkingExperience *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileWorkingExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_ESS_SYS_PROFILE_TREE : NSObject {
	
/* elements */
	NSString * ACTION;
	NSString * CODE;
	NSNumber * COMPANY_ID;
	NSString * CONTROLLER;
	NSNumber * ID_;
	NSNumber * M_ORDER;
	NSString * NAME_EN;
	NSString * NAME_VN;
	NSString * PARAMETERS;
	NSString * PARENT_CODE;
	NSNumber * PRIORITY;
	NSString * TYPE;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_ESS_SYS_PROFILE_TREE *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ACTION;
@property (retain) NSString * CODE;
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSString * CONTROLLER;
@property (retain) NSNumber * ID_;
@property (retain) NSNumber * M_ORDER;
@property (retain) NSString * NAME_EN;
@property (retain) NSString * NAME_VN;
@property (retain) NSString * PARAMETERS;
@property (retain) NSString * PARENT_CODE;
@property (retain) NSNumber * PRIORITY;
@property (retain) NSString * TYPE;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_ESS_SYS_PROFILE_TREE : NSObject {
	
/* elements */
	NSMutableArray *V_ESS_SYS_PROFILE_TREE;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_ESS_SYS_PROFILE_TREE *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ESS_SYS_PROFILE_TREE:(empl_tns1_V_ESS_SYS_PROFILE_TREE *)toAdd;
@property (readonly) NSMutableArray * V_ESS_SYS_PROFILE_TREE;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_TB_ESS_EMPLOYEE_GROUP_MAPPING : NSObject {
	
/* elements */
	NSNumber * COMPANY_ID;
	NSNumber * CREATED_BY;
	NSDate * CREATED_DATE;
	NSString * EMPLOYEE_CODE;
	NSString * EMPLOYEE_GROUP_CODE;
	NSString * EMPLOYEE_GROUP_TYPE;
	NSNumber * EMPLOYEE_ID;
	USBoolean * IS_DELETED;
	NSString * MAPPING_DESC;
	NSDate * MAPPING_IN_DATE;
	NSDate * MAPPING_OUT_DATE;
	NSNumber * MODIFIED_BY;
	NSDate * MODIFIED_DATE;
	NSString * MODULE_CODE;
	NSNumber * TB_ESS_EMPLOYEE_GROUP_MAPPING_ID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_TB_ESS_EMPLOYEE_GROUP_MAPPING *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSNumber * CREATED_BY;
@property (retain) NSDate * CREATED_DATE;
@property (retain) NSString * EMPLOYEE_CODE;
@property (retain) NSString * EMPLOYEE_GROUP_CODE;
@property (retain) NSString * EMPLOYEE_GROUP_TYPE;
@property (retain) NSNumber * EMPLOYEE_ID;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSString * MAPPING_DESC;
@property (retain) NSDate * MAPPING_IN_DATE;
@property (retain) NSDate * MAPPING_OUT_DATE;
@property (retain) NSNumber * MODIFIED_BY;
@property (retain) NSDate * MODIFIED_DATE;
@property (retain) NSString * MODULE_CODE;
@property (retain) NSNumber * TB_ESS_EMPLOYEE_GROUP_MAPPING_ID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfTB_ESS_EMPLOYEE_GROUP_MAPPING : NSObject {
	
/* elements */
	NSMutableArray *TB_ESS_EMPLOYEE_GROUP_MAPPING;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfTB_ESS_EMPLOYEE_GROUP_MAPPING *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTB_ESS_EMPLOYEE_GROUP_MAPPING:(empl_tns1_TB_ESS_EMPLOYEE_GROUP_MAPPING *)toAdd;
@property (readonly) NSMutableArray * TB_ESS_EMPLOYEE_GROUP_MAPPING;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_ESS_EMPLOYEE_GROUP_MAPPING : NSObject {
	
/* elements */
	NSString * Address;
	NSNumber * AddressId;
	NSString * BankAccountNo;
	NSString * BankBranch;
	NSString * BankName;
	NSDate * BirthDay;
	NSString * BirthPlace;
	NSNumber * BirthPlaceId;
	NSString * CVUrl;
	NSString * CodeAndFullName;
	NSNumber * CompanyId;
	NSString * ContactAddress;
	NSNumber * ContactAddressId;
	NSString * CulturalLevel;
	NSNumber * CulturalLevelId;
	NSString * EMPLOYEE_GROUP_CODE;
	NSString * EMPLOYEE_GROUP_NAME_EN;
	NSString * EMPLOYEE_GROUP_NAME_VN;
	NSString * EMPLOYEE_GROUP_TYPE;
	NSNumber * EducationLevelId;
	NSString * EducationLevelName;
	NSString * Email;
	NSNumber * EmpBasicProfileId;
	NSString * EmpWorkingStatusCode;
	NSString * EmpWorkingStatusName;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EmployeeTypeId;
	NSString * EmployeeTypeName;
	NSDate * EntryDate;
	NSString * Ethnicity;
	NSNumber * EthnicityId;
	NSString * FirstName;
	NSString * FullName;
	USBoolean * Gender;
	NSString * HomePhone;
	NSString * HomeTown;
	NSNumber * HomeTownId;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	NSString * ImageUrl;
	USBoolean * IsConcurrentPost;
	USBoolean * IsDeleted;
	USBoolean * IsOnlyYearOfBirthday;
	NSString * LastName;
	NSString * MAPPING_DESC;
	NSDate * MAPPING_IN_DATE;
	NSDate * MAPPING_OUT_DATE;
	NSString * MODULE_CODE;
	NSString * MainLanguage;
	NSNumber * MainLanguageId;
	NSString * Major;
	NSNumber * MajorId;
	NSNumber * MaritalStatusId;
	NSString * MaritalStatusValue;
	NSString * MobileNumber;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSString * OfficePhone;
	NSDate * OfficialDate;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgJobPositionName;
	NSNumber * OrgJobTitleId;
	NSString * OrgJobTitleLevelCode;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgUnitName;
	NSString * OrgWorkLevelCode;
	NSString * OrgWorkLevelDescription;
	NSNumber * OrgWorkLevelId;
	NSString * OrgWorkLevelName;
	NSString * PITCode;
	NSString * PITDateOfIssue;
	NSString * PITIssuedBy;
	NSDate * PassportDateOfExpire;
	NSDate * PassportDateOfIssue;
	NSString * PassportNo;
	NSString * PassportPlaceOfIssue;
	NSString * PermanentAddress;
	NSNumber * PermanentAddressId;
	NSString * PositionName;
	NSDate * ProbationaryDate;
	NSNumber * ProbationarySalary;
	NSString * Religion;
	NSNumber * ReligionId;
	NSNumber * TB_ESS_EMPLOYEE_GROUP_MAPPING_ID;
	NSString * TemporaryAddress;
	NSNumber * TemporaryAddressId;
	NSString * TimeWorkingFormCode;
	NSNumber * TimeWorkingFormId;
	NSString * UnitName;
	NSString * WorkLevelDescription;
	NSString * WorkLevelName;
	NSString * WorkLocation;
	NSDate * WorkingFormImplementationDate;
	NSString * WorkingFormTypeCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_ESS_EMPLOYEE_GROUP_MAPPING *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSNumber * AddressId;
@property (retain) NSString * BankAccountNo;
@property (retain) NSString * BankBranch;
@property (retain) NSString * BankName;
@property (retain) NSDate * BirthDay;
@property (retain) NSString * BirthPlace;
@property (retain) NSNumber * BirthPlaceId;
@property (retain) NSString * CVUrl;
@property (retain) NSString * CodeAndFullName;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ContactAddress;
@property (retain) NSNumber * ContactAddressId;
@property (retain) NSString * CulturalLevel;
@property (retain) NSNumber * CulturalLevelId;
@property (retain) NSString * EMPLOYEE_GROUP_CODE;
@property (retain) NSString * EMPLOYEE_GROUP_NAME_EN;
@property (retain) NSString * EMPLOYEE_GROUP_NAME_VN;
@property (retain) NSString * EMPLOYEE_GROUP_TYPE;
@property (retain) NSNumber * EducationLevelId;
@property (retain) NSString * EducationLevelName;
@property (retain) NSString * Email;
@property (retain) NSNumber * EmpBasicProfileId;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSString * EmployeeTypeName;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * Ethnicity;
@property (retain) NSNumber * EthnicityId;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * HomePhone;
@property (retain) NSString * HomeTown;
@property (retain) NSNumber * HomeTownId;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsConcurrentPost;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) NSString * LastName;
@property (retain) NSString * MAPPING_DESC;
@property (retain) NSDate * MAPPING_IN_DATE;
@property (retain) NSDate * MAPPING_OUT_DATE;
@property (retain) NSString * MODULE_CODE;
@property (retain) NSString * MainLanguage;
@property (retain) NSNumber * MainLanguageId;
@property (retain) NSString * Major;
@property (retain) NSNumber * MajorId;
@property (retain) NSNumber * MaritalStatusId;
@property (retain) NSString * MaritalStatusValue;
@property (retain) NSString * MobileNumber;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSString * OfficePhone;
@property (retain) NSDate * OfficialDate;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSNumber * OrgJobTitleId;
@property (retain) NSString * OrgJobTitleLevelCode;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitName;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSString * OrgWorkLevelDescription;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * OrgWorkLevelName;
@property (retain) NSString * PITCode;
@property (retain) NSString * PITDateOfIssue;
@property (retain) NSString * PITIssuedBy;
@property (retain) NSDate * PassportDateOfExpire;
@property (retain) NSDate * PassportDateOfIssue;
@property (retain) NSString * PassportNo;
@property (retain) NSString * PassportPlaceOfIssue;
@property (retain) NSString * PermanentAddress;
@property (retain) NSNumber * PermanentAddressId;
@property (retain) NSString * PositionName;
@property (retain) NSDate * ProbationaryDate;
@property (retain) NSNumber * ProbationarySalary;
@property (retain) NSString * Religion;
@property (retain) NSNumber * ReligionId;
@property (retain) NSNumber * TB_ESS_EMPLOYEE_GROUP_MAPPING_ID;
@property (retain) NSString * TemporaryAddress;
@property (retain) NSNumber * TemporaryAddressId;
@property (retain) NSString * TimeWorkingFormCode;
@property (retain) NSNumber * TimeWorkingFormId;
@property (retain) NSString * UnitName;
@property (retain) NSString * WorkLevelDescription;
@property (retain) NSString * WorkLevelName;
@property (retain) NSString * WorkLocation;
@property (retain) NSDate * WorkingFormImplementationDate;
@property (retain) NSString * WorkingFormTypeCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_ESS_EMPLOYEE_GROUP_MAPPING : NSObject {
	
/* elements */
	NSMutableArray *V_ESS_EMPLOYEE_GROUP_MAPPING;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_ESS_EMPLOYEE_GROUP_MAPPING *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ESS_EMPLOYEE_GROUP_MAPPING:(empl_tns1_V_ESS_EMPLOYEE_GROUP_MAPPING *)toAdd;
@property (readonly) NSMutableArray * V_ESS_EMPLOYEE_GROUP_MAPPING;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_ESS_CLOG_EMPLOYEE_GROUP : NSObject {
	
/* elements */
	NSNumber * CLOG_EMPLOYEE_GROUP_ID;
	NSNumber * COMPANY_ID;
	NSNumber * CREATED_BY;
	NSDate * CREATED_DATE;
	NSString * EMPLOYEE_GROUP_CODE;
	NSString * EMPLOYEE_GROUP_DESC;
	NSString * EMPLOYEE_GROUP_NAME_EN;
	NSString * EMPLOYEE_GROUP_NAME_VN;
	NSString * EMPLOYEE_GROUP_TYPE;
	NSString * HIERARCHICAL_CODE;
	USBoolean * IS_DELETED;
	NSString * ITEM_TYPE_CODE;
	NSNumber * MODIFIED_BY;
	NSDate * MODIFIED_DATE;
	NSString * MODULE_CODE;
	NSNumber * ORDER_BY;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_ESS_CLOG_EMPLOYEE_GROUP *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLOG_EMPLOYEE_GROUP_ID;
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSNumber * CREATED_BY;
@property (retain) NSDate * CREATED_DATE;
@property (retain) NSString * EMPLOYEE_GROUP_CODE;
@property (retain) NSString * EMPLOYEE_GROUP_DESC;
@property (retain) NSString * EMPLOYEE_GROUP_NAME_EN;
@property (retain) NSString * EMPLOYEE_GROUP_NAME_VN;
@property (retain) NSString * EMPLOYEE_GROUP_TYPE;
@property (retain) NSString * HIERARCHICAL_CODE;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSString * ITEM_TYPE_CODE;
@property (retain) NSNumber * MODIFIED_BY;
@property (retain) NSDate * MODIFIED_DATE;
@property (retain) NSString * MODULE_CODE;
@property (retain) NSNumber * ORDER_BY;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_ESS_CLOG_EMPLOYEE_GROUP : NSObject {
	
/* elements */
	NSMutableArray *V_ESS_CLOG_EMPLOYEE_GROUP;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_ESS_CLOG_EMPLOYEE_GROUP *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ESS_CLOG_EMPLOYEE_GROUP:(empl_tns1_V_ESS_CLOG_EMPLOYEE_GROUP *)toAdd;
@property (readonly) NSMutableArray * V_ESS_CLOG_EMPLOYEE_GROUP;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CBSalaryGrade : NSObject {
	
/* elements */
	empl_tns1_ArrayOfCBSalaryByCoefficient * CBSalaryByCoefficients;
	NSNumber * CBSalaryGradeId;
	NSString * CBSalaryGradeValue;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Order;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CBSalaryGrade *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfCBSalaryByCoefficient * CBSalaryByCoefficients;
@property (retain) NSNumber * CBSalaryGradeId;
@property (retain) NSString * CBSalaryGradeValue;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Order;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CBSalaryLevel : NSObject {
	
/* elements */
	empl_tns1_ArrayOfCBSalaryByCoefficient * CBSalaryByCoefficients;
	NSNumber * CBSalaryLevelId;
	NSString * CBSalaryLevelValue;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Order;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CBSalaryLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfCBSalaryByCoefficient * CBSalaryByCoefficients;
@property (retain) NSNumber * CBSalaryLevelId;
@property (retain) NSString * CBSalaryLevelValue;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Order;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CBSalaryByCoefficient : NSObject {
	
/* elements */
	NSNumber * CBSalaryByCoefficientId;
	empl_tns1_CBSalaryGrade * CBSalaryGrade;
	NSNumber * CBSalaryGradeId;
	empl_tns1_CBSalaryLevel * CBSalaryLevel;
	NSNumber * CBSalaryLevelId;
	empl_tns1_CBSalaryScale * CBSalaryScale;
	NSNumber * CBSalaryScaleId;
	NSNumber * Coefficient;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * SalaryValue;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CBSalaryByCoefficient *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBSalaryByCoefficientId;
@property (retain) empl_tns1_CBSalaryGrade * CBSalaryGrade;
@property (retain) NSNumber * CBSalaryGradeId;
@property (retain) empl_tns1_CBSalaryLevel * CBSalaryLevel;
@property (retain) NSNumber * CBSalaryLevelId;
@property (retain) empl_tns1_CBSalaryScale * CBSalaryScale;
@property (retain) NSNumber * CBSalaryScaleId;
@property (retain) NSNumber * Coefficient;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * SalaryValue;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfCBSalaryByCoefficient : NSObject {
	
/* elements */
	NSMutableArray *CBSalaryByCoefficient;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfCBSalaryByCoefficient *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBSalaryByCoefficient:(empl_tns1_CBSalaryByCoefficient *)toAdd;
@property (readonly) NSMutableArray * CBSalaryByCoefficient;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_CBSalaryScale : NSObject {
	
/* elements */
	empl_tns1_ArrayOfCBSalaryByCoefficient * CBSalaryByCoefficients;
	NSNumber * CBSalaryScaleId;
	NSString * CBSalaryScaleValue;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Order;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_CBSalaryScale *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfCBSalaryByCoefficient * CBSalaryByCoefficients;
@property (retain) NSNumber * CBSalaryScaleId;
@property (retain) NSString * CBSalaryScaleValue;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Order;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfCBSalaryScale : NSObject {
	
/* elements */
	NSMutableArray *CBSalaryScale;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfCBSalaryScale *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBSalaryScale:(empl_tns1_CBSalaryScale *)toAdd;
@property (readonly) NSMutableArray * CBSalaryScale;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfCBSalaryLevel : NSObject {
	
/* elements */
	NSMutableArray *CBSalaryLevel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfCBSalaryLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBSalaryLevel:(empl_tns1_CBSalaryLevel *)toAdd;
@property (readonly) NSMutableArray * CBSalaryLevel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfCBSalaryGrade : NSObject {
	
/* elements */
	NSMutableArray *CBSalaryGrade;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfCBSalaryGrade *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBSalaryGrade:(empl_tns1_CBSalaryGrade *)toAdd;
@property (readonly) NSMutableArray * CBSalaryGrade;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_CBSummarization : NSObject {
	
/* elements */
	NSNumber * CBSalaryByCoefficientId;
	NSNumber * CBSalaryGradeId;
	NSString * CBSalaryGradeValue;
	NSNumber * CBSalaryLevelId;
	NSString * CBSalaryLevelValue;
	NSNumber * CBSalaryScaleId;
	NSString * CBSalaryScaleValue;
	NSNumber * Coefficient;
	NSNumber * CompanyId;
	NSString * GradeCoefficient;
	NSNumber * SalaryValue;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_CBSummarization *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBSalaryByCoefficientId;
@property (retain) NSNumber * CBSalaryGradeId;
@property (retain) NSString * CBSalaryGradeValue;
@property (retain) NSNumber * CBSalaryLevelId;
@property (retain) NSString * CBSalaryLevelValue;
@property (retain) NSNumber * CBSalaryScaleId;
@property (retain) NSString * CBSalaryScaleValue;
@property (retain) NSNumber * Coefficient;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * GradeCoefficient;
@property (retain) NSNumber * SalaryValue;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_CBSummarization : NSObject {
	
/* elements */
	NSMutableArray *V_CBSummarization;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_CBSummarization *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_CBSummarization:(empl_tns1_V_CBSummarization *)toAdd;
@property (readonly) NSMutableArray * V_CBSummarization;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpProfileAllowance : NSObject {
	
/* elements */
	NSDate * ActivedDate;
	NSString * AllowanceCode;
	NSString * AllowanceName;
	NSNumber * AllowanceValue;
	NSNumber * CLogCurrency;
	NSString * CLogCurrencyName;
	NSString * CLogCurrencyNameEN;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpProfileAllowanceId;
	NSNumber * EmployeeId;
	USBoolean * IsActived;
	USBoolean * IsDeactived;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpProfileAllowance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ActivedDate;
@property (retain) NSString * AllowanceCode;
@property (retain) NSString * AllowanceName;
@property (retain) NSNumber * AllowanceValue;
@property (retain) NSNumber * CLogCurrency;
@property (retain) NSString * CLogCurrencyName;
@property (retain) NSString * CLogCurrencyNameEN;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileAllowanceId;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeactived;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_EmpProfileAllowance : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileAllowance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_EmpProfileAllowance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileAllowance:(empl_tns1_V_EmpProfileAllowance *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileAllowance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpProfileBaseSalary : NSObject {
	
/* elements */
	NSDate * ActivedDate;
	NSString * BasicPayCode;
	NSNumber * CBSalaryCofficientId;
	NSNumber * CBSalaryGradeId;
	NSString * CBSalaryGradeValue;
	NSNumber * CBSalaryLevelId;
	NSString * CBSalaryLevelValue;
	NSNumber * CBSalaryScaleId;
	NSString * CBSalaryScaleValue;
	NSNumber * Coefficient;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpProfileBaseSalaryId;
	NSNumber * EmployeeId;
	NSString * GradeCoefficient;
	USBoolean * IsActived;
	USBoolean * IsDeactived;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * NonPayScaleSalaryValue;
	NSString * Note;
	NSNumber * PayScaleSalaryValue;
	NSString * SalaryCode;
	NSNumber * SalaryId;
	NSString * SalaryName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpProfileBaseSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ActivedDate;
@property (retain) NSString * BasicPayCode;
@property (retain) NSNumber * CBSalaryCofficientId;
@property (retain) NSNumber * CBSalaryGradeId;
@property (retain) NSString * CBSalaryGradeValue;
@property (retain) NSNumber * CBSalaryLevelId;
@property (retain) NSString * CBSalaryLevelValue;
@property (retain) NSNumber * CBSalaryScaleId;
@property (retain) NSString * CBSalaryScaleValue;
@property (retain) NSNumber * Coefficient;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileBaseSalaryId;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * GradeCoefficient;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeactived;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NonPayScaleSalaryValue;
@property (retain) NSString * Note;
@property (retain) NSNumber * PayScaleSalaryValue;
@property (retain) NSString * SalaryCode;
@property (retain) NSNumber * SalaryId;
@property (retain) NSString * SalaryName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_EmpProfileBaseSalary : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileBaseSalary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_EmpProfileBaseSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileBaseSalary:(empl_tns1_V_EmpProfileBaseSalary *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileBaseSalary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpProfileTotalIncome : NSObject {
	
/* elements */
	NSDate * CompanyId;
	NSDate * CreatedDate;
	NSNumber * DependancyDeduction;
	NSNumber * EmpProfileTotalIncomeId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * FullName;
	NSNumber * GrossPayment;
	NSString * ImageUrl;
	NSDate * ModifiedDate;
	NSNumber * Month;
	NSNumber * NetPayment;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobNameEN;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgJobPositionName;
	NSString * OrgJobPositionNameEN;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgUnitName;
	NSString * OrgUnitNameEN;
	NSNumber * PIT;
	NSNumber * TaxableAmount;
	NSNumber * TotalDeduction;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpProfileTotalIncome *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * DependancyDeduction;
@property (retain) NSNumber * EmpProfileTotalIncomeId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * FullName;
@property (retain) NSNumber * GrossPayment;
@property (retain) NSString * ImageUrl;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Month;
@property (retain) NSNumber * NetPayment;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobNameEN;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSString * OrgJobPositionNameEN;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitName;
@property (retain) NSString * OrgUnitNameEN;
@property (retain) NSNumber * PIT;
@property (retain) NSNumber * TaxableAmount;
@property (retain) NSNumber * TotalDeduction;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_EmpProfileTotalIncome : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileTotalIncome;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_EmpProfileTotalIncome *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileTotalIncome:(empl_tns1_V_EmpProfileTotalIncome *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileTotalIncome;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_SP_GET_PERMIS_EMP_PROF_LAYER_Result : NSObject {
	
/* elements */
	NSString * SysEmpProfileGroupLayerCode;
	NSString * SysEmpProfileGroupLayerIcon;
	NSNumber * SysEmpProfileGroupLayerId;
	NSString * SysEmpProfileGroupLayerName;
	NSString * SysEmpProfileGroupLayerUrl;
	NSString * SysEmpProfileLayerCode;
	NSString * SysEmpProfileLayerIcon;
	NSNumber * SysEmpProfileLayerId;
	NSString * SysEmpProfileLayerName;
	NSString * SysEmpProfileLayerUrl;
	NSNumber * SysEmpProfileLayerUrlId;
	NSString * SysEmpProfileLayerUrlType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_SP_GET_PERMIS_EMP_PROF_LAYER_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * SysEmpProfileGroupLayerCode;
@property (retain) NSString * SysEmpProfileGroupLayerIcon;
@property (retain) NSNumber * SysEmpProfileGroupLayerId;
@property (retain) NSString * SysEmpProfileGroupLayerName;
@property (retain) NSString * SysEmpProfileGroupLayerUrl;
@property (retain) NSString * SysEmpProfileLayerCode;
@property (retain) NSString * SysEmpProfileLayerIcon;
@property (retain) NSNumber * SysEmpProfileLayerId;
@property (retain) NSString * SysEmpProfileLayerName;
@property (retain) NSString * SysEmpProfileLayerUrl;
@property (retain) NSNumber * SysEmpProfileLayerUrlId;
@property (retain) NSString * SysEmpProfileLayerUrlType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfSP_GET_PERMIS_EMP_PROF_LAYER_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_GET_PERMIS_EMP_PROF_LAYER_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfSP_GET_PERMIS_EMP_PROF_LAYER_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_GET_PERMIS_EMP_PROF_LAYER_Result:(empl_tns1_SP_GET_PERMIS_EMP_PROF_LAYER_Result *)toAdd;
@property (readonly) NSMutableArray * SP_GET_PERMIS_EMP_PROF_LAYER_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_SysEmpProfileGroupAndLayer : NSObject {
	
/* elements */
	NSNumber * SysEmpProfileGroupLayerCompanyId;
	NSString * SysEmpProfileGroupLayerIcon;
	NSNumber * SysEmpProfileGroupLayerId;
	USBoolean * SysEmpProfileGroupLayerIsDeleted;
	USBoolean * SysEmpProfileGroupLayerIsVisible;
	NSString * SysEmpProfileGroupLayerName;
	NSNumber * SysEmpProfileGroupLayerPriority;
	NSString * SysEmpProfileGroupLayerUrl;
	NSNumber * SysEmpProfileLayerCompanyId;
	NSString * SysEmpProfileLayerIcon;
	NSNumber * SysEmpProfileLayerId;
	USBoolean * SysEmpProfileLayerIsDeleted;
	USBoolean * SysEmpProfileLayerIsVisible;
	NSString * SysEmpProfileLayerName;
	NSNumber * SysEmpProfileLayerPriority;
	NSString * SysEmpProfileLayerUrl;
	NSNumber * SysEmpProfileLayerUrlCompanyId;
	NSNumber * SysEmpProfileLayerUrlId;
	USBoolean * SysEmpProfileLayerUrlIsDeleted;
	NSString * SysEmpProfileLayerUrlType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_SysEmpProfileGroupAndLayer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * SysEmpProfileGroupLayerCompanyId;
@property (retain) NSString * SysEmpProfileGroupLayerIcon;
@property (retain) NSNumber * SysEmpProfileGroupLayerId;
@property (retain) USBoolean * SysEmpProfileGroupLayerIsDeleted;
@property (retain) USBoolean * SysEmpProfileGroupLayerIsVisible;
@property (retain) NSString * SysEmpProfileGroupLayerName;
@property (retain) NSNumber * SysEmpProfileGroupLayerPriority;
@property (retain) NSString * SysEmpProfileGroupLayerUrl;
@property (retain) NSNumber * SysEmpProfileLayerCompanyId;
@property (retain) NSString * SysEmpProfileLayerIcon;
@property (retain) NSNumber * SysEmpProfileLayerId;
@property (retain) USBoolean * SysEmpProfileLayerIsDeleted;
@property (retain) USBoolean * SysEmpProfileLayerIsVisible;
@property (retain) NSString * SysEmpProfileLayerName;
@property (retain) NSNumber * SysEmpProfileLayerPriority;
@property (retain) NSString * SysEmpProfileLayerUrl;
@property (retain) NSNumber * SysEmpProfileLayerUrlCompanyId;
@property (retain) NSNumber * SysEmpProfileLayerUrlId;
@property (retain) USBoolean * SysEmpProfileLayerUrlIsDeleted;
@property (retain) NSString * SysEmpProfileLayerUrlType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_SysEmpProfileGroupAndLayer : NSObject {
	
/* elements */
	NSMutableArray *V_SysEmpProfileGroupAndLayer;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_SysEmpProfileGroupAndLayer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_SysEmpProfileGroupAndLayer:(empl_tns1_V_SysEmpProfileGroupAndLayer *)toAdd;
@property (readonly) NSMutableArray * V_SysEmpProfileGroupAndLayer;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_SP_GET_EMP_PROF_LAYER_PERMIS_Result : NSObject {
	
/* elements */
	NSString * SysEmpProfileGroupLayerCode;
	NSString * SysEmpProfileGroupLayerIcon;
	NSNumber * SysEmpProfileGroupLayerId;
	USBoolean * SysEmpProfileGroupLayerIsVisible;
	NSString * SysEmpProfileGroupLayerName;
	NSNumber * SysEmpProfileGroupLayerPriority;
	NSString * SysEmpProfileGroupLayerUrl;
	NSString * SysEmpProfileLayerCode;
	NSString * SysEmpProfileLayerIcon;
	NSNumber * SysEmpProfileLayerId;
	USBoolean * SysEmpProfileLayerIsVisible;
	NSString * SysEmpProfileLayerName;
	NSNumber * SysEmpProfileLayerPriority;
	NSString * SysEmpProfileLayerUrl;
	NSNumber * SysEmpProfileLayerUrlId;
	NSString * SysEmpProfileLayerUrlType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_SP_GET_EMP_PROF_LAYER_PERMIS_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * SysEmpProfileGroupLayerCode;
@property (retain) NSString * SysEmpProfileGroupLayerIcon;
@property (retain) NSNumber * SysEmpProfileGroupLayerId;
@property (retain) USBoolean * SysEmpProfileGroupLayerIsVisible;
@property (retain) NSString * SysEmpProfileGroupLayerName;
@property (retain) NSNumber * SysEmpProfileGroupLayerPriority;
@property (retain) NSString * SysEmpProfileGroupLayerUrl;
@property (retain) NSString * SysEmpProfileLayerCode;
@property (retain) NSString * SysEmpProfileLayerIcon;
@property (retain) NSNumber * SysEmpProfileLayerId;
@property (retain) USBoolean * SysEmpProfileLayerIsVisible;
@property (retain) NSString * SysEmpProfileLayerName;
@property (retain) NSNumber * SysEmpProfileLayerPriority;
@property (retain) NSString * SysEmpProfileLayerUrl;
@property (retain) NSNumber * SysEmpProfileLayerUrlId;
@property (retain) NSString * SysEmpProfileLayerUrlType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfSP_GET_EMP_PROF_LAYER_PERMIS_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_GET_EMP_PROF_LAYER_PERMIS_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfSP_GET_EMP_PROF_LAYER_PERMIS_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_GET_EMP_PROF_LAYER_PERMIS_Result:(empl_tns1_SP_GET_EMP_PROF_LAYER_PERMIS_Result *)toAdd;
@property (readonly) NSMutableArray * SP_GET_EMP_PROF_LAYER_PERMIS_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpBasicProfile : NSObject {
	
/* elements */
	NSString * BankAccountNo;
	NSString * BankBranch;
	NSString * BankBranchName;
	NSString * BankName;
	NSDate * BirthDay;
	NSString * BirthPlace;
	NSNumber * BirthPlaceId;
	NSNumber * CLogBankBranchId;
	NSNumber * CLogBankId;
	NSString * CVUrl;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * CulturalLevel;
	NSNumber * CulturalLevelId;
	NSString * EducationLevel;
	NSNumber * EducationLevelId;
	NSDate * EmpBasicProfileCreateDate;
	NSNumber * EmpBasicProfileId;
	USBoolean * EmpBasicProfileIsDeleted;
	NSDate * EmpBasicProfileModifiedDate;
	NSString * EmpWorkingStatusCode;
	NSNumber * EmpWorkingStatusId;
	NSString * EmpWorkingStatusName;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EmployeeTypeId;
	NSDate * EndWorkingDate;
	NSDate * EntryDate;
	NSString * Ethnicity;
	NSNumber * EthnicityId;
	NSString * FirstName;
	NSString * FullName;
	USBoolean * Gender;
	NSString * HomeTown;
	NSNumber * HomeTownId;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	NSString * ImageUrl;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	USBoolean * IsOnlyYearOfBirthday;
	NSString * LastName;
	NSString * MainLanguage;
	NSNumber * MainLanguageId;
	NSString * Major;
	NSNumber * MajorId;
	NSString * MaritalStatus;
	NSNumber * MaritalStatusId;
	NSDate * ModifiedDate;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSDate * OfficialDate;
	NSString * PITCode;
	NSString * PITIssuedBy;
	NSDate * PassportDateOfExpire;
	NSDate * PassportDateOfIssue;
	NSString * PassportNo;
	NSString * PassportPlaceOfIssue;
	NSDate * ProbationaryDate;
	NSNumber * ProbationarySalary;
	NSString * Religion;
	NSNumber * ReligionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpBasicProfile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * BankAccountNo;
@property (retain) NSString * BankBranch;
@property (retain) NSString * BankBranchName;
@property (retain) NSString * BankName;
@property (retain) NSDate * BirthDay;
@property (retain) NSString * BirthPlace;
@property (retain) NSNumber * BirthPlaceId;
@property (retain) NSNumber * CLogBankBranchId;
@property (retain) NSNumber * CLogBankId;
@property (retain) NSString * CVUrl;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * CulturalLevel;
@property (retain) NSNumber * CulturalLevelId;
@property (retain) NSString * EducationLevel;
@property (retain) NSNumber * EducationLevelId;
@property (retain) NSDate * EmpBasicProfileCreateDate;
@property (retain) NSNumber * EmpBasicProfileId;
@property (retain) USBoolean * EmpBasicProfileIsDeleted;
@property (retain) NSDate * EmpBasicProfileModifiedDate;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSNumber * EmpWorkingStatusId;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSDate * EndWorkingDate;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * Ethnicity;
@property (retain) NSNumber * EthnicityId;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * HomeTown;
@property (retain) NSNumber * HomeTownId;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) NSString * LastName;
@property (retain) NSString * MainLanguage;
@property (retain) NSNumber * MainLanguageId;
@property (retain) NSString * Major;
@property (retain) NSNumber * MajorId;
@property (retain) NSString * MaritalStatus;
@property (retain) NSNumber * MaritalStatusId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSDate * OfficialDate;
@property (retain) NSString * PITCode;
@property (retain) NSString * PITIssuedBy;
@property (retain) NSDate * PassportDateOfExpire;
@property (retain) NSDate * PassportDateOfIssue;
@property (retain) NSString * PassportNo;
@property (retain) NSString * PassportPlaceOfIssue;
@property (retain) NSDate * ProbationaryDate;
@property (retain) NSNumber * ProbationarySalary;
@property (retain) NSString * Religion;
@property (retain) NSNumber * ReligionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpProfilePersonality : NSObject {
	
/* elements */
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CLogPersonalityId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * Description;
	NSNumber * EmpProfilePersonalityId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * RelateRowId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpProfilePersonality *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CLogPersonalityId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) NSNumber * EmpProfilePersonalityId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * RelateRowId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_EmpProfilePersonality : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfilePersonality;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_EmpProfilePersonality *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfilePersonality:(empl_tns1_V_EmpProfilePersonality *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfilePersonality;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpProfileHobby : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CLogHobbyId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * Description;
	NSNumber * EmpProfileHobbyId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * RelateRowId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpProfileHobby *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CLogHobbyId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) NSNumber * EmpProfileHobbyId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * RelateRowId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_EmpProfileHobby : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileHobby;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_EmpProfileHobby *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileHobby:(empl_tns1_V_EmpProfileHobby *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileHobby;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpSocialInsuranceSalary : NSObject {
	
/* elements */
	NSString * CLogCurrencyCode;
	NSNumber * CLogCurrencyId;
	NSString * CLogCurrencyName;
	NSNumber * CLogCurrencyRateId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * DecisionCode;
	NSString * DecisionName;
	NSNumber * EmpSocialInsuranceSalaryId;
	NSNumber * EmployeeId;
	NSDate * ExpirationDate;
	NSString * FileDecisionUrl;
	NSDate * ImplementationDate;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	USBoolean * IsIncrease;
	USBoolean * IsRecall;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * Reason;
	NSDate * RecallMonth;
	NSDate * SocialInsuranceDate;
	NSString * cEmpSISalary;
	NSString * cEmpSISalaryCoeficient;
	NSString * cEmpSISalaryGrade;
	NSNumber * nEmpSISalary;
	NSNumber * nEmpSISalaryCoeficient;
	NSNumber * nEmpSISalaryGrade;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpSocialInsuranceSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogCurrencyCode;
@property (retain) NSNumber * CLogCurrencyId;
@property (retain) NSString * CLogCurrencyName;
@property (retain) NSNumber * CLogCurrencyRateId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DecisionCode;
@property (retain) NSString * DecisionName;
@property (retain) NSNumber * EmpSocialInsuranceSalaryId;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSString * FileDecisionUrl;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsIncrease;
@property (retain) USBoolean * IsRecall;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * Reason;
@property (retain) NSDate * RecallMonth;
@property (retain) NSDate * SocialInsuranceDate;
@property (retain) NSString * cEmpSISalary;
@property (retain) NSString * cEmpSISalaryCoeficient;
@property (retain) NSString * cEmpSISalaryGrade;
@property (retain) NSNumber * nEmpSISalary;
@property (retain) NSNumber * nEmpSISalaryCoeficient;
@property (retain) NSNumber * nEmpSISalaryGrade;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_EmpSocialInsuranceSalary : NSObject {
	
/* elements */
	NSMutableArray *V_EmpSocialInsuranceSalary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_EmpSocialInsuranceSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpSocialInsuranceSalary:(empl_tns1_V_EmpSocialInsuranceSalary *)toAdd;
@property (readonly) NSMutableArray * V_EmpSocialInsuranceSalary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileBiography : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileBiographyId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	NSString * JobName;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Other;
	NSString * PlaceIn;
	NSNumber * RelateRowId;
	NSDate * StartDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileBiography *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileBiographyId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * JobName;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIn;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSDate * StartDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileBiography : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileBiography;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileBiography *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileBiography:(empl_tns1_EmpProfileBiography *)toAdd;
@property (readonly) NSMutableArray * EmpProfileBiography;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpProfileFamilyRelationship : NSObject {
	
/* elements */
	NSString * Address;
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSDate * BirthDay;
	NSNumber * CBPITDependentId;
	NSNumber * CLogFamilyRelationshipId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DeathDate;
	NSDate * DependentBeginDate;
	NSString * DependentDemonstrative;
	NSDate * DependentEndDate;
	NSNumber * EmpProfileFamilyRelationshipId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * FamilyRelationshipName;
	NSString * FamilyRelationshipNote;
	USBoolean * FamilyRelationshipSex;
	NSString * HomeTown;
	USBoolean * IsDeleted;
	USBoolean * IsDependent;
	USBoolean * IsEmergencyContact;
	NSDate * ModifiedDate;
	USBoolean * Nourish;
	NSString * Occupation;
	NSString * OccupationPlace;
	NSString * PersonFirstName;
	NSString * PersonFullName;
	NSString * PersonLastName;
	NSNumber * PersonSameOccupationId;
	NSString * Phone;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpProfileFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSDate * BirthDay;
@property (retain) NSNumber * CBPITDependentId;
@property (retain) NSNumber * CLogFamilyRelationshipId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DeathDate;
@property (retain) NSDate * DependentBeginDate;
@property (retain) NSString * DependentDemonstrative;
@property (retain) NSDate * DependentEndDate;
@property (retain) NSNumber * EmpProfileFamilyRelationshipId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * FamilyRelationshipName;
@property (retain) NSString * FamilyRelationshipNote;
@property (retain) USBoolean * FamilyRelationshipSex;
@property (retain) NSString * HomeTown;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDependent;
@property (retain) USBoolean * IsEmergencyContact;
@property (retain) NSDate * ModifiedDate;
@property (retain) USBoolean * Nourish;
@property (retain) NSString * Occupation;
@property (retain) NSString * OccupationPlace;
@property (retain) NSString * PersonFirstName;
@property (retain) NSString * PersonFullName;
@property (retain) NSString * PersonLastName;
@property (retain) NSNumber * PersonSameOccupationId;
@property (retain) NSString * Phone;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_EmpProfileFamilyRelationship : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileFamilyRelationship;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_EmpProfileFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileFamilyRelationship:(empl_tns1_V_EmpProfileFamilyRelationship *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileFamilyRelationship;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpProfileContact : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSString * ContactAddress;
	NSNumber * ContactAddressId;
	NSString * ContactCityCode;
	NSString * ContactCityName;
	NSString * Email;
	NSNumber * EmpProfileContactId;
	NSNumber * EmployeeId;
	NSString * Fax;
	NSString * HomePhone;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	NSString * MobileNumber;
	NSString * OfficePhone;
	NSString * PermanentAddress;
	NSNumber * PermanentAddressId;
	NSString * PermanentCityCode;
	NSString * PermanentCityName;
	NSNumber * RelateRowId;
	NSString * TemporaryAddress;
	NSNumber * TemporaryAddressId;
	NSString * TemporaryCityCode;
	NSNumber * TemporaryCityId;
	NSString * TemporaryCityName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpProfileContact *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ContactAddress;
@property (retain) NSNumber * ContactAddressId;
@property (retain) NSString * ContactCityCode;
@property (retain) NSString * ContactCityName;
@property (retain) NSString * Email;
@property (retain) NSNumber * EmpProfileContactId;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * Fax;
@property (retain) NSString * HomePhone;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * MobileNumber;
@property (retain) NSString * OfficePhone;
@property (retain) NSString * PermanentAddress;
@property (retain) NSNumber * PermanentAddressId;
@property (retain) NSString * PermanentCityCode;
@property (retain) NSString * PermanentCityName;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * TemporaryAddress;
@property (retain) NSNumber * TemporaryAddressId;
@property (retain) NSString * TemporaryCityCode;
@property (retain) NSNumber * TemporaryCityId;
@property (retain) NSString * TemporaryCityName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_EmpProfileContact : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileContact;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_EmpProfileContact *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileContact:(empl_tns1_V_EmpProfileContact *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileContact;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpProfileJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * DecisionFile;
	NSString * DecisionName;
	NSString * DecisionNo;
	NSString * DirectReportToEmployeeCode;
	NSString * DirectReportToEmployeeFullName;
	NSNumber * DirectReportToEmployeeId;
	NSNumber * EmpProfileJobPositionId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ExpirationDate;
	NSString * FirstName;
	NSString * FullName;
	NSDate * ImplementationDate;
	USBoolean * IsActive;
	USBoolean * IsConcurrentPost;
	USBoolean * IsDeleted;
	USBoolean * IsMainPost;
	USBoolean * IsPromoted;
	NSString * JOB_TITLE_LEVEL_NAME_EN;
	NSString * JOB_TITLE_LEVEL_NAME_VN;
	NSString * LastName;
	NSDate * ModifiedDate;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobNameEN;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgJobPositionName;
	NSString * OrgJobPositionNameEN;
	NSString * OrgJobTitleCode;
	NSString * OrgJobTitleGroupCode;
	NSNumber * OrgJobTitleId;
	NSString * OrgJobTitleLevelCode;
	NSString * OrgJobTitleNameEN;
	NSString * OrgJobTitleNameVN;
	NSString * OrgUnitAddress;
	NSString * OrgUnitCode;
	NSString * OrgUnitDescription;
	NSNumber * OrgUnitId;
	NSString * OrgUnitName;
	NSString * OrgUnitNameEN;
	NSString * OrgUnitWorkLocation;
	NSString * OrgWorkLevelCode;
	NSString * OrgWorkLevelDescription;
	NSNumber * OrgWorkLevelId;
	NSString * OrgWorkLevelName;
	NSString * OrgWorkLevelNameEN;
	NSNumber * PercentParticipation;
	NSString * Reason;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpProfileJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DecisionFile;
@property (retain) NSString * DecisionName;
@property (retain) NSString * DecisionNo;
@property (retain) NSString * DirectReportToEmployeeCode;
@property (retain) NSString * DirectReportToEmployeeFullName;
@property (retain) NSNumber * DirectReportToEmployeeId;
@property (retain) NSNumber * EmpProfileJobPositionId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsConcurrentPost;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainPost;
@property (retain) USBoolean * IsPromoted;
@property (retain) NSString * JOB_TITLE_LEVEL_NAME_EN;
@property (retain) NSString * JOB_TITLE_LEVEL_NAME_VN;
@property (retain) NSString * LastName;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobNameEN;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSString * OrgJobPositionNameEN;
@property (retain) NSString * OrgJobTitleCode;
@property (retain) NSString * OrgJobTitleGroupCode;
@property (retain) NSNumber * OrgJobTitleId;
@property (retain) NSString * OrgJobTitleLevelCode;
@property (retain) NSString * OrgJobTitleNameEN;
@property (retain) NSString * OrgJobTitleNameVN;
@property (retain) NSString * OrgUnitAddress;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSString * OrgUnitDescription;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitName;
@property (retain) NSString * OrgUnitNameEN;
@property (retain) NSString * OrgUnitWorkLocation;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSString * OrgWorkLevelDescription;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * OrgWorkLevelName;
@property (retain) NSString * OrgWorkLevelNameEN;
@property (retain) NSNumber * PercentParticipation;
@property (retain) NSString * Reason;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_EmpProfileJobPosition : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_EmpProfileJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileJobPosition:(empl_tns1_V_EmpProfileJobPosition *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpProfileProcessOfWork : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * DecisionFile;
	NSString * DecisionName;
	NSString * DecisionNo;
	NSString * DirectReportToEmployeeCode;
	NSString * DirectReportToEmployeeFullName;
	NSNumber * DirectReportToEmployeeId;
	NSNumber * EmpProfileJobPositionId;
	NSNumber * EmpProfileProcessOfWorkId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ExpirationDate;
	NSString * FirstName;
	NSString * FullName;
	NSDate * ImplementationDate;
	USBoolean * IsActive;
	USBoolean * IsConcurrentPost;
	USBoolean * IsDeleted;
	USBoolean * IsMainPost;
	USBoolean * IsPromoted;
	NSString * JOB_TITLE_LEVEL_NAME_EN;
	NSString * JOB_TITLE_LEVEL_NAME_VN;
	NSString * LastName;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobNameEN;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgJobPositionName;
	NSString * OrgJobPositionNameEN;
	NSString * OrgJobTitleCode;
	NSString * OrgJobTitleGroupCode;
	NSNumber * OrgJobTitleId;
	NSString * OrgJobTitleLevelCode;
	NSString * OrgJobTitleNameEN;
	NSString * OrgJobTitleNameVN;
	NSString * OrgUnitAddress;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgUnitName;
	NSString * OrgUnitNameEN;
	NSString * OrgWorkLevelCode;
	NSString * OrgWorkLevelDescription;
	NSNumber * OrgWorkLevelId;
	NSString * OrgWorkLevelName;
	NSString * OrgWorkLevelNameEN;
	NSNumber * PercentParticipation;
	NSString * Reason;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpProfileProcessOfWork *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DecisionFile;
@property (retain) NSString * DecisionName;
@property (retain) NSString * DecisionNo;
@property (retain) NSString * DirectReportToEmployeeCode;
@property (retain) NSString * DirectReportToEmployeeFullName;
@property (retain) NSNumber * DirectReportToEmployeeId;
@property (retain) NSNumber * EmpProfileJobPositionId;
@property (retain) NSNumber * EmpProfileProcessOfWorkId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsConcurrentPost;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainPost;
@property (retain) USBoolean * IsPromoted;
@property (retain) NSString * JOB_TITLE_LEVEL_NAME_EN;
@property (retain) NSString * JOB_TITLE_LEVEL_NAME_VN;
@property (retain) NSString * LastName;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobNameEN;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSString * OrgJobPositionNameEN;
@property (retain) NSString * OrgJobTitleCode;
@property (retain) NSString * OrgJobTitleGroupCode;
@property (retain) NSNumber * OrgJobTitleId;
@property (retain) NSString * OrgJobTitleLevelCode;
@property (retain) NSString * OrgJobTitleNameEN;
@property (retain) NSString * OrgJobTitleNameVN;
@property (retain) NSString * OrgUnitAddress;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitName;
@property (retain) NSString * OrgUnitNameEN;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSString * OrgWorkLevelDescription;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * OrgWorkLevelName;
@property (retain) NSString * OrgWorkLevelNameEN;
@property (retain) NSNumber * PercentParticipation;
@property (retain) NSString * Reason;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_EmpProfileProcessOfWork : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileProcessOfWork;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_EmpProfileProcessOfWork *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileProcessOfWork:(empl_tns1_V_EmpProfileProcessOfWork *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileProcessOfWork;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpContract : NSObject {
	
/* elements */
	NSString * AllocationEquipmentDescription;
	NSString * Allowances;
	NSString * AttachFile;
	NSNumber * BaseWage;
	NSString * Bonuses;
	NSString * CONTRACT_CODE_TEMPLATE;
	NSString * CONTRACT_TERM_CODE;
	NSString * CONTRACT_TERM_NAME_EN;
	NSString * CONTRACT_TERM_NAME_VN;
	NSString * CONTRACT_TYPE_CODE;
	NSString * CONTRACT_TYPE_NAME_EN;
	NSString * CONTRACT_TYPE_NAME_VN;
	NSNumber * CompanyId;
	NSString * CompensationDescription;
	NSString * ContractCode;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * DAYS_PERIOD;
	NSDate * DateSigned;
	NSDate * EmpAppraiseDate;
	NSString * EmpAppraiseNote;
	NSNumber * EmpAppraiseRating;
	NSNumber * EmpContractId;
	NSString * EmployeeRepresentativeAddress;
	NSDate * EmployeeRepresentativeBirthDay;
	NSString * EmployeeRepresentativeBirthPlace;
	NSString * EmployeeRepresentativeCode;
	NSString * EmployeeRepresentativeFullName;
	NSNumber * EmployeeRepresentativeId;
	NSString * EmployeeRepresentativeMajor;
	NSString * EmployeeRepresentativeNationality;
	NSString * EmployerNationality;
	NSString * EmployerPositionName;
	NSString * EmployerRepresentativeCode;
	NSString * EmployerRepresentativeFullName;
	NSNumber * EmployerRepresentativeId;
	NSString * EmployerRepresentativePositionName;
	NSDate * EndDate;
	NSDate * ExpirationDate;
	NSDate * HRAppraiseDate;
	NSString * HRAppraiseNote;
	NSNumber * HRAppraiseRating;
	NSString * HolIdayPolicy;
	NSNumber * HoursOfOfficialWork;
	USBoolean * IS_TERM_CONTRAC_TYPE;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	USBoolean * IsDeleted;
	USBoolean * IsEnd;
	USBoolean * IsProbation;
	USBoolean * IsSigned;
	NSString * JobName;
	NSString * JobNameEN;
	NSString * JobPositionName;
	NSString * JobPositionNameEN;
	NSString * JobResponsibilitiesDescription;
	NSString * LaborSupportEquipments;
	NSDate * LeaderAppraiseDate;
	NSString * LeaderAppraiseNote;
	NSNumber * LeaderAppraiseRating;
	NSNumber * MONTHS_PERIOD;
	NSString * MedicalInsurance;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSDate * OfficialWorkEndTime;
	NSDate * OfficialWorkStartTime;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrganizationAddress;
	NSString * OrganizationName;
	NSString * OrganizationPhone;
	NSString * OtherPolicy;
	NSString * PayDay;
	NSString * PayingMethod;
	NSString * PlaceSigned;
	NSDate * ProbationaryEndDate;
	NSDate * ProbationaryStartDate;
	NSNumber * ProbationaryTime;
	NSString * ProvidedEquipment;
	NSNumber * RelationEmpContractId;
	NSString * SalaryIncreasePolicy;
	NSString * SocialInsurance;
	NSDate * StartDate;
	NSString * TrainingPolicy;
	NSString * Transportation;
	NSString * UnitName;
	NSString * UnitNameEN;
	NSString * WorkingAddress;
	NSNumber * WorkingDay;
	NSString * WorkingTime;
	NSString * WorkingTimePolicy;
	NSString * cAllowance;
	NSString * cBaseSalary;
	NSString * cSocialInsuranceSalary;
	NSNumber * nAllowance;
	NSNumber * nSocialInsuranceSalary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AllocationEquipmentDescription;
@property (retain) NSString * Allowances;
@property (retain) NSString * AttachFile;
@property (retain) NSNumber * BaseWage;
@property (retain) NSString * Bonuses;
@property (retain) NSString * CONTRACT_CODE_TEMPLATE;
@property (retain) NSString * CONTRACT_TERM_CODE;
@property (retain) NSString * CONTRACT_TERM_NAME_EN;
@property (retain) NSString * CONTRACT_TERM_NAME_VN;
@property (retain) NSString * CONTRACT_TYPE_CODE;
@property (retain) NSString * CONTRACT_TYPE_NAME_EN;
@property (retain) NSString * CONTRACT_TYPE_NAME_VN;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompensationDescription;
@property (retain) NSString * ContractCode;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * DAYS_PERIOD;
@property (retain) NSDate * DateSigned;
@property (retain) NSDate * EmpAppraiseDate;
@property (retain) NSString * EmpAppraiseNote;
@property (retain) NSNumber * EmpAppraiseRating;
@property (retain) NSNumber * EmpContractId;
@property (retain) NSString * EmployeeRepresentativeAddress;
@property (retain) NSDate * EmployeeRepresentativeBirthDay;
@property (retain) NSString * EmployeeRepresentativeBirthPlace;
@property (retain) NSString * EmployeeRepresentativeCode;
@property (retain) NSString * EmployeeRepresentativeFullName;
@property (retain) NSNumber * EmployeeRepresentativeId;
@property (retain) NSString * EmployeeRepresentativeMajor;
@property (retain) NSString * EmployeeRepresentativeNationality;
@property (retain) NSString * EmployerNationality;
@property (retain) NSString * EmployerPositionName;
@property (retain) NSString * EmployerRepresentativeCode;
@property (retain) NSString * EmployerRepresentativeFullName;
@property (retain) NSNumber * EmployerRepresentativeId;
@property (retain) NSString * EmployerRepresentativePositionName;
@property (retain) NSDate * EndDate;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSDate * HRAppraiseDate;
@property (retain) NSString * HRAppraiseNote;
@property (retain) NSNumber * HRAppraiseRating;
@property (retain) NSString * HolIdayPolicy;
@property (retain) NSNumber * HoursOfOfficialWork;
@property (retain) USBoolean * IS_TERM_CONTRAC_TYPE;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEnd;
@property (retain) USBoolean * IsProbation;
@property (retain) USBoolean * IsSigned;
@property (retain) NSString * JobName;
@property (retain) NSString * JobNameEN;
@property (retain) NSString * JobPositionName;
@property (retain) NSString * JobPositionNameEN;
@property (retain) NSString * JobResponsibilitiesDescription;
@property (retain) NSString * LaborSupportEquipments;
@property (retain) NSDate * LeaderAppraiseDate;
@property (retain) NSString * LeaderAppraiseNote;
@property (retain) NSNumber * LeaderAppraiseRating;
@property (retain) NSNumber * MONTHS_PERIOD;
@property (retain) NSString * MedicalInsurance;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * OfficialWorkEndTime;
@property (retain) NSDate * OfficialWorkStartTime;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrganizationAddress;
@property (retain) NSString * OrganizationName;
@property (retain) NSString * OrganizationPhone;
@property (retain) NSString * OtherPolicy;
@property (retain) NSString * PayDay;
@property (retain) NSString * PayingMethod;
@property (retain) NSString * PlaceSigned;
@property (retain) NSDate * ProbationaryEndDate;
@property (retain) NSDate * ProbationaryStartDate;
@property (retain) NSNumber * ProbationaryTime;
@property (retain) NSString * ProvidedEquipment;
@property (retain) NSNumber * RelationEmpContractId;
@property (retain) NSString * SalaryIncreasePolicy;
@property (retain) NSString * SocialInsurance;
@property (retain) NSDate * StartDate;
@property (retain) NSString * TrainingPolicy;
@property (retain) NSString * Transportation;
@property (retain) NSString * UnitName;
@property (retain) NSString * UnitNameEN;
@property (retain) NSString * WorkingAddress;
@property (retain) NSNumber * WorkingDay;
@property (retain) NSString * WorkingTime;
@property (retain) NSString * WorkingTimePolicy;
@property (retain) NSString * cAllowance;
@property (retain) NSString * cBaseSalary;
@property (retain) NSString * cSocialInsuranceSalary;
@property (retain) NSNumber * nAllowance;
@property (retain) NSNumber * nSocialInsuranceSalary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_EmpContract : NSObject {
	
/* elements */
	NSMutableArray *V_EmpContract;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_EmpContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpContract:(empl_tns1_V_EmpContract *)toAdd;
@property (readonly) NSMutableArray * V_EmpContract;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_ESS_ORG_POSITION_ALL_DESC : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * EXPERIENCE;
	NSString * KNOWNLEDGE;
	NSString * POSITION_CODE;
	NSNumber * POSITION_ID;
	NSString * POSITION_NAME;
	NSString * POS_DESC;
	NSString * QUALIFICATION;
	NSString * SKILL;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_ESS_ORG_POSITION_ALL_DESC *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * EXPERIENCE;
@property (retain) NSString * KNOWNLEDGE;
@property (retain) NSString * POSITION_CODE;
@property (retain) NSNumber * POSITION_ID;
@property (retain) NSString * POSITION_NAME;
@property (retain) NSString * POS_DESC;
@property (retain) NSString * QUALIFICATION;
@property (retain) NSString * SKILL;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_ESS_REQ_EQUIPMENT_MASTER : NSObject {
	
/* elements */
	NSDate * ApprovalDate;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * EQUIPMENT_BY_EMP_CODE;
	NSNumber * EQUIPMENT_BY_EMP_ID;
	NSString * EQUIPMENT_BY_EMP_NAME;
	NSString * EQUIPMENT_BY_ORG_NAME;
	NSString * EQUIPMENT_CODE;
	NSString * EQUIPMENT_FOR_EMP_CODE;
	NSNumber * EQUIPMENT_FOR_EMP_ID;
	NSString * EQUIPMENT_FOR_EMP_NAME;
	NSNumber * EQUIPMENT_FOR_ORG_ID;
	NSNumber * EQUIPMENT_IN_PLAN;
	NSNumber * EQUIPMENT_TYPE;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * REQ_MASTER_ID;
	NSString * REQ_MASTER_NAME;
	NSNumber * REQ_STATUS;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_ESS_REQ_EQUIPMENT_MASTER *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ApprovalDate;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * EQUIPMENT_BY_EMP_CODE;
@property (retain) NSNumber * EQUIPMENT_BY_EMP_ID;
@property (retain) NSString * EQUIPMENT_BY_EMP_NAME;
@property (retain) NSString * EQUIPMENT_BY_ORG_NAME;
@property (retain) NSString * EQUIPMENT_CODE;
@property (retain) NSString * EQUIPMENT_FOR_EMP_CODE;
@property (retain) NSNumber * EQUIPMENT_FOR_EMP_ID;
@property (retain) NSString * EQUIPMENT_FOR_EMP_NAME;
@property (retain) NSNumber * EQUIPMENT_FOR_ORG_ID;
@property (retain) NSNumber * EQUIPMENT_IN_PLAN;
@property (retain) NSNumber * EQUIPMENT_TYPE;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * REQ_MASTER_ID;
@property (retain) NSString * REQ_MASTER_NAME;
@property (retain) NSNumber * REQ_STATUS;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_ESS_REQ_EQUIPMENT_MASTER : NSObject {
	
/* elements */
	NSMutableArray *V_ESS_REQ_EQUIPMENT_MASTER;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_ESS_REQ_EQUIPMENT_MASTER *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ESS_REQ_EQUIPMENT_MASTER:(empl_tns1_V_ESS_REQ_EQUIPMENT_MASTER *)toAdd;
@property (readonly) NSMutableArray * V_ESS_REQ_EQUIPMENT_MASTER;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_ESS_REQ_EQUIPMENT_DETAIL : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * EQUIPMENT_DESC;
	NSString * EQUIPMENT_NAME;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * REQ_DETAIL_ID;
	NSNumber * REQ_MASTER_ID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_ESS_REQ_EQUIPMENT_DETAIL *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * EQUIPMENT_DESC;
@property (retain) NSString * EQUIPMENT_NAME;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * REQ_DETAIL_ID;
@property (retain) NSNumber * REQ_MASTER_ID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_ESS_REQ_EQUIPMENT_DETAIL : NSObject {
	
/* elements */
	NSMutableArray *V_ESS_REQ_EQUIPMENT_DETAIL;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_ESS_REQ_EQUIPMENT_DETAIL *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ESS_REQ_EQUIPMENT_DETAIL:(empl_tns1_V_ESS_REQ_EQUIPMENT_DETAIL *)toAdd;
@property (readonly) NSMutableArray * V_ESS_REQ_EQUIPMENT_DETAIL;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpProfileEquipment : NSObject {
	
/* elements */
	NSString * ASSETS_TYPE_CODE;
	NSString * ASSETS_TYPE_NAME_EN;
	NSString * ASSETS_TYPE_NAME_VN;
	NSDate * AddDate;
	NSString * CLogCurrencyCode;
	NSNumber * CLogCurrencyId;
	NSString * CLogCurrencyName;
	NSString * CLogEquipmentCode;
	NSNumber * CLogEquipmentId;
	NSString * CLogEquipmentName;
	NSString * CLogEquipmentNote;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSDate * CreatedDate;
	NSDate * DateHandover;
	NSDate * DateReceived;
	NSNumber * Depreciation;
	NSString * Description;
	NSString * EQUIPMENT_DESC;
	NSNumber * EQUIPMENT_IN_PLAN;
	NSString * EQUIPMENT_NAME;
	NSNumber * EQUIPMENT_TYPE;
	NSString * EQUIPMENT_TYPE_CODE;
	NSString * EQUIPMENT_TYPE_NAME_EN;
	NSString * EQUIPMENT_TYPE_NAME_VN;
	NSNumber * EmpProfileEquipmentId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * FullName;
	USBoolean * IsBigValue;
	USBoolean * IsDeleted;
	USBoolean * IsHandover;
	USBoolean * IsReturnToComp;
	NSDate * ModifiedDate;
	USBoolean * MustHandover;
	NSNumber * Number;
	NSNumber * OrgUnitId;
	NSNumber * REQ_DETAIL_ID;
	NSNumber * REQ_MASTER_ID;
	NSString * REQ_MASTER_NAME;
	NSNumber * REQ_STATUS;
	NSString * ReceiverCode;
	NSString * ReceiverFullName;
	NSNumber * ReceiverId;
	NSString * StateHandover;
	NSString * StateReceived;
	NSString * UnitName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpProfileEquipment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ASSETS_TYPE_CODE;
@property (retain) NSString * ASSETS_TYPE_NAME_EN;
@property (retain) NSString * ASSETS_TYPE_NAME_VN;
@property (retain) NSDate * AddDate;
@property (retain) NSString * CLogCurrencyCode;
@property (retain) NSNumber * CLogCurrencyId;
@property (retain) NSString * CLogCurrencyName;
@property (retain) NSString * CLogEquipmentCode;
@property (retain) NSNumber * CLogEquipmentId;
@property (retain) NSString * CLogEquipmentName;
@property (retain) NSString * CLogEquipmentNote;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateHandover;
@property (retain) NSDate * DateReceived;
@property (retain) NSNumber * Depreciation;
@property (retain) NSString * Description;
@property (retain) NSString * EQUIPMENT_DESC;
@property (retain) NSNumber * EQUIPMENT_IN_PLAN;
@property (retain) NSString * EQUIPMENT_NAME;
@property (retain) NSNumber * EQUIPMENT_TYPE;
@property (retain) NSString * EQUIPMENT_TYPE_CODE;
@property (retain) NSString * EQUIPMENT_TYPE_NAME_EN;
@property (retain) NSString * EQUIPMENT_TYPE_NAME_VN;
@property (retain) NSNumber * EmpProfileEquipmentId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * FullName;
@property (retain) USBoolean * IsBigValue;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsHandover;
@property (retain) USBoolean * IsReturnToComp;
@property (retain) NSDate * ModifiedDate;
@property (retain) USBoolean * MustHandover;
@property (retain) NSNumber * Number;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * REQ_DETAIL_ID;
@property (retain) NSNumber * REQ_MASTER_ID;
@property (retain) NSString * REQ_MASTER_NAME;
@property (retain) NSNumber * REQ_STATUS;
@property (retain) NSString * ReceiverCode;
@property (retain) NSString * ReceiverFullName;
@property (retain) NSNumber * ReceiverId;
@property (retain) NSString * StateHandover;
@property (retain) NSString * StateReceived;
@property (retain) NSString * UnitName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_EmpProfileEquipment : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileEquipment;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_EmpProfileEquipment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileEquipment:(empl_tns1_V_EmpProfileEquipment *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileEquipment;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpProfileAssets : NSObject {
	
/* elements */
	NSString * ASSETS_TYPE_CODE;
	NSString * ASSETS_TYPE_NAME_EN;
	NSString * ASSETS_TYPE_NAME_VN;
	NSDate * AddDate;
	NSString * CLogCurrencyCode;
	NSNumber * CLogCurrencyId;
	NSString * CLogCurrencyName;
	NSString * CLogEquipmentCode;
	NSNumber * CLogEquipmentId;
	NSString * CLogEquipmentName;
	NSString * CLogEquipmentNote;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSDate * CreatedDate;
	NSDate * DateHandover;
	NSDate * DateReceived;
	NSNumber * Depreciation;
	NSString * Description;
	NSString * EQUIPMENT_DESC;
	NSNumber * EQUIPMENT_IN_PLAN;
	NSString * EQUIPMENT_NAME;
	NSNumber * EQUIPMENT_TYPE;
	NSString * EQUIPMENT_TYPE_CODE;
	NSString * EQUIPMENT_TYPE_NAME_EN;
	NSString * EQUIPMENT_TYPE_NAME_VN;
	NSNumber * EmpProfileEquipmentId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * FullName;
	USBoolean * IsBigValue;
	USBoolean * IsDeleted;
	USBoolean * IsHandover;
	USBoolean * IsReturnToComp;
	NSDate * ModifiedDate;
	USBoolean * MustHandover;
	NSNumber * Number;
	NSNumber * OrgUnitId;
	NSNumber * REQ_DETAIL_ID;
	NSNumber * REQ_MASTER_ID;
	NSString * REQ_MASTER_NAME;
	NSNumber * REQ_STATUS;
	NSString * ReceiverCode;
	NSString * ReceiverFullName;
	NSNumber * ReceiverId;
	NSString * StateHandover;
	NSString * StateReceived;
	NSString * UnitName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpProfileAssets *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ASSETS_TYPE_CODE;
@property (retain) NSString * ASSETS_TYPE_NAME_EN;
@property (retain) NSString * ASSETS_TYPE_NAME_VN;
@property (retain) NSDate * AddDate;
@property (retain) NSString * CLogCurrencyCode;
@property (retain) NSNumber * CLogCurrencyId;
@property (retain) NSString * CLogCurrencyName;
@property (retain) NSString * CLogEquipmentCode;
@property (retain) NSNumber * CLogEquipmentId;
@property (retain) NSString * CLogEquipmentName;
@property (retain) NSString * CLogEquipmentNote;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateHandover;
@property (retain) NSDate * DateReceived;
@property (retain) NSNumber * Depreciation;
@property (retain) NSString * Description;
@property (retain) NSString * EQUIPMENT_DESC;
@property (retain) NSNumber * EQUIPMENT_IN_PLAN;
@property (retain) NSString * EQUIPMENT_NAME;
@property (retain) NSNumber * EQUIPMENT_TYPE;
@property (retain) NSString * EQUIPMENT_TYPE_CODE;
@property (retain) NSString * EQUIPMENT_TYPE_NAME_EN;
@property (retain) NSString * EQUIPMENT_TYPE_NAME_VN;
@property (retain) NSNumber * EmpProfileEquipmentId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * FullName;
@property (retain) USBoolean * IsBigValue;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsHandover;
@property (retain) USBoolean * IsReturnToComp;
@property (retain) NSDate * ModifiedDate;
@property (retain) USBoolean * MustHandover;
@property (retain) NSNumber * Number;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * REQ_DETAIL_ID;
@property (retain) NSNumber * REQ_MASTER_ID;
@property (retain) NSString * REQ_MASTER_NAME;
@property (retain) NSNumber * REQ_STATUS;
@property (retain) NSString * ReceiverCode;
@property (retain) NSString * ReceiverFullName;
@property (retain) NSNumber * ReceiverId;
@property (retain) NSString * StateHandover;
@property (retain) NSString * StateReceived;
@property (retain) NSString * UnitName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_EmpProfileAssets : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileAssets;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_EmpProfileAssets *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileAssets:(empl_tns1_V_EmpProfileAssets *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileAssets;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpProfileWageType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpProfileWageTypeId;
	NSNumber * EmployeeId;
	NSDate * ImplementationDate;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * NameEN;
	NSString * NameVN;
	NSString * Note;
	NSString * TimeWageTypeCode;
	NSNumber * TimeWageTypeId;
	NSNumber * WorkingDay;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpProfileWageType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileWageTypeId;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * NameEN;
@property (retain) NSString * NameVN;
@property (retain) NSString * Note;
@property (retain) NSString * TimeWageTypeCode;
@property (retain) NSNumber * TimeWageTypeId;
@property (retain) NSNumber * WorkingDay;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_EmpProfileWageType : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileWageType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_EmpProfileWageType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileWageType:(empl_tns1_V_EmpProfileWageType *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileWageType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpProfileWorkingForm : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpProfileWorkingFormId;
	NSNumber * EmployeeId;
	NSDate * ImplementationDate;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * NameEN;
	NSString * NameVN;
	NSString * Note;
	NSString * TimeWorkingFormCode;
	NSNumber * TimeWorkingFormId;
	NSString * TimeWorkingFormNote;
	NSDate * TimeWorkingFormStartEndTime;
	NSDate * TimeWorkingFormStartTime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpProfileWorkingForm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileWorkingFormId;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * NameEN;
@property (retain) NSString * NameVN;
@property (retain) NSString * Note;
@property (retain) NSString * TimeWorkingFormCode;
@property (retain) NSNumber * TimeWorkingFormId;
@property (retain) NSString * TimeWorkingFormNote;
@property (retain) NSDate * TimeWorkingFormStartEndTime;
@property (retain) NSDate * TimeWorkingFormStartTime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_EmpProfileWorkingForm : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileWorkingForm;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_EmpProfileWorkingForm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileWorkingForm:(empl_tns1_V_EmpProfileWorkingForm *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileWorkingForm;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpProfileComment : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSString * Content;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfileCommentId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * RelateRowId;
	NSString * ReviewerCode;
	NSString * ReviewerFullName;
	NSNumber * ReviewerId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpProfileComment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Content;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfileCommentId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * ReviewerCode;
@property (retain) NSString * ReviewerFullName;
@property (retain) NSNumber * ReviewerId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_EmpProfileComment : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileComment;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_EmpProfileComment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileComment:(empl_tns1_V_EmpProfileComment *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileComment;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpProfileDiscipline : NSObject {
	
/* elements */
	NSNumber * ApproveId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSString * DISCIPLINE_TYPE_DESC;
	NSString * DISCIPLINE_TYPE_NAME_EN;
	NSString * DISCIPLINE_TYPE_NAME_VN;
	NSDate * DateOfIssue;
	NSString * DecisionNo;
	NSNumber * EmpProfileDisciplineId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * ICON;
	USBoolean * IsDeleted;
	USBoolean * IsForEmployee;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * MonetaryValue;
	NSNumber * OrgDisciplineMasterId;
	NSString * OrgDisciplineTypeCode;
	USBoolean * Status;
	NSString * Title;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpProfileDiscipline *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproveId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSString * DISCIPLINE_TYPE_DESC;
@property (retain) NSString * DISCIPLINE_TYPE_NAME_EN;
@property (retain) NSString * DISCIPLINE_TYPE_NAME_VN;
@property (retain) NSDate * DateOfIssue;
@property (retain) NSString * DecisionNo;
@property (retain) NSNumber * EmpProfileDisciplineId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * ICON;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsForEmployee;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * MonetaryValue;
@property (retain) NSNumber * OrgDisciplineMasterId;
@property (retain) NSString * OrgDisciplineTypeCode;
@property (retain) USBoolean * Status;
@property (retain) NSString * Title;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_EmpProfileDiscipline : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileDiscipline;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_EmpProfileDiscipline *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileDiscipline:(empl_tns1_V_EmpProfileDiscipline *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileDiscipline;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpPerformanceAppraisal : NSObject {
	
/* elements */
	NSDate * AppraisalDate;
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpPerformanceAppraisalId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsEndYear;
	USBoolean * IsHalfYear;
	NSString * ManagerRatingDescription;
	NSNumber * ManagerRatingId;
	NSNumber * ManagerRatingValue;
	NSDate * ModifiedDate;
	NSNumber * RelateRowId;
	NSNumber * Score;
	NSString * SelfComment;
	NSString * SelfRatingDescription;
	NSNumber * SelfRatingId;
	NSNumber * SelfRatingValue;
	NSNumber * SelfScore;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpPerformanceAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AppraisalDate;
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpPerformanceAppraisalId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEndYear;
@property (retain) USBoolean * IsHalfYear;
@property (retain) NSString * ManagerRatingDescription;
@property (retain) NSNumber * ManagerRatingId;
@property (retain) NSNumber * ManagerRatingValue;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSString * SelfRatingDescription;
@property (retain) NSNumber * SelfRatingId;
@property (retain) NSNumber * SelfRatingValue;
@property (retain) NSNumber * SelfScore;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_EmpPerformanceAppraisal : NSObject {
	
/* elements */
	NSMutableArray *V_EmpPerformanceAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_EmpPerformanceAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpPerformanceAppraisal:(empl_tns1_V_EmpPerformanceAppraisal *)toAdd;
@property (readonly) NSMutableArray * V_EmpPerformanceAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_EmpProfileSocialInsurance : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ImplementDate;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	USBoolean * IsDifference;
	USBoolean * IsLostBook;
	NSDate * LabourBookDateOfIssue;
	NSString * LabourBookNo;
	NSString * LabourBookPlaceOfIssue;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * OtherInsuranceCompanyName;
	NSDate * OtherInsuranceDateEffect;
	NSDate * OtherInsuranceDateExpire;
	NSString * OtherInsuranceNumber;
	NSString * Remark;
	NSNumber * SICompanyRate;
	NSNumber * SIEmployeeRate;
	NSDate * SocialInsuranceDateIssue;
	NSNumber * SocialInsuranceId;
	NSNumber * SocialInsuranceJoiningBefore;
	NSString * SocialInsuranceNumber;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_EmpProfileSocialInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ImplementDate;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDifference;
@property (retain) USBoolean * IsLostBook;
@property (retain) NSDate * LabourBookDateOfIssue;
@property (retain) NSString * LabourBookNo;
@property (retain) NSString * LabourBookPlaceOfIssue;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OtherInsuranceCompanyName;
@property (retain) NSDate * OtherInsuranceDateEffect;
@property (retain) NSDate * OtherInsuranceDateExpire;
@property (retain) NSString * OtherInsuranceNumber;
@property (retain) NSString * Remark;
@property (retain) NSNumber * SICompanyRate;
@property (retain) NSNumber * SIEmployeeRate;
@property (retain) NSDate * SocialInsuranceDateIssue;
@property (retain) NSNumber * SocialInsuranceId;
@property (retain) NSNumber * SocialInsuranceJoiningBefore;
@property (retain) NSString * SocialInsuranceNumber;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfEmpProfileSocialInsurance : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileSocialInsurance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfEmpProfileSocialInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileSocialInsurance:(empl_tns1_EmpProfileSocialInsurance *)toAdd;
@property (readonly) NSMutableArray * EmpProfileSocialInsurance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpProfileHealthInsurance : NSObject {
	
/* elements */
	NSString * CLogCityCode;
	NSNumber * CLogCityId;
	NSString * CLogCityName;
	NSString * CLogCityNoIns;
	NSString * CLogHospitalCode;
	NSNumber * CLogHospitalId;
	NSString * CLogHospitalName;
	NSString * CLogHospitalNote;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * HealthInsuranceDateEffect;
	NSDate * HealthInsuranceDateExpire;
	NSNumber * HealthInsuranceId;
	NSString * HealthInsuranceNumber;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpProfileHealthInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogCityCode;
@property (retain) NSNumber * CLogCityId;
@property (retain) NSString * CLogCityName;
@property (retain) NSString * CLogCityNoIns;
@property (retain) NSString * CLogHospitalCode;
@property (retain) NSNumber * CLogHospitalId;
@property (retain) NSString * CLogHospitalName;
@property (retain) NSString * CLogHospitalNote;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * HealthInsuranceDateEffect;
@property (retain) NSDate * HealthInsuranceDateExpire;
@property (retain) NSNumber * HealthInsuranceId;
@property (retain) NSString * HealthInsuranceNumber;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_ArrayOfV_EmpProfileHealthInsurance : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileHealthInsurance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_ArrayOfV_EmpProfileHealthInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileHealthInsurance:(empl_tns1_V_EmpProfileHealthInsurance *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileHealthInsurance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns1_V_EmpProfileOtherBenefit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * EmpOtherBenefitDescription;
	NSNumber * EmpOtherBenefitId;
	NSString * EmpOtherBenefitName;
	NSNumber * EmpProfileOtherBenefitId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsEmpProfileOtherBenefitDeleted;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns1_V_EmpProfileOtherBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * EmpOtherBenefitDescription;
@property (retain) NSNumber * EmpOtherBenefitId;
@property (retain) NSString * EmpOtherBenefitName;
@property (retain) NSNumber * EmpProfileOtherBenefitId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsEmpProfileOtherBenefitDeleted;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
