#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class empl_tns2_CVPersonalModel;
@class empl_tns2_MsgNotifyModel;
#import "empl_tns1.h"
@interface empl_tns2_CVPersonalModel : NSObject {
	
/* elements */
	empl_tns1_V_EmpBasicInformation * BasicInfomation;
	empl_tns1_ArrayOfEmpProfileExperience * Experience;
	empl_tns1_ArrayOfV_EmpProfileQualification * ProfileQualification;
	empl_tns1_ArrayOfV_EmpProfileWorkingExperience * WorkingExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns2_CVPersonalModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpBasicInformation * BasicInfomation;
@property (retain) empl_tns1_ArrayOfEmpProfileExperience * Experience;
@property (retain) empl_tns1_ArrayOfV_EmpProfileQualification * ProfileQualification;
@property (retain) empl_tns1_ArrayOfV_EmpProfileWorkingExperience * WorkingExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns2_MsgNotifyModel : NSObject {
	
/* elements */
	NSNumber * Id_;
	USBoolean * IsSuccess;
	USBoolean * IsValid;
	NSString * NotifyMsg;
	NSString * NotifyMsgEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns2_MsgNotifyModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Id_;
@property (retain) USBoolean * IsSuccess;
@property (retain) USBoolean * IsValid;
@property (retain) NSString * NotifyMsg;
@property (retain) NSString * NotifyMsgEN;
/* attributes */
- (NSDictionary *)attributes;
@end
