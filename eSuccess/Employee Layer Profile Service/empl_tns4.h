#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class empl_tns4_ArrayOfEmpJobDescriptionModel;
@class empl_tns4_EmpJobDescriptionModel;
#import "empl_tns1.h"
@interface empl_tns4_EmpJobDescriptionModel : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileJobPosition * EmpProfileJobPosition;
	empl_tns1_V_ESS_ORG_POSITION_ALL_DESC * JobPositionDescription;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns4_EmpJobDescriptionModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileJobPosition * EmpProfileJobPosition;
@property (retain) empl_tns1_V_ESS_ORG_POSITION_ALL_DESC * JobPositionDescription;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface empl_tns4_ArrayOfEmpJobDescriptionModel : NSObject {
	
/* elements */
	NSMutableArray *EmpJobDescriptionModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (empl_tns4_ArrayOfEmpJobDescriptionModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpJobDescriptionModel:(empl_tns4_EmpJobDescriptionModel *)toAdd;
@property (readonly) NSMutableArray * EmpJobDescriptionModel;
/* attributes */
- (NSDictionary *)attributes;
@end
