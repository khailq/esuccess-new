#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class EmpProfileLayerServiceSvc_AddEmpProfileQualificationByView;
@class EmpProfileLayerServiceSvc_AddEmpProfileQualificationByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileQualificationByView;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileQualificationByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileQualificationByView;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileQualificationByViewResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileForeignLanguageById;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileForeignLanguageByIdResponse;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileForeignLanguageByView;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileForeignLanguageByViewResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileForeignLanguageByView;
@class EmpProfileLayerServiceSvc_AddEmpProfileForeignLanguageByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileForeignLanguageByView;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileForeignLanguageByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileForeignLanguageByView;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileForeignLanguageByViewResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileComputingSkillById;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileComputingSkillByIdResponse;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileComputingSkill;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileComputingSkillResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileComputingSkill;
@class EmpProfileLayerServiceSvc_AddEmpProfileComputingSkillResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileComputingSkill;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileComputingSkillResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileComputingSkill;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileComputingSkillResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileSkillById;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileSkillByIdResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileSkillByView;
@class EmpProfileLayerServiceSvc_AddEmpProfileSkillByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileSkillByView;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileSkillByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileSkillByView;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileSkillByViewResponse;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileSkillByView;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileSkillByViewResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileDegreeById;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileDegreeByIdResponse;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileDegreeByView;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileDegreeByViewResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileDegreeByView;
@class EmpProfileLayerServiceSvc_AddEmpProfileDegreeByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileDegreeByView;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileDegreeByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileDegreeByView;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileDegreeByViewResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileKnowledgeById;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileKnowledgeByIdResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileKnowledgeByView;
@class EmpProfileLayerServiceSvc_AddEmpProfileKnowledgeByViewResponse;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileKnowledgeByView;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileKnowledgeByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileKnowledgeByView;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileKnowledgeByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileKnowledgeByView;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileKnowledgeByViewResponse;
@class EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataGroupByEmpId;
@class EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataGroupByEmpIdResponse;
@class EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataByEmpId;
@class EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataByEmpIdResponse;
@class EmpProfileLayerServiceSvc_AddCBFactorEmployeeMetaDataByView;
@class EmpProfileLayerServiceSvc_AddCBFactorEmployeeMetaDataByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateCBFactorEmployeeMetaDataByView;
@class EmpProfileLayerServiceSvc_UpdateCBFactorEmployeeMetaDataByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteCBFactorEmployeeMetaDataByView;
@class EmpProfileLayerServiceSvc_DeleteCBFactorEmployeeMetaDataByViewResponse;
@class EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataChangesByEmpId;
@class EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataChangesByEmpIdResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileMaternityRegimeById;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileMaternityRegimeByIdResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileMaternityRegime;
@class EmpProfileLayerServiceSvc_AddEmpProfileMaternityRegimeResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileMaternityRegime;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileMaternityRegimeResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileMaternityRegime;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileMaternityRegimeResponse;
@class EmpProfileLayerServiceSvc_GetCVPersonalByEmployeeId;
@class EmpProfileLayerServiceSvc_GetCVPersonalByEmployeeIdResponse;
@class EmpProfileLayerServiceSvc_LoadAllProfileSubAndLayerToTree;
@class EmpProfileLayerServiceSvc_LoadAllProfileSubAndLayerToTreeResponse;
@class EmpProfileLayerServiceSvc_GetTableEmpGroupMappingByEmployeeId;
@class EmpProfileLayerServiceSvc_GetTableEmpGroupMappingByEmployeeIdResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpGroupMappingByEmployeeId;
@class EmpProfileLayerServiceSvc_GetViewEmpGroupMappingByEmployeeIdResponse;
@class EmpProfileLayerServiceSvc_GetAllEmployeeGroupAndSubGroup;
@class EmpProfileLayerServiceSvc_GetAllEmployeeGroupAndSubGroupResponse;
@class EmpProfileLayerServiceSvc_AddEmployeeToEmployeeGroupAndSubGroup;
@class EmpProfileLayerServiceSvc_AddEmployeeToEmployeeGroupAndSubGroupResponse;
@class EmpProfileLayerServiceSvc_GetAllEmployeeMappingByEmployeeGroupType;
@class EmpProfileLayerServiceSvc_GetAllEmployeeMappingByEmployeeGroupTypeResponse;
@class EmpProfileLayerServiceSvc_GetListPayScale;
@class EmpProfileLayerServiceSvc_GetListPayScaleResponse;
@class EmpProfileLayerServiceSvc_GetListPayLevelByPayScale;
@class EmpProfileLayerServiceSvc_GetListPayLevelByPayScaleResponse;
@class EmpProfileLayerServiceSvc_GetListPayGradeByPayLevel;
@class EmpProfileLayerServiceSvc_GetListPayGradeByPayLevelResponse;
@class EmpProfileLayerServiceSvc_GetCoefficient;
@class EmpProfileLayerServiceSvc_GetCoefficientResponse;
@class EmpProfileLayerServiceSvc_GetListSalary;
@class EmpProfileLayerServiceSvc_GetListSalaryResponse;
@class EmpProfileLayerServiceSvc_GetListAllowance;
@class EmpProfileLayerServiceSvc_GetListAllowanceResponse;
@class EmpProfileLayerServiceSvc_GetListInsurance;
@class EmpProfileLayerServiceSvc_GetListInsuranceResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileAllowanceByView;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileAllowanceByViewResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileProfileAllowanceByView;
@class EmpProfileLayerServiceSvc_AddEmpProfileProfileAllowanceByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileAllowanceByView;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileAllowanceByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileAllowanceByView;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileAllowanceByViewResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileBaseSalaryByView;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileBaseSalaryByViewResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileProfileBaseSalaryByView;
@class EmpProfileLayerServiceSvc_AddEmpProfileProfileBaseSalaryByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileBaseSalaryByView;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileBaseSalaryByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileBaseSalaryByView;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileBaseSalaryByViewResponse;
@class EmpProfileLayerServiceSvc_GetAllTotalIncomeByEmployeeId;
@class EmpProfileLayerServiceSvc_GetAllTotalIncomeByEmployeeIdResponse;
@class EmpProfileLayerServiceSvc_HasPermissionOnEmpProfileLayer;
@class EmpProfileLayerServiceSvc_HasPermissionOnEmpProfileLayerResponse;
@class EmpProfileLayerServiceSvc_GetPermissionsOnEmpProfileLayer;
@class EmpProfileLayerServiceSvc_GetPermissionsOnEmpProfileLayerResponse;
@class EmpProfileLayerServiceSvc_GetAllViewSysEmpProfileGroupAndLayers;
@class EmpProfileLayerServiceSvc_GetAllViewSysEmpProfileGroupAndLayersResponse;
@class EmpProfileLayerServiceSvc_GetViewSysEmpProfileGroupAndLayersByUrlType;
@class EmpProfileLayerServiceSvc_GetViewSysEmpProfileGroupAndLayersByUrlTypeResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpBasicProfileById;
@class EmpProfileLayerServiceSvc_GetViewEmpBasicProfileByIdResponse;
@class EmpProfileLayerServiceSvc_AddEmpBasicProfileByView;
@class EmpProfileLayerServiceSvc_AddEmpBasicProfileByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpBasicProfileByView;
@class EmpProfileLayerServiceSvc_UpdateEmpBasicProfileByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpBasicProfileByView;
@class EmpProfileLayerServiceSvc_DeleteEmpBasicProfileByViewResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfilePersonalityById;
@class EmpProfileLayerServiceSvc_GetViewEmpProfilePersonalityByIdResponse;
@class EmpProfileLayerServiceSvc_ApproveEmpProfilePersonalityByView;
@class EmpProfileLayerServiceSvc_ApproveEmpProfilePersonalityByViewResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfilePersonalityByView;
@class EmpProfileLayerServiceSvc_AddEmpProfilePersonalityByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfilePersonalityByView;
@class EmpProfileLayerServiceSvc_UpdateEmpProfilePersonalityByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfilePersonalityByView;
@class EmpProfileLayerServiceSvc_DeleteEmpProfilePersonalityByViewResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileHobbyById;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileHobbyByIdResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileHobbyByView;
@class EmpProfileLayerServiceSvc_AddEmpProfileHobbyByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileHobbyByView;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileHobbyByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileHobbyByView;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileHobbyByViewResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpSocialInsuranceSalaryById;
@class EmpProfileLayerServiceSvc_GetViewEmpSocialInsuranceSalaryByIdResponse;
@class EmpProfileLayerServiceSvc_AddEmpSocialInsuranceSalaryByView;
@class EmpProfileLayerServiceSvc_AddEmpSocialInsuranceSalaryByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpSocialInsuranceSalaryByView;
@class EmpProfileLayerServiceSvc_UpdateEmpSocialInsuranceSalaryByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpSocialInsuranceSalaryByView;
@class EmpProfileLayerServiceSvc_DeleteEmpSocialInsuranceSalaryByViewResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileBiographyById;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileBiographyByIdResponse;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileBiography;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileBiographyResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileBiography;
@class EmpProfileLayerServiceSvc_AddEmpProfileBiographyResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileBiography;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileBiographyResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileBiography;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileBiographyResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpFamilyRelationshipById;
@class EmpProfileLayerServiceSvc_GetViewEmpFamilyRelationshipByIdResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileFamilyRelationshipByView;
@class EmpProfileLayerServiceSvc_AddEmpProfileFamilyRelationshipByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileFamilyRelationshipByView;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileFamilyRelationshipByViewResponse;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileFamilyRelationshipByView;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileFamilyRelationshipByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileFamilyRelationshipByView;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileFamilyRelationshipByViewResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileContactsByEmployeeId;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileContactsByEmployeeIdResponse;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileContactByView;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileContactByViewResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileContactByView;
@class EmpProfileLayerServiceSvc_AddEmpProfileContactByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileContactByView;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileContactByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileContactByView;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileContactByViewResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileHealthy;
@class EmpProfileLayerServiceSvc_AddEmpProfileHealthyResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileHealthy;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileHealthyResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileHealthy;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileHealthyResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileParticipationById;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileParticipationByIdResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileParticipation;
@class EmpProfileLayerServiceSvc_AddEmpProfileParticipationResponse;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileParticipation;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileParticipationResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileParticipation;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileParticipationResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileParticipation;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileParticipationResponse;
@class EmpProfileLayerServiceSvc_GetActiveEmpProfileJobPositionByEmpId;
@class EmpProfileLayerServiceSvc_GetActiveEmpProfileJobPositionByEmpIdResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileJobPositions;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileJobPositionsResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileJobPositionByView;
@class EmpProfileLayerServiceSvc_AddEmpProfileJobPositionByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositionByView;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositionByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileJobPositionByView;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileJobPositionByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositions;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositionsResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileProcessOfWorkById;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileProcessOfWorkByIdResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileProcessOfWork;
@class EmpProfileLayerServiceSvc_AddEmpProfileProcessOfWorkResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileProcessOfWork;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileProcessOfWorkResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileProcessOfWork;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileProcessOfWorkResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileContractById;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileContractByIdResponse;
@class EmpProfileLayerServiceSvc_AddEmpContractByView;
@class EmpProfileLayerServiceSvc_AddEmpContractByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpContractByView;
@class EmpProfileLayerServiceSvc_UpdateEmpContractByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpContractByView;
@class EmpProfileLayerServiceSvc_DeleteEmpContractByViewResponse;
@class EmpProfileLayerServiceSvc_GetViewJobPositionDescriptionByEmployeeId;
@class EmpProfileLayerServiceSvc_GetViewJobPositionDescriptionByEmployeeIdResponse;
@class EmpProfileLayerServiceSvc_GetViewJobPositionConcurrentById;
@class EmpProfileLayerServiceSvc_GetViewJobPositionConcurrentByIdResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileJobPositionConcurrentByView;
@class EmpProfileLayerServiceSvc_AddEmpProfileJobPositionConcurrentByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositionConcurrentByView;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositionConcurrentByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileJobPositionConcurrentByView;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileJobPositionConcurrentByViewResponse;
@class EmpProfileLayerServiceSvc_GetViewRequestEquipmentMasterByEmployeeId;
@class EmpProfileLayerServiceSvc_GetViewRequestEquipmentMasterByEmployeeIdResponse;
@class EmpProfileLayerServiceSvc_GetViewRequestEquipmentDetail;
@class EmpProfileLayerServiceSvc_GetViewRequestEquipmentDetailResponse;
@class EmpProfileLayerServiceSvc_GetViewRequestEquipmentDetailByRequestId;
@class EmpProfileLayerServiceSvc_GetViewRequestEquipmentDetailByRequestIdResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileEquipmentById;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileEquipmentByIdResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileEquipmentByView;
@class EmpProfileLayerServiceSvc_AddEmpProfileEquipmentByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileEquipmentByView;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileEquipmentByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileEquipmentByView;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileEquipmentByViewResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileAssetsById;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileAssetsByIdResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileAssetsByView;
@class EmpProfileLayerServiceSvc_AddEmpProfileAssetsByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileAssetsByView;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileAssetsByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileAssetsByView;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileAssetsByViewResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileWageTypeById;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileWageTypeByIdResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileWageTypeByView;
@class EmpProfileLayerServiceSvc_AddEmpProfileWageTypeByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileWageTypeByView;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileWageTypeByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileWageTypeByView;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileWageTypeByViewResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingFormById;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingFormByIdResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileWorkingFormByView;
@class EmpProfileLayerServiceSvc_AddEmpProfileWorkingFormByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileWorkingFormByView;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileWorkingFormByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileWorkingFormByView;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileWorkingFormByViewResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingExperienceById;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingExperienceByIdResponse;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileWorkingExperienceByView;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileWorkingExperienceByViewResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileWorkingExperienceByView;
@class EmpProfileLayerServiceSvc_AddEmpProfileWorkingExperienceByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileWorkingExperienceByView;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileWorkingExperienceByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileWorkingExperienceByView;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileWorkingExperienceByViewResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileExperienceById;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileExperienceByIdResponse;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileExperience;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileExperienceResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileExperience;
@class EmpProfileLayerServiceSvc_AddEmpProfileExperienceResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileExperience;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileExperienceResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileExperience;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileExperienceResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileLeaveRegime;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileLeaveRegimeResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileLeaveRegime;
@class EmpProfileLayerServiceSvc_AddEmpProfileLeaveRegimeResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileLeaveRegime;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileLeaveRegimeResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileLeaveRegime;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileLeaveRegimeResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileCommentById;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileCommentByIdResponse;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileComment;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileCommentResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileComment;
@class EmpProfileLayerServiceSvc_AddEmpProfileCommentResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileComment;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileCommentResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileComment;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileCommentResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileTrainingById;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileTrainingByIdResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileTraining;
@class EmpProfileLayerServiceSvc_AddEmpProfileTrainingResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileTraining;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileTrainingResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileTraining;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileTrainingResponse;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileTraining;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileTrainingResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileDisciplineById;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileDisciplineByIdResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileDisciplineByView;
@class EmpProfileLayerServiceSvc_AddEmpProfileDisciplineByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileDisciplineByView;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileDisciplineByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileDisciplineByView;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileDisciplineByViewResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpPerformanceAppraisalById;
@class EmpProfileLayerServiceSvc_GetViewEmpPerformanceAppraisalByIdResponse;
@class EmpProfileLayerServiceSvc_AddEmpPerformanceAppraisalByView;
@class EmpProfileLayerServiceSvc_AddEmpPerformanceAppraisalByViewResponse;
@class EmpProfileLayerServiceSvc_ApproveEmpPerformanceAppraisalByView;
@class EmpProfileLayerServiceSvc_ApproveEmpPerformanceAppraisalByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpPerformanceAppraisalByView;
@class EmpProfileLayerServiceSvc_UpdateEmpPerformanceAppraisalByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpPerformanceAppraisalByView;
@class EmpProfileLayerServiceSvc_DeleteEmpPerformanceAppraisalByViewResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileSocialInsuranceById;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileSocialInsuranceByIdResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileSocialInsurance;
@class EmpProfileLayerServiceSvc_AddEmpProfileSocialInsuranceResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileSocialInsurance;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileSocialInsuranceResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileSocialInsurance;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileSocialInsuranceResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileHealthInsuranceById;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileHealthInsuranceByIdResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileHealthInsuranceByView;
@class EmpProfileLayerServiceSvc_AddEmpProfileHealthInsuranceByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileHealthInsuranceByView;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileHealthInsuranceByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileHealthInsuranceByView;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileHealthInsuranceByViewResponse;
@class EmpProfileLayerServiceSvc_AddEmpProfileOtherBenefitByView;
@class EmpProfileLayerServiceSvc_AddEmpProfileOtherBenefitByViewResponse;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileOtherBenefitByView;
@class EmpProfileLayerServiceSvc_UpdateEmpProfileOtherBenefitByViewResponse;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileOtherBenefitByView;
@class EmpProfileLayerServiceSvc_DeleteEmpProfileOtherBenefitByViewResponse;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileQualificationById;
@class EmpProfileLayerServiceSvc_GetViewEmpProfileQualificationByIdResponse;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileQualificationByView;
@class EmpProfileLayerServiceSvc_ApproveEmpProfileQualificationByViewResponse;
#import "empl_tns1.h"
#import "empl_tns2.h"
#import "empl_tns3.h"
#import "empl_tns4.h"
@interface EmpProfileLayerServiceSvc_AddEmpProfileQualificationByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileQualification * viewEmpProfileQualification;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileQualificationByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileQualification * viewEmpProfileQualification;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileQualificationByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileQualificationByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileQualificationByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileQualificationByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileQualificationByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileQualification * viewEmpProfileQualification;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileQualificationByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileQualification * viewEmpProfileQualification;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileQualificationByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileQualificationByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileQualificationByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileQualificationByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileQualificationByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileQualification * viewEmpProfileQualification;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileQualificationByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileQualification * viewEmpProfileQualification;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileQualificationByViewResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileQualificationByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileQualificationByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileQualificationByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileForeignLanguageById : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileForeignLanguageById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileForeignLanguageByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpProfileForeignLanguage * GetViewEmpProfileForeignLanguageByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileForeignLanguageByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpProfileForeignLanguage * GetViewEmpProfileForeignLanguageByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileForeignLanguageByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileForeignLanguage * viewEmpProfileForeignLanguage;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileForeignLanguageByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileForeignLanguage * viewEmpProfileForeignLanguage;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileForeignLanguageByViewResponse : NSObject {
	
/* elements */
	USBoolean * ApproveEmpProfileForeignLanguageByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileForeignLanguageByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * ApproveEmpProfileForeignLanguageByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileForeignLanguageByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileForeignLanguage * viewEmpProfileForeignLanguage;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileForeignLanguageByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileForeignLanguage * viewEmpProfileForeignLanguage;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileForeignLanguageByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileForeignLanguageByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileForeignLanguageByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileForeignLanguageByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileForeignLanguageByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileForeignLanguage * viewEmpProfileForeignLanguage;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileForeignLanguageByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileForeignLanguage * viewEmpProfileForeignLanguage;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileForeignLanguageByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileForeignLanguageByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileForeignLanguageByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileForeignLanguageByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileForeignLanguageByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileForeignLanguage * viewEmpProfileForeignLanguage;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileForeignLanguageByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileForeignLanguage * viewEmpProfileForeignLanguage;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileForeignLanguageByViewResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileForeignLanguageByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileForeignLanguageByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileForeignLanguageByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileComputingSkillById : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileComputingSkillById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileComputingSkillByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfEmpProfileComputingSkill * GetViewEmpProfileComputingSkillByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileComputingSkillByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfEmpProfileComputingSkill * GetViewEmpProfileComputingSkillByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileComputingSkill : NSObject {
	
/* elements */
	empl_tns1_EmpProfileComputingSkill * tableEmpProfileComputingSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileComputingSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileComputingSkill * tableEmpProfileComputingSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileComputingSkillResponse : NSObject {
	
/* elements */
	USBoolean * ApproveEmpProfileComputingSkillResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileComputingSkillResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * ApproveEmpProfileComputingSkillResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileComputingSkill : NSObject {
	
/* elements */
	empl_tns1_EmpProfileComputingSkill * tableEmpProfileComputingSkill;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileComputingSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileComputingSkill * tableEmpProfileComputingSkill;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileComputingSkillResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileComputingSkillResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileComputingSkillResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileComputingSkillResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileComputingSkill : NSObject {
	
/* elements */
	empl_tns1_EmpProfileComputingSkill * tableEmpProfileComputingSkill;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileComputingSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileComputingSkill * tableEmpProfileComputingSkill;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileComputingSkillResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileComputingSkillResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileComputingSkillResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileComputingSkillResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileComputingSkill : NSObject {
	
/* elements */
	empl_tns1_EmpProfileComputingSkill * tableEmpProfileComputingSkill;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileComputingSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileComputingSkill * tableEmpProfileComputingSkill;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileComputingSkillResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileComputingSkillResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileComputingSkillResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileComputingSkillResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileSkillById : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileSkillById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileSkillByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpProfileSkill * GetViewEmpProfileSkillByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileSkillByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpProfileSkill * GetViewEmpProfileSkillByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileSkillByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileSkill * viewEmpProfileSkill;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileSkillByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileSkill * viewEmpProfileSkill;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileSkillByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileSkillByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileSkillByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileSkillByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileSkillByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileSkill * viewEmpProfileSkill;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileSkillByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileSkill * viewEmpProfileSkill;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileSkillByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileSkillByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileSkillByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileSkillByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileSkillByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileSkill * viewEmpProfileSkill;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileSkillByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileSkill * viewEmpProfileSkill;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileSkillByViewResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileSkillByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileSkillByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileSkillByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileSkillByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileSkill * viewEmpProfileSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileSkillByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileSkill * viewEmpProfileSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileSkillByViewResponse : NSObject {
	
/* elements */
	USBoolean * ApproveEmpProfileSkillByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileSkillByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * ApproveEmpProfileSkillByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileDegreeById : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileDegreeById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileDegreeByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpProfileDegree * GetViewEmpProfileDegreeByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileDegreeByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpProfileDegree * GetViewEmpProfileDegreeByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileDegreeByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileDegree * viewEmpProfileDegree;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileDegreeByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileDegree * viewEmpProfileDegree;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileDegreeByViewResponse : NSObject {
	
/* elements */
	USBoolean * ApproveEmpProfileDegreeByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileDegreeByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * ApproveEmpProfileDegreeByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileDegreeByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileDegree * viewEmpProfileDegree;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileDegreeByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileDegree * viewEmpProfileDegree;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileDegreeByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileDegreeByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileDegreeByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileDegreeByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileDegreeByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileDegree * viewEmpProfileDegree;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileDegreeByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileDegree * viewEmpProfileDegree;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileDegreeByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileDegreeByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileDegreeByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileDegreeByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileDegreeByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileDegree * viewEmpProfileDegree;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileDegreeByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileDegree * viewEmpProfileDegree;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileDegreeByViewResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileDegreeByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileDegreeByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileDegreeByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileKnowledgeById : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileKnowledgeById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileKnowledgeByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpProfileKnowledge * GetViewEmpProfileKnowledgeByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileKnowledgeByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpProfileKnowledge * GetViewEmpProfileKnowledgeByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileKnowledgeByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileKnowledge * viewEmpProfileKnowledge;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileKnowledgeByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileKnowledge * viewEmpProfileKnowledge;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileKnowledgeByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileKnowledgeByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileKnowledgeByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileKnowledgeByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileKnowledgeByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileKnowledge * viewEmpProfileKnowledge;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileKnowledgeByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileKnowledge * viewEmpProfileKnowledge;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileKnowledgeByViewResponse : NSObject {
	
/* elements */
	USBoolean * ApproveEmpProfileKnowledgeByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileKnowledgeByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * ApproveEmpProfileKnowledgeByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileKnowledgeByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileKnowledge * viewEmpProfileKnowledge;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileKnowledgeByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileKnowledge * viewEmpProfileKnowledge;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileKnowledgeByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileKnowledgeByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileKnowledgeByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileKnowledgeByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileKnowledgeByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileKnowledge * viewEmpProfileKnowledge;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileKnowledgeByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileKnowledge * viewEmpProfileKnowledge;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileKnowledgeByViewResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileKnowledgeByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileKnowledgeByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileKnowledgeByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataGroupByEmpId : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataGroupByEmpId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataGroupByEmpIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_CBFactorEmployeeMetaDataWithGroup * GetViewCBFactorEmployeeMetaDataGroupByEmpIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataGroupByEmpIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_CBFactorEmployeeMetaDataWithGroup * GetViewCBFactorEmployeeMetaDataGroupByEmpIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataByEmpId : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataByEmpId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataByEmpIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_CBFactorEmployeeMetaData * GetViewCBFactorEmployeeMetaDataByEmpIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataByEmpIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_CBFactorEmployeeMetaData * GetViewCBFactorEmployeeMetaDataByEmpIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddCBFactorEmployeeMetaDataByView : NSObject {
	
/* elements */
	empl_tns1_V_CBFactorEmployeeMetaData * viewCBFactorEmployeeMetaData;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddCBFactorEmployeeMetaDataByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_CBFactorEmployeeMetaData * viewCBFactorEmployeeMetaData;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddCBFactorEmployeeMetaDataByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddCBFactorEmployeeMetaDataByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddCBFactorEmployeeMetaDataByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddCBFactorEmployeeMetaDataByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateCBFactorEmployeeMetaDataByView : NSObject {
	
/* elements */
	empl_tns1_V_CBFactorEmployeeMetaData * viewCBFactorEmployeeMetaData;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateCBFactorEmployeeMetaDataByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_CBFactorEmployeeMetaData * viewCBFactorEmployeeMetaData;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateCBFactorEmployeeMetaDataByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateCBFactorEmployeeMetaDataByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateCBFactorEmployeeMetaDataByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateCBFactorEmployeeMetaDataByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteCBFactorEmployeeMetaDataByView : NSObject {
	
/* elements */
	empl_tns1_V_CBFactorEmployeeMetaData * viewCBFactorEmployeeMetaData;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteCBFactorEmployeeMetaDataByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_CBFactorEmployeeMetaData * viewCBFactorEmployeeMetaData;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteCBFactorEmployeeMetaDataByViewResponse : NSObject {
	
/* elements */
	USBoolean * DeleteCBFactorEmployeeMetaDataByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteCBFactorEmployeeMetaDataByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteCBFactorEmployeeMetaDataByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataChangesByEmpId : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataChangesByEmpId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataChangesByEmpIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_CBFactorEmployeeMetaData * GetViewCBFactorEmployeeMetaDataChangesByEmpIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataChangesByEmpIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_CBFactorEmployeeMetaData * GetViewCBFactorEmployeeMetaDataChangesByEmpIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileMaternityRegimeById : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileMaternityRegimeById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileMaternityRegimeByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfEmpProfileMaternityRegime * GetViewEmpProfileMaternityRegimeByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileMaternityRegimeByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfEmpProfileMaternityRegime * GetViewEmpProfileMaternityRegimeByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileMaternityRegime : NSObject {
	
/* elements */
	empl_tns1_EmpProfileMaternityRegime * tableEmpProfileMaternityRegime;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileMaternityRegime *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileMaternityRegime * tableEmpProfileMaternityRegime;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileMaternityRegimeResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileMaternityRegimeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileMaternityRegimeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileMaternityRegimeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileMaternityRegime : NSObject {
	
/* elements */
	empl_tns1_EmpProfileMaternityRegime * tableEmpProfileMaternityRegime;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileMaternityRegime *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileMaternityRegime * tableEmpProfileMaternityRegime;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileMaternityRegimeResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileMaternityRegimeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileMaternityRegimeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileMaternityRegimeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileMaternityRegime : NSObject {
	
/* elements */
	empl_tns1_EmpProfileMaternityRegime * tableEmpProfileMaternityRegime;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileMaternityRegime *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileMaternityRegime * tableEmpProfileMaternityRegime;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileMaternityRegimeResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileMaternityRegimeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileMaternityRegimeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileMaternityRegimeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetCVPersonalByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetCVPersonalByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetCVPersonalByEmployeeIdResponse : NSObject {
	
/* elements */
	empl_tns2_CVPersonalModel * GetCVPersonalByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetCVPersonalByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns2_CVPersonalModel * GetCVPersonalByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_LoadAllProfileSubAndLayerToTree : NSObject {
	
/* elements */
	NSNumber * intUserId;
	NSNumber * intEmployeeId;
	NSString * strEmployeeCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_LoadAllProfileSubAndLayerToTree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * intUserId;
@property (retain) NSNumber * intEmployeeId;
@property (retain) NSString * strEmployeeCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_LoadAllProfileSubAndLayerToTreeResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_ESS_SYS_PROFILE_TREE * LoadAllProfileSubAndLayerToTreeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_LoadAllProfileSubAndLayerToTreeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_ESS_SYS_PROFILE_TREE * LoadAllProfileSubAndLayerToTreeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetTableEmpGroupMappingByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetTableEmpGroupMappingByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetTableEmpGroupMappingByEmployeeIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfTB_ESS_EMPLOYEE_GROUP_MAPPING * GetTableEmpGroupMappingByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetTableEmpGroupMappingByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfTB_ESS_EMPLOYEE_GROUP_MAPPING * GetTableEmpGroupMappingByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpGroupMappingByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpGroupMappingByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpGroupMappingByEmployeeIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_ESS_EMPLOYEE_GROUP_MAPPING * GetViewEmpGroupMappingByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpGroupMappingByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_ESS_EMPLOYEE_GROUP_MAPPING * GetViewEmpGroupMappingByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetAllEmployeeGroupAndSubGroup : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetAllEmployeeGroupAndSubGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetAllEmployeeGroupAndSubGroupResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_ESS_CLOG_EMPLOYEE_GROUP * GetAllEmployeeGroupAndSubGroupResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetAllEmployeeGroupAndSubGroupResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_ESS_CLOG_EMPLOYEE_GROUP * GetAllEmployeeGroupAndSubGroupResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmployeeToEmployeeGroupAndSubGroup : NSObject {
	
/* elements */
	NSNumber * employeeId;
	NSString * employeeCode;
	empl_tns1_ArrayOfV_ESS_CLOG_EMPLOYEE_GROUP * employeeGroup;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmployeeToEmployeeGroupAndSubGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
@property (retain) NSString * employeeCode;
@property (retain) empl_tns1_ArrayOfV_ESS_CLOG_EMPLOYEE_GROUP * employeeGroup;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmployeeToEmployeeGroupAndSubGroupResponse : NSObject {
	
/* elements */
	USBoolean * AddEmployeeToEmployeeGroupAndSubGroupResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmployeeToEmployeeGroupAndSubGroupResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmployeeToEmployeeGroupAndSubGroupResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetAllEmployeeMappingByEmployeeGroupType : NSObject {
	
/* elements */
	empl_tns3_ArrayOfstring * employeeGroupTypes;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetAllEmployeeMappingByEmployeeGroupType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns3_ArrayOfstring * employeeGroupTypes;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetAllEmployeeMappingByEmployeeGroupTypeResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_ESS_EMPLOYEE_GROUP_MAPPING * GetAllEmployeeMappingByEmployeeGroupTypeResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetAllEmployeeMappingByEmployeeGroupTypeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_ESS_EMPLOYEE_GROUP_MAPPING * GetAllEmployeeMappingByEmployeeGroupTypeResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetListPayScale : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetListPayScale *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetListPayScaleResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfCBSalaryScale * GetListPayScaleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetListPayScaleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfCBSalaryScale * GetListPayScaleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetListPayLevelByPayScale : NSObject {
	
/* elements */
	NSNumber * cbSalaryScaleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetListPayLevelByPayScale *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * cbSalaryScaleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetListPayLevelByPayScaleResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfCBSalaryLevel * GetListPayLevelByPayScaleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetListPayLevelByPayScaleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfCBSalaryLevel * GetListPayLevelByPayScaleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetListPayGradeByPayLevel : NSObject {
	
/* elements */
	NSNumber * cbSalaryLevelId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetListPayGradeByPayLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * cbSalaryLevelId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetListPayGradeByPayLevelResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfCBSalaryGrade * GetListPayGradeByPayLevelResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetListPayGradeByPayLevelResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfCBSalaryGrade * GetListPayGradeByPayLevelResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetCoefficient : NSObject {
	
/* elements */
	NSNumber * cbSalaryGradeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetCoefficient *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * cbSalaryGradeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetCoefficientResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_CBSummarization * GetCoefficientResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetCoefficientResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_CBSummarization * GetCoefficientResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetListSalary : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetListSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetListSalaryResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfCLogCBFactor * GetListSalaryResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetListSalaryResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfCLogCBFactor * GetListSalaryResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetListAllowance : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetListAllowance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetListAllowanceResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfCLogCBFactor * GetListAllowanceResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetListAllowanceResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfCLogCBFactor * GetListAllowanceResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetListInsurance : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetListInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetListInsuranceResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfCLogCBFactor * GetListInsuranceResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetListInsuranceResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfCLogCBFactor * GetListInsuranceResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileAllowanceByView : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileAllowanceByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileAllowanceByViewResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpProfileAllowance * GetViewEmpProfileAllowanceByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileAllowanceByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpProfileAllowance * GetViewEmpProfileAllowanceByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileProfileAllowanceByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileAllowance * viewEmpProfileAllowance;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileProfileAllowanceByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileAllowance * viewEmpProfileAllowance;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileProfileAllowanceByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileProfileAllowanceByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileProfileAllowanceByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileProfileAllowanceByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileAllowanceByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileAllowance * viewEmpProfileAllowance;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileAllowanceByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileAllowance * viewEmpProfileAllowance;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileAllowanceByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileAllowanceByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileAllowanceByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileAllowanceByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileAllowanceByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileAllowance * viewEmpProfileAllowance;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileAllowanceByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileAllowance * viewEmpProfileAllowance;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileAllowanceByViewResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileAllowanceByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileAllowanceByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileAllowanceByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileBaseSalaryByView : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileBaseSalaryByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileBaseSalaryByViewResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpProfileBaseSalary * GetViewEmpProfileBaseSalaryByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileBaseSalaryByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpProfileBaseSalary * GetViewEmpProfileBaseSalaryByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileProfileBaseSalaryByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileBaseSalary * viewEmpProfileBaseSalary;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileProfileBaseSalaryByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileBaseSalary * viewEmpProfileBaseSalary;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileProfileBaseSalaryByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileProfileBaseSalaryByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileProfileBaseSalaryByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileProfileBaseSalaryByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileBaseSalaryByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileBaseSalary * viewEmpProfileBaseSalary;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileBaseSalaryByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileBaseSalary * viewEmpProfileBaseSalary;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileBaseSalaryByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileBaseSalaryByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileBaseSalaryByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileBaseSalaryByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileBaseSalaryByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileBaseSalary * viewEmpProfileBaseSalary;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileBaseSalaryByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileBaseSalary * viewEmpProfileBaseSalary;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileBaseSalaryByViewResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileBaseSalaryByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileBaseSalaryByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileBaseSalaryByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetAllTotalIncomeByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
	empl_tns3_ArrayOfint * months;
	NSString * year;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetAllTotalIncomeByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
@property (retain) empl_tns3_ArrayOfint * months;
@property (retain) NSString * year;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetAllTotalIncomeByEmployeeIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpProfileTotalIncome * GetAllTotalIncomeByEmployeeIdResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetAllTotalIncomeByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpProfileTotalIncome * GetAllTotalIncomeByEmployeeIdResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_HasPermissionOnEmpProfileLayer : NSObject {
	
/* elements */
	NSNumber * UserId;
	NSNumber * CompanyId;
	NSString * Controller;
	NSString * Action;
	NSString * urlType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_HasPermissionOnEmpProfileLayer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * UserId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Controller;
@property (retain) NSString * Action;
@property (retain) NSString * urlType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_HasPermissionOnEmpProfileLayerResponse : NSObject {
	
/* elements */
	USBoolean * HasPermissionOnEmpProfileLayerResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_HasPermissionOnEmpProfileLayerResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * HasPermissionOnEmpProfileLayerResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetPermissionsOnEmpProfileLayer : NSObject {
	
/* elements */
	NSNumber * UserId;
	NSNumber * CompanyId;
	NSString * LayerCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetPermissionsOnEmpProfileLayer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * UserId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * LayerCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetPermissionsOnEmpProfileLayerResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfSP_GET_PERMIS_EMP_PROF_LAYER_Result * GetPermissionsOnEmpProfileLayerResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetPermissionsOnEmpProfileLayerResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfSP_GET_PERMIS_EMP_PROF_LAYER_Result * GetPermissionsOnEmpProfileLayerResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetAllViewSysEmpProfileGroupAndLayers : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetAllViewSysEmpProfileGroupAndLayers *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetAllViewSysEmpProfileGroupAndLayersResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_SysEmpProfileGroupAndLayer * GetAllViewSysEmpProfileGroupAndLayersResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetAllViewSysEmpProfileGroupAndLayersResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_SysEmpProfileGroupAndLayer * GetAllViewSysEmpProfileGroupAndLayersResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewSysEmpProfileGroupAndLayersByUrlType : NSObject {
	
/* elements */
	NSNumber * UserId;
	NSNumber * CompanyId;
	NSString * urlType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewSysEmpProfileGroupAndLayersByUrlType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * UserId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * urlType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewSysEmpProfileGroupAndLayersByUrlTypeResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfSP_GET_EMP_PROF_LAYER_PERMIS_Result * GetViewSysEmpProfileGroupAndLayersByUrlTypeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewSysEmpProfileGroupAndLayersByUrlTypeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfSP_GET_EMP_PROF_LAYER_PERMIS_Result * GetViewSysEmpProfileGroupAndLayersByUrlTypeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpBasicProfileById : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpBasicProfileById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpBasicProfileByIdResponse : NSObject {
	
/* elements */
	empl_tns1_V_EmpBasicProfile * GetViewEmpBasicProfileByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpBasicProfileByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpBasicProfile * GetViewEmpBasicProfileByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpBasicProfileByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpBasicProfile * viewEmpBasicProfile;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpBasicProfileByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpBasicProfile * viewEmpBasicProfile;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpBasicProfileByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpBasicProfileByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpBasicProfileByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpBasicProfileByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpBasicProfileByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpBasicProfile * viewEmpBasicProfile;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpBasicProfileByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpBasicProfile * viewEmpBasicProfile;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpBasicProfileByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpBasicProfileByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpBasicProfileByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpBasicProfileByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpBasicProfileByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpBasicProfile * viewEmpBasicProfile;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpBasicProfileByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpBasicProfile * viewEmpBasicProfile;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpBasicProfileByViewResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpBasicProfileByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpBasicProfileByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpBasicProfileByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfilePersonalityById : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfilePersonalityById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfilePersonalityByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpProfilePersonality * GetViewEmpProfilePersonalityByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfilePersonalityByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpProfilePersonality * GetViewEmpProfilePersonalityByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfilePersonalityByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfilePersonality * viewEmpProfilePersonality;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfilePersonalityByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfilePersonality * viewEmpProfilePersonality;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfilePersonalityByViewResponse : NSObject {
	
/* elements */
	USBoolean * ApproveEmpProfilePersonalityByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfilePersonalityByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * ApproveEmpProfilePersonalityByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfilePersonalityByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfilePersonality * viewEmpProfilePersonality;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfilePersonalityByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfilePersonality * viewEmpProfilePersonality;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfilePersonalityByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfilePersonalityByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfilePersonalityByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfilePersonalityByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfilePersonalityByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfilePersonality * viewEmpProfilePersonality;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfilePersonalityByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfilePersonality * viewEmpProfilePersonality;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfilePersonalityByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfilePersonalityByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfilePersonalityByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfilePersonalityByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfilePersonalityByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfilePersonality * viewEmpProfilePersonality;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfilePersonalityByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfilePersonality * viewEmpProfilePersonality;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfilePersonalityByViewResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfilePersonalityByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfilePersonalityByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfilePersonalityByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileHobbyById : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileHobbyById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileHobbyByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpProfileHobby * GetViewEmpProfileHobbyByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileHobbyByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpProfileHobby * GetViewEmpProfileHobbyByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileHobbyByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileHobby * viewEmpProfileHobby;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileHobbyByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileHobby * viewEmpProfileHobby;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileHobbyByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileHobbyByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileHobbyByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileHobbyByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileHobbyByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileHobby * viewEmpProfileHobby;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileHobbyByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileHobby * viewEmpProfileHobby;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileHobbyByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileHobbyByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileHobbyByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileHobbyByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileHobbyByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileHobby * viewEmpProfileHobby;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileHobbyByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileHobby * viewEmpProfileHobby;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileHobbyByViewResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileHobbyByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileHobbyByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileHobbyByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpSocialInsuranceSalaryById : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpSocialInsuranceSalaryById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpSocialInsuranceSalaryByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpSocialInsuranceSalary * GetViewEmpSocialInsuranceSalaryByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpSocialInsuranceSalaryByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpSocialInsuranceSalary * GetViewEmpSocialInsuranceSalaryByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpSocialInsuranceSalaryByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpSocialInsuranceSalary * viewEmpSocialInsuranceSalary;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpSocialInsuranceSalaryByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpSocialInsuranceSalary * viewEmpSocialInsuranceSalary;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpSocialInsuranceSalaryByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpSocialInsuranceSalaryByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpSocialInsuranceSalaryByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpSocialInsuranceSalaryByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpSocialInsuranceSalaryByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpSocialInsuranceSalary * viewEmpSocialInsuranceSalary;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpSocialInsuranceSalaryByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpSocialInsuranceSalary * viewEmpSocialInsuranceSalary;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpSocialInsuranceSalaryByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpSocialInsuranceSalaryByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpSocialInsuranceSalaryByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpSocialInsuranceSalaryByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpSocialInsuranceSalaryByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpSocialInsuranceSalary * viewEmpSocialInsuranceSalary;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpSocialInsuranceSalaryByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpSocialInsuranceSalary * viewEmpSocialInsuranceSalary;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpSocialInsuranceSalaryByViewResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpSocialInsuranceSalaryByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpSocialInsuranceSalaryByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpSocialInsuranceSalaryByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileBiographyById : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileBiographyById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileBiographyByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfEmpProfileBiography * GetViewEmpProfileBiographyByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileBiographyByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfEmpProfileBiography * GetViewEmpProfileBiographyByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileBiography : NSObject {
	
/* elements */
	empl_tns1_EmpProfileBiography * tableEmpProfileBiography;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileBiography *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileBiography * tableEmpProfileBiography;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileBiographyResponse : NSObject {
	
/* elements */
	USBoolean * ApproveEmpProfileBiographyResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileBiographyResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * ApproveEmpProfileBiographyResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileBiography : NSObject {
	
/* elements */
	empl_tns1_EmpProfileBiography * tableEmpProfileBiography;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileBiography *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileBiography * tableEmpProfileBiography;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileBiographyResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileBiographyResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileBiographyResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileBiographyResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileBiography : NSObject {
	
/* elements */
	empl_tns1_EmpProfileBiography * tableEmpProfileBiography;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileBiography *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileBiography * tableEmpProfileBiography;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileBiographyResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileBiographyResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileBiographyResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileBiographyResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileBiography : NSObject {
	
/* elements */
	empl_tns1_EmpProfileBiography * tableEmpProfileBiography;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileBiography *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileBiography * tableEmpProfileBiography;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileBiographyResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileBiographyResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileBiographyResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileBiographyResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpFamilyRelationshipById : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpFamilyRelationshipById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpFamilyRelationshipByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpProfileFamilyRelationship * GetViewEmpFamilyRelationshipByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpFamilyRelationshipByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpProfileFamilyRelationship * GetViewEmpFamilyRelationshipByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileFamilyRelationshipByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileFamilyRelationship * viewEmpProfileFamilyRelationship;
	USBoolean * isChangeMode;
	empl_tns3_ArrayOfint * changeIds;
	NSString * addChangeCauseVN;
	NSString * addChangeCauseEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileFamilyRelationshipByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileFamilyRelationship * viewEmpProfileFamilyRelationship;
@property (retain) USBoolean * isChangeMode;
@property (retain) empl_tns3_ArrayOfint * changeIds;
@property (retain) NSString * addChangeCauseVN;
@property (retain) NSString * addChangeCauseEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileFamilyRelationshipByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileFamilyRelationshipByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileFamilyRelationshipByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileFamilyRelationshipByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileFamilyRelationshipByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileFamilyRelationship * viewEmpProfileFamilyRelationship;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
	empl_tns3_ArrayOfint * changeIds;
	NSString * addChangeCauseVN;
	NSString * addChangeCauseEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileFamilyRelationshipByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileFamilyRelationship * viewEmpProfileFamilyRelationship;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
@property (retain) empl_tns3_ArrayOfint * changeIds;
@property (retain) NSString * addChangeCauseVN;
@property (retain) NSString * addChangeCauseEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileFamilyRelationshipByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileFamilyRelationshipByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileFamilyRelationshipByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileFamilyRelationshipByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileFamilyRelationshipByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileFamilyRelationship * viewEmpProfileFamilyRelationship;
	empl_tns3_ArrayOfint * changeIds;
	NSString * addChangeCauseVN;
	NSString * addChangeCauseEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileFamilyRelationshipByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileFamilyRelationship * viewEmpProfileFamilyRelationship;
@property (retain) empl_tns3_ArrayOfint * changeIds;
@property (retain) NSString * addChangeCauseVN;
@property (retain) NSString * addChangeCauseEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileFamilyRelationshipByViewResponse : NSObject {
	
/* elements */
	USBoolean * ApproveEmpProfileFamilyRelationshipByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileFamilyRelationshipByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * ApproveEmpProfileFamilyRelationshipByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileFamilyRelationshipByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileFamilyRelationship * viewEmpProfileFamilyRelationship;
	USBoolean * isChangeMode;
	empl_tns3_ArrayOfint * changeIds;
	NSString * addChangeCauseVN;
	NSString * addChangeCauseEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileFamilyRelationshipByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileFamilyRelationship * viewEmpProfileFamilyRelationship;
@property (retain) USBoolean * isChangeMode;
@property (retain) empl_tns3_ArrayOfint * changeIds;
@property (retain) NSString * addChangeCauseVN;
@property (retain) NSString * addChangeCauseEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileFamilyRelationshipByViewResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileFamilyRelationshipByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileFamilyRelationshipByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileFamilyRelationshipByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileContactsByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileContactsByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileContactsByEmployeeIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpProfileContact * GetViewEmpProfileContactsByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileContactsByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpProfileContact * GetViewEmpProfileContactsByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileContactByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileContact * viewEmpProfileContact;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileContactByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileContact * viewEmpProfileContact;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileContactByViewResponse : NSObject {
	
/* elements */
	USBoolean * ApproveEmpProfileContactByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileContactByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * ApproveEmpProfileContactByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileContactByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileContact * viewEmpProfileContact;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileContactByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileContact * viewEmpProfileContact;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileContactByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileContactByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileContactByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileContactByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileContactByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileContact * viewEmpProfileContact;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileContactByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileContact * viewEmpProfileContact;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileContactByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileContactByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileContactByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileContactByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileContactByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileContact * viewEmpProfileContact;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileContactByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileContact * viewEmpProfileContact;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileContactByViewResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileContactByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileContactByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileContactByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileHealthy : NSObject {
	
/* elements */
	empl_tns1_EmpProfileHealthy * tableEmpProfileHealthy;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileHealthy *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileHealthy * tableEmpProfileHealthy;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileHealthyResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileHealthyResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileHealthyResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileHealthyResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileHealthy : NSObject {
	
/* elements */
	empl_tns1_EmpProfileHealthy * tableEmpProfileHealthy;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileHealthy *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileHealthy * tableEmpProfileHealthy;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileHealthyResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileHealthyResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileHealthyResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileHealthyResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileHealthy : NSObject {
	
/* elements */
	empl_tns1_EmpProfileHealthy * tableEmpProfileHealthy;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileHealthy *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileHealthy * tableEmpProfileHealthy;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileHealthyResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileHealthyResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileHealthyResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileHealthyResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileParticipationById : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileParticipationById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileParticipationByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfEmpProfileParticipation * GetViewEmpProfileParticipationByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileParticipationByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfEmpProfileParticipation * GetViewEmpProfileParticipationByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileParticipation : NSObject {
	
/* elements */
	empl_tns1_EmpProfileParticipation * tableEmpProfileParticipation;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileParticipation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileParticipation * tableEmpProfileParticipation;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileParticipationResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileParticipationResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileParticipationResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileParticipationResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileParticipation : NSObject {
	
/* elements */
	empl_tns1_EmpProfileParticipation * tableEmpProfileParticipation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileParticipation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileParticipation * tableEmpProfileParticipation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileParticipationResponse : NSObject {
	
/* elements */
	USBoolean * ApproveEmpProfileParticipationResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileParticipationResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * ApproveEmpProfileParticipationResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileParticipation : NSObject {
	
/* elements */
	empl_tns1_EmpProfileParticipation * tableEmpProfileParticipation;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileParticipation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileParticipation * tableEmpProfileParticipation;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileParticipationResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileParticipationResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileParticipationResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileParticipationResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileParticipation : NSObject {
	
/* elements */
	empl_tns1_EmpProfileParticipation * tableEmpProfileParticipation;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileParticipation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileParticipation * tableEmpProfileParticipation;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileParticipationResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileParticipationResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileParticipationResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileParticipationResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetActiveEmpProfileJobPositionByEmpId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetActiveEmpProfileJobPositionByEmpId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetActiveEmpProfileJobPositionByEmpIdResponse : NSObject {
	
/* elements */
	empl_tns1_EmpProfileJobPosition * GetActiveEmpProfileJobPositionByEmpIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetActiveEmpProfileJobPositionByEmpIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileJobPosition * GetActiveEmpProfileJobPositionByEmpIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileJobPositions : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileJobPositions *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileJobPositionsResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpProfileJobPosition * GetViewEmpProfileJobPositionsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileJobPositionsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpProfileJobPosition * GetViewEmpProfileJobPositionsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileJobPositionByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileJobPosition * viewEmpProfileJobPosition;
	USBoolean * isChangeMode;
	empl_tns3_ArrayOfint * changeIds;
	NSString * addChangeCauseVN;
	NSString * addChangeCauseEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileJobPositionByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileJobPosition * viewEmpProfileJobPosition;
@property (retain) USBoolean * isChangeMode;
@property (retain) empl_tns3_ArrayOfint * changeIds;
@property (retain) NSString * addChangeCauseVN;
@property (retain) NSString * addChangeCauseEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileJobPositionByViewResponse : NSObject {
	
/* elements */
	empl_tns2_MsgNotifyModel * AddEmpProfileJobPositionByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileJobPositionByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns2_MsgNotifyModel * AddEmpProfileJobPositionByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositionByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileJobPosition * viewEmpProfileJobPosition;
	USBoolean * isOverwrite;
	USBoolean * isChangeMode;
	empl_tns3_ArrayOfint * changeIds;
	NSString * addChangeCauseVN;
	NSString * addChangeCauseEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositionByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileJobPosition * viewEmpProfileJobPosition;
@property (retain) USBoolean * isOverwrite;
@property (retain) USBoolean * isChangeMode;
@property (retain) empl_tns3_ArrayOfint * changeIds;
@property (retain) NSString * addChangeCauseVN;
@property (retain) NSString * addChangeCauseEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositionByViewResponse : NSObject {
	
/* elements */
	empl_tns2_MsgNotifyModel * UpdateEmpProfileJobPositionByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositionByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns2_MsgNotifyModel * UpdateEmpProfileJobPositionByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileJobPositionByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileJobPosition * viewEmpProfileJobPosition;
	USBoolean * isChangeMode;
	empl_tns3_ArrayOfint * changeIds;
	NSString * addChangeCauseVN;
	NSString * addChangeCauseEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileJobPositionByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileJobPosition * viewEmpProfileJobPosition;
@property (retain) USBoolean * isChangeMode;
@property (retain) empl_tns3_ArrayOfint * changeIds;
@property (retain) NSString * addChangeCauseVN;
@property (retain) NSString * addChangeCauseEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileJobPositionByViewResponse : NSObject {
	
/* elements */
	empl_tns2_MsgNotifyModel * DeleteEmpProfileJobPositionByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileJobPositionByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns2_MsgNotifyModel * DeleteEmpProfileJobPositionByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositions : NSObject {
	
/* elements */
	empl_tns1_ArrayOfEmpProfileJobPosition * listEmpProfileJobPosition;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositions *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfEmpProfileJobPosition * listEmpProfileJobPosition;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositionsResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileJobPositionsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositionsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileJobPositionsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileProcessOfWorkById : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileProcessOfWorkById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileProcessOfWorkByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpProfileProcessOfWork * GetViewEmpProfileProcessOfWorkByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileProcessOfWorkByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpProfileProcessOfWork * GetViewEmpProfileProcessOfWorkByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileProcessOfWork : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileProcessOfWork * tableEmpProfileProcessOfWork;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileProcessOfWork *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileProcessOfWork * tableEmpProfileProcessOfWork;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileProcessOfWorkResponse : NSObject {
	
/* elements */
	empl_tns2_MsgNotifyModel * AddEmpProfileProcessOfWorkResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileProcessOfWorkResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns2_MsgNotifyModel * AddEmpProfileProcessOfWorkResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileProcessOfWork : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileProcessOfWork * tableEmpProfileProcessOfWork;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileProcessOfWork *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileProcessOfWork * tableEmpProfileProcessOfWork;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileProcessOfWorkResponse : NSObject {
	
/* elements */
	empl_tns2_MsgNotifyModel * UpdateEmpProfileProcessOfWorkResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileProcessOfWorkResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns2_MsgNotifyModel * UpdateEmpProfileProcessOfWorkResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileProcessOfWork : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileProcessOfWork * tableEmpProfileProcessOfWork;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileProcessOfWork *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileProcessOfWork * tableEmpProfileProcessOfWork;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileProcessOfWorkResponse : NSObject {
	
/* elements */
	empl_tns2_MsgNotifyModel * DeleteEmpProfileProcessOfWorkResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileProcessOfWorkResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns2_MsgNotifyModel * DeleteEmpProfileProcessOfWorkResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileContractById : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileContractById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileContractByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpContract * GetViewEmpProfileContractByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileContractByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpContract * GetViewEmpProfileContractByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpContractByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpContract * viewEmpContract;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpContractByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpContract * viewEmpContract;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpContractByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpContractByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpContractByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpContractByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpContractByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpContract * viewEmpContract;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpContractByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpContract * viewEmpContract;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpContractByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpContractByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpContractByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpContractByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpContractByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpContract * viewEmpContract;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpContractByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpContract * viewEmpContract;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpContractByViewResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpContractByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpContractByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpContractByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewJobPositionDescriptionByEmployeeId : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewJobPositionDescriptionByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewJobPositionDescriptionByEmployeeIdResponse : NSObject {
	
/* elements */
	empl_tns4_ArrayOfEmpJobDescriptionModel * GetViewJobPositionDescriptionByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewJobPositionDescriptionByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns4_ArrayOfEmpJobDescriptionModel * GetViewJobPositionDescriptionByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewJobPositionConcurrentById : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewJobPositionConcurrentById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewJobPositionConcurrentByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpProfileJobPosition * GetViewJobPositionConcurrentByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewJobPositionConcurrentByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpProfileJobPosition * GetViewJobPositionConcurrentByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileJobPositionConcurrentByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileJobPosition * viewEmpProfileJobPosition;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileJobPositionConcurrentByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileJobPosition * viewEmpProfileJobPosition;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileJobPositionConcurrentByViewResponse : NSObject {
	
/* elements */
	empl_tns2_MsgNotifyModel * AddEmpProfileJobPositionConcurrentByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileJobPositionConcurrentByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns2_MsgNotifyModel * AddEmpProfileJobPositionConcurrentByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositionConcurrentByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileJobPosition * viewEmpProfileJobPosition;
	USBoolean * isOverwrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositionConcurrentByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileJobPosition * viewEmpProfileJobPosition;
@property (retain) USBoolean * isOverwrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositionConcurrentByViewResponse : NSObject {
	
/* elements */
	empl_tns2_MsgNotifyModel * UpdateEmpProfileJobPositionConcurrentByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositionConcurrentByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns2_MsgNotifyModel * UpdateEmpProfileJobPositionConcurrentByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileJobPositionConcurrentByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileJobPosition * viewEmpProfileJobPosition;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileJobPositionConcurrentByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileJobPosition * viewEmpProfileJobPosition;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileJobPositionConcurrentByViewResponse : NSObject {
	
/* elements */
	empl_tns2_MsgNotifyModel * DeleteEmpProfileJobPositionConcurrentByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileJobPositionConcurrentByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns2_MsgNotifyModel * DeleteEmpProfileJobPositionConcurrentByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewRequestEquipmentMasterByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewRequestEquipmentMasterByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewRequestEquipmentMasterByEmployeeIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_ESS_REQ_EQUIPMENT_MASTER * GetViewRequestEquipmentMasterByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewRequestEquipmentMasterByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_ESS_REQ_EQUIPMENT_MASTER * GetViewRequestEquipmentMasterByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewRequestEquipmentDetail : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewRequestEquipmentDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewRequestEquipmentDetailResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_ESS_REQ_EQUIPMENT_DETAIL * GetViewRequestEquipmentDetailResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewRequestEquipmentDetailResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_ESS_REQ_EQUIPMENT_DETAIL * GetViewRequestEquipmentDetailResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewRequestEquipmentDetailByRequestId : NSObject {
	
/* elements */
	NSNumber * reqMasterId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewRequestEquipmentDetailByRequestId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * reqMasterId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewRequestEquipmentDetailByRequestIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_ESS_REQ_EQUIPMENT_DETAIL * GetViewRequestEquipmentDetailByRequestIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewRequestEquipmentDetailByRequestIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_ESS_REQ_EQUIPMENT_DETAIL * GetViewRequestEquipmentDetailByRequestIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileEquipmentById : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileEquipmentById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileEquipmentByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpProfileEquipment * GetViewEmpProfileEquipmentByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileEquipmentByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpProfileEquipment * GetViewEmpProfileEquipmentByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileEquipmentByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileEquipment * viewEmpProfileEquipment;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileEquipmentByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileEquipment * viewEmpProfileEquipment;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileEquipmentByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileEquipmentByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileEquipmentByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileEquipmentByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileEquipmentByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileEquipment * viewEmpProfileEquipment;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileEquipmentByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileEquipment * viewEmpProfileEquipment;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileEquipmentByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileEquipmentByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileEquipmentByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileEquipmentByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileEquipmentByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileEquipment * viewEmpProfileEquipment;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileEquipmentByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileEquipment * viewEmpProfileEquipment;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileEquipmentByViewResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileEquipmentByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileEquipmentByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileEquipmentByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileAssetsById : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileAssetsById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileAssetsByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpProfileAssets * GetViewEmpProfileAssetsByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileAssetsByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpProfileAssets * GetViewEmpProfileAssetsByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileAssetsByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileAssets * viewEmpProfileAssets;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileAssetsByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileAssets * viewEmpProfileAssets;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileAssetsByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileAssetsByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileAssetsByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileAssetsByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileAssetsByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileAssets * viewEmpProfileAssets;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileAssetsByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileAssets * viewEmpProfileAssets;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileAssetsByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileAssetsByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileAssetsByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileAssetsByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileAssetsByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileAssets * viewEmpProfileAssets;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileAssetsByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileAssets * viewEmpProfileAssets;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileAssetsByViewResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileAssetsByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileAssetsByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileAssetsByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileWageTypeById : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileWageTypeById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileWageTypeByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpProfileWageType * GetViewEmpProfileWageTypeByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileWageTypeByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpProfileWageType * GetViewEmpProfileWageTypeByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileWageTypeByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileWageType * viewEmpProfileWageType;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileWageTypeByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileWageType * viewEmpProfileWageType;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileWageTypeByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileWageTypeByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileWageTypeByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileWageTypeByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileWageTypeByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileWageType * viewEmpProfileWageType;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileWageTypeByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileWageType * viewEmpProfileWageType;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileWageTypeByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileWageTypeByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileWageTypeByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileWageTypeByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileWageTypeByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileWageType * viewEmpProfileWageType;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileWageTypeByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileWageType * viewEmpProfileWageType;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileWageTypeByViewResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileWageTypeByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileWageTypeByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileWageTypeByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingFormById : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingFormById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingFormByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpProfileWorkingForm * GetViewEmpProfileWorkingFormByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingFormByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpProfileWorkingForm * GetViewEmpProfileWorkingFormByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileWorkingFormByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileWorkingForm * viewEmpProfileWorkingForm;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileWorkingFormByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileWorkingForm * viewEmpProfileWorkingForm;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileWorkingFormByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileWorkingFormByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileWorkingFormByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileWorkingFormByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileWorkingFormByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileWorkingForm * viewEmpProfileWorkingForm;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileWorkingFormByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileWorkingForm * viewEmpProfileWorkingForm;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileWorkingFormByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileWorkingFormByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileWorkingFormByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileWorkingFormByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileWorkingFormByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileWorkingForm * viewEmpProfileWorkingForm;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileWorkingFormByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileWorkingForm * viewEmpProfileWorkingForm;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileWorkingFormByViewResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileWorkingFormByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileWorkingFormByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileWorkingFormByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingExperienceById : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingExperienceById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingExperienceByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpProfileWorkingExperience * GetViewEmpProfileWorkingExperienceByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingExperienceByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpProfileWorkingExperience * GetViewEmpProfileWorkingExperienceByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileWorkingExperienceByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileWorkingExperience * viewEmpProfileWorkingExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileWorkingExperienceByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileWorkingExperience * viewEmpProfileWorkingExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileWorkingExperienceByViewResponse : NSObject {
	
/* elements */
	USBoolean * ApproveEmpProfileWorkingExperienceByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileWorkingExperienceByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * ApproveEmpProfileWorkingExperienceByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileWorkingExperienceByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileWorkingExperience * viewEmpProfileWorkingExperience;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileWorkingExperienceByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileWorkingExperience * viewEmpProfileWorkingExperience;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileWorkingExperienceByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileWorkingExperienceByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileWorkingExperienceByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileWorkingExperienceByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileWorkingExperienceByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileWorkingExperience * viewEmpProfileWorkingExperience;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileWorkingExperienceByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileWorkingExperience * viewEmpProfileWorkingExperience;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileWorkingExperienceByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileWorkingExperienceByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileWorkingExperienceByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileWorkingExperienceByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileWorkingExperienceByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileWorkingExperience * viewEmpProfileWorkingExperience;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileWorkingExperienceByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileWorkingExperience * viewEmpProfileWorkingExperience;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileWorkingExperienceByViewResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileWorkingExperienceByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileWorkingExperienceByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileWorkingExperienceByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileExperienceById : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileExperienceById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileExperienceByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfEmpProfileExperience * GetViewEmpProfileExperienceByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileExperienceByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfEmpProfileExperience * GetViewEmpProfileExperienceByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileExperience : NSObject {
	
/* elements */
	empl_tns1_EmpProfileExperience * tableEmpProfileExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileExperience * tableEmpProfileExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileExperienceResponse : NSObject {
	
/* elements */
	USBoolean * ApproveEmpProfileExperienceResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileExperienceResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * ApproveEmpProfileExperienceResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileExperience : NSObject {
	
/* elements */
	empl_tns1_EmpProfileExperience * tableEmpProfileExperience;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileExperience * tableEmpProfileExperience;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileExperienceResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileExperienceResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileExperienceResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileExperienceResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileExperience : NSObject {
	
/* elements */
	empl_tns1_EmpProfileExperience * tableEmpProfileExperience;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileExperience * tableEmpProfileExperience;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileExperienceResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileExperienceResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileExperienceResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileExperienceResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileExperience : NSObject {
	
/* elements */
	empl_tns1_EmpProfileExperience * tableEmpProfileExperience;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileExperience * tableEmpProfileExperience;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileExperienceResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileExperienceResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileExperienceResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileExperienceResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileLeaveRegime : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileLeaveRegime *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileLeaveRegimeResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfEmpProfileLeaveRegime * GetViewEmpProfileLeaveRegimeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileLeaveRegimeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfEmpProfileLeaveRegime * GetViewEmpProfileLeaveRegimeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileLeaveRegime : NSObject {
	
/* elements */
	empl_tns1_EmpProfileLeaveRegime * tableEmpProfileLeaveRegime;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileLeaveRegime *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileLeaveRegime * tableEmpProfileLeaveRegime;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileLeaveRegimeResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileLeaveRegimeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileLeaveRegimeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileLeaveRegimeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileLeaveRegime : NSObject {
	
/* elements */
	empl_tns1_EmpProfileLeaveRegime * tableEmpProfileLeaveRegime;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileLeaveRegime *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileLeaveRegime * tableEmpProfileLeaveRegime;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileLeaveRegimeResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileLeaveRegimeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileLeaveRegimeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileLeaveRegimeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileLeaveRegime : NSObject {
	
/* elements */
	empl_tns1_EmpProfileLeaveRegime * tableEmpProfileLeaveRegime;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileLeaveRegime *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileLeaveRegime * tableEmpProfileLeaveRegime;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileLeaveRegimeResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileLeaveRegimeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileLeaveRegimeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileLeaveRegimeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileCommentById : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileCommentById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileCommentByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpProfileComment * GetViewEmpProfileCommentByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileCommentByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpProfileComment * GetViewEmpProfileCommentByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileComment : NSObject {
	
/* elements */
	empl_tns1_EmpProfileComment * tableEmpProfileComment;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileComment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileComment * tableEmpProfileComment;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileCommentResponse : NSObject {
	
/* elements */
	USBoolean * ApproveEmpProfileCommentResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileCommentResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * ApproveEmpProfileCommentResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileComment : NSObject {
	
/* elements */
	empl_tns1_EmpProfileComment * tableEmpProfileComment;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileComment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileComment * tableEmpProfileComment;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileCommentResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileCommentResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileCommentResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileCommentResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileComment : NSObject {
	
/* elements */
	empl_tns1_EmpProfileComment * tableEmpProfileComment;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileComment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileComment * tableEmpProfileComment;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileCommentResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileCommentResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileCommentResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileCommentResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileComment : NSObject {
	
/* elements */
	empl_tns1_EmpProfileComment * tableEmpProfileComment;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileComment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileComment * tableEmpProfileComment;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileCommentResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileCommentResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileCommentResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileCommentResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileTrainingById : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileTrainingById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileTrainingByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfEmpProfileTraining * GetViewEmpProfileTrainingByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileTrainingByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfEmpProfileTraining * GetViewEmpProfileTrainingByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileTraining : NSObject {
	
/* elements */
	empl_tns1_EmpProfileTraining * tableEmpProfileTraining;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileTraining *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileTraining * tableEmpProfileTraining;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileTrainingResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileTrainingResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileTrainingResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileTrainingResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileTraining : NSObject {
	
/* elements */
	empl_tns1_EmpProfileTraining * tableEmpProfileTraining;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileTraining *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileTraining * tableEmpProfileTraining;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileTrainingResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileTrainingResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileTrainingResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileTrainingResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileTraining : NSObject {
	
/* elements */
	empl_tns1_EmpProfileTraining * tableEmpProfileTraining;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileTraining *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileTraining * tableEmpProfileTraining;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileTrainingResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileTrainingResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileTrainingResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileTrainingResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileTraining : NSObject {
	
/* elements */
	empl_tns1_EmpProfileTraining * tableEmpProfileTraining;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileTraining *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileTraining * tableEmpProfileTraining;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileTrainingResponse : NSObject {
	
/* elements */
	USBoolean * ApproveEmpProfileTrainingResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileTrainingResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * ApproveEmpProfileTrainingResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileDisciplineById : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileDisciplineById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileDisciplineByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpProfileDiscipline * GetViewEmpProfileDisciplineByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileDisciplineByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpProfileDiscipline * GetViewEmpProfileDisciplineByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileDisciplineByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileDiscipline * viewEmpProfileDiscipline;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileDisciplineByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileDiscipline * viewEmpProfileDiscipline;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileDisciplineByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileDisciplineByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileDisciplineByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileDisciplineByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileDisciplineByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileDiscipline * viewEmpProfileDiscipline;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileDisciplineByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileDiscipline * viewEmpProfileDiscipline;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileDisciplineByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileDisciplineByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileDisciplineByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileDisciplineByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileDisciplineByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileDiscipline * viewEmpProfileDiscipline;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileDisciplineByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileDiscipline * viewEmpProfileDiscipline;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileDisciplineByViewResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileDisciplineByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileDisciplineByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileDisciplineByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpPerformanceAppraisalById : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpPerformanceAppraisalById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpPerformanceAppraisalByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpPerformanceAppraisal * GetViewEmpPerformanceAppraisalByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpPerformanceAppraisalByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpPerformanceAppraisal * GetViewEmpPerformanceAppraisalByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpPerformanceAppraisalByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpPerformanceAppraisal * viewEmpPerformanceAppraisal;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpPerformanceAppraisalByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpPerformanceAppraisal * viewEmpPerformanceAppraisal;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpPerformanceAppraisalByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpPerformanceAppraisalByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpPerformanceAppraisalByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpPerformanceAppraisalByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpPerformanceAppraisalByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpPerformanceAppraisal * viewEmpPerformanceAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpPerformanceAppraisalByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpPerformanceAppraisal * viewEmpPerformanceAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpPerformanceAppraisalByViewResponse : NSObject {
	
/* elements */
	USBoolean * ApproveEmpPerformanceAppraisalByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpPerformanceAppraisalByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * ApproveEmpPerformanceAppraisalByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpPerformanceAppraisalByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpPerformanceAppraisal * viewEmpPerformanceAppraisal;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpPerformanceAppraisalByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpPerformanceAppraisal * viewEmpPerformanceAppraisal;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpPerformanceAppraisalByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpPerformanceAppraisalByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpPerformanceAppraisalByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpPerformanceAppraisalByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpPerformanceAppraisalByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpPerformanceAppraisal * viewEmpPerformanceAppraisal;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpPerformanceAppraisalByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpPerformanceAppraisal * viewEmpPerformanceAppraisal;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpPerformanceAppraisalByViewResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpPerformanceAppraisalByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpPerformanceAppraisalByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpPerformanceAppraisalByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileSocialInsuranceById : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileSocialInsuranceById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileSocialInsuranceByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfEmpProfileSocialInsurance * GetViewEmpProfileSocialInsuranceByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileSocialInsuranceByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfEmpProfileSocialInsurance * GetViewEmpProfileSocialInsuranceByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileSocialInsurance : NSObject {
	
/* elements */
	empl_tns1_EmpProfileSocialInsurance * tableEmpProfileSocialInsurance;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileSocialInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileSocialInsurance * tableEmpProfileSocialInsurance;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileSocialInsuranceResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileSocialInsuranceResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileSocialInsuranceResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileSocialInsuranceResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileSocialInsurance : NSObject {
	
/* elements */
	empl_tns1_EmpProfileSocialInsurance * tableEmpProfileSocialInsurance;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
	NSNumber * masterId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileSocialInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileSocialInsurance * tableEmpProfileSocialInsurance;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
@property (retain) NSNumber * masterId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileSocialInsuranceResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileSocialInsuranceResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileSocialInsuranceResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileSocialInsuranceResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileSocialInsurance : NSObject {
	
/* elements */
	empl_tns1_EmpProfileSocialInsurance * tableEmpProfileSocialInsurance;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileSocialInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_EmpProfileSocialInsurance * tableEmpProfileSocialInsurance;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileSocialInsuranceResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileSocialInsuranceResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileSocialInsuranceResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileSocialInsuranceResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileHealthInsuranceById : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileHealthInsuranceById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileHealthInsuranceByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpProfileHealthInsurance * GetViewEmpProfileHealthInsuranceByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileHealthInsuranceByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpProfileHealthInsurance * GetViewEmpProfileHealthInsuranceByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileHealthInsuranceByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileHealthInsurance * viewEmpProfileHealthInsurance;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileHealthInsuranceByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileHealthInsurance * viewEmpProfileHealthInsurance;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileHealthInsuranceByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileHealthInsuranceByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileHealthInsuranceByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileHealthInsuranceByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileHealthInsuranceByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileHealthInsurance * viewEmpProfileHealthInsurance;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileHealthInsuranceByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileHealthInsurance * viewEmpProfileHealthInsurance;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileHealthInsuranceByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileHealthInsuranceByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileHealthInsuranceByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileHealthInsuranceByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileHealthInsuranceByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileHealthInsurance * viewEmpProfileHealthInsurance;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileHealthInsuranceByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileHealthInsurance * viewEmpProfileHealthInsurance;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileHealthInsuranceByViewResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileHealthInsuranceByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileHealthInsuranceByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileHealthInsuranceByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileOtherBenefitByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileOtherBenefit * viewEmpProfileOtherBenefit;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileOtherBenefitByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileOtherBenefit * viewEmpProfileOtherBenefit;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_AddEmpProfileOtherBenefitByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpProfileOtherBenefitByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_AddEmpProfileOtherBenefitByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpProfileOtherBenefitByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileOtherBenefitByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileOtherBenefit * viewEmpProfileOtherBenefit;
	USBoolean * isOverWrite;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileOtherBenefitByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileOtherBenefit * viewEmpProfileOtherBenefit;
@property (retain) USBoolean * isOverWrite;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_UpdateEmpProfileOtherBenefitByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpProfileOtherBenefitByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_UpdateEmpProfileOtherBenefitByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpProfileOtherBenefitByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileOtherBenefitByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileOtherBenefit * viewEmpProfileOtherBenefit;
	USBoolean * isChangeMode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileOtherBenefitByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileOtherBenefit * viewEmpProfileOtherBenefit;
@property (retain) USBoolean * isChangeMode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_DeleteEmpProfileOtherBenefitByViewResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpProfileOtherBenefitByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_DeleteEmpProfileOtherBenefitByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpProfileOtherBenefitByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileQualificationById : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileQualificationById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_GetViewEmpProfileQualificationByIdResponse : NSObject {
	
/* elements */
	empl_tns1_ArrayOfV_EmpProfileQualification * GetViewEmpProfileQualificationByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_GetViewEmpProfileQualificationByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_ArrayOfV_EmpProfileQualification * GetViewEmpProfileQualificationByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileQualificationByView : NSObject {
	
/* elements */
	empl_tns1_V_EmpProfileQualification * viewEmpProfileQualification;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileQualificationByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) empl_tns1_V_EmpProfileQualification * viewEmpProfileQualification;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmpProfileLayerServiceSvc_ApproveEmpProfileQualificationByViewResponse : NSObject {
	
/* elements */
	USBoolean * ApproveEmpProfileQualificationByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmpProfileLayerServiceSvc_ApproveEmpProfileQualificationByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * ApproveEmpProfileQualificationByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
/* Cookies handling provided by http://en.wikibooks.org/wiki/Programming:WebObjects/Web_Services/Web_Service_Provider */
#import <libxml/parser.h>
#import "xs.h"
#import "EmpProfileLayerServiceSvc.h"
#import "ns1.h"
#import "empl_tns1.h"
#import "empl_tns5.h"
#import "empl_tns2.h"
#import "empl_tns3.h"
#import "empl_tns4.h"
@class BasicHttpBinding_IEmpProfileLayerServiceBinding;
@interface EmpProfileLayerServiceSvc : NSObject {
	
}
+ (BasicHttpBinding_IEmpProfileLayerServiceBinding *)BasicHttpBinding_IEmpProfileLayerServiceBinding;
@end
@class BasicHttpBinding_IEmpProfileLayerServiceBindingResponse;
@class BasicHttpBinding_IEmpProfileLayerServiceBindingOperation;
@protocol BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate <NSObject>
- (void) operation:(BasicHttpBinding_IEmpProfileLayerServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)response;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding : NSObject <BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate> {
	NSURL *address;
	NSTimeInterval defaultTimeout;
	NSMutableArray *cookies;
	BOOL logXMLInOut;
	BOOL synchronousOperationComplete;
	NSString *authUsername;
	NSString *authPassword;
}
@property (copy) NSURL *address;
@property (assign) BOOL logXMLInOut;
@property (assign) NSTimeInterval defaultTimeout;
@property (nonatomic, retain) NSMutableArray *cookies;
@property (nonatomic, retain) NSString *authUsername;
@property (nonatomic, retain) NSString *authPassword;
- (id)initWithAddress:(NSString *)anAddress;
- (void)sendHTTPCallUsingBody:(NSString *)body soapAction:(NSString *)soapAction forOperation:(BasicHttpBinding_IEmpProfileLayerServiceBindingOperation *)operation;
- (void)addCookie:(NSHTTPCookie *)toAdd;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileQualificationByViewUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileQualificationByView *)aParameters ;
- (void)AddEmpProfileQualificationByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileQualificationByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileQualificationByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileQualificationByView *)aParameters ;
- (void)UpdateEmpProfileQualificationByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileQualificationByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileQualificationByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileQualificationByView *)aParameters ;
- (void)DeleteEmpProfileQualificationByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileQualificationByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileForeignLanguageByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileForeignLanguageById *)aParameters ;
- (void)GetViewEmpProfileForeignLanguageByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileForeignLanguageById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)ApproveEmpProfileForeignLanguageByViewUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileForeignLanguageByView *)aParameters ;
- (void)ApproveEmpProfileForeignLanguageByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileForeignLanguageByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileForeignLanguageByViewUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileForeignLanguageByView *)aParameters ;
- (void)AddEmpProfileForeignLanguageByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileForeignLanguageByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileForeignLanguageByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileForeignLanguageByView *)aParameters ;
- (void)UpdateEmpProfileForeignLanguageByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileForeignLanguageByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileForeignLanguageByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileForeignLanguageByView *)aParameters ;
- (void)DeleteEmpProfileForeignLanguageByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileForeignLanguageByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileComputingSkillByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileComputingSkillById *)aParameters ;
- (void)GetViewEmpProfileComputingSkillByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileComputingSkillById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)ApproveEmpProfileComputingSkillUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileComputingSkill *)aParameters ;
- (void)ApproveEmpProfileComputingSkillAsyncUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileComputingSkill *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileComputingSkillUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileComputingSkill *)aParameters ;
- (void)AddEmpProfileComputingSkillAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileComputingSkill *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileComputingSkillUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileComputingSkill *)aParameters ;
- (void)UpdateEmpProfileComputingSkillAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileComputingSkill *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileComputingSkillUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileComputingSkill *)aParameters ;
- (void)DeleteEmpProfileComputingSkillAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileComputingSkill *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileSkillByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileSkillById *)aParameters ;
- (void)GetViewEmpProfileSkillByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileSkillById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileSkillByViewUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileSkillByView *)aParameters ;
- (void)AddEmpProfileSkillByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileSkillByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileSkillByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileSkillByView *)aParameters ;
- (void)UpdateEmpProfileSkillByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileSkillByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileSkillByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileSkillByView *)aParameters ;
- (void)DeleteEmpProfileSkillByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileSkillByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)ApproveEmpProfileSkillByViewUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileSkillByView *)aParameters ;
- (void)ApproveEmpProfileSkillByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileSkillByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileDegreeByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileDegreeById *)aParameters ;
- (void)GetViewEmpProfileDegreeByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileDegreeById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)ApproveEmpProfileDegreeByViewUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileDegreeByView *)aParameters ;
- (void)ApproveEmpProfileDegreeByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileDegreeByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileDegreeByViewUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileDegreeByView *)aParameters ;
- (void)AddEmpProfileDegreeByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileDegreeByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileDegreeByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileDegreeByView *)aParameters ;
- (void)UpdateEmpProfileDegreeByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileDegreeByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileDegreeByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileDegreeByView *)aParameters ;
- (void)DeleteEmpProfileDegreeByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileDegreeByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileKnowledgeByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileKnowledgeById *)aParameters ;
- (void)GetViewEmpProfileKnowledgeByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileKnowledgeById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileKnowledgeByViewUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileKnowledgeByView *)aParameters ;
- (void)AddEmpProfileKnowledgeByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileKnowledgeByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)ApproveEmpProfileKnowledgeByViewUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileKnowledgeByView *)aParameters ;
- (void)ApproveEmpProfileKnowledgeByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileKnowledgeByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileKnowledgeByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileKnowledgeByView *)aParameters ;
- (void)UpdateEmpProfileKnowledgeByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileKnowledgeByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileKnowledgeByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileKnowledgeByView *)aParameters ;
- (void)DeleteEmpProfileKnowledgeByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileKnowledgeByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewCBFactorEmployeeMetaDataGroupByEmpIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataGroupByEmpId *)aParameters ;
- (void)GetViewCBFactorEmployeeMetaDataGroupByEmpIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataGroupByEmpId *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewCBFactorEmployeeMetaDataByEmpIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataByEmpId *)aParameters ;
- (void)GetViewCBFactorEmployeeMetaDataByEmpIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataByEmpId *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddCBFactorEmployeeMetaDataByViewUsingParameters:(EmpProfileLayerServiceSvc_AddCBFactorEmployeeMetaDataByView *)aParameters ;
- (void)AddCBFactorEmployeeMetaDataByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddCBFactorEmployeeMetaDataByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateCBFactorEmployeeMetaDataByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateCBFactorEmployeeMetaDataByView *)aParameters ;
- (void)UpdateCBFactorEmployeeMetaDataByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateCBFactorEmployeeMetaDataByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteCBFactorEmployeeMetaDataByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteCBFactorEmployeeMetaDataByView *)aParameters ;
- (void)DeleteCBFactorEmployeeMetaDataByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteCBFactorEmployeeMetaDataByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewCBFactorEmployeeMetaDataChangesByEmpIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataChangesByEmpId *)aParameters ;
- (void)GetViewCBFactorEmployeeMetaDataChangesByEmpIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataChangesByEmpId *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileMaternityRegimeByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileMaternityRegimeById *)aParameters ;
- (void)GetViewEmpProfileMaternityRegimeByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileMaternityRegimeById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileMaternityRegimeUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileMaternityRegime *)aParameters ;
- (void)AddEmpProfileMaternityRegimeAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileMaternityRegime *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileMaternityRegimeUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileMaternityRegime *)aParameters ;
- (void)UpdateEmpProfileMaternityRegimeAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileMaternityRegime *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileMaternityRegimeUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileMaternityRegime *)aParameters ;
- (void)DeleteEmpProfileMaternityRegimeAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileMaternityRegime *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetCVPersonalByEmployeeIdUsingParameters:(EmpProfileLayerServiceSvc_GetCVPersonalByEmployeeId *)aParameters ;
- (void)GetCVPersonalByEmployeeIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetCVPersonalByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)LoadAllProfileSubAndLayerToTreeUsingParameters:(EmpProfileLayerServiceSvc_LoadAllProfileSubAndLayerToTree *)aParameters ;
- (void)LoadAllProfileSubAndLayerToTreeAsyncUsingParameters:(EmpProfileLayerServiceSvc_LoadAllProfileSubAndLayerToTree *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetTableEmpGroupMappingByEmployeeIdUsingParameters:(EmpProfileLayerServiceSvc_GetTableEmpGroupMappingByEmployeeId *)aParameters ;
- (void)GetTableEmpGroupMappingByEmployeeIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetTableEmpGroupMappingByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpGroupMappingByEmployeeIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpGroupMappingByEmployeeId *)aParameters ;
- (void)GetViewEmpGroupMappingByEmployeeIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpGroupMappingByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetAllEmployeeGroupAndSubGroupUsingParameters:(EmpProfileLayerServiceSvc_GetAllEmployeeGroupAndSubGroup *)aParameters ;
- (void)GetAllEmployeeGroupAndSubGroupAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetAllEmployeeGroupAndSubGroup *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmployeeToEmployeeGroupAndSubGroupUsingParameters:(EmpProfileLayerServiceSvc_AddEmployeeToEmployeeGroupAndSubGroup *)aParameters ;
- (void)AddEmployeeToEmployeeGroupAndSubGroupAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmployeeToEmployeeGroupAndSubGroup *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetAllEmployeeMappingByEmployeeGroupTypeUsingParameters:(EmpProfileLayerServiceSvc_GetAllEmployeeMappingByEmployeeGroupType *)aParameters ;
- (void)GetAllEmployeeMappingByEmployeeGroupTypeAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetAllEmployeeMappingByEmployeeGroupType *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetListPayScaleUsingParameters:(EmpProfileLayerServiceSvc_GetListPayScale *)aParameters ;
- (void)GetListPayScaleAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetListPayScale *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetListPayLevelByPayScaleUsingParameters:(EmpProfileLayerServiceSvc_GetListPayLevelByPayScale *)aParameters ;
- (void)GetListPayLevelByPayScaleAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetListPayLevelByPayScale *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetListPayGradeByPayLevelUsingParameters:(EmpProfileLayerServiceSvc_GetListPayGradeByPayLevel *)aParameters ;
- (void)GetListPayGradeByPayLevelAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetListPayGradeByPayLevel *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetCoefficientUsingParameters:(EmpProfileLayerServiceSvc_GetCoefficient *)aParameters ;
- (void)GetCoefficientAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetCoefficient *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetListSalaryUsingParameters:(EmpProfileLayerServiceSvc_GetListSalary *)aParameters ;
- (void)GetListSalaryAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetListSalary *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetListAllowanceUsingParameters:(EmpProfileLayerServiceSvc_GetListAllowance *)aParameters ;
- (void)GetListAllowanceAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetListAllowance *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetListInsuranceUsingParameters:(EmpProfileLayerServiceSvc_GetListInsurance *)aParameters ;
- (void)GetListInsuranceAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetListInsurance *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileAllowanceByViewUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileAllowanceByView *)aParameters ;
- (void)GetViewEmpProfileAllowanceByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileAllowanceByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileProfileAllowanceByViewUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileProfileAllowanceByView *)aParameters ;
- (void)AddEmpProfileProfileAllowanceByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileProfileAllowanceByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileAllowanceByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileAllowanceByView *)aParameters ;
- (void)UpdateEmpProfileAllowanceByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileAllowanceByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileAllowanceByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileAllowanceByView *)aParameters ;
- (void)DeleteEmpProfileAllowanceByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileAllowanceByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileBaseSalaryByViewUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileBaseSalaryByView *)aParameters ;
- (void)GetViewEmpProfileBaseSalaryByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileBaseSalaryByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileProfileBaseSalaryByViewUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileProfileBaseSalaryByView *)aParameters ;
- (void)AddEmpProfileProfileBaseSalaryByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileProfileBaseSalaryByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileBaseSalaryByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileBaseSalaryByView *)aParameters ;
- (void)UpdateEmpProfileBaseSalaryByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileBaseSalaryByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileBaseSalaryByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileBaseSalaryByView *)aParameters ;
- (void)DeleteEmpProfileBaseSalaryByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileBaseSalaryByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetAllTotalIncomeByEmployeeIdUsingParameters:(EmpProfileLayerServiceSvc_GetAllTotalIncomeByEmployeeId *)aParameters ;
- (void)GetAllTotalIncomeByEmployeeIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetAllTotalIncomeByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)HasPermissionOnEmpProfileLayerUsingParameters:(EmpProfileLayerServiceSvc_HasPermissionOnEmpProfileLayer *)aParameters ;
- (void)HasPermissionOnEmpProfileLayerAsyncUsingParameters:(EmpProfileLayerServiceSvc_HasPermissionOnEmpProfileLayer *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetPermissionsOnEmpProfileLayerUsingParameters:(EmpProfileLayerServiceSvc_GetPermissionsOnEmpProfileLayer *)aParameters ;
- (void)GetPermissionsOnEmpProfileLayerAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetPermissionsOnEmpProfileLayer *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetAllViewSysEmpProfileGroupAndLayersUsingParameters:(EmpProfileLayerServiceSvc_GetAllViewSysEmpProfileGroupAndLayers *)aParameters ;
- (void)GetAllViewSysEmpProfileGroupAndLayersAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetAllViewSysEmpProfileGroupAndLayers *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewSysEmpProfileGroupAndLayersByUrlTypeUsingParameters:(EmpProfileLayerServiceSvc_GetViewSysEmpProfileGroupAndLayersByUrlType *)aParameters ;
- (void)GetViewSysEmpProfileGroupAndLayersByUrlTypeAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewSysEmpProfileGroupAndLayersByUrlType *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpBasicProfileByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpBasicProfileById *)aParameters ;
- (void)GetViewEmpBasicProfileByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpBasicProfileById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpBasicProfileByViewUsingParameters:(EmpProfileLayerServiceSvc_AddEmpBasicProfileByView *)aParameters ;
- (void)AddEmpBasicProfileByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpBasicProfileByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpBasicProfileByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpBasicProfileByView *)aParameters ;
- (void)UpdateEmpBasicProfileByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpBasicProfileByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpBasicProfileByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpBasicProfileByView *)aParameters ;
- (void)DeleteEmpBasicProfileByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpBasicProfileByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfilePersonalityByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfilePersonalityById *)aParameters ;
- (void)GetViewEmpProfilePersonalityByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfilePersonalityById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)ApproveEmpProfilePersonalityByViewUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfilePersonalityByView *)aParameters ;
- (void)ApproveEmpProfilePersonalityByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfilePersonalityByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfilePersonalityByViewUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfilePersonalityByView *)aParameters ;
- (void)AddEmpProfilePersonalityByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfilePersonalityByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfilePersonalityByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfilePersonalityByView *)aParameters ;
- (void)UpdateEmpProfilePersonalityByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfilePersonalityByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfilePersonalityByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfilePersonalityByView *)aParameters ;
- (void)DeleteEmpProfilePersonalityByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfilePersonalityByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileHobbyByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileHobbyById *)aParameters ;
- (void)GetViewEmpProfileHobbyByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileHobbyById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileHobbyByViewUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileHobbyByView *)aParameters ;
- (void)AddEmpProfileHobbyByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileHobbyByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileHobbyByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileHobbyByView *)aParameters ;
- (void)UpdateEmpProfileHobbyByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileHobbyByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileHobbyByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileHobbyByView *)aParameters ;
- (void)DeleteEmpProfileHobbyByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileHobbyByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpSocialInsuranceSalaryByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpSocialInsuranceSalaryById *)aParameters ;
- (void)GetViewEmpSocialInsuranceSalaryByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpSocialInsuranceSalaryById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpSocialInsuranceSalaryByViewUsingParameters:(EmpProfileLayerServiceSvc_AddEmpSocialInsuranceSalaryByView *)aParameters ;
- (void)AddEmpSocialInsuranceSalaryByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpSocialInsuranceSalaryByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpSocialInsuranceSalaryByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpSocialInsuranceSalaryByView *)aParameters ;
- (void)UpdateEmpSocialInsuranceSalaryByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpSocialInsuranceSalaryByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpSocialInsuranceSalaryByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpSocialInsuranceSalaryByView *)aParameters ;
- (void)DeleteEmpSocialInsuranceSalaryByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpSocialInsuranceSalaryByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileBiographyByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileBiographyById *)aParameters ;
- (void)GetViewEmpProfileBiographyByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileBiographyById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)ApproveEmpProfileBiographyUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileBiography *)aParameters ;
- (void)ApproveEmpProfileBiographyAsyncUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileBiography *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileBiographyUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileBiography *)aParameters ;
- (void)AddEmpProfileBiographyAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileBiography *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileBiographyUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileBiography *)aParameters ;
- (void)UpdateEmpProfileBiographyAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileBiography *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileBiographyUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileBiography *)aParameters ;
- (void)DeleteEmpProfileBiographyAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileBiography *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpFamilyRelationshipByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpFamilyRelationshipById *)aParameters ;
- (void)GetViewEmpFamilyRelationshipByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpFamilyRelationshipById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileFamilyRelationshipByViewUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileFamilyRelationshipByView *)aParameters ;
- (void)AddEmpProfileFamilyRelationshipByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileFamilyRelationshipByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileFamilyRelationshipByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileFamilyRelationshipByView *)aParameters ;
- (void)UpdateEmpProfileFamilyRelationshipByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileFamilyRelationshipByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)ApproveEmpProfileFamilyRelationshipByViewUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileFamilyRelationshipByView *)aParameters ;
- (void)ApproveEmpProfileFamilyRelationshipByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileFamilyRelationshipByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileFamilyRelationshipByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileFamilyRelationshipByView *)aParameters ;
- (void)DeleteEmpProfileFamilyRelationshipByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileFamilyRelationshipByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileContactsByEmployeeIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileContactsByEmployeeId *)aParameters ;
- (void)GetViewEmpProfileContactsByEmployeeIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileContactsByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)ApproveEmpProfileContactByViewUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileContactByView *)aParameters ;
- (void)ApproveEmpProfileContactByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileContactByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileContactByViewUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileContactByView *)aParameters ;
- (void)AddEmpProfileContactByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileContactByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileContactByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileContactByView *)aParameters ;
- (void)UpdateEmpProfileContactByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileContactByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileContactByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileContactByView *)aParameters ;
- (void)DeleteEmpProfileContactByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileContactByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileHealthyUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileHealthy *)aParameters ;
- (void)AddEmpProfileHealthyAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileHealthy *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileHealthyUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileHealthy *)aParameters ;
- (void)UpdateEmpProfileHealthyAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileHealthy *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileHealthyUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileHealthy *)aParameters ;
- (void)DeleteEmpProfileHealthyAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileHealthy *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileParticipationByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileParticipationById *)aParameters ;
- (void)GetViewEmpProfileParticipationByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileParticipationById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileParticipationUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileParticipation *)aParameters ;
- (void)AddEmpProfileParticipationAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileParticipation *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)ApproveEmpProfileParticipationUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileParticipation *)aParameters ;
- (void)ApproveEmpProfileParticipationAsyncUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileParticipation *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileParticipationUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileParticipation *)aParameters ;
- (void)UpdateEmpProfileParticipationAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileParticipation *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileParticipationUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileParticipation *)aParameters ;
- (void)DeleteEmpProfileParticipationAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileParticipation *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetActiveEmpProfileJobPositionByEmpIdUsingParameters:(EmpProfileLayerServiceSvc_GetActiveEmpProfileJobPositionByEmpId *)aParameters ;
- (void)GetActiveEmpProfileJobPositionByEmpIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetActiveEmpProfileJobPositionByEmpId *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileJobPositionsUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileJobPositions *)aParameters ;
- (void)GetViewEmpProfileJobPositionsAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileJobPositions *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileJobPositionByViewUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileJobPositionByView *)aParameters ;
- (void)AddEmpProfileJobPositionByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileJobPositionByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileJobPositionByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositionByView *)aParameters ;
- (void)UpdateEmpProfileJobPositionByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositionByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileJobPositionByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileJobPositionByView *)aParameters ;
- (void)DeleteEmpProfileJobPositionByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileJobPositionByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileJobPositionsUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositions *)aParameters ;
- (void)UpdateEmpProfileJobPositionsAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositions *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileProcessOfWorkByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileProcessOfWorkById *)aParameters ;
- (void)GetViewEmpProfileProcessOfWorkByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileProcessOfWorkById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileProcessOfWorkUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileProcessOfWork *)aParameters ;
- (void)AddEmpProfileProcessOfWorkAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileProcessOfWork *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileProcessOfWorkUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileProcessOfWork *)aParameters ;
- (void)UpdateEmpProfileProcessOfWorkAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileProcessOfWork *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileProcessOfWorkUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileProcessOfWork *)aParameters ;
- (void)DeleteEmpProfileProcessOfWorkAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileProcessOfWork *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileContractByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileContractById *)aParameters ;
- (void)GetViewEmpProfileContractByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileContractById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpContractByViewUsingParameters:(EmpProfileLayerServiceSvc_AddEmpContractByView *)aParameters ;
- (void)AddEmpContractByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpContractByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpContractByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpContractByView *)aParameters ;
- (void)UpdateEmpContractByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpContractByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpContractByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpContractByView *)aParameters ;
- (void)DeleteEmpContractByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpContractByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewJobPositionDescriptionByEmployeeIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewJobPositionDescriptionByEmployeeId *)aParameters ;
- (void)GetViewJobPositionDescriptionByEmployeeIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewJobPositionDescriptionByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewJobPositionConcurrentByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewJobPositionConcurrentById *)aParameters ;
- (void)GetViewJobPositionConcurrentByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewJobPositionConcurrentById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileJobPositionConcurrentByViewUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileJobPositionConcurrentByView *)aParameters ;
- (void)AddEmpProfileJobPositionConcurrentByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileJobPositionConcurrentByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileJobPositionConcurrentByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositionConcurrentByView *)aParameters ;
- (void)UpdateEmpProfileJobPositionConcurrentByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositionConcurrentByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileJobPositionConcurrentByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileJobPositionConcurrentByView *)aParameters ;
- (void)DeleteEmpProfileJobPositionConcurrentByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileJobPositionConcurrentByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewRequestEquipmentMasterByEmployeeIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewRequestEquipmentMasterByEmployeeId *)aParameters ;
- (void)GetViewRequestEquipmentMasterByEmployeeIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewRequestEquipmentMasterByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewRequestEquipmentDetailUsingParameters:(EmpProfileLayerServiceSvc_GetViewRequestEquipmentDetail *)aParameters ;
- (void)GetViewRequestEquipmentDetailAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewRequestEquipmentDetail *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewRequestEquipmentDetailByRequestIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewRequestEquipmentDetailByRequestId *)aParameters ;
- (void)GetViewRequestEquipmentDetailByRequestIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewRequestEquipmentDetailByRequestId *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileEquipmentByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileEquipmentById *)aParameters ;
- (void)GetViewEmpProfileEquipmentByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileEquipmentById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileEquipmentByViewUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileEquipmentByView *)aParameters ;
- (void)AddEmpProfileEquipmentByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileEquipmentByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileEquipmentByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileEquipmentByView *)aParameters ;
- (void)UpdateEmpProfileEquipmentByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileEquipmentByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileEquipmentByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileEquipmentByView *)aParameters ;
- (void)DeleteEmpProfileEquipmentByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileEquipmentByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileAssetsByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileAssetsById *)aParameters ;
- (void)GetViewEmpProfileAssetsByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileAssetsById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileAssetsByViewUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileAssetsByView *)aParameters ;
- (void)AddEmpProfileAssetsByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileAssetsByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileAssetsByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileAssetsByView *)aParameters ;
- (void)UpdateEmpProfileAssetsByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileAssetsByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileAssetsByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileAssetsByView *)aParameters ;
- (void)DeleteEmpProfileAssetsByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileAssetsByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileWageTypeByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileWageTypeById *)aParameters ;
- (void)GetViewEmpProfileWageTypeByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileWageTypeById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileWageTypeByViewUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileWageTypeByView *)aParameters ;
- (void)AddEmpProfileWageTypeByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileWageTypeByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileWageTypeByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileWageTypeByView *)aParameters ;
- (void)UpdateEmpProfileWageTypeByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileWageTypeByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileWageTypeByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileWageTypeByView *)aParameters ;
- (void)DeleteEmpProfileWageTypeByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileWageTypeByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileWorkingFormByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingFormById *)aParameters ;
- (void)GetViewEmpProfileWorkingFormByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingFormById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileWorkingFormByViewUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileWorkingFormByView *)aParameters ;
- (void)AddEmpProfileWorkingFormByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileWorkingFormByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileWorkingFormByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileWorkingFormByView *)aParameters ;
- (void)UpdateEmpProfileWorkingFormByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileWorkingFormByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileWorkingFormByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileWorkingFormByView *)aParameters ;
- (void)DeleteEmpProfileWorkingFormByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileWorkingFormByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileWorkingExperienceByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingExperienceById *)aParameters ;
- (void)GetViewEmpProfileWorkingExperienceByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingExperienceById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)ApproveEmpProfileWorkingExperienceByViewUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileWorkingExperienceByView *)aParameters ;
- (void)ApproveEmpProfileWorkingExperienceByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileWorkingExperienceByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileWorkingExperienceByViewUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileWorkingExperienceByView *)aParameters ;
- (void)AddEmpProfileWorkingExperienceByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileWorkingExperienceByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileWorkingExperienceByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileWorkingExperienceByView *)aParameters ;
- (void)UpdateEmpProfileWorkingExperienceByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileWorkingExperienceByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileWorkingExperienceByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileWorkingExperienceByView *)aParameters ;
- (void)DeleteEmpProfileWorkingExperienceByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileWorkingExperienceByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileExperienceByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileExperienceById *)aParameters ;
- (void)GetViewEmpProfileExperienceByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileExperienceById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)ApproveEmpProfileExperienceUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileExperience *)aParameters ;
- (void)ApproveEmpProfileExperienceAsyncUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileExperience *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileExperienceUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileExperience *)aParameters ;
- (void)AddEmpProfileExperienceAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileExperience *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileExperienceUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileExperience *)aParameters ;
- (void)UpdateEmpProfileExperienceAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileExperience *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileExperienceUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileExperience *)aParameters ;
- (void)DeleteEmpProfileExperienceAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileExperience *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileLeaveRegimeUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileLeaveRegime *)aParameters ;
- (void)GetViewEmpProfileLeaveRegimeAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileLeaveRegime *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileLeaveRegimeUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileLeaveRegime *)aParameters ;
- (void)AddEmpProfileLeaveRegimeAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileLeaveRegime *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileLeaveRegimeUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileLeaveRegime *)aParameters ;
- (void)UpdateEmpProfileLeaveRegimeAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileLeaveRegime *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileLeaveRegimeUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileLeaveRegime *)aParameters ;
- (void)DeleteEmpProfileLeaveRegimeAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileLeaveRegime *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileCommentByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileCommentById *)aParameters ;
- (void)GetViewEmpProfileCommentByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileCommentById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)ApproveEmpProfileCommentUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileComment *)aParameters ;
- (void)ApproveEmpProfileCommentAsyncUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileComment *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileCommentUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileComment *)aParameters ;
- (void)AddEmpProfileCommentAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileComment *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileCommentUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileComment *)aParameters ;
- (void)UpdateEmpProfileCommentAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileComment *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileCommentUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileComment *)aParameters ;
- (void)DeleteEmpProfileCommentAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileComment *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileTrainingByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileTrainingById *)aParameters ;
- (void)GetViewEmpProfileTrainingByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileTrainingById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileTrainingUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileTraining *)aParameters ;
- (void)AddEmpProfileTrainingAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileTraining *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileTrainingUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileTraining *)aParameters ;
- (void)UpdateEmpProfileTrainingAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileTraining *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileTrainingUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileTraining *)aParameters ;
- (void)DeleteEmpProfileTrainingAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileTraining *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)ApproveEmpProfileTrainingUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileTraining *)aParameters ;
- (void)ApproveEmpProfileTrainingAsyncUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileTraining *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileDisciplineByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileDisciplineById *)aParameters ;
- (void)GetViewEmpProfileDisciplineByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileDisciplineById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileDisciplineByViewUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileDisciplineByView *)aParameters ;
- (void)AddEmpProfileDisciplineByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileDisciplineByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileDisciplineByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileDisciplineByView *)aParameters ;
- (void)UpdateEmpProfileDisciplineByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileDisciplineByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileDisciplineByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileDisciplineByView *)aParameters ;
- (void)DeleteEmpProfileDisciplineByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileDisciplineByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpPerformanceAppraisalByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpPerformanceAppraisalById *)aParameters ;
- (void)GetViewEmpPerformanceAppraisalByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpPerformanceAppraisalById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpPerformanceAppraisalByViewUsingParameters:(EmpProfileLayerServiceSvc_AddEmpPerformanceAppraisalByView *)aParameters ;
- (void)AddEmpPerformanceAppraisalByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpPerformanceAppraisalByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)ApproveEmpPerformanceAppraisalByViewUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpPerformanceAppraisalByView *)aParameters ;
- (void)ApproveEmpPerformanceAppraisalByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpPerformanceAppraisalByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpPerformanceAppraisalByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpPerformanceAppraisalByView *)aParameters ;
- (void)UpdateEmpPerformanceAppraisalByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpPerformanceAppraisalByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpPerformanceAppraisalByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpPerformanceAppraisalByView *)aParameters ;
- (void)DeleteEmpPerformanceAppraisalByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpPerformanceAppraisalByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileSocialInsuranceByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileSocialInsuranceById *)aParameters ;
- (void)GetViewEmpProfileSocialInsuranceByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileSocialInsuranceById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileSocialInsuranceUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileSocialInsurance *)aParameters ;
- (void)AddEmpProfileSocialInsuranceAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileSocialInsurance *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileSocialInsuranceUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileSocialInsurance *)aParameters ;
- (void)UpdateEmpProfileSocialInsuranceAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileSocialInsurance *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileSocialInsuranceUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileSocialInsurance *)aParameters ;
- (void)DeleteEmpProfileSocialInsuranceAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileSocialInsurance *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileHealthInsuranceByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileHealthInsuranceById *)aParameters ;
- (void)GetViewEmpProfileHealthInsuranceByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileHealthInsuranceById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileHealthInsuranceByViewUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileHealthInsuranceByView *)aParameters ;
- (void)AddEmpProfileHealthInsuranceByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileHealthInsuranceByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileHealthInsuranceByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileHealthInsuranceByView *)aParameters ;
- (void)UpdateEmpProfileHealthInsuranceByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileHealthInsuranceByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileHealthInsuranceByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileHealthInsuranceByView *)aParameters ;
- (void)DeleteEmpProfileHealthInsuranceByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileHealthInsuranceByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)AddEmpProfileOtherBenefitByViewUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileOtherBenefitByView *)aParameters ;
- (void)AddEmpProfileOtherBenefitByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_AddEmpProfileOtherBenefitByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)UpdateEmpProfileOtherBenefitByViewUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileOtherBenefitByView *)aParameters ;
- (void)UpdateEmpProfileOtherBenefitByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileOtherBenefitByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)DeleteEmpProfileOtherBenefitByViewUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileOtherBenefitByView *)aParameters ;
- (void)DeleteEmpProfileOtherBenefitByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileOtherBenefitByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)GetViewEmpProfileQualificationByIdUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileQualificationById *)aParameters ;
- (void)GetViewEmpProfileQualificationByIdAsyncUsingParameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileQualificationById *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)ApproveEmpProfileQualificationByViewUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileQualificationByView *)aParameters ;
- (void)ApproveEmpProfileQualificationByViewAsyncUsingParameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileQualificationByView *)aParameters  delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)responseDelegate;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBindingOperation : NSOperation {
	BasicHttpBinding_IEmpProfileLayerServiceBinding *binding;
	BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *response;
	id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate> delegate;
	NSMutableData *responseData;
	NSURLConnection *urlConnection;
}
@property (retain) BasicHttpBinding_IEmpProfileLayerServiceBinding *binding;
@property (readonly) BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *response;
@property (nonatomic, assign) id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate> delegate;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, retain) NSURLConnection *urlConnection;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileQualificationByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileQualificationByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileQualificationByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileQualificationByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileQualificationByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileQualificationByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileQualificationByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileQualificationByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileQualificationByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileQualificationByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileQualificationByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileQualificationByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileForeignLanguageById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileForeignLanguageById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileForeignLanguageById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileForeignLanguageById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_ApproveEmpProfileForeignLanguageByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_ApproveEmpProfileForeignLanguageByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_ApproveEmpProfileForeignLanguageByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileForeignLanguageByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileForeignLanguageByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileForeignLanguageByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileForeignLanguageByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileForeignLanguageByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileForeignLanguageByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileForeignLanguageByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileForeignLanguageByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileForeignLanguageByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileForeignLanguageByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileForeignLanguageByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileForeignLanguageByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileForeignLanguageByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileComputingSkillById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileComputingSkillById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileComputingSkillById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileComputingSkillById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_ApproveEmpProfileComputingSkill : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_ApproveEmpProfileComputingSkill * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_ApproveEmpProfileComputingSkill * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileComputingSkill *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileComputingSkill : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileComputingSkill * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileComputingSkill * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileComputingSkill *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileComputingSkill : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileComputingSkill * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileComputingSkill * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileComputingSkill *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileComputingSkill : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileComputingSkill * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileComputingSkill * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileComputingSkill *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileSkillById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileSkillById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileSkillById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileSkillById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileSkillByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileSkillByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileSkillByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileSkillByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileSkillByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileSkillByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileSkillByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileSkillByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileSkillByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileSkillByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileSkillByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileSkillByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_ApproveEmpProfileSkillByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_ApproveEmpProfileSkillByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_ApproveEmpProfileSkillByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileSkillByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileDegreeById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileDegreeById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileDegreeById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileDegreeById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_ApproveEmpProfileDegreeByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_ApproveEmpProfileDegreeByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_ApproveEmpProfileDegreeByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileDegreeByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileDegreeByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileDegreeByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileDegreeByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileDegreeByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileDegreeByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileDegreeByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileDegreeByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileDegreeByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileDegreeByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileDegreeByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileDegreeByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileDegreeByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileKnowledgeById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileKnowledgeById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileKnowledgeById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileKnowledgeById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileKnowledgeByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileKnowledgeByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileKnowledgeByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileKnowledgeByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_ApproveEmpProfileKnowledgeByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_ApproveEmpProfileKnowledgeByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_ApproveEmpProfileKnowledgeByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileKnowledgeByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileKnowledgeByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileKnowledgeByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileKnowledgeByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileKnowledgeByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileKnowledgeByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileKnowledgeByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileKnowledgeByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileKnowledgeByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewCBFactorEmployeeMetaDataGroupByEmpId : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataGroupByEmpId * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataGroupByEmpId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataGroupByEmpId *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewCBFactorEmployeeMetaDataByEmpId : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataByEmpId * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataByEmpId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataByEmpId *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddCBFactorEmployeeMetaDataByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddCBFactorEmployeeMetaDataByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddCBFactorEmployeeMetaDataByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddCBFactorEmployeeMetaDataByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateCBFactorEmployeeMetaDataByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateCBFactorEmployeeMetaDataByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateCBFactorEmployeeMetaDataByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateCBFactorEmployeeMetaDataByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteCBFactorEmployeeMetaDataByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteCBFactorEmployeeMetaDataByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteCBFactorEmployeeMetaDataByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteCBFactorEmployeeMetaDataByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewCBFactorEmployeeMetaDataChangesByEmpId : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataChangesByEmpId * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataChangesByEmpId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewCBFactorEmployeeMetaDataChangesByEmpId *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileMaternityRegimeById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileMaternityRegimeById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileMaternityRegimeById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileMaternityRegimeById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileMaternityRegime : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileMaternityRegime * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileMaternityRegime * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileMaternityRegime *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileMaternityRegime : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileMaternityRegime * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileMaternityRegime * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileMaternityRegime *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileMaternityRegime : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileMaternityRegime * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileMaternityRegime * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileMaternityRegime *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetCVPersonalByEmployeeId : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetCVPersonalByEmployeeId * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetCVPersonalByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetCVPersonalByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_LoadAllProfileSubAndLayerToTree : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_LoadAllProfileSubAndLayerToTree * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_LoadAllProfileSubAndLayerToTree * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_LoadAllProfileSubAndLayerToTree *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetTableEmpGroupMappingByEmployeeId : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetTableEmpGroupMappingByEmployeeId * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetTableEmpGroupMappingByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetTableEmpGroupMappingByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpGroupMappingByEmployeeId : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpGroupMappingByEmployeeId * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpGroupMappingByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpGroupMappingByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetAllEmployeeGroupAndSubGroup : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetAllEmployeeGroupAndSubGroup * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetAllEmployeeGroupAndSubGroup * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetAllEmployeeGroupAndSubGroup *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmployeeToEmployeeGroupAndSubGroup : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmployeeToEmployeeGroupAndSubGroup * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmployeeToEmployeeGroupAndSubGroup * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmployeeToEmployeeGroupAndSubGroup *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetAllEmployeeMappingByEmployeeGroupType : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetAllEmployeeMappingByEmployeeGroupType * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetAllEmployeeMappingByEmployeeGroupType * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetAllEmployeeMappingByEmployeeGroupType *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetListPayScale : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetListPayScale * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetListPayScale * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetListPayScale *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetListPayLevelByPayScale : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetListPayLevelByPayScale * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetListPayLevelByPayScale * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetListPayLevelByPayScale *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetListPayGradeByPayLevel : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetListPayGradeByPayLevel * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetListPayGradeByPayLevel * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetListPayGradeByPayLevel *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetCoefficient : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetCoefficient * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetCoefficient * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetCoefficient *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetListSalary : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetListSalary * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetListSalary * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetListSalary *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetListAllowance : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetListAllowance * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetListAllowance * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetListAllowance *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetListInsurance : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetListInsurance * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetListInsurance * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetListInsurance *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileAllowanceByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileAllowanceByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileAllowanceByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileAllowanceByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileProfileAllowanceByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileProfileAllowanceByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileProfileAllowanceByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileProfileAllowanceByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileAllowanceByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileAllowanceByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileAllowanceByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileAllowanceByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileAllowanceByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileAllowanceByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileAllowanceByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileAllowanceByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileBaseSalaryByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileBaseSalaryByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileBaseSalaryByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileBaseSalaryByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileProfileBaseSalaryByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileProfileBaseSalaryByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileProfileBaseSalaryByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileProfileBaseSalaryByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileBaseSalaryByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileBaseSalaryByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileBaseSalaryByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileBaseSalaryByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileBaseSalaryByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileBaseSalaryByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileBaseSalaryByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileBaseSalaryByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetAllTotalIncomeByEmployeeId : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetAllTotalIncomeByEmployeeId * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetAllTotalIncomeByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetAllTotalIncomeByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_HasPermissionOnEmpProfileLayer : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_HasPermissionOnEmpProfileLayer * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_HasPermissionOnEmpProfileLayer * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_HasPermissionOnEmpProfileLayer *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetPermissionsOnEmpProfileLayer : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetPermissionsOnEmpProfileLayer * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetPermissionsOnEmpProfileLayer * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetPermissionsOnEmpProfileLayer *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetAllViewSysEmpProfileGroupAndLayers : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetAllViewSysEmpProfileGroupAndLayers * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetAllViewSysEmpProfileGroupAndLayers * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetAllViewSysEmpProfileGroupAndLayers *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewSysEmpProfileGroupAndLayersByUrlType : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewSysEmpProfileGroupAndLayersByUrlType * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewSysEmpProfileGroupAndLayersByUrlType * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewSysEmpProfileGroupAndLayersByUrlType *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpBasicProfileById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpBasicProfileById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpBasicProfileById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpBasicProfileById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpBasicProfileByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpBasicProfileByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpBasicProfileByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpBasicProfileByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpBasicProfileByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpBasicProfileByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpBasicProfileByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpBasicProfileByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpBasicProfileByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpBasicProfileByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpBasicProfileByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpBasicProfileByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfilePersonalityById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfilePersonalityById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfilePersonalityById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfilePersonalityById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_ApproveEmpProfilePersonalityByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_ApproveEmpProfilePersonalityByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_ApproveEmpProfilePersonalityByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_ApproveEmpProfilePersonalityByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfilePersonalityByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfilePersonalityByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfilePersonalityByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfilePersonalityByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfilePersonalityByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfilePersonalityByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfilePersonalityByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfilePersonalityByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfilePersonalityByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfilePersonalityByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfilePersonalityByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfilePersonalityByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileHobbyById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileHobbyById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileHobbyById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileHobbyById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileHobbyByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileHobbyByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileHobbyByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileHobbyByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileHobbyByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileHobbyByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileHobbyByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileHobbyByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileHobbyByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileHobbyByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileHobbyByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileHobbyByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpSocialInsuranceSalaryById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpSocialInsuranceSalaryById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpSocialInsuranceSalaryById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpSocialInsuranceSalaryById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpSocialInsuranceSalaryByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpSocialInsuranceSalaryByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpSocialInsuranceSalaryByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpSocialInsuranceSalaryByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpSocialInsuranceSalaryByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpSocialInsuranceSalaryByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpSocialInsuranceSalaryByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpSocialInsuranceSalaryByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpSocialInsuranceSalaryByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpSocialInsuranceSalaryByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpSocialInsuranceSalaryByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpSocialInsuranceSalaryByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileBiographyById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileBiographyById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileBiographyById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileBiographyById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_ApproveEmpProfileBiography : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_ApproveEmpProfileBiography * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_ApproveEmpProfileBiography * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileBiography *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileBiography : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileBiography * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileBiography * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileBiography *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileBiography : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileBiography * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileBiography * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileBiography *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileBiography : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileBiography * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileBiography * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileBiography *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpFamilyRelationshipById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpFamilyRelationshipById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpFamilyRelationshipById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpFamilyRelationshipById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileFamilyRelationshipByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileFamilyRelationshipByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileFamilyRelationshipByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileFamilyRelationshipByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileFamilyRelationshipByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileFamilyRelationshipByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileFamilyRelationshipByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileFamilyRelationshipByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_ApproveEmpProfileFamilyRelationshipByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_ApproveEmpProfileFamilyRelationshipByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_ApproveEmpProfileFamilyRelationshipByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileFamilyRelationshipByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileFamilyRelationshipByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileFamilyRelationshipByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileFamilyRelationshipByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileFamilyRelationshipByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileContactsByEmployeeId : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileContactsByEmployeeId * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileContactsByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileContactsByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_ApproveEmpProfileContactByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_ApproveEmpProfileContactByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_ApproveEmpProfileContactByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileContactByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileContactByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileContactByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileContactByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileContactByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileContactByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileContactByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileContactByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileContactByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileContactByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileContactByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileContactByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileContactByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileHealthy : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileHealthy * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileHealthy * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileHealthy *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileHealthy : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileHealthy * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileHealthy * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileHealthy *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileHealthy : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileHealthy * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileHealthy * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileHealthy *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileParticipationById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileParticipationById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileParticipationById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileParticipationById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileParticipation : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileParticipation * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileParticipation * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileParticipation *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_ApproveEmpProfileParticipation : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_ApproveEmpProfileParticipation * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_ApproveEmpProfileParticipation * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileParticipation *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileParticipation : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileParticipation * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileParticipation * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileParticipation *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileParticipation : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileParticipation * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileParticipation * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileParticipation *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetActiveEmpProfileJobPositionByEmpId : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetActiveEmpProfileJobPositionByEmpId * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetActiveEmpProfileJobPositionByEmpId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetActiveEmpProfileJobPositionByEmpId *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileJobPositions : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileJobPositions * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileJobPositions * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileJobPositions *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileJobPositionByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileJobPositionByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileJobPositionByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileJobPositionByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileJobPositionByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositionByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositionByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositionByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileJobPositionByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileJobPositionByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileJobPositionByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileJobPositionByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileJobPositions : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositions * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositions * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositions *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileProcessOfWorkById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileProcessOfWorkById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileProcessOfWorkById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileProcessOfWorkById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileProcessOfWork : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileProcessOfWork * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileProcessOfWork * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileProcessOfWork *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileProcessOfWork : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileProcessOfWork * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileProcessOfWork * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileProcessOfWork *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileProcessOfWork : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileProcessOfWork * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileProcessOfWork * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileProcessOfWork *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileContractById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileContractById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileContractById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileContractById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpContractByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpContractByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpContractByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpContractByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpContractByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpContractByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpContractByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpContractByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpContractByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpContractByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpContractByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpContractByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewJobPositionDescriptionByEmployeeId : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewJobPositionDescriptionByEmployeeId * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewJobPositionDescriptionByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewJobPositionDescriptionByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewJobPositionConcurrentById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewJobPositionConcurrentById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewJobPositionConcurrentById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewJobPositionConcurrentById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileJobPositionConcurrentByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileJobPositionConcurrentByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileJobPositionConcurrentByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileJobPositionConcurrentByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileJobPositionConcurrentByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositionConcurrentByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositionConcurrentByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileJobPositionConcurrentByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileJobPositionConcurrentByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileJobPositionConcurrentByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileJobPositionConcurrentByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileJobPositionConcurrentByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewRequestEquipmentMasterByEmployeeId : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewRequestEquipmentMasterByEmployeeId * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewRequestEquipmentMasterByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewRequestEquipmentMasterByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewRequestEquipmentDetail : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewRequestEquipmentDetail * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewRequestEquipmentDetail * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewRequestEquipmentDetail *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewRequestEquipmentDetailByRequestId : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewRequestEquipmentDetailByRequestId * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewRequestEquipmentDetailByRequestId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewRequestEquipmentDetailByRequestId *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileEquipmentById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileEquipmentById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileEquipmentById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileEquipmentById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileEquipmentByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileEquipmentByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileEquipmentByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileEquipmentByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileEquipmentByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileEquipmentByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileEquipmentByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileEquipmentByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileEquipmentByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileEquipmentByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileEquipmentByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileEquipmentByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileAssetsById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileAssetsById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileAssetsById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileAssetsById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileAssetsByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileAssetsByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileAssetsByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileAssetsByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileAssetsByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileAssetsByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileAssetsByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileAssetsByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileAssetsByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileAssetsByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileAssetsByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileAssetsByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileWageTypeById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileWageTypeById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileWageTypeById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileWageTypeById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileWageTypeByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileWageTypeByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileWageTypeByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileWageTypeByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileWageTypeByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileWageTypeByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileWageTypeByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileWageTypeByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileWageTypeByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileWageTypeByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileWageTypeByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileWageTypeByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileWorkingFormById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingFormById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingFormById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingFormById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileWorkingFormByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileWorkingFormByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileWorkingFormByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileWorkingFormByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileWorkingFormByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileWorkingFormByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileWorkingFormByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileWorkingFormByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileWorkingFormByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileWorkingFormByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileWorkingFormByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileWorkingFormByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileWorkingExperienceById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingExperienceById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingExperienceById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingExperienceById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_ApproveEmpProfileWorkingExperienceByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_ApproveEmpProfileWorkingExperienceByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_ApproveEmpProfileWorkingExperienceByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileWorkingExperienceByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileWorkingExperienceByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileWorkingExperienceByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileWorkingExperienceByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileWorkingExperienceByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileWorkingExperienceByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileWorkingExperienceByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileWorkingExperienceByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileWorkingExperienceByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileWorkingExperienceByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileWorkingExperienceByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileWorkingExperienceByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileWorkingExperienceByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileExperienceById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileExperienceById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileExperienceById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileExperienceById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_ApproveEmpProfileExperience : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_ApproveEmpProfileExperience * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_ApproveEmpProfileExperience * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileExperience *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileExperience : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileExperience * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileExperience * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileExperience *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileExperience : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileExperience * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileExperience * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileExperience *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileExperience : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileExperience * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileExperience * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileExperience *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileLeaveRegime : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileLeaveRegime * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileLeaveRegime * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileLeaveRegime *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileLeaveRegime : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileLeaveRegime * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileLeaveRegime * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileLeaveRegime *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileLeaveRegime : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileLeaveRegime * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileLeaveRegime * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileLeaveRegime *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileLeaveRegime : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileLeaveRegime * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileLeaveRegime * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileLeaveRegime *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileCommentById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileCommentById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileCommentById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileCommentById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_ApproveEmpProfileComment : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_ApproveEmpProfileComment * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_ApproveEmpProfileComment * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileComment *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileComment : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileComment * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileComment * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileComment *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileComment : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileComment * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileComment * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileComment *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileComment : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileComment * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileComment * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileComment *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileTrainingById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileTrainingById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileTrainingById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileTrainingById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileTraining : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileTraining * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileTraining * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileTraining *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileTraining : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileTraining * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileTraining * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileTraining *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileTraining : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileTraining * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileTraining * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileTraining *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_ApproveEmpProfileTraining : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_ApproveEmpProfileTraining * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_ApproveEmpProfileTraining * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileTraining *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileDisciplineById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileDisciplineById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileDisciplineById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileDisciplineById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileDisciplineByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileDisciplineByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileDisciplineByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileDisciplineByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileDisciplineByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileDisciplineByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileDisciplineByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileDisciplineByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileDisciplineByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileDisciplineByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileDisciplineByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileDisciplineByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpPerformanceAppraisalById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpPerformanceAppraisalById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpPerformanceAppraisalById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpPerformanceAppraisalById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpPerformanceAppraisalByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpPerformanceAppraisalByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpPerformanceAppraisalByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpPerformanceAppraisalByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_ApproveEmpPerformanceAppraisalByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_ApproveEmpPerformanceAppraisalByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_ApproveEmpPerformanceAppraisalByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_ApproveEmpPerformanceAppraisalByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpPerformanceAppraisalByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpPerformanceAppraisalByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpPerformanceAppraisalByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpPerformanceAppraisalByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpPerformanceAppraisalByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpPerformanceAppraisalByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpPerformanceAppraisalByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpPerformanceAppraisalByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileSocialInsuranceById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileSocialInsuranceById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileSocialInsuranceById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileSocialInsuranceById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileSocialInsurance : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileSocialInsurance * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileSocialInsurance * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileSocialInsurance *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileSocialInsurance : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileSocialInsurance * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileSocialInsurance * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileSocialInsurance *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileSocialInsurance : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileSocialInsurance * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileSocialInsurance * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileSocialInsurance *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileHealthInsuranceById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileHealthInsuranceById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileHealthInsuranceById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileHealthInsuranceById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileHealthInsuranceByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileHealthInsuranceByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileHealthInsuranceByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileHealthInsuranceByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileHealthInsuranceByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileHealthInsuranceByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileHealthInsuranceByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileHealthInsuranceByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileHealthInsuranceByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileHealthInsuranceByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileHealthInsuranceByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileHealthInsuranceByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_AddEmpProfileOtherBenefitByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_AddEmpProfileOtherBenefitByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_AddEmpProfileOtherBenefitByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_AddEmpProfileOtherBenefitByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_UpdateEmpProfileOtherBenefitByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_UpdateEmpProfileOtherBenefitByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_UpdateEmpProfileOtherBenefitByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_UpdateEmpProfileOtherBenefitByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_DeleteEmpProfileOtherBenefitByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_DeleteEmpProfileOtherBenefitByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_DeleteEmpProfileOtherBenefitByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_DeleteEmpProfileOtherBenefitByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_GetViewEmpProfileQualificationById : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_GetViewEmpProfileQualificationById * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_GetViewEmpProfileQualificationById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_GetViewEmpProfileQualificationById *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_ApproveEmpProfileQualificationByView : BasicHttpBinding_IEmpProfileLayerServiceBindingOperation {
	EmpProfileLayerServiceSvc_ApproveEmpProfileQualificationByView * parameters;
}
@property (retain) EmpProfileLayerServiceSvc_ApproveEmpProfileQualificationByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>)aDelegate
	parameters:(EmpProfileLayerServiceSvc_ApproveEmpProfileQualificationByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBinding_envelope : NSObject {
}
+ (BasicHttpBinding_IEmpProfileLayerServiceBinding_envelope *)sharedInstance;
- (NSString *)serializedFormUsingHeaderElements:(NSDictionary *)headerElements bodyElements:(NSDictionary *)bodyElements;
@end
@interface BasicHttpBinding_IEmpProfileLayerServiceBindingResponse : NSObject {
	NSArray *headers;
	NSArray *bodyParts;
	NSError *error;
}
@property (retain) NSArray *headers;
@property (retain) NSArray *bodyParts;
@property (retain) NSError *error;
@end
