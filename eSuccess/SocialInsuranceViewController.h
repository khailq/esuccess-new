//
//  SocialInsuaranceViewController.h
//  eSuccess
//
//  Created by HPTVIETNAM on 6/21/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"
#import "EmpProfileLayerServiceSvc.h"
@interface SocialInsuranceViewController : BaseDetailTableViewController<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>
{
    IBOutlet UILabel *lblSocialInsNumber;
    IBOutlet UILabel *lblSocialInsDateIssue;
    IBOutlet UILabel *lblSocialInsMonthJoining;
    IBOutlet UILabel *lblOtherDateEffect;
    IBOutlet UILabel *lblOtherDateExpire;
    IBOutlet UILabel *lblLabourBookNo;
    IBOutlet UILabel *lblLabourBookDateIssue;
    IBOutlet UILabel *lblLabourPlace;
}
@property(nonatomic, strong) NSMutableArray *socialInsArr;
@end
