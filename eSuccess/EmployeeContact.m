//
//  EmployeeContact.m
//  eSuccess
//
//  Created by HPTVIETNAM on 8/2/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "EmployeeContact.h"

@implementation EmployeeContact

+(EmployeeContact*)parseEmployeeContactByDict:(NSDictionary *)dict
{
    EmployeeContact *empContact = [[EmployeeContact alloc] init];
    empContact.companyId = (NSNumber*)dict[@"CompanyId"];
    empContact.contactAddress = (NSString *)dict[@"ContactAddress"];
    empContact.contactAddressId = (NSNumber*)dict[@"ContactAddressId"];
    NSString *createdDateString = (NSString*)dict[@"CreatedDate"];

    empContact.createdDate = [EmployeeContact convertDateFromString:createdDateString];
    empContact.email = (NSString *)dict[@"Email"];
    empContact.emailProfileContactId = (NSNumber *)dict[@"EmpProfileContactId"];
    empContact.employeeId = (NSNumber *)dict[@"EmployeeId"];
    empContact.fax = (NSString*)dict[@"Fax"];
    empContact.homephone = (NSString*)dict[@"HomePhone"];
    empContact.isCurrent = (NSNumber*)dict[@"IsCurrent"];
    empContact.isDelete = (NSNumber*)dict[@"IsDelete"];
    empContact.mobilephone = (NSString*)dict[@"MobileNumber"];
    empContact.modifiedDate = [EmployeeContact convertDateFromString:(NSString*)dict[@"ModifiedDate"]];
    empContact.permanentAddress = (NSString*)dict[@"PermanentAddress"];
    empContact.permanentAddressId = (NSNumber*)dict[@"PermanentAddressId"];
    empContact.temporaryAddress = (NSString*)dict[@"TemporaryAddress"];
    empContact.temporaryAddressId = (NSNumber*)dict[@"TemporaryAddressId"];
    
    return empContact;
}

+(NSDate*)convertDateFromString:(NSString *)string
{
    NSError *error;
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:@"\\((\\d)\\)" options:0 error:&error];
    NSTextCheckingResult *match = [regex firstMatchInString:string options:0 range:NSMakeRange(0, [string length])];
    
    NSInteger milliseconds = ([string substringWithRange:[match rangeAtIndex:1]]).intValue;
    return [NSDate dateWithTimeIntervalSince1970:milliseconds];
}
@end
