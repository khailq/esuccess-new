//
//  TimeServicesUtils.m
//  eSuccess
//
//  Created by HPTVIETNAM on 9/23/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

   
#import "TimeServicesUtils.h"
#import "RequestServiceSvc.h"
#import "Constant.h"

@implementation TimeServicesUtils

+(id)instance
{
    static TimeServicesUtils *util = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        util = [TimeServicesUtils new];
    });
    
    return util;
}

-(void) getListOfAbsenceType
{
    BasicHttpBinding_ISystemServiceBinding *binding = [SystemServiceSvc BasicHttpBinding_ISystemServiceBinding];
    SystemServiceSvc_GetSysParamListByKeyCode *request = [SystemServiceSvc_GetSysParamListByKeyCode new];
    request.strKey = TM_DON;
    request.strCode = TM_DON_LEAVE_REQUEST;
    
    BasicHttpBinding_ISystemServiceBindingResponse *response = [binding GetSysParamListByKeyCodeUsingParameters:request];
    if (response.bodyParts.count > 0)
    {
        for(id part in response.bodyParts)
        {
            if ([part isKindOfClass:[SystemServiceSvc_GetSysParamListByKeyCodeResponse class]])
            {
                sys_tns1_ArrayOfTB_ESS_SYSTEM_PARAMETER *array = [part GetSysParamListByKeyCodeResult];
                self.listOfAbsenceTypeResult = array;
                break;
            }
        }
    }
}

-(NSString *)classifyPaymentMethodForAbsenceType:(sys_tns1_TB_ESS_SYSTEM_PARAMETER*) absenceType
{
    if (absenceType.Flex7.intValue == NOT_PAY_LEAVE_REQUEST)
        return @"Nghỉ trừ phép";
    if (absenceType.Flex7.intValue == PAY_LEAVE)
        return @"Nghỉ không trừ lương và ngày phép";
    return @"Nghỉ không lương";
}

-(BOOL) isCompensatoryLeave:(sys_tns1_TB_ESS_SYSTEM_PARAMETER*)absenceType
{
    return absenceType.Flex11.boolValue;
}

-(void) getLeadersOfEmployee:(NSNumber *)empId
{
    BasicHttpBinding_IEmployeeServiceBinding *binding = [EmployeeServiceSvc BasicHttpBinding_IEmployeeServiceBinding];
    EmployeeServiceSvc_GetAllLeader *request = [EmployeeServiceSvc_GetAllLeader new];
    request.employeeId = empId;
    
    BasicHttpBinding_IEmployeeServiceBindingResponse *response = [binding GetAllLeaderUsingParameters:request];
    for (id part in response.bodyParts)
    {
        if ([part isKindOfClass:[EmployeeServiceSvc_GetAllLeaderResponse class]])
        {
            emp_tns1_ArrayOfSP_GetAllParentEmployee_Result *result = [part GetAllLeaderResult];
            self.listOfLeadersResult = result;
            break;
        }
    }
}

-(void) getEmployeeHolidayInfo:(NSNumber *)empId
{
    BasicHttpBinding_ITimeServieBinding *binding = [TimeServieSvc BasicHttpBinding_ITimeServieBinding];
    TimeServieSvc_GetEmpHoliday *request = [TimeServieSvc_GetEmpHoliday new];
    request.employeeId = empId;
    
    BasicHttpBinding_ITimeServieBindingResponse *response = [binding GetEmpHolidayUsingParameters:request];
    for(id part in response.bodyParts)
    {
        if ([part isKindOfClass:[TimeServieSvc_GetEmpHolidayResponse class]])
        {
            self.employeeHolidayInfoResult = [part GetEmpHolidayResult];
        }
    }
}

-(void) getMethodOfLeave
{
    BasicHttpBinding_ITimeServieBinding *binding = [TimeServieSvc BasicHttpBinding_ITimeServieBinding];
    TimeServieSvc_GetWorkOffType *request = [TimeServieSvc_GetWorkOffType new];
    request.SysParamCode = TM_CT_METHODOFLEAVE;
    
    BasicHttpBinding_ITimeServieBindingResponse *response = [binding GetWorkOffTypeUsingParameters:request];
    for (id part in response.bodyParts)
    {
        if ([part isKindOfClass:[TimeServieSvc_GetWorkOffTypeResponse class]])
        {
            time_tns1_ArrayOfTB_ESS_SYSTEM_PARAMETER *results = [part GetWorkOffTypeResult];
            self.methodsOfLeaveResult = results;
        }
    }
}

-(req_tns1_ResponseModel *) createLeaveRequest:(req_tns1_LeaveRequestSubmitModel *)submitModel
{
    BasicHttpBinding_IRequestServiceBinding *binding = [RequestServiceSvc BasicHttpBinding_IRequestServiceBinding];
    RequestServiceSvc_CreateLeaveRequest *request = [RequestServiceSvc_CreateLeaveRequest new];
    request.model =  submitModel;
    
    BasicHttpBinding_IRequestServiceBindingResponse *response = [binding CreateLeaveRequestUsingParameters:request];
    for (id part in response.bodyParts)
    {
        if ([part isKindOfClass:[RequestServiceSvc_CreateLeaveRequestResponse class]])
        {
            req_tns1_ResponseModel *responseModel = [part CreateLeaveRequestResult];
            return responseModel;
        }
    }
    
    return nil;
}




@end
