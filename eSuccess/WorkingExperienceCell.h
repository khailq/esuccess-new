//
//  WorkingExperienceCell.h
//  eSuccess
//
//  Created by admin on 5/3/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WorkingExperienceCell : UITableViewCell
{
    IBOutlet UILabel *lblCompany;
    IBOutlet UILabel *lblDate;
    IBOutlet UILabel *lblJob;
    IBOutlet UILabel *lblRole;
    IBOutlet UILabel *lblResult;
    IBOutlet UILabel *lblNote;
}
@property(nonatomic, retain) IBOutlet UILabel *lblCompany;
@property(nonatomic, retain) IBOutlet UILabel *lblDate;
@property(nonatomic, retain) IBOutlet UILabel *lblJob;
@property(nonatomic, retain) IBOutlet UILabel *lblRole;
@property(nonatomic, retain) IBOutlet UILabel *lblResult;
@property(nonatomic, retain) IBOutlet UILabel *lblNote;
@end
