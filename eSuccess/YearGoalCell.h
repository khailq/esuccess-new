//
//  YearGoalCell.h
//  eSuccess
//
//  Created by admin on 5/28/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YearGoalCell : UITableViewCell
@property(nonatomic, retain) IBOutlet UITextView *tvDescrption;
@property(nonatomic, retain) IBOutlet UITextView *tvMeasure;
@property(nonatomic, retain) IBOutlet UITextView *tvUnit;
@property(nonatomic, retain) IBOutlet UITextView *tvExpense;
@property(nonatomic, retain) IBOutlet UITextView *tvCompleted;
@property(nonatomic, retain) IBOutlet UITextView *tvProgress;
//@property(nonatomic, retain) IBOutlet UITextView *tvProportion;
//@property(nonatomic, retain) IBOutlet UITextView *tvCreater;
//@property(nonatomic, retain) IBOutlet UITextView *tvFormart;
@end
