//
//  TableTitleCell.m
//  eSuccess
//
//  Created by HPTVIETNAM on 6/11/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "TableTitleCell.h"

@implementation TableTitleCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
