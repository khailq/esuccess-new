//
//  YearGoalViewController.m
//  eSuccess
//
//  Created by admin on 5/28/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "YearGoalViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "YearGoalCell.h"    
@interface YearGoalViewController ()

@property(nonatomic, strong) NSMutableArray *finacialArr;
@property(nonatomic, strong) NSMutableArray *finacialNameArr;
@property(nonatomic, strong) NSMutableArray *customerArr;
@property(nonatomic, strong) NSMutableArray *customerNameArr;
@property(nonatomic, strong) NSMutableArray *localArr;
@property(nonatomic, strong) NSMutableArray *localNameArr;
@property(nonatomic, strong) NSMutableArray *studyArr;
@property(nonatomic, strong) NSMutableArray *studyNameArr;

@end

@implementation YearGoalViewController

static NSString *yearGoalCell = @"YearGoalCell";
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self getData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 130;
//}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
        return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

- (void)fillDateCell:(YearGoalCell *)cell data:(NSMutableArray *)data row:(NSInteger)row
{
    if (row == 0) {
        cell.tvDescrption.text = @"Đặc tả";
        cell.tvMeasure.text = @"Thước đo";
        cell.tvUnit.text = @"Đơn vị";
        cell.tvExpense.text = @"Chi tiêu";
        cell.tvCompleted.text = @"Hoàn thành";
        cell.tvProgress.text = @"Tiến độ";
    } else{
        NSMutableArray *fTmp = [data objectAtIndex:row - 1];
        cell.tvDescrption.text = [fTmp objectAtIndex:0];
        cell.tvMeasure.text = [fTmp objectAtIndex:1];
        cell.tvUnit.text = [fTmp objectAtIndex:2];
        cell.tvExpense.text = [fTmp objectAtIndex:3];
        cell.tvCompleted.text = [fTmp objectAtIndex:4];
        cell.tvProgress.text = [fTmp objectAtIndex:5];
    }
}

- (void)getData
{
    _finacialNameArr = [[NSMutableArray alloc]init];
    [_finacialNameArr addObject:@"Tăng trưởng doanh thu 20%"];
    _finacialArr = [[NSMutableArray alloc]init];
    NSMutableArray *f = [[NSMutableArray alloc]init];
    NSMutableArray *f1 = [[NSMutableArray alloc]init];
    [f1 addObject:@"Doanh thu"];
    [f1 addObject:@""];
    [f1 addObject:@"Tỷ đồng"];
    [f1 addObject:@"800"];
    [f1 addObject:@"400"];
    [f1 addObject:@"50%"];
    [f addObject:f1];
    NSMutableArray *f2 = [[NSMutableArray alloc]init];
    [f2 addObject:@"Doanh thu"];
    [f2 addObject:@""];
    [f2 addObject:@"%"];
    [f2 addObject:@"25"];
    [f2 addObject:@"5"];
    [f2 addObject:@"20%"];
    [f addObject:f2];
    [_finacialArr addObject:f];
   // [self.tbFinacial reloadData];
    
    _customerArr = [[NSMutableArray alloc]init];
    _customerNameArr = [[NSMutableArray alloc]init];
    [_customerNameArr addObject:@"Tiếp cận 500 khách hàng tiềm năng"];
    NSMutableArray *c = [[NSMutableArray alloc]init];
    NSMutableArray *c1 = [[NSMutableArray alloc]init];
    [c1 addObject:@"Tổ chức Event"];
    [c1 addObject:@"Số lượng"];
    [c1 addObject:@"Tỷ đồng"];
    [c1 addObject:@"500"];
    [c1 addObject:@"5"];
    [c1 addObject:@"1%"];
    [c addObject:c1];
    NSMutableArray *c2 = [[NSMutableArray alloc]init];
    [c2 addObject:@"Marketing"];
    [c2 addObject:@"Khách hàng"];
    [c2 addObject:@"Số lượng"];
    [c2 addObject:@"200"];
    [c2 addObject:@"100"];
    [c2 addObject:@"50%"];
    [c addObject:c2];
    [_customerArr addObject:c];

    _localArr = [[NSMutableArray alloc]init];
    _localNameArr = [[NSMutableArray alloc]init];
    [_localNameArr addObject:@"Cải tiến qui trình kinh doanh"];
    NSMutableArray *l = [[NSMutableArray alloc]init];
    NSMutableArray *l1 = [[NSMutableArray alloc]init];
    [l1 addObject:@"Rút ngắn"];
    [l1 addObject:@"Tối ưu hoá"];
    [l1 addObject:@"Số lượng qui trình"];
    [l1 addObject:@"10"];
    [l1 addObject:@"1"];
    [l1 addObject:@"10%"];
    [l addObject:l1];
    NSMutableArray *l2 = [[NSMutableArray alloc]init];
    [l2 addObject:@"Giảm chi phí"];
    [l2 addObject:@"Tối ưu hoá"];
    [l2 addObject:@"Số lượng"];
    [l2 addObject:@"10"];
    [l2 addObject:@"10"];
    [l2 addObject:@"100%"];
    [l addObject:l2];
    [_localArr addObject:l];
    
    _studyArr = [[NSMutableArray alloc]init];
    _studyNameArr = [[NSMutableArray alloc]init];
    [_studyNameArr addObject:@"Phát triển năng lực đội ngũ"];
    NSMutableArray *s = [[NSMutableArray alloc]init];
    NSMutableArray *s1 = [[NSMutableArray alloc]init];
    [s1 addObject:@"Thi chứng chỉ"];
    [s1 addObject:@"Đào tạo"];
    [s1 addObject:@"Chứng chỉ"];
    [s1 addObject:@"4"];
    [s1 addObject:@"4"];
    [s1 addObject:@"100%"];
    [s addObject:s1];
    NSMutableArray *s2 = [[NSMutableArray alloc]init];
    [s2 addObject:@"Đào tạo"];
    [s2 addObject:@"Học tập"];
    [s2 addObject:@"Sales"];
    [s2 addObject:@"4"];
    [s2 addObject:@"2"];
    [s2 addObject:@"50%"];
    [s addObject:s2];
    [_studyArr addObject:s];

}

@end
