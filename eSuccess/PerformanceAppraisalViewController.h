//
//  PerformanceAppraisalViewController.h
//  eSuccess
//
//  Created by HPTVIETNAM on 6/21/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"
#import "EmpProfileLayerServiceSvc.h"  
@interface PerformanceAppraisalViewController : BaseDetailTableViewController
@property(nonatomic, strong) NSMutableArray *performAppraisalArr;
@property(nonatomic, strong) NSMutableArray *timeArr;

@end
