//
//  CoursesViewController.h
//  eSuccess
//
//  Created by HPTVIETNAM on 6/7/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"

@interface CoursesViewController : BaseDetailTableViewController

@property (strong, nonatomic) NSArray *courses;

@end
