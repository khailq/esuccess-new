//
//  BaseDetailTableViewController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 5/4/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"
#import "AppDelegate.h"
#import "CustomSplitViewController.h"

@interface BaseDetailTableViewController ()
{
    UIBarButtonItem *menuButton;
    UIView *tmpView;
}


@end

@implementation BaseDetailTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidDisappear:(BOOL)animated
{
    self.viewDidDisappear = YES;
    [super viewDidDisappear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.translucent = NO;
    
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(onMenuButtonClicked:)];
        self.navigationItem.leftBarButtonItems = @[menuButton];
    }
    
    CGRect tableRect = self.tableView.frame;
    UIView *emptyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableRect.size.width, 100)];
    self.tableView.tableFooterView = emptyView;
    
}

-(void)onMenuButtonClicked:(id)sender
{
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    CustomSplitViewController *splitVc = (CustomSplitViewController*)app.window.rootViewController;
    [splitVc toggleMasterView:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell*)getDefaultEmptyCell
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DefaultCell"];
    cell.textLabel.text = NoDataString;
    
    return cell;
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation))
    {
        if (menuButton == nil)
        {
            menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(onMenuButtonClicked:)];
        }
        self.navigationItem.leftBarButtonItem = menuButton;
    }
    else
        self.navigationItem.leftBarButtonItem = nil;
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
        //end of loading
        //for example [activityIndicator stopAnimating];
    }
    
    
}

@end
