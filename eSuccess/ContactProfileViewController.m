//
//  ContactProfileViewController.m
//  eSuccess
//
//  Created by admin on 6/15/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "ContactProfileViewController.h"
#import "HistoryProfileViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "DayLeaveDetailCell.h"
#import "EmployeeContact.h"
#import "AFNetworking.h"

@interface ContactProfileViewController ()<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>

@end

@implementation ContactProfileViewController
@synthesize contactArr;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)onGetEmployeeContactCompleted:(NSDictionary*)json
{
    
    NSArray* dataArr = (NSArray*)json[@"data"];
    EmployeeContact *empContact = nil;
    for (NSDictionary* dict in dataArr) {
        empContact = [EmployeeContact parseEmployeeContactByDict:dict];
        if (empContact.isCurrent.boolValue)
            break;
    }
    
    lblAddressContact.text = empContact.contactAddress;
    lblCompanyPhone.text = empContact.officephone;
    lblEmail.text = empContact.email;
    lblFax.text = empContact.fax;
    lblHomePhone.text = empContact.homephone;
    lblMobile.text = empContact.mobilephone;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Thông tin liên lạc";
    [self getEmployeeContact];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getEmployeeContact
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    BasicHttpBinding_IEmpProfileLayerServiceBinding *binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
    EmpProfileLayerServiceSvc_GetViewEmpProfileContactsByEmployeeId *request = [EmpProfileLayerServiceSvc_GetViewEmpProfileContactsByEmployeeId new];
    
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    request.employeeId = app.sysUser.EmployeeId;
    [binding GetViewEmpProfileContactsByEmployeeIdAsyncUsingParameters:request
                                                              delegate:self];
}

-(void)updateView:(empl_tns1_EmpProfileContact *)empContact
{
    lblAddressContact.text = empContact.ContactAddress;
    lblCompanyPhone.text = empContact.OfficePhone;
    lblEmail.text = empContact.Email;
    lblFax.text = empContact.Fax;
    lblHomePhone.text = empContact.HomePhone;
    lblMobile.text = empContact.MobileNumber;
    lblPermanentAddress.text = empContact.PermanentAddress;
    lblTemporaryAddress.text = empContact.TemporaryAddress;
}

-(void)operation:(BasicHttpBinding_IEmpProfileLayerServiceBinding *)operation completedWithResponse:(BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)response
{
    for (id part in response.bodyParts) {
        if ([part isKindOfClass:[EmpProfileLayerServiceSvc_GetViewEmpProfileContactsByEmployeeIdResponse class]])
        {
            empl_tns1_ArrayOfV_EmpProfileContact *empContacts = [part GetViewEmpProfileContactsByEmployeeIdResult];
            for(empl_tns1_EmpProfileContact *empContact in empContacts.V_EmpProfileContact)
            {
                if (empContact.IsCurrent.boolValue)
                {
                    [self updateView:empContact];
                    break;
                }
            }
        }
    }
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}



@end
