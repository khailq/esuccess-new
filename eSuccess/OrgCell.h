//
//  OrgCell.h
//  eSuccess
//
//  Created by HPTVIETNAM on 5/7/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfilePanelView.h"

@interface OrgCell : UITableViewCell

@property (strong, nonatomic) NSArray *children;

@end
