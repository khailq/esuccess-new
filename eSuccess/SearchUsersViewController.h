//
//  SearchUsersViewController.h
//  eSuccess
//
//  Created by HPTVIETNAM on 5/31/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseDetailTableViewController.h"

@interface SearchUsersViewController : BaseDetailTableViewController<UISearchDisplayDelegate>

@end
