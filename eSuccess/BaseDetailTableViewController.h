//
//  BaseDetailTableViewController.h
//  eSuccess
//
//  Created by HPTVIETNAM on 5/4/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSplitViewController.h"

@interface BaseDetailTableViewController : UITableViewController

-(UITableViewCell *)getDefaultEmptyCell;
@property (assign, nonatomic) BOOL viewDidDisappear;

@end
