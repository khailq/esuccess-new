//
//  DatePickerPopup.m
//  eSuccess
//
//  Created by Scott on 4/21/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "DatePickerPopup.h"
#define MyDateTimePickerPickerHeight 216
#define MyDateTimePickerToolbarHeight 44

@interface DatePickerPopup ()

@property (nonatomic, assign, readwrite) UIDatePicker *picker;
@property (nonatomic, assign) CGRect originalFrame;

@property (nonatomic, assign) id doneTarget;
@property (nonatomic, assign) SEL doneSelector;

- (void) donePressed;

@end

@implementation DatePickerPopup

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.originalFrame = frame;
        self.backgroundColor = [UIColor clearColor];
        
        CGFloat width = self.bounds.size.width;
        UIDatePicker *picker = [[UIDatePicker alloc] initWithFrame: CGRectMake(0, MyDateTimePickerToolbarHeight, width, MyDateTimePickerPickerHeight)];
        [self addSubview: picker];
        
        UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, width, MyDateTimePickerToolbarHeight)];
        toolbar.backgroundColor = picker.backgroundColor;
        [toolbar setOpaque:YES];
        //toolbar.barStyle = UIBarStyleBlackOpaque;
        
        UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle: @"Done" style: UIBarButtonItemStyleBordered target: self action: @selector(donePressed)];
        
       
        
        toolbar.items = [NSArray arrayWithObjects: flex, doneButton, nil];
        [self addSubview: toolbar];
        
        self.picker = picker;
    }
    
    return self;
}

- (void) setMode: (UIDatePickerMode) mode {
    self.picker.datePickerMode = mode;
}

- (void) donePressed {
//    if (self.doneTarget) {
//        if ([self.doneTarget respondsToSelector:self.doneSelector])
//            [self.doneTarget performSelector: self.doneSelector];
//    }
    [self.doneTarget performSelectorOnMainThread:self.doneSelector withObject:nil waitUntilDone:YES];
}

- (void) addTargetForDoneButton: (id) target action: (SEL) action {
    self.doneTarget = target;
    self.doneSelector = action;
}

- (void) setHidden: (BOOL) hidden animated: (BOOL) animated {
    CGRect newFrame = self.originalFrame;
    newFrame.origin.y += hidden ? MyDateTimePickerHeight : 0;
    if (animated) {
//        [UIView beginAnimations: @"animateDateTimePicker" context: nil];
//        [UIView setAnimationDuration: 0.5];
        [UIView setAnimationCurve: UIViewAnimationCurveEaseOut];
        
        //self.frame = newFrame;
        [UIView animateWithDuration:0.5 animations:^{
            self.frame = newFrame;
        } completion:^(BOOL finished) {
            if (hidden)
                [self removeFromSuperview];
        }];
        
        [UIView commitAnimations];
    } else {
        self.frame = newFrame;
        if (hidden)
            [self removeFromSuperview];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
