//
//  RewardViewController.m
//  eSuccess
//
//  Created by admin on 5/23/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "RewardViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "DayLeaveDetailCell.h"
#import "Employee Service/EmployeeServiceSvc.h"
@interface RewardViewController ()

@end

@implementation RewardViewController
@synthesize rewardArr;
@synthesize titleArr;
static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Khen thưởng";
    [self getData];
    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 130;
//}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [titleArr count] > 0 ? [titleArr objectAtIndex:section]: nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [rewardArr count] > 0 ? rewardArr.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [rewardArr count]> 0 ? 5: 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (rewardArr.count == 0) {
        return [self getDefaultEmptyCell];
    }
    
    DayLeaveDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
//    if (cell == nil) {
//        cell = [[DayLeaveDetailCell alloc]init];
//    }
//    cell.lbLeft.font = [UIFont systemFontOfSize:14];
//    cell.lbLeft.backgroundColor = [UIColor clearColor];
//    cell.lbRight.font = [UIFont boldSystemFontOfSize:16];
//    cell.lbRight.backgroundColor = [UIColor clearColor];
//    
//    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
//    [formatter setDateFormat:@"dd/MM/yyyy"];
//    
//    int section = indexPath.section;
//    empl_tns1_V_EmpProfileReward *reward = [rewardArr objectAtIndex:section];
//    
//    if (indexPath.row == 0) {
//        cell.lbLeft.text = @"Quyết định";
//        cell.lbRight.text = reward.DecisionNo;
//    }
//    if (indexPath.row == 1) {
//        cell.lbLeft.text = @"Loại khen thưởng";
//        cell.lbRight.text = reward.RewardName;
//    }
//    if (indexPath.row == 2) {
//        cell.lbLeft.text = @"Hình thức";
//        cell.lbRight.text = reward.RewardMethodName;
//    }
//    
//    if (indexPath.row == 3) {
//        cell.lbLeft.text = @"Ngày ban hành";
//        cell.lbRight.text = [formatter stringFromDate:reward.DateOfIssue];
//    }
//    if (indexPath.row == 4) {
//        cell.lbLeft.text = @"Số tiền";
//        cell.lbRight.text = reward.MonetaryRewardValue.stringValue;
//    }
    return cell;
}

#pragma get Emp Reward
-(void)getData
{
    rewardArr = [[NSMutableArray alloc]init];
    titleArr = [[NSMutableArray alloc]init];
    
    [self getRewardById];
}
-(void)getRewardById
{
//    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    hud.labelText = @"Đang cập nhật";
//    
//    BasicHttpBinding_IEmpProfileLayerServiceBinding *binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
//    EmpProfileLayerServiceSvc_GetViewEmpProfileRewardById *request = [[EmpProfileLayerServiceSvc_GetViewEmpProfileRewardById alloc]init];
//    AppDelegate *app = [UIApplication sharedApplication].delegate;
//    if (app.otherEmpId != nil)
//        request.employeeId = app.otherEmpId;
//    else
//        request.employeeId = app.sysUser.EmployeeId ;
//    //request.employeeId = [NSNumber numberWithInt:19];
//    [binding GetViewEmpProfileRewardByIdAsyncUsingParameters:request delegate:self];
}



- (void)operation:(BasicHttpBinding_IEmpProfileLayerServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)response
{
//    if ([response.bodyParts count] == 0) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi kết nối" message:@"Không thể kết nối đến server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [alert show];
//    }
//    
//    for (id mine in response.bodyParts) {
//        if ([mine isKindOfClass:[EmpProfileLayerServiceSvc_GetViewEmpProfileRewardByIdResponse class]]) {
//            empl_tns1_ArrayOfV_EmpProfileReward *result = [mine GetViewEmpProfileRewardByIdResult];
//            rewardArr = result.V_EmpProfileReward;
//            for (empl_tns1_V_EmpProfileReward *reward in rewardArr) {
//                [titleArr addObject:reward.OrgRewardTitle];
//            }
//        }
//        
//
//    }
//    
//    if (self != nil && !self.viewDidDisappear)
//    {
//        [self.tableView reloadData];
//        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//    }
}

@end
