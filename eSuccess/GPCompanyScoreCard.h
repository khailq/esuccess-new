//
//  GPCompanyScoreCard.h
//  eSuccess
//
//  Created by admin on 6/6/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GPCompanyScoreCard : NSObject
@property(nonatomic, strong) NSNumber * GPScoreCardId;
@property(nonatomic, strong) NSString *statement;
@property(nonatomic, strong) NSString *metric;
@property(nonatomic, strong) NSNumber * target;
@property(nonatomic, strong) NSNumber * actual;
@property(nonatomic, strong) NSNumber * weight;
@property(nonatomic, strong) NSString *creatorFullName;
@property(nonatomic, strong) NSNumber * progress;


+(GPCompanyScoreCard *)parseScoreCardFromDict:(NSDictionary*)dict;

@end
