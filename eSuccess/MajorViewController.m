//
//  MajorViewController.m
//  eSuccess
//
//  Created by admin on 5/7/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "MajorViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
@interface MajorViewController ()

@end

@implementation MajorViewController
@synthesize lblEducationName;
@synthesize lblMajor;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Trình độ chuyên môn";
    [self getData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma getData
- (void)getData
{
}

@end
