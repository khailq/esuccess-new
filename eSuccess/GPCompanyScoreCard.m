//
//  GPCompanyScoreCard.m
//  eSuccess
//
//  Created by admin on 6/6/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "GPCompanyScoreCard.h"

@implementation GPCompanyScoreCard

+(GPCompanyScoreCard *)parseScoreCardFromDict:(NSDictionary *)dict
{
    GPCompanyScoreCard *scoreCard = [[GPCompanyScoreCard alloc]init];
    scoreCard.GPScoreCardId = [dict objectForKey:@"GPCompanyScoreCardId"];
    scoreCard.statement = [dict objectForKey:@"Statement"];
    scoreCard.metric = [dict objectForKey:@"Metric"];
    scoreCard.target = [dict objectForKey:@"Target"];
    scoreCard.actual = [dict objectForKey:@"Actual"];
    scoreCard.weight = [dict objectForKey:@"Weight"];
    scoreCard.progress = [dict objectForKey:@"Progress"];
    NSDictionary *employee = [dict objectForKey:@"Employee"];
    scoreCard.creatorFullName = [employee objectForKey:@"FullName"];
    
    return scoreCard;
}
@end
