//
//  OrgUnit.h
//  eSuccess
//
//  Created by HPTVIETNAM on 5/18/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrgUnit : NSObject

@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *functionDescription;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *objectiveDescription;
@property (strong, nonatomic) NSString *workLocation;
@property (strong, nonatomic) NSString *leaderCode;
@property (strong, nonatomic) NSString *leaderFullName;
@property (strong, nonatomic) NSString *leaderFirstName;
@property (strong, nonatomic) NSString *leaderLastName;
@property (strong, nonatomic) NSString *leaderPositionName;

@property (strong, nonatomic) NSString *parentCode;
@property (strong, nonatomic) NSString *leaderImageURL;
@property (strong, nonatomic) NSString *unitCode;

@property (strong, nonatomic) NSNumber *companyId;
@property (strong, nonatomic) NSNumber *unitId;
@property (strong, nonatomic) NSNumber *leaderId;
@property (strong, nonatomic) NSNumber *parentId;
@property (strong, nonatomic) NSNumber *totalEmployees;

@property (strong, nonatomic) NSNumber* haveLeader;
@property (assign, nonatomic) BOOL isDeleted;

@end
