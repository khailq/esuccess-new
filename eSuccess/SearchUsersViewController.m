//
//  SearchUsersViewController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 5/31/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "SearchUsersViewController.h"
#import "AppDelegate.h"
#import "Employee Service/EmployeeServiceSvc.h"
#import "UIImageView+AFNetworking.h"
#import "MBProgressHUD.h"

@interface SearchUsersViewController ()<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate, UISearchBarDelegate>
{
    NSArray *searchResults;
}

@property (weak, nonatomic) IBOutlet UISearchDisplayController *seachController;

@end

@implementation SearchUsersViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.searchDisplayController.searchBar becomeFirstResponder];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.searchDisplayController.searchResultsTableView.tag = 10;
    self.searchDisplayController.searchBar.placeholder = @"Mã nhân viên";
    self.searchDisplayController.searchBar.delegate = self;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Search Bar delegate

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.searchDisplayController setActive:NO animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:MasterChangedNotification object:self];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView.tag == 1)
        return [super numberOfSectionsInTableView:tableView];
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag == 1)
        return [super tableView:tableView numberOfRowsInSection:section];
    return searchResults.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 1)
        return [super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    static NSString *CellIdentifier = @"SearchCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    emp_tns1_SP_FTS_EMP_PROFILE_ORG_UNIT_Result *emp = searchResults[indexPath.row];
    cell.textLabel.text = emp.FullName;
    
    return cell;
}

-(void)searchDisplayController:(UISearchDisplayController *)controller didLoadSearchResultsTableView:(UITableView *)tableView
{
    [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"SearchCell"];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self searchForString:searchString];
    return NO;
}

-(void)searchForString:(NSString *)input
{
    if ([input isEqualToString:@""])
        return;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    
    BasicHttpBinding_IEmployeeServiceBinding *binding = [EmployeeServiceSvc BasicHttpBinding_IEmployeeServiceBinding];
    EmployeeServiceSvc_FTS_EmpProfileSearchAll *request = [EmployeeServiceSvc_FTS_EmpProfileSearchAll new];

    request.condition = input;
    request.userId = app.sysUser.SysUserId;
    request.companyId = app.sysUser.CompanyId;
    request.take = [NSNumber numberWithInt:20];
    
    [binding FTS_EmpProfileSearchAllAsyncUsingParameters:request delegate:self];
}

-(void)operation:(BasicHttpBinding_IEmployeeServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmployeeServiceBindingResponse *)response
{
    for(id mine in response.bodyParts)
    {
        if ([mine isKindOfClass:[EmployeeServiceSvc_FTS_EmpProfileSearchAllResponse class]])
        {
            emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_ORG_UNIT_Result *result = [mine FTS_EmpProfileSearchAllResult];
            searchResults = result.SP_FTS_EMP_PROFILE_ORG_UNIT_Result;
            [self.searchDisplayController.searchResultsTableView reloadData];
        }
    }
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 10 && searchResults.count > 0)
    {
        emp_tns1_SP_FTS_EMP_PROFILE_ORG_UNIT_Result *emp = searchResults[indexPath.row];
        AppDelegate *app = [UIApplication sharedApplication].delegate;
        app.otherEmpId = emp.EmployeeId;
        
        [self.searchDisplayController.searchBar endEditing:YES];
        
        [self performSegueWithIdentifier:@"00" sender:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:BeginViewProfileNotification object:self];
    }
}



@end
