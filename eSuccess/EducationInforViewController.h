//
//  EducationInforViewController.h
//  eSuccess
//
//  Created by admin on 5/10/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"
@interface EducationInforViewController : BaseDetailTableViewController
@property(nonatomic, retain) NSMutableArray *educationArray;
@property(nonatomic, retain) NSMutableArray *univeristyArray;
@end
