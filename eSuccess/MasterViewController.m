//
//  MasterViewController.m
//  Presidents
//
//  Created by HPTVIETNAM on 4/12/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "RootMasterController.h"

#import "MasterViewController.h"
#import "AppDelegate.h"
#import "HomeDetailViewController.h"
@interface MasterViewController ()
{
    NSIndexPath *savedIndexPath;
}
@end

@implementation MasterViewController


- (void)awakeFromNib
{
    self.clearsSelectionOnViewWillAppear = NO;
    self.contentSizeForViewInPopover = CGSizeMake(320.0, 600.0);
    [super awakeFromNib];

}

-(void)selectFirstRow
{
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView selectRowAtIndexPath:index animated:NO scrollPosition:UITableViewScrollPositionNone];
}

- (void)performSegue:(NSString *)segueId
{
    @try {
        [self performSegueWithIdentifier:segueId sender:self];
    }
    @catch (NSException *exception) {
        NSLog(@"Segue %@ not found", segueId);
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    id destination = segue.destinationViewController;
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    UISplitViewController *splitViewController = (UISplitViewController*) app.window.rootViewController;
    
    
        //UINavigationController *nav = (UINavigationController*)destination;
        UIViewController *home = (UIViewController*)destination;
        
        splitViewController.delegate = (id)home;
        
//        if ([home respondsToSelector:@selector(setLeftBarButtonItem:)] && [home respondsToSelector:@selector(setMasterPopoverController:)])
//        {
//            [home setValue:app.masterPopoverController forKey:@"masterPopoverController"];
//            [home setValue:app.leftBarButtonItem forKey:@"leftBarButtonItem"];
//        }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceivedNotification:) name:ViewEmployeeModeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceivedNotification:) name:EndViewEmployeeModeNotification object:nil];
}

-(void)onReceivedNotification:(NSNotification*) notification
{
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    NSString *key = NSStringFromClass([MasterViewController class]);
    NSIndexPath *index = app.selectedIndexDictionary[key];
    
    if ([notification.name isEqualToString:ViewEmployeeModeNotification])
    {
        _viewProfileMode = YES;
        self.title = app.otherEmpName;
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(endViewProfile:)];
        self.navigationItem.leftBarButtonItem = item;
        if (index != nil)
            savedIndexPath = index;
    }

    [self.tableView reloadData];
    
//    if ([notification.name isEqualToString:ViewEmployeeModeNotification])
//    {
        //[self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
        //[self performSegueWithIdentifier:@"01" sender:self];
    
//    }
//    else
//    {
//        app.selectedIndexDictionary[key] = savedIndexPath;
//        NSString *identifier = [NSString stringWithFormat:@"%d%d", savedIndexPath.section, savedIndexPath.row];
//        [self.tableView selectRowAtIndexPath:index animated:NO scrollPosition:UITableViewScrollPositionNone];
//        [self performSegueWithIdentifier:identifier sender:self];
//        savedIndexPath = nil;
//    }
}

-(void)endViewProfile:(id)sender
{
    _viewProfileMode = NO;
    self.title = @"eSuccess";
    self.navigationItem.leftBarButtonItem = nil;
    
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    [app setOtherEmployeeInfo:nil name:nil];
//    if (savedIndexPath == nil)
//        savedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//    
//    NSString *identifier = [NSString stringWithFormat:@"%d%d", savedIndexPath.section, savedIndexPath.row];
//    [self.tableView selectRowAtIndexPath:savedIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
//    [self performSegue:identifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{    
    return 4;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleNone;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int section = indexPath.section;
    
    if (section == 2) {
        return 44;
    } else {
        return [super tableView:tableView heightForRowAtIndexPath:indexPath];
    }
}

- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return [super tableView:tableView indentationLevelForRowAtIndexPath:indexPath];

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0 ) {
        if (!self.viewProfileMode)
            return 4;
        return 2;
    } else {
        return [super tableView:tableView numberOfRowsInSection:section];
    }
}


@end
