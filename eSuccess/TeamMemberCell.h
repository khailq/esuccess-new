//
//  TeamMemberCell.h
//  eSuccess
//
//  Created by HPTVIETNAM on 5/28/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TeamMemberCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *avatar;
@property (strong, nonatomic) IBOutlet UILabel *lb_empName;
@property (strong, nonatomic) IBOutlet UILabel *lb_position;

@end
