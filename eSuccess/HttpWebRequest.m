//
//  HttpWebRequest.m
//  ePass
//
//  Created by Scott on 4/10/13.
//  Copyright (c) 2013 Scott. All rights reserved.
//

#import "HttpWebRequest.h"

@implementation HttpWebRequest

+(void)getDataAsyncWithURL:(NSString*)url completionHandler:(void (^)(NSData* returnedData, NSError* error))onCompletedBlock
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if (!error)
            onCompletedBlock(data, error);
    }];
}

+(void)postDataAsyncToURL:(NSString*)url postBody:(NSData*)data completionHandler:(void (^)(NSData *returnData, NSError *error))onCompletedBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:@{@"content-type": @"application/json"}];
    [request setHTTPBody:data];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *rData, NSError *error) {
        onCompletedBlock(rData, error);
    }];
}

@end
