//
//  WorkingExperienceViewController.m
//  eSuccess
//
//  Created by admin on 5/3/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "WorkProcessingViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "DayLeaveDetailCell.h"
#import "M13Checkbox.h"
#define CheckboxCellIdentifier @"CheckboxCell"

@interface WorkProcessingViewController ()

@end

@implementation WorkProcessingViewController
@synthesize workingExpsArray;
@synthesize timeArray;
static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Quá trình công tác";
    [self getData];
    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
}


#pragma mark - Table view data source

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 130;
//}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([timeArray count] > 0) {
        return [timeArray objectAtIndex:section];
    }
    return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [timeArray count] > 0 ? timeArray.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [timeArray count] > 0 ? 9 : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (timeArray.count == 0)
        return [self getDefaultEmptyCell];
    
    DayLeaveDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
    if (cell == nil) {
        cell = [[DayLeaveDetailCell alloc]init];
    }
    cell.lbLeft.font = [UIFont systemFontOfSize:14];
    cell.lbLeft.backgroundColor = [UIColor clearColor];
    cell.lbRight.font = [UIFont boldSystemFontOfSize:16];
    cell.lbRight.backgroundColor = [UIColor clearColor];
    
    int section = indexPath.section;
    empl_tns1_V_EmpProfileWorkingExperience *workingExp = workingExpsArray[section];
    if (indexPath.row == 0) {
        cell.lbLeft.text = @"Công ty";
        cell.lbRight.text = workingExp.CompanyName;
    }

    if (indexPath.row == 1) {
        cell.lbLeft.text = @"Dự án";
        cell.lbRight.text = workingExp.OrgProjectTypeName;
    }
    if (indexPath.row == 2) {
        cell.lbLeft.text = @"Thời gian";
        cell.lbRight.text = workingExp.OrgTimeInChargeDescription;
    }
    if (indexPath.row == 3) {
        cell.lbLeft.text = @"Nhiệm vụ";
        cell.lbRight.text = workingExp.Responsibilities;
    }
    if (indexPath.row == 4) {
        cell.lbLeft.text = @"Vai trò";
        cell.lbRight.text = workingExp.Role;
    }
    if (indexPath.row == 5) {
        cell.lbLeft.text = @"Đánh giá";
        cell.lbRight.text = workingExp.RatingDescription;
    }
    if (indexPath.row == 6) {
        cell.lbLeft.text = @"Thành tựu đạt được";
        cell.lbRight.text = workingExp.NotableAchievements;
    }
    if (indexPath.row == 7) {
        cell.lbLeft.text = @"Ghi chú";
        cell.lbRight.text = workingExp.Note;
        
        return cell;
    }
    return cell;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma getData
- (void)getData
{
    workingExpsArray = [[NSMutableArray alloc]init];
    timeArray = [[NSMutableArray alloc]init];
    
    [self getEmpWorkingExperienceById];
}

- (void)getEmpWorkingExperienceById
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Đang cập nhật";
    
    BasicHttpBinding_IEmpProfileLayerServiceBinding *binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
    EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingExperienceById *request = [[EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingExperienceById alloc] init];
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    if (app.otherEmpId != nil)
        request.employeeId = app.otherEmpId;
    else
        request.employeeId = app.sysUser.EmployeeId ;
    //request.EmployeeId = [NSNumber numberWithInt:2];
    [binding GetViewEmpProfileWorkingExperienceByIdAsyncUsingParameters:request delegate:self];
}

- (void)operation:(BasicHttpBinding_IEmpProfileLayerServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)response
{
    if ([response.bodyParts count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi kết nối" message:@"Không thể kết nối đến server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    for (id mine in response.bodyParts){
        if ([mine isKindOfClass:[EmpProfileLayerServiceSvc_GetViewEmpProfileWorkingExperienceByIdResponse class]]) {
            empl_tns1_ArrayOfV_EmpProfileWorkingExperience *result = [mine GetViewEmpProfileWorkingExperienceByIdResult];
            workingExpsArray = result.V_EmpProfileWorkingExperience;
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"dd/MM/yyyy"];
            for (empl_tns1_V_EmpProfileWorkingExperience *working in workingExpsArray) {
                if (working.StartDate == nil && working.EndDate == nil) {
                    [timeArray addObject:@""];
                } else
                    [timeArray addObject:[NSString stringWithFormat:@"%@ - %@",[formatter stringFromDate:working.StartDate ], [formatter stringFromDate:working.EndDate ]]];
            }
        }
        //        if ([mine isKindOfClass:[SOAPFault class]])
        //        {
        //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Server error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //            [alert show];
        //        }
    }
    
    if (!self.viewDidDisappear)
        [self.tableView reloadData];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

@end
