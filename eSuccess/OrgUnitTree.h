//
//  OrgUnitTree.h
//  eSuccess
//
//  Created by HPTVIETNAM on 5/18/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrgUnit.h"

@interface OrgUnitTree : NSObject

@property (strong, nonatomic) OrgUnit *root;
@property (weak, nonatomic) OrgUnitTree *parent;
@property (strong, nonatomic) NSMutableArray *children;     // List of OrgUnitTree objects

@end
