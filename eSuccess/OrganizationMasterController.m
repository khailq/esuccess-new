//
//  OrganizationMasterController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 5/4/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "OrganizationMasterController.h"

#define OrgChartMenu @"Sơ đồ tổ chức"
#define ReportEmployee @"Nhân viên cấp dưới"

@interface OrganizationMasterController ()
{
//    NSDictionary *leaderMenu;
//    NSDictionary *employeeMenu;
}
@end

@implementation OrganizationMasterController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Organization";
    
//    leaderMenu = @{@"My organization": @[OrgChartMenu]};
//    employeeMenu = @{@"My team": @[ReportEmployee, @"Chương trình công tác", @"Thông báo nội bộ"]};
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//
//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    AppDelegate *app = [UIApplication sharedApplication].delegate;
//    if (app.loginEmployeeInfo.IsUnitLeader.boolValue)
//        return [super numberOfSectionsInTableView:tableView];
//    return 1;
//}
//
//-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    AppDelegate *app = [UIApplication sharedApplication].delegate;
//    if (app.loginEmployeeInfo.IsUnitLeader.boolValue)
//        return [super tableView:tableView numberOfRowsInSection:section];
//    return [super tableView:tableView numberOfRowsInSection:1];
//}
//
//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    AppDelegate *app = [UIApplication sharedApplication].delegate;
//    if (app.loginEmployeeInfo.IsUnitLeader.boolValue)
//        return [super tableView:tableView cellForRowAtIndexPath:indexPath];
//    return [super tableView:tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:1]];
//}
//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
//}
@end
