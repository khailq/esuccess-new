//
//  ApplicationSharedData.h
//  eSuccess
//
//  Created by HPTVIETNAM on 4/19/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "tns2.h"
#import "RouterViewController.h"

@interface ApplicationSharedData : NSObject
{
}

@property (strong, nonatomic) tns2_Employee *loginEmployee;
@property (strong, nonatomic) tns2_V_EmpBasicInformation *loginEmployeeInfo;

@property (retain, nonatomic) RouterViewController *router;

+(ApplicationSharedData*)getInstance;
-(void)performSegueWithIdentifier:(NSString*) segueID;

@end
