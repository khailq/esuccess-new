//
//  LeaverRequestViewController.h
//  Presidents
//
//  Created by HPTVIETNAM on 4/13/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DatePickerPopup.h"
#import "SecurityServiceSvc.h"
//#import "Leave Request Service/tns1.h"
@interface LeaverRequestViewController : UITableViewController<UIAlertViewDelegate>

@property (strong, nonatomic) DatePickerPopup *datePicker;
@property (strong, nonatomic) IBOutlet UITextView *tb_reason;
@property (strong, nonatomic) UIBarButtonItem *rightItem;
@property (strong, nonatomic) IBOutlet UILabel *lb_fromDate;
@property (strong, nonatomic) IBOutlet UILabel *lb_toDate;
@property (strong, nonatomic) IBOutlet UILabel *lb_absenceType;
@property (strong, nonatomic) IBOutlet UILabel *lb_approver;
@property (strong, nonatomic) IBOutlet UILabel *lb_issueDate;

@property (strong, nonatomic) IBOutlet UILabel *lb_empCode;
@property (strong, nonatomic) IBOutlet UILabel *lb_empName;
@property (strong, nonatomic) IBOutlet UILabel *lb_email;
@property (strong, nonatomic) IBOutlet UILabel *lb_unitName;
@property (strong, nonatomic) IBOutlet UILabel *lb_position;

@property (strong, nonatomic) UIPopoverController *popover;

@property (strong, nonatomic) NSString *leaveTypeText;
@property (strong, nonatomic) NSString *confirmerName;

@property (nonatomic, assign) BOOL editable;
@property (strong, nonatomic) tns1_SelfLeaveRequest *leaveRequest;
@end
