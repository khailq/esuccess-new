//
//  DashBoardViewController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 5/28/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "DashBoardViewController.h"
#import "AppDelegate.h"
#import "CustomSplitViewController.h"
#import "NSString+FontAwesome.h"
#import "MBProgressHUD.h"

#define P_FirstViewFrame CGRectMake(20, 142, 233, 57)
#define P_SecondViewFrame CGRectMake(268, 142, 233, 57)
#define P_ThirdViewFrame CGRectMake(515, 142, 233, 57)

#define P_MyProfileView CGRectMake(20, 252, 233, 220)
#define P_MyTaskView CGRectMake(268, 252, 233, 220)
#define P_MyJobView CGRectMake(515, 252, 233, 220)
#define P_MyPerformanceView CGRectMake(20, 528, 233, 220)
#define P_SelfServiceView CGRectMake(268, 528, 233, 220)
#define P_ELearningView CGRectMake(515, 528, 233, 220)

#define P_BankLogoView CGRectMake(20, 32, 233, 58)
#define P_eSuccessLogoView CGRectMake(259, 846, 251, 66)

#define L_FirstViewFrame CGRectMake(103, 125, 233, 57)
#define L_SecondViewFrame CGRectMake(398, 125, 233, 57)
#define L_ThirdViewFrame CGRectMake(688, 125, 233, 57)

#define L_MyProfileView CGRectMake(103, 205, 233, 220)
#define L_MyTaskView CGRectMake(398, 205, 233, 220)
#define L_MyJobView CGRectMake(688, 205, 233, 220)
#define L_MyPerformanceView CGRectMake(103, 433, 233, 220)
#define L_SelfServiceView CGRectMake(398, 433, 233, 220)
#define L_ELearningView CGRectMake(688, 433, 233, 220)

#define L_BankLogoView CGRectMake(101, 41, 233, 58)
#define L_eSuccessLogoView CGRectMake(387, 674, 251, 66)


@interface DashBoardViewController ()<UIWebViewDelegate>
{
    BOOL toggle;
}

- (IBAction)btnLogoClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIImageView *bankLogo;
@property (weak, nonatomic) IBOutlet UIView *v_myTasks;
@property (weak, nonatomic) IBOutlet UIView *v_myPerformance;
@property (weak, nonatomic) IBOutlet UIView *v_selfService;
@property (weak, nonatomic) IBOutlet UIView *v_myProfile;
@property (weak, nonatomic) IBOutlet UIView *v_myJob;
@property (weak, nonatomic) IBOutlet UIView *v_eLearning;
@property (weak, nonatomic) IBOutlet UIButton *btn_eSuccessLogo;

@property (weak, nonatomic) IBOutlet UIView *firstView;
@property (weak, nonatomic) IBOutlet UILabel *lb_firstIcon;
@property (weak, nonatomic) IBOutlet UIView *secondView;
@property (weak, nonatomic) IBOutlet UILabel *lb_secondView;
@property (weak, nonatomic) IBOutlet UIView *thirdView;
@property (weak, nonatomic) IBOutlet UILabel *lb_thirdIcon;
@end

@implementation DashBoardViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.lb_firstIcon.text = [NSString fontAwesomeIconStringForIconIdentifier:@"icon-screenshot"];
    self.lb_firstIcon.textColor = [UIColor cyanColor];
    self.lb_firstIcon.font = [UIFont fontWithName:kFontAwesomeFamilyName size:40];
    
    self.lb_secondView.text = [NSString fontAwesomeIconStringForIconIdentifier:@"icon-comments"];
    self.lb_secondView.textColor = [UIColor orangeColor];
    self.lb_secondView.font = [UIFont fontWithName:kFontAwesomeFamilyName size:40];
    
    self.lb_thirdIcon.text = [NSString fontAwesomeIconStringForIconIdentifier:@"icon-signal"];
    self.lb_thirdIcon.textColor = [UIColor cyanColor];
    self.lb_thirdIcon.font = [UIFont fontWithName:kFontAwesomeFamilyName size:40];
    
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    CustomSplitViewController *custom = (CustomSplitViewController*)app.window.rootViewController;
    custom.isLandscapeOK = NO;
    [custom setMyToolBarHidden:NO animation:NO];
    
    [self setStyleForView:_firstView];
    [self setStyleForView:_secondView];
    [self setStyleForView:_thirdView];
    
    [MBProgressHUD showHUDAddedTo:self.webView animated:YES];
    self.webView.delegate = self;
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"chart" ofType:@"html"];
    
    NSString *htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    
    [self.webView loadHTMLString:htmlString baseURL:nil];
    self.webView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.webView.layer.borderWidth = 0.5;
    self.webView.layer.cornerRadius = 8;
    self.webView.layer.masksToBounds = YES;
}

-(void)setStyleForView:(UIView *)view
{
    view.backgroundColor = [UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:1.0];
    view.layer.borderColor = [UIColor darkGrayColor].CGColor;
    view.layer.borderWidth = 0.5;
    view.layer.cornerRadius = 8;
    view.layer.masksToBounds = false;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btnClicked:(id)sender
{
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    CustomSplitViewController *splitVC = (CustomSplitViewController*)app.window.rootViewController;
    [splitVC setMyToolBarHidden:NO animation:YES];
    splitVC.isLandscapeOK = YES;
    
    UIButton *btn = (UIButton*)sender;
    switch (btn.tag) {
        case 1:
            [splitVC selectTab:Profile];
            break;
        case 2:
            [splitVC selectTab:Task];
            break;
        case 3:
            [splitVC selectTab:Job];
            break;
        case 4:
            [splitVC selectTab:Performance];
            break;
        case 5:
            [splitVC selectTab:SelfService];
            break;
        default:
            [splitVC selectTab:Learning];
            break;
    }
}


- (IBAction)btnLogoClicked:(id)sender {
    if (!toggle)
    {
        self.bankLogo.image = [UIImage imageNamed:@"vib-logo.png"];
        CGRect newRect = CGRectMake(self.bankLogo.frame.origin.x, self.bankLogo.frame.origin.y, self.bankLogo.frame.size.width, self.bankLogo.frame.size.height + 10);
        self.bankLogo.frame = newRect;
        self.view.backgroundColor = [UIColor colorWithRed:155.0/255.0 green:12.0/255.0 blue:24.0/255.0 alpha:1.0];
    }
    else
    {
        CGRect newRect = CGRectMake(self.bankLogo.frame.origin.x, self.bankLogo.frame.origin.y, self.bankLogo.frame.size.width, self.bankLogo.frame.size.height - 10);
        self.bankLogo.frame = newRect;
        self.bankLogo.image = [UIImage imageNamed:@"vietinbank.png"];
        self.bankLogo.frame = newRect;
        self.view.backgroundColor = [UIColor colorWithRed:241.0/255.0 green:186.0/255.0 blue:54.0/255.0 alpha:1.0];
    }
    toggle = !toggle;
}

- (BOOL)shouldAutorotate {
    return NO;
}


- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [MBProgressHUD hideHUDForView:webView animated:YES];
}
@end
