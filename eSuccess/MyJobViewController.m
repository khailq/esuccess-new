//
//  MyJobViewController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 5/25/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "MyJobViewController.h"

@interface MyJobViewController ()

@end

@implementation MyJobViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.name = MyJobName;
    [super viewDidLoad];
    self.title = @"My Job";
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
