//
//  WorkingExperienceViewController.h
//  eSuccess
//
//  Created by admin on 5/3/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//


#import "BaseDetailTableViewController.h"
#import "EmpProfileLayerServiceSvc.h"
@interface WorkProcessingViewController : BaseDetailTableViewController<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>
@property(nonatomic, strong) NSMutableArray *workingExpsArray;
@property(nonatomic, strong) NSMutableArray *timeArray;
@end
