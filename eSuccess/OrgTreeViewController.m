//
//  OrgTreeViewController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 5/18/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "OrgTreeViewController.h"
#import "OrgUnitTree.h"
#import "AFNetworking.h"
#import "OrgUnitView.h"
#import "InnerOrgTreeViewController.h"
#import "AppDelegate.h"


#define GetOrgUnitURL @"http://10.0.18.105:5002/OrganizationStructure/GetDataJsonOrgUnits"

@interface OrgTreeViewController ()
{
    OrgUnitTree *orgTree;
    NSMutableDictionary *allOrgUnitDict;
    NSMutableArray *level1Index;
}
@end

@implementation OrgTreeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self getOrgUnitTree];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    level1Index = [NSMutableArray array];
    self.title = @"Sơ đồ tổ chức";
}

-(void)updateViewForOrgUnitId:(NSNumber*)unitId
{
    OrgUnitTree *tree = allOrgUnitDict[[NSNumber numberWithInt:1]];
    if (tree != nil)
    {
        OrgUnitView *view = [self createOrgUnitView];
        if (view != nil)
        {
            view.lbOrgName.text = tree.root.name;
            view.lbNumberOfEmployees.text = tree.root.totalEmployees.stringValue;
//            CGFloat mid = CGRectGetMinX(self.view.frame);
//            CGRect rect = CGRectMake(mid - view.bounds.size.width / 2.0, 10, view.bounds.size.width, view.bounds.size.height);
//            view.frame = rect;
            [self.view addSubview:view];
        }
    }
}

-(OrgUnitView *)createOrgUnitView
{
    NSArray *bundle = [[NSBundle mainBundle] loadNibNamed:@"OrgUnitView" owner:self options:nil];
    for(id item in bundle)
        if ([item isKindOfClass:[OrgUnitView class]])
            return item;
    
    return nil;
}

-(void)getOrgUnitTree
{
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:GetOrgUnitURL]];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        allOrgUnitDict = [self parseJSON:JSON];
        [self.tableView reloadData];
        NSLog(@"count: %d", allOrgUnitDict.count);
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Lỗi server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }];
    
    [operation start];
}

-(OrgUnit *)parseOrgUnit:(NSDictionary *)dictData
{
    OrgUnit *unit = [[OrgUnit alloc] init];
    unit.address = (NSString *)dictData[@"Address"];
    unit.companyId = (NSNumber *)dictData[@"CompanyId"];
    unit.functionDescription = (NSString *)dictData[@"Function"];
    unit.name = (NSString *)dictData[@"Name"];
    unit.objectiveDescription = (NSString *)dictData[@"Objective"];
    unit.unitCode = (NSString *)dictData[@"OrgUnitCode"];
    unit.leaderCode = (NSString *)dictData[@"OrgUnitLeaderCode"];
    unit.leaderFirstName = (NSString *)dictData[@"OrgUnitLeaderFirstName"];
    unit.leaderFullName = (NSString *)dictData[@"OrgUnitLeaderFullName"];
    unit.leaderLastName = (NSString *)dictData[@"OrgUnitLeaderLastName"];
    unit.leaderImageURL = (NSString *)dictData[@"OrgUnitLeaderImageURL"];
    unit.leaderPositionName = (NSString *)dictData[@"OrgUnitLeaderJPositionName"];
    unit.parentCode = (NSString *)dictData[@"ParentCode"];
    unit.workLocation = (NSString *)dictData[@"WorkLocation"];
    
    unit.haveLeader = (NSNumber *)dictData[@"HaveLeader"];
    unit.unitId = (NSNumber *)dictData[@"OrgUnitId"];
    unit.totalEmployees = (NSNumber *)dictData[@"TotalEmployees"];
    unit.parentId = (NSNumber*)dictData[@"ParentId"];
    unit.leaderId = (NSNumber *)dictData[@"OrgUnitLeaderId"];
    unit.isDeleted = (BOOL)dictData[@"IsDeleted"];
    
    return unit;
}
-(NSMutableDictionary *)parseJSON:(NSArray*)jArray
{
    NSMutableArray *orgUnits = [NSMutableArray array];
    
    for (id item in jArray) {
        if ([item isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *dict = (NSDictionary*)item;
            OrgUnit *unit = [self parseOrgUnit:dict];
            [orgUnits addObject:unit];
        }
    }

    return [self generateOrgTree:orgUnits];
}

-(NSMutableDictionary *)generateOrgTree:(NSArray*)orgList
{
    // <orgId, OrgTree>
    NSMutableDictionary *orgTreeDict = [NSMutableDictionary dictionary];
    
    for (OrgUnit *org in orgList)
    {
        OrgUnitTree *tree;
        if (orgTreeDict[org.unitId] == nil)
        {
            tree = [[OrgUnitTree alloc] init];
            tree.root = org;
            tree.children = [NSMutableArray array];
            
            [orgTreeDict setObject:tree forKey:org.unitId.stringValue];
        }
        else
        {
            tree = orgTreeDict[org.unitId.stringValue];
            tree.root = org;
        }
        
        @try {
            NSString *key = org.parentId.stringValue;
            if (orgTreeDict[key] == nil)
            {
                OrgUnitTree *parentTree = [[OrgUnitTree alloc] init];
                parentTree.children = [NSMutableArray arrayWithObject:tree];
                tree.parent = parentTree;
            }
            else
            {
                OrgUnitTree *parentTree = orgTreeDict[key];
                [parentTree.children addObject:tree];
                tree.parent = parentTree;
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Reason %@", exception.reason);
        }
    }
    
    NSDictionary *cloneDict = [NSDictionary dictionaryWithDictionary:orgTreeDict];
    for (id key in cloneDict.allKeys)
    {
        OrgUnitTree *tree = cloneDict[key];
        if (tree.parent != nil)
            [orgTreeDict removeObjectForKey:tree.root.unitId.stringValue];
    }
    
    return orgTreeDict;

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Tableview datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath
{
    for (int i = 0; i < level1Index.count; ++i) {
        NSNumber *num = level1Index[i];
        if (num.integerValue == indexPath.row)
            return 0;
        else if (i + 1 < level1Index.count && indexPath.row < ((NSNumber*)level1Index[i + 1]).integerValue)
            return 2;
    }
    
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int count = 0;
    for (int i = 0; i < allOrgUnitDict.count; ++i) {
        [level1Index addObject:[NSNumber numberWithInt:(count)]];
        ++count;
        OrgUnitTree *tree = allOrgUnitDict.allValues[i];
        count += tree.children.count;
    }
    
    return count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (allOrgUnitDict.count == 0)
        return nil;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];

    for (int i = 0; i < level1Index.count; ++i) {
        NSNumber *num = level1Index[i];
        if (num.integerValue == indexPath.row)
        {
            OrgUnitTree *tree = allOrgUnitDict.allValues[i];
            cell.textLabel.text = tree.root.name;
            return cell;
        }
        else if (i + 1 < level1Index.count && indexPath.row < ((NSNumber*)level1Index[i + 1]).integerValue)
        {
            int parentRow = ((NSNumber*)level1Index[i]).integerValue;
            int index = indexPath.row - parentRow - 1;
            OrgUnitTree *parentTree = allOrgUnitDict.allValues[i];
            OrgUnitTree *tree = parentTree.children[index];
            cell.textLabel.text = tree.root.name;
            
            cell.accessoryType = tree.children.count > 0 ? UITableViewCellAccessoryDisclosureIndicator : UITableViewCellAccessoryNone;
            return cell;
        }
    }
    
    int lastIndex = allOrgUnitDict.count - 1;
    int parentRow = ((NSNumber*)level1Index[lastIndex]).integerValue;
    int index = indexPath.row - parentRow - 1;
    OrgUnitTree *parentTree = allOrgUnitDict.allValues[lastIndex];
    OrgUnitTree *tree = parentTree.children[index];
    cell.textLabel.text = tree.root.name;
    
    cell.accessoryType = tree.children.count > 0 ? UITableViewCellAccessoryDisclosureIndicator : UITableViewCellAccessoryNone;

    return cell;
    
    
//    NSString *key = allOrgUnitDict.allKeys[indexPath.row];
//    OrgUnitTree *tree = allOrgUnitDict[key];
//    cell.textLabel.text = tree.root.name;
//    
//    cell.accessoryType = tree.children.count > 0 ? UITableViewCellAccessoryDisclosureIndicator : UITableViewCellAccessoryNone;
//    return cell;
}

#pragma mark - TableView delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int count = 0;
    for (int i = 0; i < allOrgUnitDict.count; ++i) {
        NSString *key = allOrgUnitDict.allKeys[i];
        OrgUnitTree *tree = allOrgUnitDict[key];
        ++count;
        for (int j = 0; j < tree.children.count; ++j) {
            if (count == indexPath.row)
            {
                OrgUnitTree *child = tree.children[j];
                if (child.children.count > 0)
                {
                    InnerOrgTreeViewController *controller = [[InnerOrgTreeViewController alloc] init];
                    controller.dataContext = child;
                    [self.navigationController pushViewController:controller animated:YES];
                }
                return;
                
            }
            ++count;
        }
    }
}

@end