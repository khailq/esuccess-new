//
//  SocialInsuaranceViewController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 6/21/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "SocialInsuranceViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
@interface SocialInsuranceViewController ()

@end

@implementation SocialInsuranceViewController
@synthesize socialInsArr;
static NSString *updating = @"Đang cập nhật";
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Bảo hiểm Xã hội";
    [self getData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma get Emp Performance Appraisal
- (void)getData
{
    socialInsArr = [[NSMutableArray alloc]init];
    [self getSocialInsuaranceById];
}

- (void)getSocialInsuaranceById
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Đang cập nhật";
    
    BasicHttpBinding_IEmpProfileLayerServiceBinding *binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
    EmpProfileLayerServiceSvc_GetViewEmpProfileSocialInsuranceById *request = [[EmpProfileLayerServiceSvc_GetViewEmpProfileSocialInsuranceById alloc]init];
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    if (app.otherEmpId != nil)
        request.employeeId = app.otherEmpId;
    else
        request.employeeId = app.sysUser.EmployeeId ;
    //request.employeeId = [NSNumber numberWithInt:2];
    [binding GetViewEmpProfileSocialInsuranceByIdAsyncUsingParameters:request delegate:self];
}

- (void)operation:(BasicHttpBinding_IEmpProfileLayerServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)response
{
    if ([response.bodyParts count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi kết nối" message:@"Không thể kết nối đến server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    for (id mine in response.bodyParts) {
        if ([mine isKindOfClass:[EmpProfileLayerServiceSvc_GetViewEmpProfileSocialInsuranceByIdResponse class]]) {
            empl_tns1_ArrayOfEmpProfileSocialInsurance *result = [mine GetViewEmpProfileSocialInsuranceByIdResult];
            socialInsArr = result.EmpProfileSocialInsurance;
            if ([socialInsArr count] > 0) {
                empl_tns1_EmpProfileSocialInsurance *socialIns = [socialInsArr objectAtIndex:0];
                NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                [formatter setDateFormat:@"dd/MM/yyyy"];
                
                lblSocialInsNumber.text = socialIns.SocialInsuranceNumber;
                lblSocialInsDateIssue.text = [formatter stringFromDate:socialIns.SocialInsuranceDateIssue];
                lblSocialInsMonthJoining.text = [NSString stringWithFormat:@"%d",[socialIns.SocialInsuranceJoiningBefore integerValue]];
                lblOtherDateEffect.text = [formatter stringFromDate:socialIns.OtherInsuranceDateEffect];
                lblOtherDateExpire.text = [formatter stringFromDate:socialIns.OtherInsuranceDateExpire];
                
                lblLabourBookNo.text = socialIns.LabourBookNo;
                lblLabourBookDateIssue.text = [formatter stringFromDate:socialIns.LabourBookDateOfIssue];
                lblLabourPlace.text = socialIns.LabourBookPlaceOfIssue;
            } else {
                lblSocialInsNumber.text = updating;
                lblSocialInsDateIssue.text = updating;
                lblSocialInsMonthJoining.text = updating;
                lblOtherDateEffect.text = updating;
                lblOtherDateExpire.text = updating;
                
                lblLabourBookNo.text = updating;
                lblLabourBookDateIssue.text = updating;
                lblLabourPlace.text = updating;
            }
            
            if (!self.viewDidDisappear)
                [self.tableView reloadData];
        }
        
        //        if ([mine isKindOfClass:[SOAPFault class]])
        //        {
        //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Server error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //            [alert show];
        //        }
    }
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

@end
