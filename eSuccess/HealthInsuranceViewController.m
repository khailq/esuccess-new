//
//  HealthInsuranceViewController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 6/21/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "HealthInsuranceViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "DayLeaveDetailCell.h"
@interface HealthInsuranceViewController ()

@end

@implementation HealthInsuranceViewController
@synthesize healthInsArr;
@synthesize healthInsNoArr;
static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Bảo hiểm Y Tế";
    [self getData];
    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 130;
//}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([healthInsNoArr count] > 0) {
        return [healthInsNoArr objectAtIndex:section];
    }
    return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [healthInsArr count] > 0 ? healthInsArr.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [healthInsArr count] > 0 ? 4 : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (healthInsArr.count == 0)
        return [self getDefaultEmptyCell];
    
    DayLeaveDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
    if (cell == nil) {
        cell = [[DayLeaveDetailCell alloc]init];
    }
    cell.lbLeft.font = [UIFont systemFontOfSize:14];
    cell.lbLeft.backgroundColor = [UIColor clearColor];
    cell.lbRight.font = [UIFont boldSystemFontOfSize:16];
    cell.lbRight.backgroundColor = [UIColor clearColor];
    
    int section = indexPath.section;
    empl_tns1_V_EmpProfileHealthInsurance *healthIns = [healthInsArr objectAtIndex:section];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    if (indexPath.row == 0) {
        cell.lbLeft.text = @"Từ ngày";
        cell.lbRight.text = [formatter stringFromDate:healthIns.HealthInsuranceDateEffect];
    }
    if (indexPath.row == 1) {
        cell.lbLeft.text = @"Đến ngày";
        cell.lbRight.text = [formatter stringFromDate:healthIns.HealthInsuranceDateExpire];
    }
    if (indexPath.row == 2) {
        cell.lbLeft.text = @"Tỉnh/ Thành phố";
        cell.lbRight.text = healthIns.CLogCityName;
    }
    if (indexPath.row == 3) {
        cell.lbLeft.text = @"Bệnh Viện";
        cell.lbRight.text = healthIns.CLogHospitalName;
    }
    return cell;
}

#pragma get Emp Health Insurance
- (void)getData
{
    healthInsArr = [[NSMutableArray alloc]init];
    healthInsNoArr = [[NSMutableArray alloc]init];
    
    [self getEmpHealthInsuranceById];
}

- (void)getEmpHealthInsuranceById
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Đang cập nhật";
    
    BasicHttpBinding_IEmpProfileLayerServiceBinding *binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
    EmpProfileLayerServiceSvc_GetViewEmpProfileHealthInsuranceById *request = [[EmpProfileLayerServiceSvc_GetViewEmpProfileHealthInsuranceById alloc]init];
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    if (app.otherEmpId != nil)
        request.employeeId = app.otherEmpId;
    else
        request.employeeId = app.sysUser.EmployeeId ;
    request.employeeId = [NSNumber numberWithInt:2];
    [binding GetViewEmpProfileHealthInsuranceByIdAsyncUsingParameters:request delegate:self];
}

- (void)operation:(BasicHttpBinding_IEmpProfileLayerServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)response
{
    if ([response.bodyParts count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi kết nối" message:@"Không thể kết nối đến server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    for (id mine in response.bodyParts) {
        if ([mine isKindOfClass:[EmpProfileLayerServiceSvc_GetViewEmpProfileHealthInsuranceByIdResponse class]]) {
            empl_tns1_ArrayOfV_EmpProfileHealthInsurance *result = [mine GetViewEmpProfileHealthInsuranceByIdResult];
            healthInsArr = result.V_EmpProfileHealthInsurance;
            for (empl_tns1_V_EmpProfileHealthInsurance *healthIns in healthInsArr) {
                if (healthIns.HealthInsuranceNumber == nil) {
                    [healthInsNoArr addObject:@""];
                } else
                    [healthInsNoArr addObject:healthIns.HealthInsuranceNumber];
            }
            
            if (!self.viewDidDisappear)
                [self.tableView reloadData];
        }
        
        //        if ([mine isKindOfClass:[SOAPFault class]])
        //        {
        //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Server error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //            [alert show];
        //        }
    }
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}


@end
