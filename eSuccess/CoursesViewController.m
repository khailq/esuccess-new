//
//  CoursesViewController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 6/7/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "CoursesViewController.h"
#import "Course.h"
#import "CourseDetailCell.h"

#define RequiredCourseHeader @"Khoá học bắt buộc"
#define NotRequiredCourseHeader @"Khoá học không bắt buộc"

@implementation CoursesViewController
{
    NSMutableArray *requiredCourses;
    NSMutableArray *notRequiredCourses;
}

static NSString *courseCellIdentifier = @"CourseDetailCell";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //[self.tableView registerClass:[CourseDetailCell class] forCellReuseIdentifier:courseCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setCourses:(NSArray *)courses
{
    if (_courses != courses)
    {
        requiredCourses = [NSMutableArray array];
        notRequiredCourses = [NSMutableArray array];
        _courses = courses;
        for (Course *course in _courses)
        {
            if (course.isRequired.boolValue)
            {
                [requiredCourses addObject:course];
            }
            else
                [notRequiredCourses addObject:course];
        }
    }
}

#pragma mark - tableview datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (requiredCourses.count == 0 || notRequiredCourses.count == 0)
        return 1;
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (requiredCourses.count > 0 && notRequiredCourses.count == 0)
        return requiredCourses.count;
    if (notRequiredCourses.count > 0 && requiredCourses.count == 0)
        return notRequiredCourses.count;
    
    if (section == 0)
        return requiredCourses.count;
    
    return notRequiredCourses.count;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (requiredCourses.count > 0 && notRequiredCourses.count == 0)
        return RequiredCourseHeader;
    if (notRequiredCourses.count > 0 && requiredCourses.count == 0)
        return NotRequiredCourseHeader;
    
    if (section == 0)
        return RequiredCourseHeader;
    
    return NotRequiredCourseHeader;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CourseDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:courseCellIdentifier forIndexPath:indexPath];
    if (cell == nil)
        cell = [[CourseDetailCell alloc] init];
    NSArray *array;
    
    if (requiredCourses.count > 0 && notRequiredCourses.count == 0)
        array = requiredCourses;
    else if (notRequiredCourses.count > 0 && requiredCourses.count == 0)
        array = notRequiredCourses;
    else if (indexPath.section == 0)
        array = requiredCourses;
    else
        array = notRequiredCourses;
    
    Course *course = array[indexPath.row];
    
    UIView *bgView = [[UIView alloc] initWithFrame:cell.frame];
    indexPath.row % 2 == 0 ? [bgView setBackgroundColor:[UIColor colorWithRed:209.0/255.0 green:209.0/255.0 blue:209.0/255.0 alpha:1.0]] : [bgView setBackgroundColor:[UIColor colorWithRed:196.0/255.0 green:196.0/255.0 blue:196.0/255.0 alpha:1.0]];
    
    cell.backgroundView = bgView;
    cell.course = course;
    
    return cell;
}

@end
