//
//  BaseMasterController.h
//  eSuccess
//
//  Created by Scott on 5/2/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootMasterController.h"
#import "AppDelegate.h"

#define NormalCellIdentifier @"NormalCell"

@interface BaseMasterController : UITableViewController

@property (strong, nonatomic) NSString *name;

@end
