//
//  AppDelegate.m
//  eSuccess
//
//  Created by HPTVIETNAM on 4/19/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "CustomSplitViewController.h"
#import "MasterViewController.h"

@interface AppDelegate ()
{
    NSIndexPath *savedIndexPath;  // Temp value to save when in view profile mode
}
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    CustomSplitViewController *splitViewController = (CustomSplitViewController *)self.window.rootViewController;
    UIStoryboard *storyboard = splitViewController.storyboard;
    RootMasterController *root = [storyboard instantiateViewControllerWithIdentifier:@"rootMaster"];
    splitViewController.masterViewController = root;
    
    UINavigationController *nav = [storyboard instantiateViewControllerWithIdentifier:@"baseNavigation"];
    splitViewController.detailViewController = nav;
    
    
    UINavigationController *navigationController = [splitViewController.viewControllers lastObject];
    
    LoginViewController *login = (LoginViewController*)[navigationController topViewController];
    
    self.rootMaster = root;
    
    splitViewController.delegate = (id)login;
    
    self.masterControllerDictionary = [NSMutableDictionary dictionary];
    
    self.selectedIndexDictionary = [NSMutableDictionary dictionary];
    return YES;

}

-(void)setOtherEmpId:(NSNumber *)otherEmpId
{
    if (_otherEmpId == nil || ![otherEmpId isEqualToNumber:_otherEmpId])
    {
        _otherEmpId = otherEmpId;
        [[NSNotificationCenter defaultCenter] postNotificationName:BeginViewProfileNotification object:nil];
    }
}

-(void)toMainPage
{
    CustomSplitViewController *splitVC = (CustomSplitViewController*)self.window.rootViewController;
    [splitVC toMainPage];
}
-(void)setOtherEmployeeInfo:(NSNumber*)empId name:(NSString*)name;
{
    if (empId == nil && name == nil)
    {
        _otherEmpName = nil;
        _otherEmpId = nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:EndViewEmployeeModeNotification object:self];
    }
    else
    {
        _otherEmpId = empId;
        _otherEmpName = name;
        [[NSNotificationCenter defaultCenter] postNotificationName:ViewEmployeeModeNotification object:self];
    }
}
				
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
