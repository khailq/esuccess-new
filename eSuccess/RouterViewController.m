//
//  RouterViewController.m
//  eSuccess
//
//  Created by Scott on 4/22/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "RouterViewController.h"
#import "AppDelegate.h"

@interface RouterViewController ()

@end

@implementation RouterViewController

@synthesize masterPopoverController = _masterPopoverController;
@synthesize leftBarButtonItem = _leftBarButtonItem;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    if ([appDelegate.master.tableView indexPathForSelectedRow] == nil)
    {
        //[self performSegueWithIdentifier:@"LoginToMainSegue" sender:self];
        [appDelegate.master selectFirstRow];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    id destination = segue.destinationViewController;
    
    UISplitViewController *splitViewController = (UISplitViewController*) [[[UIApplication sharedApplication] delegate] window].rootViewController;
    
    splitViewController.delegate = destination;
    
    
    
    if ([destination respondsToSelector:@selector(setLeftBarButtonItem:)] && [destination respondsToSelector:@selector(setMasterPopoverController:)])
    {
        [destination setValue:self.masterPopoverController forKey:@"masterPopoverController"];
        [destination setValue:self.leftBarButtonItem forKey:@"leftBarButtonItem"];
    }
}

#pragma mark - Split view

-(void)splitViewController:(UISplitViewController *)svc willShowViewController:(UIViewController *)aViewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}

-(void)splitViewController:(UISplitViewController *)svc willHideViewController:(UIViewController *)aViewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)pc
{
    barButtonItem.title = @"Điều khiển";
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = pc;
}
@end
