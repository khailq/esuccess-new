//
//  SkillProfileViewController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 6/26/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "SkillProfileViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "DayLeaveDetailCell.h"
@interface SkillProfileViewController ()

@end

@implementation SkillProfileViewController
@synthesize skillArr;
@synthesize skillNameArr;
static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Kỹ năng tay nghề";
    [self getData];
    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 130;
//}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([skillNameArr count] > 0) {
        return [skillNameArr objectAtIndex:section];
    }
    return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [skillArr count] > 0 ? skillArr.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [skillArr count] > 0 ? 4 : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (skillArr.count == 0)
        return [self getDefaultEmptyCell];
    
    DayLeaveDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
    if (cell == nil) {
        cell = [[DayLeaveDetailCell alloc]init];
    }
    cell.lbLeft.font = [UIFont systemFontOfSize:14];
    cell.lbLeft.backgroundColor = [UIColor clearColor];
    cell.lbRight.font = [UIFont boldSystemFontOfSize:16];
    cell.lbRight.backgroundColor = [UIColor clearColor];
    
    int section = indexPath.section;
    empl_tns1_V_EmpProfileSkill *skill = [skillArr objectAtIndex:section];
    if (indexPath.row == 0) {
        cell.lbLeft.text = @"Loại kỹ năng";
        cell.lbRight.text = skill.OrgSkillTypeName;
    }
    if (indexPath.row == 1) {
        cell.lbLeft.text = @"Đánh giá";
        cell.lbRight.text = skill.RatingDescription;
    }
    if (indexPath.row == 2) {
        cell.lbLeft.text = @"Mô tả";
        cell.lbRight.text = skill.OrgSkillDescription;
    }
    if (indexPath.row == 3) {
        cell.lbLeft.text = @"Tham khảo";
        cell.lbRight.text = skill.DocumentRef;
    }
    return cell;
}

#pragma get Data Skill
-(void)getData
{
    skillArr = [[NSMutableArray alloc]init];
    skillNameArr = [[NSMutableArray alloc]init];
    
    [self getEmpProfileSkillById];
}
-(void)getEmpProfileSkillById
{
    BasicHttpBinding_IEmpProfileLayerServiceBinding * binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
    EmpProfileLayerServiceSvc_GetViewEmpProfileSkillById *request = [[EmpProfileLayerServiceSvc_GetViewEmpProfileSkillById alloc] init];
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    if (app.otherEmpId != nil) {
        request.EmployeeId = app.otherEmpId;
    } else{
        request.employeeId = app.sysUser.EmployeeId ;
    }
    [binding GetViewEmpProfileSkillByIdAsyncUsingParameters:request delegate:self];
}


- (void)operation:(BasicHttpBinding_IEmpProfileLayerServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)response
{
    if ([response.bodyParts count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi kết nối" message:@"Không thể kết nối đến server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    for (id mine in response.bodyParts) {
        if ([mine isKindOfClass:[EmpProfileLayerServiceSvc_GetViewEmpProfileSkillByIdResponse class]]) {
            empl_tns1_ArrayOfV_EmpProfileSkill *result = [mine GetViewEmpProfileSkillByIdResult];
            skillArr = result.V_EmpProfileSkill;
            for (empl_tns1_V_EmpProfileSkill *skill in skillArr) {
                [skillNameArr addObject:skill.OrgSkillName];
            }
            
            if (!self.viewDidDisappear)
                [self.tableView reloadData];
        }
        
        //        if ([mine isKindOfClass:[SOAPFault class]])
        //        {
        //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Server error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //            [alert show];
        //        }
    }
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
}
@end
