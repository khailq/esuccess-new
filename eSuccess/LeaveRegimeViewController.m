//
//  LeaveRegimeViewController.m
//  eSuccess
//
//  Created by admin on 5/28/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "LeaveRegimeViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "DayLeaveDetailCell.h"
@interface LeaveRegimeViewController ()

@end

@implementation LeaveRegimeViewController
@synthesize leaveRegimeArr;
static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Chế độ nghỉ, phép";
    [self getData];
    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [leaveRegimeArr count] > 0 ? leaveRegimeArr.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [leaveRegimeArr count]> 0 ? 5: 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (leaveRegimeArr.count == 0) {
        return [self getDefaultEmptyCell];
    }
    
    DayLeaveDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
    if (cell == nil) {
        cell = [[DayLeaveDetailCell alloc]init];
    }
    cell.lbLeft.font = [UIFont systemFontOfSize:14];
    cell.lbLeft.backgroundColor = [UIColor clearColor];
    cell.lbRight.font = [UIFont boldSystemFontOfSize:16];
    cell.lbRight.backgroundColor = [UIColor clearColor];
    
    int section = indexPath.section;
    empl_tns1_EmpProfileLeaveRegime *leaveRegime = [leaveRegimeArr objectAtIndex:section];
    if (indexPath.row == 0) {
        cell.lbLeft.text = @"Phép năm trước";
        cell.lbRight.text = [NSString stringWithFormat:@"%d ngày", [leaveRegime.PreviousLeave integerValue]];
    }
    if (indexPath.row == 1) {
        cell.lbLeft.text = @"Phép tháng";
        cell.lbRight.text = [NSString stringWithFormat:@"%d ngày", [leaveRegime.MonthlyLeave integerValue]];
    }
    if (indexPath.row == 2) {
        cell.lbLeft.text = @"Phép thâm niên";
        cell.lbRight.text = [NSString stringWithFormat:@"%d ngày", [leaveRegime.SeniorityLeave integerValue]];
    }
    
    if (indexPath.row == 3) {
        cell.lbLeft.text = @"Đã sử dụng";
        cell.lbRight.text = [NSString stringWithFormat:@"%d ngày", [leaveRegime.RemainedLeave integerValue]];
    }
    if (indexPath.row == 4) {
        cell.lbLeft.text = @"Tổng phép còn lại";
        cell.lbRight.text = [NSString stringWithFormat:@"%d ngày", [leaveRegime.TotalUsedLeave integerValue]];
    }
    return cell;
}

#pragma get Data Leave Regime
- (void)getData
{
    leaveRegimeArr = [[NSMutableArray alloc]init];
    
    [self getLeaveRegimeById];
}

- (void)getLeaveRegimeById
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Đang cập nhật";
    
    BasicHttpBinding_IEmpProfileLayerServiceBinding *binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
    EmpProfileLayerServiceSvc_GetViewEmpProfileLeaveRegime *request = [[EmpProfileLayerServiceSvc_GetViewEmpProfileLeaveRegime alloc]init];
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    if (app.otherEmpId != nil)
        request.employeeId = app.otherEmpId;
    else
        request.employeeId = app.sysUser.EmployeeId ;
    //request.employeeId = [NSNumber numberWithInt:2];
    [binding GetViewEmpProfileLeaveRegimeAsyncUsingParameters:request delegate:self];
}

- (void)operation:(BasicHttpBinding_IEmpProfileLayerServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)response
{
    if ([response.bodyParts count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi kết nối" message:@"Không thể kết nối đến server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    for (id mine in response.bodyParts) {
        if ([mine isKindOfClass:[EmpProfileLayerServiceSvc_GetViewEmpProfileLeaveRegimeResponse class]]) {
            empl_tns1_ArrayOfEmpProfileLeaveRegime *result = [mine GetViewEmpProfileLeaveRegimeResult];
            leaveRegimeArr = result.EmpProfileLeaveRegime;
            
            if (!self.viewDidDisappear)
                [self.tableView reloadData];
        }
        
        //        if ([mine isKindOfClass:[SOAPFault class]])
        //        {
        //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Server error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //            [alert show];
        //        }
    }
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

@end
