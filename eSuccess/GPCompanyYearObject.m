//
//  GPCompanyYearObject.m
//  eSuccess
//
//  Created by admin on 6/6/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "GPCompanyYearObject.h"

@implementation GPCompanyYearObject
+(GPCompanyYearObject *)companyYearWithID:(NSInteger )ID statement:(NSString *)statement smartTimeGoal:(NSString *)smartTimeGoal scoreCardArr:(NSMutableArray *)scoreCardArr
{
    GPCompanyYearObject *coYear = [[GPCompanyYearObject alloc]init];
    coYear.GPCompanyId = ID;
    coYear.statement = statement;
    coYear.smartTimeGoal = smartTimeGoal;
    coYear.gpScoreCardArr = scoreCardArr;
    
    return coYear;
}
@end
