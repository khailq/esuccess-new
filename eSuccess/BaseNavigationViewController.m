//
//  BaseNavigationViewController.m
//  eSuccess
//
//  Created by Scott on 4/30/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#define ToolbarHeight 76
#import "CustomBarButtonItem.h"
#import "BaseNavigationViewController.h"
#import "AppDelegate.h"
#import "CustomSplitViewController.h"

@interface BaseNavigationViewController ()
@property (strong, nonatomic) CustomBarButtonItem *selectedItem;
@end

@implementation BaseNavigationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
