#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class emp_tns2_ArrayOfEmpContractSummaryModel;
@class emp_tns2_EmpContractSummaryModel;
@interface emp_tns2_EmpContractSummaryModel : NSObject {
	
/* elements */
	NSString * CLogContractTypeName;
	NSString * ContractCode;
	NSString * ContractTypeCode;
	NSNumber * EmpContractId;
	NSString * EmployeeRepresentativeCode;
	NSString * EmployeeRepresentativeFullName;
	NSNumber * EmployeeRepresentativeId;
	NSString * Summary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns2_EmpContractSummaryModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogContractTypeName;
@property (retain) NSString * ContractCode;
@property (retain) NSString * ContractTypeCode;
@property (retain) NSNumber * EmpContractId;
@property (retain) NSString * EmployeeRepresentativeCode;
@property (retain) NSString * EmployeeRepresentativeFullName;
@property (retain) NSNumber * EmployeeRepresentativeId;
@property (retain) NSString * Summary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns2_ArrayOfEmpContractSummaryModel : NSObject {
	
/* elements */
	NSMutableArray *EmpContractSummaryModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns2_ArrayOfEmpContractSummaryModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpContractSummaryModel:(emp_tns2_EmpContractSummaryModel *)toAdd;
@property (readonly) NSMutableArray * EmpContractSummaryModel;
/* attributes */
- (NSDictionary *)attributes;
@end
