#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class emp_tns4_ArrayOfNotificationModel;
@class emp_tns4_NotificationModel;
@class emp_tns4_ArrayOfLeaveNotificationModel;
@class emp_tns4_LeaveNotificationModel;
@class emp_tns4_ArrayOfEmpFamilyDependentModel;
@class emp_tns4_EmpFamilyDependentModel;
@class emp_tns4_ESS_EMP_IN_GENDER;
@class emp_tns4_SearchingConditionsModel;
@class emp_tns4_ArrayOfEmpGroupModel;
@class emp_tns4_EmpGroupModel;
@class emp_tns4_MsgNotifyModel;
@interface emp_tns4_NotificationModel : NSObject {
	
/* elements */
	NSDate * AppoinmenExpirationDate;
	NSDate * AppoinmenImplementationDate;
	NSDate * DegreeDateExpire;
	NSDate * DegreeDateIssue;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EntryDate;
	NSString * FullName;
	USBoolean * Gender;
	NSString * ImgUrl;
	NSDate * OfficialDate;
	NSString * OrgDegreeName;
	NSString * OrgDegreePlaceIssue;
	NSString * OrgJobPositionName;
	NSNumber * OrgUnitId;
	NSString * OrgUnitName;
	NSDate * PassportDateOfExpire;
	NSDate * PassportDateOfIssue;
	NSString * PassportNo;
	NSString * PassportPlaceOfIssue;
	NSDate * ProbationaryDate;
	NSNumber * ProbationarySalary;
	NSDate * TrainingEndDate;
	NSString * TrainingProgram;
	NSDate * TrainingStartDate;
	NSString * WorkLevelName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns4_NotificationModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AppoinmenExpirationDate;
@property (retain) NSDate * AppoinmenImplementationDate;
@property (retain) NSDate * DegreeDateExpire;
@property (retain) NSDate * DegreeDateIssue;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * ImgUrl;
@property (retain) NSDate * OfficialDate;
@property (retain) NSString * OrgDegreeName;
@property (retain) NSString * OrgDegreePlaceIssue;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitName;
@property (retain) NSDate * PassportDateOfExpire;
@property (retain) NSDate * PassportDateOfIssue;
@property (retain) NSString * PassportNo;
@property (retain) NSString * PassportPlaceOfIssue;
@property (retain) NSDate * ProbationaryDate;
@property (retain) NSNumber * ProbationarySalary;
@property (retain) NSDate * TrainingEndDate;
@property (retain) NSString * TrainingProgram;
@property (retain) NSDate * TrainingStartDate;
@property (retain) NSString * WorkLevelName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns4_ArrayOfNotificationModel : NSObject {
	
/* elements */
	NSMutableArray *NotificationModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns4_ArrayOfNotificationModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addNotificationModel:(emp_tns4_NotificationModel *)toAdd;
@property (readonly) NSMutableArray * NotificationModel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns4_LeaveNotificationModel : NSObject {
	
/* elements */
	NSString * Address;
	NSString * ApprovalCode;
	NSDate * ApprovalDate;
	NSString * ApprovalFullName;
	NSNumber * ApproverId;
	NSDate * BirthDay;
	NSDate * ChildbirthDate;
	NSString * ContactPhone;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EntryDate;
	NSDate * FromDate;
	NSString * FullName;
	USBoolean * Gender;
	NSString * ImgUrl;
	NSString * OrgJobName;
	NSString * OrgJobPositionName;
	NSNumber * OrgUnitId;
	NSString * OrgUnitName;
	NSString * Reason;
	NSDate * RetireDate;
	NSNumber * Status;
	NSString * SysParamNameEN;
	NSString * SysParamNameVN;
	NSString * TimeRequestMasTerCode;
	NSNumber * TimeRequestMasTerId;
	NSDate * ToDate;
	NSString * Type;
	NSDate * WorkAfterChildBirthDate;
	NSDate * WorkAfterLeave;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns4_LeaveNotificationModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSString * ApprovalCode;
@property (retain) NSDate * ApprovalDate;
@property (retain) NSString * ApprovalFullName;
@property (retain) NSNumber * ApproverId;
@property (retain) NSDate * BirthDay;
@property (retain) NSDate * ChildbirthDate;
@property (retain) NSString * ContactPhone;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EntryDate;
@property (retain) NSDate * FromDate;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * ImgUrl;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitName;
@property (retain) NSString * Reason;
@property (retain) NSDate * RetireDate;
@property (retain) NSNumber * Status;
@property (retain) NSString * SysParamNameEN;
@property (retain) NSString * SysParamNameVN;
@property (retain) NSString * TimeRequestMasTerCode;
@property (retain) NSNumber * TimeRequestMasTerId;
@property (retain) NSDate * ToDate;
@property (retain) NSString * Type;
@property (retain) NSDate * WorkAfterChildBirthDate;
@property (retain) NSDate * WorkAfterLeave;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns4_ArrayOfLeaveNotificationModel : NSObject {
	
/* elements */
	NSMutableArray *LeaveNotificationModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns4_ArrayOfLeaveNotificationModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLeaveNotificationModel:(emp_tns4_LeaveNotificationModel *)toAdd;
@property (readonly) NSMutableArray * LeaveNotificationModel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns4_EmpFamilyDependentModel : NSObject {
	
/* elements */
	NSNumber * CBPITDependentId;
	NSNumber * CLogFamilyRelationshipId;
	NSDate * DependentBeginDate;
	NSString * DependentDemonstrative;
	NSDate * DependentEndDate;
	NSNumber * EmpProfileFamilyRelationshipId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * FamilyRelationshipName;
	NSString * FullName;
	NSString * ImgUrl;
	NSString * OrgJobName;
	NSString * OrgJobPositionName;
	NSNumber * OrgUnitId;
	NSString * OrgUnitName;
	NSString * PersonFullName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns4_EmpFamilyDependentModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBPITDependentId;
@property (retain) NSNumber * CLogFamilyRelationshipId;
@property (retain) NSDate * DependentBeginDate;
@property (retain) NSString * DependentDemonstrative;
@property (retain) NSDate * DependentEndDate;
@property (retain) NSNumber * EmpProfileFamilyRelationshipId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * FamilyRelationshipName;
@property (retain) NSString * FullName;
@property (retain) NSString * ImgUrl;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitName;
@property (retain) NSString * PersonFullName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns4_ArrayOfEmpFamilyDependentModel : NSObject {
	
/* elements */
	NSMutableArray *EmpFamilyDependentModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns4_ArrayOfEmpFamilyDependentModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpFamilyDependentModel:(emp_tns4_EmpFamilyDependentModel *)toAdd;
@property (readonly) NSMutableArray * EmpFamilyDependentModel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns4_ESS_EMP_IN_GENDER : NSObject {
	
/* elements */
	NSNumber * FEMALE_AGE;
	NSNumber * MALE_AGE;
	NSNumber * NUM_FEMALE;
	NSNumber * NUM_MALE;
	NSNumber * PERCENT_FEMALE;
	NSNumber * PERCENT_MALE;
	NSNumber * TOTAL;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns4_ESS_EMP_IN_GENDER *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * FEMALE_AGE;
@property (retain) NSNumber * MALE_AGE;
@property (retain) NSNumber * NUM_FEMALE;
@property (retain) NSNumber * NUM_MALE;
@property (retain) NSNumber * PERCENT_FEMALE;
@property (retain) NSNumber * PERCENT_MALE;
@property (retain) NSNumber * TOTAL;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns4_SearchingConditionsModel : NSObject {
	
/* elements */
	NSString * dateOffFrom;
	NSString * dateOffTo;
	NSNumber * intEmpId;
	NSNumber * intOrgGroupId;
	NSNumber * intOrgJobPosId;
	NSNumber * intOrgUnitId;
	NSNumber * status;
	NSString * strEmpCd;
	NSString * strEmpNm;
	NSString * strEntryDate;
	NSString * strFrom;
	NSString * strOfficealDate;
	NSString * strOrgGroupCd;
	NSString * strOrgGroupNm;
	NSString * strOrgJobPosCd;
	NSString * strOrgJobPosNm;
	NSString * strOrgUnitCd;
	NSString * strOrgUnitNm;
	NSString * strTo;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns4_SearchingConditionsModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * dateOffFrom;
@property (retain) NSString * dateOffTo;
@property (retain) NSNumber * intEmpId;
@property (retain) NSNumber * intOrgGroupId;
@property (retain) NSNumber * intOrgJobPosId;
@property (retain) NSNumber * intOrgUnitId;
@property (retain) NSNumber * status;
@property (retain) NSString * strEmpCd;
@property (retain) NSString * strEmpNm;
@property (retain) NSString * strEntryDate;
@property (retain) NSString * strFrom;
@property (retain) NSString * strOfficealDate;
@property (retain) NSString * strOrgGroupCd;
@property (retain) NSString * strOrgGroupNm;
@property (retain) NSString * strOrgJobPosCd;
@property (retain) NSString * strOrgJobPosNm;
@property (retain) NSString * strOrgUnitCd;
@property (retain) NSString * strOrgUnitNm;
@property (retain) NSString * strTo;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns4_EmpGroupModel : NSObject {
	
/* elements */
	NSNumber * intEmpId;
	NSNumber * intOrgGroupId;
	NSNumber * intOrgJobPosId;
	NSNumber * intOrgUnitId;
	NSString * strEmpCd;
	NSString * strEmpNm;
	NSString * strOrgGroupCd;
	NSString * strOrgGroupNm;
	NSString * strOrgJobPosCd;
	NSString * strOrgJobPosNm;
	NSString * strOrgUnitCd;
	NSString * strOrgUnitNm;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns4_EmpGroupModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * intEmpId;
@property (retain) NSNumber * intOrgGroupId;
@property (retain) NSNumber * intOrgJobPosId;
@property (retain) NSNumber * intOrgUnitId;
@property (retain) NSString * strEmpCd;
@property (retain) NSString * strEmpNm;
@property (retain) NSString * strOrgGroupCd;
@property (retain) NSString * strOrgGroupNm;
@property (retain) NSString * strOrgJobPosCd;
@property (retain) NSString * strOrgJobPosNm;
@property (retain) NSString * strOrgUnitCd;
@property (retain) NSString * strOrgUnitNm;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns4_ArrayOfEmpGroupModel : NSObject {
	
/* elements */
	NSMutableArray *EmpGroupModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns4_ArrayOfEmpGroupModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpGroupModel:(emp_tns4_EmpGroupModel *)toAdd;
@property (readonly) NSMutableArray * EmpGroupModel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns4_MsgNotifyModel : NSObject {
	
/* elements */
	NSNumber * Id_;
	USBoolean * IsSuccess;
	USBoolean * IsValid;
	NSString * NotifyMsg;
	NSString * NotifyMsgEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns4_MsgNotifyModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Id_;
@property (retain) USBoolean * IsSuccess;
@property (retain) USBoolean * IsValid;
@property (retain) NSString * NotifyMsg;
@property (retain) NSString * NotifyMsgEN;
/* attributes */
- (NSDictionary *)attributes;
@end
