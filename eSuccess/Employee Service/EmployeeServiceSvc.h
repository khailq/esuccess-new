#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class EmployeeServiceSvc_FTS_EmpProfileOrgUnit;
@class EmployeeServiceSvc_FTS_EmpProfileOrgUnitResponse;
@class EmployeeServiceSvc_FTS_EmpContract;
@class EmployeeServiceSvc_FTS_EmpContractResponse;
@class EmployeeServiceSvc_FTS_EmpOff;
@class EmployeeServiceSvc_FTS_EmpOffResponse;
@class EmployeeServiceSvc_GetAllEmployeeByAdvancedSearch;
@class EmployeeServiceSvc_GetAllEmployeeByAdvancedSearchResponse;
@class EmployeeServiceSvc_GetAllEmployeeProfileByAdvancedSearch;
@class EmployeeServiceSvc_GetAllEmployeeProfileByAdvancedSearchResponse;
@class EmployeeServiceSvc_GetAllEmployeeContractByAdvancedSearch;
@class EmployeeServiceSvc_GetAllEmployeeContractByAdvancedSearchResponse;
@class EmployeeServiceSvc_GetAllEmployeeOffByAdvancedSearch;
@class EmployeeServiceSvc_GetAllEmployeeOffByAdvancedSearchResponse;
@class EmployeeServiceSvc_GetAllEmployer;
@class EmployeeServiceSvc_GetAllEmployerResponse;
@class EmployeeServiceSvc_GetAllCompany;
@class EmployeeServiceSvc_GetAllCompanyResponse;
@class EmployeeServiceSvc_GetCompanyInfo;
@class EmployeeServiceSvc_GetCompanyInfoResponse;
@class EmployeeServiceSvc_GetAllEmpContractSummary;
@class EmployeeServiceSvc_GetAllEmpContractSummaryResponse;
@class EmployeeServiceSvc_GetAllEmpContract;
@class EmployeeServiceSvc_GetAllEmpContractResponse;
@class EmployeeServiceSvc_GetAllEmpContractBySkipTake;
@class EmployeeServiceSvc_GetAllEmpContractBySkipTakeResponse;
@class EmployeeServiceSvc_GetEmpContract;
@class EmployeeServiceSvc_GetEmpContractResponse;
@class EmployeeServiceSvc_GetEmpContractByContractId;
@class EmployeeServiceSvc_GetEmpContractByContractIdResponse;
@class EmployeeServiceSvc_CountEmpContract;
@class EmployeeServiceSvc_CountEmpContractResponse;
@class EmployeeServiceSvc_GetEmpBasicProfileByEmployeId;
@class EmployeeServiceSvc_GetEmpBasicProfileByEmployeIdResponse;
@class EmployeeServiceSvc_GetWokingTimeByEmployeeId;
@class EmployeeServiceSvc_GetWokingTimeByEmployeeIdResponse;
@class EmployeeServiceSvc_GetContractType;
@class EmployeeServiceSvc_GetContractTypeResponse;
@class EmployeeServiceSvc_CheckExistEmpContractCode;
@class EmployeeServiceSvc_CheckExistEmpContractCodeResponse;
@class EmployeeServiceSvc_GetAllEmpContractTerm;
@class EmployeeServiceSvc_GetAllEmpContractTermResponse;
@class EmployeeServiceSvc_GetEmpContractTermUndefined;
@class EmployeeServiceSvc_GetEmpContractTermUndefinedResponse;
@class EmployeeServiceSvc_GetEmpContractTermIdentified;
@class EmployeeServiceSvc_GetEmpContractTermIdentifiedResponse;
@class EmployeeServiceSvc_CreateEmpContract;
@class EmployeeServiceSvc_CreateEmpContractResponse;
@class EmployeeServiceSvc_UpdateEmpContract;
@class EmployeeServiceSvc_UpdateEmpContractResponse;
@class EmployeeServiceSvc_EndEmpContracts;
@class EmployeeServiceSvc_EndEmpContractsResponse;
@class EmployeeServiceSvc_EndContracts;
@class EmployeeServiceSvc_EndContractsResponse;
@class EmployeeServiceSvc_DeleteEmpContracts;
@class EmployeeServiceSvc_DeleteEmpContractsResponse;
@class EmployeeServiceSvc_AppraiseContract;
@class EmployeeServiceSvc_AppraiseContractResponse;
@class EmployeeServiceSvc_GetAllEmpNotificationConstant;
@class EmployeeServiceSvc_GetAllEmpNotificationConstantResponse;
@class EmployeeServiceSvc_GetJsonListNotifyEmpBirthday;
@class EmployeeServiceSvc_GetJsonListNotifyEmpBirthdayResponse;
@class EmployeeServiceSvc_GetValueNotifyEmpBirthday;
@class EmployeeServiceSvc_GetValueNotifyEmpBirthdayResponse;
@class EmployeeServiceSvc_GetJsonListEmpBirthday;
@class EmployeeServiceSvc_GetJsonListEmpBirthdayResponse;
@class EmployeeServiceSvc_GetJsonListNotifyEmpAppoinmentExpiration;
@class EmployeeServiceSvc_GetJsonListNotifyEmpAppoinmentExpirationResponse;
@class EmployeeServiceSvc_GetValueNotifyEmpAppoinmentExpiration;
@class EmployeeServiceSvc_GetValueNotifyEmpAppoinmentExpirationResponse;
@class EmployeeServiceSvc_GetJsonListEmpAppoinmentExpiration;
@class EmployeeServiceSvc_GetJsonListEmpAppoinmentExpirationResponse;
@class EmployeeServiceSvc_GetValueNotifyEmpIncreaseSalary;
@class EmployeeServiceSvc_GetValueNotifyEmpIncreaseSalaryResponse;
@class EmployeeServiceSvc_GetJsonListNotifyEmpToOffical;
@class EmployeeServiceSvc_GetJsonListNotifyEmpToOfficalResponse;
@class EmployeeServiceSvc_GetJsonListEmpToOffical;
@class EmployeeServiceSvc_GetJsonListEmpToOfficalResponse;
@class EmployeeServiceSvc_GetValueNotifyEmpToOffical;
@class EmployeeServiceSvc_GetValueNotifyEmpToOfficalResponse;
@class EmployeeServiceSvc_GetJsonListNotifyEmpExpirationPassport;
@class EmployeeServiceSvc_GetJsonListNotifyEmpExpirationPassportResponse;
@class EmployeeServiceSvc_GetJsonListEmpExpirationPassport;
@class EmployeeServiceSvc_GetJsonListEmpExpirationPassportResponse;
@class EmployeeServiceSvc_GetValueNotifyEmpExpirationPassport;
@class EmployeeServiceSvc_GetValueNotifyEmpExpirationPassportResponse;
@class EmployeeServiceSvc_GetJsonListNotifyEmpParticipateTraining;
@class EmployeeServiceSvc_GetJsonListNotifyEmpParticipateTrainingResponse;
@class EmployeeServiceSvc_GetJsonListEmpParticipateTraining;
@class EmployeeServiceSvc_GetJsonListEmpParticipateTrainingResponse;
@class EmployeeServiceSvc_GetValueNotifyEmpParticipateTraining;
@class EmployeeServiceSvc_GetValueNotifyEmpParticipateTrainingResponse;
@class EmployeeServiceSvc_GetJsonListNotifyEmpExpirationDegree;
@class EmployeeServiceSvc_GetJsonListNotifyEmpExpirationDegreeResponse;
@class EmployeeServiceSvc_GetJsonListEmpExpirationDegree;
@class EmployeeServiceSvc_GetJsonListEmpExpirationDegreeResponse;
@class EmployeeServiceSvc_GetValueNotifyExpirationDegree;
@class EmployeeServiceSvc_GetValueNotifyExpirationDegreeResponse;
@class EmployeeServiceSvc_GetJsonListNotifyEmpExpirationContract;
@class EmployeeServiceSvc_GetJsonListNotifyEmpExpirationContractResponse;
@class EmployeeServiceSvc_GetJsonListEmpExpirationContract;
@class EmployeeServiceSvc_GetJsonListEmpExpirationContractResponse;
@class EmployeeServiceSvc_GetValueNotifyExpirationContract;
@class EmployeeServiceSvc_GetValueNotifyExpirationContractResponse;
@class EmployeeServiceSvc_GetJsonListNotifyEmpWaitReSignedContract;
@class EmployeeServiceSvc_GetJsonListNotifyEmpWaitReSignedContractResponse;
@class EmployeeServiceSvc_GetJsonListEmpWaitReSignedContract;
@class EmployeeServiceSvc_GetJsonListEmpWaitReSignedContractResponse;
@class EmployeeServiceSvc_GetValueNotifyWaitResignedContract;
@class EmployeeServiceSvc_GetValueNotifyWaitResignedContractResponse;
@class EmployeeServiceSvc_GetJsonListNotifyEmpLeave;
@class EmployeeServiceSvc_GetJsonListNotifyEmpLeaveResponse;
@class EmployeeServiceSvc_GetJsonListEmpLeave;
@class EmployeeServiceSvc_GetJsonListEmpLeaveResponse;
@class EmployeeServiceSvc_GetValueNotifyEmpLeave;
@class EmployeeServiceSvc_GetValueNotifyEmpLeaveResponse;
@class EmployeeServiceSvc_GetJsonListEmpMaterityLeave;
@class EmployeeServiceSvc_GetJsonListEmpMaterityLeaveResponse;
@class EmployeeServiceSvc_GetJsonListEmpWillMaterityLeave;
@class EmployeeServiceSvc_GetJsonListEmpWillMaterityLeaveResponse;
@class EmployeeServiceSvc_GetJsonListEmpWillPregnancyMonth7thLeave;
@class EmployeeServiceSvc_GetJsonListEmpWillPregnancyMonth7thLeaveResponse;
@class EmployeeServiceSvc_GetJsonListEmpRaisingChildExpiration;
@class EmployeeServiceSvc_GetJsonListEmpRaisingChildExpirationResponse;
@class EmployeeServiceSvc_GetJsonListEmpFamilyDependentExpiration;
@class EmployeeServiceSvc_GetJsonListEmpFamilyDependentExpirationResponse;
@class EmployeeServiceSvc_GetJsonListNotifyEmpWillChildBirth;
@class EmployeeServiceSvc_GetJsonListNotifyEmpWillChildBirthResponse;
@class EmployeeServiceSvc_GetJsonListEmpWillChildBirth;
@class EmployeeServiceSvc_GetJsonListEmpWillChildBirthResponse;
@class EmployeeServiceSvc_GetValueNotifyWillChildBirth;
@class EmployeeServiceSvc_GetValueNotifyWillChildBirthResponse;
@class EmployeeServiceSvc_GetJsonListNotifyLongTermLeave;
@class EmployeeServiceSvc_GetJsonListNotifyLongTermLeaveResponse;
@class EmployeeServiceSvc_GetJsonListLongTermLeave;
@class EmployeeServiceSvc_GetJsonListLongTermLeaveResponse;
@class EmployeeServiceSvc_GetValueNotifyLongTermLeave;
@class EmployeeServiceSvc_GetValueNotifyLongTermLeaveResponse;
@class EmployeeServiceSvc_GetJsonListNotifyEmpWillRetire;
@class EmployeeServiceSvc_GetJsonListNotifyEmpWillRetireResponse;
@class EmployeeServiceSvc_GetJsonListEmpWillRetire;
@class EmployeeServiceSvc_GetJsonListEmpWillRetireResponse;
@class EmployeeServiceSvc_GetvalueNotifyWillRetire;
@class EmployeeServiceSvc_GetvalueNotifyWillRetireResponse;
@class EmployeeServiceSvc_GetValueRetireAgeForMale;
@class EmployeeServiceSvc_GetValueRetireAgeForMaleResponse;
@class EmployeeServiceSvc_GetValueRetireAgeForFamale;
@class EmployeeServiceSvc_GetValueRetireAgeForFamaleResponse;
@class EmployeeServiceSvc_GetJsonListNotifyEmpWorkAfterChildBirth;
@class EmployeeServiceSvc_GetJsonListNotifyEmpWorkAfterChildBirthResponse;
@class EmployeeServiceSvc_GetJsonListEmpWorkAfterChildBirth;
@class EmployeeServiceSvc_GetJsonListEmpWorkAfterChildBirthResponse;
@class EmployeeServiceSvc_GetValueNotifyWorlAfterChildBirth;
@class EmployeeServiceSvc_GetValueNotifyWorlAfterChildBirthResponse;
@class EmployeeServiceSvc_GetJsonListEmpWorkAfterLeave;
@class EmployeeServiceSvc_GetJsonListEmpWorkAfterLeaveResponse;
@class EmployeeServiceSvc_GetEmpOffProfileById;
@class EmployeeServiceSvc_GetEmpOffProfileByIdResponse;
@class EmployeeServiceSvc_GETAllViewEmpOffProfile;
@class EmployeeServiceSvc_GETAllViewEmpOffProfileResponse;
@class EmployeeServiceSvc_GetViewEmpOffProfileById;
@class EmployeeServiceSvc_GetViewEmpOffProfileByIdResponse;
@class EmployeeServiceSvc_AddEmpOffProfileByView;
@class EmployeeServiceSvc_AddEmpOffProfileByViewResponse;
@class EmployeeServiceSvc_UpdateEmpOffProfileByView;
@class EmployeeServiceSvc_UpdateEmpOffProfileByViewResponse;
@class EmployeeServiceSvc_DeleteEmpOffProfileById;
@class EmployeeServiceSvc_DeleteEmpOffProfileByIdResponse;
@class EmployeeServiceSvc_GetAllCLogEquipment;
@class EmployeeServiceSvc_GetAllCLogEquipmentResponse;
@class EmployeeServiceSvc_GetAllCLogEquipmentType;
@class EmployeeServiceSvc_GetAllCLogEquipmentTypeResponse;
@class EmployeeServiceSvc_GetAllCLogAssetsType;
@class EmployeeServiceSvc_GetAllCLogAssetsTypeResponse;
@class EmployeeServiceSvc_GetAllEquipmentByEmployeeId;
@class EmployeeServiceSvc_GetAllEquipmentByEmployeeIdResponse;
@class EmployeeServiceSvc_GetAllAssetsByEmployeeId;
@class EmployeeServiceSvc_GetAllAssetsByEmployeeIdResponse;
@class EmployeeServiceSvc_GetAllViewCLogEquipment;
@class EmployeeServiceSvc_GetAllViewCLogEquipmentResponse;
@class EmployeeServiceSvc_GetAllViewCLogAssets;
@class EmployeeServiceSvc_GetAllViewCLogAssetsResponse;
@class EmployeeServiceSvc_GetAllViewOrgUnitAssets;
@class EmployeeServiceSvc_GetAllViewOrgUnitAssetsResponse;
@class EmployeeServiceSvc_GetAllViewOrgUnitEquipment;
@class EmployeeServiceSvc_GetAllViewOrgUnitEquipmentResponse;
@class EmployeeServiceSvc_GetAllEmpHasAssetsByAssetsIdAndUnitId;
@class EmployeeServiceSvc_GetAllEmpHasAssetsByAssetsIdAndUnitIdResponse;
@class EmployeeServiceSvc_GetAllEmpHasEquipmentByEquipmentIdAndUnitId;
@class EmployeeServiceSvc_GetAllEmpHasEquipmentByEquipmentIdAndUnitIdResponse;
@class EmployeeServiceSvc_GetAllEmployee;
@class EmployeeServiceSvc_GetAllEmployeeResponse;
@class EmployeeServiceSvc_GetAllCities;
@class EmployeeServiceSvc_GetAllCitiesResponse;
@class EmployeeServiceSvc_GetAllDistricts;
@class EmployeeServiceSvc_GetAllDistrictsResponse;
@class EmployeeServiceSvc_GetAllCountries;
@class EmployeeServiceSvc_GetAllCountriesResponse;
@class EmployeeServiceSvc_CreateAddress;
@class EmployeeServiceSvc_CreateAddressResponse;
@class EmployeeServiceSvc_GetDirectReportAndChildById;
@class EmployeeServiceSvc_GetDirectReportAndChildByIdResponse;
@class EmployeeServiceSvc_GetAllDirectReport;
@class EmployeeServiceSvc_GetAllDirectReportResponse;
@class EmployeeServiceSvc_GetEmployeeSearchResult;
@class EmployeeServiceSvc_GetEmployeeSearchResultResponse;
@class EmployeeServiceSvc_GetDirectReportByChildId;
@class EmployeeServiceSvc_GetDirectReportByChildIdResponse;
@class EmployeeServiceSvc_GetUserIdByEmployeeCode;
@class EmployeeServiceSvc_GetUserIdByEmployeeCodeResponse;
@class EmployeeServiceSvc_GetLeaderOfOrg;
@class EmployeeServiceSvc_GetLeaderOfOrgResponse;
@class EmployeeServiceSvc_GetInforOfEmps;
@class EmployeeServiceSvc_GetInforOfEmpsResponse;
@class EmployeeServiceSvc_GetEmpInGender;
@class EmployeeServiceSvc_GetEmpInGenderResponse;
@class EmployeeServiceSvc_GetNewestEmployee;
@class EmployeeServiceSvc_GetNewestEmployeeResponse;
@class EmployeeServiceSvc_GetAllEmpAssignedGroup;
@class EmployeeServiceSvc_GetAllEmpAssignedGroupResponse;
@class EmployeeServiceSvc_AssignEmpToGroup;
@class EmployeeServiceSvc_AssignEmpToGroupResponse;
@class EmployeeServiceSvc_RevokeEmpFromGroup;
@class EmployeeServiceSvc_RevokeEmpFromGroupResponse;
@class EmployeeServiceSvc_GetDirectReportOfEmployeeByEmployeeId;
@class EmployeeServiceSvc_GetDirectReportOfEmployeeByEmployeeIdResponse;
@class EmployeeServiceSvc_GetAllDirectReportOfEmpByEmpId;
@class EmployeeServiceSvc_GetAllDirectReportOfEmpByEmpIdResponse;
@class EmployeeServiceSvc_GetViewEmpBasicInformationById;
@class EmployeeServiceSvc_GetViewEmpBasicInformationByIdResponse;
@class EmployeeServiceSvc_GetAllViewEmployeesByUser;
@class EmployeeServiceSvc_GetAllViewEmployeesByUserResponse;
@class EmployeeServiceSvc_GetViewEmployeesByUser;
@class EmployeeServiceSvc_GetViewEmployeesByUserResponse;
@class EmployeeServiceSvc_GetAllViewEmployees;
@class EmployeeServiceSvc_GetAllViewEmployeesResponse;
@class EmployeeServiceSvc_GetViewEmployeesByPage;
@class EmployeeServiceSvc_GetViewEmployeesByPageResponse;
@class EmployeeServiceSvc_GetViewEmployeePageNumber;
@class EmployeeServiceSvc_GetViewEmployeePageNumberResponse;
@class EmployeeServiceSvc_GetViewEmployeeById;
@class EmployeeServiceSvc_GetViewEmployeeByIdResponse;
@class EmployeeServiceSvc_GetViewEmployeeProfileByEmployeeId;
@class EmployeeServiceSvc_GetViewEmployeeProfileByEmployeeIdResponse;
@class EmployeeServiceSvc_GetAllEmployees;
@class EmployeeServiceSvc_GetAllEmployeesResponse;
@class EmployeeServiceSvc_GetEmployeeById;
@class EmployeeServiceSvc_GetEmployeeByIdResponse;
@class EmployeeServiceSvc_SubmitCreateNewEmployee;
@class EmployeeServiceSvc_SubmitCreateNewEmployeeResponse;
@class EmployeeServiceSvc_CheckValidEmpCode;
@class EmployeeServiceSvc_CheckValidEmpCodeResponse;
@class EmployeeServiceSvc_CreateEmployee;
@class EmployeeServiceSvc_CreateEmployeeResponse;
@class EmployeeServiceSvc_GetAllLeader;
@class EmployeeServiceSvc_GetAllLeaderResponse;
@class EmployeeServiceSvc_CreateEmpProfileJobPosition;
@class EmployeeServiceSvc_CreateEmpProfileJobPositionResponse;
@class EmployeeServiceSvc_CreateEmpProfileProcessOfWork;
@class EmployeeServiceSvc_CreateEmpProfileProcessOfWorkResponse;
@class EmployeeServiceSvc_CreateEmpBasicProfile;
@class EmployeeServiceSvc_CreateEmpBasicProfileResponse;
@class EmployeeServiceSvc_GetViewEmpBasicProfilesByEmployeeId;
@class EmployeeServiceSvc_GetViewEmpBasicProfilesByEmployeeIdResponse;
@class EmployeeServiceSvc_GetAllCLogFamilyRelationships;
@class EmployeeServiceSvc_GetAllCLogFamilyRelationshipsResponse;
@class EmployeeServiceSvc_GetViewEmpProfileWorkingExperiencesByEmployeeId;
@class EmployeeServiceSvc_GetViewEmpProfileWorkingExperiencesByEmployeeIdResponse;
@class EmployeeServiceSvc_GetViewEmpProfileSkillsByEmployeeId;
@class EmployeeServiceSvc_GetViewEmpProfileSkillsByEmployeeIdResponse;
@class EmployeeServiceSvc_GetViewEmpProfileJobPositionsByEmployeeId;
@class EmployeeServiceSvc_GetViewEmpProfileJobPositionsByEmployeeIdResponse;
@class EmployeeServiceSvc_GetViewEmpContractsByEmployeeId;
@class EmployeeServiceSvc_GetViewEmpContractsByEmployeeIdResponse;
@class EmployeeServiceSvc_GetViewEmpProfileBasicSalarysByEmployeeId;
@class EmployeeServiceSvc_GetViewEmpProfileBasicSalarysByEmployeeIdResponse;
@class EmployeeServiceSvc_GetViewEmpProfileHealthInsurancesByEmployeeId;
@class EmployeeServiceSvc_GetViewEmpProfileHealthInsurancesByEmployeeIdResponse;
@class EmployeeServiceSvc_GetViewEmpProfileWageTypesByEmployeeId;
@class EmployeeServiceSvc_GetViewEmpProfileWageTypesByEmployeeIdResponse;
@class EmployeeServiceSvc_GetViewEmpProfileWorkingFormsByEmployeeId;
@class EmployeeServiceSvc_GetViewEmpProfileWorkingFormsByEmployeeIdResponse;
@class EmployeeServiceSvc_GetViewEmpSocialInsuranceSalariesByEmployeeId;
@class EmployeeServiceSvc_GetViewEmpSocialInsuranceSalariesByEmployeeIdResponse;
@class EmployeeServiceSvc_GetViewEmpProfileBiographiesByEmployeeId;
@class EmployeeServiceSvc_GetViewEmpProfileBiographiesByEmployeeIdResponse;
@class EmployeeServiceSvc_GetViewEmpProfileExperiencesByEmployeeId;
@class EmployeeServiceSvc_GetViewEmpProfileExperiencesByEmployeeIdResponse;
@class EmployeeServiceSvc_GetViewEmpProfileProcessOfWorksByEmployeeId;
@class EmployeeServiceSvc_GetViewEmpProfileProcessOfWorksByEmployeeIdResponse;
@class EmployeeServiceSvc_GetViewEmpProfileContactsByEmployeeId;
@class EmployeeServiceSvc_GetViewEmpProfileContactsByEmployeeIdResponse;
@class EmployeeServiceSvc_GetViewEmpProfileSocialInsurancesByEmployeeId;
@class EmployeeServiceSvc_GetViewEmpProfileSocialInsurancesByEmployeeIdResponse;
@class EmployeeServiceSvc_GetViewEmpProfileLeaveRegimesByEmployeeId;
@class EmployeeServiceSvc_GetViewEmpProfileLeaveRegimesByEmployeeIdResponse;
@class EmployeeServiceSvc_GetViewEmpPerformanceAppraisalsByEmployeeId;
@class EmployeeServiceSvc_GetViewEmpPerformanceAppraisalsByEmployeeIdResponse;
@class EmployeeServiceSvc_GetViewEmpProfileQualificationsByEmployeeId;
@class EmployeeServiceSvc_GetViewEmpProfileQualificationsByEmployeeIdResponse;
@class EmployeeServiceSvc_GetViewEmpProfileForeignLanguagesByEmployeeId;
@class EmployeeServiceSvc_GetViewEmpProfileForeignLanguagesByEmployeeIdResponse;
@class EmployeeServiceSvc_GetViewEmpProfileTrainingsByEmployeeId;
@class EmployeeServiceSvc_GetViewEmpProfileTrainingsByEmployeeIdResponse;
@class EmployeeServiceSvc_GetViewEmpProfileDegreesByEmployeeId;
@class EmployeeServiceSvc_GetViewEmpProfileDegreesByEmployeeIdResponse;
@class EmployeeServiceSvc_GetViewEmpProfileDisciplinesByEmployeeId;
@class EmployeeServiceSvc_GetViewEmpProfileDisciplinesByEmployeeIdResponse;
@class EmployeeServiceSvc_GetViewEmEmpProfilePersonalitiesByEmployeeId;
@class EmployeeServiceSvc_GetViewEmEmpProfilePersonalitiesByEmployeeIdResponse;
@class EmployeeServiceSvc_GetAllTimeWorkingForms;
@class EmployeeServiceSvc_GetAllTimeWorkingFormsResponse;
@class EmployeeServiceSvc_GetAllTimeWageTypes;
@class EmployeeServiceSvc_GetAllTimeWageTypesResponse;
@class EmployeeServiceSvc_GetTimeWageTypeById;
@class EmployeeServiceSvc_GetTimeWageTypeByIdResponse;
@class EmployeeServiceSvc_GetAllCLogCBCompensationCategories;
@class EmployeeServiceSvc_GetAllCLogCBCompensationCategoriesResponse;
@class EmployeeServiceSvc_GetCLogCitiesByCLogCountryId;
@class EmployeeServiceSvc_GetCLogCitiesByCLogCountryIdResponse;
@class EmployeeServiceSvc_GetAllCLogCities;
@class EmployeeServiceSvc_GetAllCLogCitiesResponse;
@class EmployeeServiceSvc_GetCLogCityById;
@class EmployeeServiceSvc_GetCLogCityByIdResponse;
@class EmployeeServiceSvc_GetAllCLogCurrencies;
@class EmployeeServiceSvc_GetAllCLogCurrenciesResponse;
@class EmployeeServiceSvc_GetCLogCurrencyById;
@class EmployeeServiceSvc_GetCLogCurrencyByIdResponse;
@class EmployeeServiceSvc_GetAllCLogCurrencyRates;
@class EmployeeServiceSvc_GetAllCLogCurrencyRatesResponse;
@class EmployeeServiceSvc_GetCLogCurrencyRateById;
@class EmployeeServiceSvc_GetCLogCurrencyRateByIdResponse;
@class EmployeeServiceSvc_GetAllSysEmpProfileGroupLayers;
@class EmployeeServiceSvc_GetAllSysEmpProfileGroupLayersResponse;
@class EmployeeServiceSvc_GetSysEmpProfileLayersByGroupId;
@class EmployeeServiceSvc_GetSysEmpProfileLayersByGroupIdResponse;
@class EmployeeServiceSvc_GetSysEmpProfileLayerUrlsByLayerId;
@class EmployeeServiceSvc_GetSysEmpProfileLayerUrlsByLayerIdResponse;
@class EmployeeServiceSvc_GetSysEmpProfileLayerUrlsByTypeByLayerId;
@class EmployeeServiceSvc_GetSysEmpProfileLayerUrlsByTypeByLayerIdResponse;
@class EmployeeServiceSvc_GetViewSysEmpProfileLayerByTypeByLayerId;
@class EmployeeServiceSvc_GetViewSysEmpProfileLayerByTypeByLayerIdResponse;
@class EmployeeServiceSvc_GetAllCLogEmployeeTypes;
@class EmployeeServiceSvc_GetAllCLogEmployeeTypesResponse;
@class EmployeeServiceSvc_GetCLogEmployeeTypeById;
@class EmployeeServiceSvc_GetCLogEmployeeTypeByIdResponse;
@class EmployeeServiceSvc_GetAllCLogMaritalStatuses;
@class EmployeeServiceSvc_GetAllCLogMaritalStatusesResponse;
@class EmployeeServiceSvc_GetAllCLogCountries;
@class EmployeeServiceSvc_GetAllCLogCountriesResponse;
@class EmployeeServiceSvc_GetCLogCountryById;
@class EmployeeServiceSvc_GetCLogCountryByIdResponse;
@class EmployeeServiceSvc_GetAllOrgUnits;
@class EmployeeServiceSvc_GetAllOrgUnitsResponse;
@class EmployeeServiceSvc_GetViewOrgUnitJobsByOrgUnitId;
@class EmployeeServiceSvc_GetViewOrgUnitJobsByOrgUnitIdResponse;
@class EmployeeServiceSvc_GetOrgWorkLevelsByOrgJobId;
@class EmployeeServiceSvc_GetOrgWorkLevelsByOrgJobIdResponse;
@class EmployeeServiceSvc_GetAllCLogCareers;
@class EmployeeServiceSvc_GetAllCLogCareersResponse;
@class EmployeeServiceSvc_GetOrgJobPositionById;
@class EmployeeServiceSvc_GetOrgJobPositionByIdResponse;
@class EmployeeServiceSvc_GetOrgJobPositionsByOrgUnitId;
@class EmployeeServiceSvc_GetOrgJobPositionsByOrgUnitIdResponse;
@class EmployeeServiceSvc_GetOrgJobPositionsByOrgUnitIdAndOrgJobId;
@class EmployeeServiceSvc_GetOrgJobPositionsByOrgUnitIdAndOrgJobIdResponse;
@class EmployeeServiceSvc_GetAllCLogRatings;
@class EmployeeServiceSvc_GetAllCLogRatingsResponse;
@class EmployeeServiceSvc_GetAllCLogDegrees;
@class EmployeeServiceSvc_GetAllCLogDegreesResponse;
@class EmployeeServiceSvc_GetAllCLogMajors;
@class EmployeeServiceSvc_GetAllCLogMajorsResponse;
@class EmployeeServiceSvc_GetCLogMajorById;
@class EmployeeServiceSvc_GetCLogMajorByIdResponse;
@class EmployeeServiceSvc_GetAllCLogTrainingCenters;
@class EmployeeServiceSvc_GetAllCLogTrainingCentersResponse;
@class EmployeeServiceSvc_GetAllCLogTrainings;
@class EmployeeServiceSvc_GetAllCLogTrainingsResponse;
@class EmployeeServiceSvc_GetAllOrgDegrees;
@class EmployeeServiceSvc_GetAllOrgDegreesResponse;
@class EmployeeServiceSvc_GetOrgDegreeById;
@class EmployeeServiceSvc_GetOrgDegreeByIdResponse;
@class EmployeeServiceSvc_GetAllOrgDegreeRanks;
@class EmployeeServiceSvc_GetAllOrgDegreeRanksResponse;
@class EmployeeServiceSvc_GetAllCLogHospitals;
@class EmployeeServiceSvc_GetAllCLogHospitalsResponse;
@class EmployeeServiceSvc_GetCLogHospitalById;
@class EmployeeServiceSvc_GetCLogHospitalByIdResponse;
@class EmployeeServiceSvc_GetAllCLogEthnicities;
@class EmployeeServiceSvc_GetAllCLogEthnicitiesResponse;
@class EmployeeServiceSvc_GetCLogEthnicityById;
@class EmployeeServiceSvc_GetCLogEthnicityByIdResponse;
@class EmployeeServiceSvc_GetAllCLogReligions;
@class EmployeeServiceSvc_GetAllCLogReligionsResponse;
@class EmployeeServiceSvc_GetCLogReligionById;
@class EmployeeServiceSvc_GetCLogReligionByIdResponse;
@class EmployeeServiceSvc_GetAllCLogLanguages;
@class EmployeeServiceSvc_GetAllCLogLanguagesResponse;
@class EmployeeServiceSvc_GetCLogLanguageById;
@class EmployeeServiceSvc_GetCLogLanguageByIdResponse;
@class EmployeeServiceSvc_GetAllCLogEducationLevels;
@class EmployeeServiceSvc_GetAllCLogEducationLevelsResponse;
@class EmployeeServiceSvc_GetCLogEducationLevelById;
@class EmployeeServiceSvc_GetCLogEducationLevelByIdResponse;
@class EmployeeServiceSvc_GetAllOrgQualifications;
@class EmployeeServiceSvc_GetAllOrgQualificationsResponse;
@class EmployeeServiceSvc_GetAllCLogEmpCulturalLevels;
@class EmployeeServiceSvc_GetAllCLogEmpCulturalLevelsResponse;
@class EmployeeServiceSvc_GetCLogEmpCulturalLevelById;
@class EmployeeServiceSvc_GetCLogEmpCulturalLevelByIdResponse;
@class EmployeeServiceSvc_GetAllCLogMaritalStatus;
@class EmployeeServiceSvc_GetAllCLogMaritalStatusResponse;
@class EmployeeServiceSvc_GetCLogMaritalStatusById;
@class EmployeeServiceSvc_GetCLogMaritalStatusByIdResponse;
@class EmployeeServiceSvc_GetAllCLogBanks;
@class EmployeeServiceSvc_GetAllCLogBanksResponse;
@class EmployeeServiceSvc_GetCLogBankByCLogBankBranchId;
@class EmployeeServiceSvc_GetCLogBankByCLogBankBranchIdResponse;
@class EmployeeServiceSvc_GetAllCLogBankBranchs;
@class EmployeeServiceSvc_GetAllCLogBankBranchsResponse;
@class EmployeeServiceSvc_GetCLogBankBranchById;
@class EmployeeServiceSvc_GetCLogBankBranchByIdResponse;
@class EmployeeServiceSvc_GetCLogBankBranchesByCLogBankId;
@class EmployeeServiceSvc_GetCLogBankBranchesByCLogBankIdResponse;
@class EmployeeServiceSvc_GetAllCLogCourseStatus;
@class EmployeeServiceSvc_GetAllCLogCourseStatusResponse;
@class EmployeeServiceSvc_GetCLogCourseStatusById;
@class EmployeeServiceSvc_GetCLogCourseStatusByIdResponse;
@class EmployeeServiceSvc_GetAllCLogEmpWorkingStatus;
@class EmployeeServiceSvc_GetAllCLogEmpWorkingStatusResponse;
@class EmployeeServiceSvc_GetCLogEmpWorkingStatusById;
@class EmployeeServiceSvc_GetCLogEmpWorkingStatusByIdResponse;
@class EmployeeServiceSvc_GetAllCLogPersonalities;
@class EmployeeServiceSvc_GetAllCLogPersonalitiesResponse;
@class EmployeeServiceSvc_GetCLogCLogPersonalityById;
@class EmployeeServiceSvc_GetCLogCLogPersonalityByIdResponse;
@class EmployeeServiceSvc_GetAllOrgProjectTypes;
@class EmployeeServiceSvc_GetAllOrgProjectTypesResponse;
@class EmployeeServiceSvc_GetOrgProjectTypeById;
@class EmployeeServiceSvc_GetOrgProjectTypeByIdResponse;
@class EmployeeServiceSvc_GetAllOrgTimeInCharges;
@class EmployeeServiceSvc_GetAllOrgTimeInChargesResponse;
@class EmployeeServiceSvc_GetOrgTimeInChargeById;
@class EmployeeServiceSvc_GetOrgTimeInChargeByIdResponse;
@class EmployeeServiceSvc_GetAllOrgSkills;
@class EmployeeServiceSvc_GetAllOrgSkillsResponse;
@class EmployeeServiceSvc_GetOrgSkillById;
@class EmployeeServiceSvc_GetOrgSkillByIdResponse;
@class EmployeeServiceSvc_GetAllOrgSkillTypes;
@class EmployeeServiceSvc_GetAllOrgSkillTypesResponse;
@class EmployeeServiceSvc_GetOrgSkillTypeById;
@class EmployeeServiceSvc_GetOrgSkillTypeByIdResponse;
@class EmployeeServiceSvc_GetAllCLogCBFactors;
@class EmployeeServiceSvc_GetAllCLogCBFactorsResponse;
@class EmployeeServiceSvc_GetCLogCBFactorById;
@class EmployeeServiceSvc_GetCLogCBFactorByIdResponse;
@class EmployeeServiceSvc_GetAllViewCLogCBFactors;
@class EmployeeServiceSvc_GetAllViewCLogCBFactorsResponse;
@class EmployeeServiceSvc_GetAllCLogEmpOffTypes;
@class EmployeeServiceSvc_GetAllCLogEmpOffTypesResponse;
@class EmployeeServiceSvc_GetAllOrgKnowledge;
@class EmployeeServiceSvc_GetAllOrgKnowledgeResponse;
@class EmployeeServiceSvc_GetAllOrgKnowledgeByMajorId;
@class EmployeeServiceSvc_GetAllOrgKnowledgeByMajorIdResponse;
@class EmployeeServiceSvc_GetAllOrgUnitByUser;
@class EmployeeServiceSvc_GetAllOrgUnitByUserResponse;
@class EmployeeServiceSvc_GetAllEmpBasicInfomationByOrgUnitIds;
@class EmployeeServiceSvc_GetAllEmpBasicInfomationByOrgUnitIdsResponse;
@class EmployeeServiceSvc_GetAllV_EmpBasicInfoByOrgUnitId;
@class EmployeeServiceSvc_GetAllV_EmpBasicInfoByOrgUnitIdResponse;
@class EmployeeServiceSvc_FTS_EmpProfileSearchAll;
@class EmployeeServiceSvc_FTS_EmpProfileSearchAllResponse;
@class EmployeeServiceSvc_FTS_EmpProfileBasic;
@class EmployeeServiceSvc_FTS_EmpProfileBasicResponse;
@class EmployeeServiceSvc_FTS_EmpProfileContact;
@class EmployeeServiceSvc_FTS_EmpProfileContactResponse;
@class EmployeeServiceSvc_FTS_EmpProfileDegree;
@class EmployeeServiceSvc_FTS_EmpProfileDegreeResponse;
@class EmployeeServiceSvc_FTS_EmpProfileEduQualification;
@class EmployeeServiceSvc_FTS_EmpProfileEduQualificationResponse;
@class EmployeeServiceSvc_FTS_EmpProfileForeignLanguage;
@class EmployeeServiceSvc_FTS_EmpProfileForeignLanguageResponse;
@class EmployeeServiceSvc_FTS_EmpProfileJobPosition;
@class EmployeeServiceSvc_FTS_EmpProfileJobPositionResponse;
#import "emp_tns1.h"
#import "emp_tns2.h"
#import "emp_tns3.h"
#import "emp_tns4.h"
@interface EmployeeServiceSvc_FTS_EmpProfileOrgUnit : NSObject {
	
/* elements */
	NSString * condition;
	NSNumber * userId;
	NSNumber * companyId;
	NSNumber * take;
	NSNumber * skip;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_FTS_EmpProfileOrgUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * condition;
@property (retain) NSNumber * userId;
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * take;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_FTS_EmpProfileOrgUnitResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_ORG_UNIT_Result * FTS_EmpProfileOrgUnitResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_FTS_EmpProfileOrgUnitResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_ORG_UNIT_Result * FTS_EmpProfileOrgUnitResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_FTS_EmpContract : NSObject {
	
/* elements */
	NSString * condition;
	NSString * criterion;
	NSNumber * userId;
	NSNumber * companyId;
	NSNumber * take;
	NSNumber * skip;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_FTS_EmpContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * condition;
@property (retain) NSString * criterion;
@property (retain) NSNumber * userId;
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * take;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_FTS_EmpContractResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfSP_FTS_EMP_CONTRACT_Result * FTS_EmpContractResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_FTS_EmpContractResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfSP_FTS_EMP_CONTRACT_Result * FTS_EmpContractResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_FTS_EmpOff : NSObject {
	
/* elements */
	NSString * condition;
	NSString * criterion;
	NSNumber * userId;
	NSNumber * companyId;
	NSNumber * take;
	NSNumber * skip;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_FTS_EmpOff *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * condition;
@property (retain) NSString * criterion;
@property (retain) NSNumber * userId;
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * take;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_FTS_EmpOffResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfSP_FTS_EMP_OFF_Result * FTS_EmpOffResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_FTS_EmpOffResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfSP_FTS_EMP_OFF_Result * FTS_EmpOffResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmployeeByAdvancedSearch : NSObject {
	
/* elements */
	NSNumber * userId;
	NSNumber * companyId;
	NSString * employeeCode;
	NSString * fullName;
	NSString * identityCardNo;
	NSNumber * gender;
	NSNumber * orgUnitId;
	NSNumber * orgJobPositionId;
	NSNumber * employeeTypeId;
	NSNumber * workingFormId;
	NSNumber * qualificationId;
	NSNumber * majorId;
	NSNumber * ethnicityId;
	NSNumber * religionId;
	NSString * birthday;
	NSString * officialday;
	NSNumber * take;
	NSNumber * skip;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmployeeByAdvancedSearch *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * userId;
@property (retain) NSNumber * companyId;
@property (retain) NSString * employeeCode;
@property (retain) NSString * fullName;
@property (retain) NSString * identityCardNo;
@property (retain) NSNumber * gender;
@property (retain) NSNumber * orgUnitId;
@property (retain) NSNumber * orgJobPositionId;
@property (retain) NSNumber * employeeTypeId;
@property (retain) NSNumber * workingFormId;
@property (retain) NSNumber * qualificationId;
@property (retain) NSNumber * majorId;
@property (retain) NSNumber * ethnicityId;
@property (retain) NSNumber * religionId;
@property (retain) NSString * birthday;
@property (retain) NSString * officialday;
@property (retain) NSNumber * take;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmployeeByAdvancedSearchResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfSP_EMP_ADS_EMPLOYEE_Result * GetAllEmployeeByAdvancedSearchResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmployeeByAdvancedSearchResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfSP_EMP_ADS_EMPLOYEE_Result * GetAllEmployeeByAdvancedSearchResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmployeeProfileByAdvancedSearch : NSObject {
	
/* elements */
	NSNumber * userId;
	NSNumber * companyId;
	NSString * employeeCode;
	NSString * fullName;
	NSString * identityCardNo;
	NSNumber * gender;
	NSNumber * orgUnitId;
	NSNumber * orgJobPositionId;
	NSNumber * employeeTypeId;
	NSNumber * workingFormId;
	NSNumber * qualificationId;
	NSNumber * majorId;
	NSNumber * ethnicityId;
	NSNumber * religionId;
	NSString * birthday;
	NSString * officialday;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmployeeProfileByAdvancedSearch *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * userId;
@property (retain) NSNumber * companyId;
@property (retain) NSString * employeeCode;
@property (retain) NSString * fullName;
@property (retain) NSString * identityCardNo;
@property (retain) NSNumber * gender;
@property (retain) NSNumber * orgUnitId;
@property (retain) NSNumber * orgJobPositionId;
@property (retain) NSNumber * employeeTypeId;
@property (retain) NSNumber * workingFormId;
@property (retain) NSNumber * qualificationId;
@property (retain) NSNumber * majorId;
@property (retain) NSNumber * ethnicityId;
@property (retain) NSNumber * religionId;
@property (retain) NSString * birthday;
@property (retain) NSString * officialday;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmployeeProfileByAdvancedSearchResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfSP_EMP_ADS_EMPLOYEE_Result * GetAllEmployeeProfileByAdvancedSearchResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmployeeProfileByAdvancedSearchResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfSP_EMP_ADS_EMPLOYEE_Result * GetAllEmployeeProfileByAdvancedSearchResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmployeeContractByAdvancedSearch : NSObject {
	
/* elements */
	NSNumber * userId;
	NSNumber * companyId;
	NSString * contractTypeCode;
	NSString * contractTermCode;
	NSString * employeeCode;
	NSString * fullName;
	NSNumber * orgUnitId;
	NSNumber * employeeTypeId;
	NSString * dateSignedFrom;
	NSString * dateSignedTo;
	NSString * endDateFrom;
	NSString * endDateTo;
	NSNumber * status;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmployeeContractByAdvancedSearch *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * userId;
@property (retain) NSNumber * companyId;
@property (retain) NSString * contractTypeCode;
@property (retain) NSString * contractTermCode;
@property (retain) NSString * employeeCode;
@property (retain) NSString * fullName;
@property (retain) NSNumber * orgUnitId;
@property (retain) NSNumber * employeeTypeId;
@property (retain) NSString * dateSignedFrom;
@property (retain) NSString * dateSignedTo;
@property (retain) NSString * endDateFrom;
@property (retain) NSString * endDateTo;
@property (retain) NSNumber * status;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmployeeContractByAdvancedSearchResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfSP_EMP_ADS_CONTRACT_Result * GetAllEmployeeContractByAdvancedSearchResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmployeeContractByAdvancedSearchResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfSP_EMP_ADS_CONTRACT_Result * GetAllEmployeeContractByAdvancedSearchResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmployeeOffByAdvancedSearch : NSObject {
	
/* elements */
	NSNumber * userId;
	NSNumber * companyId;
	NSString * employeeCode;
	NSString * fullName;
	NSNumber * employeeTypeId;
	NSNumber * orgUnitId;
	NSNumber * jobPositionId;
	NSNumber * empOffTypeId;
	NSString * offDateFrom;
	NSString * offDateTo;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmployeeOffByAdvancedSearch *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * userId;
@property (retain) NSNumber * companyId;
@property (retain) NSString * employeeCode;
@property (retain) NSString * fullName;
@property (retain) NSNumber * employeeTypeId;
@property (retain) NSNumber * orgUnitId;
@property (retain) NSNumber * jobPositionId;
@property (retain) NSNumber * empOffTypeId;
@property (retain) NSString * offDateFrom;
@property (retain) NSString * offDateTo;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmployeeOffByAdvancedSearchResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfSP_EMP_ADS_EMP_OFF_Result * GetAllEmployeeOffByAdvancedSearchResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmployeeOffByAdvancedSearchResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfSP_EMP_ADS_EMP_OFF_Result * GetAllEmployeeOffByAdvancedSearchResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmployer : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmployer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmployerResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfEmployer * GetAllEmployerResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmployerResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfEmployer * GetAllEmployerResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCompany : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCompany *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCompanyResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCompany * GetAllCompanyResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCompanyResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCompany * GetAllCompanyResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCompanyInfo : NSObject {
	
/* elements */
	NSNumber * CompanyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCompanyInfo *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCompanyInfoResponse : NSObject {
	
/* elements */
	emp_tns1_Company * GetCompanyInfoResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCompanyInfoResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_Company * GetCompanyInfoResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmpContractSummary : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmpContractSummary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmpContractSummaryResponse : NSObject {
	
/* elements */
	emp_tns2_ArrayOfEmpContractSummaryModel * GetAllEmpContractSummaryResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmpContractSummaryResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns2_ArrayOfEmpContractSummaryModel * GetAllEmpContractSummaryResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmpContract : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmpContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmpContractResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpContract * GetAllEmpContractResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmpContractResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpContract * GetAllEmpContractResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmpContractBySkipTake : NSObject {
	
/* elements */
	NSNumber * skip;
	NSNumber * take;
	NSNumber * total;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmpContractBySkipTake *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * total;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmpContractBySkipTakeResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpContract * GetAllEmpContractBySkipTakeResult;
	NSNumber * total;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmpContractBySkipTakeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpContract * GetAllEmpContractBySkipTakeResult;
@property (retain) NSNumber * total;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetEmpContract : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetEmpContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetEmpContractResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpContract * GetEmpContractResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetEmpContractResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpContract * GetEmpContractResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetEmpContractByContractId : NSObject {
	
/* elements */
	NSNumber * empContractId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetEmpContractByContractId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * empContractId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetEmpContractByContractIdResponse : NSObject {
	
/* elements */
	emp_tns1_V_EmpContract * GetEmpContractByContractIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetEmpContractByContractIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_V_EmpContract * GetEmpContractByContractIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_CountEmpContract : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_CountEmpContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_CountEmpContractResponse : NSObject {
	
/* elements */
	NSNumber * CountEmpContractResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_CountEmpContractResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CountEmpContractResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetEmpBasicProfileByEmployeId : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetEmpBasicProfileByEmployeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetEmpBasicProfileByEmployeIdResponse : NSObject {
	
/* elements */
	emp_tns1_EmpBasicProfile * GetEmpBasicProfileByEmployeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetEmpBasicProfileByEmployeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_EmpBasicProfile * GetEmpBasicProfileByEmployeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetWokingTimeByEmployeeId : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetWokingTimeByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetWokingTimeByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_V_EMP_CONTRACT_WOKING_HOURS * GetWokingTimeByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetWokingTimeByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_V_EMP_CONTRACT_WOKING_HOURS * GetWokingTimeByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetContractType : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetContractType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetContractTypeResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_ESS_CLOG_CONTRACT_TYPE * GetContractTypeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetContractTypeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_ESS_CLOG_CONTRACT_TYPE * GetContractTypeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_CheckExistEmpContractCode : NSObject {
	
/* elements */
	NSString * ContractCode;
	NSNumber * EmpContractId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_CheckExistEmpContractCode *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ContractCode;
@property (retain) NSNumber * EmpContractId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_CheckExistEmpContractCodeResponse : NSObject {
	
/* elements */
	USBoolean * CheckExistEmpContractCodeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_CheckExistEmpContractCodeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * CheckExistEmpContractCodeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmpContractTerm : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmpContractTerm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmpContractTermResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_ESS_CLOG_CONTRACT_TERM * GetAllEmpContractTermResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmpContractTermResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_ESS_CLOG_CONTRACT_TERM * GetAllEmpContractTermResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetEmpContractTermUndefined : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetEmpContractTermUndefined *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetEmpContractTermUndefinedResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_ESS_CLOG_CONTRACT_TERM * GetEmpContractTermUndefinedResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetEmpContractTermUndefinedResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_ESS_CLOG_CONTRACT_TERM * GetEmpContractTermUndefinedResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetEmpContractTermIdentified : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetEmpContractTermIdentified *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetEmpContractTermIdentifiedResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_ESS_CLOG_CONTRACT_TERM * GetEmpContractTermIdentifiedResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetEmpContractTermIdentifiedResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_ESS_CLOG_CONTRACT_TERM * GetEmpContractTermIdentifiedResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_CreateEmpContract : NSObject {
	
/* elements */
	emp_tns1_V_EmpContract * V_EmpContract;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_CreateEmpContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_V_EmpContract * V_EmpContract;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_CreateEmpContractResponse : NSObject {
	
/* elements */
	NSNumber * CreateEmpContractResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_CreateEmpContractResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CreateEmpContractResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_UpdateEmpContract : NSObject {
	
/* elements */
	emp_tns1_V_EmpContract * V_EmpContract;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_UpdateEmpContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_V_EmpContract * V_EmpContract;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_UpdateEmpContractResponse : NSObject {
	
/* elements */
	NSNumber * UpdateEmpContractResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_UpdateEmpContractResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * UpdateEmpContractResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_EndEmpContracts : NSObject {
	
/* elements */
	emp_tns3_ArrayOfint * EmpContractIds;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_EndEmpContracts *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns3_ArrayOfint * EmpContractIds;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_EndEmpContractsResponse : NSObject {
	
/* elements */
	NSNumber * EndEmpContractsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_EndEmpContractsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EndEmpContractsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_EndContracts : NSObject {
	
/* elements */
	emp_tns1_V_EmpContract * viewEmpContract;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_EndContracts *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_V_EmpContract * viewEmpContract;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_EndContractsResponse : NSObject {
	
/* elements */
	USBoolean * EndContractsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_EndContractsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * EndContractsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_DeleteEmpContracts : NSObject {
	
/* elements */
	emp_tns3_ArrayOfint * EmpContractIds;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_DeleteEmpContracts *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns3_ArrayOfint * EmpContractIds;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_DeleteEmpContractsResponse : NSObject {
	
/* elements */
	NSNumber * DeleteEmpContractsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_DeleteEmpContractsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * DeleteEmpContractsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_AppraiseContract : NSObject {
	
/* elements */
	emp_tns1_EmpContract * empContract;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_AppraiseContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_EmpContract * empContract;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_AppraiseContractResponse : NSObject {
	
/* elements */
	USBoolean * AppraiseContractResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_AppraiseContractResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AppraiseContractResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmpNotificationConstant : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmpNotificationConstant *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmpNotificationConstantResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfTB_ESS_SYSTEM_PARAMETER * GetAllEmpNotificationConstantResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmpNotificationConstantResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfTB_ESS_SYSTEM_PARAMETER * GetAllEmpNotificationConstantResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyEmpBirthday : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyEmpBirthday *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyEmpBirthdayResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_Employee * GetJsonListNotifyEmpBirthdayResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyEmpBirthdayResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_Employee * GetJsonListNotifyEmpBirthdayResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyEmpBirthday : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyEmpBirthday *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyEmpBirthdayResponse : NSObject {
	
/* elements */
	NSNumber * GetValueNotifyEmpBirthdayResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyEmpBirthdayResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetValueNotifyEmpBirthdayResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpBirthday : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSString * fromDate;
	NSString * toDate;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpBirthday *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSString * fromDate;
@property (retain) NSString * toDate;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpBirthdayResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_Employee * GetJsonListEmpBirthdayResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpBirthdayResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_Employee * GetJsonListEmpBirthdayResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyEmpAppoinmentExpiration : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyEmpAppoinmentExpiration *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyEmpAppoinmentExpirationResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpProfileJobPosition * GetJsonListNotifyEmpAppoinmentExpirationResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyEmpAppoinmentExpirationResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpProfileJobPosition * GetJsonListNotifyEmpAppoinmentExpirationResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyEmpAppoinmentExpiration : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyEmpAppoinmentExpiration *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyEmpAppoinmentExpirationResponse : NSObject {
	
/* elements */
	NSNumber * GetValueNotifyEmpAppoinmentExpirationResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyEmpAppoinmentExpirationResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetValueNotifyEmpAppoinmentExpirationResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpAppoinmentExpiration : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSString * fromDate;
	NSString * toDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpAppoinmentExpiration *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSString * fromDate;
@property (retain) NSString * toDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpAppoinmentExpirationResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpProfileJobPosition * GetJsonListEmpAppoinmentExpirationResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpAppoinmentExpirationResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpProfileJobPosition * GetJsonListEmpAppoinmentExpirationResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyEmpIncreaseSalary : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyEmpIncreaseSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyEmpIncreaseSalaryResponse : NSObject {
	
/* elements */
	NSNumber * GetValueNotifyEmpIncreaseSalaryResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyEmpIncreaseSalaryResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetValueNotifyEmpIncreaseSalaryResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyEmpToOffical : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyEmpToOffical *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyEmpToOfficalResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpBasicProfile * GetJsonListNotifyEmpToOfficalResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyEmpToOfficalResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpBasicProfile * GetJsonListNotifyEmpToOfficalResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpToOffical : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSString * fromDate;
	NSString * toDate;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpToOffical *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSString * fromDate;
@property (retain) NSString * toDate;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpToOfficalResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpBasicInformation * GetJsonListEmpToOfficalResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpToOfficalResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpBasicInformation * GetJsonListEmpToOfficalResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyEmpToOffical : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyEmpToOffical *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyEmpToOfficalResponse : NSObject {
	
/* elements */
	NSNumber * GetValueNotifyEmpToOfficalResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyEmpToOfficalResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetValueNotifyEmpToOfficalResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyEmpExpirationPassport : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyEmpExpirationPassport *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyEmpExpirationPassportResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpBasicProfile * GetJsonListNotifyEmpExpirationPassportResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyEmpExpirationPassportResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpBasicProfile * GetJsonListNotifyEmpExpirationPassportResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpExpirationPassport : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSString * fromDate;
	NSString * toDate;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpExpirationPassport *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSString * fromDate;
@property (retain) NSString * toDate;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpExpirationPassportResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpBasicInformation * GetJsonListEmpExpirationPassportResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpExpirationPassportResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpBasicInformation * GetJsonListEmpExpirationPassportResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyEmpExpirationPassport : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyEmpExpirationPassport *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyEmpExpirationPassportResponse : NSObject {
	
/* elements */
	NSNumber * GetValueNotifyEmpExpirationPassportResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyEmpExpirationPassportResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetValueNotifyEmpExpirationPassportResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyEmpParticipateTraining : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyEmpParticipateTraining *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyEmpParticipateTrainingResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfEmpProfileTraining * GetJsonListNotifyEmpParticipateTrainingResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyEmpParticipateTrainingResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfEmpProfileTraining * GetJsonListNotifyEmpParticipateTrainingResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpParticipateTraining : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSString * fromDate;
	NSString * toDate;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpParticipateTraining *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSString * fromDate;
@property (retain) NSString * toDate;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpParticipateTrainingResponse : NSObject {
	
/* elements */
	emp_tns4_ArrayOfNotificationModel * GetJsonListEmpParticipateTrainingResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpParticipateTrainingResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns4_ArrayOfNotificationModel * GetJsonListEmpParticipateTrainingResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyEmpParticipateTraining : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyEmpParticipateTraining *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyEmpParticipateTrainingResponse : NSObject {
	
/* elements */
	NSNumber * GetValueNotifyEmpParticipateTrainingResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyEmpParticipateTrainingResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetValueNotifyEmpParticipateTrainingResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyEmpExpirationDegree : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyEmpExpirationDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyEmpExpirationDegreeResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpProfileDegree * GetJsonListNotifyEmpExpirationDegreeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyEmpExpirationDegreeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpProfileDegree * GetJsonListNotifyEmpExpirationDegreeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpExpirationDegree : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSString * fromDate;
	NSString * toDate;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpExpirationDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSString * fromDate;
@property (retain) NSString * toDate;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpExpirationDegreeResponse : NSObject {
	
/* elements */
	emp_tns4_ArrayOfNotificationModel * GetJsonListEmpExpirationDegreeResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpExpirationDegreeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns4_ArrayOfNotificationModel * GetJsonListEmpExpirationDegreeResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyExpirationDegree : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyExpirationDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyExpirationDegreeResponse : NSObject {
	
/* elements */
	NSNumber * GetValueNotifyExpirationDegreeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyExpirationDegreeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetValueNotifyExpirationDegreeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyEmpExpirationContract : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyEmpExpirationContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyEmpExpirationContractResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpContract * GetJsonListNotifyEmpExpirationContractResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyEmpExpirationContractResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpContract * GetJsonListNotifyEmpExpirationContractResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpExpirationContract : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSString * fromDate;
	NSString * toDate;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpExpirationContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSString * fromDate;
@property (retain) NSString * toDate;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpExpirationContractResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpContract * GetJsonListEmpExpirationContractResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpExpirationContractResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpContract * GetJsonListEmpExpirationContractResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyExpirationContract : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyExpirationContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyExpirationContractResponse : NSObject {
	
/* elements */
	NSNumber * GetValueNotifyExpirationContractResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyExpirationContractResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetValueNotifyExpirationContractResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyEmpWaitReSignedContract : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyEmpWaitReSignedContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyEmpWaitReSignedContractResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpContract * GetJsonListNotifyEmpWaitReSignedContractResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyEmpWaitReSignedContractResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpContract * GetJsonListNotifyEmpWaitReSignedContractResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpWaitReSignedContract : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSString * fromDate;
	NSString * toDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpWaitReSignedContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSString * fromDate;
@property (retain) NSString * toDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpWaitReSignedContractResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpContract * GetJsonListEmpWaitReSignedContractResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpWaitReSignedContractResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpContract * GetJsonListEmpWaitReSignedContractResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyWaitResignedContract : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyWaitResignedContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyWaitResignedContractResponse : NSObject {
	
/* elements */
	NSNumber * GetValueNotifyWaitResignedContractResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyWaitResignedContractResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetValueNotifyWaitResignedContractResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyEmpLeave : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyEmpLeave *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyEmpLeaveResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_TimeRequestMasTer * GetJsonListNotifyEmpLeaveResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyEmpLeaveResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_TimeRequestMasTer * GetJsonListNotifyEmpLeaveResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpLeave : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSString * fromDate;
	NSString * toDate;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpLeave *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSString * fromDate;
@property (retain) NSString * toDate;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpLeaveResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_TimeRequestMasTer * GetJsonListEmpLeaveResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpLeaveResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_TimeRequestMasTer * GetJsonListEmpLeaveResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyEmpLeave : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyEmpLeave *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyEmpLeaveResponse : NSObject {
	
/* elements */
	NSNumber * GetValueNotifyEmpLeaveResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyEmpLeaveResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetValueNotifyEmpLeaveResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpMaterityLeave : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSString * fromDate;
	NSString * toDate;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpMaterityLeave *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSString * fromDate;
@property (retain) NSString * toDate;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpMaterityLeaveResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_TimeRequestMasTer * GetJsonListEmpMaterityLeaveResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpMaterityLeaveResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_TimeRequestMasTer * GetJsonListEmpMaterityLeaveResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpWillMaterityLeave : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSString * fromDate;
	NSString * toDate;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpWillMaterityLeave *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSString * fromDate;
@property (retain) NSString * toDate;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpWillMaterityLeaveResponse : NSObject {
	
/* elements */
	emp_tns4_ArrayOfLeaveNotificationModel * GetJsonListEmpWillMaterityLeaveResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpWillMaterityLeaveResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns4_ArrayOfLeaveNotificationModel * GetJsonListEmpWillMaterityLeaveResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpWillPregnancyMonth7thLeave : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSString * fromDate;
	NSString * toDate;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpWillPregnancyMonth7thLeave *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSString * fromDate;
@property (retain) NSString * toDate;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpWillPregnancyMonth7thLeaveResponse : NSObject {
	
/* elements */
	emp_tns4_ArrayOfLeaveNotificationModel * GetJsonListEmpWillPregnancyMonth7thLeaveResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpWillPregnancyMonth7thLeaveResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns4_ArrayOfLeaveNotificationModel * GetJsonListEmpWillPregnancyMonth7thLeaveResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpRaisingChildExpiration : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSString * fromDate;
	NSString * toDate;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpRaisingChildExpiration *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSString * fromDate;
@property (retain) NSString * toDate;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpRaisingChildExpirationResponse : NSObject {
	
/* elements */
	emp_tns4_ArrayOfLeaveNotificationModel * GetJsonListEmpRaisingChildExpirationResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpRaisingChildExpirationResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns4_ArrayOfLeaveNotificationModel * GetJsonListEmpRaisingChildExpirationResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpFamilyDependentExpiration : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSString * fromDate;
	NSString * toDate;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpFamilyDependentExpiration *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSString * fromDate;
@property (retain) NSString * toDate;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpFamilyDependentExpirationResponse : NSObject {
	
/* elements */
	emp_tns4_ArrayOfEmpFamilyDependentModel * GetJsonListEmpFamilyDependentExpirationResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpFamilyDependentExpirationResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns4_ArrayOfEmpFamilyDependentModel * GetJsonListEmpFamilyDependentExpirationResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyEmpWillChildBirth : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyEmpWillChildBirth *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyEmpWillChildBirthResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfTimeRequestMasTer * GetJsonListNotifyEmpWillChildBirthResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyEmpWillChildBirthResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfTimeRequestMasTer * GetJsonListNotifyEmpWillChildBirthResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpWillChildBirth : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpWillChildBirth *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpWillChildBirthResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfTimeRequestMasTer * GetJsonListEmpWillChildBirthResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpWillChildBirthResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfTimeRequestMasTer * GetJsonListEmpWillChildBirthResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyWillChildBirth : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyWillChildBirth *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyWillChildBirthResponse : NSObject {
	
/* elements */
	NSNumber * GetValueNotifyWillChildBirthResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyWillChildBirthResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetValueNotifyWillChildBirthResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyLongTermLeave : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyLongTermLeave *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyLongTermLeaveResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_TimeRequestMasTer * GetJsonListNotifyLongTermLeaveResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyLongTermLeaveResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_TimeRequestMasTer * GetJsonListNotifyLongTermLeaveResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListLongTermLeave : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListLongTermLeave *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListLongTermLeaveResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_TimeRequestMasTer * GetJsonListLongTermLeaveResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListLongTermLeaveResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_TimeRequestMasTer * GetJsonListLongTermLeaveResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyLongTermLeave : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyLongTermLeave *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyLongTermLeaveResponse : NSObject {
	
/* elements */
	NSNumber * GetValueNotifyLongTermLeaveResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyLongTermLeaveResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetValueNotifyLongTermLeaveResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyEmpWillRetire : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyEmpWillRetire *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyEmpWillRetireResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_Employee * GetJsonListNotifyEmpWillRetireResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyEmpWillRetireResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_Employee * GetJsonListNotifyEmpWillRetireResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpWillRetire : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSString * fromDate;
	NSString * toDate;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpWillRetire *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSString * fromDate;
@property (retain) NSString * toDate;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpWillRetireResponse : NSObject {
	
/* elements */
	emp_tns4_ArrayOfLeaveNotificationModel * GetJsonListEmpWillRetireResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpWillRetireResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns4_ArrayOfLeaveNotificationModel * GetJsonListEmpWillRetireResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetvalueNotifyWillRetire : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetvalueNotifyWillRetire *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetvalueNotifyWillRetireResponse : NSObject {
	
/* elements */
	NSNumber * GetvalueNotifyWillRetireResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetvalueNotifyWillRetireResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetvalueNotifyWillRetireResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueRetireAgeForMale : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueRetireAgeForMale *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueRetireAgeForMaleResponse : NSObject {
	
/* elements */
	NSNumber * GetValueRetireAgeForMaleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueRetireAgeForMaleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetValueRetireAgeForMaleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueRetireAgeForFamale : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueRetireAgeForFamale *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueRetireAgeForFamaleResponse : NSObject {
	
/* elements */
	NSNumber * GetValueRetireAgeForFamaleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueRetireAgeForFamaleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetValueRetireAgeForFamaleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyEmpWorkAfterChildBirth : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyEmpWorkAfterChildBirth *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListNotifyEmpWorkAfterChildBirthResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_TimeRequestMasTer * GetJsonListNotifyEmpWorkAfterChildBirthResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListNotifyEmpWorkAfterChildBirthResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_TimeRequestMasTer * GetJsonListNotifyEmpWorkAfterChildBirthResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpWorkAfterChildBirth : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpWorkAfterChildBirth *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpWorkAfterChildBirthResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_TimeRequestMasTer * GetJsonListEmpWorkAfterChildBirthResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpWorkAfterChildBirthResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_TimeRequestMasTer * GetJsonListEmpWorkAfterChildBirthResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyWorlAfterChildBirth : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyWorlAfterChildBirth *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetValueNotifyWorlAfterChildBirthResponse : NSObject {
	
/* elements */
	NSNumber * GetValueNotifyWorlAfterChildBirthResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetValueNotifyWorlAfterChildBirthResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetValueNotifyWorlAfterChildBirthResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpWorkAfterLeave : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSString * fromDate;
	NSString * toDate;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpWorkAfterLeave *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSString * fromDate;
@property (retain) NSString * toDate;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetJsonListEmpWorkAfterLeaveResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_TimeRequestMasTer * GetJsonListEmpWorkAfterLeaveResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetJsonListEmpWorkAfterLeaveResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_TimeRequestMasTer * GetJsonListEmpWorkAfterLeaveResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetEmpOffProfileById : NSObject {
	
/* elements */
	NSNumber * empOffProfileId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetEmpOffProfileById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * empOffProfileId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetEmpOffProfileByIdResponse : NSObject {
	
/* elements */
	emp_tns1_EmpOffProfile * GetEmpOffProfileByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetEmpOffProfileByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_EmpOffProfile * GetEmpOffProfileByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GETAllViewEmpOffProfile : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GETAllViewEmpOffProfile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GETAllViewEmpOffProfileResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpOffProfile * GETAllViewEmpOffProfileResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GETAllViewEmpOffProfileResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpOffProfile * GETAllViewEmpOffProfileResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpOffProfileById : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpOffProfileById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpOffProfileByIdResponse : NSObject {
	
/* elements */
	emp_tns1_V_EmpOffProfile * GetViewEmpOffProfileByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpOffProfileByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_V_EmpOffProfile * GetViewEmpOffProfileByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_AddEmpOffProfileByView : NSObject {
	
/* elements */
	emp_tns1_V_EmpOffProfile * viewEmpOffProfile;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_AddEmpOffProfileByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_V_EmpOffProfile * viewEmpOffProfile;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_AddEmpOffProfileByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddEmpOffProfileByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_AddEmpOffProfileByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddEmpOffProfileByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_UpdateEmpOffProfileByView : NSObject {
	
/* elements */
	emp_tns1_V_EmpOffProfile * viewEmpOffProfile;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_UpdateEmpOffProfileByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_V_EmpOffProfile * viewEmpOffProfile;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_UpdateEmpOffProfileByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateEmpOffProfileByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_UpdateEmpOffProfileByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateEmpOffProfileByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_DeleteEmpOffProfileById : NSObject {
	
/* elements */
	NSNumber * empOffProfileId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_DeleteEmpOffProfileById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * empOffProfileId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_DeleteEmpOffProfileByIdResponse : NSObject {
	
/* elements */
	USBoolean * DeleteEmpOffProfileByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_DeleteEmpOffProfileByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteEmpOffProfileByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogEquipment : NSObject {
	
/* elements */
	NSNumber * CompanyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogEquipment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogEquipmentResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogEquipment * GetAllCLogEquipmentResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogEquipmentResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogEquipment * GetAllCLogEquipmentResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogEquipmentType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogEquipmentType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogEquipmentTypeResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_ESS_CLOG_EQUIPMENT_TYPE * GetAllCLogEquipmentTypeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogEquipmentTypeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_ESS_CLOG_EQUIPMENT_TYPE * GetAllCLogEquipmentTypeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogAssetsType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogAssetsType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogAssetsTypeResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_ESS_CLOG_ASSETS_TYPE * GetAllCLogAssetsTypeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogAssetsTypeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_ESS_CLOG_ASSETS_TYPE * GetAllCLogAssetsTypeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEquipmentByEmployeeId : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEquipmentByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEquipmentByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpProfileEquipment * GetAllEquipmentByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEquipmentByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpProfileEquipment * GetAllEquipmentByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllAssetsByEmployeeId : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllAssetsByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllAssetsByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpProfileAssets * GetAllAssetsByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllAssetsByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpProfileAssets * GetAllAssetsByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllViewCLogEquipment : NSObject {
	
/* elements */
	NSNumber * CompanyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllViewCLogEquipment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllViewCLogEquipmentResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_ESS_CLOG_EQUIPMENT * GetAllViewCLogEquipmentResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllViewCLogEquipmentResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_ESS_CLOG_EQUIPMENT * GetAllViewCLogEquipmentResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllViewCLogAssets : NSObject {
	
/* elements */
	NSNumber * CompanyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllViewCLogAssets *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllViewCLogAssetsResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_ESS_CLOG_ASSETS * GetAllViewCLogAssetsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllViewCLogAssetsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_ESS_CLOG_ASSETS * GetAllViewCLogAssetsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllViewOrgUnitAssets : NSObject {
	
/* elements */
	NSNumber * CompanyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllViewOrgUnitAssets *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllViewOrgUnitAssetsResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_OrgUnitAssets * GetAllViewOrgUnitAssetsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllViewOrgUnitAssetsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_OrgUnitAssets * GetAllViewOrgUnitAssetsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllViewOrgUnitEquipment : NSObject {
	
/* elements */
	NSNumber * CompanyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllViewOrgUnitEquipment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllViewOrgUnitEquipmentResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_OrgUnitEquipment * GetAllViewOrgUnitEquipmentResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllViewOrgUnitEquipmentResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_OrgUnitEquipment * GetAllViewOrgUnitEquipmentResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmpHasAssetsByAssetsIdAndUnitId : NSObject {
	
/* elements */
	NSNumber * CLogEquipmentId;
	NSNumber * OrgUnitId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmpHasAssetsByAssetsIdAndUnitId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogEquipmentId;
@property (retain) NSNumber * OrgUnitId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmpHasAssetsByAssetsIdAndUnitIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpProfileAssets * GetAllEmpHasAssetsByAssetsIdAndUnitIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmpHasAssetsByAssetsIdAndUnitIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpProfileAssets * GetAllEmpHasAssetsByAssetsIdAndUnitIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmpHasEquipmentByEquipmentIdAndUnitId : NSObject {
	
/* elements */
	NSNumber * CLogEquipmentId;
	NSNumber * OrgUnitId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmpHasEquipmentByEquipmentIdAndUnitId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogEquipmentId;
@property (retain) NSNumber * OrgUnitId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmpHasEquipmentByEquipmentIdAndUnitIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpProfileEquipment * GetAllEmpHasEquipmentByEquipmentIdAndUnitIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmpHasEquipmentByEquipmentIdAndUnitIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpProfileEquipment * GetAllEmpHasEquipmentByEquipmentIdAndUnitIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmployee : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmployeeResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfEmployee * GetAllEmployeeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmployeeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfEmployee * GetAllEmployeeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCities : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCities *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCitiesResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogCity * GetAllCitiesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCitiesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogCity * GetAllCitiesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllDistricts : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllDistricts *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllDistrictsResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogDistrict * GetAllDistrictsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllDistrictsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogDistrict * GetAllDistrictsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCountries : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCountries *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCountriesResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogCountry * GetAllCountriesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCountriesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogCountry * GetAllCountriesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_CreateAddress : NSObject {
	
/* elements */
	emp_tns1_Address * address;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_CreateAddress *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_Address * address;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_CreateAddressResponse : NSObject {
	
/* elements */
	NSNumber * CreateAddressResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_CreateAddressResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CreateAddressResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetDirectReportAndChildById : NSObject {
	
/* elements */
	NSNumber * childId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetDirectReportAndChildById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * childId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetDirectReportAndChildByIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_ESS_ORG_DIRECT_REPORT * GetDirectReportAndChildByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetDirectReportAndChildByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_ESS_ORG_DIRECT_REPORT * GetDirectReportAndChildByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllDirectReport : NSObject {
	
/* elements */
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllDirectReport *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllDirectReportResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_ESS_ORG_DIRECT_REPORT * GetAllDirectReportResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllDirectReportResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_ESS_ORG_DIRECT_REPORT * GetAllDirectReportResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetEmployeeSearchResult : NSObject {
	
/* elements */
	NSString * searchInput;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetEmployeeSearchResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * searchInput;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetEmployeeSearchResultResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpBasicInformation * GetEmployeeSearchResultResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetEmployeeSearchResultResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpBasicInformation * GetEmployeeSearchResultResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetDirectReportByChildId : NSObject {
	
/* elements */
	NSNumber * childId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetDirectReportByChildId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * childId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetDirectReportByChildIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_ESS_ORG_DIRECT_REPORT * GetDirectReportByChildIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetDirectReportByChildIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_ESS_ORG_DIRECT_REPORT * GetDirectReportByChildIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetUserIdByEmployeeCode : NSObject {
	
/* elements */
	NSString * userCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetUserIdByEmployeeCode *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * userCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetUserIdByEmployeeCodeResponse : NSObject {
	
/* elements */
	emp_tns1_Employee * GetUserIdByEmployeeCodeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetUserIdByEmployeeCodeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_Employee * GetUserIdByEmployeeCodeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetLeaderOfOrg : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetLeaderOfOrg *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetLeaderOfOrgResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpBasicInformation * GetLeaderOfOrgResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetLeaderOfOrgResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpBasicInformation * GetLeaderOfOrgResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetInforOfEmps : NSObject {
	
/* elements */
	emp_tns3_ArrayOfint * empsId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetInforOfEmps *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns3_ArrayOfint * empsId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetInforOfEmpsResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfEmpProfileContact * GetInforOfEmpsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetInforOfEmpsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfEmpProfileContact * GetInforOfEmpsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetEmpInGender : NSObject {
	
/* elements */
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetEmpInGender *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetEmpInGenderResponse : NSObject {
	
/* elements */
	emp_tns4_ESS_EMP_IN_GENDER * GetEmpInGenderResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetEmpInGenderResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns4_ESS_EMP_IN_GENDER * GetEmpInGenderResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetNewestEmployee : NSObject {
	
/* elements */
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetNewestEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetNewestEmployeeResponse : NSObject {
	
/* elements */
	emp_tns1_V_EmpBasicInformation * GetNewestEmployeeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetNewestEmployeeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_V_EmpBasicInformation * GetNewestEmployeeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmpAssignedGroup : NSObject {
	
/* elements */
	emp_tns4_SearchingConditionsModel * conditions;
	NSNumber * take;
	NSNumber * skip;
	NSNumber * companyId;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmpAssignedGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns4_SearchingConditionsModel * conditions;
@property (retain) NSNumber * take;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmpAssignedGroupResponse : NSObject {
	
/* elements */
	emp_tns4_ArrayOfEmpGroupModel * GetAllEmpAssignedGroupResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmpAssignedGroupResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns4_ArrayOfEmpGroupModel * GetAllEmpAssignedGroupResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_AssignEmpToGroup : NSObject {
	
/* elements */
	emp_tns4_ArrayOfEmpGroupModel * lsEmpGroup;
	NSString * groupCd;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_AssignEmpToGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns4_ArrayOfEmpGroupModel * lsEmpGroup;
@property (retain) NSString * groupCd;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_AssignEmpToGroupResponse : NSObject {
	
/* elements */
	emp_tns4_ArrayOfEmpGroupModel * AssignEmpToGroupResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_AssignEmpToGroupResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns4_ArrayOfEmpGroupModel * AssignEmpToGroupResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_RevokeEmpFromGroup : NSObject {
	
/* elements */
	emp_tns4_ArrayOfEmpGroupModel * lsEmp;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_RevokeEmpFromGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns4_ArrayOfEmpGroupModel * lsEmp;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_RevokeEmpFromGroupResponse : NSObject {
	
/* elements */
	NSString * RevokeEmpFromGroupResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_RevokeEmpFromGroupResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * RevokeEmpFromGroupResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetDirectReportOfEmployeeByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetDirectReportOfEmployeeByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetDirectReportOfEmployeeByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_V_ESS_ORG_DIRECT_REPORT * GetDirectReportOfEmployeeByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetDirectReportOfEmployeeByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_V_ESS_ORG_DIRECT_REPORT * GetDirectReportOfEmployeeByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllDirectReportOfEmpByEmpId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllDirectReportOfEmpByEmpId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllDirectReportOfEmpByEmpIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_Employee * GetAllDirectReportOfEmpByEmpIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllDirectReportOfEmpByEmpIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_Employee * GetAllDirectReportOfEmpByEmpIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpBasicInformationById : NSObject {
	
/* elements */
	NSNumber * id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpBasicInformationById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpBasicInformationByIdResponse : NSObject {
	
/* elements */
	emp_tns1_V_EmpBasicInformation * GetViewEmpBasicInformationByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpBasicInformationByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_V_EmpBasicInformation * GetViewEmpBasicInformationByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllViewEmployeesByUser : NSObject {
	
/* elements */
	NSNumber * userId;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllViewEmployeesByUser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * userId;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllViewEmployeesByUserResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfSP_EMP_GET_VIEW_EMPLOYEE_Result * GetAllViewEmployeesByUserResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllViewEmployeesByUserResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfSP_EMP_GET_VIEW_EMPLOYEE_Result * GetAllViewEmployeesByUserResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmployeesByUser : NSObject {
	
/* elements */
	NSNumber * userId;
	NSNumber * companyId;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmployeesByUser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * userId;
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmployeesByUserResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfSP_EMP_GET_VIEW_EMPLOYEE_Result * GetViewEmployeesByUserResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmployeesByUserResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfSP_EMP_GET_VIEW_EMPLOYEE_Result * GetViewEmployeesByUserResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllViewEmployees : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllViewEmployees *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllViewEmployeesResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_Employee * GetAllViewEmployeesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllViewEmployeesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_Employee * GetAllViewEmployeesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmployeesByPage : NSObject {
	
/* elements */
	NSNumber * pageIndex;
	NSNumber * pageSize;
	NSNumber * CompanyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmployeesByPage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * pageIndex;
@property (retain) NSNumber * pageSize;
@property (retain) NSNumber * CompanyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmployeesByPageResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_Employee * GetViewEmployeesByPageResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmployeesByPageResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_Employee * GetViewEmployeesByPageResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmployeePageNumber : NSObject {
	
/* elements */
	NSNumber * pageSize;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmployeePageNumber *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * pageSize;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmployeePageNumberResponse : NSObject {
	
/* elements */
	NSNumber * GetViewEmployeePageNumberResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmployeePageNumberResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetViewEmployeePageNumberResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmployeeById : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmployeeById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmployeeByIdResponse : NSObject {
	
/* elements */
	emp_tns1_V_Employee * GetViewEmployeeByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmployeeByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_V_Employee * GetViewEmployeeByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmployeeProfileByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmployeeProfileByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmployeeProfileByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_V_EmployeeProfile * GetViewEmployeeProfileByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmployeeProfileByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_V_EmployeeProfile * GetViewEmployeeProfileByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmployees : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmployees *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmployeesResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfEmployee * GetAllEmployeesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmployeesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfEmployee * GetAllEmployeesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetEmployeeById : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetEmployeeById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetEmployeeByIdResponse : NSObject {
	
/* elements */
	emp_tns1_Employee * GetEmployeeByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetEmployeeByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_Employee * GetEmployeeByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_SubmitCreateNewEmployee : NSObject {
	
/* elements */
	NSNumber * EmployeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_SubmitCreateNewEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EmployeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_SubmitCreateNewEmployeeResponse : NSObject {
	
/* elements */
	emp_tns4_MsgNotifyModel * SubmitCreateNewEmployeeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_SubmitCreateNewEmployeeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns4_MsgNotifyModel * SubmitCreateNewEmployeeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_CheckValidEmpCode : NSObject {
	
/* elements */
	NSString * EmployeeCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_CheckValidEmpCode *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * EmployeeCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_CheckValidEmpCodeResponse : NSObject {
	
/* elements */
	emp_tns4_MsgNotifyModel * CheckValidEmpCodeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_CheckValidEmpCodeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns4_MsgNotifyModel * CheckValidEmpCodeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_CreateEmployee : NSObject {
	
/* elements */
	emp_tns1_Employee * employee;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_CreateEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_Employee * employee;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_CreateEmployeeResponse : NSObject {
	
/* elements */
	emp_tns1_Employee * CreateEmployeeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_CreateEmployeeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_Employee * CreateEmployeeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllLeader : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllLeader *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllLeaderResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfSP_GetAllParentEmployee_Result * GetAllLeaderResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllLeaderResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfSP_GetAllParentEmployee_Result * GetAllLeaderResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_CreateEmpProfileJobPosition : NSObject {
	
/* elements */
	emp_tns1_EmpProfileJobPosition * empProfileJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_CreateEmpProfileJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_EmpProfileJobPosition * empProfileJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_CreateEmpProfileJobPositionResponse : NSObject {
	
/* elements */
	emp_tns1_EmpProfileJobPosition * CreateEmpProfileJobPositionResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_CreateEmpProfileJobPositionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_EmpProfileJobPosition * CreateEmpProfileJobPositionResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_CreateEmpProfileProcessOfWork : NSObject {
	
/* elements */
	emp_tns1_EmpProfileProcessOfWork * empProfileProcessOfWork;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_CreateEmpProfileProcessOfWork *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_EmpProfileProcessOfWork * empProfileProcessOfWork;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_CreateEmpProfileProcessOfWorkResponse : NSObject {
	
/* elements */
	emp_tns1_EmpProfileProcessOfWork * CreateEmpProfileProcessOfWorkResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_CreateEmpProfileProcessOfWorkResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_EmpProfileProcessOfWork * CreateEmpProfileProcessOfWorkResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_CreateEmpBasicProfile : NSObject {
	
/* elements */
	emp_tns1_EmpBasicProfile * empBasicProfile;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_CreateEmpBasicProfile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_EmpBasicProfile * empBasicProfile;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_CreateEmpBasicProfileResponse : NSObject {
	
/* elements */
	emp_tns1_EmpBasicProfile * CreateEmpBasicProfileResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_CreateEmpBasicProfileResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_EmpBasicProfile * CreateEmpBasicProfileResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpBasicProfilesByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpBasicProfilesByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpBasicProfilesByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpBasicProfile * GetViewEmpBasicProfilesByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpBasicProfilesByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpBasicProfile * GetViewEmpBasicProfilesByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogFamilyRelationships : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogFamilyRelationships *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogFamilyRelationshipsResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogFamilyRelationship * GetAllCLogFamilyRelationshipsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogFamilyRelationshipsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogFamilyRelationship * GetAllCLogFamilyRelationshipsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileWorkingExperiencesByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileWorkingExperiencesByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileWorkingExperiencesByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpProfileWorkingExperience * GetViewEmpProfileWorkingExperiencesByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileWorkingExperiencesByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpProfileWorkingExperience * GetViewEmpProfileWorkingExperiencesByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileSkillsByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileSkillsByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileSkillsByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpProfileSkill * GetViewEmpProfileSkillsByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileSkillsByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpProfileSkill * GetViewEmpProfileSkillsByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileJobPositionsByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileJobPositionsByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileJobPositionsByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpProfileJobPosition * GetViewEmpProfileJobPositionsByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileJobPositionsByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpProfileJobPosition * GetViewEmpProfileJobPositionsByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpContractsByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpContractsByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpContractsByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpContract * GetViewEmpContractsByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpContractsByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpContract * GetViewEmpContractsByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileBasicSalarysByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileBasicSalarysByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileBasicSalarysByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpProfileBasicSalary * GetViewEmpProfileBasicSalarysByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileBasicSalarysByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpProfileBasicSalary * GetViewEmpProfileBasicSalarysByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileHealthInsurancesByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileHealthInsurancesByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileHealthInsurancesByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpProfileHealthInsurance * GetViewEmpProfileHealthInsurancesByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileHealthInsurancesByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpProfileHealthInsurance * GetViewEmpProfileHealthInsurancesByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileWageTypesByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileWageTypesByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileWageTypesByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpProfileWageType * GetViewEmpProfileWageTypesByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileWageTypesByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpProfileWageType * GetViewEmpProfileWageTypesByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileWorkingFormsByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileWorkingFormsByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileWorkingFormsByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpProfileWorkingForm * GetViewEmpProfileWorkingFormsByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileWorkingFormsByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpProfileWorkingForm * GetViewEmpProfileWorkingFormsByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpSocialInsuranceSalariesByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpSocialInsuranceSalariesByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpSocialInsuranceSalariesByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpSocialInsuranceSalary * GetViewEmpSocialInsuranceSalariesByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpSocialInsuranceSalariesByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpSocialInsuranceSalary * GetViewEmpSocialInsuranceSalariesByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileBiographiesByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileBiographiesByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileBiographiesByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfEmpProfileBiography * GetViewEmpProfileBiographiesByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileBiographiesByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfEmpProfileBiography * GetViewEmpProfileBiographiesByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileExperiencesByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileExperiencesByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileExperiencesByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfEmpProfileExperience * GetViewEmpProfileExperiencesByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileExperiencesByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfEmpProfileExperience * GetViewEmpProfileExperiencesByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileProcessOfWorksByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileProcessOfWorksByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileProcessOfWorksByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfEmpProfileProcessOfWork * GetViewEmpProfileProcessOfWorksByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileProcessOfWorksByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfEmpProfileProcessOfWork * GetViewEmpProfileProcessOfWorksByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileContactsByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileContactsByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileContactsByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfEmpProfileContact * GetViewEmpProfileContactsByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileContactsByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfEmpProfileContact * GetViewEmpProfileContactsByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileSocialInsurancesByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileSocialInsurancesByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileSocialInsurancesByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfEmpProfileSocialInsurance * GetViewEmpProfileSocialInsurancesByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileSocialInsurancesByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfEmpProfileSocialInsurance * GetViewEmpProfileSocialInsurancesByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileLeaveRegimesByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileLeaveRegimesByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileLeaveRegimesByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfEmpProfileLeaveRegime * GetViewEmpProfileLeaveRegimesByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileLeaveRegimesByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfEmpProfileLeaveRegime * GetViewEmpProfileLeaveRegimesByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpPerformanceAppraisalsByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpPerformanceAppraisalsByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpPerformanceAppraisalsByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpPerformanceAppraisal * GetViewEmpPerformanceAppraisalsByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpPerformanceAppraisalsByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpPerformanceAppraisal * GetViewEmpPerformanceAppraisalsByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileQualificationsByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileQualificationsByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileQualificationsByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpProfileQualification * GetViewEmpProfileQualificationsByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileQualificationsByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpProfileQualification * GetViewEmpProfileQualificationsByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileForeignLanguagesByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileForeignLanguagesByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileForeignLanguagesByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpProfileForeignLanguage * GetViewEmpProfileForeignLanguagesByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileForeignLanguagesByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpProfileForeignLanguage * GetViewEmpProfileForeignLanguagesByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileTrainingsByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileTrainingsByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileTrainingsByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfEmpProfileTraining * GetViewEmpProfileTrainingsByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileTrainingsByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfEmpProfileTraining * GetViewEmpProfileTrainingsByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileDegreesByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileDegreesByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileDegreesByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpProfileDegree * GetViewEmpProfileDegreesByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileDegreesByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpProfileDegree * GetViewEmpProfileDegreesByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileDisciplinesByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileDisciplinesByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmpProfileDisciplinesByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpProfileDiscipline * GetViewEmpProfileDisciplinesByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmpProfileDisciplinesByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpProfileDiscipline * GetViewEmpProfileDisciplinesByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmEmpProfilePersonalitiesByEmployeeId : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmEmpProfilePersonalitiesByEmployeeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewEmEmpProfilePersonalitiesByEmployeeIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpProfilePersonality * GetViewEmEmpProfilePersonalitiesByEmployeeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewEmEmpProfilePersonalitiesByEmployeeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpProfilePersonality * GetViewEmEmpProfilePersonalitiesByEmployeeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllTimeWorkingForms : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllTimeWorkingForms *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllTimeWorkingFormsResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfTimeWorkingForm * GetAllTimeWorkingFormsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllTimeWorkingFormsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfTimeWorkingForm * GetAllTimeWorkingFormsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllTimeWageTypes : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllTimeWageTypes *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllTimeWageTypesResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfTimeWageType * GetAllTimeWageTypesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllTimeWageTypesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfTimeWageType * GetAllTimeWageTypesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetTimeWageTypeById : NSObject {
	
/* elements */
	NSNumber * timeWageTypeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetTimeWageTypeById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * timeWageTypeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetTimeWageTypeByIdResponse : NSObject {
	
/* elements */
	emp_tns1_TimeWageType * GetTimeWageTypeByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetTimeWageTypeByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_TimeWageType * GetTimeWageTypeByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogCBCompensationCategories : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogCBCompensationCategories *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogCBCompensationCategoriesResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogCBCompensationCategory * GetAllCLogCBCompensationCategoriesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogCBCompensationCategoriesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogCBCompensationCategory * GetAllCLogCBCompensationCategoriesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogCitiesByCLogCountryId : NSObject {
	
/* elements */
	NSNumber * cLogCountryId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogCitiesByCLogCountryId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * cLogCountryId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogCitiesByCLogCountryIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogCity * GetCLogCitiesByCLogCountryIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogCitiesByCLogCountryIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogCity * GetCLogCitiesByCLogCountryIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogCities : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogCities *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogCitiesResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogCity * GetAllCLogCitiesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogCitiesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogCity * GetAllCLogCitiesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogCityById : NSObject {
	
/* elements */
	NSNumber * clogCityId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogCityById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * clogCityId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogCityByIdResponse : NSObject {
	
/* elements */
	emp_tns1_CLogCity * GetCLogCityByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogCityByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogCity * GetCLogCityByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogCurrencies : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogCurrencies *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogCurrenciesResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogCurrency * GetAllCLogCurrenciesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogCurrenciesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogCurrency * GetAllCLogCurrenciesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogCurrencyById : NSObject {
	
/* elements */
	NSNumber * currencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogCurrencyById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * currencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogCurrencyByIdResponse : NSObject {
	
/* elements */
	emp_tns1_CLogCurrency * GetCLogCurrencyByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogCurrencyByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogCurrency * GetCLogCurrencyByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogCurrencyRates : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogCurrencyRates *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogCurrencyRatesResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogCurrencyRate * GetAllCLogCurrencyRatesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogCurrencyRatesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogCurrencyRate * GetAllCLogCurrencyRatesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogCurrencyRateById : NSObject {
	
/* elements */
	NSNumber * currencyRateId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogCurrencyRateById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * currencyRateId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogCurrencyRateByIdResponse : NSObject {
	
/* elements */
	emp_tns1_CLogCurrencyRate * GetCLogCurrencyRateByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogCurrencyRateByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogCurrencyRate * GetCLogCurrencyRateByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllSysEmpProfileGroupLayers : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllSysEmpProfileGroupLayers *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllSysEmpProfileGroupLayersResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfSysEmpProfileGroupLayer * GetAllSysEmpProfileGroupLayersResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllSysEmpProfileGroupLayersResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfSysEmpProfileGroupLayer * GetAllSysEmpProfileGroupLayersResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetSysEmpProfileLayersByGroupId : NSObject {
	
/* elements */
	NSNumber * groupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetSysEmpProfileLayersByGroupId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * groupId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetSysEmpProfileLayersByGroupIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfSysEmpProfileLayer * GetSysEmpProfileLayersByGroupIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetSysEmpProfileLayersByGroupIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfSysEmpProfileLayer * GetSysEmpProfileLayersByGroupIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetSysEmpProfileLayerUrlsByLayerId : NSObject {
	
/* elements */
	NSNumber * layerId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetSysEmpProfileLayerUrlsByLayerId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * layerId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetSysEmpProfileLayerUrlsByLayerIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfSysEmpProfileLayerUrl * GetSysEmpProfileLayerUrlsByLayerIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetSysEmpProfileLayerUrlsByLayerIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfSysEmpProfileLayerUrl * GetSysEmpProfileLayerUrlsByLayerIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetSysEmpProfileLayerUrlsByTypeByLayerId : NSObject {
	
/* elements */
	NSString * typeName;
	NSNumber * layerId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetSysEmpProfileLayerUrlsByTypeByLayerId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * typeName;
@property (retain) NSNumber * layerId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetSysEmpProfileLayerUrlsByTypeByLayerIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfSysEmpProfileLayerUrl * GetSysEmpProfileLayerUrlsByTypeByLayerIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetSysEmpProfileLayerUrlsByTypeByLayerIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfSysEmpProfileLayerUrl * GetSysEmpProfileLayerUrlsByTypeByLayerIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewSysEmpProfileLayerByTypeByLayerId : NSObject {
	
/* elements */
	NSString * typeName;
	NSNumber * layerId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewSysEmpProfileLayerByTypeByLayerId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * typeName;
@property (retain) NSNumber * layerId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewSysEmpProfileLayerByTypeByLayerIdResponse : NSObject {
	
/* elements */
	emp_tns1_V_SysEmpProfileLayer * GetViewSysEmpProfileLayerByTypeByLayerIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewSysEmpProfileLayerByTypeByLayerIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_V_SysEmpProfileLayer * GetViewSysEmpProfileLayerByTypeByLayerIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogEmployeeTypes : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogEmployeeTypes *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogEmployeeTypesResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogEmployeeType * GetAllCLogEmployeeTypesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogEmployeeTypesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogEmployeeType * GetAllCLogEmployeeTypesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogEmployeeTypeById : NSObject {
	
/* elements */
	NSNumber * id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogEmployeeTypeById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogEmployeeTypeByIdResponse : NSObject {
	
/* elements */
	emp_tns1_CLogEmployeeType * GetCLogEmployeeTypeByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogEmployeeTypeByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogEmployeeType * GetCLogEmployeeTypeByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogMaritalStatuses : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogMaritalStatuses *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogMaritalStatusesResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogMaritalStatu * GetAllCLogMaritalStatusesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogMaritalStatusesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogMaritalStatu * GetAllCLogMaritalStatusesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogCountries : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogCountries *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogCountriesResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogCountry * GetAllCLogCountriesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogCountriesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogCountry * GetAllCLogCountriesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogCountryById : NSObject {
	
/* elements */
	NSNumber * id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogCountryById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogCountryByIdResponse : NSObject {
	
/* elements */
	emp_tns1_CLogCountry * GetCLogCountryByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogCountryByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogCountry * GetCLogCountryByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllOrgUnits : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllOrgUnits *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllOrgUnitsResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_OrgUnitTree * GetAllOrgUnitsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllOrgUnitsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_OrgUnitTree * GetAllOrgUnitsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewOrgUnitJobsByOrgUnitId : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewOrgUnitJobsByOrgUnitId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetViewOrgUnitJobsByOrgUnitIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_OrgUnitJob * GetViewOrgUnitJobsByOrgUnitIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetViewOrgUnitJobsByOrgUnitIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_OrgUnitJob * GetViewOrgUnitJobsByOrgUnitIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetOrgWorkLevelsByOrgJobId : NSObject {
	
/* elements */
	NSNumber * orgJobId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetOrgWorkLevelsByOrgJobId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgJobId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetOrgWorkLevelsByOrgJobIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfOrgWorkLevel * GetOrgWorkLevelsByOrgJobIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetOrgWorkLevelsByOrgJobIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfOrgWorkLevel * GetOrgWorkLevelsByOrgJobIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogCareers : NSObject {
	
/* elements */
	NSNumber * CompanyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogCareers *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogCareersResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_ESS_CLOG_CAREER * GetAllCLogCareersResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogCareersResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_ESS_CLOG_CAREER * GetAllCLogCareersResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetOrgJobPositionById : NSObject {
	
/* elements */
	NSNumber * id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetOrgJobPositionById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetOrgJobPositionByIdResponse : NSObject {
	
/* elements */
	emp_tns1_OrgJobPosition * GetOrgJobPositionByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetOrgJobPositionByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_OrgJobPosition * GetOrgJobPositionByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetOrgJobPositionsByOrgUnitId : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetOrgJobPositionsByOrgUnitId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetOrgJobPositionsByOrgUnitIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfOrgJobPosition * GetOrgJobPositionsByOrgUnitIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetOrgJobPositionsByOrgUnitIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfOrgJobPosition * GetOrgJobPositionsByOrgUnitIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetOrgJobPositionsByOrgUnitIdAndOrgJobId : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSNumber * orgJobId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetOrgJobPositionsByOrgUnitIdAndOrgJobId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSNumber * orgJobId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetOrgJobPositionsByOrgUnitIdAndOrgJobIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfOrgJobPosition * GetOrgJobPositionsByOrgUnitIdAndOrgJobIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetOrgJobPositionsByOrgUnitIdAndOrgJobIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfOrgJobPosition * GetOrgJobPositionsByOrgUnitIdAndOrgJobIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogRatings : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogRatings *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogRatingsResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogRating * GetAllCLogRatingsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogRatingsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogRating * GetAllCLogRatingsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogDegrees : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogDegrees *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogDegreesResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogDegree * GetAllCLogDegreesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogDegreesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogDegree * GetAllCLogDegreesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogMajors : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogMajors *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogMajorsResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogMajor * GetAllCLogMajorsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogMajorsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogMajor * GetAllCLogMajorsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogMajorById : NSObject {
	
/* elements */
	NSNumber * id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogMajorById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogMajorByIdResponse : NSObject {
	
/* elements */
	emp_tns1_CLogMajor * GetCLogMajorByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogMajorByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogMajor * GetCLogMajorByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogTrainingCenters : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogTrainingCenters *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogTrainingCentersResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogTrainingCenter * GetAllCLogTrainingCentersResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogTrainingCentersResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogTrainingCenter * GetAllCLogTrainingCentersResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogTrainings : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogTrainings *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogTrainingsResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogTraining * GetAllCLogTrainingsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogTrainingsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogTraining * GetAllCLogTrainingsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllOrgDegrees : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllOrgDegrees *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllOrgDegreesResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfOrgDegree * GetAllOrgDegreesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllOrgDegreesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfOrgDegree * GetAllOrgDegreesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetOrgDegreeById : NSObject {
	
/* elements */
	NSNumber * id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetOrgDegreeById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetOrgDegreeByIdResponse : NSObject {
	
/* elements */
	emp_tns1_OrgDegree * GetOrgDegreeByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetOrgDegreeByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_OrgDegree * GetOrgDegreeByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllOrgDegreeRanks : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllOrgDegreeRanks *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllOrgDegreeRanksResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfOrgDegreeRank * GetAllOrgDegreeRanksResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllOrgDegreeRanksResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfOrgDegreeRank * GetAllOrgDegreeRanksResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogHospitals : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogHospitals *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogHospitalsResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogHospital * GetAllCLogHospitalsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogHospitalsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogHospital * GetAllCLogHospitalsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogHospitalById : NSObject {
	
/* elements */
	NSNumber * hospitalId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogHospitalById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * hospitalId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogHospitalByIdResponse : NSObject {
	
/* elements */
	emp_tns1_CLogHospital * GetCLogHospitalByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogHospitalByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogHospital * GetCLogHospitalByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogEthnicities : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogEthnicities *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogEthnicitiesResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogEthnicity * GetAllCLogEthnicitiesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogEthnicitiesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogEthnicity * GetAllCLogEthnicitiesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogEthnicityById : NSObject {
	
/* elements */
	NSNumber * id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogEthnicityById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogEthnicityByIdResponse : NSObject {
	
/* elements */
	emp_tns1_CLogEthnicity * GetCLogEthnicityByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogEthnicityByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogEthnicity * GetCLogEthnicityByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogReligions : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogReligions *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogReligionsResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogReligion * GetAllCLogReligionsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogReligionsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogReligion * GetAllCLogReligionsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogReligionById : NSObject {
	
/* elements */
	NSNumber * id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogReligionById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogReligionByIdResponse : NSObject {
	
/* elements */
	emp_tns1_CLogReligion * GetCLogReligionByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogReligionByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogReligion * GetCLogReligionByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogLanguages : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogLanguages *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogLanguagesResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogLanguage * GetAllCLogLanguagesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogLanguagesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogLanguage * GetAllCLogLanguagesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogLanguageById : NSObject {
	
/* elements */
	NSNumber * id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogLanguageById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogLanguageByIdResponse : NSObject {
	
/* elements */
	emp_tns1_CLogLanguage * GetCLogLanguageByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogLanguageByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogLanguage * GetCLogLanguageByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogEducationLevels : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogEducationLevels *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogEducationLevelsResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogEducationLevel * GetAllCLogEducationLevelsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogEducationLevelsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogEducationLevel * GetAllCLogEducationLevelsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogEducationLevelById : NSObject {
	
/* elements */
	NSNumber * id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogEducationLevelById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogEducationLevelByIdResponse : NSObject {
	
/* elements */
	emp_tns1_CLogEducationLevel * GetCLogEducationLevelByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogEducationLevelByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogEducationLevel * GetCLogEducationLevelByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllOrgQualifications : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllOrgQualifications *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllOrgQualificationsResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfOrgQualification * GetAllOrgQualificationsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllOrgQualificationsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfOrgQualification * GetAllOrgQualificationsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogEmpCulturalLevels : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogEmpCulturalLevels *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogEmpCulturalLevelsResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogEmpCulturalLevel * GetAllCLogEmpCulturalLevelsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogEmpCulturalLevelsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogEmpCulturalLevel * GetAllCLogEmpCulturalLevelsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogEmpCulturalLevelById : NSObject {
	
/* elements */
	NSNumber * id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogEmpCulturalLevelById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogEmpCulturalLevelByIdResponse : NSObject {
	
/* elements */
	emp_tns1_CLogEmpCulturalLevel * GetCLogEmpCulturalLevelByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogEmpCulturalLevelByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogEmpCulturalLevel * GetCLogEmpCulturalLevelByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogMaritalStatus : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogMaritalStatus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogMaritalStatusResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogMaritalStatu * GetAllCLogMaritalStatusResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogMaritalStatusResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogMaritalStatu * GetAllCLogMaritalStatusResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogMaritalStatusById : NSObject {
	
/* elements */
	NSNumber * id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogMaritalStatusById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogMaritalStatusByIdResponse : NSObject {
	
/* elements */
	emp_tns1_CLogMaritalStatu * GetCLogMaritalStatusByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogMaritalStatusByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogMaritalStatu * GetCLogMaritalStatusByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogBanks : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogBanks *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogBanksResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogBank * GetAllCLogBanksResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogBanksResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogBank * GetAllCLogBanksResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogBankByCLogBankBranchId : NSObject {
	
/* elements */
	NSNumber * id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogBankByCLogBankBranchId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogBankByCLogBankBranchIdResponse : NSObject {
	
/* elements */
	emp_tns1_CLogBank * GetCLogBankByCLogBankBranchIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogBankByCLogBankBranchIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogBank * GetCLogBankByCLogBankBranchIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogBankBranchs : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogBankBranchs *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogBankBranchsResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogBankBranch * GetAllCLogBankBranchsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogBankBranchsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogBankBranch * GetAllCLogBankBranchsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogBankBranchById : NSObject {
	
/* elements */
	NSNumber * id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogBankBranchById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogBankBranchByIdResponse : NSObject {
	
/* elements */
	emp_tns1_CLogBankBranch * GetCLogBankBranchByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogBankBranchByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogBankBranch * GetCLogBankBranchByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogBankBranchesByCLogBankId : NSObject {
	
/* elements */
	NSNumber * cLogBankId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogBankBranchesByCLogBankId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * cLogBankId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogBankBranchesByCLogBankIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogBankBranch * GetCLogBankBranchesByCLogBankIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogBankBranchesByCLogBankIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogBankBranch * GetCLogBankBranchesByCLogBankIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogCourseStatus : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogCourseStatus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogCourseStatusResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogCourseStatu * GetAllCLogCourseStatusResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogCourseStatusResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogCourseStatu * GetAllCLogCourseStatusResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogCourseStatusById : NSObject {
	
/* elements */
	NSNumber * courseStatusId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogCourseStatusById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * courseStatusId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogCourseStatusByIdResponse : NSObject {
	
/* elements */
	emp_tns1_CLogCourseStatu * GetCLogCourseStatusByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogCourseStatusByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogCourseStatu * GetCLogCourseStatusByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogEmpWorkingStatus : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogEmpWorkingStatus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogEmpWorkingStatusResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogEmpWorkingStatu * GetAllCLogEmpWorkingStatusResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogEmpWorkingStatusResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogEmpWorkingStatu * GetAllCLogEmpWorkingStatusResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogEmpWorkingStatusById : NSObject {
	
/* elements */
	NSNumber * id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogEmpWorkingStatusById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogEmpWorkingStatusByIdResponse : NSObject {
	
/* elements */
	emp_tns1_CLogEmpWorkingStatu * GetCLogEmpWorkingStatusByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogEmpWorkingStatusByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogEmpWorkingStatu * GetCLogEmpWorkingStatusByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogPersonalities : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogPersonalities *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogPersonalitiesResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogPersonality * GetAllCLogPersonalitiesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogPersonalitiesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogPersonality * GetAllCLogPersonalitiesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogCLogPersonalityById : NSObject {
	
/* elements */
	NSNumber * id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogCLogPersonalityById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogCLogPersonalityByIdResponse : NSObject {
	
/* elements */
	emp_tns1_CLogPersonality * GetCLogCLogPersonalityByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogCLogPersonalityByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogPersonality * GetCLogCLogPersonalityByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllOrgProjectTypes : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllOrgProjectTypes *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllOrgProjectTypesResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfOrgProjectType * GetAllOrgProjectTypesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllOrgProjectTypesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfOrgProjectType * GetAllOrgProjectTypesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetOrgProjectTypeById : NSObject {
	
/* elements */
	NSNumber * id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetOrgProjectTypeById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetOrgProjectTypeByIdResponse : NSObject {
	
/* elements */
	emp_tns1_OrgProjectType * GetOrgProjectTypeByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetOrgProjectTypeByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_OrgProjectType * GetOrgProjectTypeByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllOrgTimeInCharges : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllOrgTimeInCharges *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllOrgTimeInChargesResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfOrgTimeInCharge * GetAllOrgTimeInChargesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllOrgTimeInChargesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfOrgTimeInCharge * GetAllOrgTimeInChargesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetOrgTimeInChargeById : NSObject {
	
/* elements */
	NSNumber * id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetOrgTimeInChargeById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetOrgTimeInChargeByIdResponse : NSObject {
	
/* elements */
	emp_tns1_OrgTimeInCharge * GetOrgTimeInChargeByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetOrgTimeInChargeByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_OrgTimeInCharge * GetOrgTimeInChargeByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllOrgSkills : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllOrgSkills *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllOrgSkillsResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfOrgSkill * GetAllOrgSkillsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllOrgSkillsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfOrgSkill * GetAllOrgSkillsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetOrgSkillById : NSObject {
	
/* elements */
	NSNumber * id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetOrgSkillById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetOrgSkillByIdResponse : NSObject {
	
/* elements */
	emp_tns1_OrgSkill * GetOrgSkillByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetOrgSkillByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_OrgSkill * GetOrgSkillByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllOrgSkillTypes : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllOrgSkillTypes *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllOrgSkillTypesResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfOrgSkillType * GetAllOrgSkillTypesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllOrgSkillTypesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfOrgSkillType * GetAllOrgSkillTypesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetOrgSkillTypeById : NSObject {
	
/* elements */
	NSNumber * id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetOrgSkillTypeById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetOrgSkillTypeByIdResponse : NSObject {
	
/* elements */
	emp_tns1_OrgSkillType * GetOrgSkillTypeByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetOrgSkillTypeByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_OrgSkillType * GetOrgSkillTypeByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogCBFactors : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogCBFactors *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogCBFactorsResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogCBFactor * GetAllCLogCBFactorsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogCBFactorsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogCBFactor * GetAllCLogCBFactorsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogCBFactorById : NSObject {
	
/* elements */
	NSNumber * id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogCBFactorById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetCLogCBFactorByIdResponse : NSObject {
	
/* elements */
	emp_tns1_CLogCBFactor * GetCLogCBFactorByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetCLogCBFactorByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogCBFactor * GetCLogCBFactorByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllViewCLogCBFactors : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllViewCLogCBFactors *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllViewCLogCBFactorsResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_CLogCBFactor * GetAllViewCLogCBFactorsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllViewCLogCBFactorsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_CLogCBFactor * GetAllViewCLogCBFactorsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogEmpOffTypes : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogEmpOffTypes *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllCLogEmpOffTypesResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogEmpOffType * GetAllCLogEmpOffTypesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllCLogEmpOffTypesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogEmpOffType * GetAllCLogEmpOffTypesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllOrgKnowledge : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllOrgKnowledge *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllOrgKnowledgeResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfOrgKnowledge * GetAllOrgKnowledgeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllOrgKnowledgeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfOrgKnowledge * GetAllOrgKnowledgeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllOrgKnowledgeByMajorId : NSObject {
	
/* elements */
	NSNumber * majorId;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllOrgKnowledgeByMajorId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * majorId;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllOrgKnowledgeByMajorIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfOrgKnowledge * GetAllOrgKnowledgeByMajorIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllOrgKnowledgeByMajorIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfOrgKnowledge * GetAllOrgKnowledgeByMajorIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllOrgUnitByUser : NSObject {
	
/* elements */
	NSNumber * userId;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllOrgUnitByUser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * userId;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllOrgUnitByUserResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfSP_ORG_UNIT_TREE_BY_USER_Result * GetAllOrgUnitByUserResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllOrgUnitByUserResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfSP_ORG_UNIT_TREE_BY_USER_Result * GetAllOrgUnitByUserResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmpBasicInfomationByOrgUnitIds : NSObject {
	
/* elements */
	emp_tns3_ArrayOfint * orgUnitIds;
	NSNumber * take;
	NSNumber * skip;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmpBasicInfomationByOrgUnitIds *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns3_ArrayOfint * orgUnitIds;
@property (retain) NSNumber * take;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllEmpBasicInfomationByOrgUnitIdsResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpBasicInformation * GetAllEmpBasicInfomationByOrgUnitIdsResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllEmpBasicInfomationByOrgUnitIdsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpBasicInformation * GetAllEmpBasicInfomationByOrgUnitIdsResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllV_EmpBasicInfoByOrgUnitId : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllV_EmpBasicInfoByOrgUnitId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_GetAllV_EmpBasicInfoByOrgUnitIdResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfV_EmpBasicInformation * GetAllV_EmpBasicInfoByOrgUnitIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_GetAllV_EmpBasicInfoByOrgUnitIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfV_EmpBasicInformation * GetAllV_EmpBasicInfoByOrgUnitIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_FTS_EmpProfileSearchAll : NSObject {
	
/* elements */
	NSString * condition;
	NSNumber * userId;
	NSNumber * companyId;
	NSNumber * take;
	NSNumber * skip;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_FTS_EmpProfileSearchAll *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * condition;
@property (retain) NSNumber * userId;
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * take;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_FTS_EmpProfileSearchAllResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_ORG_UNIT_Result * FTS_EmpProfileSearchAllResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_FTS_EmpProfileSearchAllResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_ORG_UNIT_Result * FTS_EmpProfileSearchAllResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_FTS_EmpProfileBasic : NSObject {
	
/* elements */
	NSString * condition;
	NSString * criterion;
	NSNumber * userId;
	NSNumber * companyId;
	NSNumber * take;
	NSNumber * skip;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_FTS_EmpProfileBasic *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * condition;
@property (retain) NSString * criterion;
@property (retain) NSNumber * userId;
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * take;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_FTS_EmpProfileBasicResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_BASIC_Result * FTS_EmpProfileBasicResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_FTS_EmpProfileBasicResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_BASIC_Result * FTS_EmpProfileBasicResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_FTS_EmpProfileContact : NSObject {
	
/* elements */
	NSString * condition;
	NSNumber * userId;
	NSNumber * companyId;
	NSNumber * take;
	NSNumber * skip;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_FTS_EmpProfileContact *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * condition;
@property (retain) NSNumber * userId;
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * take;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_FTS_EmpProfileContactResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_CONTACT_Result * FTS_EmpProfileContactResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_FTS_EmpProfileContactResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_CONTACT_Result * FTS_EmpProfileContactResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_FTS_EmpProfileDegree : NSObject {
	
/* elements */
	NSString * condition;
	NSNumber * userId;
	NSNumber * companyId;
	NSNumber * take;
	NSNumber * skip;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_FTS_EmpProfileDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * condition;
@property (retain) NSNumber * userId;
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * take;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_FTS_EmpProfileDegreeResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_DEGREE_Result * FTS_EmpProfileDegreeResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_FTS_EmpProfileDegreeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_DEGREE_Result * FTS_EmpProfileDegreeResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_FTS_EmpProfileEduQualification : NSObject {
	
/* elements */
	NSString * condition;
	NSNumber * userId;
	NSNumber * companyId;
	NSNumber * take;
	NSNumber * skip;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_FTS_EmpProfileEduQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * condition;
@property (retain) NSNumber * userId;
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * take;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_FTS_EmpProfileEduQualificationResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_EDU_QUALIFICATION_Result * FTS_EmpProfileEduQualificationResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_FTS_EmpProfileEduQualificationResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_EDU_QUALIFICATION_Result * FTS_EmpProfileEduQualificationResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_FTS_EmpProfileForeignLanguage : NSObject {
	
/* elements */
	NSString * condition;
	NSNumber * userId;
	NSNumber * companyId;
	NSNumber * take;
	NSNumber * skip;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_FTS_EmpProfileForeignLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * condition;
@property (retain) NSNumber * userId;
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * take;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_FTS_EmpProfileForeignLanguageResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_FOREIGN_LANGUAGE_Result * FTS_EmpProfileForeignLanguageResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_FTS_EmpProfileForeignLanguageResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_FOREIGN_LANGUAGE_Result * FTS_EmpProfileForeignLanguageResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_FTS_EmpProfileJobPosition : NSObject {
	
/* elements */
	NSString * condition;
	NSNumber * userId;
	NSNumber * companyId;
	NSNumber * take;
	NSNumber * skip;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_FTS_EmpProfileJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * condition;
@property (retain) NSNumber * userId;
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * take;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface EmployeeServiceSvc_FTS_EmpProfileJobPositionResponse : NSObject {
	
/* elements */
	emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_JOBPOSITION_Result * FTS_EmpProfileJobPositionResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (EmployeeServiceSvc_FTS_EmpProfileJobPositionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_JOBPOSITION_Result * FTS_EmpProfileJobPositionResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
/* Cookies handling provided by http://en.wikibooks.org/wiki/Programming:WebObjects/Web_Services/Web_Service_Provider */
#import <libxml/parser.h>
#import "xs.h"
#import "EmployeeServiceSvc.h"
#import "ns1.h"
#import "emp_tns1.h"
#import "emp_tns5.h"
#import "emp_tns2.h"
#import "emp_tns3.h"
#import "emp_tns4.h"
@class BasicHttpBinding_IEmployeeServiceBinding;
@interface EmployeeServiceSvc : NSObject {
	
}
+ (BasicHttpBinding_IEmployeeServiceBinding *)BasicHttpBinding_IEmployeeServiceBinding;
@end
@class BasicHttpBinding_IEmployeeServiceBindingResponse;
@class BasicHttpBinding_IEmployeeServiceBindingOperation;
@protocol BasicHttpBinding_IEmployeeServiceBindingResponseDelegate <NSObject>
- (void) operation:(BasicHttpBinding_IEmployeeServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmployeeServiceBindingResponse *)response;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding : NSObject <BasicHttpBinding_IEmployeeServiceBindingResponseDelegate> {
	NSURL *address;
	NSTimeInterval defaultTimeout;
	NSMutableArray *cookies;
	BOOL logXMLInOut;
	BOOL synchronousOperationComplete;
	NSString *authUsername;
	NSString *authPassword;
}
@property (copy) NSURL *address;
@property (assign) BOOL logXMLInOut;
@property (assign) NSTimeInterval defaultTimeout;
@property (nonatomic, retain) NSMutableArray *cookies;
@property (nonatomic, retain) NSString *authUsername;
@property (nonatomic, retain) NSString *authPassword;
- (id)initWithAddress:(NSString *)anAddress;
- (void)sendHTTPCallUsingBody:(NSString *)body soapAction:(NSString *)soapAction forOperation:(BasicHttpBinding_IEmployeeServiceBindingOperation *)operation;
- (void)addCookie:(NSHTTPCookie *)toAdd;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)FTS_EmpProfileOrgUnitUsingParameters:(EmployeeServiceSvc_FTS_EmpProfileOrgUnit *)aParameters ;
- (void)FTS_EmpProfileOrgUnitAsyncUsingParameters:(EmployeeServiceSvc_FTS_EmpProfileOrgUnit *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)FTS_EmpContractUsingParameters:(EmployeeServiceSvc_FTS_EmpContract *)aParameters ;
- (void)FTS_EmpContractAsyncUsingParameters:(EmployeeServiceSvc_FTS_EmpContract *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)FTS_EmpOffUsingParameters:(EmployeeServiceSvc_FTS_EmpOff *)aParameters ;
- (void)FTS_EmpOffAsyncUsingParameters:(EmployeeServiceSvc_FTS_EmpOff *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllEmployeeByAdvancedSearchUsingParameters:(EmployeeServiceSvc_GetAllEmployeeByAdvancedSearch *)aParameters ;
- (void)GetAllEmployeeByAdvancedSearchAsyncUsingParameters:(EmployeeServiceSvc_GetAllEmployeeByAdvancedSearch *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllEmployeeProfileByAdvancedSearchUsingParameters:(EmployeeServiceSvc_GetAllEmployeeProfileByAdvancedSearch *)aParameters ;
- (void)GetAllEmployeeProfileByAdvancedSearchAsyncUsingParameters:(EmployeeServiceSvc_GetAllEmployeeProfileByAdvancedSearch *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllEmployeeContractByAdvancedSearchUsingParameters:(EmployeeServiceSvc_GetAllEmployeeContractByAdvancedSearch *)aParameters ;
- (void)GetAllEmployeeContractByAdvancedSearchAsyncUsingParameters:(EmployeeServiceSvc_GetAllEmployeeContractByAdvancedSearch *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllEmployeeOffByAdvancedSearchUsingParameters:(EmployeeServiceSvc_GetAllEmployeeOffByAdvancedSearch *)aParameters ;
- (void)GetAllEmployeeOffByAdvancedSearchAsyncUsingParameters:(EmployeeServiceSvc_GetAllEmployeeOffByAdvancedSearch *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllEmployerUsingParameters:(EmployeeServiceSvc_GetAllEmployer *)aParameters ;
- (void)GetAllEmployerAsyncUsingParameters:(EmployeeServiceSvc_GetAllEmployer *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCompanyUsingParameters:(EmployeeServiceSvc_GetAllCompany *)aParameters ;
- (void)GetAllCompanyAsyncUsingParameters:(EmployeeServiceSvc_GetAllCompany *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetCompanyInfoUsingParameters:(EmployeeServiceSvc_GetCompanyInfo *)aParameters ;
- (void)GetCompanyInfoAsyncUsingParameters:(EmployeeServiceSvc_GetCompanyInfo *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllEmpContractSummaryUsingParameters:(EmployeeServiceSvc_GetAllEmpContractSummary *)aParameters ;
- (void)GetAllEmpContractSummaryAsyncUsingParameters:(EmployeeServiceSvc_GetAllEmpContractSummary *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllEmpContractUsingParameters:(EmployeeServiceSvc_GetAllEmpContract *)aParameters ;
- (void)GetAllEmpContractAsyncUsingParameters:(EmployeeServiceSvc_GetAllEmpContract *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllEmpContractBySkipTakeUsingParameters:(EmployeeServiceSvc_GetAllEmpContractBySkipTake *)aParameters ;
- (void)GetAllEmpContractBySkipTakeAsyncUsingParameters:(EmployeeServiceSvc_GetAllEmpContractBySkipTake *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetEmpContractUsingParameters:(EmployeeServiceSvc_GetEmpContract *)aParameters ;
- (void)GetEmpContractAsyncUsingParameters:(EmployeeServiceSvc_GetEmpContract *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetEmpContractByContractIdUsingParameters:(EmployeeServiceSvc_GetEmpContractByContractId *)aParameters ;
- (void)GetEmpContractByContractIdAsyncUsingParameters:(EmployeeServiceSvc_GetEmpContractByContractId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)CountEmpContractUsingParameters:(EmployeeServiceSvc_CountEmpContract *)aParameters ;
- (void)CountEmpContractAsyncUsingParameters:(EmployeeServiceSvc_CountEmpContract *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetEmpBasicProfileByEmployeIdUsingParameters:(EmployeeServiceSvc_GetEmpBasicProfileByEmployeId *)aParameters ;
- (void)GetEmpBasicProfileByEmployeIdAsyncUsingParameters:(EmployeeServiceSvc_GetEmpBasicProfileByEmployeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetWokingTimeByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetWokingTimeByEmployeeId *)aParameters ;
- (void)GetWokingTimeByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetWokingTimeByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetContractTypeUsingParameters:(EmployeeServiceSvc_GetContractType *)aParameters ;
- (void)GetContractTypeAsyncUsingParameters:(EmployeeServiceSvc_GetContractType *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)CheckExistEmpContractCodeUsingParameters:(EmployeeServiceSvc_CheckExistEmpContractCode *)aParameters ;
- (void)CheckExistEmpContractCodeAsyncUsingParameters:(EmployeeServiceSvc_CheckExistEmpContractCode *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllEmpContractTermUsingParameters:(EmployeeServiceSvc_GetAllEmpContractTerm *)aParameters ;
- (void)GetAllEmpContractTermAsyncUsingParameters:(EmployeeServiceSvc_GetAllEmpContractTerm *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetEmpContractTermUndefinedUsingParameters:(EmployeeServiceSvc_GetEmpContractTermUndefined *)aParameters ;
- (void)GetEmpContractTermUndefinedAsyncUsingParameters:(EmployeeServiceSvc_GetEmpContractTermUndefined *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetEmpContractTermIdentifiedUsingParameters:(EmployeeServiceSvc_GetEmpContractTermIdentified *)aParameters ;
- (void)GetEmpContractTermIdentifiedAsyncUsingParameters:(EmployeeServiceSvc_GetEmpContractTermIdentified *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)CreateEmpContractUsingParameters:(EmployeeServiceSvc_CreateEmpContract *)aParameters ;
- (void)CreateEmpContractAsyncUsingParameters:(EmployeeServiceSvc_CreateEmpContract *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)UpdateEmpContractUsingParameters:(EmployeeServiceSvc_UpdateEmpContract *)aParameters ;
- (void)UpdateEmpContractAsyncUsingParameters:(EmployeeServiceSvc_UpdateEmpContract *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)EndEmpContractsUsingParameters:(EmployeeServiceSvc_EndEmpContracts *)aParameters ;
- (void)EndEmpContractsAsyncUsingParameters:(EmployeeServiceSvc_EndEmpContracts *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)EndContractsUsingParameters:(EmployeeServiceSvc_EndContracts *)aParameters ;
- (void)EndContractsAsyncUsingParameters:(EmployeeServiceSvc_EndContracts *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)DeleteEmpContractsUsingParameters:(EmployeeServiceSvc_DeleteEmpContracts *)aParameters ;
- (void)DeleteEmpContractsAsyncUsingParameters:(EmployeeServiceSvc_DeleteEmpContracts *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)AppraiseContractUsingParameters:(EmployeeServiceSvc_AppraiseContract *)aParameters ;
- (void)AppraiseContractAsyncUsingParameters:(EmployeeServiceSvc_AppraiseContract *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllEmpNotificationConstantUsingParameters:(EmployeeServiceSvc_GetAllEmpNotificationConstant *)aParameters ;
- (void)GetAllEmpNotificationConstantAsyncUsingParameters:(EmployeeServiceSvc_GetAllEmpNotificationConstant *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListNotifyEmpBirthdayUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyEmpBirthday *)aParameters ;
- (void)GetJsonListNotifyEmpBirthdayAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyEmpBirthday *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetValueNotifyEmpBirthdayUsingParameters:(EmployeeServiceSvc_GetValueNotifyEmpBirthday *)aParameters ;
- (void)GetValueNotifyEmpBirthdayAsyncUsingParameters:(EmployeeServiceSvc_GetValueNotifyEmpBirthday *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListEmpBirthdayUsingParameters:(EmployeeServiceSvc_GetJsonListEmpBirthday *)aParameters ;
- (void)GetJsonListEmpBirthdayAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListEmpBirthday *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListNotifyEmpAppoinmentExpirationUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyEmpAppoinmentExpiration *)aParameters ;
- (void)GetJsonListNotifyEmpAppoinmentExpirationAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyEmpAppoinmentExpiration *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetValueNotifyEmpAppoinmentExpirationUsingParameters:(EmployeeServiceSvc_GetValueNotifyEmpAppoinmentExpiration *)aParameters ;
- (void)GetValueNotifyEmpAppoinmentExpirationAsyncUsingParameters:(EmployeeServiceSvc_GetValueNotifyEmpAppoinmentExpiration *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListEmpAppoinmentExpirationUsingParameters:(EmployeeServiceSvc_GetJsonListEmpAppoinmentExpiration *)aParameters ;
- (void)GetJsonListEmpAppoinmentExpirationAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListEmpAppoinmentExpiration *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetValueNotifyEmpIncreaseSalaryUsingParameters:(EmployeeServiceSvc_GetValueNotifyEmpIncreaseSalary *)aParameters ;
- (void)GetValueNotifyEmpIncreaseSalaryAsyncUsingParameters:(EmployeeServiceSvc_GetValueNotifyEmpIncreaseSalary *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListNotifyEmpToOfficalUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyEmpToOffical *)aParameters ;
- (void)GetJsonListNotifyEmpToOfficalAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyEmpToOffical *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListEmpToOfficalUsingParameters:(EmployeeServiceSvc_GetJsonListEmpToOffical *)aParameters ;
- (void)GetJsonListEmpToOfficalAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListEmpToOffical *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetValueNotifyEmpToOfficalUsingParameters:(EmployeeServiceSvc_GetValueNotifyEmpToOffical *)aParameters ;
- (void)GetValueNotifyEmpToOfficalAsyncUsingParameters:(EmployeeServiceSvc_GetValueNotifyEmpToOffical *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListNotifyEmpExpirationPassportUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyEmpExpirationPassport *)aParameters ;
- (void)GetJsonListNotifyEmpExpirationPassportAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyEmpExpirationPassport *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListEmpExpirationPassportUsingParameters:(EmployeeServiceSvc_GetJsonListEmpExpirationPassport *)aParameters ;
- (void)GetJsonListEmpExpirationPassportAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListEmpExpirationPassport *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetValueNotifyEmpExpirationPassportUsingParameters:(EmployeeServiceSvc_GetValueNotifyEmpExpirationPassport *)aParameters ;
- (void)GetValueNotifyEmpExpirationPassportAsyncUsingParameters:(EmployeeServiceSvc_GetValueNotifyEmpExpirationPassport *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListNotifyEmpParticipateTrainingUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyEmpParticipateTraining *)aParameters ;
- (void)GetJsonListNotifyEmpParticipateTrainingAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyEmpParticipateTraining *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListEmpParticipateTrainingUsingParameters:(EmployeeServiceSvc_GetJsonListEmpParticipateTraining *)aParameters ;
- (void)GetJsonListEmpParticipateTrainingAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListEmpParticipateTraining *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetValueNotifyEmpParticipateTrainingUsingParameters:(EmployeeServiceSvc_GetValueNotifyEmpParticipateTraining *)aParameters ;
- (void)GetValueNotifyEmpParticipateTrainingAsyncUsingParameters:(EmployeeServiceSvc_GetValueNotifyEmpParticipateTraining *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListNotifyEmpExpirationDegreeUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyEmpExpirationDegree *)aParameters ;
- (void)GetJsonListNotifyEmpExpirationDegreeAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyEmpExpirationDegree *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListEmpExpirationDegreeUsingParameters:(EmployeeServiceSvc_GetJsonListEmpExpirationDegree *)aParameters ;
- (void)GetJsonListEmpExpirationDegreeAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListEmpExpirationDegree *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetValueNotifyExpirationDegreeUsingParameters:(EmployeeServiceSvc_GetValueNotifyExpirationDegree *)aParameters ;
- (void)GetValueNotifyExpirationDegreeAsyncUsingParameters:(EmployeeServiceSvc_GetValueNotifyExpirationDegree *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListNotifyEmpExpirationContractUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyEmpExpirationContract *)aParameters ;
- (void)GetJsonListNotifyEmpExpirationContractAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyEmpExpirationContract *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListEmpExpirationContractUsingParameters:(EmployeeServiceSvc_GetJsonListEmpExpirationContract *)aParameters ;
- (void)GetJsonListEmpExpirationContractAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListEmpExpirationContract *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetValueNotifyExpirationContractUsingParameters:(EmployeeServiceSvc_GetValueNotifyExpirationContract *)aParameters ;
- (void)GetValueNotifyExpirationContractAsyncUsingParameters:(EmployeeServiceSvc_GetValueNotifyExpirationContract *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListNotifyEmpWaitReSignedContractUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyEmpWaitReSignedContract *)aParameters ;
- (void)GetJsonListNotifyEmpWaitReSignedContractAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyEmpWaitReSignedContract *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListEmpWaitReSignedContractUsingParameters:(EmployeeServiceSvc_GetJsonListEmpWaitReSignedContract *)aParameters ;
- (void)GetJsonListEmpWaitReSignedContractAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListEmpWaitReSignedContract *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetValueNotifyWaitResignedContractUsingParameters:(EmployeeServiceSvc_GetValueNotifyWaitResignedContract *)aParameters ;
- (void)GetValueNotifyWaitResignedContractAsyncUsingParameters:(EmployeeServiceSvc_GetValueNotifyWaitResignedContract *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListNotifyEmpLeaveUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyEmpLeave *)aParameters ;
- (void)GetJsonListNotifyEmpLeaveAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyEmpLeave *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListEmpLeaveUsingParameters:(EmployeeServiceSvc_GetJsonListEmpLeave *)aParameters ;
- (void)GetJsonListEmpLeaveAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListEmpLeave *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetValueNotifyEmpLeaveUsingParameters:(EmployeeServiceSvc_GetValueNotifyEmpLeave *)aParameters ;
- (void)GetValueNotifyEmpLeaveAsyncUsingParameters:(EmployeeServiceSvc_GetValueNotifyEmpLeave *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListEmpMaterityLeaveUsingParameters:(EmployeeServiceSvc_GetJsonListEmpMaterityLeave *)aParameters ;
- (void)GetJsonListEmpMaterityLeaveAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListEmpMaterityLeave *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListEmpWillMaterityLeaveUsingParameters:(EmployeeServiceSvc_GetJsonListEmpWillMaterityLeave *)aParameters ;
- (void)GetJsonListEmpWillMaterityLeaveAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListEmpWillMaterityLeave *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListEmpWillPregnancyMonth7thLeaveUsingParameters:(EmployeeServiceSvc_GetJsonListEmpWillPregnancyMonth7thLeave *)aParameters ;
- (void)GetJsonListEmpWillPregnancyMonth7thLeaveAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListEmpWillPregnancyMonth7thLeave *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListEmpRaisingChildExpirationUsingParameters:(EmployeeServiceSvc_GetJsonListEmpRaisingChildExpiration *)aParameters ;
- (void)GetJsonListEmpRaisingChildExpirationAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListEmpRaisingChildExpiration *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListEmpFamilyDependentExpirationUsingParameters:(EmployeeServiceSvc_GetJsonListEmpFamilyDependentExpiration *)aParameters ;
- (void)GetJsonListEmpFamilyDependentExpirationAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListEmpFamilyDependentExpiration *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListNotifyEmpWillChildBirthUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyEmpWillChildBirth *)aParameters ;
- (void)GetJsonListNotifyEmpWillChildBirthAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyEmpWillChildBirth *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListEmpWillChildBirthUsingParameters:(EmployeeServiceSvc_GetJsonListEmpWillChildBirth *)aParameters ;
- (void)GetJsonListEmpWillChildBirthAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListEmpWillChildBirth *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetValueNotifyWillChildBirthUsingParameters:(EmployeeServiceSvc_GetValueNotifyWillChildBirth *)aParameters ;
- (void)GetValueNotifyWillChildBirthAsyncUsingParameters:(EmployeeServiceSvc_GetValueNotifyWillChildBirth *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListNotifyLongTermLeaveUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyLongTermLeave *)aParameters ;
- (void)GetJsonListNotifyLongTermLeaveAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyLongTermLeave *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListLongTermLeaveUsingParameters:(EmployeeServiceSvc_GetJsonListLongTermLeave *)aParameters ;
- (void)GetJsonListLongTermLeaveAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListLongTermLeave *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetValueNotifyLongTermLeaveUsingParameters:(EmployeeServiceSvc_GetValueNotifyLongTermLeave *)aParameters ;
- (void)GetValueNotifyLongTermLeaveAsyncUsingParameters:(EmployeeServiceSvc_GetValueNotifyLongTermLeave *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListNotifyEmpWillRetireUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyEmpWillRetire *)aParameters ;
- (void)GetJsonListNotifyEmpWillRetireAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyEmpWillRetire *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListEmpWillRetireUsingParameters:(EmployeeServiceSvc_GetJsonListEmpWillRetire *)aParameters ;
- (void)GetJsonListEmpWillRetireAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListEmpWillRetire *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetvalueNotifyWillRetireUsingParameters:(EmployeeServiceSvc_GetvalueNotifyWillRetire *)aParameters ;
- (void)GetvalueNotifyWillRetireAsyncUsingParameters:(EmployeeServiceSvc_GetvalueNotifyWillRetire *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetValueRetireAgeForMaleUsingParameters:(EmployeeServiceSvc_GetValueRetireAgeForMale *)aParameters ;
- (void)GetValueRetireAgeForMaleAsyncUsingParameters:(EmployeeServiceSvc_GetValueRetireAgeForMale *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetValueRetireAgeForFamaleUsingParameters:(EmployeeServiceSvc_GetValueRetireAgeForFamale *)aParameters ;
- (void)GetValueRetireAgeForFamaleAsyncUsingParameters:(EmployeeServiceSvc_GetValueRetireAgeForFamale *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListNotifyEmpWorkAfterChildBirthUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyEmpWorkAfterChildBirth *)aParameters ;
- (void)GetJsonListNotifyEmpWorkAfterChildBirthAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListNotifyEmpWorkAfterChildBirth *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListEmpWorkAfterChildBirthUsingParameters:(EmployeeServiceSvc_GetJsonListEmpWorkAfterChildBirth *)aParameters ;
- (void)GetJsonListEmpWorkAfterChildBirthAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListEmpWorkAfterChildBirth *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetValueNotifyWorlAfterChildBirthUsingParameters:(EmployeeServiceSvc_GetValueNotifyWorlAfterChildBirth *)aParameters ;
- (void)GetValueNotifyWorlAfterChildBirthAsyncUsingParameters:(EmployeeServiceSvc_GetValueNotifyWorlAfterChildBirth *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetJsonListEmpWorkAfterLeaveUsingParameters:(EmployeeServiceSvc_GetJsonListEmpWorkAfterLeave *)aParameters ;
- (void)GetJsonListEmpWorkAfterLeaveAsyncUsingParameters:(EmployeeServiceSvc_GetJsonListEmpWorkAfterLeave *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetEmpOffProfileByIdUsingParameters:(EmployeeServiceSvc_GetEmpOffProfileById *)aParameters ;
- (void)GetEmpOffProfileByIdAsyncUsingParameters:(EmployeeServiceSvc_GetEmpOffProfileById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GETAllViewEmpOffProfileUsingParameters:(EmployeeServiceSvc_GETAllViewEmpOffProfile *)aParameters ;
- (void)GETAllViewEmpOffProfileAsyncUsingParameters:(EmployeeServiceSvc_GETAllViewEmpOffProfile *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmpOffProfileByIdUsingParameters:(EmployeeServiceSvc_GetViewEmpOffProfileById *)aParameters ;
- (void)GetViewEmpOffProfileByIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmpOffProfileById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)AddEmpOffProfileByViewUsingParameters:(EmployeeServiceSvc_AddEmpOffProfileByView *)aParameters ;
- (void)AddEmpOffProfileByViewAsyncUsingParameters:(EmployeeServiceSvc_AddEmpOffProfileByView *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)UpdateEmpOffProfileByViewUsingParameters:(EmployeeServiceSvc_UpdateEmpOffProfileByView *)aParameters ;
- (void)UpdateEmpOffProfileByViewAsyncUsingParameters:(EmployeeServiceSvc_UpdateEmpOffProfileByView *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)DeleteEmpOffProfileByIdUsingParameters:(EmployeeServiceSvc_DeleteEmpOffProfileById *)aParameters ;
- (void)DeleteEmpOffProfileByIdAsyncUsingParameters:(EmployeeServiceSvc_DeleteEmpOffProfileById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogEquipmentUsingParameters:(EmployeeServiceSvc_GetAllCLogEquipment *)aParameters ;
- (void)GetAllCLogEquipmentAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogEquipment *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogEquipmentTypeUsingParameters:(EmployeeServiceSvc_GetAllCLogEquipmentType *)aParameters ;
- (void)GetAllCLogEquipmentTypeAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogEquipmentType *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogAssetsTypeUsingParameters:(EmployeeServiceSvc_GetAllCLogAssetsType *)aParameters ;
- (void)GetAllCLogAssetsTypeAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogAssetsType *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllEquipmentByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetAllEquipmentByEmployeeId *)aParameters ;
- (void)GetAllEquipmentByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetAllEquipmentByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllAssetsByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetAllAssetsByEmployeeId *)aParameters ;
- (void)GetAllAssetsByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetAllAssetsByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllViewCLogEquipmentUsingParameters:(EmployeeServiceSvc_GetAllViewCLogEquipment *)aParameters ;
- (void)GetAllViewCLogEquipmentAsyncUsingParameters:(EmployeeServiceSvc_GetAllViewCLogEquipment *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllViewCLogAssetsUsingParameters:(EmployeeServiceSvc_GetAllViewCLogAssets *)aParameters ;
- (void)GetAllViewCLogAssetsAsyncUsingParameters:(EmployeeServiceSvc_GetAllViewCLogAssets *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllViewOrgUnitAssetsUsingParameters:(EmployeeServiceSvc_GetAllViewOrgUnitAssets *)aParameters ;
- (void)GetAllViewOrgUnitAssetsAsyncUsingParameters:(EmployeeServiceSvc_GetAllViewOrgUnitAssets *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllViewOrgUnitEquipmentUsingParameters:(EmployeeServiceSvc_GetAllViewOrgUnitEquipment *)aParameters ;
- (void)GetAllViewOrgUnitEquipmentAsyncUsingParameters:(EmployeeServiceSvc_GetAllViewOrgUnitEquipment *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllEmpHasAssetsByAssetsIdAndUnitIdUsingParameters:(EmployeeServiceSvc_GetAllEmpHasAssetsByAssetsIdAndUnitId *)aParameters ;
- (void)GetAllEmpHasAssetsByAssetsIdAndUnitIdAsyncUsingParameters:(EmployeeServiceSvc_GetAllEmpHasAssetsByAssetsIdAndUnitId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllEmpHasEquipmentByEquipmentIdAndUnitIdUsingParameters:(EmployeeServiceSvc_GetAllEmpHasEquipmentByEquipmentIdAndUnitId *)aParameters ;
- (void)GetAllEmpHasEquipmentByEquipmentIdAndUnitIdAsyncUsingParameters:(EmployeeServiceSvc_GetAllEmpHasEquipmentByEquipmentIdAndUnitId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllEmployeeUsingParameters:(EmployeeServiceSvc_GetAllEmployee *)aParameters ;
- (void)GetAllEmployeeAsyncUsingParameters:(EmployeeServiceSvc_GetAllEmployee *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCitiesUsingParameters:(EmployeeServiceSvc_GetAllCities *)aParameters ;
- (void)GetAllCitiesAsyncUsingParameters:(EmployeeServiceSvc_GetAllCities *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllDistrictsUsingParameters:(EmployeeServiceSvc_GetAllDistricts *)aParameters ;
- (void)GetAllDistrictsAsyncUsingParameters:(EmployeeServiceSvc_GetAllDistricts *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCountriesUsingParameters:(EmployeeServiceSvc_GetAllCountries *)aParameters ;
- (void)GetAllCountriesAsyncUsingParameters:(EmployeeServiceSvc_GetAllCountries *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)CreateAddressUsingParameters:(EmployeeServiceSvc_CreateAddress *)aParameters ;
- (void)CreateAddressAsyncUsingParameters:(EmployeeServiceSvc_CreateAddress *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetDirectReportAndChildByIdUsingParameters:(EmployeeServiceSvc_GetDirectReportAndChildById *)aParameters ;
- (void)GetDirectReportAndChildByIdAsyncUsingParameters:(EmployeeServiceSvc_GetDirectReportAndChildById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllDirectReportUsingParameters:(EmployeeServiceSvc_GetAllDirectReport *)aParameters ;
- (void)GetAllDirectReportAsyncUsingParameters:(EmployeeServiceSvc_GetAllDirectReport *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetEmployeeSearchResultUsingParameters:(EmployeeServiceSvc_GetEmployeeSearchResult *)aParameters ;
- (void)GetEmployeeSearchResultAsyncUsingParameters:(EmployeeServiceSvc_GetEmployeeSearchResult *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetDirectReportByChildIdUsingParameters:(EmployeeServiceSvc_GetDirectReportByChildId *)aParameters ;
- (void)GetDirectReportByChildIdAsyncUsingParameters:(EmployeeServiceSvc_GetDirectReportByChildId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetUserIdByEmployeeCodeUsingParameters:(EmployeeServiceSvc_GetUserIdByEmployeeCode *)aParameters ;
- (void)GetUserIdByEmployeeCodeAsyncUsingParameters:(EmployeeServiceSvc_GetUserIdByEmployeeCode *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetLeaderOfOrgUsingParameters:(EmployeeServiceSvc_GetLeaderOfOrg *)aParameters ;
- (void)GetLeaderOfOrgAsyncUsingParameters:(EmployeeServiceSvc_GetLeaderOfOrg *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetInforOfEmpsUsingParameters:(EmployeeServiceSvc_GetInforOfEmps *)aParameters ;
- (void)GetInforOfEmpsAsyncUsingParameters:(EmployeeServiceSvc_GetInforOfEmps *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetEmpInGenderUsingParameters:(EmployeeServiceSvc_GetEmpInGender *)aParameters ;
- (void)GetEmpInGenderAsyncUsingParameters:(EmployeeServiceSvc_GetEmpInGender *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetNewestEmployeeUsingParameters:(EmployeeServiceSvc_GetNewestEmployee *)aParameters ;
- (void)GetNewestEmployeeAsyncUsingParameters:(EmployeeServiceSvc_GetNewestEmployee *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllEmpAssignedGroupUsingParameters:(EmployeeServiceSvc_GetAllEmpAssignedGroup *)aParameters ;
- (void)GetAllEmpAssignedGroupAsyncUsingParameters:(EmployeeServiceSvc_GetAllEmpAssignedGroup *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)AssignEmpToGroupUsingParameters:(EmployeeServiceSvc_AssignEmpToGroup *)aParameters ;
- (void)AssignEmpToGroupAsyncUsingParameters:(EmployeeServiceSvc_AssignEmpToGroup *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)RevokeEmpFromGroupUsingParameters:(EmployeeServiceSvc_RevokeEmpFromGroup *)aParameters ;
- (void)RevokeEmpFromGroupAsyncUsingParameters:(EmployeeServiceSvc_RevokeEmpFromGroup *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetDirectReportOfEmployeeByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetDirectReportOfEmployeeByEmployeeId *)aParameters ;
- (void)GetDirectReportOfEmployeeByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetDirectReportOfEmployeeByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllDirectReportOfEmpByEmpIdUsingParameters:(EmployeeServiceSvc_GetAllDirectReportOfEmpByEmpId *)aParameters ;
- (void)GetAllDirectReportOfEmpByEmpIdAsyncUsingParameters:(EmployeeServiceSvc_GetAllDirectReportOfEmpByEmpId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmpBasicInformationByIdUsingParameters:(EmployeeServiceSvc_GetViewEmpBasicInformationById *)aParameters ;
- (void)GetViewEmpBasicInformationByIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmpBasicInformationById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllViewEmployeesByUserUsingParameters:(EmployeeServiceSvc_GetAllViewEmployeesByUser *)aParameters ;
- (void)GetAllViewEmployeesByUserAsyncUsingParameters:(EmployeeServiceSvc_GetAllViewEmployeesByUser *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmployeesByUserUsingParameters:(EmployeeServiceSvc_GetViewEmployeesByUser *)aParameters ;
- (void)GetViewEmployeesByUserAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmployeesByUser *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllViewEmployeesUsingParameters:(EmployeeServiceSvc_GetAllViewEmployees *)aParameters ;
- (void)GetAllViewEmployeesAsyncUsingParameters:(EmployeeServiceSvc_GetAllViewEmployees *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmployeesByPageUsingParameters:(EmployeeServiceSvc_GetViewEmployeesByPage *)aParameters ;
- (void)GetViewEmployeesByPageAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmployeesByPage *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmployeePageNumberUsingParameters:(EmployeeServiceSvc_GetViewEmployeePageNumber *)aParameters ;
- (void)GetViewEmployeePageNumberAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmployeePageNumber *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmployeeByIdUsingParameters:(EmployeeServiceSvc_GetViewEmployeeById *)aParameters ;
- (void)GetViewEmployeeByIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmployeeById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmployeeProfileByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetViewEmployeeProfileByEmployeeId *)aParameters ;
- (void)GetViewEmployeeProfileByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmployeeProfileByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllEmployeesUsingParameters:(EmployeeServiceSvc_GetAllEmployees *)aParameters ;
- (void)GetAllEmployeesAsyncUsingParameters:(EmployeeServiceSvc_GetAllEmployees *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetEmployeeByIdUsingParameters:(EmployeeServiceSvc_GetEmployeeById *)aParameters ;
- (void)GetEmployeeByIdAsyncUsingParameters:(EmployeeServiceSvc_GetEmployeeById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)SubmitCreateNewEmployeeUsingParameters:(EmployeeServiceSvc_SubmitCreateNewEmployee *)aParameters ;
- (void)SubmitCreateNewEmployeeAsyncUsingParameters:(EmployeeServiceSvc_SubmitCreateNewEmployee *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)CheckValidEmpCodeUsingParameters:(EmployeeServiceSvc_CheckValidEmpCode *)aParameters ;
- (void)CheckValidEmpCodeAsyncUsingParameters:(EmployeeServiceSvc_CheckValidEmpCode *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)CreateEmployeeUsingParameters:(EmployeeServiceSvc_CreateEmployee *)aParameters ;
- (void)CreateEmployeeAsyncUsingParameters:(EmployeeServiceSvc_CreateEmployee *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllLeaderUsingParameters:(EmployeeServiceSvc_GetAllLeader *)aParameters ;
- (void)GetAllLeaderAsyncUsingParameters:(EmployeeServiceSvc_GetAllLeader *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)CreateEmpProfileJobPositionUsingParameters:(EmployeeServiceSvc_CreateEmpProfileJobPosition *)aParameters ;
- (void)CreateEmpProfileJobPositionAsyncUsingParameters:(EmployeeServiceSvc_CreateEmpProfileJobPosition *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)CreateEmpProfileProcessOfWorkUsingParameters:(EmployeeServiceSvc_CreateEmpProfileProcessOfWork *)aParameters ;
- (void)CreateEmpProfileProcessOfWorkAsyncUsingParameters:(EmployeeServiceSvc_CreateEmpProfileProcessOfWork *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)CreateEmpBasicProfileUsingParameters:(EmployeeServiceSvc_CreateEmpBasicProfile *)aParameters ;
- (void)CreateEmpBasicProfileAsyncUsingParameters:(EmployeeServiceSvc_CreateEmpBasicProfile *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmpBasicProfilesByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetViewEmpBasicProfilesByEmployeeId *)aParameters ;
- (void)GetViewEmpBasicProfilesByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmpBasicProfilesByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogFamilyRelationshipsUsingParameters:(EmployeeServiceSvc_GetAllCLogFamilyRelationships *)aParameters ;
- (void)GetAllCLogFamilyRelationshipsAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogFamilyRelationships *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmpProfileWorkingExperiencesByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileWorkingExperiencesByEmployeeId *)aParameters ;
- (void)GetViewEmpProfileWorkingExperiencesByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileWorkingExperiencesByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmpProfileSkillsByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileSkillsByEmployeeId *)aParameters ;
- (void)GetViewEmpProfileSkillsByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileSkillsByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmpProfileJobPositionsByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileJobPositionsByEmployeeId *)aParameters ;
- (void)GetViewEmpProfileJobPositionsByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileJobPositionsByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmpContractsByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetViewEmpContractsByEmployeeId *)aParameters ;
- (void)GetViewEmpContractsByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmpContractsByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmpProfileBasicSalarysByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileBasicSalarysByEmployeeId *)aParameters ;
- (void)GetViewEmpProfileBasicSalarysByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileBasicSalarysByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmpProfileHealthInsurancesByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileHealthInsurancesByEmployeeId *)aParameters ;
- (void)GetViewEmpProfileHealthInsurancesByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileHealthInsurancesByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmpProfileWageTypesByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileWageTypesByEmployeeId *)aParameters ;
- (void)GetViewEmpProfileWageTypesByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileWageTypesByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmpProfileWorkingFormsByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileWorkingFormsByEmployeeId *)aParameters ;
- (void)GetViewEmpProfileWorkingFormsByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileWorkingFormsByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmpSocialInsuranceSalariesByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetViewEmpSocialInsuranceSalariesByEmployeeId *)aParameters ;
- (void)GetViewEmpSocialInsuranceSalariesByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmpSocialInsuranceSalariesByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmpProfileBiographiesByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileBiographiesByEmployeeId *)aParameters ;
- (void)GetViewEmpProfileBiographiesByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileBiographiesByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmpProfileExperiencesByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileExperiencesByEmployeeId *)aParameters ;
- (void)GetViewEmpProfileExperiencesByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileExperiencesByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmpProfileProcessOfWorksByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileProcessOfWorksByEmployeeId *)aParameters ;
- (void)GetViewEmpProfileProcessOfWorksByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileProcessOfWorksByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmpProfileContactsByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileContactsByEmployeeId *)aParameters ;
- (void)GetViewEmpProfileContactsByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileContactsByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmpProfileSocialInsurancesByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileSocialInsurancesByEmployeeId *)aParameters ;
- (void)GetViewEmpProfileSocialInsurancesByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileSocialInsurancesByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmpProfileLeaveRegimesByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileLeaveRegimesByEmployeeId *)aParameters ;
- (void)GetViewEmpProfileLeaveRegimesByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileLeaveRegimesByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmpPerformanceAppraisalsByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetViewEmpPerformanceAppraisalsByEmployeeId *)aParameters ;
- (void)GetViewEmpPerformanceAppraisalsByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmpPerformanceAppraisalsByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmpProfileQualificationsByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileQualificationsByEmployeeId *)aParameters ;
- (void)GetViewEmpProfileQualificationsByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileQualificationsByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmpProfileForeignLanguagesByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileForeignLanguagesByEmployeeId *)aParameters ;
- (void)GetViewEmpProfileForeignLanguagesByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileForeignLanguagesByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmpProfileTrainingsByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileTrainingsByEmployeeId *)aParameters ;
- (void)GetViewEmpProfileTrainingsByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileTrainingsByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmpProfileDegreesByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileDegreesByEmployeeId *)aParameters ;
- (void)GetViewEmpProfileDegreesByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileDegreesByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmpProfileDisciplinesByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileDisciplinesByEmployeeId *)aParameters ;
- (void)GetViewEmpProfileDisciplinesByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmpProfileDisciplinesByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewEmEmpProfilePersonalitiesByEmployeeIdUsingParameters:(EmployeeServiceSvc_GetViewEmEmpProfilePersonalitiesByEmployeeId *)aParameters ;
- (void)GetViewEmEmpProfilePersonalitiesByEmployeeIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewEmEmpProfilePersonalitiesByEmployeeId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllTimeWorkingFormsUsingParameters:(EmployeeServiceSvc_GetAllTimeWorkingForms *)aParameters ;
- (void)GetAllTimeWorkingFormsAsyncUsingParameters:(EmployeeServiceSvc_GetAllTimeWorkingForms *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllTimeWageTypesUsingParameters:(EmployeeServiceSvc_GetAllTimeWageTypes *)aParameters ;
- (void)GetAllTimeWageTypesAsyncUsingParameters:(EmployeeServiceSvc_GetAllTimeWageTypes *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetTimeWageTypeByIdUsingParameters:(EmployeeServiceSvc_GetTimeWageTypeById *)aParameters ;
- (void)GetTimeWageTypeByIdAsyncUsingParameters:(EmployeeServiceSvc_GetTimeWageTypeById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogCBCompensationCategoriesUsingParameters:(EmployeeServiceSvc_GetAllCLogCBCompensationCategories *)aParameters ;
- (void)GetAllCLogCBCompensationCategoriesAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogCBCompensationCategories *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetCLogCitiesByCLogCountryIdUsingParameters:(EmployeeServiceSvc_GetCLogCitiesByCLogCountryId *)aParameters ;
- (void)GetCLogCitiesByCLogCountryIdAsyncUsingParameters:(EmployeeServiceSvc_GetCLogCitiesByCLogCountryId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogCitiesUsingParameters:(EmployeeServiceSvc_GetAllCLogCities *)aParameters ;
- (void)GetAllCLogCitiesAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogCities *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetCLogCityByIdUsingParameters:(EmployeeServiceSvc_GetCLogCityById *)aParameters ;
- (void)GetCLogCityByIdAsyncUsingParameters:(EmployeeServiceSvc_GetCLogCityById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogCurrenciesUsingParameters:(EmployeeServiceSvc_GetAllCLogCurrencies *)aParameters ;
- (void)GetAllCLogCurrenciesAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogCurrencies *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetCLogCurrencyByIdUsingParameters:(EmployeeServiceSvc_GetCLogCurrencyById *)aParameters ;
- (void)GetCLogCurrencyByIdAsyncUsingParameters:(EmployeeServiceSvc_GetCLogCurrencyById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogCurrencyRatesUsingParameters:(EmployeeServiceSvc_GetAllCLogCurrencyRates *)aParameters ;
- (void)GetAllCLogCurrencyRatesAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogCurrencyRates *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetCLogCurrencyRateByIdUsingParameters:(EmployeeServiceSvc_GetCLogCurrencyRateById *)aParameters ;
- (void)GetCLogCurrencyRateByIdAsyncUsingParameters:(EmployeeServiceSvc_GetCLogCurrencyRateById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllSysEmpProfileGroupLayersUsingParameters:(EmployeeServiceSvc_GetAllSysEmpProfileGroupLayers *)aParameters ;
- (void)GetAllSysEmpProfileGroupLayersAsyncUsingParameters:(EmployeeServiceSvc_GetAllSysEmpProfileGroupLayers *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetSysEmpProfileLayersByGroupIdUsingParameters:(EmployeeServiceSvc_GetSysEmpProfileLayersByGroupId *)aParameters ;
- (void)GetSysEmpProfileLayersByGroupIdAsyncUsingParameters:(EmployeeServiceSvc_GetSysEmpProfileLayersByGroupId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetSysEmpProfileLayerUrlsByLayerIdUsingParameters:(EmployeeServiceSvc_GetSysEmpProfileLayerUrlsByLayerId *)aParameters ;
- (void)GetSysEmpProfileLayerUrlsByLayerIdAsyncUsingParameters:(EmployeeServiceSvc_GetSysEmpProfileLayerUrlsByLayerId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetSysEmpProfileLayerUrlsByTypeByLayerIdUsingParameters:(EmployeeServiceSvc_GetSysEmpProfileLayerUrlsByTypeByLayerId *)aParameters ;
- (void)GetSysEmpProfileLayerUrlsByTypeByLayerIdAsyncUsingParameters:(EmployeeServiceSvc_GetSysEmpProfileLayerUrlsByTypeByLayerId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewSysEmpProfileLayerByTypeByLayerIdUsingParameters:(EmployeeServiceSvc_GetViewSysEmpProfileLayerByTypeByLayerId *)aParameters ;
- (void)GetViewSysEmpProfileLayerByTypeByLayerIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewSysEmpProfileLayerByTypeByLayerId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogEmployeeTypesUsingParameters:(EmployeeServiceSvc_GetAllCLogEmployeeTypes *)aParameters ;
- (void)GetAllCLogEmployeeTypesAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogEmployeeTypes *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetCLogEmployeeTypeByIdUsingParameters:(EmployeeServiceSvc_GetCLogEmployeeTypeById *)aParameters ;
- (void)GetCLogEmployeeTypeByIdAsyncUsingParameters:(EmployeeServiceSvc_GetCLogEmployeeTypeById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogMaritalStatusesUsingParameters:(EmployeeServiceSvc_GetAllCLogMaritalStatuses *)aParameters ;
- (void)GetAllCLogMaritalStatusesAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogMaritalStatuses *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogCountriesUsingParameters:(EmployeeServiceSvc_GetAllCLogCountries *)aParameters ;
- (void)GetAllCLogCountriesAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogCountries *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetCLogCountryByIdUsingParameters:(EmployeeServiceSvc_GetCLogCountryById *)aParameters ;
- (void)GetCLogCountryByIdAsyncUsingParameters:(EmployeeServiceSvc_GetCLogCountryById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllOrgUnitsUsingParameters:(EmployeeServiceSvc_GetAllOrgUnits *)aParameters ;
- (void)GetAllOrgUnitsAsyncUsingParameters:(EmployeeServiceSvc_GetAllOrgUnits *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetViewOrgUnitJobsByOrgUnitIdUsingParameters:(EmployeeServiceSvc_GetViewOrgUnitJobsByOrgUnitId *)aParameters ;
- (void)GetViewOrgUnitJobsByOrgUnitIdAsyncUsingParameters:(EmployeeServiceSvc_GetViewOrgUnitJobsByOrgUnitId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetOrgWorkLevelsByOrgJobIdUsingParameters:(EmployeeServiceSvc_GetOrgWorkLevelsByOrgJobId *)aParameters ;
- (void)GetOrgWorkLevelsByOrgJobIdAsyncUsingParameters:(EmployeeServiceSvc_GetOrgWorkLevelsByOrgJobId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogCareersUsingParameters:(EmployeeServiceSvc_GetAllCLogCareers *)aParameters ;
- (void)GetAllCLogCareersAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogCareers *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetOrgJobPositionByIdUsingParameters:(EmployeeServiceSvc_GetOrgJobPositionById *)aParameters ;
- (void)GetOrgJobPositionByIdAsyncUsingParameters:(EmployeeServiceSvc_GetOrgJobPositionById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetOrgJobPositionsByOrgUnitIdUsingParameters:(EmployeeServiceSvc_GetOrgJobPositionsByOrgUnitId *)aParameters ;
- (void)GetOrgJobPositionsByOrgUnitIdAsyncUsingParameters:(EmployeeServiceSvc_GetOrgJobPositionsByOrgUnitId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetOrgJobPositionsByOrgUnitIdAndOrgJobIdUsingParameters:(EmployeeServiceSvc_GetOrgJobPositionsByOrgUnitIdAndOrgJobId *)aParameters ;
- (void)GetOrgJobPositionsByOrgUnitIdAndOrgJobIdAsyncUsingParameters:(EmployeeServiceSvc_GetOrgJobPositionsByOrgUnitIdAndOrgJobId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogRatingsUsingParameters:(EmployeeServiceSvc_GetAllCLogRatings *)aParameters ;
- (void)GetAllCLogRatingsAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogRatings *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogDegreesUsingParameters:(EmployeeServiceSvc_GetAllCLogDegrees *)aParameters ;
- (void)GetAllCLogDegreesAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogDegrees *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogMajorsUsingParameters:(EmployeeServiceSvc_GetAllCLogMajors *)aParameters ;
- (void)GetAllCLogMajorsAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogMajors *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetCLogMajorByIdUsingParameters:(EmployeeServiceSvc_GetCLogMajorById *)aParameters ;
- (void)GetCLogMajorByIdAsyncUsingParameters:(EmployeeServiceSvc_GetCLogMajorById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogTrainingCentersUsingParameters:(EmployeeServiceSvc_GetAllCLogTrainingCenters *)aParameters ;
- (void)GetAllCLogTrainingCentersAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogTrainingCenters *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogTrainingsUsingParameters:(EmployeeServiceSvc_GetAllCLogTrainings *)aParameters ;
- (void)GetAllCLogTrainingsAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogTrainings *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllOrgDegreesUsingParameters:(EmployeeServiceSvc_GetAllOrgDegrees *)aParameters ;
- (void)GetAllOrgDegreesAsyncUsingParameters:(EmployeeServiceSvc_GetAllOrgDegrees *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetOrgDegreeByIdUsingParameters:(EmployeeServiceSvc_GetOrgDegreeById *)aParameters ;
- (void)GetOrgDegreeByIdAsyncUsingParameters:(EmployeeServiceSvc_GetOrgDegreeById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllOrgDegreeRanksUsingParameters:(EmployeeServiceSvc_GetAllOrgDegreeRanks *)aParameters ;
- (void)GetAllOrgDegreeRanksAsyncUsingParameters:(EmployeeServiceSvc_GetAllOrgDegreeRanks *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogHospitalsUsingParameters:(EmployeeServiceSvc_GetAllCLogHospitals *)aParameters ;
- (void)GetAllCLogHospitalsAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogHospitals *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetCLogHospitalByIdUsingParameters:(EmployeeServiceSvc_GetCLogHospitalById *)aParameters ;
- (void)GetCLogHospitalByIdAsyncUsingParameters:(EmployeeServiceSvc_GetCLogHospitalById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogEthnicitiesUsingParameters:(EmployeeServiceSvc_GetAllCLogEthnicities *)aParameters ;
- (void)GetAllCLogEthnicitiesAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogEthnicities *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetCLogEthnicityByIdUsingParameters:(EmployeeServiceSvc_GetCLogEthnicityById *)aParameters ;
- (void)GetCLogEthnicityByIdAsyncUsingParameters:(EmployeeServiceSvc_GetCLogEthnicityById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogReligionsUsingParameters:(EmployeeServiceSvc_GetAllCLogReligions *)aParameters ;
- (void)GetAllCLogReligionsAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogReligions *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetCLogReligionByIdUsingParameters:(EmployeeServiceSvc_GetCLogReligionById *)aParameters ;
- (void)GetCLogReligionByIdAsyncUsingParameters:(EmployeeServiceSvc_GetCLogReligionById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogLanguagesUsingParameters:(EmployeeServiceSvc_GetAllCLogLanguages *)aParameters ;
- (void)GetAllCLogLanguagesAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogLanguages *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetCLogLanguageByIdUsingParameters:(EmployeeServiceSvc_GetCLogLanguageById *)aParameters ;
- (void)GetCLogLanguageByIdAsyncUsingParameters:(EmployeeServiceSvc_GetCLogLanguageById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogEducationLevelsUsingParameters:(EmployeeServiceSvc_GetAllCLogEducationLevels *)aParameters ;
- (void)GetAllCLogEducationLevelsAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogEducationLevels *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetCLogEducationLevelByIdUsingParameters:(EmployeeServiceSvc_GetCLogEducationLevelById *)aParameters ;
- (void)GetCLogEducationLevelByIdAsyncUsingParameters:(EmployeeServiceSvc_GetCLogEducationLevelById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllOrgQualificationsUsingParameters:(EmployeeServiceSvc_GetAllOrgQualifications *)aParameters ;
- (void)GetAllOrgQualificationsAsyncUsingParameters:(EmployeeServiceSvc_GetAllOrgQualifications *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogEmpCulturalLevelsUsingParameters:(EmployeeServiceSvc_GetAllCLogEmpCulturalLevels *)aParameters ;
- (void)GetAllCLogEmpCulturalLevelsAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogEmpCulturalLevels *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetCLogEmpCulturalLevelByIdUsingParameters:(EmployeeServiceSvc_GetCLogEmpCulturalLevelById *)aParameters ;
- (void)GetCLogEmpCulturalLevelByIdAsyncUsingParameters:(EmployeeServiceSvc_GetCLogEmpCulturalLevelById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogMaritalStatusUsingParameters:(EmployeeServiceSvc_GetAllCLogMaritalStatus *)aParameters ;
- (void)GetAllCLogMaritalStatusAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogMaritalStatus *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetCLogMaritalStatusByIdUsingParameters:(EmployeeServiceSvc_GetCLogMaritalStatusById *)aParameters ;
- (void)GetCLogMaritalStatusByIdAsyncUsingParameters:(EmployeeServiceSvc_GetCLogMaritalStatusById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogBanksUsingParameters:(EmployeeServiceSvc_GetAllCLogBanks *)aParameters ;
- (void)GetAllCLogBanksAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogBanks *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetCLogBankByCLogBankBranchIdUsingParameters:(EmployeeServiceSvc_GetCLogBankByCLogBankBranchId *)aParameters ;
- (void)GetCLogBankByCLogBankBranchIdAsyncUsingParameters:(EmployeeServiceSvc_GetCLogBankByCLogBankBranchId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogBankBranchsUsingParameters:(EmployeeServiceSvc_GetAllCLogBankBranchs *)aParameters ;
- (void)GetAllCLogBankBranchsAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogBankBranchs *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetCLogBankBranchByIdUsingParameters:(EmployeeServiceSvc_GetCLogBankBranchById *)aParameters ;
- (void)GetCLogBankBranchByIdAsyncUsingParameters:(EmployeeServiceSvc_GetCLogBankBranchById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetCLogBankBranchesByCLogBankIdUsingParameters:(EmployeeServiceSvc_GetCLogBankBranchesByCLogBankId *)aParameters ;
- (void)GetCLogBankBranchesByCLogBankIdAsyncUsingParameters:(EmployeeServiceSvc_GetCLogBankBranchesByCLogBankId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogCourseStatusUsingParameters:(EmployeeServiceSvc_GetAllCLogCourseStatus *)aParameters ;
- (void)GetAllCLogCourseStatusAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogCourseStatus *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetCLogCourseStatusByIdUsingParameters:(EmployeeServiceSvc_GetCLogCourseStatusById *)aParameters ;
- (void)GetCLogCourseStatusByIdAsyncUsingParameters:(EmployeeServiceSvc_GetCLogCourseStatusById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogEmpWorkingStatusUsingParameters:(EmployeeServiceSvc_GetAllCLogEmpWorkingStatus *)aParameters ;
- (void)GetAllCLogEmpWorkingStatusAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogEmpWorkingStatus *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetCLogEmpWorkingStatusByIdUsingParameters:(EmployeeServiceSvc_GetCLogEmpWorkingStatusById *)aParameters ;
- (void)GetCLogEmpWorkingStatusByIdAsyncUsingParameters:(EmployeeServiceSvc_GetCLogEmpWorkingStatusById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogPersonalitiesUsingParameters:(EmployeeServiceSvc_GetAllCLogPersonalities *)aParameters ;
- (void)GetAllCLogPersonalitiesAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogPersonalities *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetCLogCLogPersonalityByIdUsingParameters:(EmployeeServiceSvc_GetCLogCLogPersonalityById *)aParameters ;
- (void)GetCLogCLogPersonalityByIdAsyncUsingParameters:(EmployeeServiceSvc_GetCLogCLogPersonalityById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllOrgProjectTypesUsingParameters:(EmployeeServiceSvc_GetAllOrgProjectTypes *)aParameters ;
- (void)GetAllOrgProjectTypesAsyncUsingParameters:(EmployeeServiceSvc_GetAllOrgProjectTypes *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetOrgProjectTypeByIdUsingParameters:(EmployeeServiceSvc_GetOrgProjectTypeById *)aParameters ;
- (void)GetOrgProjectTypeByIdAsyncUsingParameters:(EmployeeServiceSvc_GetOrgProjectTypeById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllOrgTimeInChargesUsingParameters:(EmployeeServiceSvc_GetAllOrgTimeInCharges *)aParameters ;
- (void)GetAllOrgTimeInChargesAsyncUsingParameters:(EmployeeServiceSvc_GetAllOrgTimeInCharges *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetOrgTimeInChargeByIdUsingParameters:(EmployeeServiceSvc_GetOrgTimeInChargeById *)aParameters ;
- (void)GetOrgTimeInChargeByIdAsyncUsingParameters:(EmployeeServiceSvc_GetOrgTimeInChargeById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllOrgSkillsUsingParameters:(EmployeeServiceSvc_GetAllOrgSkills *)aParameters ;
- (void)GetAllOrgSkillsAsyncUsingParameters:(EmployeeServiceSvc_GetAllOrgSkills *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetOrgSkillByIdUsingParameters:(EmployeeServiceSvc_GetOrgSkillById *)aParameters ;
- (void)GetOrgSkillByIdAsyncUsingParameters:(EmployeeServiceSvc_GetOrgSkillById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllOrgSkillTypesUsingParameters:(EmployeeServiceSvc_GetAllOrgSkillTypes *)aParameters ;
- (void)GetAllOrgSkillTypesAsyncUsingParameters:(EmployeeServiceSvc_GetAllOrgSkillTypes *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetOrgSkillTypeByIdUsingParameters:(EmployeeServiceSvc_GetOrgSkillTypeById *)aParameters ;
- (void)GetOrgSkillTypeByIdAsyncUsingParameters:(EmployeeServiceSvc_GetOrgSkillTypeById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogCBFactorsUsingParameters:(EmployeeServiceSvc_GetAllCLogCBFactors *)aParameters ;
- (void)GetAllCLogCBFactorsAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogCBFactors *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetCLogCBFactorByIdUsingParameters:(EmployeeServiceSvc_GetCLogCBFactorById *)aParameters ;
- (void)GetCLogCBFactorByIdAsyncUsingParameters:(EmployeeServiceSvc_GetCLogCBFactorById *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllViewCLogCBFactorsUsingParameters:(EmployeeServiceSvc_GetAllViewCLogCBFactors *)aParameters ;
- (void)GetAllViewCLogCBFactorsAsyncUsingParameters:(EmployeeServiceSvc_GetAllViewCLogCBFactors *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllCLogEmpOffTypesUsingParameters:(EmployeeServiceSvc_GetAllCLogEmpOffTypes *)aParameters ;
- (void)GetAllCLogEmpOffTypesAsyncUsingParameters:(EmployeeServiceSvc_GetAllCLogEmpOffTypes *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllOrgKnowledgeUsingParameters:(EmployeeServiceSvc_GetAllOrgKnowledge *)aParameters ;
- (void)GetAllOrgKnowledgeAsyncUsingParameters:(EmployeeServiceSvc_GetAllOrgKnowledge *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllOrgKnowledgeByMajorIdUsingParameters:(EmployeeServiceSvc_GetAllOrgKnowledgeByMajorId *)aParameters ;
- (void)GetAllOrgKnowledgeByMajorIdAsyncUsingParameters:(EmployeeServiceSvc_GetAllOrgKnowledgeByMajorId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllOrgUnitByUserUsingParameters:(EmployeeServiceSvc_GetAllOrgUnitByUser *)aParameters ;
- (void)GetAllOrgUnitByUserAsyncUsingParameters:(EmployeeServiceSvc_GetAllOrgUnitByUser *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllEmpBasicInfomationByOrgUnitIdsUsingParameters:(EmployeeServiceSvc_GetAllEmpBasicInfomationByOrgUnitIds *)aParameters ;
- (void)GetAllEmpBasicInfomationByOrgUnitIdsAsyncUsingParameters:(EmployeeServiceSvc_GetAllEmpBasicInfomationByOrgUnitIds *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)GetAllV_EmpBasicInfoByOrgUnitIdUsingParameters:(EmployeeServiceSvc_GetAllV_EmpBasicInfoByOrgUnitId *)aParameters ;
- (void)GetAllV_EmpBasicInfoByOrgUnitIdAsyncUsingParameters:(EmployeeServiceSvc_GetAllV_EmpBasicInfoByOrgUnitId *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)FTS_EmpProfileSearchAllUsingParameters:(EmployeeServiceSvc_FTS_EmpProfileSearchAll *)aParameters ;
- (void)FTS_EmpProfileSearchAllAsyncUsingParameters:(EmployeeServiceSvc_FTS_EmpProfileSearchAll *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)FTS_EmpProfileBasicUsingParameters:(EmployeeServiceSvc_FTS_EmpProfileBasic *)aParameters ;
- (void)FTS_EmpProfileBasicAsyncUsingParameters:(EmployeeServiceSvc_FTS_EmpProfileBasic *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)FTS_EmpProfileContactUsingParameters:(EmployeeServiceSvc_FTS_EmpProfileContact *)aParameters ;
- (void)FTS_EmpProfileContactAsyncUsingParameters:(EmployeeServiceSvc_FTS_EmpProfileContact *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)FTS_EmpProfileDegreeUsingParameters:(EmployeeServiceSvc_FTS_EmpProfileDegree *)aParameters ;
- (void)FTS_EmpProfileDegreeAsyncUsingParameters:(EmployeeServiceSvc_FTS_EmpProfileDegree *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)FTS_EmpProfileEduQualificationUsingParameters:(EmployeeServiceSvc_FTS_EmpProfileEduQualification *)aParameters ;
- (void)FTS_EmpProfileEduQualificationAsyncUsingParameters:(EmployeeServiceSvc_FTS_EmpProfileEduQualification *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)FTS_EmpProfileForeignLanguageUsingParameters:(EmployeeServiceSvc_FTS_EmpProfileForeignLanguage *)aParameters ;
- (void)FTS_EmpProfileForeignLanguageAsyncUsingParameters:(EmployeeServiceSvc_FTS_EmpProfileForeignLanguage *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IEmployeeServiceBindingResponse *)FTS_EmpProfileJobPositionUsingParameters:(EmployeeServiceSvc_FTS_EmpProfileJobPosition *)aParameters ;
- (void)FTS_EmpProfileJobPositionAsyncUsingParameters:(EmployeeServiceSvc_FTS_EmpProfileJobPosition *)aParameters  delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)responseDelegate;
@end
@interface BasicHttpBinding_IEmployeeServiceBindingOperation : NSOperation {
	BasicHttpBinding_IEmployeeServiceBinding *binding;
	BasicHttpBinding_IEmployeeServiceBindingResponse *response;
	id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate> delegate;
	NSMutableData *responseData;
	NSURLConnection *urlConnection;
}
@property (retain) BasicHttpBinding_IEmployeeServiceBinding *binding;
@property (readonly) BasicHttpBinding_IEmployeeServiceBindingResponse *response;
@property (nonatomic, assign) id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate> delegate;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, retain) NSURLConnection *urlConnection;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_FTS_EmpProfileOrgUnit : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_FTS_EmpProfileOrgUnit * parameters;
}
@property (retain) EmployeeServiceSvc_FTS_EmpProfileOrgUnit * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_FTS_EmpProfileOrgUnit *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_FTS_EmpContract : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_FTS_EmpContract * parameters;
}
@property (retain) EmployeeServiceSvc_FTS_EmpContract * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_FTS_EmpContract *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_FTS_EmpOff : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_FTS_EmpOff * parameters;
}
@property (retain) EmployeeServiceSvc_FTS_EmpOff * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_FTS_EmpOff *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllEmployeeByAdvancedSearch : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllEmployeeByAdvancedSearch * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllEmployeeByAdvancedSearch * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllEmployeeByAdvancedSearch *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllEmployeeProfileByAdvancedSearch : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllEmployeeProfileByAdvancedSearch * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllEmployeeProfileByAdvancedSearch * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllEmployeeProfileByAdvancedSearch *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllEmployeeContractByAdvancedSearch : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllEmployeeContractByAdvancedSearch * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllEmployeeContractByAdvancedSearch * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllEmployeeContractByAdvancedSearch *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllEmployeeOffByAdvancedSearch : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllEmployeeOffByAdvancedSearch * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllEmployeeOffByAdvancedSearch * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllEmployeeOffByAdvancedSearch *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllEmployer : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllEmployer * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllEmployer * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllEmployer *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCompany : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCompany * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCompany * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCompany *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetCompanyInfo : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetCompanyInfo * parameters;
}
@property (retain) EmployeeServiceSvc_GetCompanyInfo * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetCompanyInfo *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllEmpContractSummary : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllEmpContractSummary * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllEmpContractSummary * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllEmpContractSummary *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllEmpContract : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllEmpContract * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllEmpContract * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllEmpContract *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllEmpContractBySkipTake : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllEmpContractBySkipTake * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllEmpContractBySkipTake * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllEmpContractBySkipTake *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetEmpContract : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetEmpContract * parameters;
}
@property (retain) EmployeeServiceSvc_GetEmpContract * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetEmpContract *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetEmpContractByContractId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetEmpContractByContractId * parameters;
}
@property (retain) EmployeeServiceSvc_GetEmpContractByContractId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetEmpContractByContractId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_CountEmpContract : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_CountEmpContract * parameters;
}
@property (retain) EmployeeServiceSvc_CountEmpContract * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_CountEmpContract *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetEmpBasicProfileByEmployeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetEmpBasicProfileByEmployeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetEmpBasicProfileByEmployeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetEmpBasicProfileByEmployeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetWokingTimeByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetWokingTimeByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetWokingTimeByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetWokingTimeByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetContractType : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetContractType * parameters;
}
@property (retain) EmployeeServiceSvc_GetContractType * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetContractType *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_CheckExistEmpContractCode : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_CheckExistEmpContractCode * parameters;
}
@property (retain) EmployeeServiceSvc_CheckExistEmpContractCode * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_CheckExistEmpContractCode *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllEmpContractTerm : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllEmpContractTerm * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllEmpContractTerm * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllEmpContractTerm *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetEmpContractTermUndefined : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetEmpContractTermUndefined * parameters;
}
@property (retain) EmployeeServiceSvc_GetEmpContractTermUndefined * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetEmpContractTermUndefined *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetEmpContractTermIdentified : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetEmpContractTermIdentified * parameters;
}
@property (retain) EmployeeServiceSvc_GetEmpContractTermIdentified * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetEmpContractTermIdentified *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_CreateEmpContract : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_CreateEmpContract * parameters;
}
@property (retain) EmployeeServiceSvc_CreateEmpContract * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_CreateEmpContract *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_UpdateEmpContract : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_UpdateEmpContract * parameters;
}
@property (retain) EmployeeServiceSvc_UpdateEmpContract * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_UpdateEmpContract *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_EndEmpContracts : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_EndEmpContracts * parameters;
}
@property (retain) EmployeeServiceSvc_EndEmpContracts * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_EndEmpContracts *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_EndContracts : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_EndContracts * parameters;
}
@property (retain) EmployeeServiceSvc_EndContracts * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_EndContracts *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_DeleteEmpContracts : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_DeleteEmpContracts * parameters;
}
@property (retain) EmployeeServiceSvc_DeleteEmpContracts * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_DeleteEmpContracts *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_AppraiseContract : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_AppraiseContract * parameters;
}
@property (retain) EmployeeServiceSvc_AppraiseContract * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_AppraiseContract *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllEmpNotificationConstant : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllEmpNotificationConstant * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllEmpNotificationConstant * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllEmpNotificationConstant *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListNotifyEmpBirthday : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListNotifyEmpBirthday * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListNotifyEmpBirthday * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListNotifyEmpBirthday *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetValueNotifyEmpBirthday : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetValueNotifyEmpBirthday * parameters;
}
@property (retain) EmployeeServiceSvc_GetValueNotifyEmpBirthday * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetValueNotifyEmpBirthday *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListEmpBirthday : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListEmpBirthday * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListEmpBirthday * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListEmpBirthday *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListNotifyEmpAppoinmentExpiration : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListNotifyEmpAppoinmentExpiration * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListNotifyEmpAppoinmentExpiration * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListNotifyEmpAppoinmentExpiration *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetValueNotifyEmpAppoinmentExpiration : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetValueNotifyEmpAppoinmentExpiration * parameters;
}
@property (retain) EmployeeServiceSvc_GetValueNotifyEmpAppoinmentExpiration * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetValueNotifyEmpAppoinmentExpiration *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListEmpAppoinmentExpiration : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListEmpAppoinmentExpiration * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListEmpAppoinmentExpiration * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListEmpAppoinmentExpiration *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetValueNotifyEmpIncreaseSalary : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetValueNotifyEmpIncreaseSalary * parameters;
}
@property (retain) EmployeeServiceSvc_GetValueNotifyEmpIncreaseSalary * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetValueNotifyEmpIncreaseSalary *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListNotifyEmpToOffical : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListNotifyEmpToOffical * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListNotifyEmpToOffical * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListNotifyEmpToOffical *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListEmpToOffical : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListEmpToOffical * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListEmpToOffical * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListEmpToOffical *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetValueNotifyEmpToOffical : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetValueNotifyEmpToOffical * parameters;
}
@property (retain) EmployeeServiceSvc_GetValueNotifyEmpToOffical * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetValueNotifyEmpToOffical *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListNotifyEmpExpirationPassport : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListNotifyEmpExpirationPassport * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListNotifyEmpExpirationPassport * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListNotifyEmpExpirationPassport *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListEmpExpirationPassport : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListEmpExpirationPassport * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListEmpExpirationPassport * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListEmpExpirationPassport *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetValueNotifyEmpExpirationPassport : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetValueNotifyEmpExpirationPassport * parameters;
}
@property (retain) EmployeeServiceSvc_GetValueNotifyEmpExpirationPassport * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetValueNotifyEmpExpirationPassport *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListNotifyEmpParticipateTraining : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListNotifyEmpParticipateTraining * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListNotifyEmpParticipateTraining * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListNotifyEmpParticipateTraining *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListEmpParticipateTraining : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListEmpParticipateTraining * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListEmpParticipateTraining * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListEmpParticipateTraining *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetValueNotifyEmpParticipateTraining : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetValueNotifyEmpParticipateTraining * parameters;
}
@property (retain) EmployeeServiceSvc_GetValueNotifyEmpParticipateTraining * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetValueNotifyEmpParticipateTraining *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListNotifyEmpExpirationDegree : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListNotifyEmpExpirationDegree * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListNotifyEmpExpirationDegree * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListNotifyEmpExpirationDegree *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListEmpExpirationDegree : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListEmpExpirationDegree * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListEmpExpirationDegree * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListEmpExpirationDegree *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetValueNotifyExpirationDegree : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetValueNotifyExpirationDegree * parameters;
}
@property (retain) EmployeeServiceSvc_GetValueNotifyExpirationDegree * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetValueNotifyExpirationDegree *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListNotifyEmpExpirationContract : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListNotifyEmpExpirationContract * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListNotifyEmpExpirationContract * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListNotifyEmpExpirationContract *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListEmpExpirationContract : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListEmpExpirationContract * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListEmpExpirationContract * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListEmpExpirationContract *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetValueNotifyExpirationContract : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetValueNotifyExpirationContract * parameters;
}
@property (retain) EmployeeServiceSvc_GetValueNotifyExpirationContract * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetValueNotifyExpirationContract *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListNotifyEmpWaitReSignedContract : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListNotifyEmpWaitReSignedContract * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListNotifyEmpWaitReSignedContract * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListNotifyEmpWaitReSignedContract *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListEmpWaitReSignedContract : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListEmpWaitReSignedContract * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListEmpWaitReSignedContract * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListEmpWaitReSignedContract *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetValueNotifyWaitResignedContract : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetValueNotifyWaitResignedContract * parameters;
}
@property (retain) EmployeeServiceSvc_GetValueNotifyWaitResignedContract * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetValueNotifyWaitResignedContract *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListNotifyEmpLeave : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListNotifyEmpLeave * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListNotifyEmpLeave * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListNotifyEmpLeave *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListEmpLeave : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListEmpLeave * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListEmpLeave * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListEmpLeave *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetValueNotifyEmpLeave : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetValueNotifyEmpLeave * parameters;
}
@property (retain) EmployeeServiceSvc_GetValueNotifyEmpLeave * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetValueNotifyEmpLeave *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListEmpMaterityLeave : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListEmpMaterityLeave * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListEmpMaterityLeave * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListEmpMaterityLeave *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListEmpWillMaterityLeave : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListEmpWillMaterityLeave * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListEmpWillMaterityLeave * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListEmpWillMaterityLeave *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListEmpWillPregnancyMonth7thLeave : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListEmpWillPregnancyMonth7thLeave * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListEmpWillPregnancyMonth7thLeave * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListEmpWillPregnancyMonth7thLeave *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListEmpRaisingChildExpiration : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListEmpRaisingChildExpiration * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListEmpRaisingChildExpiration * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListEmpRaisingChildExpiration *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListEmpFamilyDependentExpiration : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListEmpFamilyDependentExpiration * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListEmpFamilyDependentExpiration * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListEmpFamilyDependentExpiration *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListNotifyEmpWillChildBirth : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListNotifyEmpWillChildBirth * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListNotifyEmpWillChildBirth * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListNotifyEmpWillChildBirth *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListEmpWillChildBirth : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListEmpWillChildBirth * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListEmpWillChildBirth * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListEmpWillChildBirth *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetValueNotifyWillChildBirth : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetValueNotifyWillChildBirth * parameters;
}
@property (retain) EmployeeServiceSvc_GetValueNotifyWillChildBirth * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetValueNotifyWillChildBirth *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListNotifyLongTermLeave : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListNotifyLongTermLeave * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListNotifyLongTermLeave * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListNotifyLongTermLeave *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListLongTermLeave : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListLongTermLeave * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListLongTermLeave * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListLongTermLeave *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetValueNotifyLongTermLeave : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetValueNotifyLongTermLeave * parameters;
}
@property (retain) EmployeeServiceSvc_GetValueNotifyLongTermLeave * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetValueNotifyLongTermLeave *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListNotifyEmpWillRetire : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListNotifyEmpWillRetire * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListNotifyEmpWillRetire * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListNotifyEmpWillRetire *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListEmpWillRetire : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListEmpWillRetire * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListEmpWillRetire * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListEmpWillRetire *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetvalueNotifyWillRetire : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetvalueNotifyWillRetire * parameters;
}
@property (retain) EmployeeServiceSvc_GetvalueNotifyWillRetire * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetvalueNotifyWillRetire *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetValueRetireAgeForMale : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetValueRetireAgeForMale * parameters;
}
@property (retain) EmployeeServiceSvc_GetValueRetireAgeForMale * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetValueRetireAgeForMale *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetValueRetireAgeForFamale : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetValueRetireAgeForFamale * parameters;
}
@property (retain) EmployeeServiceSvc_GetValueRetireAgeForFamale * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetValueRetireAgeForFamale *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListNotifyEmpWorkAfterChildBirth : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListNotifyEmpWorkAfterChildBirth * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListNotifyEmpWorkAfterChildBirth * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListNotifyEmpWorkAfterChildBirth *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListEmpWorkAfterChildBirth : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListEmpWorkAfterChildBirth * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListEmpWorkAfterChildBirth * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListEmpWorkAfterChildBirth *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetValueNotifyWorlAfterChildBirth : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetValueNotifyWorlAfterChildBirth * parameters;
}
@property (retain) EmployeeServiceSvc_GetValueNotifyWorlAfterChildBirth * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetValueNotifyWorlAfterChildBirth *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetJsonListEmpWorkAfterLeave : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetJsonListEmpWorkAfterLeave * parameters;
}
@property (retain) EmployeeServiceSvc_GetJsonListEmpWorkAfterLeave * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetJsonListEmpWorkAfterLeave *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetEmpOffProfileById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetEmpOffProfileById * parameters;
}
@property (retain) EmployeeServiceSvc_GetEmpOffProfileById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetEmpOffProfileById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GETAllViewEmpOffProfile : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GETAllViewEmpOffProfile * parameters;
}
@property (retain) EmployeeServiceSvc_GETAllViewEmpOffProfile * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GETAllViewEmpOffProfile *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmpOffProfileById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmpOffProfileById * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmpOffProfileById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmpOffProfileById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_AddEmpOffProfileByView : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_AddEmpOffProfileByView * parameters;
}
@property (retain) EmployeeServiceSvc_AddEmpOffProfileByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_AddEmpOffProfileByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_UpdateEmpOffProfileByView : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_UpdateEmpOffProfileByView * parameters;
}
@property (retain) EmployeeServiceSvc_UpdateEmpOffProfileByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_UpdateEmpOffProfileByView *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_DeleteEmpOffProfileById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_DeleteEmpOffProfileById * parameters;
}
@property (retain) EmployeeServiceSvc_DeleteEmpOffProfileById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_DeleteEmpOffProfileById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogEquipment : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogEquipment * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogEquipment * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogEquipment *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogEquipmentType : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogEquipmentType * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogEquipmentType * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogEquipmentType *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogAssetsType : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogAssetsType * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogAssetsType * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogAssetsType *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllEquipmentByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllEquipmentByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllEquipmentByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllEquipmentByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllAssetsByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllAssetsByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllAssetsByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllAssetsByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllViewCLogEquipment : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllViewCLogEquipment * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllViewCLogEquipment * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllViewCLogEquipment *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllViewCLogAssets : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllViewCLogAssets * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllViewCLogAssets * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllViewCLogAssets *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllViewOrgUnitAssets : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllViewOrgUnitAssets * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllViewOrgUnitAssets * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllViewOrgUnitAssets *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllViewOrgUnitEquipment : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllViewOrgUnitEquipment * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllViewOrgUnitEquipment * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllViewOrgUnitEquipment *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllEmpHasAssetsByAssetsIdAndUnitId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllEmpHasAssetsByAssetsIdAndUnitId * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllEmpHasAssetsByAssetsIdAndUnitId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllEmpHasAssetsByAssetsIdAndUnitId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllEmpHasEquipmentByEquipmentIdAndUnitId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllEmpHasEquipmentByEquipmentIdAndUnitId * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllEmpHasEquipmentByEquipmentIdAndUnitId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllEmpHasEquipmentByEquipmentIdAndUnitId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllEmployee : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllEmployee * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllEmployee * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllEmployee *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCities : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCities * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCities * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCities *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllDistricts : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllDistricts * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllDistricts * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllDistricts *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCountries : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCountries * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCountries * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCountries *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_CreateAddress : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_CreateAddress * parameters;
}
@property (retain) EmployeeServiceSvc_CreateAddress * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_CreateAddress *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetDirectReportAndChildById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetDirectReportAndChildById * parameters;
}
@property (retain) EmployeeServiceSvc_GetDirectReportAndChildById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetDirectReportAndChildById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllDirectReport : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllDirectReport * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllDirectReport * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllDirectReport *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetEmployeeSearchResult : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetEmployeeSearchResult * parameters;
}
@property (retain) EmployeeServiceSvc_GetEmployeeSearchResult * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetEmployeeSearchResult *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetDirectReportByChildId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetDirectReportByChildId * parameters;
}
@property (retain) EmployeeServiceSvc_GetDirectReportByChildId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetDirectReportByChildId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetUserIdByEmployeeCode : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetUserIdByEmployeeCode * parameters;
}
@property (retain) EmployeeServiceSvc_GetUserIdByEmployeeCode * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetUserIdByEmployeeCode *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetLeaderOfOrg : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetLeaderOfOrg * parameters;
}
@property (retain) EmployeeServiceSvc_GetLeaderOfOrg * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetLeaderOfOrg *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetInforOfEmps : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetInforOfEmps * parameters;
}
@property (retain) EmployeeServiceSvc_GetInforOfEmps * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetInforOfEmps *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetEmpInGender : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetEmpInGender * parameters;
}
@property (retain) EmployeeServiceSvc_GetEmpInGender * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetEmpInGender *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetNewestEmployee : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetNewestEmployee * parameters;
}
@property (retain) EmployeeServiceSvc_GetNewestEmployee * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetNewestEmployee *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllEmpAssignedGroup : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllEmpAssignedGroup * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllEmpAssignedGroup * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllEmpAssignedGroup *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_AssignEmpToGroup : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_AssignEmpToGroup * parameters;
}
@property (retain) EmployeeServiceSvc_AssignEmpToGroup * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_AssignEmpToGroup *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_RevokeEmpFromGroup : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_RevokeEmpFromGroup * parameters;
}
@property (retain) EmployeeServiceSvc_RevokeEmpFromGroup * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_RevokeEmpFromGroup *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetDirectReportOfEmployeeByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetDirectReportOfEmployeeByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetDirectReportOfEmployeeByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetDirectReportOfEmployeeByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllDirectReportOfEmpByEmpId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllDirectReportOfEmpByEmpId * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllDirectReportOfEmpByEmpId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllDirectReportOfEmpByEmpId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmpBasicInformationById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmpBasicInformationById * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmpBasicInformationById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmpBasicInformationById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllViewEmployeesByUser : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllViewEmployeesByUser * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllViewEmployeesByUser * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllViewEmployeesByUser *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmployeesByUser : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmployeesByUser * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmployeesByUser * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmployeesByUser *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllViewEmployees : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllViewEmployees * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllViewEmployees * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllViewEmployees *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmployeesByPage : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmployeesByPage * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmployeesByPage * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmployeesByPage *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmployeePageNumber : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmployeePageNumber * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmployeePageNumber * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmployeePageNumber *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmployeeById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmployeeById * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmployeeById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmployeeById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmployeeProfileByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmployeeProfileByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmployeeProfileByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmployeeProfileByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllEmployees : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllEmployees * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllEmployees * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllEmployees *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetEmployeeById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetEmployeeById * parameters;
}
@property (retain) EmployeeServiceSvc_GetEmployeeById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetEmployeeById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_SubmitCreateNewEmployee : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_SubmitCreateNewEmployee * parameters;
}
@property (retain) EmployeeServiceSvc_SubmitCreateNewEmployee * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_SubmitCreateNewEmployee *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_CheckValidEmpCode : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_CheckValidEmpCode * parameters;
}
@property (retain) EmployeeServiceSvc_CheckValidEmpCode * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_CheckValidEmpCode *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_CreateEmployee : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_CreateEmployee * parameters;
}
@property (retain) EmployeeServiceSvc_CreateEmployee * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_CreateEmployee *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllLeader : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllLeader * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllLeader * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllLeader *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_CreateEmpProfileJobPosition : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_CreateEmpProfileJobPosition * parameters;
}
@property (retain) EmployeeServiceSvc_CreateEmpProfileJobPosition * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_CreateEmpProfileJobPosition *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_CreateEmpProfileProcessOfWork : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_CreateEmpProfileProcessOfWork * parameters;
}
@property (retain) EmployeeServiceSvc_CreateEmpProfileProcessOfWork * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_CreateEmpProfileProcessOfWork *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_CreateEmpBasicProfile : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_CreateEmpBasicProfile * parameters;
}
@property (retain) EmployeeServiceSvc_CreateEmpBasicProfile * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_CreateEmpBasicProfile *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmpBasicProfilesByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmpBasicProfilesByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmpBasicProfilesByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmpBasicProfilesByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogFamilyRelationships : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogFamilyRelationships * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogFamilyRelationships * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogFamilyRelationships *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmpProfileWorkingExperiencesByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmpProfileWorkingExperiencesByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmpProfileWorkingExperiencesByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmpProfileWorkingExperiencesByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmpProfileSkillsByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmpProfileSkillsByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmpProfileSkillsByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmpProfileSkillsByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmpProfileJobPositionsByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmpProfileJobPositionsByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmpProfileJobPositionsByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmpProfileJobPositionsByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmpContractsByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmpContractsByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmpContractsByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmpContractsByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmpProfileBasicSalarysByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmpProfileBasicSalarysByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmpProfileBasicSalarysByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmpProfileBasicSalarysByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmpProfileHealthInsurancesByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmpProfileHealthInsurancesByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmpProfileHealthInsurancesByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmpProfileHealthInsurancesByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmpProfileWageTypesByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmpProfileWageTypesByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmpProfileWageTypesByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmpProfileWageTypesByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmpProfileWorkingFormsByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmpProfileWorkingFormsByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmpProfileWorkingFormsByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmpProfileWorkingFormsByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmpSocialInsuranceSalariesByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmpSocialInsuranceSalariesByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmpSocialInsuranceSalariesByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmpSocialInsuranceSalariesByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmpProfileBiographiesByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmpProfileBiographiesByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmpProfileBiographiesByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmpProfileBiographiesByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmpProfileExperiencesByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmpProfileExperiencesByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmpProfileExperiencesByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmpProfileExperiencesByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmpProfileProcessOfWorksByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmpProfileProcessOfWorksByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmpProfileProcessOfWorksByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmpProfileProcessOfWorksByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmpProfileContactsByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmpProfileContactsByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmpProfileContactsByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmpProfileContactsByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmpProfileSocialInsurancesByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmpProfileSocialInsurancesByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmpProfileSocialInsurancesByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmpProfileSocialInsurancesByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmpProfileLeaveRegimesByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmpProfileLeaveRegimesByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmpProfileLeaveRegimesByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmpProfileLeaveRegimesByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmpPerformanceAppraisalsByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmpPerformanceAppraisalsByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmpPerformanceAppraisalsByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmpPerformanceAppraisalsByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmpProfileQualificationsByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmpProfileQualificationsByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmpProfileQualificationsByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmpProfileQualificationsByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmpProfileForeignLanguagesByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmpProfileForeignLanguagesByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmpProfileForeignLanguagesByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmpProfileForeignLanguagesByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmpProfileTrainingsByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmpProfileTrainingsByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmpProfileTrainingsByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmpProfileTrainingsByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmpProfileDegreesByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmpProfileDegreesByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmpProfileDegreesByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmpProfileDegreesByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmpProfileDisciplinesByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmpProfileDisciplinesByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmpProfileDisciplinesByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmpProfileDisciplinesByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewEmEmpProfilePersonalitiesByEmployeeId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewEmEmpProfilePersonalitiesByEmployeeId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewEmEmpProfilePersonalitiesByEmployeeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewEmEmpProfilePersonalitiesByEmployeeId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllTimeWorkingForms : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllTimeWorkingForms * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllTimeWorkingForms * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllTimeWorkingForms *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllTimeWageTypes : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllTimeWageTypes * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllTimeWageTypes * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllTimeWageTypes *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetTimeWageTypeById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetTimeWageTypeById * parameters;
}
@property (retain) EmployeeServiceSvc_GetTimeWageTypeById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetTimeWageTypeById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogCBCompensationCategories : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogCBCompensationCategories * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogCBCompensationCategories * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogCBCompensationCategories *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetCLogCitiesByCLogCountryId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetCLogCitiesByCLogCountryId * parameters;
}
@property (retain) EmployeeServiceSvc_GetCLogCitiesByCLogCountryId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetCLogCitiesByCLogCountryId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogCities : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogCities * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogCities * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogCities *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetCLogCityById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetCLogCityById * parameters;
}
@property (retain) EmployeeServiceSvc_GetCLogCityById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetCLogCityById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogCurrencies : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogCurrencies * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogCurrencies * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogCurrencies *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetCLogCurrencyById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetCLogCurrencyById * parameters;
}
@property (retain) EmployeeServiceSvc_GetCLogCurrencyById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetCLogCurrencyById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogCurrencyRates : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogCurrencyRates * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogCurrencyRates * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogCurrencyRates *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetCLogCurrencyRateById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetCLogCurrencyRateById * parameters;
}
@property (retain) EmployeeServiceSvc_GetCLogCurrencyRateById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetCLogCurrencyRateById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllSysEmpProfileGroupLayers : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllSysEmpProfileGroupLayers * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllSysEmpProfileGroupLayers * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllSysEmpProfileGroupLayers *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetSysEmpProfileLayersByGroupId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetSysEmpProfileLayersByGroupId * parameters;
}
@property (retain) EmployeeServiceSvc_GetSysEmpProfileLayersByGroupId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetSysEmpProfileLayersByGroupId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetSysEmpProfileLayerUrlsByLayerId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetSysEmpProfileLayerUrlsByLayerId * parameters;
}
@property (retain) EmployeeServiceSvc_GetSysEmpProfileLayerUrlsByLayerId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetSysEmpProfileLayerUrlsByLayerId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetSysEmpProfileLayerUrlsByTypeByLayerId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetSysEmpProfileLayerUrlsByTypeByLayerId * parameters;
}
@property (retain) EmployeeServiceSvc_GetSysEmpProfileLayerUrlsByTypeByLayerId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetSysEmpProfileLayerUrlsByTypeByLayerId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewSysEmpProfileLayerByTypeByLayerId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewSysEmpProfileLayerByTypeByLayerId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewSysEmpProfileLayerByTypeByLayerId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewSysEmpProfileLayerByTypeByLayerId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogEmployeeTypes : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogEmployeeTypes * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogEmployeeTypes * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogEmployeeTypes *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetCLogEmployeeTypeById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetCLogEmployeeTypeById * parameters;
}
@property (retain) EmployeeServiceSvc_GetCLogEmployeeTypeById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetCLogEmployeeTypeById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogMaritalStatuses : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogMaritalStatuses * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogMaritalStatuses * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogMaritalStatuses *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogCountries : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogCountries * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogCountries * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogCountries *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetCLogCountryById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetCLogCountryById * parameters;
}
@property (retain) EmployeeServiceSvc_GetCLogCountryById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetCLogCountryById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllOrgUnits : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllOrgUnits * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllOrgUnits * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllOrgUnits *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetViewOrgUnitJobsByOrgUnitId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetViewOrgUnitJobsByOrgUnitId * parameters;
}
@property (retain) EmployeeServiceSvc_GetViewOrgUnitJobsByOrgUnitId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetViewOrgUnitJobsByOrgUnitId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetOrgWorkLevelsByOrgJobId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetOrgWorkLevelsByOrgJobId * parameters;
}
@property (retain) EmployeeServiceSvc_GetOrgWorkLevelsByOrgJobId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetOrgWorkLevelsByOrgJobId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogCareers : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogCareers * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogCareers * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogCareers *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetOrgJobPositionById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetOrgJobPositionById * parameters;
}
@property (retain) EmployeeServiceSvc_GetOrgJobPositionById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetOrgJobPositionById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetOrgJobPositionsByOrgUnitId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetOrgJobPositionsByOrgUnitId * parameters;
}
@property (retain) EmployeeServiceSvc_GetOrgJobPositionsByOrgUnitId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetOrgJobPositionsByOrgUnitId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetOrgJobPositionsByOrgUnitIdAndOrgJobId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetOrgJobPositionsByOrgUnitIdAndOrgJobId * parameters;
}
@property (retain) EmployeeServiceSvc_GetOrgJobPositionsByOrgUnitIdAndOrgJobId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetOrgJobPositionsByOrgUnitIdAndOrgJobId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogRatings : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogRatings * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogRatings * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogRatings *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogDegrees : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogDegrees * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogDegrees * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogDegrees *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogMajors : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogMajors * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogMajors * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogMajors *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetCLogMajorById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetCLogMajorById * parameters;
}
@property (retain) EmployeeServiceSvc_GetCLogMajorById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetCLogMajorById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogTrainingCenters : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogTrainingCenters * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogTrainingCenters * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogTrainingCenters *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogTrainings : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogTrainings * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogTrainings * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogTrainings *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllOrgDegrees : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllOrgDegrees * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllOrgDegrees * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllOrgDegrees *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetOrgDegreeById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetOrgDegreeById * parameters;
}
@property (retain) EmployeeServiceSvc_GetOrgDegreeById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetOrgDegreeById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllOrgDegreeRanks : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllOrgDegreeRanks * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllOrgDegreeRanks * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllOrgDegreeRanks *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogHospitals : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogHospitals * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogHospitals * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogHospitals *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetCLogHospitalById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetCLogHospitalById * parameters;
}
@property (retain) EmployeeServiceSvc_GetCLogHospitalById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetCLogHospitalById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogEthnicities : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogEthnicities * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogEthnicities * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogEthnicities *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetCLogEthnicityById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetCLogEthnicityById * parameters;
}
@property (retain) EmployeeServiceSvc_GetCLogEthnicityById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetCLogEthnicityById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogReligions : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogReligions * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogReligions * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogReligions *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetCLogReligionById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetCLogReligionById * parameters;
}
@property (retain) EmployeeServiceSvc_GetCLogReligionById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetCLogReligionById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogLanguages : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogLanguages * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogLanguages * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogLanguages *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetCLogLanguageById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetCLogLanguageById * parameters;
}
@property (retain) EmployeeServiceSvc_GetCLogLanguageById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetCLogLanguageById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogEducationLevels : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogEducationLevels * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogEducationLevels * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogEducationLevels *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetCLogEducationLevelById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetCLogEducationLevelById * parameters;
}
@property (retain) EmployeeServiceSvc_GetCLogEducationLevelById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetCLogEducationLevelById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllOrgQualifications : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllOrgQualifications * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllOrgQualifications * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllOrgQualifications *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogEmpCulturalLevels : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogEmpCulturalLevels * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogEmpCulturalLevels * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogEmpCulturalLevels *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetCLogEmpCulturalLevelById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetCLogEmpCulturalLevelById * parameters;
}
@property (retain) EmployeeServiceSvc_GetCLogEmpCulturalLevelById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetCLogEmpCulturalLevelById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogMaritalStatus : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogMaritalStatus * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogMaritalStatus * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogMaritalStatus *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetCLogMaritalStatusById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetCLogMaritalStatusById * parameters;
}
@property (retain) EmployeeServiceSvc_GetCLogMaritalStatusById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetCLogMaritalStatusById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogBanks : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogBanks * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogBanks * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogBanks *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetCLogBankByCLogBankBranchId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetCLogBankByCLogBankBranchId * parameters;
}
@property (retain) EmployeeServiceSvc_GetCLogBankByCLogBankBranchId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetCLogBankByCLogBankBranchId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogBankBranchs : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogBankBranchs * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogBankBranchs * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogBankBranchs *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetCLogBankBranchById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetCLogBankBranchById * parameters;
}
@property (retain) EmployeeServiceSvc_GetCLogBankBranchById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetCLogBankBranchById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetCLogBankBranchesByCLogBankId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetCLogBankBranchesByCLogBankId * parameters;
}
@property (retain) EmployeeServiceSvc_GetCLogBankBranchesByCLogBankId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetCLogBankBranchesByCLogBankId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogCourseStatus : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogCourseStatus * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogCourseStatus * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogCourseStatus *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetCLogCourseStatusById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetCLogCourseStatusById * parameters;
}
@property (retain) EmployeeServiceSvc_GetCLogCourseStatusById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetCLogCourseStatusById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogEmpWorkingStatus : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogEmpWorkingStatus * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogEmpWorkingStatus * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogEmpWorkingStatus *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetCLogEmpWorkingStatusById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetCLogEmpWorkingStatusById * parameters;
}
@property (retain) EmployeeServiceSvc_GetCLogEmpWorkingStatusById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetCLogEmpWorkingStatusById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogPersonalities : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogPersonalities * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogPersonalities * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogPersonalities *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetCLogCLogPersonalityById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetCLogCLogPersonalityById * parameters;
}
@property (retain) EmployeeServiceSvc_GetCLogCLogPersonalityById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetCLogCLogPersonalityById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllOrgProjectTypes : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllOrgProjectTypes * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllOrgProjectTypes * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllOrgProjectTypes *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetOrgProjectTypeById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetOrgProjectTypeById * parameters;
}
@property (retain) EmployeeServiceSvc_GetOrgProjectTypeById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetOrgProjectTypeById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllOrgTimeInCharges : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllOrgTimeInCharges * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllOrgTimeInCharges * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllOrgTimeInCharges *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetOrgTimeInChargeById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetOrgTimeInChargeById * parameters;
}
@property (retain) EmployeeServiceSvc_GetOrgTimeInChargeById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetOrgTimeInChargeById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllOrgSkills : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllOrgSkills * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllOrgSkills * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllOrgSkills *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetOrgSkillById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetOrgSkillById * parameters;
}
@property (retain) EmployeeServiceSvc_GetOrgSkillById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetOrgSkillById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllOrgSkillTypes : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllOrgSkillTypes * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllOrgSkillTypes * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllOrgSkillTypes *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetOrgSkillTypeById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetOrgSkillTypeById * parameters;
}
@property (retain) EmployeeServiceSvc_GetOrgSkillTypeById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetOrgSkillTypeById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogCBFactors : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogCBFactors * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogCBFactors * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogCBFactors *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetCLogCBFactorById : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetCLogCBFactorById * parameters;
}
@property (retain) EmployeeServiceSvc_GetCLogCBFactorById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetCLogCBFactorById *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllViewCLogCBFactors : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllViewCLogCBFactors * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllViewCLogCBFactors * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllViewCLogCBFactors *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllCLogEmpOffTypes : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllCLogEmpOffTypes * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllCLogEmpOffTypes * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllCLogEmpOffTypes *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllOrgKnowledge : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllOrgKnowledge * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllOrgKnowledge * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllOrgKnowledge *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllOrgKnowledgeByMajorId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllOrgKnowledgeByMajorId * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllOrgKnowledgeByMajorId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllOrgKnowledgeByMajorId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllOrgUnitByUser : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllOrgUnitByUser * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllOrgUnitByUser * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllOrgUnitByUser *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllEmpBasicInfomationByOrgUnitIds : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllEmpBasicInfomationByOrgUnitIds * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllEmpBasicInfomationByOrgUnitIds * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllEmpBasicInfomationByOrgUnitIds *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_GetAllV_EmpBasicInfoByOrgUnitId : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_GetAllV_EmpBasicInfoByOrgUnitId * parameters;
}
@property (retain) EmployeeServiceSvc_GetAllV_EmpBasicInfoByOrgUnitId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_GetAllV_EmpBasicInfoByOrgUnitId *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_FTS_EmpProfileSearchAll : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_FTS_EmpProfileSearchAll * parameters;
}
@property (retain) EmployeeServiceSvc_FTS_EmpProfileSearchAll * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_FTS_EmpProfileSearchAll *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_FTS_EmpProfileBasic : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_FTS_EmpProfileBasic * parameters;
}
@property (retain) EmployeeServiceSvc_FTS_EmpProfileBasic * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_FTS_EmpProfileBasic *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_FTS_EmpProfileContact : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_FTS_EmpProfileContact * parameters;
}
@property (retain) EmployeeServiceSvc_FTS_EmpProfileContact * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_FTS_EmpProfileContact *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_FTS_EmpProfileDegree : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_FTS_EmpProfileDegree * parameters;
}
@property (retain) EmployeeServiceSvc_FTS_EmpProfileDegree * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_FTS_EmpProfileDegree *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_FTS_EmpProfileEduQualification : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_FTS_EmpProfileEduQualification * parameters;
}
@property (retain) EmployeeServiceSvc_FTS_EmpProfileEduQualification * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_FTS_EmpProfileEduQualification *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_FTS_EmpProfileForeignLanguage : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_FTS_EmpProfileForeignLanguage * parameters;
}
@property (retain) EmployeeServiceSvc_FTS_EmpProfileForeignLanguage * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_FTS_EmpProfileForeignLanguage *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_FTS_EmpProfileJobPosition : BasicHttpBinding_IEmployeeServiceBindingOperation {
	EmployeeServiceSvc_FTS_EmpProfileJobPosition * parameters;
}
@property (retain) EmployeeServiceSvc_FTS_EmpProfileJobPosition * parameters;
- (id)initWithBinding:(BasicHttpBinding_IEmployeeServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IEmployeeServiceBindingResponseDelegate>)aDelegate
	parameters:(EmployeeServiceSvc_FTS_EmpProfileJobPosition *)aParameters
;
@end
@interface BasicHttpBinding_IEmployeeServiceBinding_envelope : NSObject {
}
+ (BasicHttpBinding_IEmployeeServiceBinding_envelope *)sharedInstance;
- (NSString *)serializedFormUsingHeaderElements:(NSDictionary *)headerElements bodyElements:(NSDictionary *)bodyElements;
@end
@interface BasicHttpBinding_IEmployeeServiceBindingResponse : NSObject {
	NSArray *headers;
	NSArray *bodyParts;
	NSError *error;
}
@property (retain) NSArray *headers;
@property (retain) NSArray *bodyParts;
@property (retain) NSError *error;
@end
