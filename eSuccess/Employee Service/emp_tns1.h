#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_ORG_UNIT_Result;
@class emp_tns1_SP_FTS_EMP_PROFILE_ORG_UNIT_Result;
@class emp_tns1_ArrayOfSP_FTS_EMP_CONTRACT_Result;
@class emp_tns1_SP_FTS_EMP_CONTRACT_Result;
@class emp_tns1_ArrayOfSP_FTS_EMP_OFF_Result;
@class emp_tns1_SP_FTS_EMP_OFF_Result;
@class emp_tns1_ArrayOfSP_EMP_ADS_EMPLOYEE_Result;
@class emp_tns1_SP_EMP_ADS_EMPLOYEE_Result;
@class emp_tns1_ArrayOfSP_EMP_ADS_CONTRACT_Result;
@class emp_tns1_SP_EMP_ADS_CONTRACT_Result;
@class emp_tns1_ArrayOfSP_EMP_ADS_EMP_OFF_Result;
@class emp_tns1_SP_EMP_ADS_EMP_OFF_Result;
@class emp_tns1_ArrayOfEmployer;
@class emp_tns1_Employer;
@class emp_tns1_ArrayOfEmpContract;
@class emp_tns1_EmpContract;
@class emp_tns1_Address;
@class emp_tns1_Employee;
@class emp_tns1_OrgJob;
@class emp_tns1_OrgJobPosition;
@class emp_tns1_ArrayOfCLogTrainer;
@class emp_tns1_ArrayOfOrgUnit;
@class emp_tns1_CLogTrainer;
@class emp_tns1_ArrayOfTrainingCourse;
@class emp_tns1_ArrayOfCBAccidentInsurance;
@class emp_tns1_ArrayOfCBCompensationEmployeeGroupDetail;
@class emp_tns1_ArrayOfCBConvalescence;
@class emp_tns1_ArrayOfCBDayOffSocialInsurance;
@class emp_tns1_ArrayOfCBFactorEmployeeMetaData;
@class emp_tns1_ArrayOfCBHealthInsuranceDetail;
@class emp_tns1_CLogCity;
@class emp_tns1_CLogEmployeeType;
@class emp_tns1_ArrayOfEmpBasicProfile;
@class emp_tns1_ArrayOfEmpCompetency;
@class emp_tns1_ArrayOfEmpCompetencyRating;
@class emp_tns1_ArrayOfEmpPerformanceAppraisal;
@class emp_tns1_ArrayOfEmpProfileAllowance;
@class emp_tns1_ArrayOfEmpProfileBaseSalary;
@class emp_tns1_ArrayOfEmpProfileBenefit;
@class emp_tns1_ArrayOfEmpProfileComment;
@class emp_tns1_ArrayOfEmpProfileComputingSkill;
@class emp_tns1_ArrayOfEmpProfileContact;
@class emp_tns1_ArrayOfEmpProfileDegree;
@class emp_tns1_ArrayOfEmpProfileDiscipline;
@class emp_tns1_ArrayOfEmpProfileEducation;
@class emp_tns1_ArrayOfEmpProfileEquipment;
@class emp_tns1_ArrayOfEmpProfileExperience;
@class emp_tns1_ArrayOfEmpProfileFamilyRelationship;
@class emp_tns1_ArrayOfEmpProfileForeignLanguage;
@class emp_tns1_ArrayOfEmpProfileHealthInsurance;
@class emp_tns1_ArrayOfEmpProfileHealthy;
@class emp_tns1_ArrayOfEmpProfileHobby;
@class emp_tns1_ArrayOfEmpProfileInternalCourse;
@class emp_tns1_ArrayOfEmpProfileJobPosition;
@class emp_tns1_ArrayOfEmpProfileLeaveRegime;
@class emp_tns1_ArrayOfEmpProfileOtherBenefit;
@class emp_tns1_ArrayOfEmpProfileParticipation;
@class emp_tns1_ArrayOfEmpProfilePersonality;
@class emp_tns1_ArrayOfEmpProfileProcessOfWork;
@class emp_tns1_ArrayOfEmpProfileQualification;
@class emp_tns1_ArrayOfEmpProfileReward;
@class emp_tns1_ArrayOfEmpProfileSkill;
@class emp_tns1_ArrayOfEmpProfileTotalIncome;
@class emp_tns1_ArrayOfEmpProfileTraining;
@class emp_tns1_ArrayOfEmpProfileWageType;
@class emp_tns1_ArrayOfEmpProfileWorkingExperience;
@class emp_tns1_ArrayOfEmpProfileWorkingForm;
@class emp_tns1_ArrayOfEmpSocialInsuranceSalary;
@class emp_tns1_ArrayOfGPAdditionAppraisal;
@class emp_tns1_ArrayOfGPCompanyScoreCard;
@class emp_tns1_ArrayOfGPCompanyStrategicGoal;
@class emp_tns1_ArrayOfGPCompanyYearlyObjective;
@class emp_tns1_ArrayOfGPEmployeeScoreCard;
@class emp_tns1_ArrayOfGPIndividualYearlyObjective;
@class emp_tns1_ArrayOfGPObjectiveInitiative;
@class emp_tns1_ArrayOfGPPerformanceYearEndResult;
@class emp_tns1_ArrayOfLMSCourseAttendee;
@class emp_tns1_ArrayOfLMSCourseTranscript;
@class emp_tns1_ArrayOfNCClientConnection;
@class emp_tns1_ArrayOfNCMessage;
@class emp_tns1_ArrayOfProjectMember;
@class emp_tns1_ArrayOfProject;
@class emp_tns1_ArrayOfRecInterviewSchedule;
@class emp_tns1_ArrayOfRecInterviewer;
@class emp_tns1_ArrayOfRecPhaseEmpDisplaced;
@class emp_tns1_ArrayOfRecRecruitmentRequirement;
@class emp_tns1_ArrayOfRecRequirementEmpDisplaced;
@class emp_tns1_ReportEmployeeRequestedBook;
@class emp_tns1_ArrayOfSelfLeaveRequest;
@class emp_tns1_ArrayOfSysRecPlanApprover;
@class emp_tns1_ArrayOfSysRecRequirementApprover;
@class emp_tns1_ArrayOfSysTrainingApprover;
@class emp_tns1_ArrayOfTask;
@class emp_tns1_ArrayOfTrainingCourseEmployee;
@class emp_tns1_ArrayOfTrainingEmpPlan;
@class emp_tns1_ArrayOfTrainingPlanEmployee;
@class emp_tns1_CBAccidentInsurance;
@class emp_tns1_CBCompensationEmployeeGroupDetail;
@class emp_tns1_CBCompensationEmployeeGroup;
@class emp_tns1_CBFactorEmployeeMetaData;
@class emp_tns1_CLogCBFactor;
@class emp_tns1_ArrayOfCBCompensationTypeFactor;
@class emp_tns1_CLogCBCompensationCategory;
@class emp_tns1_CBCompensationTypeFactor;
@class emp_tns1_ArrayOfCLogCBFactor;
@class emp_tns1_CBConvalescence;
@class emp_tns1_CBDayOffSocialInsurance;
@class emp_tns1_CLogCBDisease;
@class emp_tns1_CLogHospital;
@class emp_tns1_ArrayOfCBHealthInsurance;
@class emp_tns1_CBHealthInsurance;
@class emp_tns1_CLogCountry;
@class emp_tns1_ArrayOfCLogDistrict;
@class emp_tns1_ArrayOfCLogHospital;
@class emp_tns1_ArrayOfEmployee;
@class emp_tns1_ArrayOfCLogCity;
@class emp_tns1_CLogDistrict;
@class emp_tns1_CBHealthInsuranceDetail;
@class emp_tns1_EmpBasicProfile;
@class emp_tns1_CLogEmpWorkingStatu;
@class emp_tns1_EmpCompetency;
@class emp_tns1_CLogRating;
@class emp_tns1_OrgCoreCompetency;
@class emp_tns1_OrgProficencyLevel;
@class emp_tns1_ArrayOfEmpProficencyDetailRating;
@class emp_tns1_ArrayOfEmpProficencyLevelDetailRating;
@class emp_tns1_ArrayOfEmpProficencyLevelRating;
@class emp_tns1_ArrayOfLMSCourseCompetency;
@class emp_tns1_ArrayOfLMSCourseRequiredCompetency;
@class emp_tns1_ArrayOfOrgCoreCompetency;
@class emp_tns1_ArrayOfOrgProficencyDetail;
@class emp_tns1_ArrayOfOrgProficencyLevelDetail;
@class emp_tns1_ArrayOfOrgProficencyLevel;
@class emp_tns1_ArrayOfRecCanProficencyDetailRating;
@class emp_tns1_ArrayOfRecCanProficencyLevelDetailRating;
@class emp_tns1_ArrayOfRecCandidateCompetencyRating;
@class emp_tns1_ArrayOfRecCandidateExperience;
@class emp_tns1_ArrayOfRecCandidateProficencyLevelRating;
@class emp_tns1_ArrayOfRecCandidateQualification;
@class emp_tns1_ArrayOfRecCandidateSkill;
@class emp_tns1_ArrayOfTrainingEmpProficency;
@class emp_tns1_ArrayOfTrainingProficencyExpected;
@class emp_tns1_ArrayOfTrainingProficencyRequire;
@class emp_tns1_EmpProficencyDetailRating;
@class emp_tns1_EmpProficencyLevelDetailRating;
@class emp_tns1_OrgProficencyDetail;
@class emp_tns1_EmpProficencyLevelRating;
@class emp_tns1_OrgProficencyLevelDetail;
@class emp_tns1_EmpCompetencyRating;
@class emp_tns1_OrgCompetency;
@class emp_tns1_ArrayOfOrgJobPositionSpecificCompetency;
@class emp_tns1_ArrayOfLMSTopicCompetency;
@class emp_tns1_OrgCompetencyGroup;
@class emp_tns1_ArrayOfOrgJobSpecificCompetency;
@class emp_tns1_LMSCourseCompetency;
@class emp_tns1_LMSCourse;
@class emp_tns1_CLogCourseStatu;
@class emp_tns1_GPCompanyYearlyObjective;
@class emp_tns1_GPObjectiveInitiative;
@class emp_tns1_LMSContentTopic;
@class emp_tns1_ArrayOfLMSCourseContent;
@class emp_tns1_ArrayOfLMSCourseDetail;
@class emp_tns1_ArrayOfLMSCourseProgressUnit;
@class emp_tns1_LMSCourseType;
@class emp_tns1_ArrayOfLMSCourse;
@class emp_tns1_GPCompanyStrategicGoal;
@class emp_tns1_GPPerspective;
@class emp_tns1_GPStrategy;
@class emp_tns1_GPCompanyScoreCard;
@class emp_tns1_GPPerspectiveMeasure;
@class emp_tns1_GPEmployeeScoreCard;
@class emp_tns1_ArrayOfGPEmployeeScoreCardAppraisal;
@class emp_tns1_ArrayOfGPEmployeeScoreCardProgress;
@class emp_tns1_GPPerformanceExecutionReport;
@class emp_tns1_GPEmployeeScoreCardAppraisal;
@class emp_tns1_GPEmployeeScoreCardProgress;
@class emp_tns1_GPIndividualYearlyObjective;
@class emp_tns1_ArrayOfGPExecutionPlanDetail;
@class emp_tns1_ArrayOfGPIndividualYearlyObjectiveAppraisal;
@class emp_tns1_GPExecutionPlanDetail;
@class emp_tns1_GPExecutionPlan;
@class emp_tns1_ArrayOfGPExecutionPlanActivity;
@class emp_tns1_ArrayOfGPEmployeeWholePlanAppraisal;
@class emp_tns1_GPAdditionAppraisal;
@class emp_tns1_GPEmployeeWholePlanAppraisal;
@class emp_tns1_GPPerformanceYearEndResult;
@class emp_tns1_GPExecutionPlanActivity;
@class emp_tns1_ArrayOfGPExecutionPlanActivityAppraisal;
@class emp_tns1_ArrayOfGPPeriodicReport;
@class emp_tns1_GPExecutionPlanActivityAppraisal;
@class emp_tns1_GPPeriodicReport;
@class emp_tns1_GPIndividualYearlyObjectiveAppraisal;
@class emp_tns1_ArrayOfGPPerspectiveMeasure;
@class emp_tns1_ArrayOfGPPerspectiveValue;
@class emp_tns1_ArrayOfGPCompanyStrategicGoalDetail;
@class emp_tns1_GPStrategyMapObject;
@class emp_tns1_GPCompanyStrategicGoalDetail;
@class emp_tns1_GPPerspectiveValue;
@class emp_tns1_ArrayOfGPPerspectiveValueDetail;
@class emp_tns1_GPPerspectiveValueDetail;
@class emp_tns1_ArrayOfGPStrategyMapObject;
@class emp_tns1_LMSTopicGroup;
@class emp_tns1_LMSTopicCompetency;
@class emp_tns1_ArrayOfLMSContentTopic;
@class emp_tns1_LMSCourseAttendee;
@class emp_tns1_LMSCourseContent;
@class emp_tns1_CLogLMSContentType;
@class emp_tns1_LMSCourseDetail;
@class emp_tns1_LMSCourseProgressUnit;
@class emp_tns1_OrgUnit;
@class emp_tns1_LMSCourseRequiredCompetency;
@class emp_tns1_LMSCourseTranscript;
@class emp_tns1_ArrayOfOrgCompetency;
@class emp_tns1_OrgJobSpecificCompetency;
@class emp_tns1_ArrayOfOrgJobPosition;
@class emp_tns1_ArrayOfOrgUnitJob;
@class emp_tns1_ArrayOfOrgJobPositionRequiredProficency;
@class emp_tns1_ArrayOfRecPlanJobPosition;
@class emp_tns1_ArrayOfRecProcessApplyForJobPosition;
@class emp_tns1_ArrayOfRecRecruitmentPhaseJobPosition;
@class emp_tns1_ArrayOfRecRequirementJobPosition;
@class emp_tns1_EmpProfileJobPosition;
@class emp_tns1_OrgJobPositionRequiredProficency;
@class emp_tns1_OrgJobPositionSpecificCompetency;
@class emp_tns1_RecPhaseEmpDisplaced;
@class emp_tns1_RecRecruitmentPhase;
@class emp_tns1_RecPhaseStatu;
@class emp_tns1_RecRecruitmentRequirement;
@class emp_tns1_ArrayOfRecRecruitmentPhase;
@class emp_tns1_RecRecruitmentPhaseJobPosition;
@class emp_tns1_ArrayOfRecCandidateApplication;
@class emp_tns1_RecCandidateApplication;
@class emp_tns1_RecCandidate;
@class emp_tns1_RecCandidateProfileStatu;
@class emp_tns1_ArrayOfRecProcessPhaseResult;
@class emp_tns1_ArrayOfRecCandidateDegree;
@class emp_tns1_ArrayOfRecCandidateFamilyRelationship;
@class emp_tns1_ArrayOfRecCandidateForeignLanguage;
@class emp_tns1_RecCandidateStatu;
@class emp_tns1_RecCandidateSupplier;
@class emp_tns1_RecCandidateCompetencyRating;
@class emp_tns1_RecCandidateProficencyLevelRating;
@class emp_tns1_RecCanProficencyLevelDetailRating;
@class emp_tns1_OrgProficencyType;
@class emp_tns1_RecCanProficencyDetailRating;
@class emp_tns1_ArrayOfTrainingPlanProfiency;
@class emp_tns1_TrainingPlanProfiency;
@class emp_tns1_RecCandidateDegree;
@class emp_tns1_CLogDegree;
@class emp_tns1_RecCandidateExperience;
@class emp_tns1_OrgExperience;
@class emp_tns1_OrgProjectType;
@class emp_tns1_OrgTimeInCharge;
@class emp_tns1_ArrayOfOrgExperience;
@class emp_tns1_RecCandidateFamilyRelationship;
@class emp_tns1_CLogFamilyRelationship;
@class emp_tns1_EmpProfileFamilyRelationship;
@class emp_tns1_ArrayOfCBPITDependent;
@class emp_tns1_ArrayOfEmpBeneficiary;
@class emp_tns1_CBPITDependent;
@class emp_tns1_EmpBeneficiary;
@class emp_tns1_RecCandidateForeignLanguage;
@class emp_tns1_RecCandidateQualification;
@class emp_tns1_OrgQualification;
@class emp_tns1_EmpProfileQualification;
@class emp_tns1_RecCandidateSkill;
@class emp_tns1_OrgSkill;
@class emp_tns1_OrgSkillType;
@class emp_tns1_EmpProfileSkill;
@class emp_tns1_ArrayOfOrgSkill;
@class emp_tns1_TrainingEmpProficency;
@class emp_tns1_TrainingCourseEmployee;
@class emp_tns1_TrainingCourseSchedule;
@class emp_tns1_CLogCourseSchedule;
@class emp_tns1_TrainingCourse;
@class emp_tns1_ArrayOfTrainingCourseSchedule;
@class emp_tns1_CLogTrainingCenter;
@class emp_tns1_TrainingCategory;
@class emp_tns1_ArrayOfTrainingCourseChapter;
@class emp_tns1_TrainingCoursePeriod;
@class emp_tns1_TrainingCourseType;
@class emp_tns1_ArrayOfTrainingCourseUnit;
@class emp_tns1_ArrayOfTrainingPlanDegree;
@class emp_tns1_TrainingPlanRequest;
@class emp_tns1_TrainingCourseChapter;
@class emp_tns1_TrainingCourseUnit;
@class emp_tns1_TrainingPlanDegree;
@class emp_tns1_CLogMajor;
@class emp_tns1_ArrayOfTrainingPlanRequest;
@class emp_tns1_EmpProfileEducation;
@class emp_tns1_TrainingPlanEmployee;
@class emp_tns1_TrainingProficencyExpected;
@class emp_tns1_TrainingProficencyRequire;
@class emp_tns1_ArrayOfRecCandidate;
@class emp_tns1_RecCandidateTypeSupplier;
@class emp_tns1_ArrayOfRecCandidateSupplier;
@class emp_tns1_RecProcessPhaseResult;
@class emp_tns1_RecRecruitmentProcessPhase;
@class emp_tns1_ArrayOfRecInterviewPhase;
@class emp_tns1_RecRecruitmentProcess;
@class emp_tns1_RecInterviewPhase;
@class emp_tns1_ArrayOfRecInterviewPhaseEvaluation;
@class emp_tns1_RecInterviewPhaseEvaluation;
@class emp_tns1_RecEvaluationCriterion;
@class emp_tns1_ArrayOfRecScheduleInterviewerResult;
@class emp_tns1_RecGroupEvaluationCriterion;
@class emp_tns1_ArrayOfRecEvaluationCriterion;
@class emp_tns1_RecScheduleInterviewerResult;
@class emp_tns1_RecInterviewSchedule;
@class emp_tns1_RecInterviewer;
@class emp_tns1_RecGroupInterviewer;
@class emp_tns1_RecInterviewScheduleStatu;
@class emp_tns1_ArrayOfRecRecruitmentProcessPhase;
@class emp_tns1_RecProcessApplyForJobPosition;
@class emp_tns1_RecRecruitmentPlan;
@class emp_tns1_ArrayOfRecRequirementApproveHistory;
@class emp_tns1_RecRequirementStatu;
@class emp_tns1_ArrayOfRecPlanApproveHistory;
@class emp_tns1_RecPlanStatu;
@class emp_tns1_RecPlanApproveHistory;
@class emp_tns1_SysRecPlanApprover;
@class emp_tns1_RecPlanJobPosition;
@class emp_tns1_ArrayOfRecRecruitmentPlan;
@class emp_tns1_RecRequirementApproveHistory;
@class emp_tns1_SysRecRequirementApprover;
@class emp_tns1_RecRequirementEmpDisplaced;
@class emp_tns1_RecRequirementJobPosition;
@class emp_tns1_OrgUnitJob;
@class emp_tns1_EmpProfileWorkingExperience;
@class emp_tns1_EmpPerformanceAppraisal;
@class emp_tns1_EmpProfileAllowance;
@class emp_tns1_EmpProfileBaseSalary;
@class emp_tns1_EmpProfileBenefit;
@class emp_tns1_EmpProfileComment;
@class emp_tns1_EmpProfileComputingSkill;
@class emp_tns1_EmpProfileContact;
@class emp_tns1_EmpProfileDegree;
@class emp_tns1_OrgDegree;
@class emp_tns1_EmpProfileDiscipline;
@class emp_tns1_OrgDisciplineMaster;
@class emp_tns1_ArrayOfOrgDisciplineForUnit;
@class emp_tns1_OrgDisciplineForUnit;
@class emp_tns1_EmpProfileEquipment;
@class emp_tns1_CLogEquipment;
@class emp_tns1_EmpProfileExperience;
@class emp_tns1_EmpProfileForeignLanguage;
@class emp_tns1_CLogLanguage;
@class emp_tns1_EmpProfileHealthInsurance;
@class emp_tns1_EmpProfileHealthy;
@class emp_tns1_EmpProfileHobby;
@class emp_tns1_CLogHobby;
@class emp_tns1_EmpProfileInternalCourse;
@class emp_tns1_EmpProfileLeaveRegime;
@class emp_tns1_EmpProfileOtherBenefit;
@class emp_tns1_EmpOtherBenefit;
@class emp_tns1_EmpProfileParticipation;
@class emp_tns1_EmpProfilePersonality;
@class emp_tns1_CLogPersonality;
@class emp_tns1_EmpProfileProcessOfWork;
@class emp_tns1_EmpProfileReward;
@class emp_tns1_OrgRewardMaster;
@class emp_tns1_ArrayOfOrgRewardForUnit;
@class emp_tns1_OrgRewardForUnit;
@class emp_tns1_EmpProfileTotalIncome;
@class emp_tns1_EmpProfileTraining;
@class emp_tns1_EmpProfileWageType;
@class emp_tns1_EmpProfileWorkingForm;
@class emp_tns1_EmpSocialInsuranceSalary;
@class emp_tns1_NCClientConnection;
@class emp_tns1_NCMessage;
@class emp_tns1_NCMessageType;
@class emp_tns1_ProjectMember;
@class emp_tns1_Project;
@class emp_tns1_SelfLeaveRequest;
@class emp_tns1_SysTrainingApprover;
@class emp_tns1_ArrayOfTrainingEmpPlanApproved;
@class emp_tns1_TrainingEmpPlanApproved;
@class emp_tns1_TrainingEmpPlan;
@class emp_tns1_Task;
@class emp_tns1_ArrayOfCompany;
@class emp_tns1_Company;
@class emp_tns1_ArrayOfV_EmpContract;
@class emp_tns1_V_EmpContract;
@class emp_tns1_V_EMP_CONTRACT_WOKING_HOURS;
@class emp_tns1_ArrayOfV_ESS_CLOG_CONTRACT_TYPE;
@class emp_tns1_V_ESS_CLOG_CONTRACT_TYPE;
@class emp_tns1_ArrayOfV_ESS_CLOG_CONTRACT_TERM;
@class emp_tns1_V_ESS_CLOG_CONTRACT_TERM;
@class emp_tns1_ArrayOfTB_ESS_SYSTEM_PARAMETER;
@class emp_tns1_TB_ESS_SYSTEM_PARAMETER;
@class emp_tns1_ArrayOfV_Employee;
@class emp_tns1_V_Employee;
@class emp_tns1_ArrayOfV_EmpProfileJobPosition;
@class emp_tns1_V_EmpProfileJobPosition;
@class emp_tns1_ArrayOfV_EmpBasicProfile;
@class emp_tns1_V_EmpBasicProfile;
@class emp_tns1_ArrayOfV_EmpBasicInformation;
@class emp_tns1_V_EmpBasicInformation;
@class emp_tns1_ArrayOfV_EmpProfileDegree;
@class emp_tns1_V_EmpProfileDegree;
@class emp_tns1_ArrayOfV_TimeRequestMasTer;
@class emp_tns1_V_TimeRequestMasTer;
@class emp_tns1_ArrayOfTimeRequestMasTer;
@class emp_tns1_TimeRequestMasTer;
@class emp_tns1_EmpOffProfile;
@class emp_tns1_ArrayOfV_EmpOffProfile;
@class emp_tns1_V_EmpOffProfile;
@class emp_tns1_ArrayOfCLogEquipment;
@class emp_tns1_ArrayOfV_ESS_CLOG_EQUIPMENT_TYPE;
@class emp_tns1_V_ESS_CLOG_EQUIPMENT_TYPE;
@class emp_tns1_ArrayOfV_ESS_CLOG_ASSETS_TYPE;
@class emp_tns1_V_ESS_CLOG_ASSETS_TYPE;
@class emp_tns1_ArrayOfV_EmpProfileEquipment;
@class emp_tns1_V_EmpProfileEquipment;
@class emp_tns1_ArrayOfV_EmpProfileAssets;
@class emp_tns1_V_EmpProfileAssets;
@class emp_tns1_ArrayOfV_ESS_CLOG_EQUIPMENT;
@class emp_tns1_V_ESS_CLOG_EQUIPMENT;
@class emp_tns1_ArrayOfV_ESS_CLOG_ASSETS;
@class emp_tns1_V_ESS_CLOG_ASSETS;
@class emp_tns1_ArrayOfV_OrgUnitAssets;
@class emp_tns1_V_OrgUnitAssets;
@class emp_tns1_ArrayOfV_OrgUnitEquipment;
@class emp_tns1_V_OrgUnitEquipment;
@class emp_tns1_ArrayOfCLogCountry;
@class emp_tns1_ArrayOfV_ESS_ORG_DIRECT_REPORT;
@class emp_tns1_V_ESS_ORG_DIRECT_REPORT;
@class emp_tns1_ArrayOfSP_EMP_GET_VIEW_EMPLOYEE_Result;
@class emp_tns1_SP_EMP_GET_VIEW_EMPLOYEE_Result;
@class emp_tns1_V_EmployeeProfile;
@class emp_tns1_ArrayOfSP_GetAllParentEmployee_Result;
@class emp_tns1_SP_GetAllParentEmployee_Result;
@class emp_tns1_ArrayOfCLogFamilyRelationship;
@class emp_tns1_ArrayOfV_EmpProfileWorkingExperience;
@class emp_tns1_V_EmpProfileWorkingExperience;
@class emp_tns1_ArrayOfV_EmpProfileSkill;
@class emp_tns1_V_EmpProfileSkill;
@class emp_tns1_ArrayOfV_EmpProfileBasicSalary;
@class emp_tns1_V_EmpProfileBasicSalary;
@class emp_tns1_ArrayOfV_EmpProfileHealthInsurance;
@class emp_tns1_V_EmpProfileHealthInsurance;
@class emp_tns1_ArrayOfV_EmpProfileWageType;
@class emp_tns1_V_EmpProfileWageType;
@class emp_tns1_ArrayOfV_EmpProfileWorkingForm;
@class emp_tns1_V_EmpProfileWorkingForm;
@class emp_tns1_ArrayOfV_EmpSocialInsuranceSalary;
@class emp_tns1_V_EmpSocialInsuranceSalary;
@class emp_tns1_ArrayOfEmpProfileBiography;
@class emp_tns1_EmpProfileBiography;
@class emp_tns1_ArrayOfEmpProfileSocialInsurance;
@class emp_tns1_EmpProfileSocialInsurance;
@class emp_tns1_ArrayOfV_EmpPerformanceAppraisal;
@class emp_tns1_V_EmpPerformanceAppraisal;
@class emp_tns1_ArrayOfV_EmpProfileQualification;
@class emp_tns1_V_EmpProfileQualification;
@class emp_tns1_ArrayOfV_EmpProfileForeignLanguage;
@class emp_tns1_V_EmpProfileForeignLanguage;
@class emp_tns1_ArrayOfV_EmpProfileDiscipline;
@class emp_tns1_V_EmpProfileDiscipline;
@class emp_tns1_ArrayOfV_EmpProfilePersonality;
@class emp_tns1_V_EmpProfilePersonality;
@class emp_tns1_ArrayOfTimeWorkingForm;
@class emp_tns1_TimeWorkingForm;
@class emp_tns1_ArrayOfTimeWageType;
@class emp_tns1_TimeWageType;
@class emp_tns1_ArrayOfCLogCBCompensationCategory;
@class emp_tns1_ArrayOfCLogCurrency;
@class emp_tns1_CLogCurrency;
@class emp_tns1_ArrayOfCLogCurrencyRate;
@class emp_tns1_CLogCurrencyRate;
@class emp_tns1_ArrayOfSysEmpProfileGroupLayer;
@class emp_tns1_SysEmpProfileGroupLayer;
@class emp_tns1_ArrayOfSysEmpProfileLayer;
@class emp_tns1_SysEmpProfileLayer;
@class emp_tns1_ArrayOfSysEmpProfileLayerUrl;
@class emp_tns1_ArrayOfSysGroupEmpProfilePermission;
@class emp_tns1_ArrayOfSysRoleEmpProfilePermission;
@class emp_tns1_ArrayOfSysUserEmpProfilePermission;
@class emp_tns1_SysEmpProfileLayerUrl;
@class emp_tns1_SysGroupEmpProfilePermission;
@class emp_tns1_SysGroup;
@class emp_tns1_ArrayOfSysGroupMenuPermission;
@class emp_tns1_SysGroupMenuPermission;
@class emp_tns1_SysMenu;
@class emp_tns1_ArrayOfSysRoleMenuPermission;
@class emp_tns1_ArrayOfSysUserMenuPermission;
@class emp_tns1_SysRoleMenuPermission;
@class emp_tns1_SysRole;
@class emp_tns1_SysRoleEmpProfilePermission;
@class emp_tns1_SysUserMenuPermission;
@class emp_tns1_SysUser;
@class emp_tns1_SysUserEmpProfilePermission;
@class emp_tns1_V_SysEmpProfileLayer;
@class emp_tns1_ArrayOfCLogEmployeeType;
@class emp_tns1_ArrayOfCLogMaritalStatu;
@class emp_tns1_CLogMaritalStatu;
@class emp_tns1_ArrayOfV_OrgUnitTree;
@class emp_tns1_V_OrgUnitTree;
@class emp_tns1_ArrayOfV_OrgUnitJob;
@class emp_tns1_V_OrgUnitJob;
@class emp_tns1_ArrayOfOrgWorkLevel;
@class emp_tns1_OrgWorkLevel;
@class emp_tns1_ArrayOfV_ESS_CLOG_CAREER;
@class emp_tns1_V_ESS_CLOG_CAREER;
@class emp_tns1_ArrayOfCLogRating;
@class emp_tns1_ArrayOfCLogDegree;
@class emp_tns1_ArrayOfCLogMajor;
@class emp_tns1_ArrayOfCLogTrainingCenter;
@class emp_tns1_ArrayOfCLogTraining;
@class emp_tns1_CLogTraining;
@class emp_tns1_ArrayOfOrgDegree;
@class emp_tns1_ArrayOfOrgDegreeRank;
@class emp_tns1_OrgDegreeRank;
@class emp_tns1_ArrayOfCLogEthnicity;
@class emp_tns1_CLogEthnicity;
@class emp_tns1_ArrayOfCLogReligion;
@class emp_tns1_CLogReligion;
@class emp_tns1_ArrayOfCLogLanguage;
@class emp_tns1_ArrayOfCLogEducationLevel;
@class emp_tns1_CLogEducationLevel;
@class emp_tns1_ArrayOfOrgQualification;
@class emp_tns1_ArrayOfCLogEmpCulturalLevel;
@class emp_tns1_CLogEmpCulturalLevel;
@class emp_tns1_ArrayOfCLogBank;
@class emp_tns1_CLogBank;
@class emp_tns1_ArrayOfCLogBankBranch;
@class emp_tns1_CLogBankBranch;
@class emp_tns1_ArrayOfCLogCourseStatu;
@class emp_tns1_ArrayOfCLogEmpWorkingStatu;
@class emp_tns1_ArrayOfCLogPersonality;
@class emp_tns1_ArrayOfOrgProjectType;
@class emp_tns1_ArrayOfOrgTimeInCharge;
@class emp_tns1_ArrayOfOrgSkillType;
@class emp_tns1_ArrayOfV_CLogCBFactor;
@class emp_tns1_V_CLogCBFactor;
@class emp_tns1_ArrayOfCLogEmpOffType;
@class emp_tns1_CLogEmpOffType;
@class emp_tns1_ArrayOfOrgKnowledge;
@class emp_tns1_OrgKnowledge;
@class emp_tns1_ArrayOfSP_ORG_UNIT_TREE_BY_USER_Result;
@class emp_tns1_SP_ORG_UNIT_TREE_BY_USER_Result;
@class emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_BASIC_Result;
@class emp_tns1_SP_FTS_EMP_PROFILE_BASIC_Result;
@class emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_CONTACT_Result;
@class emp_tns1_SP_FTS_EMP_PROFILE_CONTACT_Result;
@class emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_DEGREE_Result;
@class emp_tns1_SP_FTS_EMP_PROFILE_DEGREE_Result;
@class emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_EDU_QUALIFICATION_Result;
@class emp_tns1_SP_FTS_EMP_PROFILE_EDU_QUALIFICATION_Result;
@class emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_FOREIGN_LANGUAGE_Result;
@class emp_tns1_SP_FTS_EMP_PROFILE_FOREIGN_LANGUAGE_Result;
@class emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_JOBPOSITION_Result;
@class emp_tns1_SP_FTS_EMP_PROFILE_JOBPOSITION_Result;
#import "emp_tns5.h"
@interface emp_tns1_SP_FTS_EMP_PROFILE_ORG_UNIT_Result : NSObject {
	
/* elements */
	NSString * Address;
	NSNumber * AddressId;
	NSString * BankAccountNo;
	NSDate * BirthDay;
	NSString * BirthPlace;
	NSNumber * BirthPlaceId;
	NSString * CVUrl;
	NSNumber * CompanyId;
	NSString * ContactAddress;
	NSNumber * ContactAddressId;
	NSString * CulturalLevel;
	NSNumber * CulturalLevelId;
	NSNumber * EducationLevelId;
	NSString * EducationLevelName;
	NSString * Email;
	NSNumber * EmpBasicProfileId;
	NSString * EmpWorkingStatusCode;
	NSString * EmpWorkingStatusName;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EmployeeTypeId;
	NSString * EmployeeTypeName;
	NSDate * EntryDate;
	NSString * Ethnicity;
	NSNumber * EthnicityId;
	NSString * FirstName;
	NSString * FullName;
	USBoolean * Gender;
	NSString * HomePhone;
	NSString * HomeTown;
	NSNumber * HomeTownId;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	NSString * ImageUrl;
	USBoolean * IsConcurrentPost;
	USBoolean * IsDeleted;
	USBoolean * IsOnlyYearOfBirthday;
	NSString * LastName;
	NSString * MainLanguage;
	NSNumber * MainLanguageId;
	NSString * Major;
	NSNumber * MajorId;
	NSNumber * MaritalStatusId;
	NSString * MaritalStatusValue;
	NSString * MobileNumber;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSString * OfficePhone;
	NSDate * OfficialDate;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgWorkLevelCode;
	NSNumber * OrgWorkLevelId;
	NSString * PITCode;
	NSString * PITIssuedBy;
	NSDate * PassportDateOfExpire;
	NSDate * PassportDateOfIssue;
	NSString * PassportNo;
	NSString * PassportPlaceOfIssue;
	NSString * PermanentAddress;
	NSNumber * PermanentAddressId;
	NSString * PositionName;
	NSDate * ProbationaryDate;
	NSNumber * ProbationarySalary;
	NSString * Reason;
	NSString * Religion;
	NSNumber * ReligionId;
	NSString * TemporaryAddress;
	NSNumber * TemporaryAddressId;
	NSString * TimeWorkingFormCode;
	NSNumber * TimeWorkingFormId;
	NSString * UnitName;
	NSString * WorkLevelDescription;
	NSString * WorkLevelName;
	NSString * WorkLocation;
	NSDate * WorkingFormImplementationDate;
	NSString * WorkingFormTypeCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SP_FTS_EMP_PROFILE_ORG_UNIT_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSNumber * AddressId;
@property (retain) NSString * BankAccountNo;
@property (retain) NSDate * BirthDay;
@property (retain) NSString * BirthPlace;
@property (retain) NSNumber * BirthPlaceId;
@property (retain) NSString * CVUrl;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ContactAddress;
@property (retain) NSNumber * ContactAddressId;
@property (retain) NSString * CulturalLevel;
@property (retain) NSNumber * CulturalLevelId;
@property (retain) NSNumber * EducationLevelId;
@property (retain) NSString * EducationLevelName;
@property (retain) NSString * Email;
@property (retain) NSNumber * EmpBasicProfileId;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSString * EmployeeTypeName;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * Ethnicity;
@property (retain) NSNumber * EthnicityId;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * HomePhone;
@property (retain) NSString * HomeTown;
@property (retain) NSNumber * HomeTownId;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsConcurrentPost;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) NSString * LastName;
@property (retain) NSString * MainLanguage;
@property (retain) NSNumber * MainLanguageId;
@property (retain) NSString * Major;
@property (retain) NSNumber * MajorId;
@property (retain) NSNumber * MaritalStatusId;
@property (retain) NSString * MaritalStatusValue;
@property (retain) NSString * MobileNumber;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSString * OfficePhone;
@property (retain) NSDate * OfficialDate;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * PITCode;
@property (retain) NSString * PITIssuedBy;
@property (retain) NSDate * PassportDateOfExpire;
@property (retain) NSDate * PassportDateOfIssue;
@property (retain) NSString * PassportNo;
@property (retain) NSString * PassportPlaceOfIssue;
@property (retain) NSString * PermanentAddress;
@property (retain) NSNumber * PermanentAddressId;
@property (retain) NSString * PositionName;
@property (retain) NSDate * ProbationaryDate;
@property (retain) NSNumber * ProbationarySalary;
@property (retain) NSString * Reason;
@property (retain) NSString * Religion;
@property (retain) NSNumber * ReligionId;
@property (retain) NSString * TemporaryAddress;
@property (retain) NSNumber * TemporaryAddressId;
@property (retain) NSString * TimeWorkingFormCode;
@property (retain) NSNumber * TimeWorkingFormId;
@property (retain) NSString * UnitName;
@property (retain) NSString * WorkLevelDescription;
@property (retain) NSString * WorkLevelName;
@property (retain) NSString * WorkLocation;
@property (retain) NSDate * WorkingFormImplementationDate;
@property (retain) NSString * WorkingFormTypeCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_ORG_UNIT_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_FTS_EMP_PROFILE_ORG_UNIT_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_ORG_UNIT_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_FTS_EMP_PROFILE_ORG_UNIT_Result:(emp_tns1_SP_FTS_EMP_PROFILE_ORG_UNIT_Result *)toAdd;
@property (readonly) NSMutableArray * SP_FTS_EMP_PROFILE_ORG_UNIT_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SP_FTS_EMP_CONTRACT_Result : NSObject {
	
/* elements */
	NSString * AllocationEquipmentDescription;
	NSString * Allowances;
	NSString * AttachFile;
	NSNumber * BaseWage;
	NSString * Bonuses;
	NSString * CLogContractTermNameEN;
	NSString * CLogContractTermNameVN;
	NSString * CLogContractTypeNameEN;
	NSString * CLogContractTypeNameVN;
	NSString * ClogContractTermCode;
	NSNumber * CompanyId;
	NSString * CompensationDescription;
	NSString * ContractCode;
	NSString * ContractTypeCode;
	NSDate * CreatedDate;
	NSDate * DateSigned;
	NSNumber * DaysPeriod;
	NSNumber * EmpContractId;
	NSString * EmployeeRepresentativeAddress;
	NSDate * EmployeeRepresentativeBirthDay;
	NSString * EmployeeRepresentativeBirthPlace;
	NSString * EmployeeRepresentativeCode;
	NSString * EmployeeRepresentativeFullName;
	NSNumber * EmployeeRepresentativeId;
	NSString * EmployeeRepresentativeMajor;
	NSString * EmployeeRepresentativeNationality;
	NSString * EmployerNationality;
	NSString * EmployerPositionName;
	NSString * EmployerRepresentativeCode;
	NSString * EmployerRepresentativeFullName;
	NSNumber * EmployerRepresentativeId;
	NSString * EmployerRepresentativePositionName;
	NSDate * EndDate;
	NSDate * ExpirationDate;
	NSString * HolIdayPolicy;
	NSNumber * HoursOfOfficialWork;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	USBoolean * IsDeleted;
	USBoolean * IsEnd;
	USBoolean * IsProbation;
	USBoolean * IsSigned;
	NSString * JobName;
	NSString * JobPositionName;
	NSString * JobResponsibilitiesDescription;
	NSString * LaborSupportEquipments;
	NSString * MedicalInsurance;
	NSDate * ModifiedDate;
	NSNumber * MonthsPeriod;
	NSDate * OfficialWorkEndTime;
	NSDate * OfficialWorkStartTime;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrganizationAddress;
	NSString * OrganizationName;
	NSString * OrganizationPhone;
	NSString * OtherPolicy;
	NSString * PayDay;
	NSString * PayingMethod;
	NSString * PlaceSigned;
	NSDate * ProbationaryEndDate;
	NSDate * ProbationaryStartDate;
	NSNumber * ProbationaryTime;
	NSString * ProvidedEquipment;
	NSNumber * RelationEmpContractId;
	NSString * SalaryIncreasePolicy;
	NSString * SocialInsurance;
	NSDate * StartDate;
	NSString * TrainingPolicy;
	NSString * Transportation;
	NSString * UnitName;
	NSString * WorkingAddress;
	NSNumber * WorkingDay;
	NSString * WorkingTime;
	NSString * WorkingTimePolicy;
	NSString * cAllowance;
	NSString * cBaseSalary;
	NSString * cSocialInsuranceSalary;
	NSNumber * nAllowance;
	NSNumber * nSocialInsuranceSalary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SP_FTS_EMP_CONTRACT_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AllocationEquipmentDescription;
@property (retain) NSString * Allowances;
@property (retain) NSString * AttachFile;
@property (retain) NSNumber * BaseWage;
@property (retain) NSString * Bonuses;
@property (retain) NSString * CLogContractTermNameEN;
@property (retain) NSString * CLogContractTermNameVN;
@property (retain) NSString * CLogContractTypeNameEN;
@property (retain) NSString * CLogContractTypeNameVN;
@property (retain) NSString * ClogContractTermCode;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompensationDescription;
@property (retain) NSString * ContractCode;
@property (retain) NSString * ContractTypeCode;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateSigned;
@property (retain) NSNumber * DaysPeriod;
@property (retain) NSNumber * EmpContractId;
@property (retain) NSString * EmployeeRepresentativeAddress;
@property (retain) NSDate * EmployeeRepresentativeBirthDay;
@property (retain) NSString * EmployeeRepresentativeBirthPlace;
@property (retain) NSString * EmployeeRepresentativeCode;
@property (retain) NSString * EmployeeRepresentativeFullName;
@property (retain) NSNumber * EmployeeRepresentativeId;
@property (retain) NSString * EmployeeRepresentativeMajor;
@property (retain) NSString * EmployeeRepresentativeNationality;
@property (retain) NSString * EmployerNationality;
@property (retain) NSString * EmployerPositionName;
@property (retain) NSString * EmployerRepresentativeCode;
@property (retain) NSString * EmployerRepresentativeFullName;
@property (retain) NSNumber * EmployerRepresentativeId;
@property (retain) NSString * EmployerRepresentativePositionName;
@property (retain) NSDate * EndDate;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSString * HolIdayPolicy;
@property (retain) NSNumber * HoursOfOfficialWork;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEnd;
@property (retain) USBoolean * IsProbation;
@property (retain) USBoolean * IsSigned;
@property (retain) NSString * JobName;
@property (retain) NSString * JobPositionName;
@property (retain) NSString * JobResponsibilitiesDescription;
@property (retain) NSString * LaborSupportEquipments;
@property (retain) NSString * MedicalInsurance;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * MonthsPeriod;
@property (retain) NSDate * OfficialWorkEndTime;
@property (retain) NSDate * OfficialWorkStartTime;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrganizationAddress;
@property (retain) NSString * OrganizationName;
@property (retain) NSString * OrganizationPhone;
@property (retain) NSString * OtherPolicy;
@property (retain) NSString * PayDay;
@property (retain) NSString * PayingMethod;
@property (retain) NSString * PlaceSigned;
@property (retain) NSDate * ProbationaryEndDate;
@property (retain) NSDate * ProbationaryStartDate;
@property (retain) NSNumber * ProbationaryTime;
@property (retain) NSString * ProvidedEquipment;
@property (retain) NSNumber * RelationEmpContractId;
@property (retain) NSString * SalaryIncreasePolicy;
@property (retain) NSString * SocialInsurance;
@property (retain) NSDate * StartDate;
@property (retain) NSString * TrainingPolicy;
@property (retain) NSString * Transportation;
@property (retain) NSString * UnitName;
@property (retain) NSString * WorkingAddress;
@property (retain) NSNumber * WorkingDay;
@property (retain) NSString * WorkingTime;
@property (retain) NSString * WorkingTimePolicy;
@property (retain) NSString * cAllowance;
@property (retain) NSString * cBaseSalary;
@property (retain) NSString * cSocialInsuranceSalary;
@property (retain) NSNumber * nAllowance;
@property (retain) NSNumber * nSocialInsuranceSalary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSP_FTS_EMP_CONTRACT_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_FTS_EMP_CONTRACT_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSP_FTS_EMP_CONTRACT_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_FTS_EMP_CONTRACT_Result:(emp_tns1_SP_FTS_EMP_CONTRACT_Result *)toAdd;
@property (readonly) NSMutableArray * SP_FTS_EMP_CONTRACT_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SP_FTS_EMP_OFF_Result : NSObject {
	
/* elements */
	NSDate * ApproveDate;
	NSString * ApproveNote;
	NSString * ApproverCode;
	NSString * ApproverFullName;
	NSNumber * ApproverId;
	NSString * CLogEmpOffTypeCode;
	NSNumber * CLogEmpOffTypeId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * DecisionCode;
	NSString * DecisionFile;
	NSString * DecisionName;
	NSNumber * EmpOffProfileId;
	NSString * EmpOffTypeName;
	NSNumber * EmpProfileJobPositionId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EmployeeTypeId;
	NSString * EmployeeTypeName;
	NSDate * EndWorkingDate;
	NSDate * EntryDate;
	NSString * FirstName;
	NSString * FullName;
	NSDate * HICardExpireDate;
	NSDate * HandoverDate;
	NSString * ImageUrl;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	USBoolean * IsMainPost;
	USBoolean * IsReturnEquipment;
	USBoolean * IsReturnHICard;
	USBoolean * IsViolatedRequest;
	NSString * LastName;
	NSDate * LastWorkingDate;
	NSNumber * LeaveDaysNotUsed;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * OffCoefficientAllowance;
	NSDate * OffFromDate;
	NSDate * OffRequestDate;
	NSDate * OfficialDate;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgJobPositionName;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgUnitName;
	NSString * OrgWorkLevelCode;
	NSNumber * OrgWorkLevelId;
	NSString * OrgWorkLevelName;
	NSDate * ProbationaryDate;
	NSString * Reason;
	NSDate * ReturnHICardDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SP_FTS_EMP_OFF_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ApproveDate;
@property (retain) NSString * ApproveNote;
@property (retain) NSString * ApproverCode;
@property (retain) NSString * ApproverFullName;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * CLogEmpOffTypeCode;
@property (retain) NSNumber * CLogEmpOffTypeId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * DecisionCode;
@property (retain) NSString * DecisionFile;
@property (retain) NSString * DecisionName;
@property (retain) NSNumber * EmpOffProfileId;
@property (retain) NSString * EmpOffTypeName;
@property (retain) NSNumber * EmpProfileJobPositionId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSString * EmployeeTypeName;
@property (retain) NSDate * EndWorkingDate;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) NSDate * HICardExpireDate;
@property (retain) NSDate * HandoverDate;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainPost;
@property (retain) USBoolean * IsReturnEquipment;
@property (retain) USBoolean * IsReturnHICard;
@property (retain) USBoolean * IsViolatedRequest;
@property (retain) NSString * LastName;
@property (retain) NSDate * LastWorkingDate;
@property (retain) NSNumber * LeaveDaysNotUsed;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * OffCoefficientAllowance;
@property (retain) NSDate * OffFromDate;
@property (retain) NSDate * OffRequestDate;
@property (retain) NSDate * OfficialDate;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitName;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * OrgWorkLevelName;
@property (retain) NSDate * ProbationaryDate;
@property (retain) NSString * Reason;
@property (retain) NSDate * ReturnHICardDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSP_FTS_EMP_OFF_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_FTS_EMP_OFF_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSP_FTS_EMP_OFF_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_FTS_EMP_OFF_Result:(emp_tns1_SP_FTS_EMP_OFF_Result *)toAdd;
@property (readonly) NSMutableArray * SP_FTS_EMP_OFF_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SP_EMP_ADS_EMPLOYEE_Result : NSObject {
	
/* elements */
	NSString * Address;
	NSNumber * AddressId;
	NSString * BankAccountNo;
	NSString * BankBranch;
	NSString * BankName;
	NSDate * BirthDay;
	NSString * BirthPlace;
	NSNumber * BirthPlaceId;
	NSString * CVUrl;
	NSString * CodeAndFullName;
	NSNumber * CompanyId;
	NSString * ContactAddress;
	NSNumber * ContactAddressId;
	NSString * CulturalLevel;
	NSNumber * CulturalLevelId;
	NSNumber * EducationLevelId;
	NSString * EducationLevelName;
	NSString * Email;
	NSNumber * EmpBasicProfileId;
	NSString * EmpWorkingStatusCode;
	NSString * EmpWorkingStatusName;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EmployeeTypeId;
	NSString * EmployeeTypeName;
	NSDate * EntryDate;
	NSString * Ethnicity;
	NSNumber * EthnicityId;
	NSString * FirstName;
	NSString * FullName;
	USBoolean * Gender;
	NSString * HomePhone;
	NSString * HomeTown;
	NSNumber * HomeTownId;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	NSString * ImageUrl;
	USBoolean * IsConcurrentPost;
	USBoolean * IsDeleted;
	USBoolean * IsOnlyYearOfBirthday;
	NSString * LastName;
	NSString * MainLanguage;
	NSNumber * MainLanguageId;
	NSString * Major;
	NSNumber * MajorId;
	NSNumber * MaritalStatusId;
	NSString * MaritalStatusValue;
	NSString * MobileNumber;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSString * OfficePhone;
	NSDate * OfficialDate;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgJobPositionName;
	NSNumber * OrgJobTitleId;
	NSString * OrgJobTitleLevelCode;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgUnitName;
	NSString * OrgWorkLevelCode;
	NSString * OrgWorkLevelDescription;
	NSNumber * OrgWorkLevelId;
	NSString * OrgWorkLevelName;
	NSString * PITCode;
	NSString * PITDateOfIssue;
	NSString * PITIssuedBy;
	NSDate * PassportDateOfExpire;
	NSDate * PassportDateOfIssue;
	NSString * PassportNo;
	NSString * PassportPlaceOfIssue;
	NSString * PermanentAddress;
	NSNumber * PermanentAddressId;
	NSString * PositionName;
	NSDate * ProbationaryDate;
	NSNumber * ProbationarySalary;
	NSString * Religion;
	NSNumber * ReligionId;
	NSString * TemporaryAddress;
	NSNumber * TemporaryAddressId;
	NSString * TimeWorkingFormCode;
	NSNumber * TimeWorkingFormId;
	NSString * UnitName;
	NSString * WorkLevelDescription;
	NSString * WorkLevelName;
	NSString * WorkLocation;
	NSDate * WorkingFormImplementationDate;
	NSString * WorkingFormTypeCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SP_EMP_ADS_EMPLOYEE_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSNumber * AddressId;
@property (retain) NSString * BankAccountNo;
@property (retain) NSString * BankBranch;
@property (retain) NSString * BankName;
@property (retain) NSDate * BirthDay;
@property (retain) NSString * BirthPlace;
@property (retain) NSNumber * BirthPlaceId;
@property (retain) NSString * CVUrl;
@property (retain) NSString * CodeAndFullName;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ContactAddress;
@property (retain) NSNumber * ContactAddressId;
@property (retain) NSString * CulturalLevel;
@property (retain) NSNumber * CulturalLevelId;
@property (retain) NSNumber * EducationLevelId;
@property (retain) NSString * EducationLevelName;
@property (retain) NSString * Email;
@property (retain) NSNumber * EmpBasicProfileId;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSString * EmployeeTypeName;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * Ethnicity;
@property (retain) NSNumber * EthnicityId;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * HomePhone;
@property (retain) NSString * HomeTown;
@property (retain) NSNumber * HomeTownId;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsConcurrentPost;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) NSString * LastName;
@property (retain) NSString * MainLanguage;
@property (retain) NSNumber * MainLanguageId;
@property (retain) NSString * Major;
@property (retain) NSNumber * MajorId;
@property (retain) NSNumber * MaritalStatusId;
@property (retain) NSString * MaritalStatusValue;
@property (retain) NSString * MobileNumber;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSString * OfficePhone;
@property (retain) NSDate * OfficialDate;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSNumber * OrgJobTitleId;
@property (retain) NSString * OrgJobTitleLevelCode;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitName;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSString * OrgWorkLevelDescription;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * OrgWorkLevelName;
@property (retain) NSString * PITCode;
@property (retain) NSString * PITDateOfIssue;
@property (retain) NSString * PITIssuedBy;
@property (retain) NSDate * PassportDateOfExpire;
@property (retain) NSDate * PassportDateOfIssue;
@property (retain) NSString * PassportNo;
@property (retain) NSString * PassportPlaceOfIssue;
@property (retain) NSString * PermanentAddress;
@property (retain) NSNumber * PermanentAddressId;
@property (retain) NSString * PositionName;
@property (retain) NSDate * ProbationaryDate;
@property (retain) NSNumber * ProbationarySalary;
@property (retain) NSString * Religion;
@property (retain) NSNumber * ReligionId;
@property (retain) NSString * TemporaryAddress;
@property (retain) NSNumber * TemporaryAddressId;
@property (retain) NSString * TimeWorkingFormCode;
@property (retain) NSNumber * TimeWorkingFormId;
@property (retain) NSString * UnitName;
@property (retain) NSString * WorkLevelDescription;
@property (retain) NSString * WorkLevelName;
@property (retain) NSString * WorkLocation;
@property (retain) NSDate * WorkingFormImplementationDate;
@property (retain) NSString * WorkingFormTypeCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSP_EMP_ADS_EMPLOYEE_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_EMP_ADS_EMPLOYEE_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSP_EMP_ADS_EMPLOYEE_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_EMP_ADS_EMPLOYEE_Result:(emp_tns1_SP_EMP_ADS_EMPLOYEE_Result *)toAdd;
@property (readonly) NSMutableArray * SP_EMP_ADS_EMPLOYEE_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SP_EMP_ADS_CONTRACT_Result : NSObject {
	
/* elements */
	NSString * AllocationEquipmentDescription;
	NSString * Allowances;
	NSString * AttachFile;
	NSNumber * BaseWage;
	NSString * Bonuses;
	NSString * CONTRACT_CODE_TEMPLATE;
	NSString * CONTRACT_TERM_CODE;
	NSString * CONTRACT_TERM_NAME_EN;
	NSString * CONTRACT_TERM_NAME_VN;
	NSString * CONTRACT_TYPE_CODE;
	NSString * CONTRACT_TYPE_NAME_EN;
	NSString * CONTRACT_TYPE_NAME_VN;
	NSNumber * CompanyId;
	NSString * CompensationDescription;
	NSString * ContractCode;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * DAYS_PERIOD;
	NSDate * DateSigned;
	NSDate * EmpAppraiseDate;
	NSString * EmpAppraiseNote;
	NSNumber * EmpAppraiseRating;
	NSNumber * EmpContractId;
	NSString * EmployeeRepresentativeAddress;
	NSDate * EmployeeRepresentativeBirthDay;
	NSString * EmployeeRepresentativeBirthPlace;
	NSString * EmployeeRepresentativeCode;
	NSString * EmployeeRepresentativeFullName;
	NSNumber * EmployeeRepresentativeId;
	NSString * EmployeeRepresentativeMajor;
	NSString * EmployeeRepresentativeNationality;
	NSString * EmployerNationality;
	NSString * EmployerPositionName;
	NSString * EmployerRepresentativeCode;
	NSString * EmployerRepresentativeFullName;
	NSNumber * EmployerRepresentativeId;
	NSString * EmployerRepresentativePositionName;
	NSDate * EndDate;
	NSDate * ExpirationDate;
	NSDate * HRAppraiseDate;
	NSString * HRAppraiseNote;
	NSNumber * HRAppraiseRating;
	NSString * HolIdayPolicy;
	NSNumber * HoursOfOfficialWork;
	USBoolean * IS_TERM_CONTRAC_TYPE;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	USBoolean * IsDeleted;
	USBoolean * IsEnd;
	USBoolean * IsProbation;
	USBoolean * IsSigned;
	NSString * JobName;
	NSString * JobNameEN;
	NSString * JobPositionName;
	NSString * JobPositionNameEN;
	NSString * JobResponsibilitiesDescription;
	NSString * LaborSupportEquipments;
	NSDate * LeaderAppraiseDate;
	NSString * LeaderAppraiseNote;
	NSNumber * LeaderAppraiseRating;
	NSNumber * MONTHS_PERIOD;
	NSString * MedicalInsurance;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSDate * OfficialWorkEndTime;
	NSDate * OfficialWorkStartTime;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrganizationAddress;
	NSString * OrganizationName;
	NSString * OrganizationPhone;
	NSString * OtherPolicy;
	NSString * PayDay;
	NSString * PayingMethod;
	NSString * PlaceSigned;
	NSDate * ProbationaryEndDate;
	NSDate * ProbationaryStartDate;
	NSNumber * ProbationaryTime;
	NSString * ProvidedEquipment;
	NSNumber * RelationEmpContractId;
	NSString * SalaryIncreasePolicy;
	NSString * SocialInsurance;
	NSDate * StartDate;
	NSString * TrainingPolicy;
	NSString * Transportation;
	NSString * UnitName;
	NSString * UnitNameEN;
	NSString * WorkingAddress;
	NSNumber * WorkingDay;
	NSString * WorkingTime;
	NSString * WorkingTimePolicy;
	NSString * cAllowance;
	NSString * cBaseSalary;
	NSString * cSocialInsuranceSalary;
	NSNumber * nAllowance;
	NSNumber * nSocialInsuranceSalary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SP_EMP_ADS_CONTRACT_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AllocationEquipmentDescription;
@property (retain) NSString * Allowances;
@property (retain) NSString * AttachFile;
@property (retain) NSNumber * BaseWage;
@property (retain) NSString * Bonuses;
@property (retain) NSString * CONTRACT_CODE_TEMPLATE;
@property (retain) NSString * CONTRACT_TERM_CODE;
@property (retain) NSString * CONTRACT_TERM_NAME_EN;
@property (retain) NSString * CONTRACT_TERM_NAME_VN;
@property (retain) NSString * CONTRACT_TYPE_CODE;
@property (retain) NSString * CONTRACT_TYPE_NAME_EN;
@property (retain) NSString * CONTRACT_TYPE_NAME_VN;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompensationDescription;
@property (retain) NSString * ContractCode;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * DAYS_PERIOD;
@property (retain) NSDate * DateSigned;
@property (retain) NSDate * EmpAppraiseDate;
@property (retain) NSString * EmpAppraiseNote;
@property (retain) NSNumber * EmpAppraiseRating;
@property (retain) NSNumber * EmpContractId;
@property (retain) NSString * EmployeeRepresentativeAddress;
@property (retain) NSDate * EmployeeRepresentativeBirthDay;
@property (retain) NSString * EmployeeRepresentativeBirthPlace;
@property (retain) NSString * EmployeeRepresentativeCode;
@property (retain) NSString * EmployeeRepresentativeFullName;
@property (retain) NSNumber * EmployeeRepresentativeId;
@property (retain) NSString * EmployeeRepresentativeMajor;
@property (retain) NSString * EmployeeRepresentativeNationality;
@property (retain) NSString * EmployerNationality;
@property (retain) NSString * EmployerPositionName;
@property (retain) NSString * EmployerRepresentativeCode;
@property (retain) NSString * EmployerRepresentativeFullName;
@property (retain) NSNumber * EmployerRepresentativeId;
@property (retain) NSString * EmployerRepresentativePositionName;
@property (retain) NSDate * EndDate;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSDate * HRAppraiseDate;
@property (retain) NSString * HRAppraiseNote;
@property (retain) NSNumber * HRAppraiseRating;
@property (retain) NSString * HolIdayPolicy;
@property (retain) NSNumber * HoursOfOfficialWork;
@property (retain) USBoolean * IS_TERM_CONTRAC_TYPE;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEnd;
@property (retain) USBoolean * IsProbation;
@property (retain) USBoolean * IsSigned;
@property (retain) NSString * JobName;
@property (retain) NSString * JobNameEN;
@property (retain) NSString * JobPositionName;
@property (retain) NSString * JobPositionNameEN;
@property (retain) NSString * JobResponsibilitiesDescription;
@property (retain) NSString * LaborSupportEquipments;
@property (retain) NSDate * LeaderAppraiseDate;
@property (retain) NSString * LeaderAppraiseNote;
@property (retain) NSNumber * LeaderAppraiseRating;
@property (retain) NSNumber * MONTHS_PERIOD;
@property (retain) NSString * MedicalInsurance;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * OfficialWorkEndTime;
@property (retain) NSDate * OfficialWorkStartTime;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrganizationAddress;
@property (retain) NSString * OrganizationName;
@property (retain) NSString * OrganizationPhone;
@property (retain) NSString * OtherPolicy;
@property (retain) NSString * PayDay;
@property (retain) NSString * PayingMethod;
@property (retain) NSString * PlaceSigned;
@property (retain) NSDate * ProbationaryEndDate;
@property (retain) NSDate * ProbationaryStartDate;
@property (retain) NSNumber * ProbationaryTime;
@property (retain) NSString * ProvidedEquipment;
@property (retain) NSNumber * RelationEmpContractId;
@property (retain) NSString * SalaryIncreasePolicy;
@property (retain) NSString * SocialInsurance;
@property (retain) NSDate * StartDate;
@property (retain) NSString * TrainingPolicy;
@property (retain) NSString * Transportation;
@property (retain) NSString * UnitName;
@property (retain) NSString * UnitNameEN;
@property (retain) NSString * WorkingAddress;
@property (retain) NSNumber * WorkingDay;
@property (retain) NSString * WorkingTime;
@property (retain) NSString * WorkingTimePolicy;
@property (retain) NSString * cAllowance;
@property (retain) NSString * cBaseSalary;
@property (retain) NSString * cSocialInsuranceSalary;
@property (retain) NSNumber * nAllowance;
@property (retain) NSNumber * nSocialInsuranceSalary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSP_EMP_ADS_CONTRACT_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_EMP_ADS_CONTRACT_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSP_EMP_ADS_CONTRACT_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_EMP_ADS_CONTRACT_Result:(emp_tns1_SP_EMP_ADS_CONTRACT_Result *)toAdd;
@property (readonly) NSMutableArray * SP_EMP_ADS_CONTRACT_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SP_EMP_ADS_EMP_OFF_Result : NSObject {
	
/* elements */
	NSDate * ApproveDate;
	NSString * ApproveNote;
	NSString * ApproverCode;
	NSString * ApproverFullName;
	NSNumber * ApproverId;
	NSString * CLogEmpOffTypeCode;
	NSNumber * CLogEmpOffTypeId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * DecisionCode;
	NSString * DecisionFile;
	NSString * DecisionName;
	NSNumber * EmpOffProfileId;
	NSString * EmpOffTypeName;
	NSNumber * EmpProfileJobPositionId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EmployeeTypeId;
	NSString * EmployeeTypeName;
	NSDate * EndWorkingDate;
	NSDate * EntryDate;
	NSString * FirstName;
	NSString * FullName;
	NSDate * HICardExpireDate;
	NSDate * HandoverDate;
	NSString * ImageUrl;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	USBoolean * IsMainPost;
	USBoolean * IsReturnEquipment;
	USBoolean * IsReturnHICard;
	USBoolean * IsViolatedRequest;
	NSString * LastName;
	NSDate * LastWorkingDate;
	NSNumber * LeaveDaysNotUsed;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * OffCoefficientAllowance;
	NSDate * OffFromDate;
	NSDate * OffRequestDate;
	NSDate * OfficialDate;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgJobPositionName;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgUnitName;
	NSString * OrgWorkLevelCode;
	NSNumber * OrgWorkLevelId;
	NSString * OrgWorkLevelName;
	NSDate * ProbationaryDate;
	NSString * Reason;
	NSDate * ReturnHICardDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SP_EMP_ADS_EMP_OFF_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ApproveDate;
@property (retain) NSString * ApproveNote;
@property (retain) NSString * ApproverCode;
@property (retain) NSString * ApproverFullName;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * CLogEmpOffTypeCode;
@property (retain) NSNumber * CLogEmpOffTypeId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * DecisionCode;
@property (retain) NSString * DecisionFile;
@property (retain) NSString * DecisionName;
@property (retain) NSNumber * EmpOffProfileId;
@property (retain) NSString * EmpOffTypeName;
@property (retain) NSNumber * EmpProfileJobPositionId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSString * EmployeeTypeName;
@property (retain) NSDate * EndWorkingDate;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) NSDate * HICardExpireDate;
@property (retain) NSDate * HandoverDate;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainPost;
@property (retain) USBoolean * IsReturnEquipment;
@property (retain) USBoolean * IsReturnHICard;
@property (retain) USBoolean * IsViolatedRequest;
@property (retain) NSString * LastName;
@property (retain) NSDate * LastWorkingDate;
@property (retain) NSNumber * LeaveDaysNotUsed;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * OffCoefficientAllowance;
@property (retain) NSDate * OffFromDate;
@property (retain) NSDate * OffRequestDate;
@property (retain) NSDate * OfficialDate;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitName;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * OrgWorkLevelName;
@property (retain) NSDate * ProbationaryDate;
@property (retain) NSString * Reason;
@property (retain) NSDate * ReturnHICardDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSP_EMP_ADS_EMP_OFF_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_EMP_ADS_EMP_OFF_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSP_EMP_ADS_EMP_OFF_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_EMP_ADS_EMP_OFF_Result:(emp_tns1_SP_EMP_ADS_EMP_OFF_Result *)toAdd;
@property (readonly) NSMutableArray * SP_EMP_ADS_EMP_OFF_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CBAccidentInsurance : NSObject {
	
/* elements */
	NSString * AccidentInsuranceContractCode;
	NSString * AccidentInsuranceContractNumber;
	NSString * AccidentName;
	NSNumber * CBAccidentInsuranceId;
	NSNumber * Charge;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * EffectDate;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSDate * ExpireDate;
	NSString * InsuranceCompanyName;
	NSString * InsuranceKind;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CBAccidentInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AccidentInsuranceContractCode;
@property (retain) NSString * AccidentInsuranceContractNumber;
@property (retain) NSString * AccidentName;
@property (retain) NSNumber * CBAccidentInsuranceId;
@property (retain) NSNumber * Charge;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EffectDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpireDate;
@property (retain) NSString * InsuranceCompanyName;
@property (retain) NSString * InsuranceKind;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCBAccidentInsurance : NSObject {
	
/* elements */
	NSMutableArray *CBAccidentInsurance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCBAccidentInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBAccidentInsurance:(emp_tns1_CBAccidentInsurance *)toAdd;
@property (readonly) NSMutableArray * CBAccidentInsurance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CBCompensationTypeFactor : NSObject {
	
/* elements */
	NSNumber * CBCompensationTypeFactorId;
	NSNumber * CLogCBCompensationTypeId;
	emp_tns1_CLogCBFactor * CLogCBFactor;
	NSNumber * CLogCBFactorId;
	USBoolean * IsDeleted;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CBCompensationTypeFactor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBCompensationTypeFactorId;
@property (retain) NSNumber * CLogCBCompensationTypeId;
@property (retain) emp_tns1_CLogCBFactor * CLogCBFactor;
@property (retain) NSNumber * CLogCBFactorId;
@property (retain) USBoolean * IsDeleted;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCBCompensationTypeFactor : NSObject {
	
/* elements */
	NSMutableArray *CBCompensationTypeFactor;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCBCompensationTypeFactor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBCompensationTypeFactor:(emp_tns1_CBCompensationTypeFactor *)toAdd;
@property (readonly) NSMutableArray * CBCompensationTypeFactor;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogCBFactor : NSObject {
	
/* elements */
	NSMutableArray *CLogCBFactor;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogCBFactor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogCBFactor:(emp_tns1_CLogCBFactor *)toAdd;
@property (readonly) NSMutableArray * CLogCBFactor;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogCBCompensationCategory : NSObject {
	
/* elements */
	NSNumber * CLogCBCompensationCategoryId;
	emp_tns1_ArrayOfCLogCBFactor * CLogCBFactors;
	NSString * CLogCompensationCategoryCode;
	NSString * Description;
	USBoolean * IsDeleted;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogCBCompensationCategory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogCBCompensationCategoryId;
@property (retain) emp_tns1_ArrayOfCLogCBFactor * CLogCBFactors;
@property (retain) NSString * CLogCompensationCategoryCode;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogCBFactor : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCBCompensationTypeFactor * CBCompensationTypeFactors;
	emp_tns1_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
	emp_tns1_CLogCBCompensationCategory * CLogCBCompensationCategory;
	NSNumber * CLogCBCompensationCategoryId;
	NSString * CLogCBFactorCode;
	NSNumber * CLogCBFactorGroupId;
	NSNumber * CLogCBFactorId;
	NSString * Description;
	NSString * FormulaExpression;
	USBoolean * IsActived;
	USBoolean * IsDeactived;
	USBoolean * IsDeleted;
	USBoolean * IsInitial;
	NSString * Name;
	NSNumber * Priority;
	NSString * ServiceReferenceEndPointAddress;
	NSString * ServiceReferenceEndPointOperationContract;
	NSString * TableReference;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogCBFactor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCBCompensationTypeFactor * CBCompensationTypeFactors;
@property (retain) emp_tns1_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
@property (retain) emp_tns1_CLogCBCompensationCategory * CLogCBCompensationCategory;
@property (retain) NSNumber * CLogCBCompensationCategoryId;
@property (retain) NSString * CLogCBFactorCode;
@property (retain) NSNumber * CLogCBFactorGroupId;
@property (retain) NSNumber * CLogCBFactorId;
@property (retain) NSString * Description;
@property (retain) NSString * FormulaExpression;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeactived;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInitial;
@property (retain) NSString * Name;
@property (retain) NSNumber * Priority;
@property (retain) NSString * ServiceReferenceEndPointAddress;
@property (retain) NSString * ServiceReferenceEndPointOperationContract;
@property (retain) NSString * TableReference;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CBFactorEmployeeMetaData : NSObject {
	
/* elements */
	NSDate * BeginDate;
	emp_tns1_CBCompensationEmployeeGroup * CBCompensationEmployeeGroup;
	NSNumber * CBCompensationEmployeeGroupId;
	NSString * CBFactorEmployeeMetaDataDescription;
	NSNumber * CBFactorEmployeeMetaDataId;
	emp_tns1_CLogCBFactor * CLogCBFactor;
	NSNumber * CLogCBFactorId;
	NSString * ChangeReason;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	NSNumber * FactorValue;
	NSString * GroupGuid;
	USBoolean * IsActivated;
	USBoolean * IsAppliedForCompany;
	USBoolean * IsChanged;
	USBoolean * IsCurrent;
	USBoolean * IsDeactivated;
	USBoolean * IsDeleted;
	USBoolean * IsInitialed;
	NSDate * ModifiedDate;
	NSNumber * OldFactorValue;
	NSString * ReferenceColunmName;
	NSString * ReferenceRowGuid;
	NSString * ReferenceTableName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CBFactorEmployeeMetaData *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * BeginDate;
@property (retain) emp_tns1_CBCompensationEmployeeGroup * CBCompensationEmployeeGroup;
@property (retain) NSNumber * CBCompensationEmployeeGroupId;
@property (retain) NSString * CBFactorEmployeeMetaDataDescription;
@property (retain) NSNumber * CBFactorEmployeeMetaDataId;
@property (retain) emp_tns1_CLogCBFactor * CLogCBFactor;
@property (retain) NSNumber * CLogCBFactorId;
@property (retain) NSString * ChangeReason;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) NSNumber * FactorValue;
@property (retain) NSString * GroupGuid;
@property (retain) USBoolean * IsActivated;
@property (retain) USBoolean * IsAppliedForCompany;
@property (retain) USBoolean * IsChanged;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeactivated;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInitialed;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OldFactorValue;
@property (retain) NSString * ReferenceColunmName;
@property (retain) NSString * ReferenceRowGuid;
@property (retain) NSString * ReferenceTableName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCBFactorEmployeeMetaData : NSObject {
	
/* elements */
	NSMutableArray *CBFactorEmployeeMetaData;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCBFactorEmployeeMetaData *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBFactorEmployeeMetaData:(emp_tns1_CBFactorEmployeeMetaData *)toAdd;
@property (readonly) NSMutableArray * CBFactorEmployeeMetaData;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CBCompensationEmployeeGroup : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCBCompensationEmployeeGroupDetail * CBCompensationEmployeeGroupDetails;
	NSNumber * CBCompensationEmployeeGroupId;
	emp_tns1_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
	NSNumber * CompanyId;
	NSString * Description;
	USBoolean * IsActived;
	USBoolean * IsDeactived;
	USBoolean * IsDeleted;
	USBoolean * IsInitial;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CBCompensationEmployeeGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCBCompensationEmployeeGroupDetail * CBCompensationEmployeeGroupDetails;
@property (retain) NSNumber * CBCompensationEmployeeGroupId;
@property (retain) emp_tns1_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeactived;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInitial;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CBCompensationEmployeeGroupDetail : NSObject {
	
/* elements */
	emp_tns1_CBCompensationEmployeeGroup * CBCompensationEmployeeGroup;
	NSNumber * CBCompensationEmployeeGroupDetailId;
	NSNumber * CBCompensationEmployeeGroupId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CBCompensationEmployeeGroupDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CBCompensationEmployeeGroup * CBCompensationEmployeeGroup;
@property (retain) NSNumber * CBCompensationEmployeeGroupDetailId;
@property (retain) NSNumber * CBCompensationEmployeeGroupId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCBCompensationEmployeeGroupDetail : NSObject {
	
/* elements */
	NSMutableArray *CBCompensationEmployeeGroupDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCBCompensationEmployeeGroupDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBCompensationEmployeeGroupDetail:(emp_tns1_CBCompensationEmployeeGroupDetail *)toAdd;
@property (readonly) NSMutableArray * CBCompensationEmployeeGroupDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CBConvalescence : NSObject {
	
/* elements */
	NSNumber * AllowanceAmount;
	NSNumber * CBConvalescenceId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSDate * FromDate;
	NSNumber * HealthLessLevel;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * NumberDayOff;
	NSDate * NumberOfMounthApplied;
	NSDate * ToDate;
	NSNumber * TypeId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CBConvalescence *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AllowanceAmount;
@property (retain) NSNumber * CBConvalescenceId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * FromDate;
@property (retain) NSNumber * HealthLessLevel;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * NumberDayOff;
@property (retain) NSDate * NumberOfMounthApplied;
@property (retain) NSDate * ToDate;
@property (retain) NSNumber * TypeId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCBConvalescence : NSObject {
	
/* elements */
	NSMutableArray *CBConvalescence;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCBConvalescence *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBConvalescence:(emp_tns1_CBConvalescence *)toAdd;
@property (readonly) NSMutableArray * CBConvalescence;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogCBDisease : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
	NSNumber * CLogCBDiseaseId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	NSString * DiseaseCode;
	NSString * DiseaseGroupCode;
	USBoolean * IsChronic;
	USBoolean * IsDeleted;
	USBoolean * IsOccupational;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogCBDisease *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
@property (retain) NSNumber * CLogCBDiseaseId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) NSString * DiseaseCode;
@property (retain) NSString * DiseaseGroupCode;
@property (retain) USBoolean * IsChronic;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOccupational;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CBHealthInsurance : NSObject {
	
/* elements */
	NSNumber * CBHealthInsuranceId;
	emp_tns1_CLogHospital * CLogHospital;
	NSNumber * CLogHospitalId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * EffectDate;
	NSDate * ExpireDate;
	NSNumber * GrossCharge;
	NSString * HealthInsuranceContractCode;
	NSString * HealthInsuranceContractNumber;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Remark;
	NSNumber * SignerId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CBHealthInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBHealthInsuranceId;
@property (retain) emp_tns1_CLogHospital * CLogHospital;
@property (retain) NSNumber * CLogHospitalId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EffectDate;
@property (retain) NSDate * ExpireDate;
@property (retain) NSNumber * GrossCharge;
@property (retain) NSString * HealthInsuranceContractCode;
@property (retain) NSString * HealthInsuranceContractNumber;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Remark;
@property (retain) NSNumber * SignerId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCBHealthInsurance : NSObject {
	
/* elements */
	NSMutableArray *CBHealthInsurance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCBHealthInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBHealthInsurance:(emp_tns1_CBHealthInsurance *)toAdd;
@property (readonly) NSMutableArray * CBHealthInsurance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogCity : NSObject {
	
/* elements */
	NSMutableArray *CLogCity;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogCity *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogCity:(emp_tns1_CLogCity *)toAdd;
@property (readonly) NSMutableArray * CLogCity;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogCountry : NSObject {
	
/* elements */
	emp_tns1_ArrayOfCLogCity * CLogCities;
	NSString * CLogCountryCode;
	NSNumber * CLogCountryId;
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogCountry *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_ArrayOfCLogCity * CLogCities;
@property (retain) NSString * CLogCountryCode;
@property (retain) NSNumber * CLogCountryId;
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogDistrict : NSObject {
	
/* elements */
	emp_tns1_CLogCity * CLogCity;
	NSString * CLogCityCode;
	NSNumber * CLogCityId;
	NSString * CLogDistrictCode;
	NSNumber * CLogDistrictId;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogDistrict *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogCity * CLogCity;
@property (retain) NSString * CLogCityCode;
@property (retain) NSNumber * CLogCityId;
@property (retain) NSString * CLogDistrictCode;
@property (retain) NSNumber * CLogDistrictId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogDistrict : NSObject {
	
/* elements */
	NSMutableArray *CLogDistrict;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogDistrict *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogDistrict:(emp_tns1_CLogDistrict *)toAdd;
@property (readonly) NSMutableArray * CLogDistrict;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogHospital : NSObject {
	
/* elements */
	NSMutableArray *CLogHospital;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogHospital *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogHospital:(emp_tns1_CLogHospital *)toAdd;
@property (readonly) NSMutableArray * CLogHospital;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmployee : NSObject {
	
/* elements */
	NSMutableArray *Employee;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmployee:(emp_tns1_Employee *)toAdd;
@property (readonly) NSMutableArray * Employee;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogCity : NSObject {
	
/* elements */
	NSString * CLogCityCode;
	NSNumber * CLogCityId;
	NSString * CLogCityNoIns;
	emp_tns1_CLogCountry * CLogCountry;
	NSString * CLogCountryCode;
	NSNumber * CLogCountryId;
	emp_tns1_ArrayOfCLogDistrict * CLogDistricts;
	emp_tns1_ArrayOfCLogHospital * CLogHospitals;
	NSNumber * CompanyId;
	emp_tns1_ArrayOfEmployee * Employees;
	emp_tns1_ArrayOfEmployee * Employees1;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
	NSString * SysNo;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogCity *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogCityCode;
@property (retain) NSNumber * CLogCityId;
@property (retain) NSString * CLogCityNoIns;
@property (retain) emp_tns1_CLogCountry * CLogCountry;
@property (retain) NSString * CLogCountryCode;
@property (retain) NSNumber * CLogCountryId;
@property (retain) emp_tns1_ArrayOfCLogDistrict * CLogDistricts;
@property (retain) emp_tns1_ArrayOfCLogHospital * CLogHospitals;
@property (retain) NSNumber * CompanyId;
@property (retain) emp_tns1_ArrayOfEmployee * Employees;
@property (retain) emp_tns1_ArrayOfEmployee * Employees1;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
@property (retain) NSString * SysNo;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogHospital : NSObject {
	
/* elements */
	NSString * Address;
	emp_tns1_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
	emp_tns1_ArrayOfCBHealthInsurance * CBHealthInsurances;
	emp_tns1_CLogCity * CLogCity;
	NSNumber * CLogCityId;
	NSString * CLogCityNoIns;
	NSString * CLogHospitalCode;
	NSNumber * CLogHospitalId;
	NSNumber * CompanyId;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogHospital *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) emp_tns1_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
@property (retain) emp_tns1_ArrayOfCBHealthInsurance * CBHealthInsurances;
@property (retain) emp_tns1_CLogCity * CLogCity;
@property (retain) NSNumber * CLogCityId;
@property (retain) NSString * CLogCityNoIns;
@property (retain) NSString * CLogHospitalCode;
@property (retain) NSNumber * CLogHospitalId;
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CBDayOffSocialInsurance : NSObject {
	
/* elements */
	NSNumber * CBDayOffSocialInsuranceId;
	emp_tns1_CLogCBDisease * CLogCBDisease;
	NSNumber * CLogCBDiseaseId;
	emp_tns1_CLogHospital * CLogHospital;
	NSNumber * CLogHospitalId;
	NSNumber * ClogCBDayOffTypeId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * DoctorName;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSDate * FromDate;
	USBoolean * IsDeleted;
	USBoolean * IsPaid;
	USBoolean * IsWorkAccident;
	NSDate * ModifiedDate;
	NSNumber * NumberDayOff;
	NSNumber * NumberDayOffByDoctor;
	NSNumber * NumberPaidDay;
	NSString * Reason;
	NSDate * ToDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CBDayOffSocialInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBDayOffSocialInsuranceId;
@property (retain) emp_tns1_CLogCBDisease * CLogCBDisease;
@property (retain) NSNumber * CLogCBDiseaseId;
@property (retain) emp_tns1_CLogHospital * CLogHospital;
@property (retain) NSNumber * CLogHospitalId;
@property (retain) NSNumber * ClogCBDayOffTypeId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DoctorName;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * FromDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsPaid;
@property (retain) USBoolean * IsWorkAccident;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NumberDayOff;
@property (retain) NSNumber * NumberDayOffByDoctor;
@property (retain) NSNumber * NumberPaidDay;
@property (retain) NSString * Reason;
@property (retain) NSDate * ToDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCBDayOffSocialInsurance : NSObject {
	
/* elements */
	NSMutableArray *CBDayOffSocialInsurance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCBDayOffSocialInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBDayOffSocialInsurance:(emp_tns1_CBDayOffSocialInsurance *)toAdd;
@property (readonly) NSMutableArray * CBDayOffSocialInsurance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CBHealthInsuranceDetail : NSObject {
	
/* elements */
	NSNumber * CBHealthInsuranceDetailId;
	NSNumber * CBHealthInsuranceId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CBHealthInsuranceDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBHealthInsuranceDetailId;
@property (retain) NSNumber * CBHealthInsuranceId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCBHealthInsuranceDetail : NSObject {
	
/* elements */
	NSMutableArray *CBHealthInsuranceDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCBHealthInsuranceDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBHealthInsuranceDetail:(emp_tns1_CBHealthInsuranceDetail *)toAdd;
@property (readonly) NSMutableArray * CBHealthInsuranceDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogEmployeeType : NSObject {
	
/* elements */
	NSString * CLogEmployeeTypeCode;
	NSNumber * CLogEmployeeTypeId;
	emp_tns1_ArrayOfEmployee * Employees;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogEmployeeType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogEmployeeTypeCode;
@property (retain) NSNumber * CLogEmployeeTypeId;
@property (retain) emp_tns1_ArrayOfEmployee * Employees;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogEmpWorkingStatu : NSObject {
	
/* elements */
	NSNumber * CLogEmpWorkingStatusId;
	NSNumber * CompanyId;
	emp_tns1_ArrayOfEmpBasicProfile * EmpBasicProfiles;
	NSString * EmpWorkingStatusCode;
	NSString * EmpWorkingStatusName;
	NSString * EmpWorkingStatusNameEN;
	USBoolean * IsDeleted;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogEmpWorkingStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogEmpWorkingStatusId;
@property (retain) NSNumber * CompanyId;
@property (retain) emp_tns1_ArrayOfEmpBasicProfile * EmpBasicProfiles;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmpWorkingStatusNameEN;
@property (retain) USBoolean * IsDeleted;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpBasicProfile : NSObject {
	
/* elements */
	NSString * BankAccountNo;
	NSString * BankBranch;
	NSString * BankName;
	NSNumber * CLogBankBranchId;
	emp_tns1_CLogEmpWorkingStatu * CLogEmpWorkingStatu;
	NSString * CVUrl;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * CulturalLevel;
	NSNumber * CulturalLevelId;
	NSString * EducationLevel;
	NSNumber * EducationLevelId;
	NSNumber * EmpBasicProfileId;
	NSNumber * EmpWorkingStatusId;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardPlaceOfIssue;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	NSString * Major;
	NSNumber * MajorId;
	NSString * MaritalStatus;
	NSNumber * MaritalStatusId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSString * PITCode;
	NSString * PITIssuedBy;
	NSDate * PassportDateOfExpire;
	NSDate * PassportDateOfIssue;
	NSString * PassportNo;
	NSString * PassportPlaceOfIssue;
	NSNumber * ProbationarySalary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpBasicProfile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * BankAccountNo;
@property (retain) NSString * BankBranch;
@property (retain) NSString * BankName;
@property (retain) NSNumber * CLogBankBranchId;
@property (retain) emp_tns1_CLogEmpWorkingStatu * CLogEmpWorkingStatu;
@property (retain) NSString * CVUrl;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * CulturalLevel;
@property (retain) NSNumber * CulturalLevelId;
@property (retain) NSString * EducationLevel;
@property (retain) NSNumber * EducationLevelId;
@property (retain) NSNumber * EmpBasicProfileId;
@property (retain) NSNumber * EmpWorkingStatusId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Major;
@property (retain) NSNumber * MajorId;
@property (retain) NSString * MaritalStatus;
@property (retain) NSNumber * MaritalStatusId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSString * PITCode;
@property (retain) NSString * PITIssuedBy;
@property (retain) NSDate * PassportDateOfExpire;
@property (retain) NSDate * PassportDateOfIssue;
@property (retain) NSString * PassportNo;
@property (retain) NSString * PassportPlaceOfIssue;
@property (retain) NSNumber * ProbationarySalary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpBasicProfile : NSObject {
	
/* elements */
	NSMutableArray *EmpBasicProfile;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpBasicProfile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpBasicProfile:(emp_tns1_EmpBasicProfile *)toAdd;
@property (readonly) NSMutableArray * EmpBasicProfile;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProficencyLevelRating : NSObject {
	
/* elements */
	NSMutableArray *EmpProficencyLevelRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProficencyLevelRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProficencyLevelRating:(emp_tns1_EmpProficencyLevelRating *)toAdd;
@property (readonly) NSMutableArray * EmpProficencyLevelRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpCompetencyRating : NSObject {
	
/* elements */
	NSDate * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpCompetencyRatingId;
	emp_tns1_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpCompetencyRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpCompetencyRatingId;
@property (retain) emp_tns1_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProficencyLevelDetailRating : NSObject {
	
/* elements */
	NSMutableArray *EmpProficencyLevelDetailRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProficencyLevelDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProficencyLevelDetailRating:(emp_tns1_EmpProficencyLevelDetailRating *)toAdd;
@property (readonly) NSMutableArray * EmpProficencyLevelDetailRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfLMSCourse : NSObject {
	
/* elements */
	NSMutableArray *LMSCourse;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfLMSCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourse:(emp_tns1_LMSCourse *)toAdd;
@property (readonly) NSMutableArray * LMSCourse;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogCourseStatu : NSObject {
	
/* elements */
	NSNumber * CourseStatusId;
	USBoolean * IsDeleted;
	emp_tns1_ArrayOfLMSCourse * LMSCourses;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogCourseStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CourseStatusId;
@property (retain) USBoolean * IsDeleted;
@property (retain) emp_tns1_ArrayOfLMSCourse * LMSCourses;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_GPEmployeeScoreCardAppraisal : NSObject {
	
/* elements */
	NSNumber * AppraisalTime;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
	NSNumber * GPEmployeeScoreCardAppraisalId;
	NSNumber * GPEmployeeScoreCardId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * Score;
	NSString * SelfComment;
	NSNumber * SelfRating;
	NSNumber * SelfScore;
	NSNumber * Status;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_GPEmployeeScoreCardAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AppraisalTime;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
@property (retain) NSNumber * GPEmployeeScoreCardAppraisalId;
@property (retain) NSNumber * GPEmployeeScoreCardId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * SelfScore;
@property (retain) NSNumber * Status;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfGPEmployeeScoreCardAppraisal : NSObject {
	
/* elements */
	NSMutableArray *GPEmployeeScoreCardAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfGPEmployeeScoreCardAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPEmployeeScoreCardAppraisal:(emp_tns1_GPEmployeeScoreCardAppraisal *)toAdd;
@property (readonly) NSMutableArray * GPEmployeeScoreCardAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_GPEmployeeScoreCardProgress : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
	NSNumber * GPEmployeeScoreCardId;
	NSNumber * GPEmployeeScoreCardProgressId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * NewValue;
	NSNumber * OldValue;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_GPEmployeeScoreCardProgress *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
@property (retain) NSNumber * GPEmployeeScoreCardId;
@property (retain) NSNumber * GPEmployeeScoreCardProgressId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NewValue;
@property (retain) NSNumber * OldValue;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfGPEmployeeScoreCardProgress : NSObject {
	
/* elements */
	NSMutableArray *GPEmployeeScoreCardProgress;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfGPEmployeeScoreCardProgress *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPEmployeeScoreCardProgress:(emp_tns1_GPEmployeeScoreCardProgress *)toAdd;
@property (readonly) NSMutableArray * GPEmployeeScoreCardProgress;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_GPAdditionAppraisal : NSObject {
	
/* elements */
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_Employee * Employee;
	emp_tns1_Employee * Employee1;
	NSNumber * EmployeeId;
	NSNumber * GPAdditionAppraisalId;
	emp_tns1_GPExecutionPlan * GPExecutionPlan;
	NSNumber * GPExecutionPlanId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * SupervisorId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_GPAdditionAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) emp_tns1_Employee * Employee1;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * GPAdditionAppraisalId;
@property (retain) emp_tns1_GPExecutionPlan * GPExecutionPlan;
@property (retain) NSNumber * GPExecutionPlanId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * SupervisorId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfGPAdditionAppraisal : NSObject {
	
/* elements */
	NSMutableArray *GPAdditionAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfGPAdditionAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPAdditionAppraisal:(emp_tns1_GPAdditionAppraisal *)toAdd;
@property (readonly) NSMutableArray * GPAdditionAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_GPEmployeeWholePlanAppraisal : NSObject {
	
/* elements */
	NSNumber * AppraisalTime;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * GPEmployeeWholePlanAppraisalId;
	emp_tns1_GPExecutionPlan * GPExecutionPlan;
	NSNumber * GPExecutionPlanId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * Result;
	NSNumber * Score;
	NSString * SelfComment;
	NSNumber * SelfRating;
	NSNumber * SelfScore;
	NSNumber * Status;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_GPEmployeeWholePlanAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AppraisalTime;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * GPEmployeeWholePlanAppraisalId;
@property (retain) emp_tns1_GPExecutionPlan * GPExecutionPlan;
@property (retain) NSNumber * GPExecutionPlanId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * Result;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * SelfScore;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfGPEmployeeWholePlanAppraisal : NSObject {
	
/* elements */
	NSMutableArray *GPEmployeeWholePlanAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfGPEmployeeWholePlanAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPEmployeeWholePlanAppraisal:(emp_tns1_GPEmployeeWholePlanAppraisal *)toAdd;
@property (readonly) NSMutableArray * GPEmployeeWholePlanAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_GPPerformanceYearEndResult : NSObject {
	
/* elements */
	NSString * ActivityName;
	NSNumber * Actual;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	emp_tns1_GPExecutionPlan * GPExecutionPlan;
	NSNumber * GPPerformancePlanId;
	NSNumber * GPPerformanceYearEndResultId;
	USBoolean * HalfYearResult;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Progress;
	NSNumber * Rate;
	NSString * Result;
	NSNumber * Target;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_GPPerformanceYearEndResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ActivityName;
@property (retain) NSNumber * Actual;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) emp_tns1_GPExecutionPlan * GPExecutionPlan;
@property (retain) NSNumber * GPPerformancePlanId;
@property (retain) NSNumber * GPPerformanceYearEndResultId;
@property (retain) USBoolean * HalfYearResult;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Progress;
@property (retain) NSNumber * Rate;
@property (retain) NSString * Result;
@property (retain) NSNumber * Target;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfGPPerformanceYearEndResult : NSObject {
	
/* elements */
	NSMutableArray *GPPerformanceYearEndResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfGPPerformanceYearEndResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPPerformanceYearEndResult:(emp_tns1_GPPerformanceYearEndResult *)toAdd;
@property (readonly) NSMutableArray * GPPerformanceYearEndResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_GPExecutionPlan : NSObject {
	
/* elements */
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmployeeId;
	emp_tns1_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals;
	emp_tns1_ArrayOfGPEmployeeWholePlanAppraisal * GPEmployeeWholePlanAppraisals;
	emp_tns1_ArrayOfGPExecutionPlanDetail * GPExecutionPlanDetails;
	NSNumber * GPExecutionPlanId;
	emp_tns1_ArrayOfGPPerformanceYearEndResult * GPPerformanceYearEndResults;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * Period;
	NSNumber * Status;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_GPExecutionPlan *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmployeeId;
@property (retain) emp_tns1_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals;
@property (retain) emp_tns1_ArrayOfGPEmployeeWholePlanAppraisal * GPEmployeeWholePlanAppraisals;
@property (retain) emp_tns1_ArrayOfGPExecutionPlanDetail * GPExecutionPlanDetails;
@property (retain) NSNumber * GPExecutionPlanId;
@property (retain) emp_tns1_ArrayOfGPPerformanceYearEndResult * GPPerformanceYearEndResults;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * Period;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_GPExecutionPlanActivityAppraisal : NSObject {
	
/* elements */
	NSNumber * AppraisalTime;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_GPExecutionPlanActivity * GPExecutionPlanActivity;
	NSNumber * GPExecutionPlanActivityAppraisalId;
	NSNumber * GPExecutionPlanActivityId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * Score;
	NSString * SelfComment;
	NSNumber * SelfRating;
	NSNumber * SelfScore;
	NSNumber * Status;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_GPExecutionPlanActivityAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AppraisalTime;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_GPExecutionPlanActivity * GPExecutionPlanActivity;
@property (retain) NSNumber * GPExecutionPlanActivityAppraisalId;
@property (retain) NSNumber * GPExecutionPlanActivityId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * SelfScore;
@property (retain) NSNumber * Status;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfGPExecutionPlanActivityAppraisal : NSObject {
	
/* elements */
	NSMutableArray *GPExecutionPlanActivityAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfGPExecutionPlanActivityAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPExecutionPlanActivityAppraisal:(emp_tns1_GPExecutionPlanActivityAppraisal *)toAdd;
@property (readonly) NSMutableArray * GPExecutionPlanActivityAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_GPPeriodicReport : NSObject {
	
/* elements */
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_GPExecutionPlanActivity * GPExecutionPlanActivity;
	NSNumber * GPExecutionPlanActivityId;
	NSNumber * GPPeriodicReportId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSDate * ReportDateTime;
	NSNumber * ReportTimes;
	NSNumber * Status;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_GPPeriodicReport *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_GPExecutionPlanActivity * GPExecutionPlanActivity;
@property (retain) NSNumber * GPExecutionPlanActivityId;
@property (retain) NSNumber * GPPeriodicReportId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * ReportDateTime;
@property (retain) NSNumber * ReportTimes;
@property (retain) NSNumber * Status;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfGPPeriodicReport : NSObject {
	
/* elements */
	NSMutableArray *GPPeriodicReport;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfGPPeriodicReport *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPPeriodicReport:(emp_tns1_GPPeriodicReport *)toAdd;
@property (readonly) NSMutableArray * GPPeriodicReport;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_GPExecutionPlanActivity : NSObject {
	
/* elements */
	NSString * AchievementLevel;
	NSString * ActivityGroup;
	NSString * ActivityName;
	NSNumber * Budget;
	NSString * BudgetUnit;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * EndDate;
	emp_tns1_ArrayOfGPExecutionPlanActivityAppraisal * GPExecutionPlanActivityAppraisals;
	NSNumber * GPExecutionPlanActivityId;
	emp_tns1_GPExecutionPlanDetail * GPExecutionPlanDetail;
	NSNumber * GPExecutionPlanDetailId;
	emp_tns1_ArrayOfGPPeriodicReport * GPPeriodicReports;
	USBoolean * IsDeleted;
	NSNumber * Measure;
	NSString * MeasureUnit;
	NSDate * ModifiedDate;
	NSDate * StartDate;
	NSString * Status;
	NSNumber * Target;
	NSString * TargetUnit;
	NSNumber * Variance;
	NSString * VarianceUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_GPExecutionPlanActivity *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AchievementLevel;
@property (retain) NSString * ActivityGroup;
@property (retain) NSString * ActivityName;
@property (retain) NSNumber * Budget;
@property (retain) NSString * BudgetUnit;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) emp_tns1_ArrayOfGPExecutionPlanActivityAppraisal * GPExecutionPlanActivityAppraisals;
@property (retain) NSNumber * GPExecutionPlanActivityId;
@property (retain) emp_tns1_GPExecutionPlanDetail * GPExecutionPlanDetail;
@property (retain) NSNumber * GPExecutionPlanDetailId;
@property (retain) emp_tns1_ArrayOfGPPeriodicReport * GPPeriodicReports;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Measure;
@property (retain) NSString * MeasureUnit;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * StartDate;
@property (retain) NSString * Status;
@property (retain) NSNumber * Target;
@property (retain) NSString * TargetUnit;
@property (retain) NSNumber * Variance;
@property (retain) NSString * VarianceUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfGPExecutionPlanActivity : NSObject {
	
/* elements */
	NSMutableArray *GPExecutionPlanActivity;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfGPExecutionPlanActivity *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPExecutionPlanActivity:(emp_tns1_GPExecutionPlanActivity *)toAdd;
@property (readonly) NSMutableArray * GPExecutionPlanActivity;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_GPExecutionPlanDetail : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_GPExecutionPlan * GPExecutionPlan;
	emp_tns1_ArrayOfGPExecutionPlanActivity * GPExecutionPlanActivities;
	NSNumber * GPExecutionPlanDetailId;
	NSNumber * GPExecutionPlanId;
	emp_tns1_GPIndividualYearlyObjective * GPIndividualYearlyObjective;
	NSNumber * GPIndividualYearlyObjectiveId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_GPExecutionPlanDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_GPExecutionPlan * GPExecutionPlan;
@property (retain) emp_tns1_ArrayOfGPExecutionPlanActivity * GPExecutionPlanActivities;
@property (retain) NSNumber * GPExecutionPlanDetailId;
@property (retain) NSNumber * GPExecutionPlanId;
@property (retain) emp_tns1_GPIndividualYearlyObjective * GPIndividualYearlyObjective;
@property (retain) NSNumber * GPIndividualYearlyObjectiveId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfGPExecutionPlanDetail : NSObject {
	
/* elements */
	NSMutableArray *GPExecutionPlanDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfGPExecutionPlanDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPExecutionPlanDetail:(emp_tns1_GPExecutionPlanDetail *)toAdd;
@property (readonly) NSMutableArray * GPExecutionPlanDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_GPIndividualYearlyObjectiveAppraisal : NSObject {
	
/* elements */
	NSNumber * AppraisalTime;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_GPIndividualYearlyObjective * GPIndividualYearlyObjective;
	NSNumber * GPIndividualYearlyObjectiveAppraisalId;
	NSNumber * GPIndividualYearlyObjectiveId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * Score;
	NSString * SelfComment;
	NSNumber * SelfRating;
	NSNumber * SelfScore;
	NSNumber * Status;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_GPIndividualYearlyObjectiveAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AppraisalTime;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_GPIndividualYearlyObjective * GPIndividualYearlyObjective;
@property (retain) NSNumber * GPIndividualYearlyObjectiveAppraisalId;
@property (retain) NSNumber * GPIndividualYearlyObjectiveId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * SelfScore;
@property (retain) NSNumber * Status;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfGPIndividualYearlyObjectiveAppraisal : NSObject {
	
/* elements */
	NSMutableArray *GPIndividualYearlyObjectiveAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfGPIndividualYearlyObjectiveAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPIndividualYearlyObjectiveAppraisal:(emp_tns1_GPIndividualYearlyObjectiveAppraisal *)toAdd;
@property (readonly) NSMutableArray * GPIndividualYearlyObjectiveAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_GPPerspectiveValueDetail : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_GPPerspectiveValue * GPPerspectiveValue;
	emp_tns1_GPPerspectiveValue * GPPerspectiveValue1;
	NSNumber * GPPerspectiveValueDetailId;
	NSNumber * GPStrategyMapObjectId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * PerspectiveValueDestinationId;
	NSNumber * PerspectiveValueSourceId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_GPPerspectiveValueDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_GPPerspectiveValue * GPPerspectiveValue;
@property (retain) emp_tns1_GPPerspectiveValue * GPPerspectiveValue1;
@property (retain) NSNumber * GPPerspectiveValueDetailId;
@property (retain) NSNumber * GPStrategyMapObjectId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * PerspectiveValueDestinationId;
@property (retain) NSNumber * PerspectiveValueSourceId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfGPPerspectiveValueDetail : NSObject {
	
/* elements */
	NSMutableArray *GPPerspectiveValueDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfGPPerspectiveValueDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPPerspectiveValueDetail:(emp_tns1_GPPerspectiveValueDetail *)toAdd;
@property (readonly) NSMutableArray * GPPerspectiveValueDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_GPPerspectiveValue : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	emp_tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	emp_tns1_ArrayOfGPPerspectiveValueDetail * GPPerspectiveValueDetails;
	emp_tns1_ArrayOfGPPerspectiveValueDetail * GPPerspectiveValueDetails1;
	NSNumber * GPPerspectiveValueId;
	emp_tns1_GPStrategyMapObject * GPStrategyMapObject;
	NSNumber * GPStrategyMapObjectId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * PositionX;
	NSNumber * PositionY;
	NSString * ValueType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_GPPerspectiveValue *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) emp_tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) emp_tns1_ArrayOfGPPerspectiveValueDetail * GPPerspectiveValueDetails;
@property (retain) emp_tns1_ArrayOfGPPerspectiveValueDetail * GPPerspectiveValueDetails1;
@property (retain) NSNumber * GPPerspectiveValueId;
@property (retain) emp_tns1_GPStrategyMapObject * GPStrategyMapObject;
@property (retain) NSNumber * GPStrategyMapObjectId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * PositionX;
@property (retain) NSNumber * PositionY;
@property (retain) NSString * ValueType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfGPPerspectiveValue : NSObject {
	
/* elements */
	NSMutableArray *GPPerspectiveValue;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfGPPerspectiveValue *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPPerspectiveValue:(emp_tns1_GPPerspectiveValue *)toAdd;
@property (readonly) NSMutableArray * GPPerspectiveValue;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfGPCompanyYearlyObjective : NSObject {
	
/* elements */
	NSMutableArray *GPCompanyYearlyObjective;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfGPCompanyYearlyObjective *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPCompanyYearlyObjective:(emp_tns1_GPCompanyYearlyObjective *)toAdd;
@property (readonly) NSMutableArray * GPCompanyYearlyObjective;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfGPStrategyMapObject : NSObject {
	
/* elements */
	NSMutableArray *GPStrategyMapObject;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfGPStrategyMapObject *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPStrategyMapObject:(emp_tns1_GPStrategyMapObject *)toAdd;
@property (readonly) NSMutableArray * GPStrategyMapObject;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_GPStrategy : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EndYear;
	emp_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
	NSNumber * GPStrategyId;
	emp_tns1_ArrayOfGPStrategyMapObject * GPStrategyMapObjects;
	USBoolean * IsActived;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * StartYear;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_GPStrategy *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EndYear;
@property (retain) emp_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
@property (retain) NSNumber * GPStrategyId;
@property (retain) emp_tns1_ArrayOfGPStrategyMapObject * GPStrategyMapObjects;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * StartYear;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_GPStrategyMapObject : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	emp_tns1_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails;
	emp_tns1_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
	emp_tns1_ArrayOfGPPerspectiveValue * GPPerspectiveValues;
	emp_tns1_GPStrategy * GPStrategy;
	NSNumber * GPStrategyId;
	NSNumber * GPStrategyMapObjectId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_GPStrategyMapObject *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) emp_tns1_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails;
@property (retain) emp_tns1_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
@property (retain) emp_tns1_ArrayOfGPPerspectiveValue * GPPerspectiveValues;
@property (retain) emp_tns1_GPStrategy * GPStrategy;
@property (retain) NSNumber * GPStrategyId;
@property (retain) NSNumber * GPStrategyMapObjectId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_GPCompanyStrategicGoalDetail : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_GPCompanyStrategicGoal * GPCompanyStrategicGoal;
	emp_tns1_GPCompanyStrategicGoal * GPCompanyStrategicGoal1;
	NSNumber * GPCompanyStrategicGoalDestinationId;
	NSNumber * GPCompanyStrategicGoalDetailId;
	NSNumber * GPCompanyStrategicGoalSourceId;
	emp_tns1_GPStrategyMapObject * GPStrategyMapObject;
	NSNumber * GPStrategyMapObjectId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_GPCompanyStrategicGoalDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_GPCompanyStrategicGoal * GPCompanyStrategicGoal;
@property (retain) emp_tns1_GPCompanyStrategicGoal * GPCompanyStrategicGoal1;
@property (retain) NSNumber * GPCompanyStrategicGoalDestinationId;
@property (retain) NSNumber * GPCompanyStrategicGoalDetailId;
@property (retain) NSNumber * GPCompanyStrategicGoalSourceId;
@property (retain) emp_tns1_GPStrategyMapObject * GPStrategyMapObject;
@property (retain) NSNumber * GPStrategyMapObjectId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfGPCompanyStrategicGoalDetail : NSObject {
	
/* elements */
	NSMutableArray *GPCompanyStrategicGoalDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfGPCompanyStrategicGoalDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPCompanyStrategicGoalDetail:(emp_tns1_GPCompanyStrategicGoalDetail *)toAdd;
@property (readonly) NSMutableArray * GPCompanyStrategicGoalDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_GPCompanyStrategicGoal : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_Employee * Employee;
	emp_tns1_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails;
	emp_tns1_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails1;
	NSNumber * GPCompanyStrategicGoalId;
	emp_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
	emp_tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	NSNumber * GPStrategyId;
	emp_tns1_GPStrategyMapObject * GPStrategyMapObject;
	NSNumber * GPStrategyMapObjectId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OwnerId;
	NSString * PeriodStrategy;
	NSString * SmartAttainableGoal;
	NSString * SmartMeasureGoal;
	NSString * SmartRelevantGoal;
	NSString * SmartSpecificGoal;
	NSString * SmartTimeGoal;
	NSString * Statement;
	NSNumber * YearBegin;
	NSNumber * YearEnd;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_GPCompanyStrategicGoal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) emp_tns1_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails;
@property (retain) emp_tns1_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails1;
@property (retain) NSNumber * GPCompanyStrategicGoalId;
@property (retain) emp_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
@property (retain) emp_tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) NSNumber * GPStrategyId;
@property (retain) emp_tns1_GPStrategyMapObject * GPStrategyMapObject;
@property (retain) NSNumber * GPStrategyMapObjectId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OwnerId;
@property (retain) NSString * PeriodStrategy;
@property (retain) NSString * SmartAttainableGoal;
@property (retain) NSString * SmartMeasureGoal;
@property (retain) NSString * SmartRelevantGoal;
@property (retain) NSString * SmartSpecificGoal;
@property (retain) NSString * SmartTimeGoal;
@property (retain) NSString * Statement;
@property (retain) NSNumber * YearBegin;
@property (retain) NSNumber * YearEnd;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfGPCompanyStrategicGoal : NSObject {
	
/* elements */
	NSMutableArray *GPCompanyStrategicGoal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfGPCompanyStrategicGoal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPCompanyStrategicGoal:(emp_tns1_GPCompanyStrategicGoal *)toAdd;
@property (readonly) NSMutableArray * GPCompanyStrategicGoal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_GPObjectiveInitiative : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_Employee * Employee;
	emp_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
	NSNumber * GPObjectiveInitiativeId;
	emp_tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	USBoolean * IsDeleted;
	emp_tns1_ArrayOfLMSCourse * LMSCourses;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * OwnerId;
	NSNumber * Progress;
	NSString * Purpose;
	NSNumber * YearBegin;
	NSNumber * YearEnd;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_GPObjectiveInitiative *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) emp_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
@property (retain) NSNumber * GPObjectiveInitiativeId;
@property (retain) emp_tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) USBoolean * IsDeleted;
@property (retain) emp_tns1_ArrayOfLMSCourse * LMSCourses;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * OwnerId;
@property (retain) NSNumber * Progress;
@property (retain) NSString * Purpose;
@property (retain) NSNumber * YearBegin;
@property (retain) NSNumber * YearEnd;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfGPObjectiveInitiative : NSObject {
	
/* elements */
	NSMutableArray *GPObjectiveInitiative;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfGPObjectiveInitiative *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPObjectiveInitiative:(emp_tns1_GPObjectiveInitiative *)toAdd;
@property (readonly) NSMutableArray * GPObjectiveInitiative;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_GPPerspectiveMeasure : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
	emp_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
	emp_tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	NSNumber * GPPerspectiveMeasureId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_GPPerspectiveMeasure *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
@property (retain) emp_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
@property (retain) emp_tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) NSNumber * GPPerspectiveMeasureId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfGPPerspectiveMeasure : NSObject {
	
/* elements */
	NSMutableArray *GPPerspectiveMeasure;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfGPPerspectiveMeasure *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPPerspectiveMeasure:(emp_tns1_GPPerspectiveMeasure *)toAdd;
@property (readonly) NSMutableArray * GPPerspectiveMeasure;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_GPPerspective : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	emp_tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
	emp_tns1_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
	emp_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
	emp_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
	emp_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
	emp_tns1_ArrayOfGPObjectiveInitiative * GPObjectiveInitiatives;
	NSNumber * GPPerspectiveId;
	emp_tns1_ArrayOfGPPerspectiveMeasure * GPPerspectiveMeasures;
	emp_tns1_ArrayOfGPPerspectiveValue * GPPerspectiveValues;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_GPPerspective *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) emp_tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
@property (retain) emp_tns1_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
@property (retain) emp_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
@property (retain) emp_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
@property (retain) emp_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
@property (retain) emp_tns1_ArrayOfGPObjectiveInitiative * GPObjectiveInitiatives;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) emp_tns1_ArrayOfGPPerspectiveMeasure * GPPerspectiveMeasures;
@property (retain) emp_tns1_ArrayOfGPPerspectiveValue * GPPerspectiveValues;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_GPIndividualYearlyObjective : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_Employee * Employee;
	emp_tns1_Employee * Employee1;
	NSNumber * EmployeeId;
	emp_tns1_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
	NSNumber * GPCompanyYearlyObjectiveId;
	emp_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
	NSNumber * GPEmployeeScoreCardId;
	emp_tns1_ArrayOfGPExecutionPlanDetail * GPExecutionPlanDetails;
	emp_tns1_ArrayOfGPIndividualYearlyObjectiveAppraisal * GPIndividualYearlyObjectiveAppraisals;
	NSNumber * GPIndividualYearlyObjectiveId;
	emp_tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * ObjectiveType;
	NSNumber * Owner;
	NSString * SmartAttainableGoal;
	NSString * SmartMeasureGoal;
	NSString * SmartRelevantGoal;
	NSString * SmartSpecificGoal;
	NSString * SmartTimeGoal;
	NSString * Statement;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_GPIndividualYearlyObjective *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) emp_tns1_Employee * Employee1;
@property (retain) NSNumber * EmployeeId;
@property (retain) emp_tns1_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
@property (retain) NSNumber * GPCompanyYearlyObjectiveId;
@property (retain) emp_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
@property (retain) NSNumber * GPEmployeeScoreCardId;
@property (retain) emp_tns1_ArrayOfGPExecutionPlanDetail * GPExecutionPlanDetails;
@property (retain) emp_tns1_ArrayOfGPIndividualYearlyObjectiveAppraisal * GPIndividualYearlyObjectiveAppraisals;
@property (retain) NSNumber * GPIndividualYearlyObjectiveId;
@property (retain) emp_tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * ObjectiveType;
@property (retain) NSNumber * Owner;
@property (retain) NSString * SmartAttainableGoal;
@property (retain) NSString * SmartMeasureGoal;
@property (retain) NSString * SmartRelevantGoal;
@property (retain) NSString * SmartSpecificGoal;
@property (retain) NSString * SmartTimeGoal;
@property (retain) NSString * Statement;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfGPIndividualYearlyObjective : NSObject {
	
/* elements */
	NSMutableArray *GPIndividualYearlyObjective;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfGPIndividualYearlyObjective *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPIndividualYearlyObjective:(emp_tns1_GPIndividualYearlyObjective *)toAdd;
@property (readonly) NSMutableArray * GPIndividualYearlyObjective;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_GPPerformanceExecutionReport : NSObject {
	
/* elements */
	NSString * ActivityName;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
	NSNumber * GPEmployeeScoreCardId;
	NSNumber * GPPerformanceExecutionReportId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Progress;
	NSString * ReportNumber;
	NSDate * ReportTime;
	NSNumber * Reporter;
	NSNumber * Status;
	NSNumber * Target;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_GPPerformanceExecutionReport *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ActivityName;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
@property (retain) NSNumber * GPEmployeeScoreCardId;
@property (retain) NSNumber * GPPerformanceExecutionReportId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Progress;
@property (retain) NSString * ReportNumber;
@property (retain) NSDate * ReportTime;
@property (retain) NSNumber * Reporter;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * Target;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_GPEmployeeScoreCard : NSObject {
	
/* elements */
	NSNumber * Actual;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	emp_tns1_GPCompanyScoreCard * GPCompanyScoreCard;
	NSNumber * GPCompanyScoreCardId;
	emp_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCard1;
	emp_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard2;
	emp_tns1_ArrayOfGPEmployeeScoreCardAppraisal * GPEmployeeScoreCardAppraisals;
	NSNumber * GPEmployeeScoreCardId;
	emp_tns1_ArrayOfGPEmployeeScoreCardProgress * GPEmployeeScoreCardProgresses;
	emp_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
	emp_tns1_GPPerformanceExecutionReport * GPPerformanceExecutionReport;
	emp_tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	emp_tns1_GPPerspectiveMeasure * GPPerspectiveMeasure;
	NSNumber * GPPerspectiveMeasureId;
	USBoolean * IsDeleted;
	NSString * Metric;
	NSDate * ModifiedDate;
	NSNumber * ParentId;
	NSNumber * PercentageOwnership;
	NSNumber * Progress;
	NSString * ScoreCardGroup;
	NSString * ScoreCardSubGroup;
	NSString * Statement;
	NSNumber * Status;
	NSNumber * Target;
	NSNumber * Weight;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_GPEmployeeScoreCard *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Actual;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) emp_tns1_GPCompanyScoreCard * GPCompanyScoreCard;
@property (retain) NSNumber * GPCompanyScoreCardId;
@property (retain) emp_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCard1;
@property (retain) emp_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard2;
@property (retain) emp_tns1_ArrayOfGPEmployeeScoreCardAppraisal * GPEmployeeScoreCardAppraisals;
@property (retain) NSNumber * GPEmployeeScoreCardId;
@property (retain) emp_tns1_ArrayOfGPEmployeeScoreCardProgress * GPEmployeeScoreCardProgresses;
@property (retain) emp_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
@property (retain) emp_tns1_GPPerformanceExecutionReport * GPPerformanceExecutionReport;
@property (retain) emp_tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) emp_tns1_GPPerspectiveMeasure * GPPerspectiveMeasure;
@property (retain) NSNumber * GPPerspectiveMeasureId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Metric;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * ParentId;
@property (retain) NSNumber * PercentageOwnership;
@property (retain) NSNumber * Progress;
@property (retain) NSString * ScoreCardGroup;
@property (retain) NSString * ScoreCardSubGroup;
@property (retain) NSString * Statement;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * Target;
@property (retain) NSNumber * Weight;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfGPEmployeeScoreCard : NSObject {
	
/* elements */
	NSMutableArray *GPEmployeeScoreCard;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfGPEmployeeScoreCard *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPEmployeeScoreCard:(emp_tns1_GPEmployeeScoreCard *)toAdd;
@property (readonly) NSMutableArray * GPEmployeeScoreCard;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_GPCompanyScoreCard : NSObject {
	
/* elements */
	NSNumber * Actual;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_Employee * Employee;
	NSNumber * GPCompanyScoreCardId;
	emp_tns1_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
	NSNumber * GPCompanyYearlyObjectiveId;
	emp_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
	emp_tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	emp_tns1_GPPerspectiveMeasure * GPPerspectiveMeasure;
	NSNumber * GPPerspectiveMeasureId;
	USBoolean * IsDeleted;
	NSString * Metric;
	NSDate * ModifiedDate;
	NSNumber * Progress;
	NSString * ReportingRythm;
	NSNumber * ScoreCardCreator;
	NSString * Statement;
	NSNumber * Status;
	NSNumber * Target;
	NSNumber * Weight;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_GPCompanyScoreCard *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Actual;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * GPCompanyScoreCardId;
@property (retain) emp_tns1_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
@property (retain) NSNumber * GPCompanyYearlyObjectiveId;
@property (retain) emp_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
@property (retain) emp_tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) emp_tns1_GPPerspectiveMeasure * GPPerspectiveMeasure;
@property (retain) NSNumber * GPPerspectiveMeasureId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Metric;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Progress;
@property (retain) NSString * ReportingRythm;
@property (retain) NSNumber * ScoreCardCreator;
@property (retain) NSString * Statement;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * Target;
@property (retain) NSNumber * Weight;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfGPCompanyScoreCard : NSObject {
	
/* elements */
	NSMutableArray *GPCompanyScoreCard;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfGPCompanyScoreCard *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPCompanyScoreCard:(emp_tns1_GPCompanyScoreCard *)toAdd;
@property (readonly) NSMutableArray * GPCompanyScoreCard;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_GPCompanyYearlyObjective : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_Employee * Employee;
	emp_tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
	emp_tns1_GPCompanyStrategicGoal * GPCompanyStrategicGoal;
	NSNumber * GPCompanyStrategicGoalId;
	NSNumber * GPCompanyYearlyObjectiveId;
	emp_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
	emp_tns1_GPObjectiveInitiative * GPObjectiveInitiative;
	NSNumber * GPObjectiveInitiativeId;
	emp_tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	emp_tns1_GPStrategy * GPStrategy;
	NSNumber * GPStrategyId;
	USBoolean * IsDeleted;
	emp_tns1_ArrayOfLMSCourse * LMSCourses;
	NSDate * ModifiedDate;
	NSNumber * Owner;
	NSString * SmartAttainableGoal;
	NSString * SmartMeasureGoal;
	NSString * SmartRelevantGoal;
	NSString * SmartSpecificGoal;
	NSString * SmartTimeGoal;
	NSString * Statement;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_GPCompanyYearlyObjective *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) emp_tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
@property (retain) emp_tns1_GPCompanyStrategicGoal * GPCompanyStrategicGoal;
@property (retain) NSNumber * GPCompanyStrategicGoalId;
@property (retain) NSNumber * GPCompanyYearlyObjectiveId;
@property (retain) emp_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
@property (retain) emp_tns1_GPObjectiveInitiative * GPObjectiveInitiative;
@property (retain) NSNumber * GPObjectiveInitiativeId;
@property (retain) emp_tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) emp_tns1_GPStrategy * GPStrategy;
@property (retain) NSNumber * GPStrategyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) emp_tns1_ArrayOfLMSCourse * LMSCourses;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Owner;
@property (retain) NSString * SmartAttainableGoal;
@property (retain) NSString * SmartMeasureGoal;
@property (retain) NSString * SmartRelevantGoal;
@property (retain) NSString * SmartSpecificGoal;
@property (retain) NSString * SmartTimeGoal;
@property (retain) NSString * Statement;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_LMSTopicCompetency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	emp_tns1_LMSContentTopic * LMSContentTopic;
	NSNumber * LMSContentTopicId;
	NSNumber * LMSTopicCompetencyId;
	NSDate * ModifiedDate;
	emp_tns1_OrgCompetency * OrgCompetency;
	NSNumber * OrgCompetencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_LMSTopicCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) emp_tns1_LMSContentTopic * LMSContentTopic;
@property (retain) NSNumber * LMSContentTopicId;
@property (retain) NSNumber * LMSTopicCompetencyId;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_OrgCompetency * OrgCompetency;
@property (retain) NSNumber * OrgCompetencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfLMSTopicCompetency : NSObject {
	
/* elements */
	NSMutableArray *LMSTopicCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfLMSTopicCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSTopicCompetency:(emp_tns1_LMSTopicCompetency *)toAdd;
@property (readonly) NSMutableArray * LMSTopicCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfLMSContentTopic : NSObject {
	
/* elements */
	NSMutableArray *LMSContentTopic;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfLMSContentTopic *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSContentTopic:(emp_tns1_LMSContentTopic *)toAdd;
@property (readonly) NSMutableArray * LMSContentTopic;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_LMSTopicGroup : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	emp_tns1_ArrayOfLMSContentTopic * LMSContentTopics;
	NSNumber * LMSTopicGroupId;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_LMSTopicGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) emp_tns1_ArrayOfLMSContentTopic * LMSContentTopics;
@property (retain) NSNumber * LMSTopicGroupId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_LMSContentTopic : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * LMSContentTopicId;
	emp_tns1_ArrayOfLMSCourse * LMSCourses;
	emp_tns1_ArrayOfLMSTopicCompetency * LMSTopicCompetencies;
	emp_tns1_LMSTopicGroup * LMSTopicGroup;
	NSNumber * LMSTopicGroupId;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_LMSContentTopic *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * LMSContentTopicId;
@property (retain) emp_tns1_ArrayOfLMSCourse * LMSCourses;
@property (retain) emp_tns1_ArrayOfLMSTopicCompetency * LMSTopicCompetencies;
@property (retain) emp_tns1_LMSTopicGroup * LMSTopicGroup;
@property (retain) NSNumber * LMSTopicGroupId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_LMSCourseAttendee : NSObject {
	
/* elements */
	emp_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsApproved;
	USBoolean * IsDeleted;
	USBoolean * IsPassed;
	emp_tns1_LMSCourse * LMSCourse;
	NSNumber * LMSCourseAttendeeId;
	NSNumber * LMSCourseId;
	NSNumber * LMSDevelopmentPlanId;
	NSDate * ModifiedDate;
	NSNumber * NumberOfWarning;
	NSNumber * OverallScore;
	NSNumber * ParticipationStatus;
	NSNumber * ProgressStatus;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_LMSCourseAttendee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsApproved;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsPassed;
@property (retain) emp_tns1_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseAttendeeId;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSNumber * LMSDevelopmentPlanId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NumberOfWarning;
@property (retain) NSNumber * OverallScore;
@property (retain) NSNumber * ParticipationStatus;
@property (retain) NSNumber * ProgressStatus;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfLMSCourseAttendee : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseAttendee;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfLMSCourseAttendee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseAttendee:(emp_tns1_LMSCourseAttendee *)toAdd;
@property (readonly) NSMutableArray * LMSCourseAttendee;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogLMSContentType : NSObject {
	
/* elements */
	NSNumber * CLogLMSContentTypeId;
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	emp_tns1_ArrayOfLMSCourseContent * LMSCourseContents;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogLMSContentType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogLMSContentTypeId;
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) emp_tns1_ArrayOfLMSCourseContent * LMSCourseContents;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_LMSCourseContent : NSObject {
	
/* elements */
	emp_tns1_CLogLMSContentType * CLogLMSContentType;
	NSNumber * CLogLMSContentType_CLogLMSContentTypeId;
	emp_tns1_LMSCourse * LMSCourse;
	emp_tns1_LMSCourseContent * LMSCourseContent1;
	NSNumber * LMSCourseContent1_LMSCourseContentId;
	NSNumber * LMSCourseContentId;
	emp_tns1_LMSCourseContent * LMSCourseContents1;
	NSNumber * LMSCourse_LMSCourseId;
	NSString * Name;
	NSString * Statement;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_LMSCourseContent *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogLMSContentType * CLogLMSContentType;
@property (retain) NSNumber * CLogLMSContentType_CLogLMSContentTypeId;
@property (retain) emp_tns1_LMSCourse * LMSCourse;
@property (retain) emp_tns1_LMSCourseContent * LMSCourseContent1;
@property (retain) NSNumber * LMSCourseContent1_LMSCourseContentId;
@property (retain) NSNumber * LMSCourseContentId;
@property (retain) emp_tns1_LMSCourseContent * LMSCourseContents1;
@property (retain) NSNumber * LMSCourse_LMSCourseId;
@property (retain) NSString * Name;
@property (retain) NSString * Statement;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfLMSCourseContent : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseContent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfLMSCourseContent *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseContent:(emp_tns1_LMSCourseContent *)toAdd;
@property (readonly) NSMutableArray * LMSCourseContent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_LMSCourseDetail : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CourseContentTypeId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	emp_tns1_LMSCourse * LMSCourse;
	NSNumber * LMSCourseDetailId;
	NSNumber * LMSCourseId;
	NSNumber * Level;
	NSDate * ModifiedDate;
	NSString * Resource;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_LMSCourseDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CourseContentTypeId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) emp_tns1_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseDetailId;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSNumber * Level;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Resource;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfLMSCourseDetail : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfLMSCourseDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseDetail:(emp_tns1_LMSCourseDetail *)toAdd;
@property (readonly) NSMutableArray * LMSCourseDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfOrgUnit : NSObject {
	
/* elements */
	NSMutableArray *OrgUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfOrgUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgUnit:(emp_tns1_OrgUnit *)toAdd;
@property (readonly) NSMutableArray * OrgUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgUnit : NSObject {
	
/* elements */
	NSString * Address;
	emp_tns1_Address * Address1;
	NSNumber * AddressId;
	NSString * CommissionDesc;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	NSString * FunctionDesc;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	emp_tns1_ArrayOfLMSCourseProgressUnit * LMSCourseProgressUnits;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
	emp_tns1_ArrayOfOrgUnit * OrgUnit1;
	emp_tns1_OrgUnit * OrgUnit2;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSNumber * OrgUnitLevelId;
	NSString * ParentCode;
	NSNumber * ParentId;
	NSNumber * TotalEmployees;
	NSString * WorkLocation;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) emp_tns1_Address * Address1;
@property (retain) NSNumber * AddressId;
@property (retain) NSString * CommissionDesc;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) NSString * FunctionDesc;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) emp_tns1_ArrayOfLMSCourseProgressUnit * LMSCourseProgressUnits;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) emp_tns1_ArrayOfOrgUnit * OrgUnit1;
@property (retain) emp_tns1_OrgUnit * OrgUnit2;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * OrgUnitLevelId;
@property (retain) NSString * ParentCode;
@property (retain) NSNumber * ParentId;
@property (retain) NSNumber * TotalEmployees;
@property (retain) NSString * WorkLocation;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_LMSCourseProgressUnit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	emp_tns1_LMSCourse * LMSCourse;
	NSNumber * LMSCourseId;
	NSNumber * LMSCourseProgressUnitId;
	NSDate * ModifiedDate;
	NSNumber * NumOfEmpFinish;
	emp_tns1_OrgUnit * OrgUnit;
	NSNumber * OrgUnitId;
	USBoolean * Status;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_LMSCourseProgressUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) emp_tns1_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSNumber * LMSCourseProgressUnitId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NumOfEmpFinish;
@property (retain) emp_tns1_OrgUnit * OrgUnit;
@property (retain) NSNumber * OrgUnitId;
@property (retain) USBoolean * Status;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfLMSCourseProgressUnit : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseProgressUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfLMSCourseProgressUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseProgressUnit:(emp_tns1_LMSCourseProgressUnit *)toAdd;
@property (readonly) NSMutableArray * LMSCourseProgressUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_LMSCourseRequiredCompetency : NSObject {
	
/* elements */
	emp_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompetencyName;
	NSString * CompetencyType;
	NSNumber * CourseRequiredCompetencyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	emp_tns1_LMSCourse * LMSCourse;
	NSNumber * LMSCourseId;
	NSDate * ModifiedDate;
	emp_tns1_OrgCompetency * OrgCompetency;
	NSNumber * OrgCompetencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_LMSCourseRequiredCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompetencyName;
@property (retain) NSString * CompetencyType;
@property (retain) NSNumber * CourseRequiredCompetencyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) emp_tns1_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_OrgCompetency * OrgCompetency;
@property (retain) NSNumber * OrgCompetencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfLMSCourseRequiredCompetency : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseRequiredCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfLMSCourseRequiredCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseRequiredCompetency:(emp_tns1_LMSCourseRequiredCompetency *)toAdd;
@property (readonly) NSMutableArray * LMSCourseRequiredCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_LMSCourseTranscript : NSObject {
	
/* elements */
	emp_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSString * Comment;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	emp_tns1_Employee * Employee;
	emp_tns1_Employee * Employee1;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsPass;
	emp_tns1_LMSCourse * LMSCourse;
	NSNumber * LMSCourseId;
	NSNumber * LMSCourseTranscriptId;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_LMSCourseTranscript *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) emp_tns1_Employee * Employee1;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsPass;
@property (retain) emp_tns1_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSNumber * LMSCourseTranscriptId;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfLMSCourseTranscript : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseTranscript;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfLMSCourseTranscript *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseTranscript:(emp_tns1_LMSCourseTranscript *)toAdd;
@property (readonly) NSMutableArray * LMSCourseTranscript;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_LMSCourseType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * LMSCourseTypeId;
	NSString * LMSCourseTypeName;
	emp_tns1_ArrayOfLMSCourse * LMSCourses;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_LMSCourseType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * LMSCourseTypeId;
@property (retain) NSString * LMSCourseTypeName;
@property (retain) emp_tns1_ArrayOfLMSCourse * LMSCourses;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_LMSCourse : NSObject {
	
/* elements */
	NSNumber * ApprovedBy;
	emp_tns1_CLogCourseStatu * CLogCourseStatu;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSString * CourseIcon;
	NSNumber * CourseStatusId;
	NSNumber * CourseTypeId;
	NSDate * CreatedDate;
	NSDate * EndDate;
	emp_tns1_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
	emp_tns1_GPObjectiveInitiative * GPObjectiveInitiative;
	USBoolean * IsApproved;
	USBoolean * IsDeleted;
	USBoolean * IsRequired;
	NSString * Keyword;
	emp_tns1_LMSContentTopic * LMSContentTopic;
	emp_tns1_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
	NSString * LMSCourseCode;
	emp_tns1_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
	emp_tns1_ArrayOfLMSCourseContent * LMSCourseContents;
	emp_tns1_ArrayOfLMSCourseDetail * LMSCourseDetails;
	NSNumber * LMSCourseId;
	emp_tns1_ArrayOfLMSCourseProgressUnit * LMSCourseProgressUnits;
	emp_tns1_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
	emp_tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
	emp_tns1_LMSCourseType * LMSCourseType;
	NSNumber * LMSInitiativeObjectiveId;
	NSNumber * LMSTopicGroupId;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
	NSNumber * Requestedby;
	NSDate * StartDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_LMSCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApprovedBy;
@property (retain) emp_tns1_CLogCourseStatu * CLogCourseStatu;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSString * CourseIcon;
@property (retain) NSNumber * CourseStatusId;
@property (retain) NSNumber * CourseTypeId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) emp_tns1_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
@property (retain) emp_tns1_GPObjectiveInitiative * GPObjectiveInitiative;
@property (retain) USBoolean * IsApproved;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsRequired;
@property (retain) NSString * Keyword;
@property (retain) emp_tns1_LMSContentTopic * LMSContentTopic;
@property (retain) emp_tns1_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
@property (retain) NSString * LMSCourseCode;
@property (retain) emp_tns1_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
@property (retain) emp_tns1_ArrayOfLMSCourseContent * LMSCourseContents;
@property (retain) emp_tns1_ArrayOfLMSCourseDetail * LMSCourseDetails;
@property (retain) NSNumber * LMSCourseId;
@property (retain) emp_tns1_ArrayOfLMSCourseProgressUnit * LMSCourseProgressUnits;
@property (retain) emp_tns1_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
@property (retain) emp_tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
@property (retain) emp_tns1_LMSCourseType * LMSCourseType;
@property (retain) NSNumber * LMSInitiativeObjectiveId;
@property (retain) NSNumber * LMSTopicGroupId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) NSNumber * Requestedby;
@property (retain) NSDate * StartDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_LMSCourseCompetency : NSObject {
	
/* elements */
	emp_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompetencyName;
	NSNumber * CompetencyTypeId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	emp_tns1_LMSCourse * LMSCourse;
	NSNumber * LMSCourseCompetencyId;
	NSNumber * LMSCourseId;
	NSDate * ModifiedDate;
	emp_tns1_OrgCompetency * OrgCompetency;
	NSNumber * OrgCompetencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_LMSCourseCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompetencyName;
@property (retain) NSNumber * CompetencyTypeId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) emp_tns1_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseCompetencyId;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_OrgCompetency * OrgCompetency;
@property (retain) NSNumber * OrgCompetencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfLMSCourseCompetency : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfLMSCourseCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseCompetency:(emp_tns1_LMSCourseCompetency *)toAdd;
@property (readonly) NSMutableArray * LMSCourseCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfOrgCompetency : NSObject {
	
/* elements */
	NSMutableArray *OrgCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfOrgCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgCompetency:(emp_tns1_OrgCompetency *)toAdd;
@property (readonly) NSMutableArray * OrgCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgCompetencyGroup : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * GroupName;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_ArrayOfOrgCompetency * OrgCompetencies;
	NSNumber * OrgCompetencyGroupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgCompetencyGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * GroupName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_ArrayOfOrgCompetency * OrgCompetencies;
@property (retain) NSNumber * OrgCompetencyGroupId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * DecisionFile;
	NSString * DecisionName;
	NSString * DecisionNo;
	NSString * DirectReportToEmployeeCode;
	NSNumber * DirectReportToEmployeeId;
	NSString * EmpJobPositionCode;
	NSNumber * EmpProfileJobPositionId;
	emp_tns1_Employee * Employee;
	emp_tns1_Employee * Employee1;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ExpirationDate;
	NSDate * ImplementationDate;
	USBoolean * IsActive;
	USBoolean * IsConcurrentPost;
	USBoolean * IsDeleted;
	USBoolean * IsMainPost;
	USBoolean * IsPromoted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * OrgJobExternalTitleCode;
	emp_tns1_OrgJobPosition * OrgJobPosition;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSNumber * PercentParticipation;
	NSString * Reason;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DecisionFile;
@property (retain) NSString * DecisionName;
@property (retain) NSString * DecisionNo;
@property (retain) NSString * DirectReportToEmployeeCode;
@property (retain) NSNumber * DirectReportToEmployeeId;
@property (retain) NSString * EmpJobPositionCode;
@property (retain) NSNumber * EmpProfileJobPositionId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) emp_tns1_Employee * Employee1;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsConcurrentPost;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainPost;
@property (retain) USBoolean * IsPromoted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * OrgJobExternalTitleCode;
@property (retain) emp_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * PercentParticipation;
@property (retain) NSString * Reason;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileJobPosition : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileJobPosition:(emp_tns1_EmpProfileJobPosition *)toAdd;
@property (readonly) NSMutableArray * EmpProfileJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgJobPositionRequiredProficency : NSObject {
	
/* elements */
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSNumber * OrgJobPositionRequiredProficencyId;
	NSString * ProficiencyTypeCode;
	NSNumber * RecordId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgJobPositionRequiredProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * OrgJobPositionRequiredProficencyId;
@property (retain) NSString * ProficiencyTypeCode;
@property (retain) NSNumber * RecordId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfOrgJobPositionRequiredProficency : NSObject {
	
/* elements */
	NSMutableArray *OrgJobPositionRequiredProficency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfOrgJobPositionRequiredProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgJobPositionRequiredProficency:(emp_tns1_OrgJobPositionRequiredProficency *)toAdd;
@property (readonly) NSMutableArray * OrgJobPositionRequiredProficency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgJobPositionSpecificCompetency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSNumber * OrgJobPositionSpecificCompetencyId;
	emp_tns1_OrgProficencyLevel * OrgProficencyLevel;
	NSNumber * OrgProficencyLevelId;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgJobPositionSpecificCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * OrgJobPositionSpecificCompetencyId;
@property (retain) emp_tns1_OrgProficencyLevel * OrgProficencyLevel;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfOrgJobPositionSpecificCompetency : NSObject {
	
/* elements */
	NSMutableArray *OrgJobPositionSpecificCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfOrgJobPositionSpecificCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgJobPositionSpecificCompetency:(emp_tns1_OrgJobPositionSpecificCompetency *)toAdd;
@property (readonly) NSMutableArray * OrgJobPositionSpecificCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecRecruitmentPhase : NSObject {
	
/* elements */
	NSMutableArray *RecRecruitmentPhase;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecRecruitmentPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRecruitmentPhase:(emp_tns1_RecRecruitmentPhase *)toAdd;
@property (readonly) NSMutableArray * RecRecruitmentPhase;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecPhaseStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSString * PhaseStatusName;
	NSNumber * RecPhaseStatusId;
	emp_tns1_ArrayOfRecRecruitmentPhase * RecRecruitmentPhases;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecPhaseStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * PhaseStatusName;
@property (retain) NSNumber * RecPhaseStatusId;
@property (retain) emp_tns1_ArrayOfRecRecruitmentPhase * RecRecruitmentPhases;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecCanProficencyDetailRating : NSObject {
	
/* elements */
	emp_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * Mark;
	NSDate * ModifiedDate;
	NSString * Note;
	emp_tns1_OrgProficencyDetail * OrgProficencyDetail;
	NSNumber * OrgProficencyDetailId;
	NSNumber * RecCanProficencyDetailRatingId;
	emp_tns1_RecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRating;
	NSNumber * RecCanProficencyLevelDetailRatingId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecCanProficencyDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Mark;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) emp_tns1_OrgProficencyDetail * OrgProficencyDetail;
@property (retain) NSNumber * OrgProficencyDetailId;
@property (retain) NSNumber * RecCanProficencyDetailRatingId;
@property (retain) emp_tns1_RecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRating;
@property (retain) NSNumber * RecCanProficencyLevelDetailRatingId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecCanProficencyDetailRating : NSObject {
	
/* elements */
	NSMutableArray *RecCanProficencyDetailRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecCanProficencyDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCanProficencyDetailRating:(emp_tns1_RecCanProficencyDetailRating *)toAdd;
@property (readonly) NSMutableArray * RecCanProficencyDetailRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgProficencyDetail : NSObject {
	
/* elements */
	emp_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgCompetencyTypeId;
	NSNumber * OrgProficencyDetailId;
	emp_tns1_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
	NSNumber * OrgProficencyLevelDetailId;
	emp_tns1_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgProficencyDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgCompetencyTypeId;
@property (retain) NSNumber * OrgProficencyDetailId;
@property (retain) emp_tns1_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) emp_tns1_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfOrgProficencyDetail : NSObject {
	
/* elements */
	NSMutableArray *OrgProficencyDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfOrgProficencyDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgProficencyDetail:(emp_tns1_OrgProficencyDetail *)toAdd;
@property (readonly) NSMutableArray * OrgProficencyDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfOrgProficencyLevelDetail : NSObject {
	
/* elements */
	NSMutableArray *OrgProficencyLevelDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfOrgProficencyLevelDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgProficencyLevelDetail:(emp_tns1_OrgProficencyLevelDetail *)toAdd;
@property (readonly) NSMutableArray * OrgProficencyLevelDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_TrainingPlanProfiency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_OrgProficencyType * OrgProficencyType;
	NSNumber * OrgProficencyTypeId;
	NSNumber * ProficencyId;
	NSNumber * TrainingPlanProfiencyId;
	NSNumber * TrainingPlanRequestId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_TrainingPlanProfiency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_OrgProficencyType * OrgProficencyType;
@property (retain) NSNumber * OrgProficencyTypeId;
@property (retain) NSNumber * ProficencyId;
@property (retain) NSNumber * TrainingPlanProfiencyId;
@property (retain) NSNumber * TrainingPlanRequestId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfTrainingPlanProfiency : NSObject {
	
/* elements */
	NSMutableArray *TrainingPlanProfiency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfTrainingPlanProfiency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingPlanProfiency:(emp_tns1_TrainingPlanProfiency *)toAdd;
@property (readonly) NSMutableArray * TrainingPlanProfiency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgProficencyType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
	NSNumber * OrgProficencyTypeId;
	NSString * ProficencyTypeCode;
	NSString * ProficencyTypeName;
	emp_tns1_ArrayOfTrainingPlanProfiency * TrainingPlanProfiencies;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgProficencyType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
@property (retain) NSNumber * OrgProficencyTypeId;
@property (retain) NSString * ProficencyTypeCode;
@property (retain) NSString * ProficencyTypeName;
@property (retain) emp_tns1_ArrayOfTrainingPlanProfiency * TrainingPlanProfiencies;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgProficencyLevelDetail : NSObject {
	
/* elements */
	emp_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_ArrayOfOrgProficencyDetail * OrgProficencyDetails;
	emp_tns1_OrgProficencyLevel * OrgProficencyLevel;
	NSNumber * OrgProficencyLevelDetailId;
	NSNumber * OrgProficencyLevelId;
	emp_tns1_OrgProficencyType * OrgProficencyType;
	NSNumber * OrgProficencyTypeId;
	emp_tns1_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
	NSString * Statement;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgProficencyLevelDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_ArrayOfOrgProficencyDetail * OrgProficencyDetails;
@property (retain) emp_tns1_OrgProficencyLevel * OrgProficencyLevel;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) emp_tns1_OrgProficencyType * OrgProficencyType;
@property (retain) NSNumber * OrgProficencyTypeId;
@property (retain) emp_tns1_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
@property (retain) NSString * Statement;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecCanProficencyLevelDetailRating : NSObject {
	
/* elements */
	emp_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * Mark;
	NSDate * ModifiedDate;
	NSString * Note;
	emp_tns1_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
	NSNumber * OrgProficencyLevelDetailId;
	emp_tns1_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
	NSNumber * RecCanProficencyLevelDetailRatingId;
	emp_tns1_RecCandidateProficencyLevelRating * RecCandidateProficencyLevelRating;
	NSNumber * RecCandidateProficencyLevelRatingId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecCanProficencyLevelDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Mark;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) emp_tns1_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) emp_tns1_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
@property (retain) NSNumber * RecCanProficencyLevelDetailRatingId;
@property (retain) emp_tns1_RecCandidateProficencyLevelRating * RecCandidateProficencyLevelRating;
@property (retain) NSNumber * RecCandidateProficencyLevelRatingId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecCanProficencyLevelDetailRating : NSObject {
	
/* elements */
	NSMutableArray *RecCanProficencyLevelDetailRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecCanProficencyLevelDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCanProficencyLevelDetailRating:(emp_tns1_RecCanProficencyLevelDetailRating *)toAdd;
@property (readonly) NSMutableArray * RecCanProficencyLevelDetailRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecCandidateProficencyLevelRating : NSObject {
	
/* elements */
	emp_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * Mark;
	NSDate * ModifiedDate;
	NSString * Note;
	emp_tns1_OrgProficencyLevel * OrgProficencyLevel;
	NSNumber * OrgProficencyLevelId;
	emp_tns1_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
	emp_tns1_RecCandidateCompetencyRating * RecCandidateCompetencyRating;
	NSNumber * RecCandidateCompetencyRatingId;
	NSNumber * RecCandidateProficencyLevelRatingId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecCandidateProficencyLevelRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Mark;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) emp_tns1_OrgProficencyLevel * OrgProficencyLevel;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) emp_tns1_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
@property (retain) emp_tns1_RecCandidateCompetencyRating * RecCandidateCompetencyRating;
@property (retain) NSNumber * RecCandidateCompetencyRatingId;
@property (retain) NSNumber * RecCandidateProficencyLevelRatingId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecCandidateProficencyLevelRating : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateProficencyLevelRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecCandidateProficencyLevelRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateProficencyLevelRating:(emp_tns1_RecCandidateProficencyLevelRating *)toAdd;
@property (readonly) NSMutableArray * RecCandidateProficencyLevelRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecCandidateCompetencyRating : NSObject {
	
/* elements */
	emp_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * Mark;
	NSDate * ModifiedDate;
	NSString * Note;
	emp_tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateCompetencyRatingId;
	NSNumber * RecCandidateId;
	emp_tns1_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecCandidateCompetencyRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Mark;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) emp_tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateCompetencyRatingId;
@property (retain) NSNumber * RecCandidateId;
@property (retain) emp_tns1_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecCandidateCompetencyRating : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateCompetencyRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecCandidateCompetencyRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateCompetencyRating:(emp_tns1_RecCandidateCompetencyRating *)toAdd;
@property (readonly) NSMutableArray * RecCandidateCompetencyRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogDegree : NSObject {
	
/* elements */
	NSNumber * CLogDegreeId;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * Note;
	emp_tns1_ArrayOfRecCandidateDegree * RecCandidateDegrees;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogDegreeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) emp_tns1_ArrayOfRecCandidateDegree * RecCandidateDegrees;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecCandidateDegree : NSObject {
	
/* elements */
	emp_tns1_CLogDegree * CLogDegree;
	NSNumber * CLogDegreeId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	USBoolean * IsDeleted;
	USBoolean * IsValId;
	NSDate * ModifiedDate;
	NSString * Other;
	NSString * PlaceIssue;
	emp_tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateDegreeId;
	NSNumber * RecCandidateId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecCandidateDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogDegree * CLogDegree;
@property (retain) NSNumber * CLogDegreeId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) emp_tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateDegreeId;
@property (retain) NSNumber * RecCandidateId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecCandidateDegree : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateDegree;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecCandidateDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateDegree:(emp_tns1_RecCandidateDegree *)toAdd;
@property (readonly) NSMutableArray * RecCandidateDegree;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfOrgExperience : NSObject {
	
/* elements */
	NSMutableArray *OrgExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfOrgExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgExperience:(emp_tns1_OrgExperience *)toAdd;
@property (readonly) NSMutableArray * OrgExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgProjectType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	emp_tns1_ArrayOfOrgExperience * OrgExperiences;
	NSNumber * OrgProjectTypeId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgProjectType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) emp_tns1_ArrayOfOrgExperience * OrgExperiences;
@property (retain) NSNumber * OrgProjectTypeId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgTimeInCharge : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_ArrayOfOrgExperience * OrgExperiences;
	NSNumber * OrgTimeInChargeId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgTimeInCharge *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_ArrayOfOrgExperience * OrgExperiences;
@property (retain) NSNumber * OrgTimeInChargeId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgExperience : NSObject {
	
/* elements */
	NSString * Achievement;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgExperienceId;
	emp_tns1_OrgProjectType * OrgProjectType;
	NSNumber * OrgProjectTypeId;
	emp_tns1_OrgTimeInCharge * OrgTimeInCharge;
	NSNumber * OrgTimeInChargeId;
	emp_tns1_ArrayOfRecCandidateExperience * RecCandidateExperiences;
	NSString * RoleDescription;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Achievement;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgExperienceId;
@property (retain) emp_tns1_OrgProjectType * OrgProjectType;
@property (retain) NSNumber * OrgProjectTypeId;
@property (retain) emp_tns1_OrgTimeInCharge * OrgTimeInCharge;
@property (retain) NSNumber * OrgTimeInChargeId;
@property (retain) emp_tns1_ArrayOfRecCandidateExperience * RecCandidateExperiences;
@property (retain) NSString * RoleDescription;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecCandidateExperience : NSObject {
	
/* elements */
	emp_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompanyName;
	NSDate * CreatedDate;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	USBoolean * IsInternal;
	NSString * JobPosition;
	NSDate * ModifiedDate;
	NSString * NotableAchievements;
	NSString * Note;
	emp_tns1_OrgExperience * OrgExperience;
	NSNumber * OrgExperienceId;
	emp_tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateExperienceId;
	NSNumber * RecCandidateId;
	NSString * Responsibilities;
	NSDate * StartDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecCandidateExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompanyName;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInternal;
@property (retain) NSString * JobPosition;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * NotableAchievements;
@property (retain) NSString * Note;
@property (retain) emp_tns1_OrgExperience * OrgExperience;
@property (retain) NSNumber * OrgExperienceId;
@property (retain) emp_tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateExperienceId;
@property (retain) NSNumber * RecCandidateId;
@property (retain) NSString * Responsibilities;
@property (retain) NSDate * StartDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecCandidateExperience : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecCandidateExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateExperience:(emp_tns1_RecCandidateExperience *)toAdd;
@property (readonly) NSMutableArray * RecCandidateExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CBPITDependent : NSObject {
	
/* elements */
	NSNumber * CBPITDependentId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DependentBeginDate;
	NSString * DependentDemonstrative;
	NSDate * DependentEndDate;
	emp_tns1_EmpProfileFamilyRelationship * EmpProfileFamilyRelationship;
	NSNumber * EmpProfileFamilyRelationshipId;
	USBoolean * IsDeleted;
	USBoolean * IsDependent;
	NSDate * ModifiedDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CBPITDependent *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBPITDependentId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DependentBeginDate;
@property (retain) NSString * DependentDemonstrative;
@property (retain) NSDate * DependentEndDate;
@property (retain) emp_tns1_EmpProfileFamilyRelationship * EmpProfileFamilyRelationship;
@property (retain) NSNumber * EmpProfileFamilyRelationshipId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDependent;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCBPITDependent : NSObject {
	
/* elements */
	NSMutableArray *CBPITDependent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCBPITDependent *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBPITDependent:(emp_tns1_CBPITDependent *)toAdd;
@property (readonly) NSMutableArray * CBPITDependent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpBeneficiary : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpBeneficiaryId;
	NSNumber * EmpFamilyRelationshipId;
	emp_tns1_EmpProfileFamilyRelationship * EmpProfileFamilyRelationship;
	NSDate * ImplementationDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpBeneficiary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpBeneficiaryId;
@property (retain) NSNumber * EmpFamilyRelationshipId;
@property (retain) emp_tns1_EmpProfileFamilyRelationship * EmpProfileFamilyRelationship;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpBeneficiary : NSObject {
	
/* elements */
	NSMutableArray *EmpBeneficiary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpBeneficiary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpBeneficiary:(emp_tns1_EmpBeneficiary *)toAdd;
@property (readonly) NSMutableArray * EmpBeneficiary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileFamilyRelationship : NSObject {
	
/* elements */
	NSString * Address;
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSDate * BirthDay;
	emp_tns1_ArrayOfCBPITDependent * CBPITDependents;
	emp_tns1_CLogFamilyRelationship * CLogFamilyRelationship;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DeathDate;
	emp_tns1_ArrayOfEmpBeneficiary * EmpBeneficiaries;
	NSNumber * EmpProfileFamilyRelationshipId;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * HomeTown;
	USBoolean * IsDeleted;
	USBoolean * IsEmergencyContact;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	USBoolean * Nourish;
	NSString * Occupation;
	NSString * OccupationPlace;
	NSString * PersonFirstName;
	NSString * PersonFullName;
	NSString * PersonLastName;
	NSNumber * PersonSameOccupationId;
	NSString * Phone;
	NSNumber * RelateRowId;
	NSNumber * RelationshipId;
	NSString * RelationshipName;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSDate * BirthDay;
@property (retain) emp_tns1_ArrayOfCBPITDependent * CBPITDependents;
@property (retain) emp_tns1_CLogFamilyRelationship * CLogFamilyRelationship;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DeathDate;
@property (retain) emp_tns1_ArrayOfEmpBeneficiary * EmpBeneficiaries;
@property (retain) NSNumber * EmpProfileFamilyRelationshipId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * HomeTown;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEmergencyContact;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) USBoolean * Nourish;
@property (retain) NSString * Occupation;
@property (retain) NSString * OccupationPlace;
@property (retain) NSString * PersonFirstName;
@property (retain) NSString * PersonFullName;
@property (retain) NSString * PersonLastName;
@property (retain) NSNumber * PersonSameOccupationId;
@property (retain) NSString * Phone;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSNumber * RelationshipId;
@property (retain) NSString * RelationshipName;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileFamilyRelationship : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileFamilyRelationship;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileFamilyRelationship:(emp_tns1_EmpProfileFamilyRelationship *)toAdd;
@property (readonly) NSMutableArray * EmpProfileFamilyRelationship;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogFamilyRelationship : NSObject {
	
/* elements */
	NSNumber * CLogFamilyRelationshipId;
	NSNumber * CompanyId;
	emp_tns1_ArrayOfEmpProfileFamilyRelationship * EmpProfileFamilyRelationships;
	NSString * FamilyRelationshipCode;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
	emp_tns1_ArrayOfRecCandidateFamilyRelationship * RecCandidateFamilyRelationships;
	USBoolean * Sex;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogFamilyRelationshipId;
@property (retain) NSNumber * CompanyId;
@property (retain) emp_tns1_ArrayOfEmpProfileFamilyRelationship * EmpProfileFamilyRelationships;
@property (retain) NSString * FamilyRelationshipCode;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
@property (retain) emp_tns1_ArrayOfRecCandidateFamilyRelationship * RecCandidateFamilyRelationships;
@property (retain) USBoolean * Sex;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecCandidateFamilyRelationship : NSObject {
	
/* elements */
	NSString * Address;
	NSDate * BirthDay;
	emp_tns1_CLogFamilyRelationship * CLogFamilyRelationship;
	NSNumber * CLogFamilyRelationshipId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DeathDate;
	NSString * HomeTown;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	USBoolean * Nourish;
	NSString * Occupation;
	NSString * OccupationPlace;
	NSString * PersonFirstName;
	NSString * PersonFullName;
	NSString * PersonLastName;
	NSNumber * PersonSameOccupationId;
	NSString * Phone;
	emp_tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateFamilyRelationshipId;
	NSNumber * RecCandidateId;
	NSString * RelationshipName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecCandidateFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSDate * BirthDay;
@property (retain) emp_tns1_CLogFamilyRelationship * CLogFamilyRelationship;
@property (retain) NSNumber * CLogFamilyRelationshipId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DeathDate;
@property (retain) NSString * HomeTown;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) USBoolean * Nourish;
@property (retain) NSString * Occupation;
@property (retain) NSString * OccupationPlace;
@property (retain) NSString * PersonFirstName;
@property (retain) NSString * PersonFullName;
@property (retain) NSString * PersonLastName;
@property (retain) NSNumber * PersonSameOccupationId;
@property (retain) NSString * Phone;
@property (retain) emp_tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateFamilyRelationshipId;
@property (retain) NSNumber * RecCandidateId;
@property (retain) NSString * RelationshipName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecCandidateFamilyRelationship : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateFamilyRelationship;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecCandidateFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateFamilyRelationship:(emp_tns1_RecCandidateFamilyRelationship *)toAdd;
@property (readonly) NSMutableArray * RecCandidateFamilyRelationship;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecCandidateForeignLanguage : NSObject {
	
/* elements */
	NSString * AttachmenttUrl;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Degree;
	NSDate * Duration_;
	NSDate * EffectiveDate;
	USBoolean * IsDeleted;
	NSString * Language;
	NSString * LanguageSkill;
	NSDate * ModifiedDate;
	NSString * Note;
	emp_tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateForeignLanguageId;
	NSNumber * RecCandidateId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecCandidateForeignLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AttachmenttUrl;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Degree;
@property (retain) NSDate * Duration_;
@property (retain) NSDate * EffectiveDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Language;
@property (retain) NSString * LanguageSkill;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) emp_tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateForeignLanguageId;
@property (retain) NSNumber * RecCandidateId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecCandidateForeignLanguage : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateForeignLanguage;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecCandidateForeignLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateForeignLanguage:(emp_tns1_RecCandidateForeignLanguage *)toAdd;
@property (readonly) NSMutableArray * RecCandidateForeignLanguage;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileQualification : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	NSNumber * EmpProfileQualificationId;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsValId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * OrgDegreeRankId;
	emp_tns1_OrgQualification * OrgQualification;
	NSNumber * OrgQualificationId;
	NSString * Other;
	NSString * PlaceIssue;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) NSNumber * EmpProfileQualificationId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgDegreeRankId;
@property (retain) emp_tns1_OrgQualification * OrgQualification;
@property (retain) NSNumber * OrgQualificationId;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileQualification : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileQualification;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileQualification:(emp_tns1_EmpProfileQualification *)toAdd;
@property (readonly) NSMutableArray * EmpProfileQualification;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgQualification : NSObject {
	
/* elements */
	NSNumber * CLogEducationLevelId;
	NSNumber * CLogMajorId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * Description;
	emp_tns1_ArrayOfEmpProfileQualification * EmpProfileQualifications;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * OrgQualificationCode;
	NSNumber * OrgQualificationId;
	NSString * OrgQualificationName;
	emp_tns1_ArrayOfRecCandidateQualification * RecCandidateQualifications;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogEducationLevelId;
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) emp_tns1_ArrayOfEmpProfileQualification * EmpProfileQualifications;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OrgQualificationCode;
@property (retain) NSNumber * OrgQualificationId;
@property (retain) NSString * OrgQualificationName;
@property (retain) emp_tns1_ArrayOfRecCandidateQualification * RecCandidateQualifications;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecCandidateQualification : NSObject {
	
/* elements */
	emp_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	USBoolean * IsDeleted;
	USBoolean * IsValId;
	NSDate * ModifiedDate;
	emp_tns1_OrgQualification * OrgQualification;
	NSNumber * OrgQualificationId;
	NSString * Other;
	NSString * PlaceIssue;
	emp_tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateId;
	NSNumber * RecCandidateQualificationId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecCandidateQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValId;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_OrgQualification * OrgQualification;
@property (retain) NSNumber * OrgQualificationId;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) emp_tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateId;
@property (retain) NSNumber * RecCandidateQualificationId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecCandidateQualification : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateQualification;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecCandidateQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateQualification:(emp_tns1_RecCandidateQualification *)toAdd;
@property (readonly) NSMutableArray * RecCandidateQualification;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileSkill : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	emp_tns1_CLogRating * CLogRating;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * DocumentRef;
	NSNumber * EmpProfileSkillId;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	emp_tns1_OrgSkill * OrgSkill;
	NSNumber * OrgSkillId;
	NSNumber * RatingId;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) emp_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DocumentRef;
@property (retain) NSNumber * EmpProfileSkillId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_OrgSkill * OrgSkill;
@property (retain) NSNumber * OrgSkillId;
@property (retain) NSNumber * RatingId;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileSkill : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileSkill:(emp_tns1_EmpProfileSkill *)toAdd;
@property (readonly) NSMutableArray * EmpProfileSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfOrgSkill : NSObject {
	
/* elements */
	NSMutableArray *OrgSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfOrgSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgSkill:(emp_tns1_OrgSkill *)toAdd;
@property (readonly) NSMutableArray * OrgSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgSkillType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * OrgSkillTypeId;
	emp_tns1_ArrayOfOrgSkill * OrgSkills;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgSkillType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * OrgSkillTypeId;
@property (retain) emp_tns1_ArrayOfOrgSkill * OrgSkills;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfTrainingCourseSchedule : NSObject {
	
/* elements */
	NSMutableArray *TrainingCourseSchedule;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfTrainingCourseSchedule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingCourseSchedule:(emp_tns1_TrainingCourseSchedule *)toAdd;
@property (readonly) NSMutableArray * TrainingCourseSchedule;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogCourseSchedule : NSObject {
	
/* elements */
	NSNumber * ClogCourseScheduleId;
	NSNumber * DayOfWeek;
	NSDate * EndTime;
	USBoolean * IsDeleted;
	NSNumber * Shift;
	NSDate * StartTime;
	emp_tns1_ArrayOfTrainingCourseSchedule * TrainingCourseSchedules;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogCourseSchedule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ClogCourseScheduleId;
@property (retain) NSNumber * DayOfWeek;
@property (retain) NSDate * EndTime;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Shift;
@property (retain) NSDate * StartTime;
@property (retain) emp_tns1_ArrayOfTrainingCourseSchedule * TrainingCourseSchedules;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfTrainingCourse : NSObject {
	
/* elements */
	NSMutableArray *TrainingCourse;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfTrainingCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingCourse:(emp_tns1_TrainingCourse *)toAdd;
@property (readonly) NSMutableArray * TrainingCourse;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogTrainingCenter : NSObject {
	
/* elements */
	NSNumber * CLogTrainingCenterId;
	USBoolean * IsDeleted;
	NSString * Name;
	emp_tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogTrainingCenter *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogTrainingCenterId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) emp_tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_TrainingCategory : NSObject {
	
/* elements */
	NSDate * CreateDate;
	NSString * Description;
	NSString * ImgUrl;
	USBoolean * IsDeleted;
	NSString * Name;
	NSNumber * TrainingCategoryId;
	emp_tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_TrainingCategory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) NSString * ImgUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSNumber * TrainingCategoryId;
@property (retain) emp_tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_TrainingCourseChapter : NSObject {
	
/* elements */
	NSNumber * Chapter;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * Session;
	emp_tns1_TrainingCourse * TrainingCourse;
	NSNumber * TrainingCourseChapterId;
	NSNumber * TrainingCourseId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_TrainingCourseChapter *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Chapter;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * Session;
@property (retain) emp_tns1_TrainingCourse * TrainingCourse;
@property (retain) NSNumber * TrainingCourseChapterId;
@property (retain) NSNumber * TrainingCourseId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfTrainingCourseChapter : NSObject {
	
/* elements */
	NSMutableArray *TrainingCourseChapter;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfTrainingCourseChapter *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingCourseChapter:(emp_tns1_TrainingCourseChapter *)toAdd;
@property (readonly) NSMutableArray * TrainingCourseChapter;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_TrainingCoursePeriod : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
	NSNumber * Session;
	NSString * Target;
	NSNumber * TrainingCoursePeriodId;
	emp_tns1_ArrayOfTrainingCourse * TrainingCourses;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_TrainingCoursePeriod *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) NSNumber * Session;
@property (retain) NSString * Target;
@property (retain) NSNumber * TrainingCoursePeriodId;
@property (retain) emp_tns1_ArrayOfTrainingCourse * TrainingCourses;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_TrainingCourseType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * Note;
	NSNumber * TrainingCourseTypeId;
	emp_tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_TrainingCourseType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) NSNumber * TrainingCourseTypeId;
@property (retain) emp_tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_TrainingCourseUnit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * OrgUnitId;
	emp_tns1_TrainingCourse * TrainingCourse;
	NSNumber * TrainingCourseId;
	NSNumber * TrainingCourseUnitId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_TrainingCourseUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * OrgUnitId;
@property (retain) emp_tns1_TrainingCourse * TrainingCourse;
@property (retain) NSNumber * TrainingCourseId;
@property (retain) NSNumber * TrainingCourseUnitId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfTrainingCourseUnit : NSObject {
	
/* elements */
	NSMutableArray *TrainingCourseUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfTrainingCourseUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingCourseUnit:(emp_tns1_TrainingCourseUnit *)toAdd;
@property (readonly) NSMutableArray * TrainingCourseUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_TrainingPlanDegree : NSObject {
	
/* elements */
	NSNumber * CLogDegreeId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_TrainingCourse * TrainingCourse;
	NSNumber * TrainingPlanDegreeId;
	NSNumber * TraningCourseId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_TrainingPlanDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogDegreeId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_TrainingCourse * TrainingCourse;
@property (retain) NSNumber * TrainingPlanDegreeId;
@property (retain) NSNumber * TraningCourseId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfTrainingPlanDegree : NSObject {
	
/* elements */
	NSMutableArray *TrainingPlanDegree;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfTrainingPlanDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingPlanDegree:(emp_tns1_TrainingPlanDegree *)toAdd;
@property (readonly) NSMutableArray * TrainingPlanDegree;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileEducation : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	emp_tns1_CLogMajor * CLogMajor;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileEducationId;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EndYear;
	USBoolean * IsDeleted;
	NSString * Major;
	NSNumber * MajorId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * RelateRowId;
	NSNumber * StartYear;
	NSString * University;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileEducation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) emp_tns1_CLogMajor * CLogMajor;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileEducationId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EndYear;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Major;
@property (retain) NSNumber * MajorId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSNumber * StartYear;
@property (retain) NSString * University;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileEducation : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileEducation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileEducation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileEducation:(emp_tns1_EmpProfileEducation *)toAdd;
@property (readonly) NSMutableArray * EmpProfileEducation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfTrainingPlanRequest : NSObject {
	
/* elements */
	NSMutableArray *TrainingPlanRequest;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfTrainingPlanRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingPlanRequest:(emp_tns1_TrainingPlanRequest *)toAdd;
@property (readonly) NSMutableArray * TrainingPlanRequest;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogMajor : NSObject {
	
/* elements */
	NSNumber * CLogCareerId;
	NSString * CLogMajorCode;
	NSNumber * CLogMajorId;
	emp_tns1_ArrayOfEmpProfileEducation * EmpProfileEducations;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	emp_tns1_ArrayOfTrainingPlanRequest * TrainingPlanRequests;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogMajor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogCareerId;
@property (retain) NSString * CLogMajorCode;
@property (retain) NSNumber * CLogMajorId;
@property (retain) emp_tns1_ArrayOfEmpProfileEducation * EmpProfileEducations;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) emp_tns1_ArrayOfTrainingPlanRequest * TrainingPlanRequests;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_TrainingPlanEmployee : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSDate * CreatedDate;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * TrainingPlanEmployeeId;
	emp_tns1_TrainingPlanRequest * TrainingPlanRequest;
	NSNumber * TrainingPlanRequestId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_TrainingPlanEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * TrainingPlanEmployeeId;
@property (retain) emp_tns1_TrainingPlanRequest * TrainingPlanRequest;
@property (retain) NSNumber * TrainingPlanRequestId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfTrainingPlanEmployee : NSObject {
	
/* elements */
	NSMutableArray *TrainingPlanEmployee;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfTrainingPlanEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingPlanEmployee:(emp_tns1_TrainingPlanEmployee *)toAdd;
@property (readonly) NSMutableArray * TrainingPlanEmployee;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_TrainingPlanRequest : NSObject {
	
/* elements */
	NSNumber * ApproverId;
	emp_tns1_CLogMajor * CLogMajor;
	NSNumber * CLogMajorId;
	NSNumber * CompanyId;
	NSString * Content;
	NSNumber * Cost;
	NSDate * CreatedDate;
	NSDate * EndDate;
	USBoolean * IsApproved;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSDate * StartDate;
	NSNumber * StrategyGoalId;
	NSString * Target;
	NSString * Title;
	emp_tns1_ArrayOfTrainingCourse * TrainingCourses;
	emp_tns1_ArrayOfTrainingPlanEmployee * TrainingPlanEmployees;
	NSNumber * TrainingPlanRequestId;
	NSNumber * TrainingPlanRequestTypeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_TrainingPlanRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproverId;
@property (retain) emp_tns1_CLogMajor * CLogMajor;
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Content;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsApproved;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * StartDate;
@property (retain) NSNumber * StrategyGoalId;
@property (retain) NSString * Target;
@property (retain) NSString * Title;
@property (retain) emp_tns1_ArrayOfTrainingCourse * TrainingCourses;
@property (retain) emp_tns1_ArrayOfTrainingPlanEmployee * TrainingPlanEmployees;
@property (retain) NSNumber * TrainingPlanRequestId;
@property (retain) NSNumber * TrainingPlanRequestTypeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_TrainingProficencyExpected : NSObject {
	
/* elements */
	emp_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	USBoolean * IsDeleted;
	NSNumber * ProficencyId;
	NSNumber * ProficencyType;
	emp_tns1_TrainingCourse * TrainingCourse;
	NSNumber * TrainingCourseId;
	NSNumber * TrainingProficencyExpectedId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_TrainingProficencyExpected *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ProficencyId;
@property (retain) NSNumber * ProficencyType;
@property (retain) emp_tns1_TrainingCourse * TrainingCourse;
@property (retain) NSNumber * TrainingCourseId;
@property (retain) NSNumber * TrainingProficencyExpectedId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfTrainingProficencyExpected : NSObject {
	
/* elements */
	NSMutableArray *TrainingProficencyExpected;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfTrainingProficencyExpected *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingProficencyExpected:(emp_tns1_TrainingProficencyExpected *)toAdd;
@property (readonly) NSMutableArray * TrainingProficencyExpected;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_TrainingProficencyRequire : NSObject {
	
/* elements */
	emp_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	USBoolean * IsDeleted;
	NSNumber * ProficencyId;
	NSNumber * ProficencyType;
	emp_tns1_TrainingCourse * TrainingCourse;
	NSNumber * TrainingCourseId;
	NSNumber * TrainingProficencyRequireId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_TrainingProficencyRequire *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ProficencyId;
@property (retain) NSNumber * ProficencyType;
@property (retain) emp_tns1_TrainingCourse * TrainingCourse;
@property (retain) NSNumber * TrainingCourseId;
@property (retain) NSNumber * TrainingProficencyRequireId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfTrainingProficencyRequire : NSObject {
	
/* elements */
	NSMutableArray *TrainingProficencyRequire;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfTrainingProficencyRequire *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingProficencyRequire:(emp_tns1_TrainingProficencyRequire *)toAdd;
@property (readonly) NSMutableArray * TrainingProficencyRequire;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_TrainingCourse : NSObject {
	
/* elements */
	emp_tns1_CLogTrainer * CLogTrainer;
	NSNumber * CLogTrainerId;
	emp_tns1_CLogTrainingCenter * CLogTrainingCenter;
	NSNumber * CLogTrainingCenterId;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSDate * CreatedDate;
	NSString * Email;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	USBoolean * IsExpired;
	USBoolean * IsLongTrainingCourse;
	USBoolean * IsNecessitated;
	USBoolean * IsPublish;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * Number;
	NSString * PhoneNumber;
	NSString * Result;
	NSDate * StartDate;
	NSNumber * StrategyGoalId;
	emp_tns1_TrainingCategory * TrainingCategory;
	NSNumber * TrainingCategoryId;
	emp_tns1_ArrayOfTrainingCourseChapter * TrainingCourseChapters;
	NSNumber * TrainingCourseId;
	emp_tns1_TrainingCoursePeriod * TrainingCoursePeriod;
	NSNumber * TrainingCoursePeriodId;
	emp_tns1_ArrayOfTrainingCourseSchedule * TrainingCourseSchedules;
	emp_tns1_TrainingCourseType * TrainingCourseType;
	NSNumber * TrainingCourseTypeId;
	emp_tns1_ArrayOfTrainingCourseUnit * TrainingCourseUnits;
	emp_tns1_ArrayOfTrainingPlanDegree * TrainingPlanDegrees;
	emp_tns1_TrainingPlanRequest * TrainingPlanRequest;
	NSNumber * TrainingPlanRequestId;
	emp_tns1_ArrayOfTrainingProficencyExpected * TrainingProficencyExpecteds;
	emp_tns1_ArrayOfTrainingProficencyRequire * TrainingProficencyRequires;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_TrainingCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogTrainer * CLogTrainer;
@property (retain) NSNumber * CLogTrainerId;
@property (retain) emp_tns1_CLogTrainingCenter * CLogTrainingCenter;
@property (retain) NSNumber * CLogTrainingCenterId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Email;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsExpired;
@property (retain) USBoolean * IsLongTrainingCourse;
@property (retain) USBoolean * IsNecessitated;
@property (retain) USBoolean * IsPublish;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * Number;
@property (retain) NSString * PhoneNumber;
@property (retain) NSString * Result;
@property (retain) NSDate * StartDate;
@property (retain) NSNumber * StrategyGoalId;
@property (retain) emp_tns1_TrainingCategory * TrainingCategory;
@property (retain) NSNumber * TrainingCategoryId;
@property (retain) emp_tns1_ArrayOfTrainingCourseChapter * TrainingCourseChapters;
@property (retain) NSNumber * TrainingCourseId;
@property (retain) emp_tns1_TrainingCoursePeriod * TrainingCoursePeriod;
@property (retain) NSNumber * TrainingCoursePeriodId;
@property (retain) emp_tns1_ArrayOfTrainingCourseSchedule * TrainingCourseSchedules;
@property (retain) emp_tns1_TrainingCourseType * TrainingCourseType;
@property (retain) NSNumber * TrainingCourseTypeId;
@property (retain) emp_tns1_ArrayOfTrainingCourseUnit * TrainingCourseUnits;
@property (retain) emp_tns1_ArrayOfTrainingPlanDegree * TrainingPlanDegrees;
@property (retain) emp_tns1_TrainingPlanRequest * TrainingPlanRequest;
@property (retain) NSNumber * TrainingPlanRequestId;
@property (retain) emp_tns1_ArrayOfTrainingProficencyExpected * TrainingProficencyExpecteds;
@property (retain) emp_tns1_ArrayOfTrainingProficencyRequire * TrainingProficencyRequires;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfTrainingCourseEmployee : NSObject {
	
/* elements */
	NSMutableArray *TrainingCourseEmployee;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfTrainingCourseEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingCourseEmployee:(emp_tns1_TrainingCourseEmployee *)toAdd;
@property (readonly) NSMutableArray * TrainingCourseEmployee;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_TrainingCourseSchedule : NSObject {
	
/* elements */
	emp_tns1_CLogCourseSchedule * CLogCourseSchedule;
	NSNumber * ClogCourseScheduleId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_TrainingCourse * TrainingCourse;
	emp_tns1_ArrayOfTrainingCourseEmployee * TrainingCourseEmployees;
	NSNumber * TrainingCourseId;
	NSNumber * TrainingCourseScheduleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_TrainingCourseSchedule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogCourseSchedule * CLogCourseSchedule;
@property (retain) NSNumber * ClogCourseScheduleId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_TrainingCourse * TrainingCourse;
@property (retain) emp_tns1_ArrayOfTrainingCourseEmployee * TrainingCourseEmployees;
@property (retain) NSNumber * TrainingCourseId;
@property (retain) NSNumber * TrainingCourseScheduleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_TrainingCourseEmployee : NSObject {
	
/* elements */
	NSNumber * Absence;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSDate * CreatedDate;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsGraduated;
	NSNumber * Mark;
	NSDate * ModifiedDate;
	NSNumber * TrainingCourseEmployeeId;
	emp_tns1_TrainingCourseSchedule * TrainingCourseSchedule;
	NSNumber * TrainingCourseScheduleId;
	emp_tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_TrainingCourseEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Absence;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsGraduated;
@property (retain) NSNumber * Mark;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * TrainingCourseEmployeeId;
@property (retain) emp_tns1_TrainingCourseSchedule * TrainingCourseSchedule;
@property (retain) NSNumber * TrainingCourseScheduleId;
@property (retain) emp_tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_TrainingEmpProficency : NSObject {
	
/* elements */
	NSNumber * CLogEmpRatingId;
	emp_tns1_CLogRating * CLogRating;
	emp_tns1_CLogRating * CLogRating1;
	NSNumber * ClogEmpManagerId;
	USBoolean * IsDeleted;
	NSNumber * ManagerId;
	emp_tns1_OrgSkill * OrgSkill;
	NSNumber * ProficencyId;
	NSNumber * ProficencyType;
	emp_tns1_TrainingCourseEmployee * TrainingCourseEmployee;
	NSNumber * TrainingCourseEmployeeId;
	NSNumber * TrainingEmpProficencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_TrainingEmpProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogEmpRatingId;
@property (retain) emp_tns1_CLogRating * CLogRating;
@property (retain) emp_tns1_CLogRating * CLogRating1;
@property (retain) NSNumber * ClogEmpManagerId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ManagerId;
@property (retain) emp_tns1_OrgSkill * OrgSkill;
@property (retain) NSNumber * ProficencyId;
@property (retain) NSNumber * ProficencyType;
@property (retain) emp_tns1_TrainingCourseEmployee * TrainingCourseEmployee;
@property (retain) NSNumber * TrainingCourseEmployeeId;
@property (retain) NSNumber * TrainingEmpProficencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfTrainingEmpProficency : NSObject {
	
/* elements */
	NSMutableArray *TrainingEmpProficency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfTrainingEmpProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingEmpProficency:(emp_tns1_TrainingEmpProficency *)toAdd;
@property (readonly) NSMutableArray * TrainingEmpProficency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgSkill : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * Description;
	emp_tns1_ArrayOfEmpProfileSkill * EmpProfileSkills;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * OrgSkillCode;
	NSNumber * OrgSkillId;
	emp_tns1_OrgSkillType * OrgSkillType;
	NSNumber * OrgSkillTypeId;
	NSNumber * Priority;
	NSString * PriorityName;
	emp_tns1_ArrayOfRecCandidateSkill * RecCandidateSkills;
	emp_tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) emp_tns1_ArrayOfEmpProfileSkill * EmpProfileSkills;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * OrgSkillCode;
@property (retain) NSNumber * OrgSkillId;
@property (retain) emp_tns1_OrgSkillType * OrgSkillType;
@property (retain) NSNumber * OrgSkillTypeId;
@property (retain) NSNumber * Priority;
@property (retain) NSString * PriorityName;
@property (retain) emp_tns1_ArrayOfRecCandidateSkill * RecCandidateSkills;
@property (retain) emp_tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecCandidateSkill : NSObject {
	
/* elements */
	emp_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * DocumentRef;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_OrgSkill * OrgSkill;
	NSNumber * OrgSkillId;
	emp_tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateId;
	NSNumber * RecCandidateSkillId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecCandidateSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DocumentRef;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_OrgSkill * OrgSkill;
@property (retain) NSNumber * OrgSkillId;
@property (retain) emp_tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateId;
@property (retain) NSNumber * RecCandidateSkillId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecCandidateSkill : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecCandidateSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateSkill:(emp_tns1_RecCandidateSkill *)toAdd;
@property (readonly) NSMutableArray * RecCandidateSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecCandidate : NSObject {
	
/* elements */
	NSMutableArray *RecCandidate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecCandidate *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidate:(emp_tns1_RecCandidate *)toAdd;
@property (readonly) NSMutableArray * RecCandidate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecCandidateStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * RecCandidateStatusId;
	emp_tns1_ArrayOfRecCandidate * RecCandidates;
	NSString * StatusName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecCandidateStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecCandidateStatusId;
@property (retain) emp_tns1_ArrayOfRecCandidate * RecCandidates;
@property (retain) NSString * StatusName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecCandidateSupplier : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateSupplier;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecCandidateSupplier *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateSupplier:(emp_tns1_RecCandidateSupplier *)toAdd;
@property (readonly) NSMutableArray * RecCandidateSupplier;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecCandidateTypeSupplier : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * RecCadidateTypeSupplierId;
	emp_tns1_ArrayOfRecCandidateSupplier * RecCandidateSuppliers;
	NSString * TypeSupplierName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecCandidateTypeSupplier *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecCadidateTypeSupplierId;
@property (retain) emp_tns1_ArrayOfRecCandidateSupplier * RecCandidateSuppliers;
@property (retain) NSString * TypeSupplierName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecCandidateSupplier : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * RecCadidateSupplierId;
	NSNumber * RecCadidateTypeSupplierId;
	emp_tns1_RecCandidateTypeSupplier * RecCandidateTypeSupplier;
	emp_tns1_ArrayOfRecCandidate * RecCandidates;
	NSString * SupplierAddress;
	NSString * SupplierEmail;
	NSString * SupplierName;
	NSString * SupplierPhone;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecCandidateSupplier *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecCadidateSupplierId;
@property (retain) NSNumber * RecCadidateTypeSupplierId;
@property (retain) emp_tns1_RecCandidateTypeSupplier * RecCandidateTypeSupplier;
@property (retain) emp_tns1_ArrayOfRecCandidate * RecCandidates;
@property (retain) NSString * SupplierAddress;
@property (retain) NSString * SupplierEmail;
@property (retain) NSString * SupplierName;
@property (retain) NSString * SupplierPhone;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecEvaluationCriterion : NSObject {
	
/* elements */
	NSMutableArray *RecEvaluationCriterion;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecEvaluationCriterion *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecEvaluationCriterion:(emp_tns1_RecEvaluationCriterion *)toAdd;
@property (readonly) NSMutableArray * RecEvaluationCriterion;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecGroupEvaluationCriterion : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * Priority;
	emp_tns1_ArrayOfRecEvaluationCriterion * RecEvaluationCriterions;
	NSString * RecGroupEvalDesc;
	NSString * RecGroupEvalName;
	NSNumber * RecGroupEvaluationCriterionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecGroupEvaluationCriterion *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Priority;
@property (retain) emp_tns1_ArrayOfRecEvaluationCriterion * RecEvaluationCriterions;
@property (retain) NSString * RecGroupEvalDesc;
@property (retain) NSString * RecGroupEvalName;
@property (retain) NSNumber * RecGroupEvaluationCriterionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecEvaluationCriterion : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * MaxPoint;
	NSNumber * Priority;
	NSString * RecEvalDesc;
	NSString * RecEvalName;
	NSNumber * RecEvaluationCriterionId;
	emp_tns1_RecGroupEvaluationCriterion * RecGroupEvaluationCriterion;
	NSNumber * RecGroupEvaluationCriterionId;
	emp_tns1_ArrayOfRecInterviewPhaseEvaluation * RecInterviewPhaseEvaluations;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecEvaluationCriterion *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * MaxPoint;
@property (retain) NSNumber * Priority;
@property (retain) NSString * RecEvalDesc;
@property (retain) NSString * RecEvalName;
@property (retain) NSNumber * RecEvaluationCriterionId;
@property (retain) emp_tns1_RecGroupEvaluationCriterion * RecGroupEvaluationCriterion;
@property (retain) NSNumber * RecGroupEvaluationCriterionId;
@property (retain) emp_tns1_ArrayOfRecInterviewPhaseEvaluation * RecInterviewPhaseEvaluations;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecInterviewSchedule : NSObject {
	
/* elements */
	NSMutableArray *RecInterviewSchedule;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecInterviewSchedule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecInterviewSchedule:(emp_tns1_RecInterviewSchedule *)toAdd;
@property (readonly) NSMutableArray * RecInterviewSchedule;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecInterviewer : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsMainInterviewer;
	NSDate * ModifiedDate;
	emp_tns1_RecGroupInterviewer * RecGroupInterviewer;
	NSNumber * RecGroupInterviewerId;
	NSNumber * RecInterviewerId;
	emp_tns1_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecInterviewer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainInterviewer;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_RecGroupInterviewer * RecGroupInterviewer;
@property (retain) NSNumber * RecGroupInterviewerId;
@property (retain) NSNumber * RecInterviewerId;
@property (retain) emp_tns1_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecInterviewer : NSObject {
	
/* elements */
	NSMutableArray *RecInterviewer;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecInterviewer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecInterviewer:(emp_tns1_RecInterviewer *)toAdd;
@property (readonly) NSMutableArray * RecInterviewer;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecGroupInterviewer : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * GroupName;
	USBoolean * IsDeleted;
	NSNumber * RecGroupInterviewerId;
	emp_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
	emp_tns1_ArrayOfRecInterviewer * RecInterviewers;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecGroupInterviewer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * GroupName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecGroupInterviewerId;
@property (retain) emp_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
@property (retain) emp_tns1_ArrayOfRecInterviewer * RecInterviewers;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecInterviewScheduleStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * Priority;
	NSNumber * RecInterviewScheduleStatusId;
	emp_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
	NSString * StatusCode;
	NSString * StatusName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecInterviewScheduleStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Priority;
@property (retain) NSNumber * RecInterviewScheduleStatusId;
@property (retain) emp_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
@property (retain) NSString * StatusCode;
@property (retain) NSString * StatusName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecInterviewSchedule : NSObject {
	
/* elements */
	NSNumber * AveragePoint;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeCreateId;
	NSDate * InterviewDate;
	NSString * InterviewPlace;
	NSDate * InterviewTime;
	USBoolean * IsDeleted;
	USBoolean * IsIntervieweeNotified;
	USBoolean * IsInterviewerNotified;
	NSDate * ModifiedDate;
	emp_tns1_RecCandidateApplication * RecCandidateApplication;
	NSNumber * RecCandidateApplicationId;
	emp_tns1_RecGroupInterviewer * RecGroupInterviewer;
	NSNumber * RecGroupInterviewerId;
	emp_tns1_RecInterviewPhase * RecInterviewPhase;
	NSNumber * RecInterviewPhaseId;
	NSNumber * RecInterviewScheduleId;
	emp_tns1_RecInterviewScheduleStatu * RecInterviewScheduleStatu;
	NSNumber * RecInterviewScheduleStatusId;
	emp_tns1_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecInterviewSchedule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AveragePoint;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeCreateId;
@property (retain) NSDate * InterviewDate;
@property (retain) NSString * InterviewPlace;
@property (retain) NSDate * InterviewTime;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsIntervieweeNotified;
@property (retain) USBoolean * IsInterviewerNotified;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_RecCandidateApplication * RecCandidateApplication;
@property (retain) NSNumber * RecCandidateApplicationId;
@property (retain) emp_tns1_RecGroupInterviewer * RecGroupInterviewer;
@property (retain) NSNumber * RecGroupInterviewerId;
@property (retain) emp_tns1_RecInterviewPhase * RecInterviewPhase;
@property (retain) NSNumber * RecInterviewPhaseId;
@property (retain) NSNumber * RecInterviewScheduleId;
@property (retain) emp_tns1_RecInterviewScheduleStatu * RecInterviewScheduleStatu;
@property (retain) NSNumber * RecInterviewScheduleStatusId;
@property (retain) emp_tns1_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecScheduleInterviewerResult : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Point_;
	emp_tns1_RecInterviewPhaseEvaluation * RecInterviewPhaseEvaluation;
	NSNumber * RecInterviewPhaseEvaluationId;
	emp_tns1_RecInterviewSchedule * RecInterviewSchedule;
	NSNumber * RecInterviewScheduleId;
	emp_tns1_RecInterviewer * RecInterviewer;
	NSNumber * RecInterviewerId;
	NSNumber * RecScheduleInterviewerResultId;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecScheduleInterviewerResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Point_;
@property (retain) emp_tns1_RecInterviewPhaseEvaluation * RecInterviewPhaseEvaluation;
@property (retain) NSNumber * RecInterviewPhaseEvaluationId;
@property (retain) emp_tns1_RecInterviewSchedule * RecInterviewSchedule;
@property (retain) NSNumber * RecInterviewScheduleId;
@property (retain) emp_tns1_RecInterviewer * RecInterviewer;
@property (retain) NSNumber * RecInterviewerId;
@property (retain) NSNumber * RecScheduleInterviewerResultId;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecScheduleInterviewerResult : NSObject {
	
/* elements */
	NSMutableArray *RecScheduleInterviewerResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecScheduleInterviewerResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecScheduleInterviewerResult:(emp_tns1_RecScheduleInterviewerResult *)toAdd;
@property (readonly) NSMutableArray * RecScheduleInterviewerResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecInterviewPhaseEvaluation : NSObject {
	
/* elements */
	NSNumber * ComanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_RecEvaluationCriterion * RecEvaluationCriterion;
	NSNumber * RecEvaluationCriterionId;
	emp_tns1_RecInterviewPhase * RecInterviewPhase;
	NSNumber * RecInterviewPhaseEvaluationId;
	NSNumber * RecInterviewPhaseId;
	emp_tns1_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecInterviewPhaseEvaluation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ComanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_RecEvaluationCriterion * RecEvaluationCriterion;
@property (retain) NSNumber * RecEvaluationCriterionId;
@property (retain) emp_tns1_RecInterviewPhase * RecInterviewPhase;
@property (retain) NSNumber * RecInterviewPhaseEvaluationId;
@property (retain) NSNumber * RecInterviewPhaseId;
@property (retain) emp_tns1_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecInterviewPhaseEvaluation : NSObject {
	
/* elements */
	NSMutableArray *RecInterviewPhaseEvaluation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecInterviewPhaseEvaluation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecInterviewPhaseEvaluation:(emp_tns1_RecInterviewPhaseEvaluation *)toAdd;
@property (readonly) NSMutableArray * RecInterviewPhaseEvaluation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecInterviewPhase : NSObject {
	
/* elements */
	NSNumber * CoefficientPoint;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * InterviewPhaseName;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Priority;
	emp_tns1_ArrayOfRecInterviewPhaseEvaluation * RecInterviewPhaseEvaluations;
	NSNumber * RecInterviewPhaseId;
	emp_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
	NSNumber * RecProcessPhaseId;
	emp_tns1_RecRecruitmentProcessPhase * RecRecruitmentProcessPhase;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecInterviewPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CoefficientPoint;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * InterviewPhaseName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Priority;
@property (retain) emp_tns1_ArrayOfRecInterviewPhaseEvaluation * RecInterviewPhaseEvaluations;
@property (retain) NSNumber * RecInterviewPhaseId;
@property (retain) emp_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
@property (retain) NSNumber * RecProcessPhaseId;
@property (retain) emp_tns1_RecRecruitmentProcessPhase * RecRecruitmentProcessPhase;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecInterviewPhase : NSObject {
	
/* elements */
	NSMutableArray *RecInterviewPhase;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecInterviewPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecInterviewPhase:(emp_tns1_RecInterviewPhase *)toAdd;
@property (readonly) NSMutableArray * RecInterviewPhase;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecProcessApplyForJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_OrgJobPosition * OrgJobPosition;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSNumber * RecProcessApplyForJobPositionId;
	emp_tns1_RecRecruitmentProcess * RecRecruitmentProcess;
	NSNumber * RecRecruitmentProcessId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecProcessApplyForJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * RecProcessApplyForJobPositionId;
@property (retain) emp_tns1_RecRecruitmentProcess * RecRecruitmentProcess;
@property (retain) NSNumber * RecRecruitmentProcessId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecProcessApplyForJobPosition : NSObject {
	
/* elements */
	NSMutableArray *RecProcessApplyForJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecProcessApplyForJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecProcessApplyForJobPosition:(emp_tns1_RecProcessApplyForJobPosition *)toAdd;
@property (readonly) NSMutableArray * RecProcessApplyForJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecRecruitmentProcessPhase : NSObject {
	
/* elements */
	NSMutableArray *RecRecruitmentProcessPhase;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecRecruitmentProcessPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRecruitmentProcessPhase:(emp_tns1_RecRecruitmentProcessPhase *)toAdd;
@property (readonly) NSMutableArray * RecRecruitmentProcessPhase;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecRecruitmentProcess : NSObject {
	
/* elements */
	NSDate * ApplyDate;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_ArrayOfRecProcessApplyForJobPosition * RecProcessApplyForJobPositions;
	NSString * RecProcessDescription;
	NSString * RecProcessName;
	NSNumber * RecRecruitmentProcessId;
	emp_tns1_ArrayOfRecRecruitmentProcessPhase * RecRecruitmentProcessPhases;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecRecruitmentProcess *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ApplyDate;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_ArrayOfRecProcessApplyForJobPosition * RecProcessApplyForJobPositions;
@property (retain) NSString * RecProcessDescription;
@property (retain) NSString * RecProcessName;
@property (retain) NSNumber * RecRecruitmentProcessId;
@property (retain) emp_tns1_ArrayOfRecRecruitmentProcessPhase * RecRecruitmentProcessPhases;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecRecruitmentProcessPhase : NSObject {
	
/* elements */
	NSNumber * CoefficientPoint;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Priority;
	emp_tns1_ArrayOfRecInterviewPhase * RecInterviewPhases;
	NSString * RecProcessPhaseName;
	emp_tns1_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
	emp_tns1_RecRecruitmentProcess * RecRecruitmentProcess;
	NSNumber * RecRecruitmentProcessId;
	NSNumber * RecRecruitmentProcessPhaseId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecRecruitmentProcessPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CoefficientPoint;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Priority;
@property (retain) emp_tns1_ArrayOfRecInterviewPhase * RecInterviewPhases;
@property (retain) NSString * RecProcessPhaseName;
@property (retain) emp_tns1_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
@property (retain) emp_tns1_RecRecruitmentProcess * RecRecruitmentProcess;
@property (retain) NSNumber * RecRecruitmentProcessId;
@property (retain) NSNumber * RecRecruitmentProcessPhaseId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecProcessPhaseResult : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Point_;
	emp_tns1_RecCandidate * RecCandidate;
	emp_tns1_RecCandidateApplication * RecCandidateApplication;
	NSNumber * RecCandidateApplicationId;
	NSNumber * RecCandidateId;
	NSNumber * RecProcessPhaseResultId;
	emp_tns1_RecRecruitmentProcessPhase * RecRecruitmentProcessPhase;
	NSNumber * RecRecruitmentProcessPhaseId;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecProcessPhaseResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Point_;
@property (retain) emp_tns1_RecCandidate * RecCandidate;
@property (retain) emp_tns1_RecCandidateApplication * RecCandidateApplication;
@property (retain) NSNumber * RecCandidateApplicationId;
@property (retain) NSNumber * RecCandidateId;
@property (retain) NSNumber * RecProcessPhaseResultId;
@property (retain) emp_tns1_RecRecruitmentProcessPhase * RecRecruitmentProcessPhase;
@property (retain) NSNumber * RecRecruitmentProcessPhaseId;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecProcessPhaseResult : NSObject {
	
/* elements */
	NSMutableArray *RecProcessPhaseResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecProcessPhaseResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecProcessPhaseResult:(emp_tns1_RecProcessPhaseResult *)toAdd;
@property (readonly) NSMutableArray * RecProcessPhaseResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecCandidate : NSObject {
	
/* elements */
	NSString * Address1;
	NSNumber * Address1Id;
	NSString * Address1Phone;
	NSString * Address2;
	NSNumber * Address2Id;
	NSString * Address2Phone;
	NSDate * ApplyDate;
	NSDate * BirthDay;
	NSString * BirthPlace;
	NSNumber * BirthPlaceId;
	NSString * CVFileType;
	NSString * CVUrl;
	USBoolean * CanBusinessTrip;
	USBoolean * CanOT;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * CulturalLevelId;
	NSString * CurrentSalary;
	NSNumber * EducationLevelId;
	NSString * Email;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * Ethnicity;
	NSNumber * EthnicityId;
	NSString * ExpectedSalary;
	NSString * FirstName;
	NSString * Forte;
	NSString * FullName;
	USBoolean * Gender;
	NSString * HomeTown;
	NSNumber * HomeTownId;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	NSString * ImageUrl;
	USBoolean * IsDeleted;
	USBoolean * IsManager;
	USBoolean * IsOnlyYearOfBirthday;
	NSString * LastName;
	NSString * MainLanguage;
	NSNumber * MainLanguageId;
	NSNumber * MajorId;
	NSNumber * MaritalStatusId;
	NSString * Mobile;
	NSDate * ModifiedDate;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSNumber * NumEmpManaged;
	NSString * OfficePhone;
	NSNumber * RecCadidateSupplierId;
	emp_tns1_ArrayOfRecCandidateApplication * RecCandidateApplications;
	NSString * RecCandidateCode;
	emp_tns1_ArrayOfRecCandidateCompetencyRating * RecCandidateCompetencyRatings;
	emp_tns1_ArrayOfRecCandidateDegree * RecCandidateDegrees;
	emp_tns1_ArrayOfRecCandidateExperience * RecCandidateExperiences;
	emp_tns1_ArrayOfRecCandidateFamilyRelationship * RecCandidateFamilyRelationships;
	emp_tns1_ArrayOfRecCandidateForeignLanguage * RecCandidateForeignLanguages;
	NSNumber * RecCandidateId;
	emp_tns1_ArrayOfRecCandidateQualification * RecCandidateQualifications;
	emp_tns1_ArrayOfRecCandidateSkill * RecCandidateSkills;
	emp_tns1_RecCandidateStatu * RecCandidateStatu;
	NSNumber * RecCandidateStatusId;
	emp_tns1_RecCandidateSupplier * RecCandidateSupplier;
	emp_tns1_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
	NSString * Religion;
	NSNumber * ReligionId;
	NSString * Weaknesses;
	NSNumber * YearsExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecCandidate *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address1;
@property (retain) NSNumber * Address1Id;
@property (retain) NSString * Address1Phone;
@property (retain) NSString * Address2;
@property (retain) NSNumber * Address2Id;
@property (retain) NSString * Address2Phone;
@property (retain) NSDate * ApplyDate;
@property (retain) NSDate * BirthDay;
@property (retain) NSString * BirthPlace;
@property (retain) NSNumber * BirthPlaceId;
@property (retain) NSString * CVFileType;
@property (retain) NSString * CVUrl;
@property (retain) USBoolean * CanBusinessTrip;
@property (retain) USBoolean * CanOT;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * CulturalLevelId;
@property (retain) NSString * CurrentSalary;
@property (retain) NSNumber * EducationLevelId;
@property (retain) NSString * Email;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * Ethnicity;
@property (retain) NSNumber * EthnicityId;
@property (retain) NSString * ExpectedSalary;
@property (retain) NSString * FirstName;
@property (retain) NSString * Forte;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * HomeTown;
@property (retain) NSNumber * HomeTownId;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsManager;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) NSString * LastName;
@property (retain) NSString * MainLanguage;
@property (retain) NSNumber * MainLanguageId;
@property (retain) NSNumber * MajorId;
@property (retain) NSNumber * MaritalStatusId;
@property (retain) NSString * Mobile;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSNumber * NumEmpManaged;
@property (retain) NSString * OfficePhone;
@property (retain) NSNumber * RecCadidateSupplierId;
@property (retain) emp_tns1_ArrayOfRecCandidateApplication * RecCandidateApplications;
@property (retain) NSString * RecCandidateCode;
@property (retain) emp_tns1_ArrayOfRecCandidateCompetencyRating * RecCandidateCompetencyRatings;
@property (retain) emp_tns1_ArrayOfRecCandidateDegree * RecCandidateDegrees;
@property (retain) emp_tns1_ArrayOfRecCandidateExperience * RecCandidateExperiences;
@property (retain) emp_tns1_ArrayOfRecCandidateFamilyRelationship * RecCandidateFamilyRelationships;
@property (retain) emp_tns1_ArrayOfRecCandidateForeignLanguage * RecCandidateForeignLanguages;
@property (retain) NSNumber * RecCandidateId;
@property (retain) emp_tns1_ArrayOfRecCandidateQualification * RecCandidateQualifications;
@property (retain) emp_tns1_ArrayOfRecCandidateSkill * RecCandidateSkills;
@property (retain) emp_tns1_RecCandidateStatu * RecCandidateStatu;
@property (retain) NSNumber * RecCandidateStatusId;
@property (retain) emp_tns1_RecCandidateSupplier * RecCandidateSupplier;
@property (retain) emp_tns1_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
@property (retain) NSString * Religion;
@property (retain) NSNumber * ReligionId;
@property (retain) NSString * Weaknesses;
@property (retain) NSNumber * YearsExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecCandidateProfileStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * Priority;
	NSString * ProfileStatusCode;
	NSString * ProfileStatusName;
	emp_tns1_ArrayOfRecCandidateApplication * RecCandidateApplications;
	NSNumber * RecCandidateProfileStatusId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecCandidateProfileStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Priority;
@property (retain) NSString * ProfileStatusCode;
@property (retain) NSString * ProfileStatusName;
@property (retain) emp_tns1_ArrayOfRecCandidateApplication * RecCandidateApplications;
@property (retain) NSNumber * RecCandidateProfileStatusId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecCandidateApplication : NSObject {
	
/* elements */
	NSNumber * AveragePoint;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * CurrentInterviewPhaseId;
	NSNumber * CurrentProcessPhaseId;
	NSString * HRRemark;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSDate * NegotiateDate;
	emp_tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateApplicationId;
	NSNumber * RecCandidateId;
	emp_tns1_RecCandidateProfileStatu * RecCandidateProfileStatu;
	NSNumber * RecCandidateProfileStatusId;
	emp_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
	emp_tns1_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
	emp_tns1_RecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPosition;
	NSNumber * RecRecruitmentPhaseJobPositionId;
	NSDate * StartWorkingDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecCandidateApplication *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AveragePoint;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * CurrentInterviewPhaseId;
@property (retain) NSNumber * CurrentProcessPhaseId;
@property (retain) NSString * HRRemark;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * NegotiateDate;
@property (retain) emp_tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateApplicationId;
@property (retain) NSNumber * RecCandidateId;
@property (retain) emp_tns1_RecCandidateProfileStatu * RecCandidateProfileStatu;
@property (retain) NSNumber * RecCandidateProfileStatusId;
@property (retain) emp_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
@property (retain) emp_tns1_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
@property (retain) emp_tns1_RecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPosition;
@property (retain) NSNumber * RecRecruitmentPhaseJobPositionId;
@property (retain) NSDate * StartWorkingDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecCandidateApplication : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateApplication;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecCandidateApplication *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateApplication:(emp_tns1_RecCandidateApplication *)toAdd;
@property (readonly) NSMutableArray * RecCandidateApplication;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecRecruitmentPhaseJobPosition : NSObject {
	
/* elements */
	NSString * AgeLimitation;
	NSString * BeginSalaryLevel;
	NSString * BenefitAllowance;
	NSNumber * CompanyId;
	NSString * ContractTerm;
	NSDate * CreatedDate;
	NSNumber * DirectLeader;
	NSDate * FromDate;
	NSString * GenderLimitation;
	USBoolean * HasProbation;
	USBoolean * IsDeleted;
	USBoolean * IsDisplacement;
	NSString * JobDescription;
	NSDate * ModifiedDate;
	emp_tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSString * ProbationTime;
	emp_tns1_ArrayOfRecCandidateApplication * RecCandidateApplications;
	emp_tns1_RecRecruitmentPhase * RecRecruitmentPhase;
	NSNumber * RecRecruitmentPhaseId;
	NSNumber * RecRecruitmentPhaseJobPositionId;
	NSNumber * RecruitedQuantity;
	NSNumber * RequirementQuantity;
	NSDate * StartWorkDate;
	NSDate * ToDate;
	NSString * WorkingLocation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecRecruitmentPhaseJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AgeLimitation;
@property (retain) NSString * BeginSalaryLevel;
@property (retain) NSString * BenefitAllowance;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ContractTerm;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * DirectLeader;
@property (retain) NSDate * FromDate;
@property (retain) NSString * GenderLimitation;
@property (retain) USBoolean * HasProbation;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDisplacement;
@property (retain) NSString * JobDescription;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * ProbationTime;
@property (retain) emp_tns1_ArrayOfRecCandidateApplication * RecCandidateApplications;
@property (retain) emp_tns1_RecRecruitmentPhase * RecRecruitmentPhase;
@property (retain) NSNumber * RecRecruitmentPhaseId;
@property (retain) NSNumber * RecRecruitmentPhaseJobPositionId;
@property (retain) NSNumber * RecruitedQuantity;
@property (retain) NSNumber * RequirementQuantity;
@property (retain) NSDate * StartWorkDate;
@property (retain) NSDate * ToDate;
@property (retain) NSString * WorkingLocation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecRecruitmentPhaseJobPosition : NSObject {
	
/* elements */
	NSMutableArray *RecRecruitmentPhaseJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecRecruitmentPhaseJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRecruitmentPhaseJobPosition:(emp_tns1_RecRecruitmentPhaseJobPosition *)toAdd;
@property (readonly) NSMutableArray * RecRecruitmentPhaseJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SysRecPlanApprover : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsMainApprover;
	NSNumber * Priority;
	emp_tns1_ArrayOfRecPlanApproveHistory * RecPlanApproveHistories;
	NSNumber * SysRecPlanApproverId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SysRecPlanApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainApprover;
@property (retain) NSNumber * Priority;
@property (retain) emp_tns1_ArrayOfRecPlanApproveHistory * RecPlanApproveHistories;
@property (retain) NSNumber * SysRecPlanApproverId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecPlanApproveHistory : NSObject {
	
/* elements */
	NSDate * Date;
	USBoolean * IsApproved;
	USBoolean * IsDeleted;
	NSNumber * RecPlanApproveHistoryId;
	emp_tns1_RecRecruitmentPlan * RecRecruitmentPlan;
	NSNumber * RecRecruitmentPlanId;
	NSString * Remark;
	emp_tns1_SysRecPlanApprover * SysRecPlanApprover;
	NSNumber * SysRecPlanApproverId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecPlanApproveHistory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * Date;
@property (retain) USBoolean * IsApproved;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecPlanApproveHistoryId;
@property (retain) emp_tns1_RecRecruitmentPlan * RecRecruitmentPlan;
@property (retain) NSNumber * RecRecruitmentPlanId;
@property (retain) NSString * Remark;
@property (retain) emp_tns1_SysRecPlanApprover * SysRecPlanApprover;
@property (retain) NSNumber * SysRecPlanApproverId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecPlanApproveHistory : NSObject {
	
/* elements */
	NSMutableArray *RecPlanApproveHistory;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecPlanApproveHistory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecPlanApproveHistory:(emp_tns1_RecPlanApproveHistory *)toAdd;
@property (readonly) NSMutableArray * RecPlanApproveHistory;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecPlanJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSNumber * RecPlanJobPositionId;
	NSString * RecReason;
	emp_tns1_RecRecruitmentPlan * RecRecruitmentPlan;
	NSNumber * RecRecruitmentPlanId;
	NSNumber * RecruitedQuantity;
	NSNumber * RequirementQuantity;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecPlanJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * RecPlanJobPositionId;
@property (retain) NSString * RecReason;
@property (retain) emp_tns1_RecRecruitmentPlan * RecRecruitmentPlan;
@property (retain) NSNumber * RecRecruitmentPlanId;
@property (retain) NSNumber * RecruitedQuantity;
@property (retain) NSNumber * RequirementQuantity;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecPlanJobPosition : NSObject {
	
/* elements */
	NSMutableArray *RecPlanJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecPlanJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecPlanJobPosition:(emp_tns1_RecPlanJobPosition *)toAdd;
@property (readonly) NSMutableArray * RecPlanJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecRecruitmentPlan : NSObject {
	
/* elements */
	NSMutableArray *RecRecruitmentPlan;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecRecruitmentPlan *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRecruitmentPlan:(emp_tns1_RecRecruitmentPlan *)toAdd;
@property (readonly) NSMutableArray * RecRecruitmentPlan;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecPlanStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * RecPlanStatusId;
	emp_tns1_ArrayOfRecRecruitmentPlan * RecRecruitmentPlans;
	NSString * StatusName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecPlanStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecPlanStatusId;
@property (retain) emp_tns1_ArrayOfRecRecruitmentPlan * RecRecruitmentPlans;
@property (retain) NSString * StatusName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecRecruitmentRequirement : NSObject {
	
/* elements */
	NSMutableArray *RecRecruitmentRequirement;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecRecruitmentRequirement *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRecruitmentRequirement:(emp_tns1_RecRecruitmentRequirement *)toAdd;
@property (readonly) NSMutableArray * RecRecruitmentRequirement;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecRecruitmentPlan : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * PlanDescription;
	emp_tns1_ArrayOfRecPlanApproveHistory * RecPlanApproveHistories;
	NSNumber * RecPlanApproveHistoryId;
	emp_tns1_ArrayOfRecPlanJobPosition * RecPlanJobPositions;
	emp_tns1_RecPlanStatu * RecPlanStatu;
	NSNumber * RecPlanStatusId;
	NSString * RecRecruitmentPlanCode;
	NSNumber * RecRecruitmentPlanId;
	emp_tns1_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
	NSDate * StartDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecRecruitmentPlan *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * PlanDescription;
@property (retain) emp_tns1_ArrayOfRecPlanApproveHistory * RecPlanApproveHistories;
@property (retain) NSNumber * RecPlanApproveHistoryId;
@property (retain) emp_tns1_ArrayOfRecPlanJobPosition * RecPlanJobPositions;
@property (retain) emp_tns1_RecPlanStatu * RecPlanStatu;
@property (retain) NSNumber * RecPlanStatusId;
@property (retain) NSString * RecRecruitmentPlanCode;
@property (retain) NSNumber * RecRecruitmentPlanId;
@property (retain) emp_tns1_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
@property (retain) NSDate * StartDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SysRecRequirementApprover : NSObject {
	
/* elements */
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsMainApprover;
	NSNumber * Priority;
	emp_tns1_ArrayOfRecRequirementApproveHistory * RecRequirementApproveHistories;
	NSNumber * SysRecRecruitmentApproverId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SysRecRequirementApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainApprover;
@property (retain) NSNumber * Priority;
@property (retain) emp_tns1_ArrayOfRecRequirementApproveHistory * RecRequirementApproveHistories;
@property (retain) NSNumber * SysRecRecruitmentApproverId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecRequirementApproveHistory : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * Date;
	USBoolean * IsApproved;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * RecRecruitmentApproveHistoryId;
	emp_tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
	NSNumber * RecRequirementId;
	NSString * Remark;
	NSNumber * SysRecRecruitmentApproverId;
	emp_tns1_SysRecRequirementApprover * SysRecRequirementApprover;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecRequirementApproveHistory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * Date;
@property (retain) USBoolean * IsApproved;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * RecRecruitmentApproveHistoryId;
@property (retain) emp_tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
@property (retain) NSNumber * RecRequirementId;
@property (retain) NSString * Remark;
@property (retain) NSNumber * SysRecRecruitmentApproverId;
@property (retain) emp_tns1_SysRecRequirementApprover * SysRecRequirementApprover;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecRequirementApproveHistory : NSObject {
	
/* elements */
	NSMutableArray *RecRequirementApproveHistory;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecRequirementApproveHistory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRequirementApproveHistory:(emp_tns1_RecRequirementApproveHistory *)toAdd;
@property (readonly) NSMutableArray * RecRequirementApproveHistory;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecRequirementEmpDisplaced : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSString * Reason;
	emp_tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
	NSNumber * RecRecruitmentRequirementId;
	NSNumber * RecRequirementEmpDisplaced1;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecRequirementEmpDisplaced *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * Reason;
@property (retain) emp_tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
@property (retain) NSNumber * RecRecruitmentRequirementId;
@property (retain) NSNumber * RecRequirementEmpDisplaced1;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecRequirementEmpDisplaced : NSObject {
	
/* elements */
	NSMutableArray *RecRequirementEmpDisplaced;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecRequirementEmpDisplaced *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRequirementEmpDisplaced:(emp_tns1_RecRequirementEmpDisplaced *)toAdd;
@property (readonly) NSMutableArray * RecRequirementEmpDisplaced;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecRequirementJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	USBoolean * IsDisplacement;
	NSString * JobDescription;
	NSNumber * JobDescriptionId;
	NSDate * ModifiedDate;
	emp_tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSString * RecReason;
	emp_tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
	NSNumber * RecRecruitmentRequirementId;
	NSNumber * RecRequirementJobPositionId;
	NSNumber * RecruitedQuantity;
	NSNumber * RequirementQuantity;
	NSString * WorkingLocation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecRequirementJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDisplacement;
@property (retain) NSString * JobDescription;
@property (retain) NSNumber * JobDescriptionId;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * RecReason;
@property (retain) emp_tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
@property (retain) NSNumber * RecRecruitmentRequirementId;
@property (retain) NSNumber * RecRequirementJobPositionId;
@property (retain) NSNumber * RecruitedQuantity;
@property (retain) NSNumber * RequirementQuantity;
@property (retain) NSString * WorkingLocation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecRequirementJobPosition : NSObject {
	
/* elements */
	NSMutableArray *RecRequirementJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecRequirementJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRequirementJobPosition:(emp_tns1_RecRequirementJobPosition *)toAdd;
@property (readonly) NSMutableArray * RecRequirementJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecRequirementStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	emp_tns1_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
	NSNumber * RecRequirementStatusId;
	NSString * StatusName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecRequirementStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) emp_tns1_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
@property (retain) NSNumber * RecRequirementStatusId;
@property (retain) NSString * StatusName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecRecruitmentRequirement : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgUnitId;
	NSDate * RecBeginDate;
	NSDate * RecEndDate;
	NSNumber * RecRecruitmentApproveHistoryId;
	emp_tns1_ArrayOfRecRecruitmentPhase * RecRecruitmentPhases;
	emp_tns1_RecRecruitmentPlan * RecRecruitmentPlan;
	NSNumber * RecRecruitmentPlanId;
	NSNumber * RecRecruitmentRequirementId;
	emp_tns1_ArrayOfRecRequirementApproveHistory * RecRequirementApproveHistories;
	emp_tns1_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
	emp_tns1_ArrayOfRecRequirementJobPosition * RecRequirementJobPositions;
	emp_tns1_RecRequirementStatu * RecRequirementStatu;
	NSNumber * RecRequirementStatusId;
	NSNumber * RecruitedQuantity;
	NSString * RecruitmentPurpose;
	NSString * Remark;
	NSString * RequirementCode;
	NSDate * RequirementDate;
	NSString * RequirementName;
	NSNumber * TotalQuantity;
	NSNumber * UrgentLevel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecRecruitmentRequirement *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSDate * RecBeginDate;
@property (retain) NSDate * RecEndDate;
@property (retain) NSNumber * RecRecruitmentApproveHistoryId;
@property (retain) emp_tns1_ArrayOfRecRecruitmentPhase * RecRecruitmentPhases;
@property (retain) emp_tns1_RecRecruitmentPlan * RecRecruitmentPlan;
@property (retain) NSNumber * RecRecruitmentPlanId;
@property (retain) NSNumber * RecRecruitmentRequirementId;
@property (retain) emp_tns1_ArrayOfRecRequirementApproveHistory * RecRequirementApproveHistories;
@property (retain) emp_tns1_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
@property (retain) emp_tns1_ArrayOfRecRequirementJobPosition * RecRequirementJobPositions;
@property (retain) emp_tns1_RecRequirementStatu * RecRequirementStatu;
@property (retain) NSNumber * RecRequirementStatusId;
@property (retain) NSNumber * RecruitedQuantity;
@property (retain) NSString * RecruitmentPurpose;
@property (retain) NSString * Remark;
@property (retain) NSString * RequirementCode;
@property (retain) NSDate * RequirementDate;
@property (retain) NSString * RequirementName;
@property (retain) NSNumber * TotalQuantity;
@property (retain) NSNumber * UrgentLevel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecRecruitmentPhase : NSObject {
	
/* elements */
	NSDate * ApplicationDate;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	USBoolean * IsNotified;
	NSDate * ModifiedDate;
	NSString * PhaseName;
	emp_tns1_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
	emp_tns1_RecPhaseStatu * RecPhaseStatu;
	NSNumber * RecPhaseStatusId;
	NSString * RecRecruitmentPhaseCode;
	NSNumber * RecRecruitmentPhaseId;
	emp_tns1_ArrayOfRecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPositions;
	emp_tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
	NSNumber * RecRecruitmentRequirementId;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecRecruitmentPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ApplicationDate;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsNotified;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * PhaseName;
@property (retain) emp_tns1_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
@property (retain) emp_tns1_RecPhaseStatu * RecPhaseStatu;
@property (retain) NSNumber * RecPhaseStatusId;
@property (retain) NSString * RecRecruitmentPhaseCode;
@property (retain) NSNumber * RecRecruitmentPhaseId;
@property (retain) emp_tns1_ArrayOfRecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPositions;
@property (retain) emp_tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
@property (retain) NSNumber * RecRecruitmentRequirementId;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_RecPhaseEmpDisplaced : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSString * Reason;
	NSNumber * RecPhaseEmpDisplaced1;
	emp_tns1_RecRecruitmentPhase * RecRecruitmentPhase;
	NSNumber * RecRecruitmentPhaseId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_RecPhaseEmpDisplaced *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * Reason;
@property (retain) NSNumber * RecPhaseEmpDisplaced1;
@property (retain) emp_tns1_RecRecruitmentPhase * RecRecruitmentPhase;
@property (retain) NSNumber * RecRecruitmentPhaseId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfRecPhaseEmpDisplaced : NSObject {
	
/* elements */
	NSMutableArray *RecPhaseEmpDisplaced;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfRecPhaseEmpDisplaced *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecPhaseEmpDisplaced:(emp_tns1_RecPhaseEmpDisplaced *)toAdd;
@property (readonly) NSMutableArray * RecPhaseEmpDisplaced;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	emp_tns1_ArrayOfEmpContract * EmpContracts;
	emp_tns1_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
	emp_tns1_OrgJob * OrgJob;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	emp_tns1_ArrayOfOrgJobPositionRequiredProficency * OrgJobPositionRequiredProficencies;
	emp_tns1_ArrayOfOrgJobPositionSpecificCompetency * OrgJobPositionSpecificCompetencies;
	NSString * OrgJobPositionTitle;
	NSString * OrgJobTitleCode;
	NSNumber * OrgJobTitleId;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgWorkLevelCode;
	NSNumber * OrgWorkLevelId;
	emp_tns1_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
	emp_tns1_ArrayOfRecPlanJobPosition * RecPlanJobPositions;
	emp_tns1_ArrayOfRecProcessApplyForJobPosition * RecProcessApplyForJobPositions;
	emp_tns1_ArrayOfRecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPositions;
	emp_tns1_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
	emp_tns1_ArrayOfRecRequirementJobPosition * RecRequirementJobPositions;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_ArrayOfEmpContract * EmpContracts;
@property (retain) emp_tns1_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) emp_tns1_OrgJob * OrgJob;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) emp_tns1_ArrayOfOrgJobPositionRequiredProficency * OrgJobPositionRequiredProficencies;
@property (retain) emp_tns1_ArrayOfOrgJobPositionSpecificCompetency * OrgJobPositionSpecificCompetencies;
@property (retain) NSString * OrgJobPositionTitle;
@property (retain) NSString * OrgJobTitleCode;
@property (retain) NSNumber * OrgJobTitleId;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) emp_tns1_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
@property (retain) emp_tns1_ArrayOfRecPlanJobPosition * RecPlanJobPositions;
@property (retain) emp_tns1_ArrayOfRecProcessApplyForJobPosition * RecProcessApplyForJobPositions;
@property (retain) emp_tns1_ArrayOfRecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPositions;
@property (retain) emp_tns1_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
@property (retain) emp_tns1_ArrayOfRecRequirementJobPosition * RecRequirementJobPositions;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfOrgJobPosition : NSObject {
	
/* elements */
	NSMutableArray *OrgJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfOrgJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgJobPosition:(emp_tns1_OrgJobPosition *)toAdd;
@property (readonly) NSMutableArray * OrgJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgUnitJob : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_OrgJob * OrgJob;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSNumber * OrgUnitJobId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgUnitJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_OrgJob * OrgJob;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * OrgUnitJobId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfOrgUnitJob : NSObject {
	
/* elements */
	NSMutableArray *OrgUnitJob;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfOrgUnitJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgUnitJob:(emp_tns1_OrgUnitJob *)toAdd;
@property (readonly) NSMutableArray * OrgUnitJob;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgJob : NSObject {
	
/* elements */
	NSString * CLogCareerCode;
	NSNumber * CLogCareerId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_ArrayOfEmpContract * EmpContracts;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	emp_tns1_ArrayOfOrgJobPosition * OrgJobPositions;
	emp_tns1_ArrayOfOrgJobSpecificCompetency * OrgJobSpecificCompetencies;
	emp_tns1_ArrayOfOrgUnitJob * OrgUnitJobs;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogCareerCode;
@property (retain) NSNumber * CLogCareerId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_ArrayOfEmpContract * EmpContracts;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) emp_tns1_ArrayOfOrgJobPosition * OrgJobPositions;
@property (retain) emp_tns1_ArrayOfOrgJobSpecificCompetency * OrgJobSpecificCompetencies;
@property (retain) emp_tns1_ArrayOfOrgUnitJob * OrgUnitJobs;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgJobSpecificCompetency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_OrgCompetency * OrgCompetency;
	NSNumber * OrgCompetencyId;
	emp_tns1_OrgJob * OrgJob;
	NSNumber * OrgJobId;
	NSNumber * OrgJobSpecificCompetencyId;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgJobSpecificCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_OrgCompetency * OrgCompetency;
@property (retain) NSNumber * OrgCompetencyId;
@property (retain) emp_tns1_OrgJob * OrgJob;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSNumber * OrgJobSpecificCompetencyId;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfOrgJobSpecificCompetency : NSObject {
	
/* elements */
	NSMutableArray *OrgJobSpecificCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfOrgJobSpecificCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgJobSpecificCompetency:(emp_tns1_OrgJobSpecificCompetency *)toAdd;
@property (readonly) NSMutableArray * OrgJobSpecificCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfOrgProficencyLevel : NSObject {
	
/* elements */
	NSMutableArray *OrgProficencyLevel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfOrgProficencyLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgProficencyLevel:(emp_tns1_OrgProficencyLevel *)toAdd;
@property (readonly) NSMutableArray * OrgProficencyLevel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgCompetency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Definition;
	USBoolean * IsCore;
	USBoolean * IsDeleted;
	emp_tns1_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
	emp_tns1_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
	emp_tns1_ArrayOfLMSTopicCompetency * LMSTopicCompetencies;
	NSDate * ModifiedDate;
	NSString * Name;
	emp_tns1_OrgCompetencyGroup * OrgCompetencyGroup;
	NSNumber * OrgCompetencyGroupId;
	NSNumber * OrgCompetencyId;
	emp_tns1_ArrayOfOrgJobSpecificCompetency * OrgJobSpecificCompetencies;
	emp_tns1_ArrayOfOrgProficencyLevel * OrgProficencyLevels;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Definition;
@property (retain) USBoolean * IsCore;
@property (retain) USBoolean * IsDeleted;
@property (retain) emp_tns1_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
@property (retain) emp_tns1_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
@property (retain) emp_tns1_ArrayOfLMSTopicCompetency * LMSTopicCompetencies;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) emp_tns1_OrgCompetencyGroup * OrgCompetencyGroup;
@property (retain) NSNumber * OrgCompetencyGroupId;
@property (retain) NSNumber * OrgCompetencyId;
@property (retain) emp_tns1_ArrayOfOrgJobSpecificCompetency * OrgJobSpecificCompetencies;
@property (retain) emp_tns1_ArrayOfOrgProficencyLevel * OrgProficencyLevels;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgProficencyLevel : NSObject {
	
/* elements */
	emp_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompetencyLevelName;
	NSDate * CreatedDate;
	emp_tns1_ArrayOfEmpCompetency * EmpCompetencies;
	emp_tns1_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_OrgCompetency * OrgCompetency;
	NSNumber * OrgCompetencyId;
	emp_tns1_ArrayOfOrgJobPositionSpecificCompetency * OrgJobPositionSpecificCompetencies;
	emp_tns1_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
	NSNumber * OrgProficencyLevelId;
	emp_tns1_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
	NSString * Statement;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgProficencyLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompetencyLevelName;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_ArrayOfEmpCompetency * EmpCompetencies;
@property (retain) emp_tns1_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_OrgCompetency * OrgCompetency;
@property (retain) NSNumber * OrgCompetencyId;
@property (retain) emp_tns1_ArrayOfOrgJobPositionSpecificCompetency * OrgJobPositionSpecificCompetencies;
@property (retain) emp_tns1_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) emp_tns1_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
@property (retain) NSString * Statement;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProficencyLevelRating : NSObject {
	
/* elements */
	NSDate * AppraisalDate;
	emp_tns1_CLogRating * CLogRating;
	NSDate * CreatedDate;
	emp_tns1_EmpCompetencyRating * EmpCompetencyRating;
	NSNumber * EmpCompetencyRatingId;
	emp_tns1_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
	NSNumber * EmpProficencyLevelRatingId;
	NSString * ManagerNote;
	NSNumber * ManagerRating;
	emp_tns1_OrgProficencyLevel * OrgProficencyLevel;
	NSNumber * OrgProficencyLevelId;
	NSNumber * PeriodicallyAssessment;
	NSNumber * Score;
	NSString * SelfNote;
	NSNumber * SelfRating;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProficencyLevelRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AppraisalDate;
@property (retain) emp_tns1_CLogRating * CLogRating;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_EmpCompetencyRating * EmpCompetencyRating;
@property (retain) NSNumber * EmpCompetencyRatingId;
@property (retain) emp_tns1_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
@property (retain) NSNumber * EmpProficencyLevelRatingId;
@property (retain) NSString * ManagerNote;
@property (retain) NSNumber * ManagerRating;
@property (retain) emp_tns1_OrgProficencyLevel * OrgProficencyLevel;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) NSNumber * PeriodicallyAssessment;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfNote;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProficencyLevelDetailRating : NSObject {
	
/* elements */
	NSDate * AppraisalDate;
	emp_tns1_CLogRating * CLogRating;
	NSDate * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
	NSNumber * EmpProficencyLevelDetailRatingId;
	emp_tns1_EmpProficencyLevelRating * EmpProficencyLevelRating;
	NSNumber * EmpProficencyLevelRatingId;
	NSString * ManagerNote;
	NSNumber * ManagerRating;
	emp_tns1_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
	NSNumber * OrgProficencyLevelDetailId;
	NSNumber * PeriodicallyAssessment;
	NSNumber * Score;
	NSString * SelfNote;
	NSNumber * SelfRatingId;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProficencyLevelDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AppraisalDate;
@property (retain) emp_tns1_CLogRating * CLogRating;
@property (retain) NSDate * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
@property (retain) NSNumber * EmpProficencyLevelDetailRatingId;
@property (retain) emp_tns1_EmpProficencyLevelRating * EmpProficencyLevelRating;
@property (retain) NSNumber * EmpProficencyLevelRatingId;
@property (retain) NSString * ManagerNote;
@property (retain) NSNumber * ManagerRating;
@property (retain) emp_tns1_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) NSNumber * PeriodicallyAssessment;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfNote;
@property (retain) NSNumber * SelfRatingId;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProficencyDetailRating : NSObject {
	
/* elements */
	NSDate * AppraisalDate;
	emp_tns1_CLogRating * CLogRating;
	NSDate * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpProficencyDetailRatingId;
	emp_tns1_EmpProficencyLevelDetailRating * EmpProficencyLevelDetailRating;
	NSNumber * EmpProficencyLevelDetailRatingId;
	NSString * ManagerNote;
	NSNumber * ManagerRating;
	emp_tns1_OrgProficencyDetail * OrgProficencyDetail;
	NSNumber * OrgProficencyDetailId;
	NSNumber * PeriodicallyAssessment;
	NSString * SelfNote;
	NSNumber * SelfRating;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProficencyDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AppraisalDate;
@property (retain) emp_tns1_CLogRating * CLogRating;
@property (retain) NSDate * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProficencyDetailRatingId;
@property (retain) emp_tns1_EmpProficencyLevelDetailRating * EmpProficencyLevelDetailRating;
@property (retain) NSNumber * EmpProficencyLevelDetailRatingId;
@property (retain) NSString * ManagerNote;
@property (retain) NSNumber * ManagerRating;
@property (retain) emp_tns1_OrgProficencyDetail * OrgProficencyDetail;
@property (retain) NSNumber * OrgProficencyDetailId;
@property (retain) NSNumber * PeriodicallyAssessment;
@property (retain) NSString * SelfNote;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProficencyDetailRating : NSObject {
	
/* elements */
	NSMutableArray *EmpProficencyDetailRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProficencyDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProficencyDetailRating:(emp_tns1_EmpProficencyDetailRating *)toAdd;
@property (readonly) NSMutableArray * EmpProficencyDetailRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileWorkingExperience : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	emp_tns1_CLogRating * CLogRating;
	NSNumber * CompanyId;
	NSString * CompanyName;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileWorkingExperienceId;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	USBoolean * IsInternal;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * NotableAchievements;
	NSString * Note;
	NSNumber * OrgProjectTypeId;
	NSNumber * OrgTimeInChargeId;
	NSString * ProjectName;
	NSNumber * RatingId;
	NSNumber * RelateRowId;
	NSString * Responsibilities;
	NSString * Role;
	NSDate * StartDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileWorkingExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) emp_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompanyName;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileWorkingExperienceId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInternal;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * NotableAchievements;
@property (retain) NSString * Note;
@property (retain) NSNumber * OrgProjectTypeId;
@property (retain) NSNumber * OrgTimeInChargeId;
@property (retain) NSString * ProjectName;
@property (retain) NSNumber * RatingId;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * Responsibilities;
@property (retain) NSString * Role;
@property (retain) NSDate * StartDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileWorkingExperience : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileWorkingExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileWorkingExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileWorkingExperience:(emp_tns1_EmpProfileWorkingExperience *)toAdd;
@property (readonly) NSMutableArray * EmpProfileWorkingExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgCoreCompetency : NSObject {
	
/* elements */
	emp_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompetencyLevel;
	NSDate * CreatedDate;
	NSString * Definition;
	emp_tns1_ArrayOfEmpCompetency * EmpCompetencies;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * OrgCoreCompetencyId;
	NSString * Statement;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgCoreCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompetencyLevel;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Definition;
@property (retain) emp_tns1_ArrayOfEmpCompetency * EmpCompetencies;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * OrgCoreCompetencyId;
@property (retain) NSString * Statement;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfOrgCoreCompetency : NSObject {
	
/* elements */
	NSMutableArray *OrgCoreCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfOrgCoreCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgCoreCompetency:(emp_tns1_OrgCoreCompetency *)toAdd;
@property (readonly) NSMutableArray * OrgCoreCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogRating : NSObject {
	
/* elements */
	NSNumber * CLogRatingId;
	NSString * Description;
	emp_tns1_ArrayOfEmpCompetency * EmpCompetencies;
	emp_tns1_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
	emp_tns1_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
	emp_tns1_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
	emp_tns1_ArrayOfEmpProfileSkill * EmpProfileSkills;
	emp_tns1_ArrayOfEmpProfileWorkingExperience * EmpProfileWorkingExperiences;
	USBoolean * IsDeleted;
	emp_tns1_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
	emp_tns1_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
	emp_tns1_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
	emp_tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
	emp_tns1_ArrayOfOrgCoreCompetency * OrgCoreCompetencies;
	emp_tns1_ArrayOfOrgProficencyDetail * OrgProficencyDetails;
	emp_tns1_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
	emp_tns1_ArrayOfOrgProficencyLevel * OrgProficencyLevels;
	emp_tns1_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
	emp_tns1_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
	emp_tns1_ArrayOfRecCandidateCompetencyRating * RecCandidateCompetencyRatings;
	emp_tns1_ArrayOfRecCandidateExperience * RecCandidateExperiences;
	emp_tns1_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
	emp_tns1_ArrayOfRecCandidateQualification * RecCandidateQualifications;
	emp_tns1_ArrayOfRecCandidateSkill * RecCandidateSkills;
	emp_tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
	emp_tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies1;
	emp_tns1_ArrayOfTrainingProficencyExpected * TrainingProficencyExpecteds;
	emp_tns1_ArrayOfTrainingProficencyRequire * TrainingProficencyRequires;
	NSNumber * Value;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSString * Description;
@property (retain) emp_tns1_ArrayOfEmpCompetency * EmpCompetencies;
@property (retain) emp_tns1_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
@property (retain) emp_tns1_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
@property (retain) emp_tns1_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
@property (retain) emp_tns1_ArrayOfEmpProfileSkill * EmpProfileSkills;
@property (retain) emp_tns1_ArrayOfEmpProfileWorkingExperience * EmpProfileWorkingExperiences;
@property (retain) USBoolean * IsDeleted;
@property (retain) emp_tns1_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
@property (retain) emp_tns1_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
@property (retain) emp_tns1_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
@property (retain) emp_tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
@property (retain) emp_tns1_ArrayOfOrgCoreCompetency * OrgCoreCompetencies;
@property (retain) emp_tns1_ArrayOfOrgProficencyDetail * OrgProficencyDetails;
@property (retain) emp_tns1_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
@property (retain) emp_tns1_ArrayOfOrgProficencyLevel * OrgProficencyLevels;
@property (retain) emp_tns1_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
@property (retain) emp_tns1_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
@property (retain) emp_tns1_ArrayOfRecCandidateCompetencyRating * RecCandidateCompetencyRatings;
@property (retain) emp_tns1_ArrayOfRecCandidateExperience * RecCandidateExperiences;
@property (retain) emp_tns1_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
@property (retain) emp_tns1_ArrayOfRecCandidateQualification * RecCandidateQualifications;
@property (retain) emp_tns1_ArrayOfRecCandidateSkill * RecCandidateSkills;
@property (retain) emp_tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
@property (retain) emp_tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies1;
@property (retain) emp_tns1_ArrayOfTrainingProficencyExpected * TrainingProficencyExpecteds;
@property (retain) emp_tns1_ArrayOfTrainingProficencyRequire * TrainingProficencyRequires;
@property (retain) NSNumber * Value;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpCompetency : NSObject {
	
/* elements */
	emp_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpCompetencyId;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_OrgCoreCompetency * OrgCoreCompetency;
	NSNumber * OrgCoreCompetencyId;
	emp_tns1_OrgProficencyLevel * OrgProficencyLevel;
	NSNumber * OrgProficencyLevelId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpCompetencyId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_OrgCoreCompetency * OrgCoreCompetency;
@property (retain) NSNumber * OrgCoreCompetencyId;
@property (retain) emp_tns1_OrgProficencyLevel * OrgProficencyLevel;
@property (retain) NSNumber * OrgProficencyLevelId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpCompetency : NSObject {
	
/* elements */
	NSMutableArray *EmpCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpCompetency:(emp_tns1_EmpCompetency *)toAdd;
@property (readonly) NSMutableArray * EmpCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpCompetencyRating : NSObject {
	
/* elements */
	NSMutableArray *EmpCompetencyRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpCompetencyRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpCompetencyRating:(emp_tns1_EmpCompetencyRating *)toAdd;
@property (readonly) NSMutableArray * EmpCompetencyRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpPerformanceAppraisal : NSObject {
	
/* elements */
	NSDate * AppraisalDate;
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * Comment;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpPerformanceAppraisalId;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsEndYear;
	USBoolean * IsHalfYear;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * RelateRowId;
	NSNumber * Score;
	NSString * SelfComment;
	NSNumber * SelfRating;
	NSNumber * SelfScore;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpPerformanceAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AppraisalDate;
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpPerformanceAppraisalId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEndYear;
@property (retain) USBoolean * IsHalfYear;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * SelfScore;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpPerformanceAppraisal : NSObject {
	
/* elements */
	NSMutableArray *EmpPerformanceAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpPerformanceAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpPerformanceAppraisal:(emp_tns1_EmpPerformanceAppraisal *)toAdd;
@property (readonly) NSMutableArray * EmpPerformanceAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileAllowance : NSObject {
	
/* elements */
	NSDate * ActivedDate;
	NSString * AllowanceCode;
	NSString * AllowanceName;
	NSNumber * AllowanceValue;
	NSNumber * CLogCurrency;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpProfileAllowanceId;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsActived;
	USBoolean * IsDeactived;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileAllowance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ActivedDate;
@property (retain) NSString * AllowanceCode;
@property (retain) NSString * AllowanceName;
@property (retain) NSNumber * AllowanceValue;
@property (retain) NSNumber * CLogCurrency;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileAllowanceId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeactived;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileAllowance : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileAllowance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileAllowance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileAllowance:(emp_tns1_EmpProfileAllowance *)toAdd;
@property (readonly) NSMutableArray * EmpProfileAllowance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileBaseSalary : NSObject {
	
/* elements */
	NSDate * ActivedDate;
	NSString * BasicPayCode;
	NSNumber * CBSalaryCofficientId;
	NSNumber * CBSalaryGradeId;
	NSNumber * CBSalaryLevelId;
	NSNumber * CBSalaryScaleId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpProfileBaseSalaryId;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsActived;
	USBoolean * IsDeactived;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * NonPayScaleSalaryValue;
	NSString * Note;
	NSNumber * PayScaleSalaryValue;
	NSString * SalaryCode;
	NSNumber * SalaryId;
	NSString * SalaryName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileBaseSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ActivedDate;
@property (retain) NSString * BasicPayCode;
@property (retain) NSNumber * CBSalaryCofficientId;
@property (retain) NSNumber * CBSalaryGradeId;
@property (retain) NSNumber * CBSalaryLevelId;
@property (retain) NSNumber * CBSalaryScaleId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileBaseSalaryId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeactived;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NonPayScaleSalaryValue;
@property (retain) NSString * Note;
@property (retain) NSNumber * PayScaleSalaryValue;
@property (retain) NSString * SalaryCode;
@property (retain) NSNumber * SalaryId;
@property (retain) NSString * SalaryName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileBaseSalary : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileBaseSalary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileBaseSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileBaseSalary:(emp_tns1_EmpProfileBaseSalary *)toAdd;
@property (readonly) NSMutableArray * EmpProfileBaseSalary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileBenefit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileBenefitId;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * HealthInsuranceDateExpire;
	NSDate * HealthInsuranceDateIssue;
	NSNumber * HealthInsuranceId;
	NSString * HealthInsuranceNumber;
	NSString * HealthInsuranceRegPlace;
	USBoolean * IsDeleted;
	NSDate * LabourBookDateOfIssue;
	NSString * LabourBookNo;
	NSString * LabourBookPlaceOfIssue;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * OtherInsuranceCompanyName;
	NSDate * OtherInsuranceDateExpire;
	NSDate * OtherInsuranceDateIssue;
	NSString * OtherInsuranceNumber;
	NSDate * SocialInsuranceDateIssue;
	NSNumber * SocialInsuranceId;
	NSNumber * SocialInsuranceJoiningBefore;
	NSString * SocialInsuranceNumber;
	NSDate * UnemploymentInsuranceDateIssue;
	NSNumber * UnemploymentInsuranceId;
	NSString * UnemploymentInsuranceNumber;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileBenefitId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * HealthInsuranceDateExpire;
@property (retain) NSDate * HealthInsuranceDateIssue;
@property (retain) NSNumber * HealthInsuranceId;
@property (retain) NSString * HealthInsuranceNumber;
@property (retain) NSString * HealthInsuranceRegPlace;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * LabourBookDateOfIssue;
@property (retain) NSString * LabourBookNo;
@property (retain) NSString * LabourBookPlaceOfIssue;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OtherInsuranceCompanyName;
@property (retain) NSDate * OtherInsuranceDateExpire;
@property (retain) NSDate * OtherInsuranceDateIssue;
@property (retain) NSString * OtherInsuranceNumber;
@property (retain) NSDate * SocialInsuranceDateIssue;
@property (retain) NSNumber * SocialInsuranceId;
@property (retain) NSNumber * SocialInsuranceJoiningBefore;
@property (retain) NSString * SocialInsuranceNumber;
@property (retain) NSDate * UnemploymentInsuranceDateIssue;
@property (retain) NSNumber * UnemploymentInsuranceId;
@property (retain) NSString * UnemploymentInsuranceNumber;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileBenefit : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileBenefit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileBenefit:(emp_tns1_EmpProfileBenefit *)toAdd;
@property (readonly) NSMutableArray * EmpProfileBenefit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileComment : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSString * Content;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfileCommentId;
	emp_tns1_Employee * Employee;
	emp_tns1_Employee * Employee1;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * RelateRowId;
	NSString * ReviewerCode;
	NSNumber * ReviewerId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileComment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Content;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfileCommentId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) emp_tns1_Employee * Employee1;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * ReviewerCode;
@property (retain) NSNumber * ReviewerId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileComment : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileComment;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileComment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileComment:(emp_tns1_EmpProfileComment *)toAdd;
@property (readonly) NSMutableArray * EmpProfileComment;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileComputingSkill : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSString * ComputingSkillName;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	NSNumber * EmpProfileComputingSkillId;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsValid;
	NSDate * ModifiedDate;
	NSString * Other;
	NSString * PlaceIssue;
	NSNumber * RatingId;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileComputingSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ComputingSkillName;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) NSNumber * EmpProfileComputingSkillId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValid;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) NSNumber * RatingId;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileComputingSkill : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileComputingSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileComputingSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileComputingSkill:(emp_tns1_EmpProfileComputingSkill *)toAdd;
@property (readonly) NSMutableArray * EmpProfileComputingSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileContact : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSString * ContactAddress;
	NSNumber * ContactAddressId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * Email;
	NSNumber * EmpProfileContactId;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSString * Fax;
	NSString * HomePhone;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	NSString * MobileNumber;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * OfficePhone;
	NSString * PermanentAddress;
	NSNumber * PermanentAddressId;
	NSNumber * RelateRowId;
	NSString * TemporaryAddress;
	NSNumber * TemporaryAddressId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileContact *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ContactAddress;
@property (retain) NSNumber * ContactAddressId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Email;
@property (retain) NSNumber * EmpProfileContactId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * Fax;
@property (retain) NSString * HomePhone;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * MobileNumber;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OfficePhone;
@property (retain) NSString * PermanentAddress;
@property (retain) NSNumber * PermanentAddressId;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * TemporaryAddress;
@property (retain) NSNumber * TemporaryAddressId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileContact : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileContact;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileContact *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileContact:(emp_tns1_EmpProfileContact *)toAdd;
@property (readonly) NSMutableArray * EmpProfileContact;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgDegree : NSObject {
	
/* elements */
	NSNumber * CLogMajorId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * Description;
	emp_tns1_ArrayOfEmpProfileDegree * EmpProfileDegrees;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * OrgDegreeId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) emp_tns1_ArrayOfEmpProfileDegree * EmpProfileDegrees;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * OrgDegreeId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileDegree : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	NSNumber * EmpProfileDegreeId;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsValId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	emp_tns1_OrgDegree * OrgDegree;
	NSNumber * OrgDegreeId;
	NSNumber * OrgDegreeRankId;
	NSString * Other;
	NSString * PlaceIssue;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) NSNumber * EmpProfileDegreeId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_OrgDegree * OrgDegree;
@property (retain) NSNumber * OrgDegreeId;
@property (retain) NSNumber * OrgDegreeRankId;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileDegree : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileDegree;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileDegree:(emp_tns1_EmpProfileDegree *)toAdd;
@property (readonly) NSMutableArray * EmpProfileDegree;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgDisciplineForUnit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgDisciplineForUnitId;
	NSNumber * OrgDisciplineId;
	emp_tns1_OrgDisciplineMaster * OrgDisciplineMaster;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgDisciplineForUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgDisciplineForUnitId;
@property (retain) NSNumber * OrgDisciplineId;
@property (retain) emp_tns1_OrgDisciplineMaster * OrgDisciplineMaster;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfOrgDisciplineForUnit : NSObject {
	
/* elements */
	NSMutableArray *OrgDisciplineForUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfOrgDisciplineForUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgDisciplineForUnit:(emp_tns1_OrgDisciplineForUnit *)toAdd;
@property (readonly) NSMutableArray * OrgDisciplineForUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgDisciplineMaster : NSObject {
	
/* elements */
	NSNumber * ApproveId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSDate * DateOfIssue;
	NSString * DecisionNo;
	emp_tns1_ArrayOfEmpProfileDiscipline * EmpProfileDisciplines;
	USBoolean * IsDeleted;
	USBoolean * IsForEmployee;
	NSDate * ModifiedDate;
	NSNumber * MonetaryValue;
	emp_tns1_ArrayOfOrgDisciplineForUnit * OrgDisciplineForUnits;
	NSNumber * OrgDisciplineMasterId;
	NSString * OrgDisciplineTypeCode;
	USBoolean * Status;
	NSString * Title;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgDisciplineMaster *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproveId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSDate * DateOfIssue;
@property (retain) NSString * DecisionNo;
@property (retain) emp_tns1_ArrayOfEmpProfileDiscipline * EmpProfileDisciplines;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsForEmployee;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * MonetaryValue;
@property (retain) emp_tns1_ArrayOfOrgDisciplineForUnit * OrgDisciplineForUnits;
@property (retain) NSNumber * OrgDisciplineMasterId;
@property (retain) NSString * OrgDisciplineTypeCode;
@property (retain) USBoolean * Status;
@property (retain) NSString * Title;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileDiscipline : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfileDisciplineId;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	emp_tns1_OrgDisciplineMaster * OrgDisciplineMaster;
	NSNumber * OrgDisciplineMasterId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileDiscipline *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfileDisciplineId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_OrgDisciplineMaster * OrgDisciplineMaster;
@property (retain) NSNumber * OrgDisciplineMasterId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileDiscipline : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileDiscipline;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileDiscipline *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileDiscipline:(emp_tns1_EmpProfileDiscipline *)toAdd;
@property (readonly) NSMutableArray * EmpProfileDiscipline;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogEquipment : NSObject {
	
/* elements */
	NSDate * AddDate;
	NSString * CLogAssetsTypeCode;
	NSNumber * CLogCurrencyId;
	NSNumber * CLogEquipmentId;
	NSString * CLogEquipmentTypeCode;
	NSString * Code;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * Depreciation;
	emp_tns1_ArrayOfEmpProfileEquipment * EmpProfileEquipments;
	USBoolean * IsBigValue;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
	NSNumber * Number;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogEquipment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AddDate;
@property (retain) NSString * CLogAssetsTypeCode;
@property (retain) NSNumber * CLogCurrencyId;
@property (retain) NSNumber * CLogEquipmentId;
@property (retain) NSString * CLogEquipmentTypeCode;
@property (retain) NSString * Code;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * Depreciation;
@property (retain) emp_tns1_ArrayOfEmpProfileEquipment * EmpProfileEquipments;
@property (retain) USBoolean * IsBigValue;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) NSNumber * Number;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileEquipment : NSObject {
	
/* elements */
	emp_tns1_CLogEquipment * CLogEquipment;
	NSNumber * CLogEquipmentId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DateHandover;
	NSDate * DateReceived;
	NSString * Description;
	NSNumber * EmpProfileEquipmentId;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * HandoverStatus;
	USBoolean * IsDeleted;
	USBoolean * IsHandover;
	USBoolean * IsReturnToComp;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	USBoolean * MustHandover;
	NSNumber * Number;
	NSString * ReceiverCode;
	NSNumber * ReceiverId;
	NSNumber * RequestDetailId;
	NSString * StateHandover;
	NSString * StateReceived;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileEquipment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_CLogEquipment * CLogEquipment;
@property (retain) NSNumber * CLogEquipmentId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateHandover;
@property (retain) NSDate * DateReceived;
@property (retain) NSString * Description;
@property (retain) NSNumber * EmpProfileEquipmentId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * HandoverStatus;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsHandover;
@property (retain) USBoolean * IsReturnToComp;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) USBoolean * MustHandover;
@property (retain) NSNumber * Number;
@property (retain) NSString * ReceiverCode;
@property (retain) NSNumber * ReceiverId;
@property (retain) NSNumber * RequestDetailId;
@property (retain) NSString * StateHandover;
@property (retain) NSString * StateReceived;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileEquipment : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileEquipment;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileEquipment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileEquipment:(emp_tns1_EmpProfileEquipment *)toAdd;
@property (readonly) NSMutableArray * EmpProfileEquipment;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileExperience : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * AttachedDocumentUrl;
	NSString * Company;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * Customer;
	NSNumber * EmpProfileExperienceId;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * ProjectName;
	NSString * ProjectResult;
	NSNumber * RelateRowId;
	NSString * Roles;
	NSDate * StartDate;
	NSString * TeamSize;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * AttachedDocumentUrl;
@property (retain) NSString * Company;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Customer;
@property (retain) NSNumber * EmpProfileExperienceId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * ProjectName;
@property (retain) NSString * ProjectResult;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * Roles;
@property (retain) NSDate * StartDate;
@property (retain) NSString * TeamSize;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileExperience : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileExperience:(emp_tns1_EmpProfileExperience *)toAdd;
@property (readonly) NSMutableArray * EmpProfileExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogLanguage : NSObject {
	
/* elements */
	NSNumber * CLogLanguageId;
	emp_tns1_ArrayOfEmpProfileForeignLanguage * EmpProfileForeignLanguages;
	USBoolean * IsDeleted;
	NSString * Language;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogLanguageId;
@property (retain) emp_tns1_ArrayOfEmpProfileForeignLanguage * EmpProfileForeignLanguages;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Language;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileForeignLanguage : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * AttachmenttUrl;
	emp_tns1_CLogLanguage * CLogLanguage;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DateOfIssue;
	NSString * DegreeName;
	NSNumber * EffectiveTime;
	NSNumber * EmpProfileForeignLanguageId;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * LanguageId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * OrgDegreeRankId;
	NSString * PlaceOfIssue;
	NSNumber * RelateRowId;
	NSString * TrainingCenter;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileForeignLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * AttachmenttUrl;
@property (retain) emp_tns1_CLogLanguage * CLogLanguage;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateOfIssue;
@property (retain) NSString * DegreeName;
@property (retain) NSNumber * EffectiveTime;
@property (retain) NSNumber * EmpProfileForeignLanguageId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * LanguageId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * OrgDegreeRankId;
@property (retain) NSString * PlaceOfIssue;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * TrainingCenter;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileForeignLanguage : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileForeignLanguage;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileForeignLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileForeignLanguage:(emp_tns1_EmpProfileForeignLanguage *)toAdd;
@property (readonly) NSMutableArray * EmpProfileForeignLanguage;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileHealthInsurance : NSObject {
	
/* elements */
	NSNumber * CLogHospitalId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * HICompanyRate;
	NSNumber * HIEmployeeRate;
	NSDate * HealthInsuranceDateEffect;
	NSDate * HealthInsuranceDateExpire;
	NSNumber * HealthInsuranceId;
	NSString * HealthInsuranceNumber;
	NSDate * ImplementDate;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	USBoolean * IsDifference;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileHealthInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogHospitalId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * HICompanyRate;
@property (retain) NSNumber * HIEmployeeRate;
@property (retain) NSDate * HealthInsuranceDateEffect;
@property (retain) NSDate * HealthInsuranceDateExpire;
@property (retain) NSNumber * HealthInsuranceId;
@property (retain) NSString * HealthInsuranceNumber;
@property (retain) NSDate * ImplementDate;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDifference;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileHealthInsurance : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileHealthInsurance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileHealthInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileHealthInsurance:(emp_tns1_EmpProfileHealthInsurance *)toAdd;
@property (readonly) NSMutableArray * EmpProfileHealthInsurance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileHealthy : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * BloodGroup;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DiseaseAbsenceEndDay;
	NSDate * DiseaseAbsenceStartDay;
	NSNumber * DiseaseBenefit;
	NSString * DiseaseName;
	NSNumber * EmpProfileHealthyId;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	USBoolean * IsDiseaseAbsence;
	USBoolean * IsEnoughHealthToWork;
	USBoolean * IsGoodHealth;
	USBoolean * IsMaternityAbsence;
	USBoolean * IsMedicalHistory;
	USBoolean * IsPeopleWithDisability;
	NSDate * MaternityAbsenceEndDay;
	NSDate * MaternityAbsenceStartDay;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileHealthy *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * BloodGroup;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DiseaseAbsenceEndDay;
@property (retain) NSDate * DiseaseAbsenceStartDay;
@property (retain) NSNumber * DiseaseBenefit;
@property (retain) NSString * DiseaseName;
@property (retain) NSNumber * EmpProfileHealthyId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDiseaseAbsence;
@property (retain) USBoolean * IsEnoughHealthToWork;
@property (retain) USBoolean * IsGoodHealth;
@property (retain) USBoolean * IsMaternityAbsence;
@property (retain) USBoolean * IsMedicalHistory;
@property (retain) USBoolean * IsPeopleWithDisability;
@property (retain) NSDate * MaternityAbsenceEndDay;
@property (retain) NSDate * MaternityAbsenceStartDay;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileHealthy : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileHealthy;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileHealthy *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileHealthy:(emp_tns1_EmpProfileHealthy *)toAdd;
@property (readonly) NSMutableArray * EmpProfileHealthy;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogHobby : NSObject {
	
/* elements */
	NSNumber * CLogHobbyId;
	NSString * Description;
	emp_tns1_ArrayOfEmpProfileHobby * EmpProfileHobbies;
	USBoolean * IsDeleted;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogHobby *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogHobbyId;
@property (retain) NSString * Description;
@property (retain) emp_tns1_ArrayOfEmpProfileHobby * EmpProfileHobbies;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileHobby : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	emp_tns1_CLogHobby * CLogHobby;
	NSNumber * CLogHobbyId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfileHobbyId;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileHobby *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) emp_tns1_CLogHobby * CLogHobby;
@property (retain) NSNumber * CLogHobbyId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfileHobbyId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileHobby : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileHobby;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileHobby *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileHobby:(emp_tns1_EmpProfileHobby *)toAdd;
@property (readonly) NSMutableArray * EmpProfileHobby;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileInternalCourse : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpInternalCourseId;
	NSNumber * EmpProfileInternalCourseId;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Result;
	NSNumber * Status;
	NSString * Support;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileInternalCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpInternalCourseId;
@property (retain) NSNumber * EmpProfileInternalCourseId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Result;
@property (retain) NSNumber * Status;
@property (retain) NSString * Support;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileInternalCourse : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileInternalCourse;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileInternalCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileInternalCourse:(emp_tns1_EmpProfileInternalCourse *)toAdd;
@property (readonly) NSMutableArray * EmpProfileInternalCourse;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileLeaveRegime : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CompensatoryLeave;
	NSNumber * CompensatoryLeaveOfYear;
	NSNumber * CompensatoryRemainedLeave;
	NSNumber * CompensatoryUsedLeave;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileLeaveRegimeId;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * MonthlyLeave;
	NSNumber * PlusMinusLeave;
	NSNumber * PreviousLeave;
	NSNumber * RemainedLeave;
	NSNumber * SeniorityLeave;
	NSNumber * TotalUsedLeave;
	NSNumber * Year;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileLeaveRegime *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CompensatoryLeave;
@property (retain) NSNumber * CompensatoryLeaveOfYear;
@property (retain) NSNumber * CompensatoryRemainedLeave;
@property (retain) NSNumber * CompensatoryUsedLeave;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileLeaveRegimeId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * MonthlyLeave;
@property (retain) NSNumber * PlusMinusLeave;
@property (retain) NSNumber * PreviousLeave;
@property (retain) NSNumber * RemainedLeave;
@property (retain) NSNumber * SeniorityLeave;
@property (retain) NSNumber * TotalUsedLeave;
@property (retain) NSNumber * Year;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileLeaveRegime : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileLeaveRegime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileLeaveRegime *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileLeaveRegime:(emp_tns1_EmpProfileLeaveRegime *)toAdd;
@property (readonly) NSMutableArray * EmpProfileLeaveRegime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpOtherBenefit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	NSNumber * EmpOtherBenefitId;
	emp_tns1_ArrayOfEmpProfileOtherBenefit * EmpProfileOtherBenefits;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpOtherBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) NSNumber * EmpOtherBenefitId;
@property (retain) emp_tns1_ArrayOfEmpProfileOtherBenefit * EmpProfileOtherBenefits;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileOtherBenefit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	emp_tns1_EmpOtherBenefit * EmpOtherBenefit;
	NSNumber * EmpOtherBenefitId;
	NSNumber * EmpProfileOtherBenefitId;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileOtherBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_EmpOtherBenefit * EmpOtherBenefit;
@property (retain) NSNumber * EmpOtherBenefitId;
@property (retain) NSNumber * EmpProfileOtherBenefitId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileOtherBenefit : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileOtherBenefit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileOtherBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileOtherBenefit:(emp_tns1_EmpProfileOtherBenefit *)toAdd;
@property (readonly) NSMutableArray * EmpProfileOtherBenefit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileParticipation : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileParticipationId;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSDate * PartyFeeEndDate;
	NSNumber * PartyFeePercent;
	NSString * PartyFeeRole;
	NSDate * PartyFeeStartDate;
	NSNumber * RelateRowId;
	NSDate * UnionFeeEndDate;
	NSNumber * UnionFeePercent;
	NSString * UnionFeeRole;
	NSDate * UnionFeeStartDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileParticipation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileParticipationId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * PartyFeeEndDate;
@property (retain) NSNumber * PartyFeePercent;
@property (retain) NSString * PartyFeeRole;
@property (retain) NSDate * PartyFeeStartDate;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSDate * UnionFeeEndDate;
@property (retain) NSNumber * UnionFeePercent;
@property (retain) NSString * UnionFeeRole;
@property (retain) NSDate * UnionFeeStartDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileParticipation : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileParticipation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileParticipation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileParticipation:(emp_tns1_EmpProfileParticipation *)toAdd;
@property (readonly) NSMutableArray * EmpProfileParticipation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogPersonality : NSObject {
	
/* elements */
	NSNumber * CLogPersonalityId;
	NSString * Description;
	emp_tns1_ArrayOfEmpProfilePersonality * EmpProfilePersonalities;
	USBoolean * IsDeleted;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogPersonality *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogPersonalityId;
@property (retain) NSString * Description;
@property (retain) emp_tns1_ArrayOfEmpProfilePersonality * EmpProfilePersonalities;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfilePersonality : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	emp_tns1_CLogPersonality * CLogPersonality;
	NSNumber * CLogPersonalityId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfilePersonalityId;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfilePersonality *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) emp_tns1_CLogPersonality * CLogPersonality;
@property (retain) NSNumber * CLogPersonalityId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfilePersonalityId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfilePersonality : NSObject {
	
/* elements */
	NSMutableArray *EmpProfilePersonality;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfilePersonality *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfilePersonality:(emp_tns1_EmpProfilePersonality *)toAdd;
@property (readonly) NSMutableArray * EmpProfilePersonality;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileProcessOfWork : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * DecisionFile;
	NSString * DecisionName;
	NSString * DecisionNo;
	NSNumber * EmpProfileJobPositionId;
	NSNumber * EmpProfileProcessOfWorkId;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ExpirationDate;
	NSDate * ImplementationDate;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * OrgUnitAddress;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgUnitName;
	NSString * OrgUnitNameEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileProcessOfWork *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DecisionFile;
@property (retain) NSString * DecisionName;
@property (retain) NSString * DecisionNo;
@property (retain) NSNumber * EmpProfileJobPositionId;
@property (retain) NSNumber * EmpProfileProcessOfWorkId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OrgUnitAddress;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitName;
@property (retain) NSString * OrgUnitNameEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileProcessOfWork : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileProcessOfWork;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileProcessOfWork *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileProcessOfWork:(emp_tns1_EmpProfileProcessOfWork *)toAdd;
@property (readonly) NSMutableArray * EmpProfileProcessOfWork;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgRewardForUnit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgRewardForUnitId;
	NSNumber * OrgRewardId;
	emp_tns1_OrgRewardMaster * OrgRewardMaster;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgRewardForUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgRewardForUnitId;
@property (retain) NSNumber * OrgRewardId;
@property (retain) emp_tns1_OrgRewardMaster * OrgRewardMaster;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfOrgRewardForUnit : NSObject {
	
/* elements */
	NSMutableArray *OrgRewardForUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfOrgRewardForUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgRewardForUnit:(emp_tns1_OrgRewardForUnit *)toAdd;
@property (readonly) NSMutableArray * OrgRewardForUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgRewardMaster : NSObject {
	
/* elements */
	NSNumber * ApproveId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSDate * DateOfIssue;
	NSString * DecisionNo;
	emp_tns1_ArrayOfEmpProfileReward * EmpProfileRewards;
	USBoolean * IsAllCompany;
	USBoolean * IsDeleted;
	USBoolean * IsForEmployee;
	NSDate * ModifiedDate;
	NSNumber * MonetaryValue;
	emp_tns1_ArrayOfOrgRewardForUnit * OrgRewardForUnits;
	NSNumber * OrgRewardMasterId;
	NSString * OrgRewardTypeCode;
	USBoolean * Status;
	NSString * Title;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgRewardMaster *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproveId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSDate * DateOfIssue;
@property (retain) NSString * DecisionNo;
@property (retain) emp_tns1_ArrayOfEmpProfileReward * EmpProfileRewards;
@property (retain) USBoolean * IsAllCompany;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsForEmployee;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * MonetaryValue;
@property (retain) emp_tns1_ArrayOfOrgRewardForUnit * OrgRewardForUnits;
@property (retain) NSNumber * OrgRewardMasterId;
@property (retain) NSString * OrgRewardTypeCode;
@property (retain) USBoolean * Status;
@property (retain) NSString * Title;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileReward : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfileRewardId;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	emp_tns1_OrgRewardMaster * OrgRewardMaster;
	NSNumber * OrgRewardMasterId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileReward *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfileRewardId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_OrgRewardMaster * OrgRewardMaster;
@property (retain) NSNumber * OrgRewardMasterId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileReward : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileReward;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileReward *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileReward:(emp_tns1_EmpProfileReward *)toAdd;
@property (readonly) NSMutableArray * EmpProfileReward;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileTotalIncome : NSObject {
	
/* elements */
	NSDate * CompanyId;
	NSDate * CreatedDate;
	NSNumber * DependancyDeduction;
	NSNumber * EmpProfileTotalIncomeId;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSNumber * GrossPayment;
	NSDate * ModifiedDate;
	NSNumber * Month;
	NSNumber * NetPayment;
	NSNumber * PIT;
	NSNumber * TaxableAmount;
	NSNumber * TotalDeduction;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileTotalIncome *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * DependancyDeduction;
@property (retain) NSNumber * EmpProfileTotalIncomeId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * GrossPayment;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Month;
@property (retain) NSNumber * NetPayment;
@property (retain) NSNumber * PIT;
@property (retain) NSNumber * TaxableAmount;
@property (retain) NSNumber * TotalDeduction;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileTotalIncome : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileTotalIncome;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileTotalIncome *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileTotalIncome:(emp_tns1_EmpProfileTotalIncome *)toAdd;
@property (readonly) NSMutableArray * EmpProfileTotalIncome;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileTraining : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * AttachedDocumentUrl;
	NSString * Certifications;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileTrainingId;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	NSDate * ExpiredDate;
	USBoolean * IsDeleted;
	USBoolean * IsInternal;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * RelateRowId;
	NSString * Result;
	NSDate * StartDate;
	NSNumber * Status;
	NSString * TrainingProgram;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileTraining *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * AttachedDocumentUrl;
@property (retain) NSString * Certifications;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileTrainingId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) NSDate * ExpiredDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInternal;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * Result;
@property (retain) NSDate * StartDate;
@property (retain) NSNumber * Status;
@property (retain) NSString * TrainingProgram;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileTraining : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileTraining;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileTraining *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileTraining:(emp_tns1_EmpProfileTraining *)toAdd;
@property (readonly) NSMutableArray * EmpProfileTraining;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileWageType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileWageTypeId;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ImplementationDate;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * TimeWageTypeCode;
	NSNumber * TimeWageTypeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileWageType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileWageTypeId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * TimeWageTypeCode;
@property (retain) NSNumber * TimeWageTypeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileWageType : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileWageType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileWageType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileWageType:(emp_tns1_EmpProfileWageType *)toAdd;
@property (readonly) NSMutableArray * EmpProfileWageType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileWorkingForm : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileWorkingFormId;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ImplementationDate;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * TimeWorkingFormCode;
	NSNumber * TimeWorkingFormId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileWorkingForm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileWorkingFormId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * TimeWorkingFormCode;
@property (retain) NSNumber * TimeWorkingFormId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileWorkingForm : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileWorkingForm;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileWorkingForm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileWorkingForm:(emp_tns1_EmpProfileWorkingForm *)toAdd;
@property (readonly) NSMutableArray * EmpProfileWorkingForm;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpSocialInsuranceSalary : NSObject {
	
/* elements */
	NSNumber * CLogCurrencyId;
	NSNumber * CLogCurrencyRateId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * DecisionCode;
	NSString * DecisionName;
	NSNumber * EmpSocialInsuranceSalaryId;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSDate * ExpirationDate;
	NSString * FileDecisionUrl;
	NSDate * ImplementationDate;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	USBoolean * IsIncrease;
	USBoolean * IsRecall;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * Reason;
	NSDate * RecallMonth;
	NSDate * SocialInsuranceDate;
	NSString * cEmpSISalary;
	NSString * cEmpSISalaryCoeficient;
	NSString * cEmpSISalaryGrade;
	NSNumber * nEmpSISalary;
	NSNumber * nEmpSISalaryCoeficient;
	NSNumber * nEmpSISalaryGrade;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpSocialInsuranceSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogCurrencyId;
@property (retain) NSNumber * CLogCurrencyRateId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DecisionCode;
@property (retain) NSString * DecisionName;
@property (retain) NSNumber * EmpSocialInsuranceSalaryId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSString * FileDecisionUrl;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsIncrease;
@property (retain) USBoolean * IsRecall;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * Reason;
@property (retain) NSDate * RecallMonth;
@property (retain) NSDate * SocialInsuranceDate;
@property (retain) NSString * cEmpSISalary;
@property (retain) NSString * cEmpSISalaryCoeficient;
@property (retain) NSString * cEmpSISalaryGrade;
@property (retain) NSNumber * nEmpSISalary;
@property (retain) NSNumber * nEmpSISalaryCoeficient;
@property (retain) NSNumber * nEmpSISalaryGrade;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpSocialInsuranceSalary : NSObject {
	
/* elements */
	NSMutableArray *EmpSocialInsuranceSalary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpSocialInsuranceSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpSocialInsuranceSalary:(emp_tns1_EmpSocialInsuranceSalary *)toAdd;
@property (readonly) NSMutableArray * EmpSocialInsuranceSalary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_NCClientConnection : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * ConnectionId;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSNumber * NCClientConnectionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_NCClientConnection *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ConnectionId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * NCClientConnectionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfNCClientConnection : NSObject {
	
/* elements */
	NSMutableArray *NCClientConnection;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfNCClientConnection *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addNCClientConnection:(emp_tns1_NCClientConnection *)toAdd;
@property (readonly) NSMutableArray * NCClientConnection;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_NCMessageType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * NCMessageTypeId;
	emp_tns1_ArrayOfNCMessage * NCMessages;
	NSString * Name;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_NCMessageType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NCMessageTypeId;
@property (retain) emp_tns1_ArrayOfNCMessage * NCMessages;
@property (retain) NSString * Name;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_NCMessage : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_Employee * Employee;
	emp_tns1_Employee * Employee1;
	NSString * FullMessage;
	USBoolean * IsDeleted;
	USBoolean * IsNew;
	NSDate * ModifiedDate;
	NSNumber * NCMessageId;
	emp_tns1_NCMessageType * NCMessageType;
	NSNumber * NCMessageTypeId;
	NSNumber * ReceiverEmployeeId;
	NSNumber * SenderEmployeeId;
	NSString * ShortcutMessage;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_NCMessage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) emp_tns1_Employee * Employee1;
@property (retain) NSString * FullMessage;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsNew;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NCMessageId;
@property (retain) emp_tns1_NCMessageType * NCMessageType;
@property (retain) NSNumber * NCMessageTypeId;
@property (retain) NSNumber * ReceiverEmployeeId;
@property (retain) NSNumber * SenderEmployeeId;
@property (retain) NSString * ShortcutMessage;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfNCMessage : NSObject {
	
/* elements */
	NSMutableArray *NCMessage;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfNCMessage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addNCMessage:(emp_tns1_NCMessage *)toAdd;
@property (readonly) NSMutableArray * NCMessage;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_Project : NSObject {
	
/* elements */
	NSString * Description;
	emp_tns1_Employee * Employee;
	NSDate * EndDate;
	NSNumber * Id_;
	NSString * Name;
	NSNumber * OwnerId;
	NSNumber * Progress;
	emp_tns1_ArrayOfProjectMember * ProjectMembers;
	NSDate * StartDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_Project *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Description;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSDate * EndDate;
@property (retain) NSNumber * Id_;
@property (retain) NSString * Name;
@property (retain) NSNumber * OwnerId;
@property (retain) NSNumber * Progress;
@property (retain) emp_tns1_ArrayOfProjectMember * ProjectMembers;
@property (retain) NSDate * StartDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ProjectMember : NSObject {
	
/* elements */
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSNumber * Id_;
	emp_tns1_Project * Project;
	NSNumber * ProjectId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ProjectMember *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * Id_;
@property (retain) emp_tns1_Project * Project;
@property (retain) NSNumber * ProjectId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfProjectMember : NSObject {
	
/* elements */
	NSMutableArray *ProjectMember;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfProjectMember *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addProjectMember:(emp_tns1_ProjectMember *)toAdd;
@property (readonly) NSMutableArray * ProjectMember;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfProject : NSObject {
	
/* elements */
	NSMutableArray *Project;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfProject *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addProject:(emp_tns1_Project *)toAdd;
@property (readonly) NSMutableArray * Project;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ReportEmployeeRequestedBook : NSObject {
	
/* elements */
	NSString * Address1;
	NSString * Benefit;
	NSDate * BirthDay;
	NSString * CLogCityNoIns;
	NSString * CLogHospitalCode;
	emp_tns1_Employee * Employee;
	NSString * Ethnicity;
	NSString * FullName;
	NSString * Gender;
	USBoolean * HadSIBook;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	NSString * Note;
	NSString * OrgUnitName;
	NSNumber * ReportEmployeeRequestedBookId;
	NSDate * RequestedDay;
	NSString * SICode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ReportEmployeeRequestedBook *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address1;
@property (retain) NSString * Benefit;
@property (retain) NSDate * BirthDay;
@property (retain) NSString * CLogCityNoIns;
@property (retain) NSString * CLogHospitalCode;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * Ethnicity;
@property (retain) NSString * FullName;
@property (retain) NSString * Gender;
@property (retain) USBoolean * HadSIBook;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) NSString * Note;
@property (retain) NSString * OrgUnitName;
@property (retain) NSNumber * ReportEmployeeRequestedBookId;
@property (retain) NSDate * RequestedDay;
@property (retain) NSString * SICode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SelfLeaveRequest : NSObject {
	
/* elements */
	NSString * Address;
	NSNumber * ApprovalStatus;
	NSString * ApproverCode;
	NSNumber * ApproverId;
	NSDate * CreatedDate;
	emp_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	USBoolean * IsCompensatoryLeave;
	USBoolean * IsDeleted;
	USBoolean * IsLeaveWithoutPay;
	USBoolean * IsPaidLeave;
	USBoolean * IsUnpaidLeave;
	NSDate * ModifiedDate;
	NSNumber * NumberDaysOff;
	NSNumber * OrganizationId;
	NSString * PhoneNumber;
	NSString * Reason;
	NSNumber * SalaryPenaltyValue;
	NSNumber * SelfLeaveRequestId;
	NSDate * StartDate;
	NSNumber * TimeAbsenceTypeId;
	NSString * WorkflowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SelfLeaveRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSNumber * ApprovalStatus;
@property (retain) NSString * ApproverCode;
@property (retain) NSNumber * ApproverId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsCompensatoryLeave;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsLeaveWithoutPay;
@property (retain) USBoolean * IsPaidLeave;
@property (retain) USBoolean * IsUnpaidLeave;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NumberDaysOff;
@property (retain) NSNumber * OrganizationId;
@property (retain) NSString * PhoneNumber;
@property (retain) NSString * Reason;
@property (retain) NSNumber * SalaryPenaltyValue;
@property (retain) NSNumber * SelfLeaveRequestId;
@property (retain) NSDate * StartDate;
@property (retain) NSNumber * TimeAbsenceTypeId;
@property (retain) NSString * WorkflowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSelfLeaveRequest : NSObject {
	
/* elements */
	NSMutableArray *SelfLeaveRequest;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSelfLeaveRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSelfLeaveRequest:(emp_tns1_SelfLeaveRequest *)toAdd;
@property (readonly) NSMutableArray * SelfLeaveRequest;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSysRecPlanApprover : NSObject {
	
/* elements */
	NSMutableArray *SysRecPlanApprover;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSysRecPlanApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysRecPlanApprover:(emp_tns1_SysRecPlanApprover *)toAdd;
@property (readonly) NSMutableArray * SysRecPlanApprover;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSysRecRequirementApprover : NSObject {
	
/* elements */
	NSMutableArray *SysRecRequirementApprover;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSysRecRequirementApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysRecRequirementApprover:(emp_tns1_SysRecRequirementApprover *)toAdd;
@property (readonly) NSMutableArray * SysRecRequirementApprover;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_TrainingEmpPlan : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsSubmited;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * State;
	NSNumber * StrategyGoalId;
	NSString * Target;
	emp_tns1_ArrayOfTrainingEmpPlanApproved * TrainingEmpPlanApproveds;
	NSNumber * TrainingEmpPlanId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_TrainingEmpPlan *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsSubmited;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * State;
@property (retain) NSNumber * StrategyGoalId;
@property (retain) NSString * Target;
@property (retain) emp_tns1_ArrayOfTrainingEmpPlanApproved * TrainingEmpPlanApproveds;
@property (retain) NSNumber * TrainingEmpPlanId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_TrainingEmpPlanApproved : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_SysTrainingApprover * SysTrainingApprover;
	NSNumber * SysTrainingApproverId;
	emp_tns1_TrainingEmpPlan * TrainingEmpPlan;
	NSNumber * TrainingEmpPlanApprovedId;
	NSNumber * TrainingEmpPlanId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_TrainingEmpPlanApproved *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_SysTrainingApprover * SysTrainingApprover;
@property (retain) NSNumber * SysTrainingApproverId;
@property (retain) emp_tns1_TrainingEmpPlan * TrainingEmpPlan;
@property (retain) NSNumber * TrainingEmpPlanApprovedId;
@property (retain) NSNumber * TrainingEmpPlanId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfTrainingEmpPlanApproved : NSObject {
	
/* elements */
	NSMutableArray *TrainingEmpPlanApproved;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfTrainingEmpPlanApproved *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingEmpPlanApproved:(emp_tns1_TrainingEmpPlanApproved *)toAdd;
@property (readonly) NSMutableArray * TrainingEmpPlanApproved;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SysTrainingApprover : NSObject {
	
/* elements */
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	emp_tns1_Employee * Employee;
	USBoolean * IsDeleted;
	NSNumber * Number;
	NSNumber * SysTrainingApproverId;
	emp_tns1_ArrayOfTrainingEmpPlanApproved * TrainingEmpPlanApproveds;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SysTrainingApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Number;
@property (retain) NSNumber * SysTrainingApproverId;
@property (retain) emp_tns1_ArrayOfTrainingEmpPlanApproved * TrainingEmpPlanApproveds;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSysTrainingApprover : NSObject {
	
/* elements */
	NSMutableArray *SysTrainingApprover;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSysTrainingApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysTrainingApprover:(emp_tns1_SysTrainingApprover *)toAdd;
@property (readonly) NSMutableArray * SysTrainingApprover;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_Task : NSObject {
	
/* elements */
	NSString * Description;
	emp_tns1_Employee * Employee;
	NSDate * FromDate;
	NSNumber * Id_;
	NSString * Name;
	NSNumber * Process;
	NSNumber * ProjectId;
	NSNumber * Status;
	NSNumber * TaskOwnerId;
	NSDate * ToDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_Task *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Description;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSDate * FromDate;
@property (retain) NSNumber * Id_;
@property (retain) NSString * Name;
@property (retain) NSNumber * Process;
@property (retain) NSNumber * ProjectId;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * TaskOwnerId;
@property (retain) NSDate * ToDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfTask : NSObject {
	
/* elements */
	NSMutableArray *Task;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfTask *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTask:(emp_tns1_Task *)toAdd;
@property (readonly) NSMutableArray * Task;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfTrainingEmpPlan : NSObject {
	
/* elements */
	NSMutableArray *TrainingEmpPlan;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfTrainingEmpPlan *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingEmpPlan:(emp_tns1_TrainingEmpPlan *)toAdd;
@property (readonly) NSMutableArray * TrainingEmpPlan;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_Employee : NSObject {
	
/* elements */
	NSDate * BirthDay;
	NSString * BirthPlace;
	NSNumber * BirthPlaceId;
	emp_tns1_ArrayOfCBAccidentInsurance * CBAccidentInsurances;
	emp_tns1_ArrayOfCBCompensationEmployeeGroupDetail * CBCompensationEmployeeGroupDetails;
	emp_tns1_ArrayOfCBConvalescence * CBConvalescences;
	emp_tns1_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
	emp_tns1_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
	emp_tns1_ArrayOfCBHealthInsuranceDetail * CBHealthInsuranceDetails;
	emp_tns1_CLogCity * CLogCity;
	emp_tns1_CLogCity * CLogCity1;
	emp_tns1_CLogEmployeeType * CLogEmployeeType;
	emp_tns1_ArrayOfCLogTrainer * CLogTrainers;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	emp_tns1_ArrayOfEmpBasicProfile * EmpBasicProfiles;
	emp_tns1_ArrayOfEmpCompetency * EmpCompetencies;
	emp_tns1_ArrayOfEmpCompetencyRating * EmpCompetencyRatings;
	emp_tns1_ArrayOfEmpContract * EmpContracts;
	emp_tns1_ArrayOfEmpPerformanceAppraisal * EmpPerformanceAppraisals;
	emp_tns1_ArrayOfEmpProfileAllowance * EmpProfileAllowances;
	emp_tns1_ArrayOfEmpProfileBaseSalary * EmpProfileBaseSalaries;
	emp_tns1_ArrayOfEmpProfileBenefit * EmpProfileBenefits;
	emp_tns1_ArrayOfEmpProfileComment * EmpProfileComments;
	emp_tns1_ArrayOfEmpProfileComment * EmpProfileComments1;
	emp_tns1_ArrayOfEmpProfileComputingSkill * EmpProfileComputingSkills;
	emp_tns1_ArrayOfEmpProfileContact * EmpProfileContacts;
	emp_tns1_ArrayOfEmpProfileDegree * EmpProfileDegrees;
	emp_tns1_ArrayOfEmpProfileDiscipline * EmpProfileDisciplines;
	emp_tns1_ArrayOfEmpProfileEducation * EmpProfileEducations;
	emp_tns1_ArrayOfEmpProfileEquipment * EmpProfileEquipments;
	emp_tns1_ArrayOfEmpProfileExperience * EmpProfileExperiences;
	emp_tns1_ArrayOfEmpProfileFamilyRelationship * EmpProfileFamilyRelationships;
	emp_tns1_ArrayOfEmpProfileForeignLanguage * EmpProfileForeignLanguages;
	emp_tns1_ArrayOfEmpProfileHealthInsurance * EmpProfileHealthInsurances;
	emp_tns1_ArrayOfEmpProfileHealthy * EmpProfileHealthies;
	emp_tns1_ArrayOfEmpProfileHobby * EmpProfileHobbies;
	emp_tns1_ArrayOfEmpProfileInternalCourse * EmpProfileInternalCourses;
	emp_tns1_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions;
	emp_tns1_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions1;
	emp_tns1_ArrayOfEmpProfileLeaveRegime * EmpProfileLeaveRegimes;
	emp_tns1_ArrayOfEmpProfileOtherBenefit * EmpProfileOtherBenefits;
	emp_tns1_ArrayOfEmpProfileParticipation * EmpProfileParticipations;
	emp_tns1_ArrayOfEmpProfilePersonality * EmpProfilePersonalities;
	emp_tns1_ArrayOfEmpProfileProcessOfWork * EmpProfileProcessOfWorks;
	emp_tns1_ArrayOfEmpProfileQualification * EmpProfileQualifications;
	emp_tns1_ArrayOfEmpProfileReward * EmpProfileRewards;
	emp_tns1_ArrayOfEmpProfileSkill * EmpProfileSkills;
	emp_tns1_ArrayOfEmpProfileTotalIncome * EmpProfileTotalIncomes;
	emp_tns1_ArrayOfEmpProfileTraining * EmpProfileTrainings;
	emp_tns1_ArrayOfEmpProfileWageType * EmpProfileWageTypes;
	emp_tns1_ArrayOfEmpProfileWorkingExperience * EmpProfileWorkingExperiences;
	emp_tns1_ArrayOfEmpProfileWorkingForm * EmpProfileWorkingForms;
	emp_tns1_ArrayOfEmpSocialInsuranceSalary * EmpSocialInsuranceSalaries;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * EmployeeTKCode;
	NSNumber * EmployeeTypeId;
	NSDate * EndWorkingDate;
	NSDate * EntryDate;
	NSString * Ethnicity;
	NSNumber * EthnicityId;
	NSString * FirstName;
	NSString * FullName;
	emp_tns1_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals;
	emp_tns1_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals1;
	emp_tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
	emp_tns1_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
	emp_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
	emp_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
	emp_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
	emp_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives1;
	emp_tns1_ArrayOfGPObjectiveInitiative * GPObjectiveInitiatives;
	emp_tns1_ArrayOfGPPerformanceYearEndResult * GPPerformanceYearEndResults;
	USBoolean * Gender;
	NSString * HomeTown;
	NSNumber * HomeTownId;
	NSString * IdentityCardNo;
	NSString * ImageUrl;
	USBoolean * IsDeleted;
	USBoolean * IsOnlyYearOfBirthday;
	emp_tns1_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
	emp_tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
	emp_tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts1;
	NSString * LastName;
	NSString * MainLanguage;
	NSNumber * MainLanguageId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	emp_tns1_ArrayOfNCClientConnection * NCClientConnections;
	emp_tns1_ArrayOfNCMessage * NCMessages;
	emp_tns1_ArrayOfNCMessage * NCMessages1;
	NSDate * OfficialDate;
	NSString * OrgGroupCd;
	NSNumber * OrgGroupId;
	NSDate * ProbationaryDate;
	NSDate * ProbationaryEndDate;
	emp_tns1_ArrayOfProjectMember * ProjectMembers;
	emp_tns1_ArrayOfProject * Projects;
	emp_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
	emp_tns1_ArrayOfRecInterviewer * RecInterviewers;
	emp_tns1_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
	emp_tns1_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
	emp_tns1_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
	NSString * Religion;
	NSNumber * ReligionId;
	emp_tns1_ReportEmployeeRequestedBook * ReportEmployeeRequestedBook;
	emp_tns1_ArrayOfSelfLeaveRequest * SelfLeaveRequests;
	emp_tns1_ArrayOfSysRecPlanApprover * SysRecPlanApprovers;
	emp_tns1_ArrayOfSysRecRequirementApprover * SysRecRequirementApprovers;
	emp_tns1_ArrayOfSysTrainingApprover * SysTrainingApprovers;
	emp_tns1_ArrayOfTask * Tasks;
	emp_tns1_ArrayOfTrainingCourseEmployee * TrainingCourseEmployees;
	emp_tns1_ArrayOfTrainingEmpPlan * TrainingEmpPlans;
	emp_tns1_ArrayOfTrainingPlanEmployee * TrainingPlanEmployees;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_Employee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * BirthDay;
@property (retain) NSString * BirthPlace;
@property (retain) NSNumber * BirthPlaceId;
@property (retain) emp_tns1_ArrayOfCBAccidentInsurance * CBAccidentInsurances;
@property (retain) emp_tns1_ArrayOfCBCompensationEmployeeGroupDetail * CBCompensationEmployeeGroupDetails;
@property (retain) emp_tns1_ArrayOfCBConvalescence * CBConvalescences;
@property (retain) emp_tns1_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
@property (retain) emp_tns1_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
@property (retain) emp_tns1_ArrayOfCBHealthInsuranceDetail * CBHealthInsuranceDetails;
@property (retain) emp_tns1_CLogCity * CLogCity;
@property (retain) emp_tns1_CLogCity * CLogCity1;
@property (retain) emp_tns1_CLogEmployeeType * CLogEmployeeType;
@property (retain) emp_tns1_ArrayOfCLogTrainer * CLogTrainers;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_ArrayOfEmpBasicProfile * EmpBasicProfiles;
@property (retain) emp_tns1_ArrayOfEmpCompetency * EmpCompetencies;
@property (retain) emp_tns1_ArrayOfEmpCompetencyRating * EmpCompetencyRatings;
@property (retain) emp_tns1_ArrayOfEmpContract * EmpContracts;
@property (retain) emp_tns1_ArrayOfEmpPerformanceAppraisal * EmpPerformanceAppraisals;
@property (retain) emp_tns1_ArrayOfEmpProfileAllowance * EmpProfileAllowances;
@property (retain) emp_tns1_ArrayOfEmpProfileBaseSalary * EmpProfileBaseSalaries;
@property (retain) emp_tns1_ArrayOfEmpProfileBenefit * EmpProfileBenefits;
@property (retain) emp_tns1_ArrayOfEmpProfileComment * EmpProfileComments;
@property (retain) emp_tns1_ArrayOfEmpProfileComment * EmpProfileComments1;
@property (retain) emp_tns1_ArrayOfEmpProfileComputingSkill * EmpProfileComputingSkills;
@property (retain) emp_tns1_ArrayOfEmpProfileContact * EmpProfileContacts;
@property (retain) emp_tns1_ArrayOfEmpProfileDegree * EmpProfileDegrees;
@property (retain) emp_tns1_ArrayOfEmpProfileDiscipline * EmpProfileDisciplines;
@property (retain) emp_tns1_ArrayOfEmpProfileEducation * EmpProfileEducations;
@property (retain) emp_tns1_ArrayOfEmpProfileEquipment * EmpProfileEquipments;
@property (retain) emp_tns1_ArrayOfEmpProfileExperience * EmpProfileExperiences;
@property (retain) emp_tns1_ArrayOfEmpProfileFamilyRelationship * EmpProfileFamilyRelationships;
@property (retain) emp_tns1_ArrayOfEmpProfileForeignLanguage * EmpProfileForeignLanguages;
@property (retain) emp_tns1_ArrayOfEmpProfileHealthInsurance * EmpProfileHealthInsurances;
@property (retain) emp_tns1_ArrayOfEmpProfileHealthy * EmpProfileHealthies;
@property (retain) emp_tns1_ArrayOfEmpProfileHobby * EmpProfileHobbies;
@property (retain) emp_tns1_ArrayOfEmpProfileInternalCourse * EmpProfileInternalCourses;
@property (retain) emp_tns1_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions;
@property (retain) emp_tns1_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions1;
@property (retain) emp_tns1_ArrayOfEmpProfileLeaveRegime * EmpProfileLeaveRegimes;
@property (retain) emp_tns1_ArrayOfEmpProfileOtherBenefit * EmpProfileOtherBenefits;
@property (retain) emp_tns1_ArrayOfEmpProfileParticipation * EmpProfileParticipations;
@property (retain) emp_tns1_ArrayOfEmpProfilePersonality * EmpProfilePersonalities;
@property (retain) emp_tns1_ArrayOfEmpProfileProcessOfWork * EmpProfileProcessOfWorks;
@property (retain) emp_tns1_ArrayOfEmpProfileQualification * EmpProfileQualifications;
@property (retain) emp_tns1_ArrayOfEmpProfileReward * EmpProfileRewards;
@property (retain) emp_tns1_ArrayOfEmpProfileSkill * EmpProfileSkills;
@property (retain) emp_tns1_ArrayOfEmpProfileTotalIncome * EmpProfileTotalIncomes;
@property (retain) emp_tns1_ArrayOfEmpProfileTraining * EmpProfileTrainings;
@property (retain) emp_tns1_ArrayOfEmpProfileWageType * EmpProfileWageTypes;
@property (retain) emp_tns1_ArrayOfEmpProfileWorkingExperience * EmpProfileWorkingExperiences;
@property (retain) emp_tns1_ArrayOfEmpProfileWorkingForm * EmpProfileWorkingForms;
@property (retain) emp_tns1_ArrayOfEmpSocialInsuranceSalary * EmpSocialInsuranceSalaries;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * EmployeeTKCode;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSDate * EndWorkingDate;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * Ethnicity;
@property (retain) NSNumber * EthnicityId;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) emp_tns1_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals;
@property (retain) emp_tns1_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals1;
@property (retain) emp_tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
@property (retain) emp_tns1_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
@property (retain) emp_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
@property (retain) emp_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
@property (retain) emp_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
@property (retain) emp_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives1;
@property (retain) emp_tns1_ArrayOfGPObjectiveInitiative * GPObjectiveInitiatives;
@property (retain) emp_tns1_ArrayOfGPPerformanceYearEndResult * GPPerformanceYearEndResults;
@property (retain) USBoolean * Gender;
@property (retain) NSString * HomeTown;
@property (retain) NSNumber * HomeTownId;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) emp_tns1_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
@property (retain) emp_tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
@property (retain) emp_tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts1;
@property (retain) NSString * LastName;
@property (retain) NSString * MainLanguage;
@property (retain) NSNumber * MainLanguageId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_ArrayOfNCClientConnection * NCClientConnections;
@property (retain) emp_tns1_ArrayOfNCMessage * NCMessages;
@property (retain) emp_tns1_ArrayOfNCMessage * NCMessages1;
@property (retain) NSDate * OfficialDate;
@property (retain) NSString * OrgGroupCd;
@property (retain) NSNumber * OrgGroupId;
@property (retain) NSDate * ProbationaryDate;
@property (retain) NSDate * ProbationaryEndDate;
@property (retain) emp_tns1_ArrayOfProjectMember * ProjectMembers;
@property (retain) emp_tns1_ArrayOfProject * Projects;
@property (retain) emp_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
@property (retain) emp_tns1_ArrayOfRecInterviewer * RecInterviewers;
@property (retain) emp_tns1_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
@property (retain) emp_tns1_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
@property (retain) emp_tns1_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
@property (retain) NSString * Religion;
@property (retain) NSNumber * ReligionId;
@property (retain) emp_tns1_ReportEmployeeRequestedBook * ReportEmployeeRequestedBook;
@property (retain) emp_tns1_ArrayOfSelfLeaveRequest * SelfLeaveRequests;
@property (retain) emp_tns1_ArrayOfSysRecPlanApprover * SysRecPlanApprovers;
@property (retain) emp_tns1_ArrayOfSysRecRequirementApprover * SysRecRequirementApprovers;
@property (retain) emp_tns1_ArrayOfSysTrainingApprover * SysTrainingApprovers;
@property (retain) emp_tns1_ArrayOfTask * Tasks;
@property (retain) emp_tns1_ArrayOfTrainingCourseEmployee * TrainingCourseEmployees;
@property (retain) emp_tns1_ArrayOfTrainingEmpPlan * TrainingEmpPlans;
@property (retain) emp_tns1_ArrayOfTrainingPlanEmployee * TrainingPlanEmployees;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogTrainer : NSObject {
	
/* elements */
	NSString * Address;
	emp_tns1_Address * Address1;
	NSNumber * AddressId;
	NSNumber * CLogTrainerId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * EducationUnit;
	NSString * Email;
	emp_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSString * FirstName;
	NSString * FullName;
	USBoolean * IsDeleted;
	USBoolean * IsEmployee;
	NSString * LastName;
	NSDate * ModifiedDate;
	NSString * PhoneNumber;
	emp_tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogTrainer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) emp_tns1_Address * Address1;
@property (retain) NSNumber * AddressId;
@property (retain) NSNumber * CLogTrainerId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * EducationUnit;
@property (retain) NSString * Email;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEmployee;
@property (retain) NSString * LastName;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * PhoneNumber;
@property (retain) emp_tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogTrainer : NSObject {
	
/* elements */
	NSMutableArray *CLogTrainer;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogTrainer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogTrainer:(emp_tns1_CLogTrainer *)toAdd;
@property (readonly) NSMutableArray * CLogTrainer;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_Address : NSObject {
	
/* elements */
	NSNumber * AddressId;
	NSString * AddressNumber;
	NSNumber * CLogCityId;
	NSNumber * CLogCountryId;
	NSNumber * CLogDistrictId;
	emp_tns1_ArrayOfCLogTrainer * CLogTrainers;
	emp_tns1_ArrayOfEmpContract * EmpContracts;
	NSString * FullAddress;
	emp_tns1_ArrayOfOrgUnit * OrgUnits;
	NSString * PostalCode;
	NSString * Street;
	NSString * Ward;
	NSString * ZipCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_Address *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AddressId;
@property (retain) NSString * AddressNumber;
@property (retain) NSNumber * CLogCityId;
@property (retain) NSNumber * CLogCountryId;
@property (retain) NSNumber * CLogDistrictId;
@property (retain) emp_tns1_ArrayOfCLogTrainer * CLogTrainers;
@property (retain) emp_tns1_ArrayOfEmpContract * EmpContracts;
@property (retain) NSString * FullAddress;
@property (retain) emp_tns1_ArrayOfOrgUnit * OrgUnits;
@property (retain) NSString * PostalCode;
@property (retain) NSString * Street;
@property (retain) NSString * Ward;
@property (retain) NSString * ZipCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpContract : NSObject {
	
/* elements */
	emp_tns1_Address * Address;
	NSString * AllocationEquipmentDescription;
	NSString * Allowances;
	NSString * AttachFile;
	NSNumber * BaseWage;
	NSString * Bonuses;
	NSString * CLogContractTermCode;
	NSString * CLogContractTypeCode;
	NSNumber * CompanyId;
	NSString * CompensationDescription;
	NSString * ContractCode;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DateSigned;
	NSDate * EmpAppraiseDate;
	NSString * EmpAppraiseNote;
	NSNumber * EmpAppraiseRating;
	NSNumber * EmpContractId;
	emp_tns1_Employee * Employee;
	NSString * EmployeeRepresentativeAddress;
	NSNumber * EmployeeRepresentativeAddressId;
	NSDate * EmployeeRepresentativeBirthDay;
	NSString * EmployeeRepresentativeBirthPlace;
	NSString * EmployeeRepresentativeCode;
	NSString * EmployeeRepresentativeFullName;
	NSNumber * EmployeeRepresentativeId;
	NSString * EmployeeRepresentativeMajor;
	NSString * EmployeeRepresentativeNationality;
	emp_tns1_Employer * Employer;
	NSNumber * EmployerRepresentativeId;
	NSDate * EndDate;
	NSDate * ExpirationDate;
	NSDate * HRAppraiseDate;
	NSString * HRAppraiseNote;
	NSNumber * HRAppraiseRating;
	NSString * HolIdayPolicy;
	NSNumber * HoursOfOfficialWork;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	USBoolean * IsDeleted;
	USBoolean * IsEnd;
	USBoolean * IsProbation;
	USBoolean * IsSigned;
	NSString * JobResponsibilitiesDescription;
	NSString * LaborSupportEquipments;
	NSDate * LeaderAppraiseDate;
	NSString * LeaderAppraiseNote;
	NSNumber * LeaderAppraiseRating;
	NSString * MedicalInsurance;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSDate * OfficialWorkEndTime;
	NSDate * OfficialWorkStartTime;
	emp_tns1_OrgJob * OrgJob;
	NSNumber * OrgJobId;
	emp_tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSNumber * OrgUnitId;
	NSString * OrganizationAddress;
	NSNumber * OrganizationAddressId;
	NSString * OrganizationName;
	NSString * OrganizationPhone;
	NSString * OtherPolicy;
	NSString * PayDay;
	NSString * PayingMethod;
	NSString * PlaceSigned;
	NSDate * ProbationaryEndDate;
	NSDate * ProbationaryStartDate;
	NSNumber * ProbationaryTime;
	NSString * ProvidedEquipment;
	NSNumber * RelationEmpContractId;
	NSString * SalaryIncreasePolicy;
	NSString * SocialInsurance;
	NSDate * StartDate;
	NSString * TrainingPolicy;
	NSString * Transportation;
	NSString * WorkingAddress;
	NSNumber * WorkingAddressId;
	NSNumber * WorkingDay;
	NSString * WorkingTime;
	NSString * WorkingTimePolicy;
	NSString * cAllowance;
	NSString * cBaseSalary;
	NSString * cSocialInsuranceSalary;
	NSNumber * nAllowance;
	NSNumber * nSocialInsuranceSalary;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) emp_tns1_Address * Address;
@property (retain) NSString * AllocationEquipmentDescription;
@property (retain) NSString * Allowances;
@property (retain) NSString * AttachFile;
@property (retain) NSNumber * BaseWage;
@property (retain) NSString * Bonuses;
@property (retain) NSString * CLogContractTermCode;
@property (retain) NSString * CLogContractTypeCode;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompensationDescription;
@property (retain) NSString * ContractCode;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateSigned;
@property (retain) NSDate * EmpAppraiseDate;
@property (retain) NSString * EmpAppraiseNote;
@property (retain) NSNumber * EmpAppraiseRating;
@property (retain) NSNumber * EmpContractId;
@property (retain) emp_tns1_Employee * Employee;
@property (retain) NSString * EmployeeRepresentativeAddress;
@property (retain) NSNumber * EmployeeRepresentativeAddressId;
@property (retain) NSDate * EmployeeRepresentativeBirthDay;
@property (retain) NSString * EmployeeRepresentativeBirthPlace;
@property (retain) NSString * EmployeeRepresentativeCode;
@property (retain) NSString * EmployeeRepresentativeFullName;
@property (retain) NSNumber * EmployeeRepresentativeId;
@property (retain) NSString * EmployeeRepresentativeMajor;
@property (retain) NSString * EmployeeRepresentativeNationality;
@property (retain) emp_tns1_Employer * Employer;
@property (retain) NSNumber * EmployerRepresentativeId;
@property (retain) NSDate * EndDate;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSDate * HRAppraiseDate;
@property (retain) NSString * HRAppraiseNote;
@property (retain) NSNumber * HRAppraiseRating;
@property (retain) NSString * HolIdayPolicy;
@property (retain) NSNumber * HoursOfOfficialWork;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEnd;
@property (retain) USBoolean * IsProbation;
@property (retain) USBoolean * IsSigned;
@property (retain) NSString * JobResponsibilitiesDescription;
@property (retain) NSString * LaborSupportEquipments;
@property (retain) NSDate * LeaderAppraiseDate;
@property (retain) NSString * LeaderAppraiseNote;
@property (retain) NSNumber * LeaderAppraiseRating;
@property (retain) NSString * MedicalInsurance;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * OfficialWorkEndTime;
@property (retain) NSDate * OfficialWorkStartTime;
@property (retain) emp_tns1_OrgJob * OrgJob;
@property (retain) NSNumber * OrgJobId;
@property (retain) emp_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrganizationAddress;
@property (retain) NSNumber * OrganizationAddressId;
@property (retain) NSString * OrganizationName;
@property (retain) NSString * OrganizationPhone;
@property (retain) NSString * OtherPolicy;
@property (retain) NSString * PayDay;
@property (retain) NSString * PayingMethod;
@property (retain) NSString * PlaceSigned;
@property (retain) NSDate * ProbationaryEndDate;
@property (retain) NSDate * ProbationaryStartDate;
@property (retain) NSNumber * ProbationaryTime;
@property (retain) NSString * ProvidedEquipment;
@property (retain) NSNumber * RelationEmpContractId;
@property (retain) NSString * SalaryIncreasePolicy;
@property (retain) NSString * SocialInsurance;
@property (retain) NSDate * StartDate;
@property (retain) NSString * TrainingPolicy;
@property (retain) NSString * Transportation;
@property (retain) NSString * WorkingAddress;
@property (retain) NSNumber * WorkingAddressId;
@property (retain) NSNumber * WorkingDay;
@property (retain) NSString * WorkingTime;
@property (retain) NSString * WorkingTimePolicy;
@property (retain) NSString * cAllowance;
@property (retain) NSString * cBaseSalary;
@property (retain) NSString * cSocialInsuranceSalary;
@property (retain) NSNumber * nAllowance;
@property (retain) NSNumber * nSocialInsuranceSalary;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpContract : NSObject {
	
/* elements */
	NSMutableArray *EmpContract;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpContract:(emp_tns1_EmpContract *)toAdd;
@property (readonly) NSMutableArray * EmpContract;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_Employer : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	emp_tns1_ArrayOfEmpContract * EmpContracts;
	NSString * EmployerCode;
	NSNumber * EmployerId;
	NSString * FullName;
	USBoolean * IsDeleted;
	NSString * JobPositionName;
	NSDate * ModifiedDate;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_Employer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) emp_tns1_ArrayOfEmpContract * EmpContracts;
@property (retain) NSString * EmployerCode;
@property (retain) NSNumber * EmployerId;
@property (retain) NSString * FullName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * JobPositionName;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmployer : NSObject {
	
/* elements */
	NSMutableArray *Employer;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmployer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmployer:(emp_tns1_Employer *)toAdd;
@property (readonly) NSMutableArray * Employer;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_Company : NSObject {
	
/* elements */
	NSString * AccountNo;
	NSString * Address;
	NSNumber * AddressId;
	NSString * BankBranch;
	NSString * CompanyCode;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Fax;
	NSNumber * Female;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * PhoneNumber;
	NSNumber * TSalaryFund;
	NSNumber * TotalEmployees;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_Company *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AccountNo;
@property (retain) NSString * Address;
@property (retain) NSNumber * AddressId;
@property (retain) NSString * BankBranch;
@property (retain) NSString * CompanyCode;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Fax;
@property (retain) NSNumber * Female;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * PhoneNumber;
@property (retain) NSNumber * TSalaryFund;
@property (retain) NSNumber * TotalEmployees;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCompany : NSObject {
	
/* elements */
	NSMutableArray *Company;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCompany *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCompany:(emp_tns1_Company *)toAdd;
@property (readonly) NSMutableArray * Company;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_EmpContract : NSObject {
	
/* elements */
	NSString * AllocationEquipmentDescription;
	NSString * Allowances;
	NSString * AttachFile;
	NSNumber * BaseWage;
	NSString * Bonuses;
	NSString * CONTRACT_CODE_TEMPLATE;
	NSString * CONTRACT_TERM_CODE;
	NSString * CONTRACT_TERM_NAME_EN;
	NSString * CONTRACT_TERM_NAME_VN;
	NSString * CONTRACT_TYPE_CODE;
	NSString * CONTRACT_TYPE_NAME_EN;
	NSString * CONTRACT_TYPE_NAME_VN;
	NSNumber * CompanyId;
	NSString * CompensationDescription;
	NSString * ContractCode;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * DAYS_PERIOD;
	NSDate * DateSigned;
	NSDate * EmpAppraiseDate;
	NSString * EmpAppraiseNote;
	NSNumber * EmpAppraiseRating;
	NSNumber * EmpContractId;
	NSString * EmployeeRepresentativeAddress;
	NSDate * EmployeeRepresentativeBirthDay;
	NSString * EmployeeRepresentativeBirthPlace;
	NSString * EmployeeRepresentativeCode;
	NSString * EmployeeRepresentativeFullName;
	NSNumber * EmployeeRepresentativeId;
	NSString * EmployeeRepresentativeMajor;
	NSString * EmployeeRepresentativeNationality;
	NSString * EmployerNationality;
	NSString * EmployerPositionName;
	NSString * EmployerRepresentativeCode;
	NSString * EmployerRepresentativeFullName;
	NSNumber * EmployerRepresentativeId;
	NSString * EmployerRepresentativePositionName;
	NSDate * EndDate;
	NSDate * ExpirationDate;
	NSDate * HRAppraiseDate;
	NSString * HRAppraiseNote;
	NSNumber * HRAppraiseRating;
	NSString * HolIdayPolicy;
	NSNumber * HoursOfOfficialWork;
	USBoolean * IS_TERM_CONTRAC_TYPE;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	USBoolean * IsDeleted;
	USBoolean * IsEnd;
	USBoolean * IsProbation;
	USBoolean * IsSigned;
	NSString * JobName;
	NSString * JobNameEN;
	NSString * JobPositionName;
	NSString * JobPositionNameEN;
	NSString * JobResponsibilitiesDescription;
	NSString * LaborSupportEquipments;
	NSDate * LeaderAppraiseDate;
	NSString * LeaderAppraiseNote;
	NSNumber * LeaderAppraiseRating;
	NSNumber * MONTHS_PERIOD;
	NSString * MedicalInsurance;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSDate * OfficialWorkEndTime;
	NSDate * OfficialWorkStartTime;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrganizationAddress;
	NSString * OrganizationName;
	NSString * OrganizationPhone;
	NSString * OtherPolicy;
	NSString * PayDay;
	NSString * PayingMethod;
	NSString * PlaceSigned;
	NSDate * ProbationaryEndDate;
	NSDate * ProbationaryStartDate;
	NSNumber * ProbationaryTime;
	NSString * ProvidedEquipment;
	NSNumber * RelationEmpContractId;
	NSString * SalaryIncreasePolicy;
	NSString * SocialInsurance;
	NSDate * StartDate;
	NSString * TrainingPolicy;
	NSString * Transportation;
	NSString * UnitName;
	NSString * UnitNameEN;
	NSString * WorkingAddress;
	NSNumber * WorkingDay;
	NSString * WorkingTime;
	NSString * WorkingTimePolicy;
	NSString * cAllowance;
	NSString * cBaseSalary;
	NSString * cSocialInsuranceSalary;
	NSNumber * nAllowance;
	NSNumber * nSocialInsuranceSalary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_EmpContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AllocationEquipmentDescription;
@property (retain) NSString * Allowances;
@property (retain) NSString * AttachFile;
@property (retain) NSNumber * BaseWage;
@property (retain) NSString * Bonuses;
@property (retain) NSString * CONTRACT_CODE_TEMPLATE;
@property (retain) NSString * CONTRACT_TERM_CODE;
@property (retain) NSString * CONTRACT_TERM_NAME_EN;
@property (retain) NSString * CONTRACT_TERM_NAME_VN;
@property (retain) NSString * CONTRACT_TYPE_CODE;
@property (retain) NSString * CONTRACT_TYPE_NAME_EN;
@property (retain) NSString * CONTRACT_TYPE_NAME_VN;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompensationDescription;
@property (retain) NSString * ContractCode;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * DAYS_PERIOD;
@property (retain) NSDate * DateSigned;
@property (retain) NSDate * EmpAppraiseDate;
@property (retain) NSString * EmpAppraiseNote;
@property (retain) NSNumber * EmpAppraiseRating;
@property (retain) NSNumber * EmpContractId;
@property (retain) NSString * EmployeeRepresentativeAddress;
@property (retain) NSDate * EmployeeRepresentativeBirthDay;
@property (retain) NSString * EmployeeRepresentativeBirthPlace;
@property (retain) NSString * EmployeeRepresentativeCode;
@property (retain) NSString * EmployeeRepresentativeFullName;
@property (retain) NSNumber * EmployeeRepresentativeId;
@property (retain) NSString * EmployeeRepresentativeMajor;
@property (retain) NSString * EmployeeRepresentativeNationality;
@property (retain) NSString * EmployerNationality;
@property (retain) NSString * EmployerPositionName;
@property (retain) NSString * EmployerRepresentativeCode;
@property (retain) NSString * EmployerRepresentativeFullName;
@property (retain) NSNumber * EmployerRepresentativeId;
@property (retain) NSString * EmployerRepresentativePositionName;
@property (retain) NSDate * EndDate;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSDate * HRAppraiseDate;
@property (retain) NSString * HRAppraiseNote;
@property (retain) NSNumber * HRAppraiseRating;
@property (retain) NSString * HolIdayPolicy;
@property (retain) NSNumber * HoursOfOfficialWork;
@property (retain) USBoolean * IS_TERM_CONTRAC_TYPE;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEnd;
@property (retain) USBoolean * IsProbation;
@property (retain) USBoolean * IsSigned;
@property (retain) NSString * JobName;
@property (retain) NSString * JobNameEN;
@property (retain) NSString * JobPositionName;
@property (retain) NSString * JobPositionNameEN;
@property (retain) NSString * JobResponsibilitiesDescription;
@property (retain) NSString * LaborSupportEquipments;
@property (retain) NSDate * LeaderAppraiseDate;
@property (retain) NSString * LeaderAppraiseNote;
@property (retain) NSNumber * LeaderAppraiseRating;
@property (retain) NSNumber * MONTHS_PERIOD;
@property (retain) NSString * MedicalInsurance;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * OfficialWorkEndTime;
@property (retain) NSDate * OfficialWorkStartTime;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrganizationAddress;
@property (retain) NSString * OrganizationName;
@property (retain) NSString * OrganizationPhone;
@property (retain) NSString * OtherPolicy;
@property (retain) NSString * PayDay;
@property (retain) NSString * PayingMethod;
@property (retain) NSString * PlaceSigned;
@property (retain) NSDate * ProbationaryEndDate;
@property (retain) NSDate * ProbationaryStartDate;
@property (retain) NSNumber * ProbationaryTime;
@property (retain) NSString * ProvidedEquipment;
@property (retain) NSNumber * RelationEmpContractId;
@property (retain) NSString * SalaryIncreasePolicy;
@property (retain) NSString * SocialInsurance;
@property (retain) NSDate * StartDate;
@property (retain) NSString * TrainingPolicy;
@property (retain) NSString * Transportation;
@property (retain) NSString * UnitName;
@property (retain) NSString * UnitNameEN;
@property (retain) NSString * WorkingAddress;
@property (retain) NSNumber * WorkingDay;
@property (retain) NSString * WorkingTime;
@property (retain) NSString * WorkingTimePolicy;
@property (retain) NSString * cAllowance;
@property (retain) NSString * cBaseSalary;
@property (retain) NSString * cSocialInsuranceSalary;
@property (retain) NSNumber * nAllowance;
@property (retain) NSNumber * nSocialInsuranceSalary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_EmpContract : NSObject {
	
/* elements */
	NSMutableArray *V_EmpContract;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_EmpContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpContract:(emp_tns1_V_EmpContract *)toAdd;
@property (readonly) NSMutableArray * V_EmpContract;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_EMP_CONTRACT_WOKING_HOURS : NSObject {
	
/* elements */
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndTime;
	NSString * FullName;
	NSNumber * HoursOfOfficialWork;
	NSDate * StartTime;
	NSString * TimeWageTypeCode;
	NSString * TimeWageType_NameEN;
	NSString * TimeWageType_NameVN;
	NSString * TimeWorkingFormCode;
	NSString * TimeWorkingForm_NameEN;
	NSString * TimeWorkingForm_NameVN;
	NSNumber * WorkingDay;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_EMP_CONTRACT_WOKING_HOURS *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndTime;
@property (retain) NSString * FullName;
@property (retain) NSNumber * HoursOfOfficialWork;
@property (retain) NSDate * StartTime;
@property (retain) NSString * TimeWageTypeCode;
@property (retain) NSString * TimeWageType_NameEN;
@property (retain) NSString * TimeWageType_NameVN;
@property (retain) NSString * TimeWorkingFormCode;
@property (retain) NSString * TimeWorkingForm_NameEN;
@property (retain) NSString * TimeWorkingForm_NameVN;
@property (retain) NSNumber * WorkingDay;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_ESS_CLOG_CONTRACT_TYPE : NSObject {
	
/* elements */
	NSNumber * CLOG_CONTRACT_TYPE_ID;
	NSNumber * COMPANY_ID;
	NSString * CONTRACT_CODE_TEMPLATE;
	NSString * CONTRACT_TYPE_CODE;
	NSString * CONTRACT_TYPE_DESC;
	NSString * CONTRACT_TYPE_NAME_EN;
	NSString * CONTRACT_TYPE_NAME_VN;
	NSNumber * CREATED_BY;
	NSDate * CREATED_DATE;
	USBoolean * IS_DELETED;
	USBoolean * IS_TERM_CONTRAC_TYPE;
	NSNumber * MODIFIED_BY;
	NSDate * MODIFIED_DATE;
	NSNumber * ORDER_BY;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_ESS_CLOG_CONTRACT_TYPE *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLOG_CONTRACT_TYPE_ID;
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSString * CONTRACT_CODE_TEMPLATE;
@property (retain) NSString * CONTRACT_TYPE_CODE;
@property (retain) NSString * CONTRACT_TYPE_DESC;
@property (retain) NSString * CONTRACT_TYPE_NAME_EN;
@property (retain) NSString * CONTRACT_TYPE_NAME_VN;
@property (retain) NSNumber * CREATED_BY;
@property (retain) NSDate * CREATED_DATE;
@property (retain) USBoolean * IS_DELETED;
@property (retain) USBoolean * IS_TERM_CONTRAC_TYPE;
@property (retain) NSNumber * MODIFIED_BY;
@property (retain) NSDate * MODIFIED_DATE;
@property (retain) NSNumber * ORDER_BY;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_ESS_CLOG_CONTRACT_TYPE : NSObject {
	
/* elements */
	NSMutableArray *V_ESS_CLOG_CONTRACT_TYPE;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_ESS_CLOG_CONTRACT_TYPE *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ESS_CLOG_CONTRACT_TYPE:(emp_tns1_V_ESS_CLOG_CONTRACT_TYPE *)toAdd;
@property (readonly) NSMutableArray * V_ESS_CLOG_CONTRACT_TYPE;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_ESS_CLOG_CONTRACT_TERM : NSObject {
	
/* elements */
	NSNumber * CLOG_CONTRACT_TERM_ID;
	NSNumber * COMPANY_ID;
	NSString * CONTRACT_TERM_CODE;
	NSString * CONTRACT_TERM_DESC;
	NSString * CONTRACT_TERM_NAME_EN;
	NSString * CONTRACT_TERM_NAME_VN;
	NSNumber * CREATED_BY;
	NSDate * CREATED_DATE;
	NSNumber * DAYS_PERIOD;
	USBoolean * IS_DELETED;
	NSNumber * MODIFIED_BY;
	NSDate * MODIFIED_DATE;
	NSNumber * MONTHS_PERIOD;
	NSNumber * ORDER_BY;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_ESS_CLOG_CONTRACT_TERM *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLOG_CONTRACT_TERM_ID;
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSString * CONTRACT_TERM_CODE;
@property (retain) NSString * CONTRACT_TERM_DESC;
@property (retain) NSString * CONTRACT_TERM_NAME_EN;
@property (retain) NSString * CONTRACT_TERM_NAME_VN;
@property (retain) NSNumber * CREATED_BY;
@property (retain) NSDate * CREATED_DATE;
@property (retain) NSNumber * DAYS_PERIOD;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSNumber * MODIFIED_BY;
@property (retain) NSDate * MODIFIED_DATE;
@property (retain) NSNumber * MONTHS_PERIOD;
@property (retain) NSNumber * ORDER_BY;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_ESS_CLOG_CONTRACT_TERM : NSObject {
	
/* elements */
	NSMutableArray *V_ESS_CLOG_CONTRACT_TERM;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_ESS_CLOG_CONTRACT_TERM *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ESS_CLOG_CONTRACT_TERM:(emp_tns1_V_ESS_CLOG_CONTRACT_TERM *)toAdd;
@property (readonly) NSMutableArray * V_ESS_CLOG_CONTRACT_TERM;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_TB_ESS_SYSTEM_PARAMETER : NSObject {
	
/* elements */
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * Flex1;
	NSNumber * Flex10;
	USBoolean * Flex11;
	USBoolean * Flex12;
	USBoolean * Flex13;
	USBoolean * Flex14;
	NSNumber * Flex15;
	NSNumber * Flex16;
	NSNumber * Flex17;
	NSNumber * Flex18;
	NSString * Flex19;
	NSString * Flex2;
	NSString * Flex20;
	NSString * Flex3;
	NSDate * Flex4;
	NSDate * Flex5;
	NSDate * Flex6;
	NSNumber * Flex7;
	NSNumber * Flex8;
	NSNumber * Flex9;
	NSNumber * IntFlex1;
	NSNumber * IntFlex2;
	NSNumber * IntFlex3;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * SysParamCode;
	NSNumber * SysParamCompanyId;
	NSString * SysParamDesc;
	NSNumber * SysParamId;
	NSString * SysParamKey;
	NSString * SysParamNameEN;
	NSString * SysParamNameVN;
	NSNumber * SysParamOrder;
	NSString * SysParamType;
	NSString * SysParamValue;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_TB_ESS_SYSTEM_PARAMETER *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Flex1;
@property (retain) NSNumber * Flex10;
@property (retain) USBoolean * Flex11;
@property (retain) USBoolean * Flex12;
@property (retain) USBoolean * Flex13;
@property (retain) USBoolean * Flex14;
@property (retain) NSNumber * Flex15;
@property (retain) NSNumber * Flex16;
@property (retain) NSNumber * Flex17;
@property (retain) NSNumber * Flex18;
@property (retain) NSString * Flex19;
@property (retain) NSString * Flex2;
@property (retain) NSString * Flex20;
@property (retain) NSString * Flex3;
@property (retain) NSDate * Flex4;
@property (retain) NSDate * Flex5;
@property (retain) NSDate * Flex6;
@property (retain) NSNumber * Flex7;
@property (retain) NSNumber * Flex8;
@property (retain) NSNumber * Flex9;
@property (retain) NSNumber * IntFlex1;
@property (retain) NSNumber * IntFlex2;
@property (retain) NSNumber * IntFlex3;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * SysParamCode;
@property (retain) NSNumber * SysParamCompanyId;
@property (retain) NSString * SysParamDesc;
@property (retain) NSNumber * SysParamId;
@property (retain) NSString * SysParamKey;
@property (retain) NSString * SysParamNameEN;
@property (retain) NSString * SysParamNameVN;
@property (retain) NSNumber * SysParamOrder;
@property (retain) NSString * SysParamType;
@property (retain) NSString * SysParamValue;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfTB_ESS_SYSTEM_PARAMETER : NSObject {
	
/* elements */
	NSMutableArray *TB_ESS_SYSTEM_PARAMETER;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfTB_ESS_SYSTEM_PARAMETER *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTB_ESS_SYSTEM_PARAMETER:(emp_tns1_TB_ESS_SYSTEM_PARAMETER *)toAdd;
@property (readonly) NSMutableArray * TB_ESS_SYSTEM_PARAMETER;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_Employee : NSObject {
	
/* elements */
	NSDate * BirthDay;
	NSString * CAREER_NAME_EN;
	NSString * CAREER_NAME_VN;
	NSString * CLOG_CAREER_CODE;
	NSNumber * CompanyId;
	NSString * Email;
	NSString * EmpWorkingStatusCode;
	NSNumber * EmpWorkingStatusId;
	NSString * EmpWorkingStatusName;
	NSString * EmpWorkingStatusNameEN;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * EmployeeTKCode;
	NSNumber * EmployeeTypeId;
	NSString * EmployeeTypeName;
	NSString * EmployeeTypeNameEN;
	NSDate * EndWorkingDate;
	NSDate * EntryDate;
	NSString * FullName;
	USBoolean * Gender;
	NSString * IdentityCardNo;
	NSString * ImageUrl;
	USBoolean * IsDeleted;
	USBoolean * IsOnlyYearOfBirthday;
	NSString * MobileNumber;
	NSString * OrgGroupCd;
	NSNumber * OrgGroupId;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobNameEN;
	NSNumber * OrgJobPositionId;
	NSString * OrgJobPositionName;
	NSString * OrgJobPositionNameEN;
	NSString * OrgJobTitleCode;
	NSNumber * OrgJobTitleId;
	NSString * OrgJobTitleLevelCode;
	NSString * OrgJobTitleLevelNameEN;
	NSString * OrgJobTitleLevelNameVN;
	NSString * OrgJobTitleNameEN;
	NSString * OrgJobTitleNameVN;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSNumber * OrgWorkLevelId;
	NSString * UnitName;
	NSString * UnitNameEN;
	NSString * WorkLevelName;
	NSString * WorkLevelNameEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_Employee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * BirthDay;
@property (retain) NSString * CAREER_NAME_EN;
@property (retain) NSString * CAREER_NAME_VN;
@property (retain) NSString * CLOG_CAREER_CODE;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Email;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSNumber * EmpWorkingStatusId;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmpWorkingStatusNameEN;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * EmployeeTKCode;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSString * EmployeeTypeName;
@property (retain) NSString * EmployeeTypeNameEN;
@property (retain) NSDate * EndWorkingDate;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) NSString * MobileNumber;
@property (retain) NSString * OrgGroupCd;
@property (retain) NSNumber * OrgGroupId;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobNameEN;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSString * OrgJobPositionNameEN;
@property (retain) NSString * OrgJobTitleCode;
@property (retain) NSNumber * OrgJobTitleId;
@property (retain) NSString * OrgJobTitleLevelCode;
@property (retain) NSString * OrgJobTitleLevelNameEN;
@property (retain) NSString * OrgJobTitleLevelNameVN;
@property (retain) NSString * OrgJobTitleNameEN;
@property (retain) NSString * OrgJobTitleNameVN;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * UnitName;
@property (retain) NSString * UnitNameEN;
@property (retain) NSString * WorkLevelName;
@property (retain) NSString * WorkLevelNameEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_Employee : NSObject {
	
/* elements */
	NSMutableArray *V_Employee;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_Employee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_Employee:(emp_tns1_V_Employee *)toAdd;
@property (readonly) NSMutableArray * V_Employee;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_EmpProfileJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * DecisionFile;
	NSString * DecisionName;
	NSString * DecisionNo;
	NSString * DirectReportToEmployeeCode;
	NSString * DirectReportToEmployeeFullName;
	NSNumber * DirectReportToEmployeeId;
	NSNumber * EmpProfileJobPositionId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ExpirationDate;
	NSString * FirstName;
	NSString * FullName;
	NSDate * ImplementationDate;
	USBoolean * IsActive;
	USBoolean * IsConcurrentPost;
	USBoolean * IsDeleted;
	USBoolean * IsMainPost;
	USBoolean * IsPromoted;
	NSString * JOB_TITLE_LEVEL_NAME_EN;
	NSString * JOB_TITLE_LEVEL_NAME_VN;
	NSString * LastName;
	NSDate * ModifiedDate;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobNameEN;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgJobPositionName;
	NSString * OrgJobPositionNameEN;
	NSString * OrgJobTitleCode;
	NSString * OrgJobTitleGroupCode;
	NSNumber * OrgJobTitleId;
	NSString * OrgJobTitleLevelCode;
	NSString * OrgJobTitleNameEN;
	NSString * OrgJobTitleNameVN;
	NSString * OrgUnitAddress;
	NSString * OrgUnitCode;
	NSString * OrgUnitDescription;
	NSNumber * OrgUnitId;
	NSString * OrgUnitName;
	NSString * OrgUnitNameEN;
	NSString * OrgUnitWorkLocation;
	NSString * OrgWorkLevelCode;
	NSString * OrgWorkLevelDescription;
	NSNumber * OrgWorkLevelId;
	NSString * OrgWorkLevelName;
	NSString * OrgWorkLevelNameEN;
	NSNumber * PercentParticipation;
	NSString * Reason;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_EmpProfileJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DecisionFile;
@property (retain) NSString * DecisionName;
@property (retain) NSString * DecisionNo;
@property (retain) NSString * DirectReportToEmployeeCode;
@property (retain) NSString * DirectReportToEmployeeFullName;
@property (retain) NSNumber * DirectReportToEmployeeId;
@property (retain) NSNumber * EmpProfileJobPositionId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsConcurrentPost;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainPost;
@property (retain) USBoolean * IsPromoted;
@property (retain) NSString * JOB_TITLE_LEVEL_NAME_EN;
@property (retain) NSString * JOB_TITLE_LEVEL_NAME_VN;
@property (retain) NSString * LastName;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobNameEN;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSString * OrgJobPositionNameEN;
@property (retain) NSString * OrgJobTitleCode;
@property (retain) NSString * OrgJobTitleGroupCode;
@property (retain) NSNumber * OrgJobTitleId;
@property (retain) NSString * OrgJobTitleLevelCode;
@property (retain) NSString * OrgJobTitleNameEN;
@property (retain) NSString * OrgJobTitleNameVN;
@property (retain) NSString * OrgUnitAddress;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSString * OrgUnitDescription;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitName;
@property (retain) NSString * OrgUnitNameEN;
@property (retain) NSString * OrgUnitWorkLocation;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSString * OrgWorkLevelDescription;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * OrgWorkLevelName;
@property (retain) NSString * OrgWorkLevelNameEN;
@property (retain) NSNumber * PercentParticipation;
@property (retain) NSString * Reason;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_EmpProfileJobPosition : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_EmpProfileJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileJobPosition:(emp_tns1_V_EmpProfileJobPosition *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_EmpBasicProfile : NSObject {
	
/* elements */
	NSString * BankAccountNo;
	NSString * BankBranch;
	NSString * BankBranchName;
	NSString * BankName;
	NSDate * BirthDay;
	NSString * BirthPlace;
	NSNumber * BirthPlaceId;
	NSNumber * CLogBankBranchId;
	NSNumber * CLogBankId;
	NSString * CVUrl;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * CulturalLevel;
	NSNumber * CulturalLevelId;
	NSString * EducationLevel;
	NSNumber * EducationLevelId;
	NSDate * EmpBasicProfileCreateDate;
	NSNumber * EmpBasicProfileId;
	USBoolean * EmpBasicProfileIsDeleted;
	NSDate * EmpBasicProfileModifiedDate;
	NSString * EmpWorkingStatusCode;
	NSNumber * EmpWorkingStatusId;
	NSString * EmpWorkingStatusName;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EmployeeTypeId;
	NSDate * EndWorkingDate;
	NSDate * EntryDate;
	NSString * Ethnicity;
	NSNumber * EthnicityId;
	NSString * FirstName;
	NSString * FullName;
	USBoolean * Gender;
	NSString * HomeTown;
	NSNumber * HomeTownId;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	NSString * ImageUrl;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	USBoolean * IsOnlyYearOfBirthday;
	NSString * LastName;
	NSString * MainLanguage;
	NSNumber * MainLanguageId;
	NSString * Major;
	NSNumber * MajorId;
	NSString * MaritalStatus;
	NSNumber * MaritalStatusId;
	NSDate * ModifiedDate;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSDate * OfficialDate;
	NSString * PITCode;
	NSString * PITIssuedBy;
	NSDate * PassportDateOfExpire;
	NSDate * PassportDateOfIssue;
	NSString * PassportNo;
	NSString * PassportPlaceOfIssue;
	NSDate * ProbationaryDate;
	NSNumber * ProbationarySalary;
	NSString * Religion;
	NSNumber * ReligionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_EmpBasicProfile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * BankAccountNo;
@property (retain) NSString * BankBranch;
@property (retain) NSString * BankBranchName;
@property (retain) NSString * BankName;
@property (retain) NSDate * BirthDay;
@property (retain) NSString * BirthPlace;
@property (retain) NSNumber * BirthPlaceId;
@property (retain) NSNumber * CLogBankBranchId;
@property (retain) NSNumber * CLogBankId;
@property (retain) NSString * CVUrl;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * CulturalLevel;
@property (retain) NSNumber * CulturalLevelId;
@property (retain) NSString * EducationLevel;
@property (retain) NSNumber * EducationLevelId;
@property (retain) NSDate * EmpBasicProfileCreateDate;
@property (retain) NSNumber * EmpBasicProfileId;
@property (retain) USBoolean * EmpBasicProfileIsDeleted;
@property (retain) NSDate * EmpBasicProfileModifiedDate;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSNumber * EmpWorkingStatusId;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSDate * EndWorkingDate;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * Ethnicity;
@property (retain) NSNumber * EthnicityId;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * HomeTown;
@property (retain) NSNumber * HomeTownId;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) NSString * LastName;
@property (retain) NSString * MainLanguage;
@property (retain) NSNumber * MainLanguageId;
@property (retain) NSString * Major;
@property (retain) NSNumber * MajorId;
@property (retain) NSString * MaritalStatus;
@property (retain) NSNumber * MaritalStatusId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSDate * OfficialDate;
@property (retain) NSString * PITCode;
@property (retain) NSString * PITIssuedBy;
@property (retain) NSDate * PassportDateOfExpire;
@property (retain) NSDate * PassportDateOfIssue;
@property (retain) NSString * PassportNo;
@property (retain) NSString * PassportPlaceOfIssue;
@property (retain) NSDate * ProbationaryDate;
@property (retain) NSNumber * ProbationarySalary;
@property (retain) NSString * Religion;
@property (retain) NSNumber * ReligionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_EmpBasicProfile : NSObject {
	
/* elements */
	NSMutableArray *V_EmpBasicProfile;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_EmpBasicProfile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpBasicProfile:(emp_tns1_V_EmpBasicProfile *)toAdd;
@property (readonly) NSMutableArray * V_EmpBasicProfile;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_EmpBasicInformation : NSObject {
	
/* elements */
	NSString * Address;
	NSNumber * AddressId;
	NSString * BankAccountNo;
	NSString * BankBranch;
	NSString * BankName;
	NSDate * BirthDay;
	NSString * BirthPlace;
	NSNumber * BirthPlaceId;
	NSString * CVUrl;
	NSString * CodeAndFullName;
	NSNumber * CompanyId;
	NSString * ContactAddress;
	NSNumber * ContactAddressId;
	NSString * CulturalLevel;
	NSNumber * CulturalLevelId;
	NSNumber * EducationLevelId;
	NSString * EducationLevelName;
	NSString * EducationLevelNameEN;
	NSString * Email;
	NSNumber * EmpBasicProfileId;
	NSString * EmpWorkingStatusCode;
	NSString * EmpWorkingStatusName;
	NSString * EmpWorkingStatusNameEN;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EmployeeTypeId;
	NSString * EmployeeTypeName;
	NSString * EmployeeTypeNameEN;
	NSDate * EntryDate;
	NSString * Ethnicity;
	NSNumber * EthnicityId;
	NSString * FirstName;
	NSString * FullName;
	USBoolean * Gender;
	NSString * HomePhone;
	NSString * HomeTown;
	NSNumber * HomeTownId;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	NSString * ImageUrl;
	USBoolean * IsConcurrentPost;
	USBoolean * IsDeleted;
	USBoolean * IsOnlyYearOfBirthday;
	NSString * LastName;
	NSString * MainLanguage;
	NSNumber * MainLanguageId;
	NSString * Major;
	NSNumber * MajorId;
	NSNumber * MaritalStatusId;
	NSString * MaritalStatusValue;
	NSString * MobileNumber;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSString * OfficePhone;
	NSDate * OfficialDate;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobNameEN;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgJobPositionName;
	NSNumber * OrgJobTitleId;
	NSString * OrgJobTitleLevelCode;
	NSString * OrgJobTitleNameEN;
	NSString * OrgJobTitleNameVN;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgUnitName;
	NSString * OrgWorkLevelCode;
	NSString * OrgWorkLevelDescription;
	NSNumber * OrgWorkLevelId;
	NSString * OrgWorkLevelName;
	NSString * PITCode;
	NSString * PITDateOfIssue;
	NSString * PITIssuedBy;
	NSDate * PassportDateOfExpire;
	NSDate * PassportDateOfIssue;
	NSString * PassportNo;
	NSString * PassportPlaceOfIssue;
	NSString * PermanentAddress;
	NSNumber * PermanentAddressId;
	NSString * PositionName;
	NSString * PositionNameEN;
	NSDate * ProbationaryDate;
	NSDate * ProbationaryEndDate;
	NSNumber * ProbationarySalary;
	NSString * Religion;
	NSNumber * ReligionId;
	NSString * TemporaryAddress;
	NSNumber * TemporaryAddressId;
	NSString * TimeWorkingFormCode;
	NSNumber * TimeWorkingFormId;
	NSString * UnitName;
	NSString * UnitNameEN;
	NSString * WorkLevelDescription;
	NSString * WorkLevelName;
	NSString * WorkLevelNameEN;
	NSString * WorkLocation;
	NSDate * WorkingFormImplementationDate;
	NSString * WorkingFormTypeCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_EmpBasicInformation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSNumber * AddressId;
@property (retain) NSString * BankAccountNo;
@property (retain) NSString * BankBranch;
@property (retain) NSString * BankName;
@property (retain) NSDate * BirthDay;
@property (retain) NSString * BirthPlace;
@property (retain) NSNumber * BirthPlaceId;
@property (retain) NSString * CVUrl;
@property (retain) NSString * CodeAndFullName;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ContactAddress;
@property (retain) NSNumber * ContactAddressId;
@property (retain) NSString * CulturalLevel;
@property (retain) NSNumber * CulturalLevelId;
@property (retain) NSNumber * EducationLevelId;
@property (retain) NSString * EducationLevelName;
@property (retain) NSString * EducationLevelNameEN;
@property (retain) NSString * Email;
@property (retain) NSNumber * EmpBasicProfileId;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmpWorkingStatusNameEN;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSString * EmployeeTypeName;
@property (retain) NSString * EmployeeTypeNameEN;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * Ethnicity;
@property (retain) NSNumber * EthnicityId;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * HomePhone;
@property (retain) NSString * HomeTown;
@property (retain) NSNumber * HomeTownId;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsConcurrentPost;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) NSString * LastName;
@property (retain) NSString * MainLanguage;
@property (retain) NSNumber * MainLanguageId;
@property (retain) NSString * Major;
@property (retain) NSNumber * MajorId;
@property (retain) NSNumber * MaritalStatusId;
@property (retain) NSString * MaritalStatusValue;
@property (retain) NSString * MobileNumber;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSString * OfficePhone;
@property (retain) NSDate * OfficialDate;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobNameEN;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSNumber * OrgJobTitleId;
@property (retain) NSString * OrgJobTitleLevelCode;
@property (retain) NSString * OrgJobTitleNameEN;
@property (retain) NSString * OrgJobTitleNameVN;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitName;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSString * OrgWorkLevelDescription;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * OrgWorkLevelName;
@property (retain) NSString * PITCode;
@property (retain) NSString * PITDateOfIssue;
@property (retain) NSString * PITIssuedBy;
@property (retain) NSDate * PassportDateOfExpire;
@property (retain) NSDate * PassportDateOfIssue;
@property (retain) NSString * PassportNo;
@property (retain) NSString * PassportPlaceOfIssue;
@property (retain) NSString * PermanentAddress;
@property (retain) NSNumber * PermanentAddressId;
@property (retain) NSString * PositionName;
@property (retain) NSString * PositionNameEN;
@property (retain) NSDate * ProbationaryDate;
@property (retain) NSDate * ProbationaryEndDate;
@property (retain) NSNumber * ProbationarySalary;
@property (retain) NSString * Religion;
@property (retain) NSNumber * ReligionId;
@property (retain) NSString * TemporaryAddress;
@property (retain) NSNumber * TemporaryAddressId;
@property (retain) NSString * TimeWorkingFormCode;
@property (retain) NSNumber * TimeWorkingFormId;
@property (retain) NSString * UnitName;
@property (retain) NSString * UnitNameEN;
@property (retain) NSString * WorkLevelDescription;
@property (retain) NSString * WorkLevelName;
@property (retain) NSString * WorkLevelNameEN;
@property (retain) NSString * WorkLocation;
@property (retain) NSDate * WorkingFormImplementationDate;
@property (retain) NSString * WorkingFormTypeCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_EmpBasicInformation : NSObject {
	
/* elements */
	NSMutableArray *V_EmpBasicInformation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_EmpBasicInformation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpBasicInformation:(emp_tns1_V_EmpBasicInformation *)toAdd;
@property (readonly) NSMutableArray * V_EmpBasicInformation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_EmpProfileDegree : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	NSNumber * EmpProfileDegreeId;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsValId;
	NSDate * ModifiedDate;
	NSString * OrgDegreeDescription;
	NSNumber * OrgDegreeId;
	NSString * OrgDegreeName;
	NSNumber * OrgDegreeRankId;
	NSString * OrgDegreeRankName;
	NSString * Other;
	NSString * PlaceIssue;
	NSString * ROW_ID;
	NSNumber * RelateRowId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_EmpProfileDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) NSNumber * EmpProfileDegreeId;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OrgDegreeDescription;
@property (retain) NSNumber * OrgDegreeId;
@property (retain) NSString * OrgDegreeName;
@property (retain) NSNumber * OrgDegreeRankId;
@property (retain) NSString * OrgDegreeRankName;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) NSString * ROW_ID;
@property (retain) NSNumber * RelateRowId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_EmpProfileDegree : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileDegree;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_EmpProfileDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileDegree:(emp_tns1_V_EmpProfileDegree *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileDegree;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_TimeRequestMasTer : NSObject {
	
/* elements */
	NSString * Address;
	NSString * ApprovalCode;
	NSDate * ApprovalDate;
	NSString * ApprovalFullName;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSString * ContactPhone;
	NSString * Email;
	NSString * EmpWorkingStatusCode;
	NSString * EmployeeCode;
	NSString * EmployeeFullName;
	NSNumber * EmployeeId;
	NSString * EmployeeTypeName;
	NSString * EmployeeTypeNameEN;
	NSDate * EndDate;
	NSString * ImageUrl;
	NSString * MobileNumber;
	NSNumber * OrgJobPositionId;
	NSString * OrgJobPositionName;
	NSString * OrgJobPositionNameEN;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * Reason;
	NSDate * StartDate;
	NSNumber * Status;
	NSString * SysParamNameEN;
	NSString * SysParamNameVN;
	NSString * TimeRequestMasTerCode;
	NSNumber * TimeRequestMasTerId;
	NSString * Type;
	NSString * UnitName;
	NSString * UnitNameEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_TimeRequestMasTer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSString * ApprovalCode;
@property (retain) NSDate * ApprovalDate;
@property (retain) NSString * ApprovalFullName;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ContactPhone;
@property (retain) NSString * Email;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSString * EmployeeCode;
@property (retain) NSString * EmployeeFullName;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * EmployeeTypeName;
@property (retain) NSString * EmployeeTypeNameEN;
@property (retain) NSDate * EndDate;
@property (retain) NSString * ImageUrl;
@property (retain) NSString * MobileNumber;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSString * OrgJobPositionNameEN;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * Reason;
@property (retain) NSDate * StartDate;
@property (retain) NSNumber * Status;
@property (retain) NSString * SysParamNameEN;
@property (retain) NSString * SysParamNameVN;
@property (retain) NSString * TimeRequestMasTerCode;
@property (retain) NSNumber * TimeRequestMasTerId;
@property (retain) NSString * Type;
@property (retain) NSString * UnitName;
@property (retain) NSString * UnitNameEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_TimeRequestMasTer : NSObject {
	
/* elements */
	NSMutableArray *V_TimeRequestMasTer;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_TimeRequestMasTer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_TimeRequestMasTer:(emp_tns1_V_TimeRequestMasTer *)toAdd;
@property (readonly) NSMutableArray * V_TimeRequestMasTer;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_TimeRequestMasTer : NSObject {
	
/* elements */
	NSDate * ApprovalDate;
	NSString * ApproverCode;
	NSNumber * ApproverId;
	NSString * Code;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * Flex1;
	NSNumber * Flex10;
	NSNumber * Flex11;
	NSNumber * Flex12;
	NSNumber * Flex13;
	USBoolean * Flex14;
	USBoolean * Flex15;
	USBoolean * Flex16;
	USBoolean * Flex17;
	NSDate * Flex18;
	NSDate * Flex19;
	NSString * Flex2;
	NSDate * Flex20;
	NSDate * Flex21;
	NSNumber * Flex22;
	NSNumber * Flex23;
	NSString * Flex24;
	NSString * Flex25;
	NSString * Flex26;
	NSString * Flex27;
	NSDate * Flex28;
	NSDate * Flex29;
	NSString * Flex3;
	NSString * Flex30;
	NSString * Flex31;
	NSString * Flex32;
	NSString * Flex33;
	NSString * Flex34;
	NSString * Flex35;
	NSString * Flex36;
	NSString * Flex37;
	NSString * Flex38;
	NSString * Flex39;
	NSDate * Flex4;
	NSNumber * Flex40;
	NSNumber * Flex41;
	NSNumber * Flex42;
	NSNumber * Flex43;
	NSNumber * Flex44;
	NSNumber * Flex5;
	NSNumber * Flex6;
	NSNumber * Flex7;
	NSNumber * Flex8;
	NSNumber * Flex9;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * Status;
	NSNumber * TimeRequestMasTerId;
	NSString * Type;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_TimeRequestMasTer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ApprovalDate;
@property (retain) NSString * ApproverCode;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * Code;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * Flex1;
@property (retain) NSNumber * Flex10;
@property (retain) NSNumber * Flex11;
@property (retain) NSNumber * Flex12;
@property (retain) NSNumber * Flex13;
@property (retain) USBoolean * Flex14;
@property (retain) USBoolean * Flex15;
@property (retain) USBoolean * Flex16;
@property (retain) USBoolean * Flex17;
@property (retain) NSDate * Flex18;
@property (retain) NSDate * Flex19;
@property (retain) NSString * Flex2;
@property (retain) NSDate * Flex20;
@property (retain) NSDate * Flex21;
@property (retain) NSNumber * Flex22;
@property (retain) NSNumber * Flex23;
@property (retain) NSString * Flex24;
@property (retain) NSString * Flex25;
@property (retain) NSString * Flex26;
@property (retain) NSString * Flex27;
@property (retain) NSDate * Flex28;
@property (retain) NSDate * Flex29;
@property (retain) NSString * Flex3;
@property (retain) NSString * Flex30;
@property (retain) NSString * Flex31;
@property (retain) NSString * Flex32;
@property (retain) NSString * Flex33;
@property (retain) NSString * Flex34;
@property (retain) NSString * Flex35;
@property (retain) NSString * Flex36;
@property (retain) NSString * Flex37;
@property (retain) NSString * Flex38;
@property (retain) NSString * Flex39;
@property (retain) NSDate * Flex4;
@property (retain) NSNumber * Flex40;
@property (retain) NSNumber * Flex41;
@property (retain) NSNumber * Flex42;
@property (retain) NSNumber * Flex43;
@property (retain) NSNumber * Flex44;
@property (retain) NSNumber * Flex5;
@property (retain) NSNumber * Flex6;
@property (retain) NSNumber * Flex7;
@property (retain) NSNumber * Flex8;
@property (retain) NSNumber * Flex9;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * TimeRequestMasTerId;
@property (retain) NSString * Type;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfTimeRequestMasTer : NSObject {
	
/* elements */
	NSMutableArray *TimeRequestMasTer;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfTimeRequestMasTer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeRequestMasTer:(emp_tns1_TimeRequestMasTer *)toAdd;
@property (readonly) NSMutableArray * TimeRequestMasTer;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpOffProfile : NSObject {
	
/* elements */
	NSDate * ApproveDate;
	NSString * ApproveNote;
	NSString * ApproverCode;
	NSNumber * ApproverId;
	NSNumber * CLogEmpOffTypeId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSString * DecisionCode;
	NSString * DecisionFile;
	NSString * DecisionName;
	NSNumber * EmpOffProfileId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * HICardExpireDate;
	NSDate * HandoverDate;
	USBoolean * IsDeleted;
	USBoolean * IsReturnEquipment;
	USBoolean * IsReturnHICard;
	USBoolean * IsViolatedRequest;
	NSDate * LastWorkingDate;
	NSNumber * LeaveDaysNotUsed;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * OffCoefficientAllowance;
	NSDate * OffFromDate;
	NSDate * OffRequestDate;
	NSString * Reason;
	NSDate * ReturnHICardDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpOffProfile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ApproveDate;
@property (retain) NSString * ApproveNote;
@property (retain) NSString * ApproverCode;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CLogEmpOffTypeId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSString * DecisionCode;
@property (retain) NSString * DecisionFile;
@property (retain) NSString * DecisionName;
@property (retain) NSNumber * EmpOffProfileId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * HICardExpireDate;
@property (retain) NSDate * HandoverDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsReturnEquipment;
@property (retain) USBoolean * IsReturnHICard;
@property (retain) USBoolean * IsViolatedRequest;
@property (retain) NSDate * LastWorkingDate;
@property (retain) NSNumber * LeaveDaysNotUsed;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * OffCoefficientAllowance;
@property (retain) NSDate * OffFromDate;
@property (retain) NSDate * OffRequestDate;
@property (retain) NSString * Reason;
@property (retain) NSDate * ReturnHICardDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_EmpOffProfile : NSObject {
	
/* elements */
	NSDate * ApproveDate;
	NSString * ApproveNote;
	NSString * ApproverCode;
	NSString * ApproverFullName;
	NSNumber * ApproverId;
	NSString * CLogEmpOffTypeCode;
	NSNumber * CLogEmpOffTypeId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * DecisionCode;
	NSString * DecisionFile;
	NSString * DecisionName;
	NSNumber * EmpOffProfileId;
	NSString * EmpOffTypeName;
	NSNumber * EmpProfileJobPositionId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EmployeeTypeId;
	NSString * EmployeeTypeName;
	NSDate * EndWorkingDate;
	NSDate * EntryDate;
	NSString * FirstName;
	NSString * FullName;
	NSDate * HICardExpireDate;
	NSDate * HandoverDate;
	NSString * ImageUrl;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	USBoolean * IsMainPost;
	USBoolean * IsReturnEquipment;
	USBoolean * IsReturnHICard;
	USBoolean * IsViolatedRequest;
	NSString * LastName;
	NSDate * LastWorkingDate;
	NSNumber * LeaveDaysNotUsed;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * OffCoefficientAllowance;
	NSDate * OffFromDate;
	NSDate * OffRequestDate;
	NSDate * OfficialDate;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgJobPositionName;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgUnitName;
	NSString * OrgWorkLevelCode;
	NSNumber * OrgWorkLevelId;
	NSString * OrgWorkLevelName;
	NSDate * ProbationaryDate;
	NSString * Reason;
	NSDate * ReturnHICardDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_EmpOffProfile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ApproveDate;
@property (retain) NSString * ApproveNote;
@property (retain) NSString * ApproverCode;
@property (retain) NSString * ApproverFullName;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * CLogEmpOffTypeCode;
@property (retain) NSNumber * CLogEmpOffTypeId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * DecisionCode;
@property (retain) NSString * DecisionFile;
@property (retain) NSString * DecisionName;
@property (retain) NSNumber * EmpOffProfileId;
@property (retain) NSString * EmpOffTypeName;
@property (retain) NSNumber * EmpProfileJobPositionId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSString * EmployeeTypeName;
@property (retain) NSDate * EndWorkingDate;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) NSDate * HICardExpireDate;
@property (retain) NSDate * HandoverDate;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainPost;
@property (retain) USBoolean * IsReturnEquipment;
@property (retain) USBoolean * IsReturnHICard;
@property (retain) USBoolean * IsViolatedRequest;
@property (retain) NSString * LastName;
@property (retain) NSDate * LastWorkingDate;
@property (retain) NSNumber * LeaveDaysNotUsed;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * OffCoefficientAllowance;
@property (retain) NSDate * OffFromDate;
@property (retain) NSDate * OffRequestDate;
@property (retain) NSDate * OfficialDate;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitName;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * OrgWorkLevelName;
@property (retain) NSDate * ProbationaryDate;
@property (retain) NSString * Reason;
@property (retain) NSDate * ReturnHICardDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_EmpOffProfile : NSObject {
	
/* elements */
	NSMutableArray *V_EmpOffProfile;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_EmpOffProfile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpOffProfile:(emp_tns1_V_EmpOffProfile *)toAdd;
@property (readonly) NSMutableArray * V_EmpOffProfile;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogEquipment : NSObject {
	
/* elements */
	NSMutableArray *CLogEquipment;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogEquipment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogEquipment:(emp_tns1_CLogEquipment *)toAdd;
@property (readonly) NSMutableArray * CLogEquipment;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_ESS_CLOG_EQUIPMENT_TYPE : NSObject {
	
/* elements */
	NSNumber * CLOG_EQUIPMENT_TYPE_ID;
	NSNumber * COMPANY_ID;
	NSNumber * CREATED_BY;
	NSDate * CREATED_DATE;
	NSString * EQUIPMENT_TYPE_CODE;
	NSString * EQUIPMENT_TYPE_DESC;
	NSString * EQUIPMENT_TYPE_NAME_EN;
	NSString * EQUIPMENT_TYPE_NAME_VN;
	USBoolean * IS_DELETED;
	NSNumber * MODIFIED_BY;
	NSDate * MODIFIED_DATE;
	NSNumber * ORDER_BY;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_ESS_CLOG_EQUIPMENT_TYPE *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLOG_EQUIPMENT_TYPE_ID;
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSNumber * CREATED_BY;
@property (retain) NSDate * CREATED_DATE;
@property (retain) NSString * EQUIPMENT_TYPE_CODE;
@property (retain) NSString * EQUIPMENT_TYPE_DESC;
@property (retain) NSString * EQUIPMENT_TYPE_NAME_EN;
@property (retain) NSString * EQUIPMENT_TYPE_NAME_VN;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSNumber * MODIFIED_BY;
@property (retain) NSDate * MODIFIED_DATE;
@property (retain) NSNumber * ORDER_BY;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_ESS_CLOG_EQUIPMENT_TYPE : NSObject {
	
/* elements */
	NSMutableArray *V_ESS_CLOG_EQUIPMENT_TYPE;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_ESS_CLOG_EQUIPMENT_TYPE *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ESS_CLOG_EQUIPMENT_TYPE:(emp_tns1_V_ESS_CLOG_EQUIPMENT_TYPE *)toAdd;
@property (readonly) NSMutableArray * V_ESS_CLOG_EQUIPMENT_TYPE;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_ESS_CLOG_ASSETS_TYPE : NSObject {
	
/* elements */
	NSString * ASSETS_TYPE_CODE;
	NSString * ASSETS_TYPE_DESC;
	NSString * ASSETS_TYPE_NAME_EN;
	NSString * ASSETS_TYPE_NAME_VN;
	NSNumber * CLOG_ASSETS_TYPE_ID;
	NSNumber * COMPANY_ID;
	NSNumber * CREATED_BY;
	NSDate * CREATED_DATE;
	USBoolean * IS_DELETED;
	NSNumber * MODIFIED_BY;
	NSDate * MODIFIED_DATE;
	NSNumber * ORDER_BY;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_ESS_CLOG_ASSETS_TYPE *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ASSETS_TYPE_CODE;
@property (retain) NSString * ASSETS_TYPE_DESC;
@property (retain) NSString * ASSETS_TYPE_NAME_EN;
@property (retain) NSString * ASSETS_TYPE_NAME_VN;
@property (retain) NSNumber * CLOG_ASSETS_TYPE_ID;
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSNumber * CREATED_BY;
@property (retain) NSDate * CREATED_DATE;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSNumber * MODIFIED_BY;
@property (retain) NSDate * MODIFIED_DATE;
@property (retain) NSNumber * ORDER_BY;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_ESS_CLOG_ASSETS_TYPE : NSObject {
	
/* elements */
	NSMutableArray *V_ESS_CLOG_ASSETS_TYPE;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_ESS_CLOG_ASSETS_TYPE *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ESS_CLOG_ASSETS_TYPE:(emp_tns1_V_ESS_CLOG_ASSETS_TYPE *)toAdd;
@property (readonly) NSMutableArray * V_ESS_CLOG_ASSETS_TYPE;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_EmpProfileEquipment : NSObject {
	
/* elements */
	NSString * ASSETS_TYPE_CODE;
	NSString * ASSETS_TYPE_NAME_EN;
	NSString * ASSETS_TYPE_NAME_VN;
	NSDate * AddDate;
	NSString * CLogCurrencyCode;
	NSNumber * CLogCurrencyId;
	NSString * CLogCurrencyName;
	NSString * CLogEquipmentCode;
	NSNumber * CLogEquipmentId;
	NSString * CLogEquipmentName;
	NSString * CLogEquipmentNote;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSDate * CreatedDate;
	NSDate * DateHandover;
	NSDate * DateReceived;
	NSNumber * Depreciation;
	NSString * Description;
	NSString * EQUIPMENT_DESC;
	NSNumber * EQUIPMENT_IN_PLAN;
	NSString * EQUIPMENT_NAME;
	NSNumber * EQUIPMENT_TYPE;
	NSString * EQUIPMENT_TYPE_CODE;
	NSString * EQUIPMENT_TYPE_NAME_EN;
	NSString * EQUIPMENT_TYPE_NAME_VN;
	NSNumber * EmpProfileEquipmentId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * FullName;
	USBoolean * IsBigValue;
	USBoolean * IsDeleted;
	USBoolean * IsHandover;
	USBoolean * IsReturnToComp;
	NSDate * ModifiedDate;
	USBoolean * MustHandover;
	NSNumber * Number;
	NSNumber * OrgUnitId;
	NSNumber * REQ_DETAIL_ID;
	NSNumber * REQ_MASTER_ID;
	NSString * REQ_MASTER_NAME;
	NSNumber * REQ_STATUS;
	NSString * ReceiverCode;
	NSString * ReceiverFullName;
	NSNumber * ReceiverId;
	NSString * StateHandover;
	NSString * StateReceived;
	NSString * UnitName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_EmpProfileEquipment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ASSETS_TYPE_CODE;
@property (retain) NSString * ASSETS_TYPE_NAME_EN;
@property (retain) NSString * ASSETS_TYPE_NAME_VN;
@property (retain) NSDate * AddDate;
@property (retain) NSString * CLogCurrencyCode;
@property (retain) NSNumber * CLogCurrencyId;
@property (retain) NSString * CLogCurrencyName;
@property (retain) NSString * CLogEquipmentCode;
@property (retain) NSNumber * CLogEquipmentId;
@property (retain) NSString * CLogEquipmentName;
@property (retain) NSString * CLogEquipmentNote;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateHandover;
@property (retain) NSDate * DateReceived;
@property (retain) NSNumber * Depreciation;
@property (retain) NSString * Description;
@property (retain) NSString * EQUIPMENT_DESC;
@property (retain) NSNumber * EQUIPMENT_IN_PLAN;
@property (retain) NSString * EQUIPMENT_NAME;
@property (retain) NSNumber * EQUIPMENT_TYPE;
@property (retain) NSString * EQUIPMENT_TYPE_CODE;
@property (retain) NSString * EQUIPMENT_TYPE_NAME_EN;
@property (retain) NSString * EQUIPMENT_TYPE_NAME_VN;
@property (retain) NSNumber * EmpProfileEquipmentId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * FullName;
@property (retain) USBoolean * IsBigValue;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsHandover;
@property (retain) USBoolean * IsReturnToComp;
@property (retain) NSDate * ModifiedDate;
@property (retain) USBoolean * MustHandover;
@property (retain) NSNumber * Number;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * REQ_DETAIL_ID;
@property (retain) NSNumber * REQ_MASTER_ID;
@property (retain) NSString * REQ_MASTER_NAME;
@property (retain) NSNumber * REQ_STATUS;
@property (retain) NSString * ReceiverCode;
@property (retain) NSString * ReceiverFullName;
@property (retain) NSNumber * ReceiverId;
@property (retain) NSString * StateHandover;
@property (retain) NSString * StateReceived;
@property (retain) NSString * UnitName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_EmpProfileEquipment : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileEquipment;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_EmpProfileEquipment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileEquipment:(emp_tns1_V_EmpProfileEquipment *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileEquipment;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_EmpProfileAssets : NSObject {
	
/* elements */
	NSString * ASSETS_TYPE_CODE;
	NSString * ASSETS_TYPE_NAME_EN;
	NSString * ASSETS_TYPE_NAME_VN;
	NSDate * AddDate;
	NSString * CLogCurrencyCode;
	NSNumber * CLogCurrencyId;
	NSString * CLogCurrencyName;
	NSString * CLogEquipmentCode;
	NSNumber * CLogEquipmentId;
	NSString * CLogEquipmentName;
	NSString * CLogEquipmentNote;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSDate * CreatedDate;
	NSDate * DateHandover;
	NSDate * DateReceived;
	NSNumber * Depreciation;
	NSString * Description;
	NSString * EQUIPMENT_DESC;
	NSNumber * EQUIPMENT_IN_PLAN;
	NSString * EQUIPMENT_NAME;
	NSNumber * EQUIPMENT_TYPE;
	NSString * EQUIPMENT_TYPE_CODE;
	NSString * EQUIPMENT_TYPE_NAME_EN;
	NSString * EQUIPMENT_TYPE_NAME_VN;
	NSNumber * EmpProfileEquipmentId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * FullName;
	USBoolean * IsBigValue;
	USBoolean * IsDeleted;
	USBoolean * IsHandover;
	USBoolean * IsReturnToComp;
	NSDate * ModifiedDate;
	USBoolean * MustHandover;
	NSNumber * Number;
	NSNumber * OrgUnitId;
	NSNumber * REQ_DETAIL_ID;
	NSNumber * REQ_MASTER_ID;
	NSString * REQ_MASTER_NAME;
	NSNumber * REQ_STATUS;
	NSString * ReceiverCode;
	NSString * ReceiverFullName;
	NSNumber * ReceiverId;
	NSString * StateHandover;
	NSString * StateReceived;
	NSString * UnitName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_EmpProfileAssets *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ASSETS_TYPE_CODE;
@property (retain) NSString * ASSETS_TYPE_NAME_EN;
@property (retain) NSString * ASSETS_TYPE_NAME_VN;
@property (retain) NSDate * AddDate;
@property (retain) NSString * CLogCurrencyCode;
@property (retain) NSNumber * CLogCurrencyId;
@property (retain) NSString * CLogCurrencyName;
@property (retain) NSString * CLogEquipmentCode;
@property (retain) NSNumber * CLogEquipmentId;
@property (retain) NSString * CLogEquipmentName;
@property (retain) NSString * CLogEquipmentNote;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateHandover;
@property (retain) NSDate * DateReceived;
@property (retain) NSNumber * Depreciation;
@property (retain) NSString * Description;
@property (retain) NSString * EQUIPMENT_DESC;
@property (retain) NSNumber * EQUIPMENT_IN_PLAN;
@property (retain) NSString * EQUIPMENT_NAME;
@property (retain) NSNumber * EQUIPMENT_TYPE;
@property (retain) NSString * EQUIPMENT_TYPE_CODE;
@property (retain) NSString * EQUIPMENT_TYPE_NAME_EN;
@property (retain) NSString * EQUIPMENT_TYPE_NAME_VN;
@property (retain) NSNumber * EmpProfileEquipmentId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * FullName;
@property (retain) USBoolean * IsBigValue;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsHandover;
@property (retain) USBoolean * IsReturnToComp;
@property (retain) NSDate * ModifiedDate;
@property (retain) USBoolean * MustHandover;
@property (retain) NSNumber * Number;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * REQ_DETAIL_ID;
@property (retain) NSNumber * REQ_MASTER_ID;
@property (retain) NSString * REQ_MASTER_NAME;
@property (retain) NSNumber * REQ_STATUS;
@property (retain) NSString * ReceiverCode;
@property (retain) NSString * ReceiverFullName;
@property (retain) NSNumber * ReceiverId;
@property (retain) NSString * StateHandover;
@property (retain) NSString * StateReceived;
@property (retain) NSString * UnitName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_EmpProfileAssets : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileAssets;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_EmpProfileAssets *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileAssets:(emp_tns1_V_EmpProfileAssets *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileAssets;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_ESS_CLOG_EQUIPMENT : NSObject {
	
/* elements */
	NSString * ASSETS_TYPE_CODE;
	NSString * ASSETS_TYPE_NAME_EN;
	NSString * ASSETS_TYPE_NAME_VN;
	NSDate * AddDate;
	NSString * CLogCurrencyCode;
	NSNumber * CLogCurrencyId;
	NSString * CLogCurrencyName;
	NSNumber * CLogEquipmentId;
	NSString * Code;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSNumber * Depreciation;
	NSString * EQUIPMENT_TYPE_CODE;
	NSString * EQUIPMENT_TYPE_NAME_EN;
	NSString * EQUIPMENT_TYPE_NAME_VN;
	USBoolean * IsBigValue;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * Note;
	NSNumber * Number;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_ESS_CLOG_EQUIPMENT *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ASSETS_TYPE_CODE;
@property (retain) NSString * ASSETS_TYPE_NAME_EN;
@property (retain) NSString * ASSETS_TYPE_NAME_VN;
@property (retain) NSDate * AddDate;
@property (retain) NSString * CLogCurrencyCode;
@property (retain) NSNumber * CLogCurrencyId;
@property (retain) NSString * CLogCurrencyName;
@property (retain) NSNumber * CLogEquipmentId;
@property (retain) NSString * Code;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSNumber * Depreciation;
@property (retain) NSString * EQUIPMENT_TYPE_CODE;
@property (retain) NSString * EQUIPMENT_TYPE_NAME_EN;
@property (retain) NSString * EQUIPMENT_TYPE_NAME_VN;
@property (retain) USBoolean * IsBigValue;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) NSNumber * Number;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_ESS_CLOG_EQUIPMENT : NSObject {
	
/* elements */
	NSMutableArray *V_ESS_CLOG_EQUIPMENT;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_ESS_CLOG_EQUIPMENT *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ESS_CLOG_EQUIPMENT:(emp_tns1_V_ESS_CLOG_EQUIPMENT *)toAdd;
@property (readonly) NSMutableArray * V_ESS_CLOG_EQUIPMENT;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_ESS_CLOG_ASSETS : NSObject {
	
/* elements */
	NSString * ASSETS_TYPE_CODE;
	NSString * ASSETS_TYPE_NAME_EN;
	NSString * ASSETS_TYPE_NAME_VN;
	NSDate * AddDate;
	NSString * CLogCurrencyCode;
	NSNumber * CLogCurrencyId;
	NSString * CLogCurrencyName;
	NSNumber * CLogEquipmentId;
	NSString * Code;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSNumber * Depreciation;
	NSString * EQUIPMENT_TYPE_CODE;
	NSString * EQUIPMENT_TYPE_NAME_EN;
	NSString * EQUIPMENT_TYPE_NAME_VN;
	USBoolean * IsBigValue;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * Note;
	NSNumber * Number;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_ESS_CLOG_ASSETS *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ASSETS_TYPE_CODE;
@property (retain) NSString * ASSETS_TYPE_NAME_EN;
@property (retain) NSString * ASSETS_TYPE_NAME_VN;
@property (retain) NSDate * AddDate;
@property (retain) NSString * CLogCurrencyCode;
@property (retain) NSNumber * CLogCurrencyId;
@property (retain) NSString * CLogCurrencyName;
@property (retain) NSNumber * CLogEquipmentId;
@property (retain) NSString * Code;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSNumber * Depreciation;
@property (retain) NSString * EQUIPMENT_TYPE_CODE;
@property (retain) NSString * EQUIPMENT_TYPE_NAME_EN;
@property (retain) NSString * EQUIPMENT_TYPE_NAME_VN;
@property (retain) USBoolean * IsBigValue;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) NSNumber * Number;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_ESS_CLOG_ASSETS : NSObject {
	
/* elements */
	NSMutableArray *V_ESS_CLOG_ASSETS;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_ESS_CLOG_ASSETS *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ESS_CLOG_ASSETS:(emp_tns1_V_ESS_CLOG_ASSETS *)toAdd;
@property (readonly) NSMutableArray * V_ESS_CLOG_ASSETS;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_OrgUnitAssets : NSObject {
	
/* elements */
	NSString * ASSETS_TYPE_CODE;
	NSString * ASSETS_TYPE_NAME_EN;
	NSString * ASSETS_TYPE_NAME_VN;
	NSDate * AddDate;
	NSString * CLogCurrencyCode;
	NSNumber * CLogCurrencyId;
	NSString * CLogCurrencyName;
	NSString * CLogEquipmentCode;
	NSNumber * CLogEquipmentId;
	NSString * CLogEquipmentName;
	NSString * CLogEquipmentNote;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSNumber * Depreciation;
	NSString * EQUIPMENT_TYPE_CODE;
	NSString * EQUIPMENT_TYPE_NAME_EN;
	NSString * EQUIPMENT_TYPE_NAME_VN;
	NSNumber * OrgUnitId;
	NSString * UnitName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_OrgUnitAssets *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ASSETS_TYPE_CODE;
@property (retain) NSString * ASSETS_TYPE_NAME_EN;
@property (retain) NSString * ASSETS_TYPE_NAME_VN;
@property (retain) NSDate * AddDate;
@property (retain) NSString * CLogCurrencyCode;
@property (retain) NSNumber * CLogCurrencyId;
@property (retain) NSString * CLogCurrencyName;
@property (retain) NSString * CLogEquipmentCode;
@property (retain) NSNumber * CLogEquipmentId;
@property (retain) NSString * CLogEquipmentName;
@property (retain) NSString * CLogEquipmentNote;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSNumber * Depreciation;
@property (retain) NSString * EQUIPMENT_TYPE_CODE;
@property (retain) NSString * EQUIPMENT_TYPE_NAME_EN;
@property (retain) NSString * EQUIPMENT_TYPE_NAME_VN;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * UnitName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_OrgUnitAssets : NSObject {
	
/* elements */
	NSMutableArray *V_OrgUnitAssets;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_OrgUnitAssets *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgUnitAssets:(emp_tns1_V_OrgUnitAssets *)toAdd;
@property (readonly) NSMutableArray * V_OrgUnitAssets;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_OrgUnitEquipment : NSObject {
	
/* elements */
	NSString * ASSETS_TYPE_CODE;
	NSString * ASSETS_TYPE_NAME_EN;
	NSString * ASSETS_TYPE_NAME_VN;
	NSDate * AddDate;
	NSString * CLogCurrencyCode;
	NSNumber * CLogCurrencyId;
	NSString * CLogCurrencyName;
	NSString * CLogEquipmentCode;
	NSNumber * CLogEquipmentId;
	NSString * CLogEquipmentName;
	NSString * CLogEquipmentNote;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSNumber * Depreciation;
	NSString * EQUIPMENT_TYPE_CODE;
	NSString * EQUIPMENT_TYPE_NAME_EN;
	NSString * EQUIPMENT_TYPE_NAME_VN;
	NSNumber * OrgUnitId;
	NSString * UnitName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_OrgUnitEquipment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ASSETS_TYPE_CODE;
@property (retain) NSString * ASSETS_TYPE_NAME_EN;
@property (retain) NSString * ASSETS_TYPE_NAME_VN;
@property (retain) NSDate * AddDate;
@property (retain) NSString * CLogCurrencyCode;
@property (retain) NSNumber * CLogCurrencyId;
@property (retain) NSString * CLogCurrencyName;
@property (retain) NSString * CLogEquipmentCode;
@property (retain) NSNumber * CLogEquipmentId;
@property (retain) NSString * CLogEquipmentName;
@property (retain) NSString * CLogEquipmentNote;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSNumber * Depreciation;
@property (retain) NSString * EQUIPMENT_TYPE_CODE;
@property (retain) NSString * EQUIPMENT_TYPE_NAME_EN;
@property (retain) NSString * EQUIPMENT_TYPE_NAME_VN;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * UnitName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_OrgUnitEquipment : NSObject {
	
/* elements */
	NSMutableArray *V_OrgUnitEquipment;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_OrgUnitEquipment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgUnitEquipment:(emp_tns1_V_OrgUnitEquipment *)toAdd;
@property (readonly) NSMutableArray * V_OrgUnitEquipment;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogCountry : NSObject {
	
/* elements */
	NSMutableArray *CLogCountry;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogCountry *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogCountry:(emp_tns1_CLogCountry *)toAdd;
@property (readonly) NSMutableArray * CLogCountry;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_ESS_ORG_DIRECT_REPORT : NSObject {
	
/* elements */
	NSString * ChildCode;
	NSString * ChildEmail;
	NSNumber * ChildEmpJobPositionId;
	NSNumber * ChildId;
	NSString * ChildJobPositionName;
	NSString * ChildMobileNumber;
	NSString * ChildName;
	NSString * ChildUnitName;
	NSNumber * CompanyId;
	NSString * DirectReportToEmployeeCode;
	NSString * ImageUrl;
	USBoolean * IsDeleted;
	NSString * ParentCode;
	NSNumber * ParentId;
	NSString * ParentName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_ESS_ORG_DIRECT_REPORT *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ChildCode;
@property (retain) NSString * ChildEmail;
@property (retain) NSNumber * ChildEmpJobPositionId;
@property (retain) NSNumber * ChildId;
@property (retain) NSString * ChildJobPositionName;
@property (retain) NSString * ChildMobileNumber;
@property (retain) NSString * ChildName;
@property (retain) NSString * ChildUnitName;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * DirectReportToEmployeeCode;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * ParentCode;
@property (retain) NSNumber * ParentId;
@property (retain) NSString * ParentName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_ESS_ORG_DIRECT_REPORT : NSObject {
	
/* elements */
	NSMutableArray *V_ESS_ORG_DIRECT_REPORT;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_ESS_ORG_DIRECT_REPORT *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ESS_ORG_DIRECT_REPORT:(emp_tns1_V_ESS_ORG_DIRECT_REPORT *)toAdd;
@property (readonly) NSMutableArray * V_ESS_ORG_DIRECT_REPORT;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SP_EMP_GET_VIEW_EMPLOYEE_Result : NSObject {
	
/* elements */
	NSDate * BirthDay;
	NSNumber * CompanyId;
	NSString * Email;
	NSString * EmpWorkingStatusCode;
	NSNumber * EmpWorkingStatusId;
	NSString * EmpWorkingStatusName;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EmployeeTypeId;
	NSString * EmployeeTypeName;
	NSDate * EndWorkingDate;
	NSDate * EntryDate;
	NSString * FullName;
	USBoolean * Gender;
	NSString * IdentityCardNo;
	NSString * ImageUrl;
	USBoolean * IsDeleted;
	USBoolean * IsOnlyYearOfBirthday;
	NSString * MobileNumber;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSNumber * OrgJobPositionId;
	NSString * OrgJobPositionName;
	NSNumber * OrgUnitId;
	NSString * UnitName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SP_EMP_GET_VIEW_EMPLOYEE_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * BirthDay;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Email;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSNumber * EmpWorkingStatusId;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSString * EmployeeTypeName;
@property (retain) NSDate * EndWorkingDate;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) NSString * MobileNumber;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * UnitName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSP_EMP_GET_VIEW_EMPLOYEE_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_EMP_GET_VIEW_EMPLOYEE_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSP_EMP_GET_VIEW_EMPLOYEE_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_EMP_GET_VIEW_EMPLOYEE_Result:(emp_tns1_SP_EMP_GET_VIEW_EMPLOYEE_Result *)toAdd;
@property (readonly) NSMutableArray * SP_EMP_GET_VIEW_EMPLOYEE_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_EmployeeProfile : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * BankAccountNo;
	NSString * BankBranch;
	NSString * BankName;
	NSDate * BirthDay;
	NSString * BirthPlace;
	NSNumber * BirthPlaceId;
	NSNumber * CLogBankBranchId;
	NSString * CVUrl;
	NSNumber * CompanyId;
	NSString * ContactAddress;
	NSNumber * ContactAddressId;
	NSDate * CreatedDate;
	NSString * CulturalLevel;
	NSNumber * CulturalLevelId;
	NSString * EducationLevel;
	NSNumber * EducationLevelId;
	NSString * Email;
	NSNumber * EmpBasicProfileCompanyId;
	NSDate * EmpBasicProfileCreatedDate;
	NSNumber * EmpBasicProfileId;
	USBoolean * EmpBasicProfileIsConcurrent;
	USBoolean * EmpBasicProfileIsDeleted;
	NSDate * EmpBasicProfileModifiedDate;
	NSNumber * EmpProfileContactCompanyId;
	NSDate * EmpProfileContactCreatedDate;
	NSNumber * EmpProfileContactId;
	USBoolean * EmpProfileContactIsConcurrent;
	USBoolean * EmpProfileContactIsDeleted;
	NSDate * EmpProfileContactModifiedDate;
	NSNumber * EmpWorkingStatusId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * EmployeeTKCode;
	NSNumber * EmployeeTypeId;
	NSDate * EndWorkingDate;
	NSDate * EntryDate;
	NSString * Ethnicity;
	NSNumber * EthnicityId;
	NSString * Fax;
	NSString * FirstName;
	NSString * FullName;
	USBoolean * Gender;
	NSString * HomePhone;
	NSString * HomeTown;
	NSNumber * HomeTownId;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	NSString * ImageUrl;
	USBoolean * IsDeleted;
	USBoolean * IsOnlyYearOfBirthday;
	NSString * LastName;
	NSString * MainLanguage;
	NSNumber * MainLanguageId;
	NSString * Major;
	NSNumber * MajorId;
	NSString * MaritalStatus;
	NSNumber * MaritalStatusId;
	NSString * MobileNumber;
	NSDate * ModifiedDate;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSString * OfficePhone;
	NSDate * OfficialDate;
	NSString * OrgGroupCd;
	NSNumber * OrgGroupId;
	NSString * PITCode;
	NSString * PITIssuedBy;
	NSDate * PassportDateOfExpire;
	NSDate * PassportDateOfIssue;
	NSString * PassportNo;
	NSString * PassportPlaceOfIssue;
	NSString * PermanentAddress;
	NSNumber * PermanentAddressId;
	NSDate * ProbationaryDate;
	NSDate * ProbationaryEndDate;
	NSNumber * ProbationarySalary;
	NSNumber * RelateRowId;
	NSString * Religion;
	NSNumber * ReligionId;
	NSString * TemporaryAddress;
	NSNumber * TemporaryAddressId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_EmployeeProfile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * BankAccountNo;
@property (retain) NSString * BankBranch;
@property (retain) NSString * BankName;
@property (retain) NSDate * BirthDay;
@property (retain) NSString * BirthPlace;
@property (retain) NSNumber * BirthPlaceId;
@property (retain) NSNumber * CLogBankBranchId;
@property (retain) NSString * CVUrl;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ContactAddress;
@property (retain) NSNumber * ContactAddressId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * CulturalLevel;
@property (retain) NSNumber * CulturalLevelId;
@property (retain) NSString * EducationLevel;
@property (retain) NSNumber * EducationLevelId;
@property (retain) NSString * Email;
@property (retain) NSNumber * EmpBasicProfileCompanyId;
@property (retain) NSDate * EmpBasicProfileCreatedDate;
@property (retain) NSNumber * EmpBasicProfileId;
@property (retain) USBoolean * EmpBasicProfileIsConcurrent;
@property (retain) USBoolean * EmpBasicProfileIsDeleted;
@property (retain) NSDate * EmpBasicProfileModifiedDate;
@property (retain) NSNumber * EmpProfileContactCompanyId;
@property (retain) NSDate * EmpProfileContactCreatedDate;
@property (retain) NSNumber * EmpProfileContactId;
@property (retain) USBoolean * EmpProfileContactIsConcurrent;
@property (retain) USBoolean * EmpProfileContactIsDeleted;
@property (retain) NSDate * EmpProfileContactModifiedDate;
@property (retain) NSNumber * EmpWorkingStatusId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * EmployeeTKCode;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSDate * EndWorkingDate;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * Ethnicity;
@property (retain) NSNumber * EthnicityId;
@property (retain) NSString * Fax;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * HomePhone;
@property (retain) NSString * HomeTown;
@property (retain) NSNumber * HomeTownId;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) NSString * LastName;
@property (retain) NSString * MainLanguage;
@property (retain) NSNumber * MainLanguageId;
@property (retain) NSString * Major;
@property (retain) NSNumber * MajorId;
@property (retain) NSString * MaritalStatus;
@property (retain) NSNumber * MaritalStatusId;
@property (retain) NSString * MobileNumber;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSString * OfficePhone;
@property (retain) NSDate * OfficialDate;
@property (retain) NSString * OrgGroupCd;
@property (retain) NSNumber * OrgGroupId;
@property (retain) NSString * PITCode;
@property (retain) NSString * PITIssuedBy;
@property (retain) NSDate * PassportDateOfExpire;
@property (retain) NSDate * PassportDateOfIssue;
@property (retain) NSString * PassportNo;
@property (retain) NSString * PassportPlaceOfIssue;
@property (retain) NSString * PermanentAddress;
@property (retain) NSNumber * PermanentAddressId;
@property (retain) NSDate * ProbationaryDate;
@property (retain) NSDate * ProbationaryEndDate;
@property (retain) NSNumber * ProbationarySalary;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * Religion;
@property (retain) NSNumber * ReligionId;
@property (retain) NSString * TemporaryAddress;
@property (retain) NSNumber * TemporaryAddressId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SP_GetAllParentEmployee_Result : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * Email;
	NSString * EmpJobPositionName;
	NSString * EmployeeCode;
	NSString * ImageUrl;
	USBoolean * IsDeleted;
	NSNumber * Level;
	NSNumber * OrgJobPositionId;
	NSNumber * OrgUnitId;
	NSString * OrgUnitName;
	NSString * ParentFullName;
	NSNumber * ParentId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SP_GetAllParentEmployee_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Email;
@property (retain) NSString * EmpJobPositionName;
@property (retain) NSString * EmployeeCode;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Level;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitName;
@property (retain) NSString * ParentFullName;
@property (retain) NSNumber * ParentId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSP_GetAllParentEmployee_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_GetAllParentEmployee_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSP_GetAllParentEmployee_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_GetAllParentEmployee_Result:(emp_tns1_SP_GetAllParentEmployee_Result *)toAdd;
@property (readonly) NSMutableArray * SP_GetAllParentEmployee_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogFamilyRelationship : NSObject {
	
/* elements */
	NSMutableArray *CLogFamilyRelationship;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogFamilyRelationship:(emp_tns1_CLogFamilyRelationship *)toAdd;
@property (readonly) NSMutableArray * CLogFamilyRelationship;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_EmpProfileWorkingExperience : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompanyName;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileWorkingExperienceId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	USBoolean * IsInternal;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * NotableAchievements;
	NSString * Note;
	NSNumber * OrgProjectTypeId;
	NSString * OrgProjectTypeName;
	NSString * OrgTimeInChargeDescription;
	NSNumber * OrgTimeInChargeId;
	NSString * ProjectName;
	NSString * RatingDescription;
	NSNumber * RatingValue;
	NSNumber * RelateRowId;
	NSString * Responsibilities;
	NSString * Role;
	NSDate * StartDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_EmpProfileWorkingExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompanyName;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileWorkingExperienceId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInternal;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * NotableAchievements;
@property (retain) NSString * Note;
@property (retain) NSNumber * OrgProjectTypeId;
@property (retain) NSString * OrgProjectTypeName;
@property (retain) NSString * OrgTimeInChargeDescription;
@property (retain) NSNumber * OrgTimeInChargeId;
@property (retain) NSString * ProjectName;
@property (retain) NSString * RatingDescription;
@property (retain) NSNumber * RatingValue;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * Responsibilities;
@property (retain) NSString * Role;
@property (retain) NSDate * StartDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_EmpProfileWorkingExperience : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileWorkingExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_EmpProfileWorkingExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileWorkingExperience:(emp_tns1_V_EmpProfileWorkingExperience *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileWorkingExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_EmpProfileSkill : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * DocumentRef;
	NSNumber * EmpProfileSkillId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * OrgSkillCode;
	NSString * OrgSkillDescription;
	NSNumber * OrgSkillId;
	NSString * OrgSkillName;
	NSNumber * OrgSkillPriority;
	NSString * OrgSkillPriorityName;
	NSNumber * OrgSkillTypeId;
	NSString * OrgSkillTypeName;
	NSString * RatingDescription;
	NSNumber * RatingId;
	NSNumber * RatingValue;
	NSNumber * RelateRowId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_EmpProfileSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DocumentRef;
@property (retain) NSNumber * EmpProfileSkillId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OrgSkillCode;
@property (retain) NSString * OrgSkillDescription;
@property (retain) NSNumber * OrgSkillId;
@property (retain) NSString * OrgSkillName;
@property (retain) NSNumber * OrgSkillPriority;
@property (retain) NSString * OrgSkillPriorityName;
@property (retain) NSNumber * OrgSkillTypeId;
@property (retain) NSString * OrgSkillTypeName;
@property (retain) NSString * RatingDescription;
@property (retain) NSNumber * RatingId;
@property (retain) NSNumber * RatingValue;
@property (retain) NSNumber * RelateRowId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_EmpProfileSkill : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_EmpProfileSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileSkill:(emp_tns1_V_EmpProfileSkill *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_EmpProfileBasicSalary : NSObject {
	
/* elements */
	NSString * CLogCurrencyCode;
	NSNumber * CLogCurrencyId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * DecisionCode;
	NSString * DecisionName;
	NSNumber * EmpProfileBasicSalaryId;
	NSNumber * EmployeeId;
	NSDate * ExpirationDate;
	NSString * FileDecisionUrl;
	NSDate * ImplementationDate;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	USBoolean * IsIncrease;
	USBoolean * IsRecall;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
	NSString * Reason;
	NSDate * RecallMonth;
	NSDate * SocialInsuranceDate;
	NSString * cEmpAllowance;
	NSString * cEmpBasicSalary;
	NSString * cEmpKPISalary;
	NSString * cEmpSalaryCoeficient;
	NSString * cEmpSalaryGrade;
	NSString * cEmpSalaryQuota;
	NSNumber * nEmpAllowance;
	NSNumber * nEmpBasicSalary;
	NSNumber * nEmpKPISalary;
	NSNumber * nEmpSalaryCoeficient;
	NSNumber * nEmpSalaryGrade;
	NSNumber * nEmpSalaryQuota;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_EmpProfileBasicSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogCurrencyCode;
@property (retain) NSNumber * CLogCurrencyId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DecisionCode;
@property (retain) NSString * DecisionName;
@property (retain) NSNumber * EmpProfileBasicSalaryId;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSString * FileDecisionUrl;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsIncrease;
@property (retain) USBoolean * IsRecall;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) NSString * Reason;
@property (retain) NSDate * RecallMonth;
@property (retain) NSDate * SocialInsuranceDate;
@property (retain) NSString * cEmpAllowance;
@property (retain) NSString * cEmpBasicSalary;
@property (retain) NSString * cEmpKPISalary;
@property (retain) NSString * cEmpSalaryCoeficient;
@property (retain) NSString * cEmpSalaryGrade;
@property (retain) NSString * cEmpSalaryQuota;
@property (retain) NSNumber * nEmpAllowance;
@property (retain) NSNumber * nEmpBasicSalary;
@property (retain) NSNumber * nEmpKPISalary;
@property (retain) NSNumber * nEmpSalaryCoeficient;
@property (retain) NSNumber * nEmpSalaryGrade;
@property (retain) NSNumber * nEmpSalaryQuota;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_EmpProfileBasicSalary : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileBasicSalary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_EmpProfileBasicSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileBasicSalary:(emp_tns1_V_EmpProfileBasicSalary *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileBasicSalary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_EmpProfileHealthInsurance : NSObject {
	
/* elements */
	NSString * CLogCityCode;
	NSNumber * CLogCityId;
	NSString * CLogCityName;
	NSString * CLogCityNoIns;
	NSString * CLogHospitalCode;
	NSNumber * CLogHospitalId;
	NSString * CLogHospitalName;
	NSString * CLogHospitalNote;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * HealthInsuranceDateEffect;
	NSDate * HealthInsuranceDateExpire;
	NSNumber * HealthInsuranceId;
	NSString * HealthInsuranceNumber;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_EmpProfileHealthInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogCityCode;
@property (retain) NSNumber * CLogCityId;
@property (retain) NSString * CLogCityName;
@property (retain) NSString * CLogCityNoIns;
@property (retain) NSString * CLogHospitalCode;
@property (retain) NSNumber * CLogHospitalId;
@property (retain) NSString * CLogHospitalName;
@property (retain) NSString * CLogHospitalNote;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * HealthInsuranceDateEffect;
@property (retain) NSDate * HealthInsuranceDateExpire;
@property (retain) NSNumber * HealthInsuranceId;
@property (retain) NSString * HealthInsuranceNumber;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_EmpProfileHealthInsurance : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileHealthInsurance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_EmpProfileHealthInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileHealthInsurance:(emp_tns1_V_EmpProfileHealthInsurance *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileHealthInsurance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_EmpProfileWageType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpProfileWageTypeId;
	NSNumber * EmployeeId;
	NSDate * ImplementationDate;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * NameEN;
	NSString * NameVN;
	NSString * Note;
	NSString * TimeWageTypeCode;
	NSNumber * TimeWageTypeId;
	NSNumber * WorkingDay;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_EmpProfileWageType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileWageTypeId;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * NameEN;
@property (retain) NSString * NameVN;
@property (retain) NSString * Note;
@property (retain) NSString * TimeWageTypeCode;
@property (retain) NSNumber * TimeWageTypeId;
@property (retain) NSNumber * WorkingDay;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_EmpProfileWageType : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileWageType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_EmpProfileWageType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileWageType:(emp_tns1_V_EmpProfileWageType *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileWageType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_EmpProfileWorkingForm : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpProfileWorkingFormId;
	NSNumber * EmployeeId;
	NSDate * ImplementationDate;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * NameEN;
	NSString * NameVN;
	NSString * Note;
	NSString * TimeWorkingFormCode;
	NSNumber * TimeWorkingFormId;
	NSString * TimeWorkingFormNote;
	NSDate * TimeWorkingFormStartEndTime;
	NSDate * TimeWorkingFormStartTime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_EmpProfileWorkingForm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileWorkingFormId;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * NameEN;
@property (retain) NSString * NameVN;
@property (retain) NSString * Note;
@property (retain) NSString * TimeWorkingFormCode;
@property (retain) NSNumber * TimeWorkingFormId;
@property (retain) NSString * TimeWorkingFormNote;
@property (retain) NSDate * TimeWorkingFormStartEndTime;
@property (retain) NSDate * TimeWorkingFormStartTime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_EmpProfileWorkingForm : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileWorkingForm;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_EmpProfileWorkingForm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileWorkingForm:(emp_tns1_V_EmpProfileWorkingForm *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileWorkingForm;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_EmpSocialInsuranceSalary : NSObject {
	
/* elements */
	NSString * CLogCurrencyCode;
	NSNumber * CLogCurrencyId;
	NSString * CLogCurrencyName;
	NSNumber * CLogCurrencyRateId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * DecisionCode;
	NSString * DecisionName;
	NSNumber * EmpSocialInsuranceSalaryId;
	NSNumber * EmployeeId;
	NSDate * ExpirationDate;
	NSString * FileDecisionUrl;
	NSDate * ImplementationDate;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	USBoolean * IsIncrease;
	USBoolean * IsRecall;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * Reason;
	NSDate * RecallMonth;
	NSDate * SocialInsuranceDate;
	NSString * cEmpSISalary;
	NSString * cEmpSISalaryCoeficient;
	NSString * cEmpSISalaryGrade;
	NSNumber * nEmpSISalary;
	NSNumber * nEmpSISalaryCoeficient;
	NSNumber * nEmpSISalaryGrade;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_EmpSocialInsuranceSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogCurrencyCode;
@property (retain) NSNumber * CLogCurrencyId;
@property (retain) NSString * CLogCurrencyName;
@property (retain) NSNumber * CLogCurrencyRateId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DecisionCode;
@property (retain) NSString * DecisionName;
@property (retain) NSNumber * EmpSocialInsuranceSalaryId;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSString * FileDecisionUrl;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsIncrease;
@property (retain) USBoolean * IsRecall;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * Reason;
@property (retain) NSDate * RecallMonth;
@property (retain) NSDate * SocialInsuranceDate;
@property (retain) NSString * cEmpSISalary;
@property (retain) NSString * cEmpSISalaryCoeficient;
@property (retain) NSString * cEmpSISalaryGrade;
@property (retain) NSNumber * nEmpSISalary;
@property (retain) NSNumber * nEmpSISalaryCoeficient;
@property (retain) NSNumber * nEmpSISalaryGrade;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_EmpSocialInsuranceSalary : NSObject {
	
/* elements */
	NSMutableArray *V_EmpSocialInsuranceSalary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_EmpSocialInsuranceSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpSocialInsuranceSalary:(emp_tns1_V_EmpSocialInsuranceSalary *)toAdd;
@property (readonly) NSMutableArray * V_EmpSocialInsuranceSalary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileBiography : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileBiographyId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	NSString * JobName;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Other;
	NSString * PlaceIn;
	NSNumber * RelateRowId;
	NSDate * StartDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileBiography *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileBiographyId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * JobName;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIn;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSDate * StartDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileBiography : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileBiography;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileBiography *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileBiography:(emp_tns1_EmpProfileBiography *)toAdd;
@property (readonly) NSMutableArray * EmpProfileBiography;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_EmpProfileSocialInsurance : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ImplementDate;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	USBoolean * IsDifference;
	USBoolean * IsLostBook;
	NSDate * LabourBookDateOfIssue;
	NSString * LabourBookNo;
	NSString * LabourBookPlaceOfIssue;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * OtherInsuranceCompanyName;
	NSDate * OtherInsuranceDateEffect;
	NSDate * OtherInsuranceDateExpire;
	NSString * OtherInsuranceNumber;
	NSString * Remark;
	NSNumber * SICompanyRate;
	NSNumber * SIEmployeeRate;
	NSDate * SocialInsuranceDateIssue;
	NSNumber * SocialInsuranceId;
	NSNumber * SocialInsuranceJoiningBefore;
	NSString * SocialInsuranceNumber;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_EmpProfileSocialInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ImplementDate;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDifference;
@property (retain) USBoolean * IsLostBook;
@property (retain) NSDate * LabourBookDateOfIssue;
@property (retain) NSString * LabourBookNo;
@property (retain) NSString * LabourBookPlaceOfIssue;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OtherInsuranceCompanyName;
@property (retain) NSDate * OtherInsuranceDateEffect;
@property (retain) NSDate * OtherInsuranceDateExpire;
@property (retain) NSString * OtherInsuranceNumber;
@property (retain) NSString * Remark;
@property (retain) NSNumber * SICompanyRate;
@property (retain) NSNumber * SIEmployeeRate;
@property (retain) NSDate * SocialInsuranceDateIssue;
@property (retain) NSNumber * SocialInsuranceId;
@property (retain) NSNumber * SocialInsuranceJoiningBefore;
@property (retain) NSString * SocialInsuranceNumber;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfEmpProfileSocialInsurance : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileSocialInsurance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfEmpProfileSocialInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileSocialInsurance:(emp_tns1_EmpProfileSocialInsurance *)toAdd;
@property (readonly) NSMutableArray * EmpProfileSocialInsurance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_EmpPerformanceAppraisal : NSObject {
	
/* elements */
	NSDate * AppraisalDate;
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpPerformanceAppraisalId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsEndYear;
	USBoolean * IsHalfYear;
	NSString * ManagerRatingDescription;
	NSNumber * ManagerRatingId;
	NSNumber * ManagerRatingValue;
	NSDate * ModifiedDate;
	NSNumber * RelateRowId;
	NSNumber * Score;
	NSString * SelfComment;
	NSString * SelfRatingDescription;
	NSNumber * SelfRatingId;
	NSNumber * SelfRatingValue;
	NSNumber * SelfScore;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_EmpPerformanceAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AppraisalDate;
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpPerformanceAppraisalId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEndYear;
@property (retain) USBoolean * IsHalfYear;
@property (retain) NSString * ManagerRatingDescription;
@property (retain) NSNumber * ManagerRatingId;
@property (retain) NSNumber * ManagerRatingValue;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSString * SelfRatingDescription;
@property (retain) NSNumber * SelfRatingId;
@property (retain) NSNumber * SelfRatingValue;
@property (retain) NSNumber * SelfScore;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_EmpPerformanceAppraisal : NSObject {
	
/* elements */
	NSMutableArray *V_EmpPerformanceAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_EmpPerformanceAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpPerformanceAppraisal:(emp_tns1_V_EmpPerformanceAppraisal *)toAdd;
@property (readonly) NSMutableArray * V_EmpPerformanceAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_EmpProfileQualification : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CLogEducationLevelId;
	NSString * CLogMajorCode;
	NSNumber * CLogMajorId;
	NSString * CLogMajorName;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	NSString * EducationLevelName;
	NSNumber * EmpProfileQualificationId;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsValId;
	NSDate * ModifiedDate;
	NSNumber * OrgDegreeRankId;
	NSString * OrgDegreeRankName;
	NSString * OrgQualificationCode;
	NSString * OrgQualificationDescription;
	NSNumber * OrgQualificationId;
	NSString * OrgQualificationName;
	NSString * Other;
	NSString * PlaceIssue;
	NSNumber * RelateRowId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_EmpProfileQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CLogEducationLevelId;
@property (retain) NSString * CLogMajorCode;
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSString * CLogMajorName;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) NSString * EducationLevelName;
@property (retain) NSNumber * EmpProfileQualificationId;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgDegreeRankId;
@property (retain) NSString * OrgDegreeRankName;
@property (retain) NSString * OrgQualificationCode;
@property (retain) NSString * OrgQualificationDescription;
@property (retain) NSNumber * OrgQualificationId;
@property (retain) NSString * OrgQualificationName;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) NSNumber * RelateRowId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_EmpProfileQualification : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileQualification;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_EmpProfileQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileQualification:(emp_tns1_V_EmpProfileQualification *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileQualification;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_EmpProfileForeignLanguage : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * AttachmenttUrl;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DateOfIssue;
	NSString * DegreeName;
	NSNumber * EffectiveTime;
	NSNumber * EmpProfileForeignLanguageId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSString * LanguageCode;
	NSNumber * LanguageId;
	NSString * LanguageName;
	NSString * LanguageNote;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * OrgDegreeRankId;
	NSString * OrgDegreeRankName;
	NSNumber * OrgDegreeRankPriority;
	NSString * PlaceOfIssue;
	NSNumber * RelateRowId;
	NSString * TrainingCenter;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_EmpProfileForeignLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * AttachmenttUrl;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateOfIssue;
@property (retain) NSString * DegreeName;
@property (retain) NSNumber * EffectiveTime;
@property (retain) NSNumber * EmpProfileForeignLanguageId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * LanguageCode;
@property (retain) NSNumber * LanguageId;
@property (retain) NSString * LanguageName;
@property (retain) NSString * LanguageNote;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * OrgDegreeRankId;
@property (retain) NSString * OrgDegreeRankName;
@property (retain) NSNumber * OrgDegreeRankPriority;
@property (retain) NSString * PlaceOfIssue;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * TrainingCenter;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_EmpProfileForeignLanguage : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileForeignLanguage;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_EmpProfileForeignLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileForeignLanguage:(emp_tns1_V_EmpProfileForeignLanguage *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileForeignLanguage;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_EmpProfileDiscipline : NSObject {
	
/* elements */
	NSNumber * ApproveId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSString * DISCIPLINE_TYPE_DESC;
	NSString * DISCIPLINE_TYPE_NAME_EN;
	NSString * DISCIPLINE_TYPE_NAME_VN;
	NSDate * DateOfIssue;
	NSString * DecisionNo;
	NSNumber * EmpProfileDisciplineId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * ICON;
	USBoolean * IsDeleted;
	USBoolean * IsForEmployee;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * MonetaryValue;
	NSNumber * OrgDisciplineMasterId;
	NSString * OrgDisciplineTypeCode;
	USBoolean * Status;
	NSString * Title;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_EmpProfileDiscipline *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproveId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSString * DISCIPLINE_TYPE_DESC;
@property (retain) NSString * DISCIPLINE_TYPE_NAME_EN;
@property (retain) NSString * DISCIPLINE_TYPE_NAME_VN;
@property (retain) NSDate * DateOfIssue;
@property (retain) NSString * DecisionNo;
@property (retain) NSNumber * EmpProfileDisciplineId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * ICON;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsForEmployee;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * MonetaryValue;
@property (retain) NSNumber * OrgDisciplineMasterId;
@property (retain) NSString * OrgDisciplineTypeCode;
@property (retain) USBoolean * Status;
@property (retain) NSString * Title;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_EmpProfileDiscipline : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfileDiscipline;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_EmpProfileDiscipline *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfileDiscipline:(emp_tns1_V_EmpProfileDiscipline *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfileDiscipline;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_EmpProfilePersonality : NSObject {
	
/* elements */
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CLogPersonalityId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * Description;
	NSNumber * EmpProfilePersonalityId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * RelateRowId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_EmpProfilePersonality *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CLogPersonalityId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) NSNumber * EmpProfilePersonalityId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * RelateRowId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_EmpProfilePersonality : NSObject {
	
/* elements */
	NSMutableArray *V_EmpProfilePersonality;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_EmpProfilePersonality *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpProfilePersonality:(emp_tns1_V_EmpProfilePersonality *)toAdd;
@property (readonly) NSMutableArray * V_EmpProfilePersonality;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_TimeWorkingForm : NSObject {
	
/* elements */
	NSString * After_OT_Type;
	NSString * Allowance_Type;
	NSString * Before_OT_Type;
	NSString * Color;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * EndOfBreakTime;
	NSDate * EndTime;
	NSString * FLex1;
	NSNumber * FLex10;
	NSDate * FLex11;
	NSDate * FLex12;
	NSDate * FLex13;
	USBoolean * FLex14;
	USBoolean * FLex15;
	USBoolean * FLex16;
	NSString * FLex2;
	NSString * FLex3;
	NSString * FLex4;
	NSString * FLex5;
	NSNumber * FLex6;
	NSNumber * FLex7;
	NSNumber * FLex8;
	NSNumber * FLex9;
	NSNumber * HoursOfOfficialWork;
	USBoolean * IsDeleted;
	USBoolean * IsWorkingTimeIncludeBreakTime;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * NameEN;
	NSString * NameVN;
	NSString * Note;
	NSNumber * NumberOfBreakTime;
	NSString * ParentCode;
	USBoolean * SpecialShift;
	NSNumber * Standard_Working_Hours;
	NSDate * StartOfBreakTime;
	NSDate * StartTime;
	NSString * TimeWorkingFormCode;
	NSNumber * TimeWorkingFormId;
	NSString * Time_In_Out_Rule;
	NSString * Time_Log_Rule;
	NSString * WorkingFormTypeCode;
	NSString * Working_Form_Group;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_TimeWorkingForm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * After_OT_Type;
@property (retain) NSString * Allowance_Type;
@property (retain) NSString * Before_OT_Type;
@property (retain) NSString * Color;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndOfBreakTime;
@property (retain) NSDate * EndTime;
@property (retain) NSString * FLex1;
@property (retain) NSNumber * FLex10;
@property (retain) NSDate * FLex11;
@property (retain) NSDate * FLex12;
@property (retain) NSDate * FLex13;
@property (retain) USBoolean * FLex14;
@property (retain) USBoolean * FLex15;
@property (retain) USBoolean * FLex16;
@property (retain) NSString * FLex2;
@property (retain) NSString * FLex3;
@property (retain) NSString * FLex4;
@property (retain) NSString * FLex5;
@property (retain) NSNumber * FLex6;
@property (retain) NSNumber * FLex7;
@property (retain) NSNumber * FLex8;
@property (retain) NSNumber * FLex9;
@property (retain) NSNumber * HoursOfOfficialWork;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsWorkingTimeIncludeBreakTime;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * NameEN;
@property (retain) NSString * NameVN;
@property (retain) NSString * Note;
@property (retain) NSNumber * NumberOfBreakTime;
@property (retain) NSString * ParentCode;
@property (retain) USBoolean * SpecialShift;
@property (retain) NSNumber * Standard_Working_Hours;
@property (retain) NSDate * StartOfBreakTime;
@property (retain) NSDate * StartTime;
@property (retain) NSString * TimeWorkingFormCode;
@property (retain) NSNumber * TimeWorkingFormId;
@property (retain) NSString * Time_In_Out_Rule;
@property (retain) NSString * Time_Log_Rule;
@property (retain) NSString * WorkingFormTypeCode;
@property (retain) NSString * Working_Form_Group;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfTimeWorkingForm : NSObject {
	
/* elements */
	NSMutableArray *TimeWorkingForm;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfTimeWorkingForm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeWorkingForm:(emp_tns1_TimeWorkingForm *)toAdd;
@property (readonly) NSMutableArray * TimeWorkingForm;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_TimeWageType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * FLex1;
	NSNumber * FLex10;
	NSDate * FLex11;
	NSDate * FLex12;
	NSDate * FLex13;
	USBoolean * FLex14;
	USBoolean * FLex15;
	USBoolean * FLex16;
	NSString * FLex2;
	NSString * FLex3;
	NSString * FLex4;
	NSString * FLex5;
	NSNumber * FLex6;
	NSNumber * FLex7;
	NSNumber * FLex8;
	NSNumber * FLex9;
	NSString * Friday;
	USBoolean * IsApproveOvertime;
	USBoolean * IsDefault;
	USBoolean * IsDeleted;
	USBoolean * IsFollowPeriod;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Monday;
	NSString * NameEN;
	NSString * NameVN;
	NSNumber * NumberOfDaysInAPeriod;
	NSNumber * NumberOfDaysOffInAPeriod;
	NSString * Saturday;
	NSString * Sunday;
	NSString * Thursday;
	NSString * TimeWageTypeCode;
	NSNumber * TimeWageTypeId;
	NSString * Tuesday;
	NSString * Wednesday;
	NSNumber * WorkingDay;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_TimeWageType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * FLex1;
@property (retain) NSNumber * FLex10;
@property (retain) NSDate * FLex11;
@property (retain) NSDate * FLex12;
@property (retain) NSDate * FLex13;
@property (retain) USBoolean * FLex14;
@property (retain) USBoolean * FLex15;
@property (retain) USBoolean * FLex16;
@property (retain) NSString * FLex2;
@property (retain) NSString * FLex3;
@property (retain) NSString * FLex4;
@property (retain) NSString * FLex5;
@property (retain) NSNumber * FLex6;
@property (retain) NSNumber * FLex7;
@property (retain) NSNumber * FLex8;
@property (retain) NSNumber * FLex9;
@property (retain) NSString * Friday;
@property (retain) USBoolean * IsApproveOvertime;
@property (retain) USBoolean * IsDefault;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsFollowPeriod;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Monday;
@property (retain) NSString * NameEN;
@property (retain) NSString * NameVN;
@property (retain) NSNumber * NumberOfDaysInAPeriod;
@property (retain) NSNumber * NumberOfDaysOffInAPeriod;
@property (retain) NSString * Saturday;
@property (retain) NSString * Sunday;
@property (retain) NSString * Thursday;
@property (retain) NSString * TimeWageTypeCode;
@property (retain) NSNumber * TimeWageTypeId;
@property (retain) NSString * Tuesday;
@property (retain) NSString * Wednesday;
@property (retain) NSNumber * WorkingDay;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfTimeWageType : NSObject {
	
/* elements */
	NSMutableArray *TimeWageType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfTimeWageType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeWageType:(emp_tns1_TimeWageType *)toAdd;
@property (readonly) NSMutableArray * TimeWageType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogCBCompensationCategory : NSObject {
	
/* elements */
	NSMutableArray *CLogCBCompensationCategory;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogCBCompensationCategory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogCBCompensationCategory:(emp_tns1_CLogCBCompensationCategory *)toAdd;
@property (readonly) NSMutableArray * CLogCBCompensationCategory;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogCurrencyRate : NSObject {
	
/* elements */
	NSNumber * AverageRate;
	emp_tns1_CLogCurrency * CLogCurrency;
	emp_tns1_CLogCurrency * CLogCurrency1;
	NSNumber * CLogCurrencyRateId;
	NSDate * CurrencyRateDate;
	NSNumber * EndOfDayRate;
	NSNumber * FromCurrencyId;
	USBoolean * IsDeleted;
	NSNumber * ToCurrencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogCurrencyRate *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AverageRate;
@property (retain) emp_tns1_CLogCurrency * CLogCurrency;
@property (retain) emp_tns1_CLogCurrency * CLogCurrency1;
@property (retain) NSNumber * CLogCurrencyRateId;
@property (retain) NSDate * CurrencyRateDate;
@property (retain) NSNumber * EndOfDayRate;
@property (retain) NSNumber * FromCurrencyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ToCurrencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogCurrencyRate : NSObject {
	
/* elements */
	NSMutableArray *CLogCurrencyRate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogCurrencyRate *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogCurrencyRate:(emp_tns1_CLogCurrencyRate *)toAdd;
@property (readonly) NSMutableArray * CLogCurrencyRate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogCurrency : NSObject {
	
/* elements */
	NSString * CLogCurrencyCode;
	NSNumber * CLogCurrencyId;
	emp_tns1_ArrayOfCLogCurrencyRate * CLogCurrencyRates;
	emp_tns1_ArrayOfCLogCurrencyRate * CLogCurrencyRates1;
	NSString * CompanyId;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogCurrency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogCurrencyCode;
@property (retain) NSNumber * CLogCurrencyId;
@property (retain) emp_tns1_ArrayOfCLogCurrencyRate * CLogCurrencyRates;
@property (retain) emp_tns1_ArrayOfCLogCurrencyRate * CLogCurrencyRates1;
@property (retain) NSString * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogCurrency : NSObject {
	
/* elements */
	NSMutableArray *CLogCurrency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogCurrency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogCurrency:(emp_tns1_CLogCurrency *)toAdd;
@property (readonly) NSMutableArray * CLogCurrency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SysEmpProfileLayerUrl : NSObject {
	
/* elements */
	NSString * Action;
	NSNumber * CompanyId;
	NSString * Controller;
	USBoolean * IsDeleted;
	emp_tns1_SysEmpProfileLayer * SysEmpProfileLayer;
	NSNumber * SysEmpProfileLayerId;
	NSNumber * SysEmpProfileLayerUrlId;
	NSString * Url;
	NSString * UrlType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SysEmpProfileLayerUrl *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Action;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Controller;
@property (retain) USBoolean * IsDeleted;
@property (retain) emp_tns1_SysEmpProfileLayer * SysEmpProfileLayer;
@property (retain) NSNumber * SysEmpProfileLayerId;
@property (retain) NSNumber * SysEmpProfileLayerUrlId;
@property (retain) NSString * Url;
@property (retain) NSString * UrlType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSysEmpProfileLayerUrl : NSObject {
	
/* elements */
	NSMutableArray *SysEmpProfileLayerUrl;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSysEmpProfileLayerUrl *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysEmpProfileLayerUrl:(emp_tns1_SysEmpProfileLayerUrl *)toAdd;
@property (readonly) NSMutableArray * SysEmpProfileLayerUrl;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SysRoleEmpProfilePermission : NSObject {
	
/* elements */
	USBoolean * AddPermission;
	USBoolean * AprovePermisson;
	NSNumber * AssignedBy;
	NSDate * CreatedDate;
	USBoolean * DeletePermission;
	USBoolean * EditPermission;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_SysEmpProfileLayer * SysEmpProfileLayer;
	NSNumber * SysEmpProfileLayerId;
	emp_tns1_SysRole * SysRole;
	NSNumber * SysRoleEmpProfilePermissionId;
	NSNumber * SysRoleId;
	USBoolean * ViewPermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SysRoleEmpProfilePermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddPermission;
@property (retain) USBoolean * AprovePermisson;
@property (retain) NSNumber * AssignedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * DeletePermission;
@property (retain) USBoolean * EditPermission;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_SysEmpProfileLayer * SysEmpProfileLayer;
@property (retain) NSNumber * SysEmpProfileLayerId;
@property (retain) emp_tns1_SysRole * SysRole;
@property (retain) NSNumber * SysRoleEmpProfilePermissionId;
@property (retain) NSNumber * SysRoleId;
@property (retain) USBoolean * ViewPermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSysRoleEmpProfilePermission : NSObject {
	
/* elements */
	NSMutableArray *SysRoleEmpProfilePermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSysRoleEmpProfilePermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysRoleEmpProfilePermission:(emp_tns1_SysRoleEmpProfilePermission *)toAdd;
@property (readonly) NSMutableArray * SysRoleEmpProfilePermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SysRole : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * Description;
	NSString * Name;
	emp_tns1_ArrayOfSysRoleEmpProfilePermission * SysRoleEmpProfilePermissions;
	NSNumber * SysRoleId;
	emp_tns1_ArrayOfSysRoleMenuPermission * SysRoleMenuPermissions;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SysRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Description;
@property (retain) NSString * Name;
@property (retain) emp_tns1_ArrayOfSysRoleEmpProfilePermission * SysRoleEmpProfilePermissions;
@property (retain) NSNumber * SysRoleId;
@property (retain) emp_tns1_ArrayOfSysRoleMenuPermission * SysRoleMenuPermissions;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SysRoleMenuPermission : NSObject {
	
/* elements */
	NSNumber * AssignedBy;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_SysMenu * SysMenu;
	NSNumber * SysMenuId;
	emp_tns1_SysRole * SysRole;
	NSNumber * SysRoleId;
	NSNumber * SysRoleMenuPermissionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SysRoleMenuPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AssignedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_SysMenu * SysMenu;
@property (retain) NSNumber * SysMenuId;
@property (retain) emp_tns1_SysRole * SysRole;
@property (retain) NSNumber * SysRoleId;
@property (retain) NSNumber * SysRoleMenuPermissionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSysRoleMenuPermission : NSObject {
	
/* elements */
	NSMutableArray *SysRoleMenuPermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSysRoleMenuPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysRoleMenuPermission:(emp_tns1_SysRoleMenuPermission *)toAdd;
@property (readonly) NSMutableArray * SysRoleMenuPermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SysUserEmpProfilePermission : NSObject {
	
/* elements */
	USBoolean * AddPermission;
	USBoolean * AprovePermisson;
	NSNumber * AssignedBy;
	NSDate * CreatedDate;
	USBoolean * DeletePermission;
	USBoolean * EditPermission;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_SysEmpProfileLayer * SysEmpProfileLayer;
	NSNumber * SysEmpProfileLayerId;
	emp_tns1_SysUser * SysUser;
	NSNumber * SysUserEmpProfilePermissionId;
	NSNumber * SysUserId;
	USBoolean * ViewPermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SysUserEmpProfilePermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddPermission;
@property (retain) USBoolean * AprovePermisson;
@property (retain) NSNumber * AssignedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * DeletePermission;
@property (retain) USBoolean * EditPermission;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_SysEmpProfileLayer * SysEmpProfileLayer;
@property (retain) NSNumber * SysEmpProfileLayerId;
@property (retain) emp_tns1_SysUser * SysUser;
@property (retain) NSNumber * SysUserEmpProfilePermissionId;
@property (retain) NSNumber * SysUserId;
@property (retain) USBoolean * ViewPermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSysUserEmpProfilePermission : NSObject {
	
/* elements */
	NSMutableArray *SysUserEmpProfilePermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSysUserEmpProfilePermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysUserEmpProfilePermission:(emp_tns1_SysUserEmpProfilePermission *)toAdd;
@property (readonly) NSMutableArray * SysUserEmpProfilePermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SysUser : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * CurrentSessionId;
	NSNumber * EmployeeId;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSString * SelfServiceXML;
	emp_tns1_ArrayOfSysUserEmpProfilePermission * SysUserEmpProfilePermissions;
	NSNumber * SysUserId;
	emp_tns1_ArrayOfSysUserMenuPermission * SysUserMenuPermissions;
	NSString * Username;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SysUser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CurrentSessionId;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * SelfServiceXML;
@property (retain) emp_tns1_ArrayOfSysUserEmpProfilePermission * SysUserEmpProfilePermissions;
@property (retain) NSNumber * SysUserId;
@property (retain) emp_tns1_ArrayOfSysUserMenuPermission * SysUserMenuPermissions;
@property (retain) NSString * Username;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SysUserMenuPermission : NSObject {
	
/* elements */
	NSNumber * AssignedBy;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_SysMenu * SysMenu;
	NSNumber * SysMenuId;
	emp_tns1_SysUser * SysUser;
	NSNumber * SysUserId;
	NSNumber * SysUserMenuPermissionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SysUserMenuPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AssignedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_SysMenu * SysMenu;
@property (retain) NSNumber * SysMenuId;
@property (retain) emp_tns1_SysUser * SysUser;
@property (retain) NSNumber * SysUserId;
@property (retain) NSNumber * SysUserMenuPermissionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSysUserMenuPermission : NSObject {
	
/* elements */
	NSMutableArray *SysUserMenuPermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSysUserMenuPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysUserMenuPermission:(emp_tns1_SysUserMenuPermission *)toAdd;
@property (readonly) NSMutableArray * SysUserMenuPermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SysMenu : NSObject {
	
/* elements */
	NSString * Action;
	NSString * ApplicationCodeAction;
	NSString * Controller;
	USBoolean * IsDefault;
	USBoolean * IsDeleted;
	NSNumber * Priority;
	NSString * ProductCode;
	NSNumber * SysGroupMenuId;
	emp_tns1_ArrayOfSysGroupMenuPermission * SysGroupMenuPermissions;
	NSNumber * SysMenuId;
	NSString * SysMenuName;
	emp_tns1_ArrayOfSysRoleMenuPermission * SysRoleMenuPermissions;
	emp_tns1_ArrayOfSysUserMenuPermission * SysUserMenuPermissions;
	NSString * Url;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SysMenu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Action;
@property (retain) NSString * ApplicationCodeAction;
@property (retain) NSString * Controller;
@property (retain) USBoolean * IsDefault;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Priority;
@property (retain) NSString * ProductCode;
@property (retain) NSNumber * SysGroupMenuId;
@property (retain) emp_tns1_ArrayOfSysGroupMenuPermission * SysGroupMenuPermissions;
@property (retain) NSNumber * SysMenuId;
@property (retain) NSString * SysMenuName;
@property (retain) emp_tns1_ArrayOfSysRoleMenuPermission * SysRoleMenuPermissions;
@property (retain) emp_tns1_ArrayOfSysUserMenuPermission * SysUserMenuPermissions;
@property (retain) NSString * Url;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SysGroupMenuPermission : NSObject {
	
/* elements */
	NSNumber * AssignedBy;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_SysGroup * SysGroup;
	NSNumber * SysGroupId;
	NSNumber * SysGroupMenuPermissionId;
	emp_tns1_SysMenu * SysMenu;
	NSNumber * SysMenuId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SysGroupMenuPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AssignedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_SysGroup * SysGroup;
@property (retain) NSNumber * SysGroupId;
@property (retain) NSNumber * SysGroupMenuPermissionId;
@property (retain) emp_tns1_SysMenu * SysMenu;
@property (retain) NSNumber * SysMenuId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSysGroupMenuPermission : NSObject {
	
/* elements */
	NSMutableArray *SysGroupMenuPermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSysGroupMenuPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysGroupMenuPermission:(emp_tns1_SysGroupMenuPermission *)toAdd;
@property (readonly) NSMutableArray * SysGroupMenuPermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SysGroup : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * Description;
	NSString * Name;
	emp_tns1_ArrayOfSysGroupEmpProfilePermission * SysGroupEmpProfilePermissions;
	NSNumber * SysGroupId;
	emp_tns1_ArrayOfSysGroupMenuPermission * SysGroupMenuPermissions;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SysGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Description;
@property (retain) NSString * Name;
@property (retain) emp_tns1_ArrayOfSysGroupEmpProfilePermission * SysGroupEmpProfilePermissions;
@property (retain) NSNumber * SysGroupId;
@property (retain) emp_tns1_ArrayOfSysGroupMenuPermission * SysGroupMenuPermissions;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SysGroupEmpProfilePermission : NSObject {
	
/* elements */
	USBoolean * AddPermission;
	USBoolean * AprovePermisson;
	NSNumber * AssignedBy;
	NSDate * CreatedDate;
	USBoolean * DeletePermission;
	USBoolean * EditPermission;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	emp_tns1_SysEmpProfileLayer * SysEmpProfileLayer;
	NSNumber * SysEmpProfileLayerId;
	emp_tns1_SysGroup * SysGroup;
	NSNumber * SysGroupEmpProfilePermissionId;
	NSNumber * SysGroupId;
	USBoolean * ViewPermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SysGroupEmpProfilePermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddPermission;
@property (retain) USBoolean * AprovePermisson;
@property (retain) NSNumber * AssignedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * DeletePermission;
@property (retain) USBoolean * EditPermission;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) emp_tns1_SysEmpProfileLayer * SysEmpProfileLayer;
@property (retain) NSNumber * SysEmpProfileLayerId;
@property (retain) emp_tns1_SysGroup * SysGroup;
@property (retain) NSNumber * SysGroupEmpProfilePermissionId;
@property (retain) NSNumber * SysGroupId;
@property (retain) USBoolean * ViewPermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSysGroupEmpProfilePermission : NSObject {
	
/* elements */
	NSMutableArray *SysGroupEmpProfilePermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSysGroupEmpProfilePermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysGroupEmpProfilePermission:(emp_tns1_SysGroupEmpProfilePermission *)toAdd;
@property (readonly) NSMutableArray * SysGroupEmpProfilePermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SysEmpProfileLayer : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * Icon;
	USBoolean * IsDeleted;
	USBoolean * IsVisible;
	NSNumber * Priority;
	emp_tns1_SysEmpProfileGroupLayer * SysEmpProfileGroupLayer;
	NSNumber * SysEmpProfileGroupLayerId;
	NSString * SysEmpProfileLayerCode;
	NSNumber * SysEmpProfileLayerId;
	NSString * SysEmpProfileLayerName;
	emp_tns1_ArrayOfSysEmpProfileLayerUrl * SysEmpProfileLayerUrls;
	emp_tns1_ArrayOfSysGroupEmpProfilePermission * SysGroupEmpProfilePermissions;
	emp_tns1_ArrayOfSysRoleEmpProfilePermission * SysRoleEmpProfilePermissions;
	emp_tns1_ArrayOfSysUserEmpProfilePermission * SysUserEmpProfilePermissions;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SysEmpProfileLayer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Icon;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsVisible;
@property (retain) NSNumber * Priority;
@property (retain) emp_tns1_SysEmpProfileGroupLayer * SysEmpProfileGroupLayer;
@property (retain) NSNumber * SysEmpProfileGroupLayerId;
@property (retain) NSString * SysEmpProfileLayerCode;
@property (retain) NSNumber * SysEmpProfileLayerId;
@property (retain) NSString * SysEmpProfileLayerName;
@property (retain) emp_tns1_ArrayOfSysEmpProfileLayerUrl * SysEmpProfileLayerUrls;
@property (retain) emp_tns1_ArrayOfSysGroupEmpProfilePermission * SysGroupEmpProfilePermissions;
@property (retain) emp_tns1_ArrayOfSysRoleEmpProfilePermission * SysRoleEmpProfilePermissions;
@property (retain) emp_tns1_ArrayOfSysUserEmpProfilePermission * SysUserEmpProfilePermissions;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSysEmpProfileLayer : NSObject {
	
/* elements */
	NSMutableArray *SysEmpProfileLayer;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSysEmpProfileLayer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysEmpProfileLayer:(emp_tns1_SysEmpProfileLayer *)toAdd;
@property (readonly) NSMutableArray * SysEmpProfileLayer;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SysEmpProfileGroupLayer : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * Icon;
	USBoolean * IsDeleted;
	USBoolean * IsVisible;
	NSNumber * Priority;
	NSString * SysEmpProfileGroupLayerCode;
	NSNumber * SysEmpProfileGroupLayerId;
	NSString * SysEmpProfileGroupLayerName;
	emp_tns1_ArrayOfSysEmpProfileLayer * SysEmpProfileLayers;
	NSString * Url;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SysEmpProfileGroupLayer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Icon;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsVisible;
@property (retain) NSNumber * Priority;
@property (retain) NSString * SysEmpProfileGroupLayerCode;
@property (retain) NSNumber * SysEmpProfileGroupLayerId;
@property (retain) NSString * SysEmpProfileGroupLayerName;
@property (retain) emp_tns1_ArrayOfSysEmpProfileLayer * SysEmpProfileLayers;
@property (retain) NSString * Url;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSysEmpProfileGroupLayer : NSObject {
	
/* elements */
	NSMutableArray *SysEmpProfileGroupLayer;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSysEmpProfileGroupLayer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysEmpProfileGroupLayer:(emp_tns1_SysEmpProfileGroupLayer *)toAdd;
@property (readonly) NSMutableArray * SysEmpProfileGroupLayer;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_SysEmpProfileLayer : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * Icon;
	USBoolean * IsDeleted;
	USBoolean * IsVisible;
	NSNumber * Priority;
	NSNumber * SysEmpProfileGroupLayerId;
	NSNumber * SysEmpProfileLayerId;
	NSString * SysEmpProfileLayerName;
	NSNumber * SysEmpProfileLayerUrlId;
	USBoolean * UlrIsDeleted;
	NSString * Url;
	NSString * UrlType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_SysEmpProfileLayer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Icon;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsVisible;
@property (retain) NSNumber * Priority;
@property (retain) NSNumber * SysEmpProfileGroupLayerId;
@property (retain) NSNumber * SysEmpProfileLayerId;
@property (retain) NSString * SysEmpProfileLayerName;
@property (retain) NSNumber * SysEmpProfileLayerUrlId;
@property (retain) USBoolean * UlrIsDeleted;
@property (retain) NSString * Url;
@property (retain) NSString * UrlType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogEmployeeType : NSObject {
	
/* elements */
	NSMutableArray *CLogEmployeeType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogEmployeeType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogEmployeeType:(emp_tns1_CLogEmployeeType *)toAdd;
@property (readonly) NSMutableArray * CLogEmployeeType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogMaritalStatu : NSObject {
	
/* elements */
	NSNumber * CLogMaritalStatusId;
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSString * MaritalStatusCode;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogMaritalStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogMaritalStatusId;
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * MaritalStatusCode;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogMaritalStatu : NSObject {
	
/* elements */
	NSMutableArray *CLogMaritalStatu;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogMaritalStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogMaritalStatu:(emp_tns1_CLogMaritalStatu *)toAdd;
@property (readonly) NSMutableArray * CLogMaritalStatu;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_OrgUnitTree : NSObject {
	
/* elements */
	NSString * Address;
	NSNumber * AddressId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSNumber * OrgUnitLevelId;
	NSString * ParentCode;
	NSNumber * ParentId;
	NSNumber * TotalEmployees;
	NSString * WorkLocation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_OrgUnitTree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSNumber * AddressId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * OrgUnitLevelId;
@property (retain) NSString * ParentCode;
@property (retain) NSNumber * ParentId;
@property (retain) NSNumber * TotalEmployees;
@property (retain) NSString * WorkLocation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_OrgUnitTree : NSObject {
	
/* elements */
	NSMutableArray *V_OrgUnitTree;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_OrgUnitTree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgUnitTree:(emp_tns1_V_OrgUnitTree *)toAdd;
@property (readonly) NSMutableArray * V_OrgUnitTree;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_OrgUnitJob : NSObject {
	
/* elements */
	NSString * CAREER_NAME_EN;
	NSString * CAREER_NAME_VN;
	NSString * CLOG_CAREER_CODE;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobNameEN;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSNumber * OrgUnitJobId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_OrgUnitJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CAREER_NAME_EN;
@property (retain) NSString * CAREER_NAME_VN;
@property (retain) NSString * CLOG_CAREER_CODE;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobNameEN;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * OrgUnitJobId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_OrgUnitJob : NSObject {
	
/* elements */
	NSMutableArray *V_OrgUnitJob;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_OrgUnitJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgUnitJob:(emp_tns1_V_OrgUnitJob *)toAdd;
@property (readonly) NSMutableArray * V_OrgUnitJob;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgWorkLevel : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgWorkLevelCode;
	NSNumber * OrgWorkLevelId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgWorkLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfOrgWorkLevel : NSObject {
	
/* elements */
	NSMutableArray *OrgWorkLevel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfOrgWorkLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgWorkLevel:(emp_tns1_OrgWorkLevel *)toAdd;
@property (readonly) NSMutableArray * OrgWorkLevel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_ESS_CLOG_CAREER : NSObject {
	
/* elements */
	NSString * CAREER_DESC;
	NSString * CAREER_NAME_EN;
	NSString * CAREER_NAME_VN;
	NSString * CLOG_CAREER_CODE;
	NSNumber * CLOG_CAREER_ID;
	NSNumber * COMPANY_ID;
	NSNumber * CREATED_BY;
	NSDate * CREATED_DATE;
	NSString * GROUP_CAREER_CODE;
	USBoolean * IS_DELETED;
	NSNumber * MODIFIED_BY;
	NSDate * MODIFIED_DATE;
	NSNumber * ORDER_BY;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_ESS_CLOG_CAREER *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CAREER_DESC;
@property (retain) NSString * CAREER_NAME_EN;
@property (retain) NSString * CAREER_NAME_VN;
@property (retain) NSString * CLOG_CAREER_CODE;
@property (retain) NSNumber * CLOG_CAREER_ID;
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSNumber * CREATED_BY;
@property (retain) NSDate * CREATED_DATE;
@property (retain) NSString * GROUP_CAREER_CODE;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSNumber * MODIFIED_BY;
@property (retain) NSDate * MODIFIED_DATE;
@property (retain) NSNumber * ORDER_BY;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_ESS_CLOG_CAREER : NSObject {
	
/* elements */
	NSMutableArray *V_ESS_CLOG_CAREER;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_ESS_CLOG_CAREER *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ESS_CLOG_CAREER:(emp_tns1_V_ESS_CLOG_CAREER *)toAdd;
@property (readonly) NSMutableArray * V_ESS_CLOG_CAREER;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogRating : NSObject {
	
/* elements */
	NSMutableArray *CLogRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogRating:(emp_tns1_CLogRating *)toAdd;
@property (readonly) NSMutableArray * CLogRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogDegree : NSObject {
	
/* elements */
	NSMutableArray *CLogDegree;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogDegree:(emp_tns1_CLogDegree *)toAdd;
@property (readonly) NSMutableArray * CLogDegree;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogMajor : NSObject {
	
/* elements */
	NSMutableArray *CLogMajor;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogMajor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogMajor:(emp_tns1_CLogMajor *)toAdd;
@property (readonly) NSMutableArray * CLogMajor;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogTrainingCenter : NSObject {
	
/* elements */
	NSMutableArray *CLogTrainingCenter;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogTrainingCenter *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogTrainingCenter:(emp_tns1_CLogTrainingCenter *)toAdd;
@property (readonly) NSMutableArray * CLogTrainingCenter;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogTraining : NSObject {
	
/* elements */
	NSNumber * CLogTrainingId;
	NSDate * CreateDate;
	NSString * Description;
	NSString * ImgUrl;
	USBoolean * IsDeleted;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogTraining *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogTrainingId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) NSString * ImgUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogTraining : NSObject {
	
/* elements */
	NSMutableArray *CLogTraining;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogTraining *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogTraining:(emp_tns1_CLogTraining *)toAdd;
@property (readonly) NSMutableArray * CLogTraining;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfOrgDegree : NSObject {
	
/* elements */
	NSMutableArray *OrgDegree;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfOrgDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgDegree:(emp_tns1_OrgDegree *)toAdd;
@property (readonly) NSMutableArray * OrgDegree;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgDegreeRank : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * OrgDegreeRankId;
	NSNumber * Priority;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgDegreeRank *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * OrgDegreeRankId;
@property (retain) NSNumber * Priority;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfOrgDegreeRank : NSObject {
	
/* elements */
	NSMutableArray *OrgDegreeRank;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfOrgDegreeRank *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgDegreeRank:(emp_tns1_OrgDegreeRank *)toAdd;
@property (readonly) NSMutableArray * OrgDegreeRank;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogEthnicity : NSObject {
	
/* elements */
	NSNumber * CLogEthnicityId;
	NSNumber * Companyid;
	NSString * EthnicityCode;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogEthnicity *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogEthnicityId;
@property (retain) NSNumber * Companyid;
@property (retain) NSString * EthnicityCode;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogEthnicity : NSObject {
	
/* elements */
	NSMutableArray *CLogEthnicity;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogEthnicity *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogEthnicity:(emp_tns1_CLogEthnicity *)toAdd;
@property (readonly) NSMutableArray * CLogEthnicity;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogReligion : NSObject {
	
/* elements */
	NSNumber * CLogReligionId;
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
	NSString * ReligionCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogReligion *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogReligionId;
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
@property (retain) NSString * ReligionCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogReligion : NSObject {
	
/* elements */
	NSMutableArray *CLogReligion;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogReligion *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogReligion:(emp_tns1_CLogReligion *)toAdd;
@property (readonly) NSMutableArray * CLogReligion;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogLanguage : NSObject {
	
/* elements */
	NSMutableArray *CLogLanguage;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogLanguage:(emp_tns1_CLogLanguage *)toAdd;
@property (readonly) NSMutableArray * CLogLanguage;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogEducationLevel : NSObject {
	
/* elements */
	NSNumber * CLogEducationLevelId;
	NSNumber * CompanyId;
	NSString * EducationLevelCode;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogEducationLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogEducationLevelId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * EducationLevelCode;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogEducationLevel : NSObject {
	
/* elements */
	NSMutableArray *CLogEducationLevel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogEducationLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogEducationLevel:(emp_tns1_CLogEducationLevel *)toAdd;
@property (readonly) NSMutableArray * CLogEducationLevel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfOrgQualification : NSObject {
	
/* elements */
	NSMutableArray *OrgQualification;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfOrgQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgQualification:(emp_tns1_OrgQualification *)toAdd;
@property (readonly) NSMutableArray * OrgQualification;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogEmpCulturalLevel : NSObject {
	
/* elements */
	NSNumber * CLogEmpCulturalLevelId;
	NSNumber * CompanyId;
	NSString * EmpCulturalLevelCode;
	NSString * EmpCulturalLevelName;
	NSString * EmpCulturalLevelNameEN;
	USBoolean * IsDeleted;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogEmpCulturalLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogEmpCulturalLevelId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * EmpCulturalLevelCode;
@property (retain) NSString * EmpCulturalLevelName;
@property (retain) NSString * EmpCulturalLevelNameEN;
@property (retain) USBoolean * IsDeleted;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogEmpCulturalLevel : NSObject {
	
/* elements */
	NSMutableArray *CLogEmpCulturalLevel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogEmpCulturalLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogEmpCulturalLevel:(emp_tns1_CLogEmpCulturalLevel *)toAdd;
@property (readonly) NSMutableArray * CLogEmpCulturalLevel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogBankBranch : NSObject {
	
/* elements */
	NSString * BankBranchName;
	emp_tns1_CLogBank * CLogBank;
	NSNumber * CLogBankBranchId;
	NSNumber * CLogBankId;
	USBoolean * IsDeleted;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogBankBranch *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * BankBranchName;
@property (retain) emp_tns1_CLogBank * CLogBank;
@property (retain) NSNumber * CLogBankBranchId;
@property (retain) NSNumber * CLogBankId;
@property (retain) USBoolean * IsDeleted;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogBankBranch : NSObject {
	
/* elements */
	NSMutableArray *CLogBankBranch;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogBankBranch *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogBankBranch:(emp_tns1_CLogBankBranch *)toAdd;
@property (readonly) NSMutableArray * CLogBankBranch;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogBank : NSObject {
	
/* elements */
	NSString * BankCode;
	NSString * BankName;
	NSString * BankNameEN;
	emp_tns1_ArrayOfCLogBankBranch * CLogBankBranches;
	NSNumber * CLogBankId;
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogBank *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * BankCode;
@property (retain) NSString * BankName;
@property (retain) NSString * BankNameEN;
@property (retain) emp_tns1_ArrayOfCLogBankBranch * CLogBankBranches;
@property (retain) NSNumber * CLogBankId;
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogBank : NSObject {
	
/* elements */
	NSMutableArray *CLogBank;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogBank *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogBank:(emp_tns1_CLogBank *)toAdd;
@property (readonly) NSMutableArray * CLogBank;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogCourseStatu : NSObject {
	
/* elements */
	NSMutableArray *CLogCourseStatu;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogCourseStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogCourseStatu:(emp_tns1_CLogCourseStatu *)toAdd;
@property (readonly) NSMutableArray * CLogCourseStatu;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogEmpWorkingStatu : NSObject {
	
/* elements */
	NSMutableArray *CLogEmpWorkingStatu;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogEmpWorkingStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogEmpWorkingStatu:(emp_tns1_CLogEmpWorkingStatu *)toAdd;
@property (readonly) NSMutableArray * CLogEmpWorkingStatu;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogPersonality : NSObject {
	
/* elements */
	NSMutableArray *CLogPersonality;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogPersonality *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogPersonality:(emp_tns1_CLogPersonality *)toAdd;
@property (readonly) NSMutableArray * CLogPersonality;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfOrgProjectType : NSObject {
	
/* elements */
	NSMutableArray *OrgProjectType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfOrgProjectType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgProjectType:(emp_tns1_OrgProjectType *)toAdd;
@property (readonly) NSMutableArray * OrgProjectType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfOrgTimeInCharge : NSObject {
	
/* elements */
	NSMutableArray *OrgTimeInCharge;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfOrgTimeInCharge *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgTimeInCharge:(emp_tns1_OrgTimeInCharge *)toAdd;
@property (readonly) NSMutableArray * OrgTimeInCharge;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfOrgSkillType : NSObject {
	
/* elements */
	NSMutableArray *OrgSkillType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfOrgSkillType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgSkillType:(emp_tns1_OrgSkillType *)toAdd;
@property (readonly) NSMutableArray * OrgSkillType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_V_CLogCBFactor : NSObject {
	
/* elements */
	NSString * CLogCBFactorCode;
	NSString * CLogCBFactorGroupCode;
	NSNumber * CLogCBFactorGroupId;
	NSString * CLogCBFactorGroupName;
	NSNumber * CLogCBFactorId;
	NSString * CLogCBFactorName;
	NSString * CLogCompensationCategoryCode;
	NSNumber * CLogCompensationCategoryId;
	NSString * CLogCompensationCategoryName;
	NSString * Description;
	NSString * FormulaExpression;
	USBoolean * IsActived;
	USBoolean * IsDeactived;
	USBoolean * IsDeleted;
	USBoolean * IsInitial;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_V_CLogCBFactor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogCBFactorCode;
@property (retain) NSString * CLogCBFactorGroupCode;
@property (retain) NSNumber * CLogCBFactorGroupId;
@property (retain) NSString * CLogCBFactorGroupName;
@property (retain) NSNumber * CLogCBFactorId;
@property (retain) NSString * CLogCBFactorName;
@property (retain) NSString * CLogCompensationCategoryCode;
@property (retain) NSNumber * CLogCompensationCategoryId;
@property (retain) NSString * CLogCompensationCategoryName;
@property (retain) NSString * Description;
@property (retain) NSString * FormulaExpression;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeactived;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInitial;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfV_CLogCBFactor : NSObject {
	
/* elements */
	NSMutableArray *V_CLogCBFactor;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfV_CLogCBFactor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_CLogCBFactor:(emp_tns1_V_CLogCBFactor *)toAdd;
@property (readonly) NSMutableArray * V_CLogCBFactor;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_CLogEmpOffType : NSObject {
	
/* elements */
	NSString * CLogEmpOffTypeCode;
	NSNumber * CLogEmpOffTypeId;
	NSNumber * CompanyId;
	NSString * EmpOffTypeName;
	NSString * EmpOffTypeNameEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_CLogEmpOffType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogEmpOffTypeCode;
@property (retain) NSNumber * CLogEmpOffTypeId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * EmpOffTypeName;
@property (retain) NSString * EmpOffTypeNameEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfCLogEmpOffType : NSObject {
	
/* elements */
	NSMutableArray *CLogEmpOffType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfCLogEmpOffType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogEmpOffType:(emp_tns1_CLogEmpOffType *)toAdd;
@property (readonly) NSMutableArray * CLogEmpOffType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_OrgKnowledge : NSObject {
	
/* elements */
	NSNumber * CLogMajorId;
	NSNumber * CompanyId;
	NSString * Description;
	USBoolean * IsDeleted;
	NSString * OrgKnowledgeCode;
	NSNumber * OrgKnowledgeId;
	NSString * OrgKnowledgeName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_OrgKnowledge *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * OrgKnowledgeCode;
@property (retain) NSNumber * OrgKnowledgeId;
@property (retain) NSString * OrgKnowledgeName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfOrgKnowledge : NSObject {
	
/* elements */
	NSMutableArray *OrgKnowledge;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfOrgKnowledge *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgKnowledge:(emp_tns1_OrgKnowledge *)toAdd;
@property (readonly) NSMutableArray * OrgKnowledge;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SP_ORG_UNIT_TREE_BY_USER_Result : NSObject {
	
/* elements */
	NSString * Address;
	NSNumber * AddressId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsActive;
	NSNumber * MasterId;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSNumber * OrgUnitLevelId;
	NSString * ParentCode;
	NSNumber * ParentId;
	NSNumber * TotalEmployees;
	NSString * WorkLocation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SP_ORG_UNIT_TREE_BY_USER_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSNumber * AddressId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsActive;
@property (retain) NSNumber * MasterId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * OrgUnitLevelId;
@property (retain) NSString * ParentCode;
@property (retain) NSNumber * ParentId;
@property (retain) NSNumber * TotalEmployees;
@property (retain) NSString * WorkLocation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSP_ORG_UNIT_TREE_BY_USER_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_ORG_UNIT_TREE_BY_USER_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSP_ORG_UNIT_TREE_BY_USER_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_ORG_UNIT_TREE_BY_USER_Result:(emp_tns1_SP_ORG_UNIT_TREE_BY_USER_Result *)toAdd;
@property (readonly) NSMutableArray * SP_ORG_UNIT_TREE_BY_USER_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SP_FTS_EMP_PROFILE_BASIC_Result : NSObject {
	
/* elements */
	NSString * Address;
	NSNumber * AddressId;
	NSString * BankAccountNo;
	NSDate * BirthDay;
	NSString * BirthPlace;
	NSNumber * BirthPlaceId;
	NSString * CVUrl;
	NSNumber * CompanyId;
	NSString * ContactAddress;
	NSNumber * ContactAddressId;
	NSString * CulturalLevel;
	NSNumber * CulturalLevelId;
	NSNumber * EducationLevelId;
	NSString * EducationLevelName;
	NSString * Email;
	NSNumber * EmpBasicProfileId;
	NSString * EmpWorkingStatusCode;
	NSString * EmpWorkingStatusName;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EmployeeTypeId;
	NSString * EmployeeTypeName;
	NSDate * EntryDate;
	NSString * Ethnicity;
	NSNumber * EthnicityId;
	NSString * FirstName;
	NSString * FullName;
	USBoolean * Gender;
	NSString * HomePhone;
	NSString * HomeTown;
	NSNumber * HomeTownId;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	NSString * ImageUrl;
	USBoolean * IsConcurrentPost;
	USBoolean * IsDeleted;
	USBoolean * IsOnlyYearOfBirthday;
	NSString * LastName;
	NSString * MainLanguage;
	NSNumber * MainLanguageId;
	NSString * Major;
	NSNumber * MajorId;
	NSNumber * MaritalStatusId;
	NSString * MaritalStatusValue;
	NSString * MobileNumber;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSString * OfficePhone;
	NSDate * OfficialDate;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgWorkLevelCode;
	NSNumber * OrgWorkLevelId;
	NSString * PITCode;
	NSString * PITIssuedBy;
	NSDate * PassportDateOfExpire;
	NSDate * PassportDateOfIssue;
	NSString * PassportNo;
	NSString * PassportPlaceOfIssue;
	NSString * PermanentAddress;
	NSNumber * PermanentAddressId;
	NSString * PositionName;
	NSDate * ProbationaryDate;
	NSNumber * ProbationarySalary;
	NSString * Reason;
	NSString * Religion;
	NSNumber * ReligionId;
	NSString * TemporaryAddress;
	NSNumber * TemporaryAddressId;
	NSString * TimeWorkingFormCode;
	NSNumber * TimeWorkingFormId;
	NSString * UnitName;
	NSString * WorkLevelDescription;
	NSString * WorkLevelName;
	NSString * WorkLocation;
	NSDate * WorkingFormImplementationDate;
	NSString * WorkingFormTypeCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SP_FTS_EMP_PROFILE_BASIC_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSNumber * AddressId;
@property (retain) NSString * BankAccountNo;
@property (retain) NSDate * BirthDay;
@property (retain) NSString * BirthPlace;
@property (retain) NSNumber * BirthPlaceId;
@property (retain) NSString * CVUrl;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ContactAddress;
@property (retain) NSNumber * ContactAddressId;
@property (retain) NSString * CulturalLevel;
@property (retain) NSNumber * CulturalLevelId;
@property (retain) NSNumber * EducationLevelId;
@property (retain) NSString * EducationLevelName;
@property (retain) NSString * Email;
@property (retain) NSNumber * EmpBasicProfileId;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSString * EmployeeTypeName;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * Ethnicity;
@property (retain) NSNumber * EthnicityId;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * HomePhone;
@property (retain) NSString * HomeTown;
@property (retain) NSNumber * HomeTownId;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsConcurrentPost;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) NSString * LastName;
@property (retain) NSString * MainLanguage;
@property (retain) NSNumber * MainLanguageId;
@property (retain) NSString * Major;
@property (retain) NSNumber * MajorId;
@property (retain) NSNumber * MaritalStatusId;
@property (retain) NSString * MaritalStatusValue;
@property (retain) NSString * MobileNumber;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSString * OfficePhone;
@property (retain) NSDate * OfficialDate;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * PITCode;
@property (retain) NSString * PITIssuedBy;
@property (retain) NSDate * PassportDateOfExpire;
@property (retain) NSDate * PassportDateOfIssue;
@property (retain) NSString * PassportNo;
@property (retain) NSString * PassportPlaceOfIssue;
@property (retain) NSString * PermanentAddress;
@property (retain) NSNumber * PermanentAddressId;
@property (retain) NSString * PositionName;
@property (retain) NSDate * ProbationaryDate;
@property (retain) NSNumber * ProbationarySalary;
@property (retain) NSString * Reason;
@property (retain) NSString * Religion;
@property (retain) NSNumber * ReligionId;
@property (retain) NSString * TemporaryAddress;
@property (retain) NSNumber * TemporaryAddressId;
@property (retain) NSString * TimeWorkingFormCode;
@property (retain) NSNumber * TimeWorkingFormId;
@property (retain) NSString * UnitName;
@property (retain) NSString * WorkLevelDescription;
@property (retain) NSString * WorkLevelName;
@property (retain) NSString * WorkLocation;
@property (retain) NSDate * WorkingFormImplementationDate;
@property (retain) NSString * WorkingFormTypeCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_BASIC_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_FTS_EMP_PROFILE_BASIC_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_BASIC_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_FTS_EMP_PROFILE_BASIC_Result:(emp_tns1_SP_FTS_EMP_PROFILE_BASIC_Result *)toAdd;
@property (readonly) NSMutableArray * SP_FTS_EMP_PROFILE_BASIC_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SP_FTS_EMP_PROFILE_CONTACT_Result : NSObject {
	
/* elements */
	NSString * Address;
	NSNumber * AddressId;
	NSString * BankAccountNo;
	NSDate * BirthDay;
	NSString * BirthPlace;
	NSNumber * BirthPlaceId;
	NSString * CVUrl;
	NSNumber * CompanyId;
	NSString * ContactAddress;
	NSNumber * ContactAddressId;
	NSString * CulturalLevel;
	NSNumber * CulturalLevelId;
	NSNumber * EducationLevelId;
	NSString * EducationLevelName;
	NSString * Email;
	NSNumber * EmpBasicProfileId;
	NSString * EmpWorkingStatusCode;
	NSString * EmpWorkingStatusName;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EmployeeTypeId;
	NSString * EmployeeTypeName;
	NSDate * EntryDate;
	NSString * Ethnicity;
	NSNumber * EthnicityId;
	NSString * FirstName;
	NSString * FullName;
	USBoolean * Gender;
	NSString * HomePhone;
	NSString * HomeTown;
	NSNumber * HomeTownId;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	NSString * ImageUrl;
	USBoolean * IsConcurrentPost;
	USBoolean * IsDeleted;
	USBoolean * IsOnlyYearOfBirthday;
	NSString * LastName;
	NSString * MainLanguage;
	NSNumber * MainLanguageId;
	NSString * Major;
	NSNumber * MajorId;
	NSNumber * MaritalStatusId;
	NSString * MaritalStatusValue;
	NSString * MobileNumber;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSString * OfficePhone;
	NSDate * OfficialDate;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgWorkLevelCode;
	NSNumber * OrgWorkLevelId;
	NSString * PITCode;
	NSString * PITIssuedBy;
	NSDate * PassportDateOfExpire;
	NSDate * PassportDateOfIssue;
	NSString * PassportNo;
	NSString * PassportPlaceOfIssue;
	NSString * PermanentAddress;
	NSNumber * PermanentAddressId;
	NSString * PositionName;
	NSDate * ProbationaryDate;
	NSNumber * ProbationarySalary;
	NSString * Reason;
	NSString * Religion;
	NSNumber * ReligionId;
	NSString * TemporaryAddress;
	NSNumber * TemporaryAddressId;
	NSString * TimeWorkingFormCode;
	NSNumber * TimeWorkingFormId;
	NSString * UnitName;
	NSString * WorkLevelDescription;
	NSString * WorkLevelName;
	NSString * WorkLocation;
	NSDate * WorkingFormImplementationDate;
	NSString * WorkingFormTypeCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SP_FTS_EMP_PROFILE_CONTACT_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSNumber * AddressId;
@property (retain) NSString * BankAccountNo;
@property (retain) NSDate * BirthDay;
@property (retain) NSString * BirthPlace;
@property (retain) NSNumber * BirthPlaceId;
@property (retain) NSString * CVUrl;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ContactAddress;
@property (retain) NSNumber * ContactAddressId;
@property (retain) NSString * CulturalLevel;
@property (retain) NSNumber * CulturalLevelId;
@property (retain) NSNumber * EducationLevelId;
@property (retain) NSString * EducationLevelName;
@property (retain) NSString * Email;
@property (retain) NSNumber * EmpBasicProfileId;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSString * EmployeeTypeName;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * Ethnicity;
@property (retain) NSNumber * EthnicityId;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * HomePhone;
@property (retain) NSString * HomeTown;
@property (retain) NSNumber * HomeTownId;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsConcurrentPost;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) NSString * LastName;
@property (retain) NSString * MainLanguage;
@property (retain) NSNumber * MainLanguageId;
@property (retain) NSString * Major;
@property (retain) NSNumber * MajorId;
@property (retain) NSNumber * MaritalStatusId;
@property (retain) NSString * MaritalStatusValue;
@property (retain) NSString * MobileNumber;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSString * OfficePhone;
@property (retain) NSDate * OfficialDate;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * PITCode;
@property (retain) NSString * PITIssuedBy;
@property (retain) NSDate * PassportDateOfExpire;
@property (retain) NSDate * PassportDateOfIssue;
@property (retain) NSString * PassportNo;
@property (retain) NSString * PassportPlaceOfIssue;
@property (retain) NSString * PermanentAddress;
@property (retain) NSNumber * PermanentAddressId;
@property (retain) NSString * PositionName;
@property (retain) NSDate * ProbationaryDate;
@property (retain) NSNumber * ProbationarySalary;
@property (retain) NSString * Reason;
@property (retain) NSString * Religion;
@property (retain) NSNumber * ReligionId;
@property (retain) NSString * TemporaryAddress;
@property (retain) NSNumber * TemporaryAddressId;
@property (retain) NSString * TimeWorkingFormCode;
@property (retain) NSNumber * TimeWorkingFormId;
@property (retain) NSString * UnitName;
@property (retain) NSString * WorkLevelDescription;
@property (retain) NSString * WorkLevelName;
@property (retain) NSString * WorkLocation;
@property (retain) NSDate * WorkingFormImplementationDate;
@property (retain) NSString * WorkingFormTypeCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_CONTACT_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_FTS_EMP_PROFILE_CONTACT_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_CONTACT_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_FTS_EMP_PROFILE_CONTACT_Result:(emp_tns1_SP_FTS_EMP_PROFILE_CONTACT_Result *)toAdd;
@property (readonly) NSMutableArray * SP_FTS_EMP_PROFILE_CONTACT_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SP_FTS_EMP_PROFILE_DEGREE_Result : NSObject {
	
/* elements */
	NSString * Address;
	NSNumber * AddressId;
	NSString * BankAccountNo;
	NSDate * BirthDay;
	NSString * BirthPlace;
	NSNumber * BirthPlaceId;
	NSString * CVUrl;
	NSNumber * CompanyId;
	NSString * ContactAddress;
	NSNumber * ContactAddressId;
	NSString * CulturalLevel;
	NSNumber * CulturalLevelId;
	NSNumber * EducationLevelId;
	NSString * EducationLevelName;
	NSString * Email;
	NSNumber * EmpBasicProfileId;
	NSString * EmpWorkingStatusCode;
	NSString * EmpWorkingStatusName;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EmployeeTypeId;
	NSString * EmployeeTypeName;
	NSDate * EntryDate;
	NSString * Ethnicity;
	NSNumber * EthnicityId;
	NSString * FirstName;
	NSString * FullName;
	USBoolean * Gender;
	NSString * HomePhone;
	NSString * HomeTown;
	NSNumber * HomeTownId;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	NSString * ImageUrl;
	USBoolean * IsConcurrentPost;
	USBoolean * IsDeleted;
	USBoolean * IsOnlyYearOfBirthday;
	NSString * LastName;
	NSString * MainLanguage;
	NSNumber * MainLanguageId;
	NSString * Major;
	NSNumber * MajorId;
	NSNumber * MaritalStatusId;
	NSString * MaritalStatusValue;
	NSString * MobileNumber;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSString * OfficePhone;
	NSDate * OfficialDate;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgWorkLevelCode;
	NSNumber * OrgWorkLevelId;
	NSString * PITCode;
	NSString * PITIssuedBy;
	NSDate * PassportDateOfExpire;
	NSDate * PassportDateOfIssue;
	NSString * PassportNo;
	NSString * PassportPlaceOfIssue;
	NSString * PermanentAddress;
	NSNumber * PermanentAddressId;
	NSString * PositionName;
	NSDate * ProbationaryDate;
	NSNumber * ProbationarySalary;
	NSString * Reason;
	NSString * Religion;
	NSNumber * ReligionId;
	NSString * TemporaryAddress;
	NSNumber * TemporaryAddressId;
	NSString * TimeWorkingFormCode;
	NSNumber * TimeWorkingFormId;
	NSString * UnitName;
	NSString * WorkLevelDescription;
	NSString * WorkLevelName;
	NSString * WorkLocation;
	NSDate * WorkingFormImplementationDate;
	NSString * WorkingFormTypeCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SP_FTS_EMP_PROFILE_DEGREE_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSNumber * AddressId;
@property (retain) NSString * BankAccountNo;
@property (retain) NSDate * BirthDay;
@property (retain) NSString * BirthPlace;
@property (retain) NSNumber * BirthPlaceId;
@property (retain) NSString * CVUrl;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ContactAddress;
@property (retain) NSNumber * ContactAddressId;
@property (retain) NSString * CulturalLevel;
@property (retain) NSNumber * CulturalLevelId;
@property (retain) NSNumber * EducationLevelId;
@property (retain) NSString * EducationLevelName;
@property (retain) NSString * Email;
@property (retain) NSNumber * EmpBasicProfileId;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSString * EmployeeTypeName;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * Ethnicity;
@property (retain) NSNumber * EthnicityId;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * HomePhone;
@property (retain) NSString * HomeTown;
@property (retain) NSNumber * HomeTownId;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsConcurrentPost;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) NSString * LastName;
@property (retain) NSString * MainLanguage;
@property (retain) NSNumber * MainLanguageId;
@property (retain) NSString * Major;
@property (retain) NSNumber * MajorId;
@property (retain) NSNumber * MaritalStatusId;
@property (retain) NSString * MaritalStatusValue;
@property (retain) NSString * MobileNumber;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSString * OfficePhone;
@property (retain) NSDate * OfficialDate;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * PITCode;
@property (retain) NSString * PITIssuedBy;
@property (retain) NSDate * PassportDateOfExpire;
@property (retain) NSDate * PassportDateOfIssue;
@property (retain) NSString * PassportNo;
@property (retain) NSString * PassportPlaceOfIssue;
@property (retain) NSString * PermanentAddress;
@property (retain) NSNumber * PermanentAddressId;
@property (retain) NSString * PositionName;
@property (retain) NSDate * ProbationaryDate;
@property (retain) NSNumber * ProbationarySalary;
@property (retain) NSString * Reason;
@property (retain) NSString * Religion;
@property (retain) NSNumber * ReligionId;
@property (retain) NSString * TemporaryAddress;
@property (retain) NSNumber * TemporaryAddressId;
@property (retain) NSString * TimeWorkingFormCode;
@property (retain) NSNumber * TimeWorkingFormId;
@property (retain) NSString * UnitName;
@property (retain) NSString * WorkLevelDescription;
@property (retain) NSString * WorkLevelName;
@property (retain) NSString * WorkLocation;
@property (retain) NSDate * WorkingFormImplementationDate;
@property (retain) NSString * WorkingFormTypeCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_DEGREE_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_FTS_EMP_PROFILE_DEGREE_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_DEGREE_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_FTS_EMP_PROFILE_DEGREE_Result:(emp_tns1_SP_FTS_EMP_PROFILE_DEGREE_Result *)toAdd;
@property (readonly) NSMutableArray * SP_FTS_EMP_PROFILE_DEGREE_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SP_FTS_EMP_PROFILE_EDU_QUALIFICATION_Result : NSObject {
	
/* elements */
	NSString * Address;
	NSNumber * AddressId;
	NSString * BankAccountNo;
	NSDate * BirthDay;
	NSString * BirthPlace;
	NSNumber * BirthPlaceId;
	NSString * CVUrl;
	NSNumber * CompanyId;
	NSString * ContactAddress;
	NSNumber * ContactAddressId;
	NSString * CulturalLevel;
	NSNumber * CulturalLevelId;
	NSNumber * EducationLevelId;
	NSString * EducationLevelName;
	NSString * Email;
	NSNumber * EmpBasicProfileId;
	NSString * EmpWorkingStatusCode;
	NSString * EmpWorkingStatusName;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EmployeeTypeId;
	NSString * EmployeeTypeName;
	NSDate * EntryDate;
	NSString * Ethnicity;
	NSNumber * EthnicityId;
	NSString * FirstName;
	NSString * FullName;
	USBoolean * Gender;
	NSString * HomePhone;
	NSString * HomeTown;
	NSNumber * HomeTownId;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	NSString * ImageUrl;
	USBoolean * IsConcurrentPost;
	USBoolean * IsDeleted;
	USBoolean * IsOnlyYearOfBirthday;
	NSString * LastName;
	NSString * MainLanguage;
	NSNumber * MainLanguageId;
	NSString * Major;
	NSNumber * MajorId;
	NSNumber * MaritalStatusId;
	NSString * MaritalStatusValue;
	NSString * MobileNumber;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSString * OfficePhone;
	NSDate * OfficialDate;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgWorkLevelCode;
	NSNumber * OrgWorkLevelId;
	NSString * PITCode;
	NSString * PITIssuedBy;
	NSDate * PassportDateOfExpire;
	NSDate * PassportDateOfIssue;
	NSString * PassportNo;
	NSString * PassportPlaceOfIssue;
	NSString * PermanentAddress;
	NSNumber * PermanentAddressId;
	NSString * PositionName;
	NSDate * ProbationaryDate;
	NSNumber * ProbationarySalary;
	NSString * Reason;
	NSString * Religion;
	NSNumber * ReligionId;
	NSString * TemporaryAddress;
	NSNumber * TemporaryAddressId;
	NSString * TimeWorkingFormCode;
	NSNumber * TimeWorkingFormId;
	NSString * UnitName;
	NSString * WorkLevelDescription;
	NSString * WorkLevelName;
	NSString * WorkLocation;
	NSDate * WorkingFormImplementationDate;
	NSString * WorkingFormTypeCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SP_FTS_EMP_PROFILE_EDU_QUALIFICATION_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSNumber * AddressId;
@property (retain) NSString * BankAccountNo;
@property (retain) NSDate * BirthDay;
@property (retain) NSString * BirthPlace;
@property (retain) NSNumber * BirthPlaceId;
@property (retain) NSString * CVUrl;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ContactAddress;
@property (retain) NSNumber * ContactAddressId;
@property (retain) NSString * CulturalLevel;
@property (retain) NSNumber * CulturalLevelId;
@property (retain) NSNumber * EducationLevelId;
@property (retain) NSString * EducationLevelName;
@property (retain) NSString * Email;
@property (retain) NSNumber * EmpBasicProfileId;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSString * EmployeeTypeName;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * Ethnicity;
@property (retain) NSNumber * EthnicityId;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * HomePhone;
@property (retain) NSString * HomeTown;
@property (retain) NSNumber * HomeTownId;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsConcurrentPost;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) NSString * LastName;
@property (retain) NSString * MainLanguage;
@property (retain) NSNumber * MainLanguageId;
@property (retain) NSString * Major;
@property (retain) NSNumber * MajorId;
@property (retain) NSNumber * MaritalStatusId;
@property (retain) NSString * MaritalStatusValue;
@property (retain) NSString * MobileNumber;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSString * OfficePhone;
@property (retain) NSDate * OfficialDate;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * PITCode;
@property (retain) NSString * PITIssuedBy;
@property (retain) NSDate * PassportDateOfExpire;
@property (retain) NSDate * PassportDateOfIssue;
@property (retain) NSString * PassportNo;
@property (retain) NSString * PassportPlaceOfIssue;
@property (retain) NSString * PermanentAddress;
@property (retain) NSNumber * PermanentAddressId;
@property (retain) NSString * PositionName;
@property (retain) NSDate * ProbationaryDate;
@property (retain) NSNumber * ProbationarySalary;
@property (retain) NSString * Reason;
@property (retain) NSString * Religion;
@property (retain) NSNumber * ReligionId;
@property (retain) NSString * TemporaryAddress;
@property (retain) NSNumber * TemporaryAddressId;
@property (retain) NSString * TimeWorkingFormCode;
@property (retain) NSNumber * TimeWorkingFormId;
@property (retain) NSString * UnitName;
@property (retain) NSString * WorkLevelDescription;
@property (retain) NSString * WorkLevelName;
@property (retain) NSString * WorkLocation;
@property (retain) NSDate * WorkingFormImplementationDate;
@property (retain) NSString * WorkingFormTypeCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_EDU_QUALIFICATION_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_FTS_EMP_PROFILE_EDU_QUALIFICATION_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_EDU_QUALIFICATION_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_FTS_EMP_PROFILE_EDU_QUALIFICATION_Result:(emp_tns1_SP_FTS_EMP_PROFILE_EDU_QUALIFICATION_Result *)toAdd;
@property (readonly) NSMutableArray * SP_FTS_EMP_PROFILE_EDU_QUALIFICATION_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SP_FTS_EMP_PROFILE_FOREIGN_LANGUAGE_Result : NSObject {
	
/* elements */
	NSString * Address;
	NSNumber * AddressId;
	NSString * BankAccountNo;
	NSDate * BirthDay;
	NSString * BirthPlace;
	NSNumber * BirthPlaceId;
	NSString * CVUrl;
	NSNumber * CompanyId;
	NSString * ContactAddress;
	NSNumber * ContactAddressId;
	NSString * CulturalLevel;
	NSNumber * CulturalLevelId;
	NSNumber * EducationLevelId;
	NSString * EducationLevelName;
	NSString * Email;
	NSNumber * EmpBasicProfileId;
	NSString * EmpWorkingStatusCode;
	NSString * EmpWorkingStatusName;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EmployeeTypeId;
	NSString * EmployeeTypeName;
	NSDate * EntryDate;
	NSString * Ethnicity;
	NSNumber * EthnicityId;
	NSString * FirstName;
	NSString * FullName;
	USBoolean * Gender;
	NSString * HomePhone;
	NSString * HomeTown;
	NSNumber * HomeTownId;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	NSString * ImageUrl;
	USBoolean * IsConcurrentPost;
	USBoolean * IsDeleted;
	USBoolean * IsOnlyYearOfBirthday;
	NSString * LastName;
	NSString * MainLanguage;
	NSNumber * MainLanguageId;
	NSString * Major;
	NSNumber * MajorId;
	NSNumber * MaritalStatusId;
	NSString * MaritalStatusValue;
	NSString * MobileNumber;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSString * OfficePhone;
	NSDate * OfficialDate;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgWorkLevelCode;
	NSNumber * OrgWorkLevelId;
	NSString * PITCode;
	NSString * PITIssuedBy;
	NSDate * PassportDateOfExpire;
	NSDate * PassportDateOfIssue;
	NSString * PassportNo;
	NSString * PassportPlaceOfIssue;
	NSString * PermanentAddress;
	NSNumber * PermanentAddressId;
	NSString * PositionName;
	NSDate * ProbationaryDate;
	NSNumber * ProbationarySalary;
	NSString * Reason;
	NSString * Religion;
	NSNumber * ReligionId;
	NSString * TemporaryAddress;
	NSNumber * TemporaryAddressId;
	NSString * TimeWorkingFormCode;
	NSNumber * TimeWorkingFormId;
	NSString * UnitName;
	NSString * WorkLevelDescription;
	NSString * WorkLevelName;
	NSString * WorkLocation;
	NSDate * WorkingFormImplementationDate;
	NSString * WorkingFormTypeCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SP_FTS_EMP_PROFILE_FOREIGN_LANGUAGE_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSNumber * AddressId;
@property (retain) NSString * BankAccountNo;
@property (retain) NSDate * BirthDay;
@property (retain) NSString * BirthPlace;
@property (retain) NSNumber * BirthPlaceId;
@property (retain) NSString * CVUrl;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ContactAddress;
@property (retain) NSNumber * ContactAddressId;
@property (retain) NSString * CulturalLevel;
@property (retain) NSNumber * CulturalLevelId;
@property (retain) NSNumber * EducationLevelId;
@property (retain) NSString * EducationLevelName;
@property (retain) NSString * Email;
@property (retain) NSNumber * EmpBasicProfileId;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSString * EmployeeTypeName;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * Ethnicity;
@property (retain) NSNumber * EthnicityId;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * HomePhone;
@property (retain) NSString * HomeTown;
@property (retain) NSNumber * HomeTownId;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsConcurrentPost;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) NSString * LastName;
@property (retain) NSString * MainLanguage;
@property (retain) NSNumber * MainLanguageId;
@property (retain) NSString * Major;
@property (retain) NSNumber * MajorId;
@property (retain) NSNumber * MaritalStatusId;
@property (retain) NSString * MaritalStatusValue;
@property (retain) NSString * MobileNumber;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSString * OfficePhone;
@property (retain) NSDate * OfficialDate;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * PITCode;
@property (retain) NSString * PITIssuedBy;
@property (retain) NSDate * PassportDateOfExpire;
@property (retain) NSDate * PassportDateOfIssue;
@property (retain) NSString * PassportNo;
@property (retain) NSString * PassportPlaceOfIssue;
@property (retain) NSString * PermanentAddress;
@property (retain) NSNumber * PermanentAddressId;
@property (retain) NSString * PositionName;
@property (retain) NSDate * ProbationaryDate;
@property (retain) NSNumber * ProbationarySalary;
@property (retain) NSString * Reason;
@property (retain) NSString * Religion;
@property (retain) NSNumber * ReligionId;
@property (retain) NSString * TemporaryAddress;
@property (retain) NSNumber * TemporaryAddressId;
@property (retain) NSString * TimeWorkingFormCode;
@property (retain) NSNumber * TimeWorkingFormId;
@property (retain) NSString * UnitName;
@property (retain) NSString * WorkLevelDescription;
@property (retain) NSString * WorkLevelName;
@property (retain) NSString * WorkLocation;
@property (retain) NSDate * WorkingFormImplementationDate;
@property (retain) NSString * WorkingFormTypeCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_FOREIGN_LANGUAGE_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_FTS_EMP_PROFILE_FOREIGN_LANGUAGE_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_FOREIGN_LANGUAGE_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_FTS_EMP_PROFILE_FOREIGN_LANGUAGE_Result:(emp_tns1_SP_FTS_EMP_PROFILE_FOREIGN_LANGUAGE_Result *)toAdd;
@property (readonly) NSMutableArray * SP_FTS_EMP_PROFILE_FOREIGN_LANGUAGE_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_SP_FTS_EMP_PROFILE_JOBPOSITION_Result : NSObject {
	
/* elements */
	NSString * Address;
	NSNumber * AddressId;
	NSString * BankAccountNo;
	NSDate * BirthDay;
	NSString * BirthPlace;
	NSNumber * BirthPlaceId;
	NSString * CVUrl;
	NSNumber * CompanyId;
	NSString * ContactAddress;
	NSNumber * ContactAddressId;
	NSString * CulturalLevel;
	NSNumber * CulturalLevelId;
	NSNumber * EducationLevelId;
	NSString * EducationLevelName;
	NSString * Email;
	NSNumber * EmpBasicProfileId;
	NSString * EmpWorkingStatusCode;
	NSString * EmpWorkingStatusName;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EmployeeTypeId;
	NSString * EmployeeTypeName;
	NSDate * EntryDate;
	NSString * Ethnicity;
	NSNumber * EthnicityId;
	NSString * FirstName;
	NSString * FullName;
	USBoolean * Gender;
	NSString * HomePhone;
	NSString * HomeTown;
	NSNumber * HomeTownId;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	NSString * ImageUrl;
	USBoolean * IsConcurrentPost;
	USBoolean * IsDeleted;
	USBoolean * IsOnlyYearOfBirthday;
	NSString * LastName;
	NSString * MainLanguage;
	NSNumber * MainLanguageId;
	NSString * Major;
	NSNumber * MajorId;
	NSNumber * MaritalStatusId;
	NSString * MaritalStatusValue;
	NSString * MobileNumber;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSString * OfficePhone;
	NSDate * OfficialDate;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgWorkLevelCode;
	NSNumber * OrgWorkLevelId;
	NSString * PITCode;
	NSString * PITIssuedBy;
	NSDate * PassportDateOfExpire;
	NSDate * PassportDateOfIssue;
	NSString * PassportNo;
	NSString * PassportPlaceOfIssue;
	NSString * PermanentAddress;
	NSNumber * PermanentAddressId;
	NSString * PositionName;
	NSDate * ProbationaryDate;
	NSNumber * ProbationarySalary;
	NSString * Reason;
	NSString * Religion;
	NSNumber * ReligionId;
	NSString * TemporaryAddress;
	NSNumber * TemporaryAddressId;
	NSString * TimeWorkingFormCode;
	NSNumber * TimeWorkingFormId;
	NSString * UnitName;
	NSString * WorkLevelDescription;
	NSString * WorkLevelName;
	NSString * WorkLocation;
	NSDate * WorkingFormImplementationDate;
	NSString * WorkingFormTypeCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_SP_FTS_EMP_PROFILE_JOBPOSITION_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSNumber * AddressId;
@property (retain) NSString * BankAccountNo;
@property (retain) NSDate * BirthDay;
@property (retain) NSString * BirthPlace;
@property (retain) NSNumber * BirthPlaceId;
@property (retain) NSString * CVUrl;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ContactAddress;
@property (retain) NSNumber * ContactAddressId;
@property (retain) NSString * CulturalLevel;
@property (retain) NSNumber * CulturalLevelId;
@property (retain) NSNumber * EducationLevelId;
@property (retain) NSString * EducationLevelName;
@property (retain) NSString * Email;
@property (retain) NSNumber * EmpBasicProfileId;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSString * EmployeeTypeName;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * Ethnicity;
@property (retain) NSNumber * EthnicityId;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * HomePhone;
@property (retain) NSString * HomeTown;
@property (retain) NSNumber * HomeTownId;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsConcurrentPost;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) NSString * LastName;
@property (retain) NSString * MainLanguage;
@property (retain) NSNumber * MainLanguageId;
@property (retain) NSString * Major;
@property (retain) NSNumber * MajorId;
@property (retain) NSNumber * MaritalStatusId;
@property (retain) NSString * MaritalStatusValue;
@property (retain) NSString * MobileNumber;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSString * OfficePhone;
@property (retain) NSDate * OfficialDate;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * PITCode;
@property (retain) NSString * PITIssuedBy;
@property (retain) NSDate * PassportDateOfExpire;
@property (retain) NSDate * PassportDateOfIssue;
@property (retain) NSString * PassportNo;
@property (retain) NSString * PassportPlaceOfIssue;
@property (retain) NSString * PermanentAddress;
@property (retain) NSNumber * PermanentAddressId;
@property (retain) NSString * PositionName;
@property (retain) NSDate * ProbationaryDate;
@property (retain) NSNumber * ProbationarySalary;
@property (retain) NSString * Reason;
@property (retain) NSString * Religion;
@property (retain) NSNumber * ReligionId;
@property (retain) NSString * TemporaryAddress;
@property (retain) NSNumber * TemporaryAddressId;
@property (retain) NSString * TimeWorkingFormCode;
@property (retain) NSNumber * TimeWorkingFormId;
@property (retain) NSString * UnitName;
@property (retain) NSString * WorkLevelDescription;
@property (retain) NSString * WorkLevelName;
@property (retain) NSString * WorkLocation;
@property (retain) NSDate * WorkingFormImplementationDate;
@property (retain) NSString * WorkingFormTypeCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_JOBPOSITION_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_FTS_EMP_PROFILE_JOBPOSITION_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (emp_tns1_ArrayOfSP_FTS_EMP_PROFILE_JOBPOSITION_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_FTS_EMP_PROFILE_JOBPOSITION_Result:(emp_tns1_SP_FTS_EMP_PROFILE_JOBPOSITION_Result *)toAdd;
@property (readonly) NSMutableArray * SP_FTS_EMP_PROFILE_JOBPOSITION_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
