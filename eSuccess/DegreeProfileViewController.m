//
//  DegreeProfileViewController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 6/26/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "DegreeProfileViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "DayLeaveDetailCell.h"
@interface DegreeProfileViewController ()

@end

@implementation DegreeProfileViewController
@synthesize orgDegreeArr;
@synthesize degreeNameArr;
static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Chứng chỉ";
    [self getData];
    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 130;
//}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([degreeNameArr count] > 0) {
        return [degreeNameArr objectAtIndex:section];
    }
    return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [orgDegreeArr count] > 0 ? orgDegreeArr.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [orgDegreeArr count] > 0 ? 7 : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (orgDegreeArr.count == 0)
        return [self getDefaultEmptyCell];
    
    DayLeaveDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
    if (cell == nil) {
        cell = [[DayLeaveDetailCell alloc]init];
    }
    cell.lbLeft.font = [UIFont systemFontOfSize:14];
    cell.lbLeft.backgroundColor = [UIColor clearColor];
    cell.lbRight.font = [UIFont boldSystemFontOfSize:16];
    cell.lbRight.backgroundColor = [UIColor clearColor];
    
    int section = indexPath.section;
    empl_tns1_V_EmpProfileDegree *degree = [orgDegreeArr objectAtIndex:section];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    if (indexPath.row == 0) {
        cell.lbLeft.text = @"Xếp loại";
        cell.lbRight.text = degree.OrgDegreeRankName;
    }
    if (indexPath.row == 1) {
        cell.lbLeft.text = @"Hiệu lực";
        if ([degree.IsValId boolValue] == true) {
            cell.lbRight.text = @"Còn hiệu lực";
        } else
            cell.lbRight.text = @"Hết hiệu lực";
    }
    if (indexPath.row == 2) {
        cell.lbLeft.text = @"Ngày cấp";
        cell.lbRight.text = [formatter stringFromDate:degree.DateIssue];
    }
    if (indexPath.row == 3) {
        cell.lbLeft.text = @"Ngày hết hạn";
        cell.lbRight.text = [formatter stringFromDate:degree.DateExpire];
    }
    if (indexPath.row == 4) {
        cell.lbLeft.text = @"Nơi cấp";
        cell.lbRight.text = degree.PlaceIssue;
    }
    if (indexPath.row == 5) {
        cell.lbLeft.text = @"Mô tả";
        cell.lbRight.text = degree.OrgDegreeDescription;
    }
    if (indexPath.row == 6) {
        cell.lbLeft.text = @"Ghi chú";
        cell.lbRight.text = degree.Other;
    }
    return cell;
}

#pragma get Data Degree
-(void)getData
{
    orgDegreeArr = [[NSMutableArray alloc]init];
    degreeNameArr= [[NSMutableArray alloc]init];
    
    [self getEmpProfileDegreeById];
}
-(void)getEmpProfileDegreeById
{
    BasicHttpBinding_IEmpProfileLayerServiceBinding * binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
    EmpProfileLayerServiceSvc_GetViewEmpProfileDegreeById *request = [[EmpProfileLayerServiceSvc_GetViewEmpProfileDegreeById alloc] init];
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    if (app.otherEmpId != nil) {
        request.EmployeeId = app.otherEmpId;
    } else{
        request.employeeId = app.sysUser.EmployeeId ;
    }
    [binding GetViewEmpProfileDegreeByIdAsyncUsingParameters:request delegate:self];
}


- (void)operation:(BasicHttpBinding_IEmpProfileLayerServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)response
{
    if ([response.bodyParts count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi kết nối" message:@"Không thể kết nối đến server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    for (id mine in response.bodyParts) {
        if ([mine isKindOfClass:[EmpProfileLayerServiceSvc_GetViewEmpProfileDegreeByIdResponse class]]) {
            empl_tns1_ArrayOfV_EmpProfileDegree *result = [mine GetViewEmpProfileDegreeByIdResult];
            orgDegreeArr = result.V_EmpProfileDegree;
            for (empl_tns1_V_EmpProfileDegree *degree in orgDegreeArr) {
                [degreeNameArr addObject:degree.OrgDegreeName];
            }
            
            if (!self.viewDidDisappear)
                [self.tableView reloadData];
        }
    }
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

@end
