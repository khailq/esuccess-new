//
//  WageTypeViewController.h
//  eSuccess
//
//  Created by admin on 6/15/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"
#import "EmpProfileLayerServiceSvc.h"
@interface WageTypeViewController : BaseDetailTableViewController<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>
@property(nonatomic, strong) NSMutableArray *wageTypeArr;
@property(nonatomic, strong) NSMutableArray *typeNameArr;
@end
