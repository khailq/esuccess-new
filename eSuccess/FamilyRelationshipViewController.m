//
//  FamilyRelationshipViewController.m
//  eSuccess
//
//  Created by admin on 6/15/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "FamilyRelationshipViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "DayLeaveDetailCell.h"
@interface FamilyRelationshipViewController ()

@end

@implementation FamilyRelationshipViewController
@synthesize familyArr;
static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Quan hệ gia đình";
    [self getData];
    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 130;
//}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([familyArr count]>0)
    {
        empl_tns1_V_EmpProfileFamilyRelationship *relationship = familyArr[section];
        return relationship.FamilyRelationshipName;
    }
    
    return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [familyArr count] > 0 ? familyArr.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //return [profileArray count];
    return [familyArr count] > 0 ? 8 : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (familyArr.count == 0) {
        return [self getDefaultEmptyCell];
    }
    
    DayLeaveDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
    if (cell == nil) {
        cell = [[DayLeaveDetailCell alloc]init];
    }
    cell.lbLeft.font = [UIFont systemFontOfSize:14];
    cell.lbLeft.backgroundColor = [UIColor clearColor];
    cell.lbRight.font = [UIFont boldSystemFontOfSize:16];
    cell.lbRight.backgroundColor = [UIColor clearColor];
    int section = indexPath.section;
    empl_tns1_V_EmpProfileFamilyRelationship *family = [familyArr objectAtIndex:section];
    if (indexPath.row == 0) {
        cell.lbLeft.text = @"Họ tên";
        cell.lbRight.text = family.PersonFullName;
    }
    if (indexPath.row == 1) {
        cell.lbLeft.text = @"Phụ thuộc";
        if (family.Nourish == false) {
            cell.lbRight.text = @"Không";
        } else
            cell.lbRight.text = @"Có";
    }
    if (indexPath.row == 2) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"dd/MM/yyyy"];
        cell.lbLeft.text = @"Ngày sinh";
        cell.lbRight.text = [formatter stringFromDate:family.BirthDay];
    }
    if (indexPath.row == 3) {
        cell.lbLeft.text = @"Quê quán";
        cell.lbRight.text = family.HomeTown;
    }
    if (indexPath.row == 4) {
        cell.lbLeft.text = @"Công việc";
        cell.lbRight.text = family.Occupation;
    }
    if (indexPath.row == 5) {
        cell.lbLeft.text = @"Nơi làm việc";
        cell.lbRight.text = family.OccupationPlace;
    }
    if (indexPath.row == 6) {
        cell.lbLeft.text = @"Địa chỉ";
        cell.lbRight.text = family.Address;
    }
    if (indexPath.row == 7) {
        cell.lbLeft.text = @"Điện thoại";
        cell.lbRight.text = family.Phone;
    }
    return cell;
}

#pragma get Data Profile
- (void)getData
{
    familyArr = [[NSMutableArray alloc]init];
    
    [self getEmpFamilyRelationshipById];
}

- (void)getEmpFamilyRelationshipById
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Đang cập nhật";
    
    BasicHttpBinding_IEmpProfileLayerServiceBinding *binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
    EmpProfileLayerServiceSvc_GetViewEmpFamilyRelationshipById *request = [[EmpProfileLayerServiceSvc_GetViewEmpFamilyRelationshipById    alloc] init];
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    if (app.otherEmpId != nil)
        request.employeeId = app.otherEmpId;
    else
        request.employeeId = app.sysUser.EmployeeId ;
    //request.EmployeeId = [NSNumber numberWithInt:2];
    [binding GetViewEmpFamilyRelationshipByIdAsyncUsingParameters:request delegate:self];
}

- (void)operation:(BasicHttpBinding_IEmpProfileLayerServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)response
{
    if ([response.bodyParts count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi kết nối" message:@"Không thể kết nối đến server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    for (id mine in response.bodyParts){
        if ([mine isKindOfClass:[EmpProfileLayerServiceSvc_GetViewEmpFamilyRelationshipByIdResponse class]]) {
            empl_tns1_ArrayOfV_EmpProfileFamilyRelationship *result = [mine GetViewEmpFamilyRelationshipByIdResult];
            familyArr = result.V_EmpProfileFamilyRelationship;
        }
        
        if (!self.viewDidDisappear)
            [self.tableView reloadData];
        //        if ([mine isKindOfClass:[SOAPFault class]])
        //        {
        //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Server error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //            [alert show];
        //        }
    }
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

@end
