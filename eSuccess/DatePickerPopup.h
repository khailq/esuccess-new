//
//  DatePickerPopup.h
//  eSuccess
//
//  Created by Scott on 4/21/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>
#define MyDateTimePickerHeight 260

@interface DatePickerPopup : UIView

@property (nonatomic, assign, readonly) UIDatePicker *picker;

- (void) setMode: (UIDatePickerMode) mode;
- (void) setHidden: (BOOL) hidden animated: (BOOL) animated;
- (void) addTargetForDoneButton: (id) target action: (SEL) action;

@end
