//
//  WorkingExperienceViewController.m
//  eSuccess
//
//  Created by admin on 5/11/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "WorkingExperienceViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "DayLeaveDetailCell.h"
@interface WorkingExperienceViewController ()

@end

@implementation WorkingExperienceViewController
@synthesize workingArray;
@synthesize projectArray;
static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Kinh nghiệm làm việc";
    [self getData];
    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([projectArray count] > 0) {
        return [projectArray objectAtIndex:section];
    }
    return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [workingArray count] > 0 ? workingArray.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([workingArray count] > 0){
        return 5;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (workingArray.count == 0) {
        return [self getDefaultEmptyCell];
    }
    
    DayLeaveDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
    if (cell == nil) {
        cell = [[DayLeaveDetailCell alloc]init];
    }
    cell.lbLeft.font = [UIFont systemFontOfSize:14];
    cell.lbLeft.backgroundColor = [UIColor clearColor];
    cell.lbRight.font = [UIFont boldSystemFontOfSize:16];
    cell.lbRight.backgroundColor = [UIColor clearColor];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    int section = indexPath.section;
    empl_tns1_EmpProfileExperience *workingExp = [workingArray objectAtIndex:section];
    if (indexPath.row == 0) {
        cell.lbLeft.text = @"Khách hàng";
        cell.lbRight.text = workingExp.Customer;
    }
    if (indexPath.row == 1) {
        cell.lbLeft.text = @"Thời gian";
        if (workingExp.StartDate == nil && workingExp.EndDate == nil) {
            cell.lbRight.text = @"Đang cập nhật";
        } else
            cell.lbRight.text = [NSString stringWithFormat:@"%@ - %@", [formatter stringFromDate:workingExp.StartDate], [formatter stringFromDate:workingExp.EndDate]];
    }
    if (indexPath.row == 2) {
        cell.lbLeft.text = @"Vai trò";
        cell.lbRight.text = workingExp.Roles;
    }
    
    if (indexPath.row == 3) {
        cell.lbLeft.text = @"Team Size";
        cell.lbRight.text = workingExp.TeamSize;
    }
    if (indexPath.row == 4) {
        cell.lbLeft.text = @"Kết quả";
        cell.lbRight.text = workingExp.ProjectResult;
    }
    return cell;
}

#pragma get Data Working Experience
- (void)getData
{
    workingArray = [[NSMutableArray alloc]init];
    projectArray = [[NSMutableArray alloc]init];
    
    [self getEmpExperienceById];
}

- (void)getEmpExperienceById
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Đang cập nhật";
    
    BasicHttpBinding_IEmpProfileLayerServiceBinding *binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
    EmpProfileLayerServiceSvc_GetViewEmpProfileExperienceById *request = [[EmpProfileLayerServiceSvc_GetViewEmpProfileExperienceById alloc]init];
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    if (app.otherEmpId != nil)
        request.employeeId = app.otherEmpId;
    else
        request.employeeId = app.sysUser.EmployeeId ;
    //request.employeeId = [NSNumber numberWithInt:2];
    [binding GetViewEmpProfileExperienceByIdAsyncUsingParameters:request delegate:self];
}

- (void)operation:(BasicHttpBinding_IEmpProfileLayerServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)response
{
    if ([response.bodyParts count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi kết nối" message:@"Không thể kết nối đến server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    for (id mine in response.bodyParts) {
        if ([mine isKindOfClass:[EmpProfileLayerServiceSvc_GetViewEmpProfileExperienceByIdResponse class]]) {
            empl_tns1_ArrayOfEmpProfileExperience *result = [mine GetViewEmpProfileExperienceByIdResult];
            workingArray = result.EmpProfileExperience;
            for (empl_tns1_EmpProfileExperience *arr in workingArray) {
                [projectArray addObject:arr.ProjectName];
            }
            if (!self.viewDidDisappear)
                [self.tableView reloadData];
        }
        
//        if ([mine isKindOfClass:[SOAPFault class]])
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Server error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//            [alert show];
//        }
    }
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

@end
