//
//  PopoverContentViewController.h
//  Presidents
//
//  Created by HPTVIETNAM on 4/13/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeaverRequestViewController.h"

@interface PopoverContentViewController : UITableViewController

@property(weak, nonatomic) LeaverRequestViewController *parentViewController;
@property (copy, nonatomic) NSArray *data;
@property (nonatomic) NSInteger type;

-(void)addTarget:(id)target forSelectionChanged:(SEL)selector;
@end
