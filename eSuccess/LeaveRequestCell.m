//
//  LeaveRequestCell.m
//  eSuccess
//
//  Created by HPTVIETNAM on 4/26/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "LeaveRequestCell.h"


@interface LeaveRequestCell ()
{
    
}
@property (assign, nonatomic) id editTarget;
@property (assign, nonatomic) SEL acceptSelector;
@property (assign, nonatomic) SEL denySelector;


@end

@implementation LeaveRequestCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)willTransitionToState:(UITableViewCellStateMask)state
{
    [super willTransitionToState:state];
    if (state == UITableViewCellStateEditingMask)
    {
        self.isAcceptSelected = self.isDenySelected = NO;
        [self.btnAccept setType:BButtonTypeSuccess];
        [self.btnDeny setType:BButtonTypeDanger];
        
        [self.btnAccept setHidden:NO];
        [self.btnDeny setHidden:NO];
    }
    else
    {
        [self.btnAccept setHidden:YES];
        [self.btnDeny setHidden:YES];
    }
}

-(void)btnAccept_clicked:(id)sender
{
    if (!self.isAcceptSelected)
        [self.btnAccept setType:BButtonTypeWarning];
    else
        [self.btnAccept setType:BButtonTypeSuccess];
    
    self.isAcceptSelected = !self.isAcceptSelected;
    
    if (self.isDenySelected)
    {
        [self.btnDeny setType:BButtonTypeDanger];
        self.isDenySelected = !self.isDenySelected;
    }
    
    if (self.editTarget)
        if ([self.editTarget respondsToSelector:self.acceptSelector])
            [self.editTarget performSelectorOnMainThread:self.acceptSelector withObject:self.leaveRequestId waitUntilDone:NO];
}

-(void)btnDeny_clicked:(id)sender
{
    if (!self.isDenySelected)
        [self.btnDeny setType:BButtonTypeWarning];
    else
        [self.btnDeny setType:BButtonTypeDanger];
    
    self.isDenySelected = !self.isDenySelected;
    
    if (self.isAcceptSelected)
    {
        [self.btnAccept setType:BButtonTypeSuccess];
        self.isAcceptSelected = !self.isAcceptSelected;
    }
    
    if (self.editTarget)
        if ([self.editTarget respondsToSelector:self.denySelector])
            [self.editTarget performSelectorOnMainThread:self.denySelector withObject:self.leaveRequestId waitUntilDone:NO];
}

-(void)addTarget:(id)target forAcceptAction:(SEL)acceptSel denyAction:(SEL)denySel;
{
    self.editTarget = target;
    self.acceptSelector = acceptSel;
    self.denySelector = denySel;
}
@end
