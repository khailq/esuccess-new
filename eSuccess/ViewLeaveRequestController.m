//
//  ViewLeaveRequestController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 5/3/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "ViewLeaveRequestController.h"
#import "DayLeaveDetailCell.h"
#import "MBProgressHUD.h"
#import "AFHTTPClient.h"
#import "AppDelegate.h"
#import "DatePickerPopup.h"
#import "PopoverContentViewController.h"

static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";


@interface ViewLeaveRequestController ()
{
    NSArray *timeAbsenceTypes;
    NSArray *approvers;
    NSDateFormatter *dateFormatter;
    NSMutableArray *absenceTypeStringList;
    NSMutableArray *approverStringList;
}
@property (strong, nonatomic) DatePickerPopup *datePicker;
@property (strong, nonatomic) NSArray *arrayOfLeaveRequestDetail;
@property (strong, nonatomic) UIPopoverController *popover;

@property (weak, nonatomic) IBOutlet UILabel *lb_empId;
@property (weak, nonatomic) IBOutlet UILabel *lb_empName;
@property (weak, nonatomic) IBOutlet UILabel *lb_unitName;
@property (weak, nonatomic) IBOutlet UILabel *lb_position;
@property (weak, nonatomic) IBOutlet UILabel *lb_issueDate;
@property (weak, nonatomic) IBOutlet UILabel *lb_startDate;
@property (weak, nonatomic) IBOutlet UILabel *lb_endDate;
@property (weak, nonatomic) IBOutlet UILabel *lb_absenceType;
@property (weak, nonatomic) IBOutlet UILabel *lb_approver;
@property (weak, nonatomic) IBOutlet UILabel *lb_email;
@property (weak, nonatomic) IBOutlet UILabel *lb_approvalStatus;

@property (weak, nonatomic) IBOutlet UITextView *tb_reason;
@end

@implementation ViewLeaveRequestController
{
    NSDateFormatter *dateFormatter;
    NSArray *leaveStatusArray;
    BOOL viewDidLoad;
    MBProgressHUD *hud;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    leaveStatusArray = @[@"Cả ngày/Full", @"Sáng/AM", @"Chiều/PM"];

    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
    
    viewDidLoad = YES;
}



-(void)setIsManageView:(NSNumber *)isManageView
{
    if (![_isManageView isEqualToNumber:isManageView])
    {
        _isManageView = isManageView;
        
        BOOL isManage = [_isManageView boolValue];
        if (isManage && viewDidLoad)
        {
            UIBarButtonItem *btnConfirm = [[UIBarButtonItem alloc] initWithTitle:@"Đồng ý" style:UIBarButtonItemStyleDone target:self action:@selector(confirmButtonItemClicked:)];
            UIBarButtonItem *btnDeny = [[UIBarButtonItem alloc] initWithTitle:@"Từ chối" style:UIBarButtonItemStyleBordered target:self action:@selector(denyButtonItemClicked:)];
            self.navigationItem.rightBarButtonItems = @[btnConfirm, btnDeny];
        }
    }
}

-(void)setIsEditable:(NSNumber *)isEditable
{
    if(![_isEditable isEqualToNumber:isEditable])
    {
        _isEditable = isEditable;
        
        if ([self.isEditable boolValue])
        {
            UIBarButtonItem *btnEdit = [[UIBarButtonItem alloc] initWithTitle:@"Sửa" style:UIBarButtonItemStyleDone target:self action:@selector(editButtonItemClicked:)];
            self.navigationItem.rightBarButtonItem = btnEdit;
        }
    }
}
-(void)editButtonItemClicked:(id)sender
{
    
}

-(void)confirmButtonItemClicked:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Bạn muốn phê duyệt đơn xin nghỉ này?" delegate:self cancelButtonTitle:@"Không" otherButtonTitles:@"Có", nil];
    alert.tag = 1;
    [alert show];
}

-(void)denyButtonItemClicked:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Bạn muốn từ chối đơn xin nghỉ này?" delegate:self cancelButtonTitle:@"Không" otherButtonTitles:@"Có", nil];
    alert.tag = 2;
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1)
        if (buttonIndex == 1)
            [self approveLeaveRequest:self.leaveRequestId];
    if (alertView.tag == 2)
        if (buttonIndex == 1)
            [self denyLeaveRequest:self.leaveRequestId];
    if (alertView.tag == 10)
        [self.navigationController popViewControllerAnimated:YES];
}

//-(void)setLeaveRequestId:(NSNumber *)leaveRequestId
//{
//    if (![_leaveRequestId isEqualToNumber:leaveRequestId])
//    {
//        _leaveRequestId = leaveRequestId;
//        
//        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        hud.mode = MBProgressHUDModeIndeterminate;
//        
//        BasicHttpBinding_ILeaveRequestServiceBinding *binding = [LeaveRequestServiceSvc BasicHttpBinding_ILeaveRequestServiceBinding];
//        LeaveRequestServiceSvc_GetListLeaveRequestDetailView *request = [[LeaveRequestServiceSvc_GetListLeaveRequestDetailView alloc] init];
//        request.leaveRequestId = _leaveRequestId;
//        
//        [binding GetListLeaveRequestDetailViewAsyncUsingParameters:request delegate:self];
//    }
//}
//-(void)setArrayOfLeaveRequestDetail:(NSArray *)array
//{
//    if (_arrayOfLeaveRequestDetail != array)
//    {
//        _arrayOfLeaveRequestDetail = array;
//        if (_arrayOfLeaveRequestDetail.count == 0)
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Không lấy được thông tin đơn nghỉ phép" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//            alert.tag = 10;
//            [alert show];
//            return;
//        }
//        
//        if (!viewDidLoad)
//            return;
//        
//        tns1_V_SelfLeaveRequestDetail *detail = _arrayOfLeaveRequestDetail[0];
//        self.lb_empId.text = detail.EmployeeCode;
//        //self.lb_empName.text = detail.EmployeeName;
//        NSLog(@"name: %@", detail.EmployeeName);
//        //self.lb_empName.text = @"Test";
//        
//        self.lb_email.text = detail.EmpEmail;
//        self.lb_unitName.text = detail.EmpUnitName;
//        self.lb_position.text = detail.EmpJobName;
//        
//        self.lb_issueDate.text = [dateFormatter stringFromDate:detail.SelfLeaveRequestCreatedDate];
//        self.lb_startDate.text = [dateFormatter stringFromDate:detail.StartDate];
//        self.lb_endDate.text = [dateFormatter stringFromDate:detail.EndDate];
//        self.lb_absenceType.text = detail.TimeAbsenceTypeName;
//        self.lb_approver.text = detail.ApproverName;
//        
//        self.tb_reason.text = detail.Reason;
//        self.lb_empName.text = detail.EmployeeName;
//        
//        switch (detail.ApprovalStatus.intValue) {
//            case 1:
//                self.lb_approvalStatus.text = @"Chờ phê duyệt";
//                break;
//            case 2:
//                self.lb_approvalStatus.text = @"Đã phê duyệt";
//                break;
//            default:
//                self.lb_approvalStatus.text = @"Không chấp nhận";
//                break;
//        }
//        
//        [self.tableView reloadData];
//    }
//}

//-(void)populateArray
//{
//    AppDelegate *app = [UIApplication sharedApplication].delegate;
//    timeAbsenceTypes = app.timeAbsenceTypes;
//    approvers = app.approvers;
//    
//    approverStringList = [NSMutableArray array];
//    for(tns1_SP_GetAllParentEmployee_Result *approver in approvers)
//    {
//        [approverStringList addObject:approver.ParentFullName];
//    }
//    
//    absenceTypeStringList = [NSMutableArray array];
//    for(tns1_TimeAbsenceType *absenceType in timeAbsenceTypes)
//        [absenceTypeStringList addObject:absenceType.Name];
//    
//}

//-(void)initPicker:(BOOL)isToDateLabel
//{
//    tns1_V_SelfLeaveRequestDetail *detail = self.arrayOfLeaveRequestDetail[0];
//    
//    CGRect screenRect = self.view.frame;
//    
//    CGFloat y = screenRect.size.height;
//    y = y - MyDateTimePickerHeight - screenRect.origin.y;
//    CGRect rect = CGRectMake(0, y, screenRect.size.width, MyDateTimePickerHeight);
//    self.datePicker = [[DatePickerPopup alloc] initWithFrame:rect];
//    
//    if(isToDateLabel)
//    {
//        [self.datePicker.picker setMinimumDate:detail.StartDate];
//        [self.datePicker.picker setDate:detail.EndDate];
//    }
//    else
//    {
//        [self.datePicker.picker setMinimumDate:detail.StartDate];
//        [self.datePicker.picker setDate:detail.StartDate];
//    }
//    
//    [self.datePicker setMode:UIDatePickerModeDate];
//    [self.datePicker addTargetForDoneButton:self action:@selector(onDoneButtonClicked:)];
//    [self.datePicker.picker addTarget:self action:@selector(onDatePickerSelectionChanged:) forControlEvents:UIControlEventValueChanged];
//    [self.datePicker setHidden:YES animated:NO];
//    [self.view insertSubview:self.datePicker atIndex:[self.view subviews].count];
//}

//-(void)onDatePickerSelectionChanged:(id)sender
//{
//    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
//    NSDate *selectedDate = [self.datePicker.picker date];
//    NSString *date = [dateFormatter stringFromDate:selectedDate];
//    if (indexPath.section == 1)
//    {
//        if (indexPath.row == 1)
//        {
//            self.lb_fromDate.text = date;
//            NSDate *toDate = [dateFormatter dateFromString:self.lb_toDate.text];
//            if ([selectedDate compare:toDate] > 0)
//                self.lb_toDate.text = date;
//        }
//        if (indexPath.row == 2)
//            self.lb_toDate.text = date;
//    }
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleNone;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int section = indexPath.section;
    
    if (section == 2) {
        return 44;
    } else {
        return [super tableView:tableView heightForRowAtIndexPath:indexPath];
    }
}

- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    int section = indexPath.section;
    if (section == 2) {
        return 1;
    } else {
        return [super tableView:tableView indentationLevelForRowAtIndexPath:indexPath];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.arrayOfLeaveRequestDetail.count == 0)
        return 1;
    if (section == 2 ) {
        return self.arrayOfLeaveRequestDetail.count;
    } else {
        return [super tableView:tableView numberOfRowsInSection:section];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.arrayOfLeaveRequestDetail.count == 0)
        return 1;
    return [super numberOfSectionsInTableView:tableView];
}

//-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
//{
//    if (self.arrayOfLeaveRequestDetail == nil || self.arrayOfLeaveRequestDetail.count == 0)
//        return [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
//    int section = indexPath.section;
//    if (section != 2)
//        return [super tableView:tableView cellForRowAtIndexPath:indexPath];
//    
//    tns1_V_SelfLeaveRequestDetail *detail = self.arrayOfLeaveRequestDetail[indexPath.row];
//    DayLeaveDetailCell *cell = [self.tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
//    cell.lbLeft.text = [dateFormatter stringFromDate:detail.DayOff];
//    
//    if ([detail.IsFirstShiftRequest boolValue] && ![detail.IsSecondShiftRequest boolValue]) // AM
//        cell.lbRight.text = leaveStatusArray[1];
//    else if (![detail.IsFirstShiftRequest boolValue] && [detail.IsSecondShiftRequest boolValue])  // PM
//        cell.lbRight.text = leaveStatusArray[2];
//    else  // Full
//        cell.lbRight.text = leaveStatusArray[0];
//    
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    
//    return cell;
//}

#pragma mark - Table view delegate

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (self.editing)
//    {
//        if (indexPath.section == 1 && indexPath.row == 1)
//        {
//            if (self.datePicker == nil)
//            {
//                [self initPicker: YES];
//                [self.datePicker setHidden:NO animated:YES];
//            }
//        }
//        else if (indexPath.section == 1 && indexPath.row == 2)
//        {
//            if (self.datePicker == nil)
//            {
//                [self initPicker:NO];
//                [self.datePicker setHidden:NO animated:YES];
//            }
//        }
//        else if (indexPath.section == 1 && indexPath.row == 3)
//        {
//            PopoverContentViewController *content = [[PopoverContentViewController alloc] init];
//            content.data = absenceTypeStringList;
//            content.type  = 1;
//            
//            UIPopoverController *pop = [[UIPopoverController alloc] initWithContentViewController:content];
//            self.popover = pop;
//            
//            [self.popover presentPopoverFromRect:cell.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//            
//            content.parentViewController = self;
//        }
//        else if (indexPath.section == 1 && indexPath.row == 4)
//        {
//            PopoverContentViewController *content = [[PopoverContentViewController alloc] init];
//            content.type = 2;
//            content.data = approverStringList;
//            
//            self.popover = [[UIPopoverController alloc] initWithContentViewController:content];
//            
//            
//            [self.popover presentPopoverFromRect:cell.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//            content.parentViewController = self;
//        }
//        else if (indexPath.section == 2)
//        {
//            PopoverContentViewController *content = [[PopoverContentViewController alloc] init];
//            content.data = leaveStatuses;
//            self.popover = [[UIPopoverController alloc] initWithContentViewController:content];
//            [content addTarget:self forSelectionChanged:@selector(onSelectionChanged:)];
//            [self.popover presentPopoverFromRect:cell.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//            
//        }
//    }
//}

#pragma mark - Leave Request Service

-(void)approveLeaveRequest:(NSNumber *)leaveRequestId
{
    static NSString *okString = @"Đơn xin phép đã được phê duyệt";
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSDictionary *dict = @{@"leaveRequestId": leaveRequestId};
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dict options:kNilOptions error:nil];
    
    NSString *string = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
    NSLog(@"data: %@", string);
    
    NSString *url = BaseUrl;
    
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:url]];
    [client postPath:@"LeaveTimeRequest/ApprovalLeaveRequest" parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [hud hide:YES];
         NSData *returnData = (NSData*)responseObject;
         NSString *message = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
         message = [message stringByReplacingOccurrencesOfString:@"\"" withString:@""];
         
         
         if ([message rangeOfString:okString].location != NSNotFound)
             [self.navigationController popViewControllerAnimated:YES];
         else
         {
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thông báo" message: @"Có lỗi trong quá trình kết nối" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
             [alert show];
         }
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [hud hide:YES];
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Có lỗi trong quá trình kết nối" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
         [alert show];
     }];
}

-(void)denyLeaveRequest:(NSNumber *)leaveRequestId
{
    static NSString* okString = @"Đã từ chối đơn xin nghỉ phép này";
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSDictionary *dict = @{@"leaveRequestId": leaveRequestId};
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dict options:kNilOptions error:nil];
    
    NSString *string = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
    NSLog(@"data: %@", string);
    
    NSString *url = BaseUrl;
    
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:url]];
    [client postPath:@"LeaveTimeRequest/RejectLeaveRequest" parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [hud hide:YES];
         NSData *returnData = (NSData*)responseObject;
         NSString *message = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
         
         if ([message rangeOfString:okString].location != NSNotFound)
             [self.navigationController popViewControllerAnimated:YES];
         else
         {
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Có lỗi trong quá trình thực hiện" delegate:nil cancelButtonTitle:@"Thử lại" otherButtonTitles: nil];
             [alert show];
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [hud hide:YES];
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Có lỗi trong quá trình kết nối" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
         [alert show];
     }];
}

//-(void)operation:(BasicHttpBinding_ILeaveRequestServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_ILeaveRequestServiceBindingResponse *)response
//{
//    if (response.bodyParts.count == 0)
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi kết nối" message:@"Không thể kết nối đến server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [alert show];
//        return;
//    }
//    
//    for(id mine in response.bodyParts)
//    {
//        if ([mine isKindOfClass:[LeaveRequestServiceSvc_GetListLeaveRequestDetailViewResponse class]])
//        {
//            tns1_ArrayOfV_SelfLeaveRequestDetail *result = [mine GetListLeaveRequestDetailViewResult];
//            self.arrayOfLeaveRequestDetail = result.V_SelfLeaveRequestDetail;
//        }
//        
//        if ([mine isKindOfClass:[SOAPFault class]])
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi kết nối" message:@"Không thể kết nối đến server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//            [alert show];
//        }
//    }
//    
//    [hud hide:YES];
//}

@end
