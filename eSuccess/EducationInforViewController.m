//
//  EducationInforViewController.m
//  eSuccess
//
//  Created by admin on 5/10/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "EducationInforViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "DayLeaveDetailCell.h"
@interface EducationInforViewController ()

@end

@implementation EducationInforViewController
@synthesize educationArray;
@synthesize univeristyArray;
static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Quá trình học tập- đào tạo";
    [self getData];
    
    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 130;
//}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([univeristyArray count] > 0) {
        return [univeristyArray objectAtIndex:section];
    }
    return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [univeristyArray count] > 0 ? univeristyArray.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //return [profileArray count];
    return univeristyArray.count > 0 ? univeristyArray.count : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (univeristyArray.count > 0)
    {
        DayLeaveDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
        if (cell == nil) {
            cell = [[DayLeaveDetailCell alloc]init];
        }
        //AppDelegate *app = [UIApplication sharedApplication].delegate;
        //UIView *bgView = [[UIView alloc] initWithFrame:cell.frame];indexPath.row % 2 == 0 ? [bgView setBackgroundColor:[app colorFromRGB:209 g:209 b:209 a:1]] : [bgView setBackgroundColor:[app colorFromRGB:196 g:196 b:196 a:1]];
        //[cell setBackgroundView:bgView];
        cell.lbLeft.font = [UIFont systemFontOfSize:14];
        cell.lbLeft.backgroundColor = [UIColor clearColor];
        cell.lbRight.font = [UIFont boldSystemFontOfSize:16];
        cell.lbRight.backgroundColor = [UIColor clearColor];
        int section = indexPath.section;
        tns2_EmpProfileEducation *education = educationArray[section];
        if (indexPath.row == 0) {
            cell.lbLeft.text = @"Chuyên ngành";
            cell.lbRight.text = education.Major;
        }
        if (indexPath.row == 1) {
            cell.lbLeft.text = @"Ngày học";
            cell.lbRight.text = [NSString stringWithFormat:@"%d",[education.StartYear integerValue]];
        }
        if (indexPath.row == 2) {
            cell.lbLeft.text = @"Tốt nghiệp";
            cell.lbRight.text = [NSString stringWithFormat:@"%d",[education.EndYear integerValue]];
        }
        if (indexPath.row == 3) {
            cell.lbLeft.text = @"Ghi chú";
            cell.lbRight.text = education.Note;
        }
        return cell;
    }
    
    return [self getDefaultEmptyCell];
}

#pragma get Data Univeristy
- (void)getData
{
}



@end
