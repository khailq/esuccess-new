//
//  HealthInsuranceViewController.h
//  eSuccess
//
//  Created by HPTVIETNAM on 6/21/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"
#import "EmpProfileLayerServiceSvc.h"
@interface HealthInsuranceViewController : BaseDetailTableViewController<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>
@property(nonatomic, strong) NSMutableArray *healthInsArr;
@property(nonatomic, strong) NSMutableArray *healthInsNoArr;
@end
