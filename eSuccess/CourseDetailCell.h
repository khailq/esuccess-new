//
//  CourseDetailCell.h
//  eSuccess
//
//  Created by HPTVIETNAM on 6/7/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADVPercentProgressBar.h"
#import "Course.h"

@interface CourseDetailCell : UITableViewCell

@property (strong, nonatomic) IBOutlet ADVPercentProgressBar *progressBar;

@property (weak, nonatomic) IBOutlet UILabel *lb_courseName;
@property (weak, nonatomic) IBOutlet UILabel *lb_courseType;
@property (weak, nonatomic) IBOutlet UILabel *lb_courseTopic;
@property (weak, nonatomic) IBOutlet UILabel *lb_courseStatus;
@property (weak, nonatomic) IBOutlet UITextView *tv_courseDescription;
@property (weak, nonatomic) IBOutlet UIView *v_progressBarContainer;

@property (weak, nonatomic) Course *course;

@end
