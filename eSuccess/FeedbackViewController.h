//
//  FeedbackViewController.h
//  eSuccess
//
//  Created by admin on 5/31/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"

@interface FeedbackViewController : BaseDetailTableViewController
@property(nonatomic, retain) NSMutableArray *feedbackArr;
@property(nonatomic, retain) NSMutableArray *timeArr;
@end
