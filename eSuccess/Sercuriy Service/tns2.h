#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class tns2_MsgNotifyModel;
@interface tns2_MsgNotifyModel : NSObject {
	
/* elements */
	NSNumber * Id_;
	USBoolean * IsSuccess;
	USBoolean * IsValid;
	NSString * NotifyMsg;
	NSString * NotifyMsgEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns2_MsgNotifyModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Id_;
@property (retain) USBoolean * IsSuccess;
@property (retain) USBoolean * IsValid;
@property (retain) NSString * NotifyMsg;
@property (retain) NSString * NotifyMsgEN;
/* attributes */
- (NSDictionary *)attributes;
@end
