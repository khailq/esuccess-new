#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class tns1_ArrayOfV_OrgUnit;
@class tns1_V_OrgUnit;
@class tns1_SysMembership;
@class tns1_SysUser;
@class tns1_ArrayOfSysUserEmpProfilePermission;
@class tns1_ArrayOfSysUserMenuPermission;
@class tns1_SysUserEmpProfilePermission;
@class tns1_SysEmpProfileLayer;
@class tns1_SysEmpProfileGroupLayer;
@class tns1_ArrayOfSysEmpProfileLayerUrl;
@class tns1_ArrayOfSysGroupEmpProfilePermission;
@class tns1_ArrayOfSysRoleEmpProfilePermission;
@class tns1_ArrayOfSysEmpProfileLayer;
@class tns1_SysEmpProfileLayerUrl;
@class tns1_SysGroupEmpProfilePermission;
@class tns1_SysGroup;
@class tns1_ArrayOfSysGroupMenuPermission;
@class tns1_SysGroupMenuPermission;
@class tns1_SysMenu;
@class tns1_ArrayOfSysRoleMenuPermission;
@class tns1_SysRoleMenuPermission;
@class tns1_SysRole;
@class tns1_SysRoleEmpProfilePermission;
@class tns1_SysUserMenuPermission;
@class tns1_V_SysUserMembership;
@class tns1_ArrayOfSysUser;
@class tns1_ArrayOfV_SysUserInRole;
@class tns1_V_SysUserInRole;
@class tns1_ArrayOfV_SysUserMembership;
@class tns1_ArrayOfV_SysUserInGroup;
@class tns1_V_SysUserInGroup;
@class tns1_ArrayOfV_SysGroupInRole;
@class tns1_V_SysGroupInRole;
@class tns1_ArrayOfSysGroup;
@class tns1_ArrayOfV_SysRoleInGroup;
@class tns1_V_SysRoleInGroup;
@class tns1_ArrayOfSysRole;
@class tns1_Employee;
@class tns1_ArrayOfCBAccidentInsurance;
@class tns1_ArrayOfCBCompensationEmployeeGroupDetail;
@class tns1_ArrayOfCBConvalescence;
@class tns1_ArrayOfCBDayOffSocialInsurance;
@class tns1_ArrayOfCBFactorEmployeeMetaData;
@class tns1_ArrayOfCBHealthInsuranceDetail;
@class tns1_CLogCity;
@class tns1_CLogEmployeeType;
@class tns1_ArrayOfCLogTrainer;
@class tns1_ArrayOfEmpBasicProfile;
@class tns1_ArrayOfEmpCompetency;
@class tns1_ArrayOfEmpCompetencyRating;
@class tns1_ArrayOfEmpContract;
@class tns1_ArrayOfEmpPerformanceAppraisal;
@class tns1_ArrayOfEmpProfileAllowance;
@class tns1_ArrayOfEmpProfileBaseSalary;
@class tns1_ArrayOfEmpProfileBenefit;
@class tns1_ArrayOfEmpProfileComment;
@class tns1_ArrayOfEmpProfileComputingSkill;
@class tns1_ArrayOfEmpProfileContact;
@class tns1_ArrayOfEmpProfileDegree;
@class tns1_ArrayOfEmpProfileDiscipline;
@class tns1_ArrayOfEmpProfileEducation;
@class tns1_ArrayOfEmpProfileEquipment;
@class tns1_ArrayOfEmpProfileExperience;
@class tns1_ArrayOfEmpProfileFamilyRelationship;
@class tns1_ArrayOfEmpProfileForeignLanguage;
@class tns1_ArrayOfEmpProfileHealthInsurance;
@class tns1_ArrayOfEmpProfileHealthy;
@class tns1_ArrayOfEmpProfileHobby;
@class tns1_ArrayOfEmpProfileInternalCourse;
@class tns1_ArrayOfEmpProfileJobPosition;
@class tns1_ArrayOfEmpProfileLeaveRegime;
@class tns1_ArrayOfEmpProfileOtherBenefit;
@class tns1_ArrayOfEmpProfileParticipation;
@class tns1_ArrayOfEmpProfilePersonality;
@class tns1_ArrayOfEmpProfileProcessOfWork;
@class tns1_ArrayOfEmpProfileQualification;
@class tns1_ArrayOfEmpProfileReward;
@class tns1_ArrayOfEmpProfileSkill;
@class tns1_ArrayOfEmpProfileTotalIncome;
@class tns1_ArrayOfEmpProfileTraining;
@class tns1_ArrayOfEmpProfileWageType;
@class tns1_ArrayOfEmpProfileWorkingExperience;
@class tns1_ArrayOfEmpProfileWorkingForm;
@class tns1_ArrayOfEmpSocialInsuranceSalary;
@class tns1_ArrayOfGPAdditionAppraisal;
@class tns1_ArrayOfGPCompanyScoreCard;
@class tns1_ArrayOfGPCompanyStrategicGoal;
@class tns1_ArrayOfGPCompanyYearlyObjective;
@class tns1_ArrayOfGPEmployeeScoreCard;
@class tns1_ArrayOfGPIndividualYearlyObjective;
@class tns1_ArrayOfGPObjectiveInitiative;
@class tns1_ArrayOfGPPerformanceYearEndResult;
@class tns1_ArrayOfLMSCourseAttendee;
@class tns1_ArrayOfLMSCourseTranscript;
@class tns1_ArrayOfNCClientConnection;
@class tns1_ArrayOfNCMessage;
@class tns1_ArrayOfProjectMember;
@class tns1_ArrayOfProject;
@class tns1_ArrayOfRecInterviewSchedule;
@class tns1_ArrayOfRecInterviewer;
@class tns1_ArrayOfRecPhaseEmpDisplaced;
@class tns1_ArrayOfRecRecruitmentRequirement;
@class tns1_ArrayOfRecRequirementEmpDisplaced;
@class tns1_ReportEmployeeRequestedBook;
@class tns1_ArrayOfSelfLeaveRequest;
@class tns1_ArrayOfSysRecPlanApprover;
@class tns1_ArrayOfSysRecRequirementApprover;
@class tns1_ArrayOfSysTrainingApprover;
@class tns1_ArrayOfTask;
@class tns1_ArrayOfTrainingCourseEmployee;
@class tns1_ArrayOfTrainingEmpPlan;
@class tns1_ArrayOfTrainingPlanEmployee;
@class tns1_CBAccidentInsurance;
@class tns1_CBCompensationEmployeeGroupDetail;
@class tns1_CBCompensationEmployeeGroup;
@class tns1_CBFactorEmployeeMetaData;
@class tns1_CLogCBFactor;
@class tns1_ArrayOfCBCompensationTypeFactor;
@class tns1_CLogCBCompensationCategory;
@class tns1_CBCompensationTypeFactor;
@class tns1_ArrayOfCLogCBFactor;
@class tns1_CBConvalescence;
@class tns1_CBDayOffSocialInsurance;
@class tns1_CLogCBDisease;
@class tns1_CLogHospital;
@class tns1_ArrayOfCBHealthInsurance;
@class tns1_CBHealthInsurance;
@class tns1_CLogCountry;
@class tns1_ArrayOfCLogDistrict;
@class tns1_ArrayOfCLogHospital;
@class tns1_ArrayOfEmployee;
@class tns1_ArrayOfCLogCity;
@class tns1_CLogDistrict;
@class tns1_CBHealthInsuranceDetail;
@class tns1_CLogTrainer;
@class tns1_Address;
@class tns1_ArrayOfTrainingCourse;
@class tns1_ArrayOfOrgUnit;
@class tns1_EmpContract;
@class tns1_Employer;
@class tns1_OrgJob;
@class tns1_OrgJobPosition;
@class tns1_ArrayOfOrgJobPosition;
@class tns1_ArrayOfOrgJobSpecificCompetency;
@class tns1_ArrayOfOrgUnitJob;
@class tns1_ArrayOfOrgJobPositionRequiredProficency;
@class tns1_ArrayOfOrgJobPositionSpecificCompetency;
@class tns1_ArrayOfRecPlanJobPosition;
@class tns1_ArrayOfRecProcessApplyForJobPosition;
@class tns1_ArrayOfRecRecruitmentPhaseJobPosition;
@class tns1_ArrayOfRecRequirementJobPosition;
@class tns1_EmpProfileJobPosition;
@class tns1_OrgJobPositionRequiredProficency;
@class tns1_OrgJobPositionSpecificCompetency;
@class tns1_OrgProficencyLevel;
@class tns1_CLogRating;
@class tns1_ArrayOfEmpProficencyLevelRating;
@class tns1_OrgCompetency;
@class tns1_ArrayOfOrgProficencyLevelDetail;
@class tns1_ArrayOfRecCandidateProficencyLevelRating;
@class tns1_ArrayOfEmpProficencyDetailRating;
@class tns1_ArrayOfEmpProficencyLevelDetailRating;
@class tns1_ArrayOfLMSCourseCompetency;
@class tns1_ArrayOfLMSCourseRequiredCompetency;
@class tns1_ArrayOfOrgCoreCompetency;
@class tns1_ArrayOfOrgProficencyDetail;
@class tns1_ArrayOfOrgProficencyLevel;
@class tns1_ArrayOfRecCanProficencyDetailRating;
@class tns1_ArrayOfRecCanProficencyLevelDetailRating;
@class tns1_ArrayOfRecCandidateCompetencyRating;
@class tns1_ArrayOfRecCandidateExperience;
@class tns1_ArrayOfRecCandidateQualification;
@class tns1_ArrayOfRecCandidateSkill;
@class tns1_ArrayOfTrainingEmpProficency;
@class tns1_ArrayOfTrainingProficencyExpected;
@class tns1_ArrayOfTrainingProficencyRequire;
@class tns1_EmpCompetency;
@class tns1_OrgCoreCompetency;
@class tns1_EmpProficencyDetailRating;
@class tns1_EmpProficencyLevelDetailRating;
@class tns1_OrgProficencyDetail;
@class tns1_EmpProficencyLevelRating;
@class tns1_OrgProficencyLevelDetail;
@class tns1_EmpCompetencyRating;
@class tns1_OrgProficencyType;
@class tns1_RecCanProficencyDetailRating;
@class tns1_RecCanProficencyLevelDetailRating;
@class tns1_RecCandidateProficencyLevelRating;
@class tns1_RecCandidateCompetencyRating;
@class tns1_RecCandidate;
@class tns1_ArrayOfRecCandidateApplication;
@class tns1_ArrayOfRecCandidateDegree;
@class tns1_ArrayOfRecCandidateFamilyRelationship;
@class tns1_ArrayOfRecCandidateForeignLanguage;
@class tns1_RecCandidateStatu;
@class tns1_RecCandidateSupplier;
@class tns1_ArrayOfRecProcessPhaseResult;
@class tns1_RecCandidateApplication;
@class tns1_RecCandidateProfileStatu;
@class tns1_RecRecruitmentPhaseJobPosition;
@class tns1_RecInterviewSchedule;
@class tns1_RecGroupInterviewer;
@class tns1_RecInterviewPhase;
@class tns1_RecInterviewScheduleStatu;
@class tns1_ArrayOfRecScheduleInterviewerResult;
@class tns1_RecInterviewer;
@class tns1_RecScheduleInterviewerResult;
@class tns1_RecInterviewPhaseEvaluation;
@class tns1_RecEvaluationCriterion;
@class tns1_RecGroupEvaluationCriterion;
@class tns1_ArrayOfRecInterviewPhaseEvaluation;
@class tns1_ArrayOfRecEvaluationCriterion;
@class tns1_RecRecruitmentProcessPhase;
@class tns1_ArrayOfRecInterviewPhase;
@class tns1_RecRecruitmentProcess;
@class tns1_RecProcessPhaseResult;
@class tns1_ArrayOfRecRecruitmentProcessPhase;
@class tns1_RecProcessApplyForJobPosition;
@class tns1_RecRecruitmentPhase;
@class tns1_RecPhaseStatu;
@class tns1_RecRecruitmentRequirement;
@class tns1_RecPhaseEmpDisplaced;
@class tns1_ArrayOfRecRecruitmentPhase;
@class tns1_RecRecruitmentPlan;
@class tns1_ArrayOfRecRequirementApproveHistory;
@class tns1_RecRequirementStatu;
@class tns1_ArrayOfRecPlanApproveHistory;
@class tns1_RecPlanStatu;
@class tns1_RecPlanApproveHistory;
@class tns1_SysRecPlanApprover;
@class tns1_RecPlanJobPosition;
@class tns1_ArrayOfRecRecruitmentPlan;
@class tns1_RecRequirementApproveHistory;
@class tns1_SysRecRequirementApprover;
@class tns1_RecRequirementEmpDisplaced;
@class tns1_RecRequirementJobPosition;
@class tns1_RecCandidateDegree;
@class tns1_CLogDegree;
@class tns1_RecCandidateExperience;
@class tns1_OrgExperience;
@class tns1_OrgProjectType;
@class tns1_OrgTimeInCharge;
@class tns1_ArrayOfOrgExperience;
@class tns1_RecCandidateFamilyRelationship;
@class tns1_CLogFamilyRelationship;
@class tns1_EmpProfileFamilyRelationship;
@class tns1_ArrayOfCBPITDependent;
@class tns1_ArrayOfEmpBeneficiary;
@class tns1_CBPITDependent;
@class tns1_EmpBeneficiary;
@class tns1_RecCandidateForeignLanguage;
@class tns1_RecCandidateQualification;
@class tns1_OrgQualification;
@class tns1_EmpProfileQualification;
@class tns1_RecCandidateSkill;
@class tns1_OrgSkill;
@class tns1_OrgSkillType;
@class tns1_EmpProfileSkill;
@class tns1_ArrayOfOrgSkill;
@class tns1_TrainingEmpProficency;
@class tns1_TrainingCourseEmployee;
@class tns1_TrainingCourseSchedule;
@class tns1_CLogCourseSchedule;
@class tns1_TrainingCourse;
@class tns1_ArrayOfTrainingCourseSchedule;
@class tns1_CLogTrainingCenter;
@class tns1_TrainingCategory;
@class tns1_ArrayOfTrainingCourseChapter;
@class tns1_TrainingCoursePeriod;
@class tns1_TrainingCourseType;
@class tns1_ArrayOfTrainingCourseUnit;
@class tns1_ArrayOfTrainingPlanDegree;
@class tns1_TrainingPlanRequest;
@class tns1_TrainingCourseChapter;
@class tns1_TrainingCourseUnit;
@class tns1_TrainingPlanDegree;
@class tns1_CLogMajor;
@class tns1_ArrayOfTrainingPlanRequest;
@class tns1_EmpProfileEducation;
@class tns1_TrainingPlanEmployee;
@class tns1_TrainingProficencyExpected;
@class tns1_TrainingProficencyRequire;
@class tns1_ArrayOfRecCandidate;
@class tns1_RecCandidateTypeSupplier;
@class tns1_ArrayOfRecCandidateSupplier;
@class tns1_ArrayOfTrainingPlanProfiency;
@class tns1_TrainingPlanProfiency;
@class tns1_EmpProfileWorkingExperience;
@class tns1_LMSCourseAttendee;
@class tns1_LMSCourse;
@class tns1_CLogCourseStatu;
@class tns1_GPCompanyYearlyObjective;
@class tns1_GPObjectiveInitiative;
@class tns1_LMSContentTopic;
@class tns1_ArrayOfLMSCourseContent;
@class tns1_ArrayOfLMSCourseDetail;
@class tns1_ArrayOfLMSCourseProgressUnit;
@class tns1_LMSCourseType;
@class tns1_ArrayOfLMSCourse;
@class tns1_GPCompanyStrategicGoal;
@class tns1_GPPerspective;
@class tns1_GPStrategy;
@class tns1_GPCompanyScoreCard;
@class tns1_GPPerspectiveMeasure;
@class tns1_GPEmployeeScoreCard;
@class tns1_ArrayOfGPEmployeeScoreCardAppraisal;
@class tns1_ArrayOfGPEmployeeScoreCardProgress;
@class tns1_GPPerformanceExecutionReport;
@class tns1_GPEmployeeScoreCardAppraisal;
@class tns1_GPEmployeeScoreCardProgress;
@class tns1_GPIndividualYearlyObjective;
@class tns1_ArrayOfGPExecutionPlanDetail;
@class tns1_ArrayOfGPIndividualYearlyObjectiveAppraisal;
@class tns1_GPExecutionPlanDetail;
@class tns1_GPExecutionPlan;
@class tns1_ArrayOfGPExecutionPlanActivity;
@class tns1_ArrayOfGPEmployeeWholePlanAppraisal;
@class tns1_GPAdditionAppraisal;
@class tns1_GPEmployeeWholePlanAppraisal;
@class tns1_GPPerformanceYearEndResult;
@class tns1_GPExecutionPlanActivity;
@class tns1_ArrayOfGPExecutionPlanActivityAppraisal;
@class tns1_ArrayOfGPPeriodicReport;
@class tns1_GPExecutionPlanActivityAppraisal;
@class tns1_GPPeriodicReport;
@class tns1_GPIndividualYearlyObjectiveAppraisal;
@class tns1_ArrayOfGPPerspectiveMeasure;
@class tns1_ArrayOfGPPerspectiveValue;
@class tns1_ArrayOfGPCompanyStrategicGoalDetail;
@class tns1_GPStrategyMapObject;
@class tns1_GPCompanyStrategicGoalDetail;
@class tns1_GPPerspectiveValue;
@class tns1_ArrayOfGPPerspectiveValueDetail;
@class tns1_GPPerspectiveValueDetail;
@class tns1_ArrayOfGPStrategyMapObject;
@class tns1_ArrayOfLMSTopicCompetency;
@class tns1_LMSTopicGroup;
@class tns1_LMSTopicCompetency;
@class tns1_OrgCompetencyGroup;
@class tns1_LMSCourseCompetency;
@class tns1_LMSCourseRequiredCompetency;
@class tns1_ArrayOfOrgCompetency;
@class tns1_OrgJobSpecificCompetency;
@class tns1_ArrayOfLMSContentTopic;
@class tns1_LMSCourseContent;
@class tns1_CLogLMSContentType;
@class tns1_LMSCourseDetail;
@class tns1_LMSCourseProgressUnit;
@class tns1_OrgUnit;
@class tns1_LMSCourseTranscript;
@class tns1_OrgUnitJob;
@class tns1_EmpBasicProfile;
@class tns1_CLogEmpWorkingStatu;
@class tns1_EmpPerformanceAppraisal;
@class tns1_EmpProfileAllowance;
@class tns1_EmpProfileBaseSalary;
@class tns1_EmpProfileBenefit;
@class tns1_EmpProfileComment;
@class tns1_EmpProfileComputingSkill;
@class tns1_EmpProfileContact;
@class tns1_EmpProfileDegree;
@class tns1_OrgDegree;
@class tns1_EmpProfileDiscipline;
@class tns1_OrgDisciplineMaster;
@class tns1_ArrayOfOrgDisciplineForUnit;
@class tns1_OrgDisciplineForUnit;
@class tns1_EmpProfileEquipment;
@class tns1_CLogEquipment;
@class tns1_EmpProfileExperience;
@class tns1_EmpProfileForeignLanguage;
@class tns1_CLogLanguage;
@class tns1_EmpProfileHealthInsurance;
@class tns1_EmpProfileHealthy;
@class tns1_EmpProfileHobby;
@class tns1_CLogHobby;
@class tns1_EmpProfileInternalCourse;
@class tns1_EmpProfileLeaveRegime;
@class tns1_EmpProfileOtherBenefit;
@class tns1_EmpOtherBenefit;
@class tns1_EmpProfileParticipation;
@class tns1_EmpProfilePersonality;
@class tns1_CLogPersonality;
@class tns1_EmpProfileProcessOfWork;
@class tns1_EmpProfileReward;
@class tns1_OrgRewardMaster;
@class tns1_ArrayOfOrgRewardForUnit;
@class tns1_OrgRewardForUnit;
@class tns1_EmpProfileTotalIncome;
@class tns1_EmpProfileTraining;
@class tns1_EmpProfileWageType;
@class tns1_EmpProfileWorkingForm;
@class tns1_EmpSocialInsuranceSalary;
@class tns1_NCClientConnection;
@class tns1_NCMessage;
@class tns1_NCMessageType;
@class tns1_ProjectMember;
@class tns1_Project;
@class tns1_SelfLeaveRequest;
@class tns1_SysTrainingApprover;
@class tns1_ArrayOfTrainingEmpPlanApproved;
@class tns1_TrainingEmpPlanApproved;
@class tns1_TrainingEmpPlan;
@class tns1_Task;
@class tns1_ArrayOfV_SysUserEmployee;
@class tns1_V_SysUserEmployee;
@class tns1_ArrayOfCompany;
@class tns1_Company;
@class tns1_ArrayOfSysApplication;
@class tns1_SysApplication;
@class tns1_ArrayOfSysGroupMenu;
@class tns1_SysGroupMenu;
@class tns1_ArrayOfSysModule;
@class tns1_SysModule;
@class tns1_ArrayOfSysFeature;
@class tns1_SysFeature;
@class tns1_ArrayOfSysAction;
@class tns1_SysAction;
@class tns1_V_SysAction;
@class tns1_ArrayOfV_SysAction;
@class tns1_ArrayOfSysLink;
@class tns1_SysLink;
@class tns1_ArrayOfSysActionType;
@class tns1_SysActionType;
@class tns1_SysUserLog;
@class tns1_ArrayOfSysUserPermission;
@class tns1_SysUserPermission;
@class tns1_ArrayOfSysRolePermission;
@class tns1_SysRolePermission;
@class tns1_ArrayOfSysGroupPermission;
@class tns1_SysGroupPermission;
@class tns1_ArrayOfSysUserInRole;
@class tns1_SysUserInRole;
@class tns1_ArrayOfSysGroupInRole;
@class tns1_SysGroupInRole;
@class tns1_ArrayOfSysUserInGroup;
@class tns1_SysUserInGroup;
@class tns1_ArrayOfSysUserOrgUnitPermission;
@class tns1_SysUserOrgUnitPermission;
@class tns1_ArrayOfSysRoleOrgUnitPermission;
@class tns1_SysRoleOrgUnitPermission;
@class tns1_ArrayOfSysGroupOrgUnitPermission;
@class tns1_SysGroupOrgUnitPermission;
@class tns1_ArrayOfSP_ORG_UNIT_TREE_BY_USER_Result;
@class tns1_SP_ORG_UNIT_TREE_BY_USER_Result;
@class tns1_ArrayOfSysEmpProfileGroupLayer;
@class tns1_ArrayOfSP_ESS_ALL_PERMIS_ON_PROFILE_LAYER_Result;
@class tns1_SP_ESS_ALL_PERMIS_ON_PROFILE_LAYER_Result;
@class tns1_ArrayOfSP_GET_PROF_LAYER_ALL_PERMIS_Result;
@class tns1_SP_GET_PROF_LAYER_ALL_PERMIS_Result;
@class tns1_ArrayOfSP_ESS_PRO_LAYER_VIEW_PER_TREE_Result;
@class tns1_SP_ESS_PRO_LAYER_VIEW_PER_TREE_Result;
#import "tns4.h"
@interface tns1_V_OrgUnit : NSObject {
	
/* elements */
	NSString * Address;
	NSNumber * CompanyId;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	USBoolean * IsLeaderActive;
	NSString * Name;
	NSNumber * OrgJobTitleId;
	NSString * OrgJobTitleLevelCode;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgUnitLeaderCode;
	NSString * OrgUnitLeaderFirstName;
	NSString * OrgUnitLeaderFullName;
	NSNumber * OrgUnitLeaderId;
	NSString * OrgUnitLeaderImageURL;
	NSString * OrgUnitLeaderJobPositionName;
	NSString * OrgUnitLeaderLastName;
	NSString * ParentCode;
	NSNumber * ParentId;
	NSNumber * TotalEmployees;
	NSString * WorkLocation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_V_OrgUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsLeaderActive;
@property (retain) NSString * Name;
@property (retain) NSNumber * OrgJobTitleId;
@property (retain) NSString * OrgJobTitleLevelCode;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitLeaderCode;
@property (retain) NSString * OrgUnitLeaderFirstName;
@property (retain) NSString * OrgUnitLeaderFullName;
@property (retain) NSNumber * OrgUnitLeaderId;
@property (retain) NSString * OrgUnitLeaderImageURL;
@property (retain) NSString * OrgUnitLeaderJobPositionName;
@property (retain) NSString * OrgUnitLeaderLastName;
@property (retain) NSString * ParentCode;
@property (retain) NSNumber * ParentId;
@property (retain) NSNumber * TotalEmployees;
@property (retain) NSString * WorkLocation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfV_OrgUnit : NSObject {
	
/* elements */
	NSMutableArray *V_OrgUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfV_OrgUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgUnit:(tns1_V_OrgUnit *)toAdd;
@property (readonly) NSMutableArray * V_OrgUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysMembership : NSObject {
	
/* elements */
	NSDate * LastLogin;
	NSString * Password;
	NSString * PasswordSalt;
	NSNumber * SysMembershipId;
	NSNumber * SysUserId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysMembership *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * LastLogin;
@property (retain) NSString * Password;
@property (retain) NSString * PasswordSalt;
@property (retain) NSNumber * SysMembershipId;
@property (retain) NSNumber * SysUserId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysEmpProfileLayer : NSObject {
	
/* elements */
	NSMutableArray *SysEmpProfileLayer;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysEmpProfileLayer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysEmpProfileLayer:(tns1_SysEmpProfileLayer *)toAdd;
@property (readonly) NSMutableArray * SysEmpProfileLayer;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysEmpProfileGroupLayer : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * Icon;
	USBoolean * IsDeleted;
	USBoolean * IsVisible;
	NSNumber * Priority;
	NSString * SysEmpProfileGroupLayerCode;
	NSNumber * SysEmpProfileGroupLayerId;
	NSString * SysEmpProfileGroupLayerName;
	tns1_ArrayOfSysEmpProfileLayer * SysEmpProfileLayers;
	NSString * Url;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysEmpProfileGroupLayer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Icon;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsVisible;
@property (retain) NSNumber * Priority;
@property (retain) NSString * SysEmpProfileGroupLayerCode;
@property (retain) NSNumber * SysEmpProfileGroupLayerId;
@property (retain) NSString * SysEmpProfileGroupLayerName;
@property (retain) tns1_ArrayOfSysEmpProfileLayer * SysEmpProfileLayers;
@property (retain) NSString * Url;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysEmpProfileLayerUrl : NSObject {
	
/* elements */
	NSString * Action;
	NSNumber * CompanyId;
	NSString * Controller;
	USBoolean * IsDeleted;
	tns1_SysEmpProfileLayer * SysEmpProfileLayer;
	NSNumber * SysEmpProfileLayerId;
	NSNumber * SysEmpProfileLayerUrlId;
	NSString * Url;
	NSString * UrlType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysEmpProfileLayerUrl *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Action;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Controller;
@property (retain) USBoolean * IsDeleted;
@property (retain) tns1_SysEmpProfileLayer * SysEmpProfileLayer;
@property (retain) NSNumber * SysEmpProfileLayerId;
@property (retain) NSNumber * SysEmpProfileLayerUrlId;
@property (retain) NSString * Url;
@property (retain) NSString * UrlType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysEmpProfileLayerUrl : NSObject {
	
/* elements */
	NSMutableArray *SysEmpProfileLayerUrl;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysEmpProfileLayerUrl *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysEmpProfileLayerUrl:(tns1_SysEmpProfileLayerUrl *)toAdd;
@property (readonly) NSMutableArray * SysEmpProfileLayerUrl;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysRoleEmpProfilePermission : NSObject {
	
/* elements */
	USBoolean * AddPermission;
	USBoolean * AprovePermisson;
	NSNumber * AssignedBy;
	NSDate * CreatedDate;
	USBoolean * DeletePermission;
	USBoolean * EditPermission;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_SysEmpProfileLayer * SysEmpProfileLayer;
	NSNumber * SysEmpProfileLayerId;
	tns1_SysRole * SysRole;
	NSNumber * SysRoleEmpProfilePermissionId;
	NSNumber * SysRoleId;
	USBoolean * ViewPermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysRoleEmpProfilePermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddPermission;
@property (retain) USBoolean * AprovePermisson;
@property (retain) NSNumber * AssignedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * DeletePermission;
@property (retain) USBoolean * EditPermission;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_SysEmpProfileLayer * SysEmpProfileLayer;
@property (retain) NSNumber * SysEmpProfileLayerId;
@property (retain) tns1_SysRole * SysRole;
@property (retain) NSNumber * SysRoleEmpProfilePermissionId;
@property (retain) NSNumber * SysRoleId;
@property (retain) USBoolean * ViewPermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysRoleEmpProfilePermission : NSObject {
	
/* elements */
	NSMutableArray *SysRoleEmpProfilePermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysRoleEmpProfilePermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysRoleEmpProfilePermission:(tns1_SysRoleEmpProfilePermission *)toAdd;
@property (readonly) NSMutableArray * SysRoleEmpProfilePermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysRole : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * Description;
	NSString * Name;
	tns1_ArrayOfSysRoleEmpProfilePermission * SysRoleEmpProfilePermissions;
	NSNumber * SysRoleId;
	tns1_ArrayOfSysRoleMenuPermission * SysRoleMenuPermissions;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Description;
@property (retain) NSString * Name;
@property (retain) tns1_ArrayOfSysRoleEmpProfilePermission * SysRoleEmpProfilePermissions;
@property (retain) NSNumber * SysRoleId;
@property (retain) tns1_ArrayOfSysRoleMenuPermission * SysRoleMenuPermissions;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysRoleMenuPermission : NSObject {
	
/* elements */
	NSNumber * AssignedBy;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_SysMenu * SysMenu;
	NSNumber * SysMenuId;
	tns1_SysRole * SysRole;
	NSNumber * SysRoleId;
	NSNumber * SysRoleMenuPermissionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysRoleMenuPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AssignedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_SysMenu * SysMenu;
@property (retain) NSNumber * SysMenuId;
@property (retain) tns1_SysRole * SysRole;
@property (retain) NSNumber * SysRoleId;
@property (retain) NSNumber * SysRoleMenuPermissionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysRoleMenuPermission : NSObject {
	
/* elements */
	NSMutableArray *SysRoleMenuPermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysRoleMenuPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysRoleMenuPermission:(tns1_SysRoleMenuPermission *)toAdd;
@property (readonly) NSMutableArray * SysRoleMenuPermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysUserMenuPermission : NSObject {
	
/* elements */
	NSNumber * AssignedBy;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_SysMenu * SysMenu;
	NSNumber * SysMenuId;
	tns1_SysUser * SysUser;
	NSNumber * SysUserId;
	NSNumber * SysUserMenuPermissionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysUserMenuPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AssignedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_SysMenu * SysMenu;
@property (retain) NSNumber * SysMenuId;
@property (retain) tns1_SysUser * SysUser;
@property (retain) NSNumber * SysUserId;
@property (retain) NSNumber * SysUserMenuPermissionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysUserMenuPermission : NSObject {
	
/* elements */
	NSMutableArray *SysUserMenuPermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysUserMenuPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysUserMenuPermission:(tns1_SysUserMenuPermission *)toAdd;
@property (readonly) NSMutableArray * SysUserMenuPermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysMenu : NSObject {
	
/* elements */
	NSString * Action;
	NSString * ApplicationCodeAction;
	NSString * Controller;
	USBoolean * IsDefault;
	USBoolean * IsDeleted;
	NSNumber * Priority;
	NSString * ProductCode;
	NSNumber * SysGroupMenuId;
	tns1_ArrayOfSysGroupMenuPermission * SysGroupMenuPermissions;
	NSNumber * SysMenuId;
	NSString * SysMenuName;
	tns1_ArrayOfSysRoleMenuPermission * SysRoleMenuPermissions;
	tns1_ArrayOfSysUserMenuPermission * SysUserMenuPermissions;
	NSString * Url;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysMenu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Action;
@property (retain) NSString * ApplicationCodeAction;
@property (retain) NSString * Controller;
@property (retain) USBoolean * IsDefault;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Priority;
@property (retain) NSString * ProductCode;
@property (retain) NSNumber * SysGroupMenuId;
@property (retain) tns1_ArrayOfSysGroupMenuPermission * SysGroupMenuPermissions;
@property (retain) NSNumber * SysMenuId;
@property (retain) NSString * SysMenuName;
@property (retain) tns1_ArrayOfSysRoleMenuPermission * SysRoleMenuPermissions;
@property (retain) tns1_ArrayOfSysUserMenuPermission * SysUserMenuPermissions;
@property (retain) NSString * Url;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysGroupMenuPermission : NSObject {
	
/* elements */
	NSNumber * AssignedBy;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_SysGroup * SysGroup;
	NSNumber * SysGroupId;
	NSNumber * SysGroupMenuPermissionId;
	tns1_SysMenu * SysMenu;
	NSNumber * SysMenuId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysGroupMenuPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AssignedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_SysGroup * SysGroup;
@property (retain) NSNumber * SysGroupId;
@property (retain) NSNumber * SysGroupMenuPermissionId;
@property (retain) tns1_SysMenu * SysMenu;
@property (retain) NSNumber * SysMenuId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysGroupMenuPermission : NSObject {
	
/* elements */
	NSMutableArray *SysGroupMenuPermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysGroupMenuPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysGroupMenuPermission:(tns1_SysGroupMenuPermission *)toAdd;
@property (readonly) NSMutableArray * SysGroupMenuPermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysGroup : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * Description;
	NSString * Name;
	tns1_ArrayOfSysGroupEmpProfilePermission * SysGroupEmpProfilePermissions;
	NSNumber * SysGroupId;
	tns1_ArrayOfSysGroupMenuPermission * SysGroupMenuPermissions;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Description;
@property (retain) NSString * Name;
@property (retain) tns1_ArrayOfSysGroupEmpProfilePermission * SysGroupEmpProfilePermissions;
@property (retain) NSNumber * SysGroupId;
@property (retain) tns1_ArrayOfSysGroupMenuPermission * SysGroupMenuPermissions;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysGroupEmpProfilePermission : NSObject {
	
/* elements */
	USBoolean * AddPermission;
	USBoolean * AprovePermisson;
	NSNumber * AssignedBy;
	NSDate * CreatedDate;
	USBoolean * DeletePermission;
	USBoolean * EditPermission;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_SysEmpProfileLayer * SysEmpProfileLayer;
	NSNumber * SysEmpProfileLayerId;
	tns1_SysGroup * SysGroup;
	NSNumber * SysGroupEmpProfilePermissionId;
	NSNumber * SysGroupId;
	USBoolean * ViewPermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysGroupEmpProfilePermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddPermission;
@property (retain) USBoolean * AprovePermisson;
@property (retain) NSNumber * AssignedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * DeletePermission;
@property (retain) USBoolean * EditPermission;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_SysEmpProfileLayer * SysEmpProfileLayer;
@property (retain) NSNumber * SysEmpProfileLayerId;
@property (retain) tns1_SysGroup * SysGroup;
@property (retain) NSNumber * SysGroupEmpProfilePermissionId;
@property (retain) NSNumber * SysGroupId;
@property (retain) USBoolean * ViewPermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysGroupEmpProfilePermission : NSObject {
	
/* elements */
	NSMutableArray *SysGroupEmpProfilePermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysGroupEmpProfilePermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysGroupEmpProfilePermission:(tns1_SysGroupEmpProfilePermission *)toAdd;
@property (readonly) NSMutableArray * SysGroupEmpProfilePermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysEmpProfileLayer : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * Icon;
	USBoolean * IsDeleted;
	USBoolean * IsVisible;
	NSNumber * Priority;
	tns1_SysEmpProfileGroupLayer * SysEmpProfileGroupLayer;
	NSNumber * SysEmpProfileGroupLayerId;
	NSString * SysEmpProfileLayerCode;
	NSNumber * SysEmpProfileLayerId;
	NSString * SysEmpProfileLayerName;
	tns1_ArrayOfSysEmpProfileLayerUrl * SysEmpProfileLayerUrls;
	tns1_ArrayOfSysGroupEmpProfilePermission * SysGroupEmpProfilePermissions;
	tns1_ArrayOfSysRoleEmpProfilePermission * SysRoleEmpProfilePermissions;
	tns1_ArrayOfSysUserEmpProfilePermission * SysUserEmpProfilePermissions;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysEmpProfileLayer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Icon;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsVisible;
@property (retain) NSNumber * Priority;
@property (retain) tns1_SysEmpProfileGroupLayer * SysEmpProfileGroupLayer;
@property (retain) NSNumber * SysEmpProfileGroupLayerId;
@property (retain) NSString * SysEmpProfileLayerCode;
@property (retain) NSNumber * SysEmpProfileLayerId;
@property (retain) NSString * SysEmpProfileLayerName;
@property (retain) tns1_ArrayOfSysEmpProfileLayerUrl * SysEmpProfileLayerUrls;
@property (retain) tns1_ArrayOfSysGroupEmpProfilePermission * SysGroupEmpProfilePermissions;
@property (retain) tns1_ArrayOfSysRoleEmpProfilePermission * SysRoleEmpProfilePermissions;
@property (retain) tns1_ArrayOfSysUserEmpProfilePermission * SysUserEmpProfilePermissions;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysUserEmpProfilePermission : NSObject {
	
/* elements */
	USBoolean * AddPermission;
	USBoolean * AprovePermisson;
	NSNumber * AssignedBy;
	NSDate * CreatedDate;
	USBoolean * DeletePermission;
	USBoolean * EditPermission;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_SysEmpProfileLayer * SysEmpProfileLayer;
	NSNumber * SysEmpProfileLayerId;
	tns1_SysUser * SysUser;
	NSNumber * SysUserEmpProfilePermissionId;
	NSNumber * SysUserId;
	USBoolean * ViewPermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysUserEmpProfilePermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddPermission;
@property (retain) USBoolean * AprovePermisson;
@property (retain) NSNumber * AssignedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * DeletePermission;
@property (retain) USBoolean * EditPermission;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_SysEmpProfileLayer * SysEmpProfileLayer;
@property (retain) NSNumber * SysEmpProfileLayerId;
@property (retain) tns1_SysUser * SysUser;
@property (retain) NSNumber * SysUserEmpProfilePermissionId;
@property (retain) NSNumber * SysUserId;
@property (retain) USBoolean * ViewPermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysUserEmpProfilePermission : NSObject {
	
/* elements */
	NSMutableArray *SysUserEmpProfilePermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysUserEmpProfilePermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysUserEmpProfilePermission:(tns1_SysUserEmpProfilePermission *)toAdd;
@property (readonly) NSMutableArray * SysUserEmpProfilePermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysUser : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * CurrentSessionId;
	NSNumber * EmployeeId;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSString * SelfServiceXML;
	tns1_ArrayOfSysUserEmpProfilePermission * SysUserEmpProfilePermissions;
	NSNumber * SysUserId;
	tns1_ArrayOfSysUserMenuPermission * SysUserMenuPermissions;
	NSString * Username;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysUser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CurrentSessionId;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * SelfServiceXML;
@property (retain) tns1_ArrayOfSysUserEmpProfilePermission * SysUserEmpProfilePermissions;
@property (retain) NSNumber * SysUserId;
@property (retain) tns1_ArrayOfSysUserMenuPermission * SysUserMenuPermissions;
@property (retain) NSString * Username;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_V_SysUserMembership : NSObject {
	
/* elements */
	NSString * CompanyCode;
	NSNumber * CompanyId;
	NSString * CompanyName;
	NSString * DomainName;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSDate * LastLogin;
	NSString * Password;
	NSString * PasswordSalt;
	NSNumber * SysMembershipId;
	NSNumber * SysUserId;
	NSString * Username;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_V_SysUserMembership *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CompanyCode;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompanyName;
@property (retain) NSString * DomainName;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * LastLogin;
@property (retain) NSString * Password;
@property (retain) NSString * PasswordSalt;
@property (retain) NSNumber * SysMembershipId;
@property (retain) NSNumber * SysUserId;
@property (retain) NSString * Username;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysUser : NSObject {
	
/* elements */
	NSMutableArray *SysUser;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysUser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysUser:(tns1_SysUser *)toAdd;
@property (readonly) NSMutableArray * SysUser;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_V_SysUserInRole : NSObject {
	
/* elements */
	NSString * CAREER_NAME_EN;
	NSString * CAREER_NAME_VN;
	NSString * CLOG_CAREER_CODE;
	NSString * CompanyCode;
	NSNumber * CompanyId;
	NSString * CompanyName;
	NSString * Description;
	NSString * DomainName;
	NSString * Email;
	NSString * EmpWorkingStatusCode;
	NSString * EmpWorkingStatusName;
	NSString * EmpWorkingStatusNameEN;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EmployeeTypeId;
	NSString * EmployeeTypeName;
	NSString * EmployeeTypeNameEN;
	NSDate * EntryDate;
	NSString * FullName;
	USBoolean * Gender;
	NSString * IdentityCardNo;
	NSString * ImageUrl;
	USBoolean * IsDeleted;
	NSString * MobileNumber;
	NSString * Name;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobNameEN;
	NSNumber * OrgJobPositionId;
	NSString * OrgJobPositionName;
	NSString * OrgJobPositionNameEN;
	NSString * OrgJobTitleCode;
	NSNumber * OrgJobTitleId;
	NSString * OrgJobTitleLevelCode;
	NSString * OrgJobTitleLevelNameEN;
	NSString * OrgJobTitleLevelNameVN;
	NSString * OrgJobTitleNameEN;
	NSString * OrgJobTitleNameVN;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSNumber * OrgWorkLevelId;
	NSNumber * SysRoleId;
	NSNumber * SysUserId;
	NSNumber * SysUserInRoleId;
	NSString * UnitName;
	NSString * UnitNameEN;
	NSString * Username;
	NSString * WorkLevelName;
	NSString * WorkLevelNameEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_V_SysUserInRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CAREER_NAME_EN;
@property (retain) NSString * CAREER_NAME_VN;
@property (retain) NSString * CLOG_CAREER_CODE;
@property (retain) NSString * CompanyCode;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompanyName;
@property (retain) NSString * Description;
@property (retain) NSString * DomainName;
@property (retain) NSString * Email;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmpWorkingStatusNameEN;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSString * EmployeeTypeName;
@property (retain) NSString * EmployeeTypeNameEN;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * MobileNumber;
@property (retain) NSString * Name;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobNameEN;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSString * OrgJobPositionNameEN;
@property (retain) NSString * OrgJobTitleCode;
@property (retain) NSNumber * OrgJobTitleId;
@property (retain) NSString * OrgJobTitleLevelCode;
@property (retain) NSString * OrgJobTitleLevelNameEN;
@property (retain) NSString * OrgJobTitleLevelNameVN;
@property (retain) NSString * OrgJobTitleNameEN;
@property (retain) NSString * OrgJobTitleNameVN;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSNumber * SysRoleId;
@property (retain) NSNumber * SysUserId;
@property (retain) NSNumber * SysUserInRoleId;
@property (retain) NSString * UnitName;
@property (retain) NSString * UnitNameEN;
@property (retain) NSString * Username;
@property (retain) NSString * WorkLevelName;
@property (retain) NSString * WorkLevelNameEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfV_SysUserInRole : NSObject {
	
/* elements */
	NSMutableArray *V_SysUserInRole;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfV_SysUserInRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_SysUserInRole:(tns1_V_SysUserInRole *)toAdd;
@property (readonly) NSMutableArray * V_SysUserInRole;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfV_SysUserMembership : NSObject {
	
/* elements */
	NSMutableArray *V_SysUserMembership;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfV_SysUserMembership *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_SysUserMembership:(tns1_V_SysUserMembership *)toAdd;
@property (readonly) NSMutableArray * V_SysUserMembership;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_V_SysUserInGroup : NSObject {
	
/* elements */
	NSString * CAREER_NAME_EN;
	NSString * CAREER_NAME_VN;
	NSString * CLOG_CAREER_CODE;
	NSString * CompanyCode;
	NSNumber * CompanyId;
	NSString * CompanyName;
	NSString * Description;
	NSString * DomainName;
	NSString * Email;
	NSString * EmpWorkingStatusCode;
	NSString * EmpWorkingStatusName;
	NSString * EmpWorkingStatusNameEN;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EmployeeTypeId;
	NSString * EmployeeTypeName;
	NSString * EmployeeTypeNameEN;
	NSDate * EntryDate;
	NSString * FullName;
	USBoolean * Gender;
	NSString * IdentityCardNo;
	NSString * ImageUrl;
	USBoolean * IsDeleted;
	NSString * MobileNumber;
	NSString * Name;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobNameEN;
	NSNumber * OrgJobPositionId;
	NSString * OrgJobPositionName;
	NSString * OrgJobPositionNameEN;
	NSString * OrgJobTitleCode;
	NSNumber * OrgJobTitleId;
	NSString * OrgJobTitleLevelCode;
	NSString * OrgJobTitleLevelNameEN;
	NSString * OrgJobTitleLevelNameVN;
	NSString * OrgJobTitleNameEN;
	NSString * OrgJobTitleNameVN;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSNumber * OrgWorkLevelId;
	NSNumber * SysGroupId;
	NSNumber * SysUserId;
	NSNumber * SysUserInGroupId;
	NSString * UnitName;
	NSString * UnitNameEN;
	NSString * Username;
	NSString * WorkLevelName;
	NSString * WorkLevelNameEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_V_SysUserInGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CAREER_NAME_EN;
@property (retain) NSString * CAREER_NAME_VN;
@property (retain) NSString * CLOG_CAREER_CODE;
@property (retain) NSString * CompanyCode;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompanyName;
@property (retain) NSString * Description;
@property (retain) NSString * DomainName;
@property (retain) NSString * Email;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmpWorkingStatusNameEN;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSString * EmployeeTypeName;
@property (retain) NSString * EmployeeTypeNameEN;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * MobileNumber;
@property (retain) NSString * Name;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobNameEN;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSString * OrgJobPositionNameEN;
@property (retain) NSString * OrgJobTitleCode;
@property (retain) NSNumber * OrgJobTitleId;
@property (retain) NSString * OrgJobTitleLevelCode;
@property (retain) NSString * OrgJobTitleLevelNameEN;
@property (retain) NSString * OrgJobTitleLevelNameVN;
@property (retain) NSString * OrgJobTitleNameEN;
@property (retain) NSString * OrgJobTitleNameVN;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSNumber * SysGroupId;
@property (retain) NSNumber * SysUserId;
@property (retain) NSNumber * SysUserInGroupId;
@property (retain) NSString * UnitName;
@property (retain) NSString * UnitNameEN;
@property (retain) NSString * Username;
@property (retain) NSString * WorkLevelName;
@property (retain) NSString * WorkLevelNameEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfV_SysUserInGroup : NSObject {
	
/* elements */
	NSMutableArray *V_SysUserInGroup;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfV_SysUserInGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_SysUserInGroup:(tns1_V_SysUserInGroup *)toAdd;
@property (readonly) NSMutableArray * V_SysUserInGroup;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_V_SysGroupInRole : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * Name;
	NSString * RoleName;
	NSNumber * SysGroupId;
	NSNumber * SysGroupInRoleId;
	NSNumber * SysRoleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_V_SysGroupInRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Name;
@property (retain) NSString * RoleName;
@property (retain) NSNumber * SysGroupId;
@property (retain) NSNumber * SysGroupInRoleId;
@property (retain) NSNumber * SysRoleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfV_SysGroupInRole : NSObject {
	
/* elements */
	NSMutableArray *V_SysGroupInRole;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfV_SysGroupInRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_SysGroupInRole:(tns1_V_SysGroupInRole *)toAdd;
@property (readonly) NSMutableArray * V_SysGroupInRole;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysGroup : NSObject {
	
/* elements */
	NSMutableArray *SysGroup;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysGroup:(tns1_SysGroup *)toAdd;
@property (readonly) NSMutableArray * SysGroup;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_V_SysRoleInGroup : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * GroupName;
	NSString * Name;
	NSNumber * SysGroupId;
	NSNumber * SysGroupInRoleId;
	NSNumber * SysRoleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_V_SysRoleInGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * GroupName;
@property (retain) NSString * Name;
@property (retain) NSNumber * SysGroupId;
@property (retain) NSNumber * SysGroupInRoleId;
@property (retain) NSNumber * SysRoleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfV_SysRoleInGroup : NSObject {
	
/* elements */
	NSMutableArray *V_SysRoleInGroup;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfV_SysRoleInGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_SysRoleInGroup:(tns1_V_SysRoleInGroup *)toAdd;
@property (readonly) NSMutableArray * V_SysRoleInGroup;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysRole : NSObject {
	
/* elements */
	NSMutableArray *SysRole;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysRole:(tns1_SysRole *)toAdd;
@property (readonly) NSMutableArray * SysRole;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CBAccidentInsurance : NSObject {
	
/* elements */
	NSString * AccidentInsuranceContractCode;
	NSString * AccidentInsuranceContractNumber;
	NSString * AccidentName;
	NSNumber * CBAccidentInsuranceId;
	NSNumber * Charge;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * EffectDate;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSDate * ExpireDate;
	NSString * InsuranceCompanyName;
	NSString * InsuranceKind;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CBAccidentInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AccidentInsuranceContractCode;
@property (retain) NSString * AccidentInsuranceContractNumber;
@property (retain) NSString * AccidentName;
@property (retain) NSNumber * CBAccidentInsuranceId;
@property (retain) NSNumber * Charge;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EffectDate;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpireDate;
@property (retain) NSString * InsuranceCompanyName;
@property (retain) NSString * InsuranceKind;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfCBAccidentInsurance : NSObject {
	
/* elements */
	NSMutableArray *CBAccidentInsurance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfCBAccidentInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBAccidentInsurance:(tns1_CBAccidentInsurance *)toAdd;
@property (readonly) NSMutableArray * CBAccidentInsurance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CBCompensationTypeFactor : NSObject {
	
/* elements */
	NSNumber * CBCompensationTypeFactorId;
	NSNumber * CLogCBCompensationTypeId;
	tns1_CLogCBFactor * CLogCBFactor;
	NSNumber * CLogCBFactorId;
	USBoolean * IsDeleted;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CBCompensationTypeFactor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBCompensationTypeFactorId;
@property (retain) NSNumber * CLogCBCompensationTypeId;
@property (retain) tns1_CLogCBFactor * CLogCBFactor;
@property (retain) NSNumber * CLogCBFactorId;
@property (retain) USBoolean * IsDeleted;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfCBCompensationTypeFactor : NSObject {
	
/* elements */
	NSMutableArray *CBCompensationTypeFactor;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfCBCompensationTypeFactor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBCompensationTypeFactor:(tns1_CBCompensationTypeFactor *)toAdd;
@property (readonly) NSMutableArray * CBCompensationTypeFactor;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfCLogCBFactor : NSObject {
	
/* elements */
	NSMutableArray *CLogCBFactor;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfCLogCBFactor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogCBFactor:(tns1_CLogCBFactor *)toAdd;
@property (readonly) NSMutableArray * CLogCBFactor;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CLogCBCompensationCategory : NSObject {
	
/* elements */
	NSNumber * CLogCBCompensationCategoryId;
	tns1_ArrayOfCLogCBFactor * CLogCBFactors;
	NSString * CLogCompensationCategoryCode;
	NSString * Description;
	USBoolean * IsDeleted;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CLogCBCompensationCategory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogCBCompensationCategoryId;
@property (retain) tns1_ArrayOfCLogCBFactor * CLogCBFactors;
@property (retain) NSString * CLogCompensationCategoryCode;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CLogCBFactor : NSObject {
	
/* elements */
	tns1_ArrayOfCBCompensationTypeFactor * CBCompensationTypeFactors;
	tns1_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
	tns1_CLogCBCompensationCategory * CLogCBCompensationCategory;
	NSNumber * CLogCBCompensationCategoryId;
	NSString * CLogCBFactorCode;
	NSNumber * CLogCBFactorGroupId;
	NSNumber * CLogCBFactorId;
	NSString * Description;
	NSString * FormulaExpression;
	USBoolean * IsActived;
	USBoolean * IsDeactived;
	USBoolean * IsDeleted;
	USBoolean * IsInitial;
	NSString * Name;
	NSNumber * Priority;
	NSString * ServiceReferenceEndPointAddress;
	NSString * ServiceReferenceEndPointOperationContract;
	NSString * TableReference;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CLogCBFactor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfCBCompensationTypeFactor * CBCompensationTypeFactors;
@property (retain) tns1_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
@property (retain) tns1_CLogCBCompensationCategory * CLogCBCompensationCategory;
@property (retain) NSNumber * CLogCBCompensationCategoryId;
@property (retain) NSString * CLogCBFactorCode;
@property (retain) NSNumber * CLogCBFactorGroupId;
@property (retain) NSNumber * CLogCBFactorId;
@property (retain) NSString * Description;
@property (retain) NSString * FormulaExpression;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeactived;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInitial;
@property (retain) NSString * Name;
@property (retain) NSNumber * Priority;
@property (retain) NSString * ServiceReferenceEndPointAddress;
@property (retain) NSString * ServiceReferenceEndPointOperationContract;
@property (retain) NSString * TableReference;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CBFactorEmployeeMetaData : NSObject {
	
/* elements */
	NSDate * BeginDate;
	tns1_CBCompensationEmployeeGroup * CBCompensationEmployeeGroup;
	NSNumber * CBCompensationEmployeeGroupId;
	NSString * CBFactorEmployeeMetaDataDescription;
	NSNumber * CBFactorEmployeeMetaDataId;
	tns1_CLogCBFactor * CLogCBFactor;
	NSNumber * CLogCBFactorId;
	NSString * ChangeReason;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	NSNumber * FactorValue;
	NSString * GroupGuid;
	USBoolean * IsActivated;
	USBoolean * IsAppliedForCompany;
	USBoolean * IsChanged;
	USBoolean * IsCurrent;
	USBoolean * IsDeactivated;
	USBoolean * IsDeleted;
	USBoolean * IsInitialed;
	NSDate * ModifiedDate;
	NSNumber * OldFactorValue;
	NSString * ReferenceColunmName;
	NSString * ReferenceRowGuid;
	NSString * ReferenceTableName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CBFactorEmployeeMetaData *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * BeginDate;
@property (retain) tns1_CBCompensationEmployeeGroup * CBCompensationEmployeeGroup;
@property (retain) NSNumber * CBCompensationEmployeeGroupId;
@property (retain) NSString * CBFactorEmployeeMetaDataDescription;
@property (retain) NSNumber * CBFactorEmployeeMetaDataId;
@property (retain) tns1_CLogCBFactor * CLogCBFactor;
@property (retain) NSNumber * CLogCBFactorId;
@property (retain) NSString * ChangeReason;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) NSNumber * FactorValue;
@property (retain) NSString * GroupGuid;
@property (retain) USBoolean * IsActivated;
@property (retain) USBoolean * IsAppliedForCompany;
@property (retain) USBoolean * IsChanged;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeactivated;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInitialed;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OldFactorValue;
@property (retain) NSString * ReferenceColunmName;
@property (retain) NSString * ReferenceRowGuid;
@property (retain) NSString * ReferenceTableName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfCBFactorEmployeeMetaData : NSObject {
	
/* elements */
	NSMutableArray *CBFactorEmployeeMetaData;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfCBFactorEmployeeMetaData *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBFactorEmployeeMetaData:(tns1_CBFactorEmployeeMetaData *)toAdd;
@property (readonly) NSMutableArray * CBFactorEmployeeMetaData;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CBCompensationEmployeeGroup : NSObject {
	
/* elements */
	tns1_ArrayOfCBCompensationEmployeeGroupDetail * CBCompensationEmployeeGroupDetails;
	NSNumber * CBCompensationEmployeeGroupId;
	tns1_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
	NSNumber * CompanyId;
	NSString * Description;
	USBoolean * IsActived;
	USBoolean * IsDeactived;
	USBoolean * IsDeleted;
	USBoolean * IsInitial;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CBCompensationEmployeeGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfCBCompensationEmployeeGroupDetail * CBCompensationEmployeeGroupDetails;
@property (retain) NSNumber * CBCompensationEmployeeGroupId;
@property (retain) tns1_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeactived;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInitial;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CBCompensationEmployeeGroupDetail : NSObject {
	
/* elements */
	tns1_CBCompensationEmployeeGroup * CBCompensationEmployeeGroup;
	NSNumber * CBCompensationEmployeeGroupDetailId;
	NSNumber * CBCompensationEmployeeGroupId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CBCompensationEmployeeGroupDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_CBCompensationEmployeeGroup * CBCompensationEmployeeGroup;
@property (retain) NSNumber * CBCompensationEmployeeGroupDetailId;
@property (retain) NSNumber * CBCompensationEmployeeGroupId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfCBCompensationEmployeeGroupDetail : NSObject {
	
/* elements */
	NSMutableArray *CBCompensationEmployeeGroupDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfCBCompensationEmployeeGroupDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBCompensationEmployeeGroupDetail:(tns1_CBCompensationEmployeeGroupDetail *)toAdd;
@property (readonly) NSMutableArray * CBCompensationEmployeeGroupDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CBConvalescence : NSObject {
	
/* elements */
	NSNumber * AllowanceAmount;
	NSNumber * CBConvalescenceId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSDate * FromDate;
	NSNumber * HealthLessLevel;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * NumberDayOff;
	NSDate * NumberOfMounthApplied;
	NSDate * ToDate;
	NSNumber * TypeId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CBConvalescence *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AllowanceAmount;
@property (retain) NSNumber * CBConvalescenceId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * FromDate;
@property (retain) NSNumber * HealthLessLevel;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * NumberDayOff;
@property (retain) NSDate * NumberOfMounthApplied;
@property (retain) NSDate * ToDate;
@property (retain) NSNumber * TypeId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfCBConvalescence : NSObject {
	
/* elements */
	NSMutableArray *CBConvalescence;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfCBConvalescence *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBConvalescence:(tns1_CBConvalescence *)toAdd;
@property (readonly) NSMutableArray * CBConvalescence;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CLogCBDisease : NSObject {
	
/* elements */
	tns1_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
	NSNumber * CLogCBDiseaseId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	NSString * DiseaseCode;
	NSString * DiseaseGroupCode;
	USBoolean * IsChronic;
	USBoolean * IsDeleted;
	USBoolean * IsOccupational;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CLogCBDisease *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
@property (retain) NSNumber * CLogCBDiseaseId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) NSString * DiseaseCode;
@property (retain) NSString * DiseaseGroupCode;
@property (retain) USBoolean * IsChronic;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOccupational;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CBHealthInsurance : NSObject {
	
/* elements */
	NSNumber * CBHealthInsuranceId;
	tns1_CLogHospital * CLogHospital;
	NSNumber * CLogHospitalId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * EffectDate;
	NSDate * ExpireDate;
	NSNumber * GrossCharge;
	NSString * HealthInsuranceContractCode;
	NSString * HealthInsuranceContractNumber;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Remark;
	NSNumber * SignerId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CBHealthInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBHealthInsuranceId;
@property (retain) tns1_CLogHospital * CLogHospital;
@property (retain) NSNumber * CLogHospitalId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EffectDate;
@property (retain) NSDate * ExpireDate;
@property (retain) NSNumber * GrossCharge;
@property (retain) NSString * HealthInsuranceContractCode;
@property (retain) NSString * HealthInsuranceContractNumber;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Remark;
@property (retain) NSNumber * SignerId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfCBHealthInsurance : NSObject {
	
/* elements */
	NSMutableArray *CBHealthInsurance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfCBHealthInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBHealthInsurance:(tns1_CBHealthInsurance *)toAdd;
@property (readonly) NSMutableArray * CBHealthInsurance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfCLogCity : NSObject {
	
/* elements */
	NSMutableArray *CLogCity;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfCLogCity *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogCity:(tns1_CLogCity *)toAdd;
@property (readonly) NSMutableArray * CLogCity;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CLogCountry : NSObject {
	
/* elements */
	tns1_ArrayOfCLogCity * CLogCities;
	NSString * CLogCountryCode;
	NSNumber * CLogCountryId;
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CLogCountry *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfCLogCity * CLogCities;
@property (retain) NSString * CLogCountryCode;
@property (retain) NSNumber * CLogCountryId;
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CLogDistrict : NSObject {
	
/* elements */
	tns1_CLogCity * CLogCity;
	NSString * CLogCityCode;
	NSNumber * CLogCityId;
	NSString * CLogDistrictCode;
	NSNumber * CLogDistrictId;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CLogDistrict *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_CLogCity * CLogCity;
@property (retain) NSString * CLogCityCode;
@property (retain) NSNumber * CLogCityId;
@property (retain) NSString * CLogDistrictCode;
@property (retain) NSNumber * CLogDistrictId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfCLogDistrict : NSObject {
	
/* elements */
	NSMutableArray *CLogDistrict;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfCLogDistrict *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogDistrict:(tns1_CLogDistrict *)toAdd;
@property (readonly) NSMutableArray * CLogDistrict;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfCLogHospital : NSObject {
	
/* elements */
	NSMutableArray *CLogHospital;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfCLogHospital *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogHospital:(tns1_CLogHospital *)toAdd;
@property (readonly) NSMutableArray * CLogHospital;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmployee : NSObject {
	
/* elements */
	NSMutableArray *Employee;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmployee:(tns1_Employee *)toAdd;
@property (readonly) NSMutableArray * Employee;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CLogCity : NSObject {
	
/* elements */
	NSString * CLogCityCode;
	NSNumber * CLogCityId;
	NSString * CLogCityNoIns;
	tns1_CLogCountry * CLogCountry;
	NSString * CLogCountryCode;
	NSNumber * CLogCountryId;
	tns1_ArrayOfCLogDistrict * CLogDistricts;
	tns1_ArrayOfCLogHospital * CLogHospitals;
	NSNumber * CompanyId;
	tns1_ArrayOfEmployee * Employees;
	tns1_ArrayOfEmployee * Employees1;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
	NSString * SysNo;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CLogCity *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogCityCode;
@property (retain) NSNumber * CLogCityId;
@property (retain) NSString * CLogCityNoIns;
@property (retain) tns1_CLogCountry * CLogCountry;
@property (retain) NSString * CLogCountryCode;
@property (retain) NSNumber * CLogCountryId;
@property (retain) tns1_ArrayOfCLogDistrict * CLogDistricts;
@property (retain) tns1_ArrayOfCLogHospital * CLogHospitals;
@property (retain) NSNumber * CompanyId;
@property (retain) tns1_ArrayOfEmployee * Employees;
@property (retain) tns1_ArrayOfEmployee * Employees1;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
@property (retain) NSString * SysNo;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CLogHospital : NSObject {
	
/* elements */
	NSString * Address;
	tns1_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
	tns1_ArrayOfCBHealthInsurance * CBHealthInsurances;
	tns1_CLogCity * CLogCity;
	NSNumber * CLogCityId;
	NSString * CLogCityNoIns;
	NSString * CLogHospitalCode;
	NSNumber * CLogHospitalId;
	NSNumber * CompanyId;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CLogHospital *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) tns1_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
@property (retain) tns1_ArrayOfCBHealthInsurance * CBHealthInsurances;
@property (retain) tns1_CLogCity * CLogCity;
@property (retain) NSNumber * CLogCityId;
@property (retain) NSString * CLogCityNoIns;
@property (retain) NSString * CLogHospitalCode;
@property (retain) NSNumber * CLogHospitalId;
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CBDayOffSocialInsurance : NSObject {
	
/* elements */
	NSNumber * CBDayOffSocialInsuranceId;
	tns1_CLogCBDisease * CLogCBDisease;
	NSNumber * CLogCBDiseaseId;
	tns1_CLogHospital * CLogHospital;
	NSNumber * CLogHospitalId;
	NSNumber * ClogCBDayOffTypeId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * DoctorName;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSDate * FromDate;
	USBoolean * IsDeleted;
	USBoolean * IsPaid;
	USBoolean * IsWorkAccident;
	NSDate * ModifiedDate;
	NSNumber * NumberDayOff;
	NSNumber * NumberDayOffByDoctor;
	NSNumber * NumberPaidDay;
	NSString * Reason;
	NSDate * ToDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CBDayOffSocialInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBDayOffSocialInsuranceId;
@property (retain) tns1_CLogCBDisease * CLogCBDisease;
@property (retain) NSNumber * CLogCBDiseaseId;
@property (retain) tns1_CLogHospital * CLogHospital;
@property (retain) NSNumber * CLogHospitalId;
@property (retain) NSNumber * ClogCBDayOffTypeId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DoctorName;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * FromDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsPaid;
@property (retain) USBoolean * IsWorkAccident;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NumberDayOff;
@property (retain) NSNumber * NumberDayOffByDoctor;
@property (retain) NSNumber * NumberPaidDay;
@property (retain) NSString * Reason;
@property (retain) NSDate * ToDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfCBDayOffSocialInsurance : NSObject {
	
/* elements */
	NSMutableArray *CBDayOffSocialInsurance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfCBDayOffSocialInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBDayOffSocialInsurance:(tns1_CBDayOffSocialInsurance *)toAdd;
@property (readonly) NSMutableArray * CBDayOffSocialInsurance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CBHealthInsuranceDetail : NSObject {
	
/* elements */
	NSNumber * CBHealthInsuranceDetailId;
	NSNumber * CBHealthInsuranceId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CBHealthInsuranceDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBHealthInsuranceDetailId;
@property (retain) NSNumber * CBHealthInsuranceId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfCBHealthInsuranceDetail : NSObject {
	
/* elements */
	NSMutableArray *CBHealthInsuranceDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfCBHealthInsuranceDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBHealthInsuranceDetail:(tns1_CBHealthInsuranceDetail *)toAdd;
@property (readonly) NSMutableArray * CBHealthInsuranceDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CLogEmployeeType : NSObject {
	
/* elements */
	NSString * CLogEmployeeTypeCode;
	NSNumber * CLogEmployeeTypeId;
	tns1_ArrayOfEmployee * Employees;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CLogEmployeeType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogEmployeeTypeCode;
@property (retain) NSNumber * CLogEmployeeTypeId;
@property (retain) tns1_ArrayOfEmployee * Employees;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_Employer : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_ArrayOfEmpContract * EmpContracts;
	NSString * EmployerCode;
	NSNumber * EmployerId;
	NSString * FullName;
	USBoolean * IsDeleted;
	NSString * JobPositionName;
	NSDate * ModifiedDate;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_Employer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_ArrayOfEmpContract * EmpContracts;
@property (retain) NSString * EmployerCode;
@property (retain) NSNumber * EmployerId;
@property (retain) NSString * FullName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * JobPositionName;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * DecisionFile;
	NSString * DecisionName;
	NSString * DecisionNo;
	NSString * DirectReportToEmployeeCode;
	NSNumber * DirectReportToEmployeeId;
	NSString * EmpJobPositionCode;
	NSNumber * EmpProfileJobPositionId;
	tns1_Employee * Employee;
	tns1_Employee * Employee1;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ExpirationDate;
	NSDate * ImplementationDate;
	USBoolean * IsActive;
	USBoolean * IsConcurrentPost;
	USBoolean * IsDeleted;
	USBoolean * IsMainPost;
	USBoolean * IsPromoted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * OrgJobExternalTitleCode;
	tns1_OrgJobPosition * OrgJobPosition;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSNumber * PercentParticipation;
	NSString * Reason;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DecisionFile;
@property (retain) NSString * DecisionName;
@property (retain) NSString * DecisionNo;
@property (retain) NSString * DirectReportToEmployeeCode;
@property (retain) NSNumber * DirectReportToEmployeeId;
@property (retain) NSString * EmpJobPositionCode;
@property (retain) NSNumber * EmpProfileJobPositionId;
@property (retain) tns1_Employee * Employee;
@property (retain) tns1_Employee * Employee1;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsConcurrentPost;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainPost;
@property (retain) USBoolean * IsPromoted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * OrgJobExternalTitleCode;
@property (retain) tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * PercentParticipation;
@property (retain) NSString * Reason;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileJobPosition : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileJobPosition:(tns1_EmpProfileJobPosition *)toAdd;
@property (readonly) NSMutableArray * EmpProfileJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_OrgJobPositionRequiredProficency : NSObject {
	
/* elements */
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSNumber * OrgJobPositionRequiredProficencyId;
	NSString * ProficiencyTypeCode;
	NSNumber * RecordId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_OrgJobPositionRequiredProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * OrgJobPositionRequiredProficencyId;
@property (retain) NSString * ProficiencyTypeCode;
@property (retain) NSNumber * RecordId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfOrgJobPositionRequiredProficency : NSObject {
	
/* elements */
	NSMutableArray *OrgJobPositionRequiredProficency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfOrgJobPositionRequiredProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgJobPositionRequiredProficency:(tns1_OrgJobPositionRequiredProficency *)toAdd;
@property (readonly) NSMutableArray * OrgJobPositionRequiredProficency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_OrgCoreCompetency : NSObject {
	
/* elements */
	tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompetencyLevel;
	NSDate * CreatedDate;
	NSString * Definition;
	tns1_ArrayOfEmpCompetency * EmpCompetencies;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * OrgCoreCompetencyId;
	NSString * Statement;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_OrgCoreCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompetencyLevel;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Definition;
@property (retain) tns1_ArrayOfEmpCompetency * EmpCompetencies;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * OrgCoreCompetencyId;
@property (retain) NSString * Statement;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpCompetency : NSObject {
	
/* elements */
	tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpCompetencyId;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_OrgCoreCompetency * OrgCoreCompetency;
	NSNumber * OrgCoreCompetencyId;
	tns1_OrgProficencyLevel * OrgProficencyLevel;
	NSNumber * OrgProficencyLevelId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpCompetencyId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_OrgCoreCompetency * OrgCoreCompetency;
@property (retain) NSNumber * OrgCoreCompetencyId;
@property (retain) tns1_OrgProficencyLevel * OrgProficencyLevel;
@property (retain) NSNumber * OrgProficencyLevelId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpCompetency : NSObject {
	
/* elements */
	NSMutableArray *EmpCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpCompetency:(tns1_EmpCompetency *)toAdd;
@property (readonly) NSMutableArray * EmpCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProficencyLevelRating : NSObject {
	
/* elements */
	NSMutableArray *EmpProficencyLevelRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProficencyLevelRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProficencyLevelRating:(tns1_EmpProficencyLevelRating *)toAdd;
@property (readonly) NSMutableArray * EmpProficencyLevelRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpCompetencyRating : NSObject {
	
/* elements */
	NSDate * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpCompetencyRatingId;
	tns1_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpCompetencyRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpCompetencyRatingId;
@property (retain) tns1_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProficencyLevelDetailRating : NSObject {
	
/* elements */
	NSMutableArray *EmpProficencyLevelDetailRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProficencyLevelDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProficencyLevelDetailRating:(tns1_EmpProficencyLevelDetailRating *)toAdd;
@property (readonly) NSMutableArray * EmpProficencyLevelDetailRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProficencyLevelRating : NSObject {
	
/* elements */
	NSDate * AppraisalDate;
	tns1_CLogRating * CLogRating;
	NSDate * CreatedDate;
	tns1_EmpCompetencyRating * EmpCompetencyRating;
	NSNumber * EmpCompetencyRatingId;
	tns1_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
	NSNumber * EmpProficencyLevelRatingId;
	NSString * ManagerNote;
	NSNumber * ManagerRating;
	tns1_OrgProficencyLevel * OrgProficencyLevel;
	NSNumber * OrgProficencyLevelId;
	NSNumber * PeriodicallyAssessment;
	NSNumber * Score;
	NSString * SelfNote;
	NSNumber * SelfRating;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProficencyLevelRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AppraisalDate;
@property (retain) tns1_CLogRating * CLogRating;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_EmpCompetencyRating * EmpCompetencyRating;
@property (retain) NSNumber * EmpCompetencyRatingId;
@property (retain) tns1_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
@property (retain) NSNumber * EmpProficencyLevelRatingId;
@property (retain) NSString * ManagerNote;
@property (retain) NSNumber * ManagerRating;
@property (retain) tns1_OrgProficencyLevel * OrgProficencyLevel;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) NSNumber * PeriodicallyAssessment;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfNote;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecCanProficencyLevelDetailRating : NSObject {
	
/* elements */
	NSMutableArray *RecCanProficencyLevelDetailRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecCanProficencyLevelDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCanProficencyLevelDetailRating:(tns1_RecCanProficencyLevelDetailRating *)toAdd;
@property (readonly) NSMutableArray * RecCanProficencyLevelDetailRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecCandidateProfileStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * Priority;
	NSString * ProfileStatusCode;
	NSString * ProfileStatusName;
	tns1_ArrayOfRecCandidateApplication * RecCandidateApplications;
	NSNumber * RecCandidateProfileStatusId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecCandidateProfileStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Priority;
@property (retain) NSString * ProfileStatusCode;
@property (retain) NSString * ProfileStatusName;
@property (retain) tns1_ArrayOfRecCandidateApplication * RecCandidateApplications;
@property (retain) NSNumber * RecCandidateProfileStatusId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecEvaluationCriterion : NSObject {
	
/* elements */
	NSMutableArray *RecEvaluationCriterion;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecEvaluationCriterion *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecEvaluationCriterion:(tns1_RecEvaluationCriterion *)toAdd;
@property (readonly) NSMutableArray * RecEvaluationCriterion;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecGroupEvaluationCriterion : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * Priority;
	tns1_ArrayOfRecEvaluationCriterion * RecEvaluationCriterions;
	NSString * RecGroupEvalDesc;
	NSString * RecGroupEvalName;
	NSNumber * RecGroupEvaluationCriterionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecGroupEvaluationCriterion *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Priority;
@property (retain) tns1_ArrayOfRecEvaluationCriterion * RecEvaluationCriterions;
@property (retain) NSString * RecGroupEvalDesc;
@property (retain) NSString * RecGroupEvalName;
@property (retain) NSNumber * RecGroupEvaluationCriterionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecInterviewPhaseEvaluation : NSObject {
	
/* elements */
	NSMutableArray *RecInterviewPhaseEvaluation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecInterviewPhaseEvaluation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecInterviewPhaseEvaluation:(tns1_RecInterviewPhaseEvaluation *)toAdd;
@property (readonly) NSMutableArray * RecInterviewPhaseEvaluation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecEvaluationCriterion : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * MaxPoint;
	NSNumber * Priority;
	NSString * RecEvalDesc;
	NSString * RecEvalName;
	NSNumber * RecEvaluationCriterionId;
	tns1_RecGroupEvaluationCriterion * RecGroupEvaluationCriterion;
	NSNumber * RecGroupEvaluationCriterionId;
	tns1_ArrayOfRecInterviewPhaseEvaluation * RecInterviewPhaseEvaluations;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecEvaluationCriterion *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * MaxPoint;
@property (retain) NSNumber * Priority;
@property (retain) NSString * RecEvalDesc;
@property (retain) NSString * RecEvalName;
@property (retain) NSNumber * RecEvaluationCriterionId;
@property (retain) tns1_RecGroupEvaluationCriterion * RecGroupEvaluationCriterion;
@property (retain) NSNumber * RecGroupEvaluationCriterionId;
@property (retain) tns1_ArrayOfRecInterviewPhaseEvaluation * RecInterviewPhaseEvaluations;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecInterviewPhase : NSObject {
	
/* elements */
	NSMutableArray *RecInterviewPhase;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecInterviewPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecInterviewPhase:(tns1_RecInterviewPhase *)toAdd;
@property (readonly) NSMutableArray * RecInterviewPhase;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecProcessPhaseResult : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Point_;
	tns1_RecCandidate * RecCandidate;
	tns1_RecCandidateApplication * RecCandidateApplication;
	NSNumber * RecCandidateApplicationId;
	NSNumber * RecCandidateId;
	NSNumber * RecProcessPhaseResultId;
	tns1_RecRecruitmentProcessPhase * RecRecruitmentProcessPhase;
	NSNumber * RecRecruitmentProcessPhaseId;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecProcessPhaseResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Point_;
@property (retain) tns1_RecCandidate * RecCandidate;
@property (retain) tns1_RecCandidateApplication * RecCandidateApplication;
@property (retain) NSNumber * RecCandidateApplicationId;
@property (retain) NSNumber * RecCandidateId;
@property (retain) NSNumber * RecProcessPhaseResultId;
@property (retain) tns1_RecRecruitmentProcessPhase * RecRecruitmentProcessPhase;
@property (retain) NSNumber * RecRecruitmentProcessPhaseId;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecProcessPhaseResult : NSObject {
	
/* elements */
	NSMutableArray *RecProcessPhaseResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecProcessPhaseResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecProcessPhaseResult:(tns1_RecProcessPhaseResult *)toAdd;
@property (readonly) NSMutableArray * RecProcessPhaseResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecProcessApplyForJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_OrgJobPosition * OrgJobPosition;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSNumber * RecProcessApplyForJobPositionId;
	tns1_RecRecruitmentProcess * RecRecruitmentProcess;
	NSNumber * RecRecruitmentProcessId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecProcessApplyForJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * RecProcessApplyForJobPositionId;
@property (retain) tns1_RecRecruitmentProcess * RecRecruitmentProcess;
@property (retain) NSNumber * RecRecruitmentProcessId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecProcessApplyForJobPosition : NSObject {
	
/* elements */
	NSMutableArray *RecProcessApplyForJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecProcessApplyForJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecProcessApplyForJobPosition:(tns1_RecProcessApplyForJobPosition *)toAdd;
@property (readonly) NSMutableArray * RecProcessApplyForJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecRecruitmentProcessPhase : NSObject {
	
/* elements */
	NSMutableArray *RecRecruitmentProcessPhase;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecRecruitmentProcessPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRecruitmentProcessPhase:(tns1_RecRecruitmentProcessPhase *)toAdd;
@property (readonly) NSMutableArray * RecRecruitmentProcessPhase;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecRecruitmentProcess : NSObject {
	
/* elements */
	NSDate * ApplyDate;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_ArrayOfRecProcessApplyForJobPosition * RecProcessApplyForJobPositions;
	NSString * RecProcessDescription;
	NSString * RecProcessName;
	NSNumber * RecRecruitmentProcessId;
	tns1_ArrayOfRecRecruitmentProcessPhase * RecRecruitmentProcessPhases;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecRecruitmentProcess *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ApplyDate;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_ArrayOfRecProcessApplyForJobPosition * RecProcessApplyForJobPositions;
@property (retain) NSString * RecProcessDescription;
@property (retain) NSString * RecProcessName;
@property (retain) NSNumber * RecRecruitmentProcessId;
@property (retain) tns1_ArrayOfRecRecruitmentProcessPhase * RecRecruitmentProcessPhases;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecRecruitmentProcessPhase : NSObject {
	
/* elements */
	NSNumber * CoefficientPoint;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Priority;
	tns1_ArrayOfRecInterviewPhase * RecInterviewPhases;
	NSString * RecProcessPhaseName;
	tns1_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
	tns1_RecRecruitmentProcess * RecRecruitmentProcess;
	NSNumber * RecRecruitmentProcessId;
	NSNumber * RecRecruitmentProcessPhaseId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecRecruitmentProcessPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CoefficientPoint;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Priority;
@property (retain) tns1_ArrayOfRecInterviewPhase * RecInterviewPhases;
@property (retain) NSString * RecProcessPhaseName;
@property (retain) tns1_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
@property (retain) tns1_RecRecruitmentProcess * RecRecruitmentProcess;
@property (retain) NSNumber * RecRecruitmentProcessId;
@property (retain) NSNumber * RecRecruitmentProcessPhaseId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecInterviewPhase : NSObject {
	
/* elements */
	NSNumber * CoefficientPoint;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * InterviewPhaseName;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Priority;
	tns1_ArrayOfRecInterviewPhaseEvaluation * RecInterviewPhaseEvaluations;
	NSNumber * RecInterviewPhaseId;
	tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
	NSNumber * RecProcessPhaseId;
	tns1_RecRecruitmentProcessPhase * RecRecruitmentProcessPhase;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecInterviewPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CoefficientPoint;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * InterviewPhaseName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Priority;
@property (retain) tns1_ArrayOfRecInterviewPhaseEvaluation * RecInterviewPhaseEvaluations;
@property (retain) NSNumber * RecInterviewPhaseId;
@property (retain) tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
@property (retain) NSNumber * RecProcessPhaseId;
@property (retain) tns1_RecRecruitmentProcessPhase * RecRecruitmentProcessPhase;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecInterviewPhaseEvaluation : NSObject {
	
/* elements */
	NSNumber * ComanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_RecEvaluationCriterion * RecEvaluationCriterion;
	NSNumber * RecEvaluationCriterionId;
	tns1_RecInterviewPhase * RecInterviewPhase;
	NSNumber * RecInterviewPhaseEvaluationId;
	NSNumber * RecInterviewPhaseId;
	tns1_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecInterviewPhaseEvaluation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ComanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_RecEvaluationCriterion * RecEvaluationCriterion;
@property (retain) NSNumber * RecEvaluationCriterionId;
@property (retain) tns1_RecInterviewPhase * RecInterviewPhase;
@property (retain) NSNumber * RecInterviewPhaseEvaluationId;
@property (retain) NSNumber * RecInterviewPhaseId;
@property (retain) tns1_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecScheduleInterviewerResult : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Point_;
	tns1_RecInterviewPhaseEvaluation * RecInterviewPhaseEvaluation;
	NSNumber * RecInterviewPhaseEvaluationId;
	tns1_RecInterviewSchedule * RecInterviewSchedule;
	NSNumber * RecInterviewScheduleId;
	tns1_RecInterviewer * RecInterviewer;
	NSNumber * RecInterviewerId;
	NSNumber * RecScheduleInterviewerResultId;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecScheduleInterviewerResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Point_;
@property (retain) tns1_RecInterviewPhaseEvaluation * RecInterviewPhaseEvaluation;
@property (retain) NSNumber * RecInterviewPhaseEvaluationId;
@property (retain) tns1_RecInterviewSchedule * RecInterviewSchedule;
@property (retain) NSNumber * RecInterviewScheduleId;
@property (retain) tns1_RecInterviewer * RecInterviewer;
@property (retain) NSNumber * RecInterviewerId;
@property (retain) NSNumber * RecScheduleInterviewerResultId;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecScheduleInterviewerResult : NSObject {
	
/* elements */
	NSMutableArray *RecScheduleInterviewerResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecScheduleInterviewerResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecScheduleInterviewerResult:(tns1_RecScheduleInterviewerResult *)toAdd;
@property (readonly) NSMutableArray * RecScheduleInterviewerResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecInterviewer : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsMainInterviewer;
	NSDate * ModifiedDate;
	tns1_RecGroupInterviewer * RecGroupInterviewer;
	NSNumber * RecGroupInterviewerId;
	NSNumber * RecInterviewerId;
	tns1_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecInterviewer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainInterviewer;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_RecGroupInterviewer * RecGroupInterviewer;
@property (retain) NSNumber * RecGroupInterviewerId;
@property (retain) NSNumber * RecInterviewerId;
@property (retain) tns1_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecInterviewer : NSObject {
	
/* elements */
	NSMutableArray *RecInterviewer;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecInterviewer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecInterviewer:(tns1_RecInterviewer *)toAdd;
@property (readonly) NSMutableArray * RecInterviewer;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecGroupInterviewer : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * GroupName;
	USBoolean * IsDeleted;
	NSNumber * RecGroupInterviewerId;
	tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
	tns1_ArrayOfRecInterviewer * RecInterviewers;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecGroupInterviewer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * GroupName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecGroupInterviewerId;
@property (retain) tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
@property (retain) tns1_ArrayOfRecInterviewer * RecInterviewers;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecInterviewScheduleStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * Priority;
	NSNumber * RecInterviewScheduleStatusId;
	tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
	NSString * StatusCode;
	NSString * StatusName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecInterviewScheduleStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Priority;
@property (retain) NSNumber * RecInterviewScheduleStatusId;
@property (retain) tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
@property (retain) NSString * StatusCode;
@property (retain) NSString * StatusName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecInterviewSchedule : NSObject {
	
/* elements */
	NSNumber * AveragePoint;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_Employee * Employee;
	NSNumber * EmployeeCreateId;
	NSDate * InterviewDate;
	NSString * InterviewPlace;
	NSDate * InterviewTime;
	USBoolean * IsDeleted;
	USBoolean * IsIntervieweeNotified;
	USBoolean * IsInterviewerNotified;
	NSDate * ModifiedDate;
	tns1_RecCandidateApplication * RecCandidateApplication;
	NSNumber * RecCandidateApplicationId;
	tns1_RecGroupInterviewer * RecGroupInterviewer;
	NSNumber * RecGroupInterviewerId;
	tns1_RecInterviewPhase * RecInterviewPhase;
	NSNumber * RecInterviewPhaseId;
	NSNumber * RecInterviewScheduleId;
	tns1_RecInterviewScheduleStatu * RecInterviewScheduleStatu;
	NSNumber * RecInterviewScheduleStatusId;
	tns1_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecInterviewSchedule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AveragePoint;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeCreateId;
@property (retain) NSDate * InterviewDate;
@property (retain) NSString * InterviewPlace;
@property (retain) NSDate * InterviewTime;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsIntervieweeNotified;
@property (retain) USBoolean * IsInterviewerNotified;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_RecCandidateApplication * RecCandidateApplication;
@property (retain) NSNumber * RecCandidateApplicationId;
@property (retain) tns1_RecGroupInterviewer * RecGroupInterviewer;
@property (retain) NSNumber * RecGroupInterviewerId;
@property (retain) tns1_RecInterviewPhase * RecInterviewPhase;
@property (retain) NSNumber * RecInterviewPhaseId;
@property (retain) NSNumber * RecInterviewScheduleId;
@property (retain) tns1_RecInterviewScheduleStatu * RecInterviewScheduleStatu;
@property (retain) NSNumber * RecInterviewScheduleStatusId;
@property (retain) tns1_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecInterviewSchedule : NSObject {
	
/* elements */
	NSMutableArray *RecInterviewSchedule;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecInterviewSchedule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecInterviewSchedule:(tns1_RecInterviewSchedule *)toAdd;
@property (readonly) NSMutableArray * RecInterviewSchedule;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecPhaseEmpDisplaced : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSString * Reason;
	NSNumber * RecPhaseEmpDisplaced1;
	tns1_RecRecruitmentPhase * RecRecruitmentPhase;
	NSNumber * RecRecruitmentPhaseId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecPhaseEmpDisplaced *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * Reason;
@property (retain) NSNumber * RecPhaseEmpDisplaced1;
@property (retain) tns1_RecRecruitmentPhase * RecRecruitmentPhase;
@property (retain) NSNumber * RecRecruitmentPhaseId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecPhaseEmpDisplaced : NSObject {
	
/* elements */
	NSMutableArray *RecPhaseEmpDisplaced;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecPhaseEmpDisplaced *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecPhaseEmpDisplaced:(tns1_RecPhaseEmpDisplaced *)toAdd;
@property (readonly) NSMutableArray * RecPhaseEmpDisplaced;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecRecruitmentPhase : NSObject {
	
/* elements */
	NSMutableArray *RecRecruitmentPhase;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecRecruitmentPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRecruitmentPhase:(tns1_RecRecruitmentPhase *)toAdd;
@property (readonly) NSMutableArray * RecRecruitmentPhase;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecPhaseStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSString * PhaseStatusName;
	NSNumber * RecPhaseStatusId;
	tns1_ArrayOfRecRecruitmentPhase * RecRecruitmentPhases;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecPhaseStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * PhaseStatusName;
@property (retain) NSNumber * RecPhaseStatusId;
@property (retain) tns1_ArrayOfRecRecruitmentPhase * RecRecruitmentPhases;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecRecruitmentPhaseJobPosition : NSObject {
	
/* elements */
	NSMutableArray *RecRecruitmentPhaseJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecRecruitmentPhaseJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRecruitmentPhaseJobPosition:(tns1_RecRecruitmentPhaseJobPosition *)toAdd;
@property (readonly) NSMutableArray * RecRecruitmentPhaseJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysRecPlanApprover : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsMainApprover;
	NSNumber * Priority;
	tns1_ArrayOfRecPlanApproveHistory * RecPlanApproveHistories;
	NSNumber * SysRecPlanApproverId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysRecPlanApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainApprover;
@property (retain) NSNumber * Priority;
@property (retain) tns1_ArrayOfRecPlanApproveHistory * RecPlanApproveHistories;
@property (retain) NSNumber * SysRecPlanApproverId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecPlanApproveHistory : NSObject {
	
/* elements */
	NSDate * Date;
	USBoolean * IsApproved;
	USBoolean * IsDeleted;
	NSNumber * RecPlanApproveHistoryId;
	tns1_RecRecruitmentPlan * RecRecruitmentPlan;
	NSNumber * RecRecruitmentPlanId;
	NSString * Remark;
	tns1_SysRecPlanApprover * SysRecPlanApprover;
	NSNumber * SysRecPlanApproverId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecPlanApproveHistory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * Date;
@property (retain) USBoolean * IsApproved;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecPlanApproveHistoryId;
@property (retain) tns1_RecRecruitmentPlan * RecRecruitmentPlan;
@property (retain) NSNumber * RecRecruitmentPlanId;
@property (retain) NSString * Remark;
@property (retain) tns1_SysRecPlanApprover * SysRecPlanApprover;
@property (retain) NSNumber * SysRecPlanApproverId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecPlanApproveHistory : NSObject {
	
/* elements */
	NSMutableArray *RecPlanApproveHistory;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecPlanApproveHistory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecPlanApproveHistory:(tns1_RecPlanApproveHistory *)toAdd;
@property (readonly) NSMutableArray * RecPlanApproveHistory;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecPlanJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSNumber * RecPlanJobPositionId;
	NSString * RecReason;
	tns1_RecRecruitmentPlan * RecRecruitmentPlan;
	NSNumber * RecRecruitmentPlanId;
	NSNumber * RecruitedQuantity;
	NSNumber * RequirementQuantity;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecPlanJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * RecPlanJobPositionId;
@property (retain) NSString * RecReason;
@property (retain) tns1_RecRecruitmentPlan * RecRecruitmentPlan;
@property (retain) NSNumber * RecRecruitmentPlanId;
@property (retain) NSNumber * RecruitedQuantity;
@property (retain) NSNumber * RequirementQuantity;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecPlanJobPosition : NSObject {
	
/* elements */
	NSMutableArray *RecPlanJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecPlanJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecPlanJobPosition:(tns1_RecPlanJobPosition *)toAdd;
@property (readonly) NSMutableArray * RecPlanJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecRecruitmentPlan : NSObject {
	
/* elements */
	NSMutableArray *RecRecruitmentPlan;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecRecruitmentPlan *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRecruitmentPlan:(tns1_RecRecruitmentPlan *)toAdd;
@property (readonly) NSMutableArray * RecRecruitmentPlan;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecPlanStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * RecPlanStatusId;
	tns1_ArrayOfRecRecruitmentPlan * RecRecruitmentPlans;
	NSString * StatusName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecPlanStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecPlanStatusId;
@property (retain) tns1_ArrayOfRecRecruitmentPlan * RecRecruitmentPlans;
@property (retain) NSString * StatusName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecRecruitmentRequirement : NSObject {
	
/* elements */
	NSMutableArray *RecRecruitmentRequirement;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecRecruitmentRequirement *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRecruitmentRequirement:(tns1_RecRecruitmentRequirement *)toAdd;
@property (readonly) NSMutableArray * RecRecruitmentRequirement;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecRecruitmentPlan : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * PlanDescription;
	tns1_ArrayOfRecPlanApproveHistory * RecPlanApproveHistories;
	NSNumber * RecPlanApproveHistoryId;
	tns1_ArrayOfRecPlanJobPosition * RecPlanJobPositions;
	tns1_RecPlanStatu * RecPlanStatu;
	NSNumber * RecPlanStatusId;
	NSString * RecRecruitmentPlanCode;
	NSNumber * RecRecruitmentPlanId;
	tns1_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
	NSDate * StartDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecRecruitmentPlan *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * PlanDescription;
@property (retain) tns1_ArrayOfRecPlanApproveHistory * RecPlanApproveHistories;
@property (retain) NSNumber * RecPlanApproveHistoryId;
@property (retain) tns1_ArrayOfRecPlanJobPosition * RecPlanJobPositions;
@property (retain) tns1_RecPlanStatu * RecPlanStatu;
@property (retain) NSNumber * RecPlanStatusId;
@property (retain) NSString * RecRecruitmentPlanCode;
@property (retain) NSNumber * RecRecruitmentPlanId;
@property (retain) tns1_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
@property (retain) NSDate * StartDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysRecRequirementApprover : NSObject {
	
/* elements */
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsMainApprover;
	NSNumber * Priority;
	tns1_ArrayOfRecRequirementApproveHistory * RecRequirementApproveHistories;
	NSNumber * SysRecRecruitmentApproverId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysRecRequirementApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainApprover;
@property (retain) NSNumber * Priority;
@property (retain) tns1_ArrayOfRecRequirementApproveHistory * RecRequirementApproveHistories;
@property (retain) NSNumber * SysRecRecruitmentApproverId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecRequirementApproveHistory : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * Date;
	USBoolean * IsApproved;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * RecRecruitmentApproveHistoryId;
	tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
	NSNumber * RecRequirementId;
	NSString * Remark;
	NSNumber * SysRecRecruitmentApproverId;
	tns1_SysRecRequirementApprover * SysRecRequirementApprover;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecRequirementApproveHistory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * Date;
@property (retain) USBoolean * IsApproved;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * RecRecruitmentApproveHistoryId;
@property (retain) tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
@property (retain) NSNumber * RecRequirementId;
@property (retain) NSString * Remark;
@property (retain) NSNumber * SysRecRecruitmentApproverId;
@property (retain) tns1_SysRecRequirementApprover * SysRecRequirementApprover;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecRequirementApproveHistory : NSObject {
	
/* elements */
	NSMutableArray *RecRequirementApproveHistory;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecRequirementApproveHistory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRequirementApproveHistory:(tns1_RecRequirementApproveHistory *)toAdd;
@property (readonly) NSMutableArray * RecRequirementApproveHistory;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecRequirementEmpDisplaced : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSString * Reason;
	tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
	NSNumber * RecRecruitmentRequirementId;
	NSNumber * RecRequirementEmpDisplaced1;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecRequirementEmpDisplaced *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * Reason;
@property (retain) tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
@property (retain) NSNumber * RecRecruitmentRequirementId;
@property (retain) NSNumber * RecRequirementEmpDisplaced1;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecRequirementEmpDisplaced : NSObject {
	
/* elements */
	NSMutableArray *RecRequirementEmpDisplaced;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecRequirementEmpDisplaced *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRequirementEmpDisplaced:(tns1_RecRequirementEmpDisplaced *)toAdd;
@property (readonly) NSMutableArray * RecRequirementEmpDisplaced;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecRequirementJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	USBoolean * IsDisplacement;
	NSString * JobDescription;
	NSNumber * JobDescriptionId;
	NSDate * ModifiedDate;
	tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSString * RecReason;
	tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
	NSNumber * RecRecruitmentRequirementId;
	NSNumber * RecRequirementJobPositionId;
	NSNumber * RecruitedQuantity;
	NSNumber * RequirementQuantity;
	NSString * WorkingLocation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecRequirementJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDisplacement;
@property (retain) NSString * JobDescription;
@property (retain) NSNumber * JobDescriptionId;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * RecReason;
@property (retain) tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
@property (retain) NSNumber * RecRecruitmentRequirementId;
@property (retain) NSNumber * RecRequirementJobPositionId;
@property (retain) NSNumber * RecruitedQuantity;
@property (retain) NSNumber * RequirementQuantity;
@property (retain) NSString * WorkingLocation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecRequirementJobPosition : NSObject {
	
/* elements */
	NSMutableArray *RecRequirementJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecRequirementJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRequirementJobPosition:(tns1_RecRequirementJobPosition *)toAdd;
@property (readonly) NSMutableArray * RecRequirementJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecRequirementStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	tns1_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
	NSNumber * RecRequirementStatusId;
	NSString * StatusName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecRequirementStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) tns1_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
@property (retain) NSNumber * RecRequirementStatusId;
@property (retain) NSString * StatusName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecRecruitmentRequirement : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgUnitId;
	NSDate * RecBeginDate;
	NSDate * RecEndDate;
	NSNumber * RecRecruitmentApproveHistoryId;
	tns1_ArrayOfRecRecruitmentPhase * RecRecruitmentPhases;
	tns1_RecRecruitmentPlan * RecRecruitmentPlan;
	NSNumber * RecRecruitmentPlanId;
	NSNumber * RecRecruitmentRequirementId;
	tns1_ArrayOfRecRequirementApproveHistory * RecRequirementApproveHistories;
	tns1_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
	tns1_ArrayOfRecRequirementJobPosition * RecRequirementJobPositions;
	tns1_RecRequirementStatu * RecRequirementStatu;
	NSNumber * RecRequirementStatusId;
	NSNumber * RecruitedQuantity;
	NSString * RecruitmentPurpose;
	NSString * Remark;
	NSString * RequirementCode;
	NSDate * RequirementDate;
	NSString * RequirementName;
	NSNumber * TotalQuantity;
	NSNumber * UrgentLevel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecRecruitmentRequirement *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSDate * RecBeginDate;
@property (retain) NSDate * RecEndDate;
@property (retain) NSNumber * RecRecruitmentApproveHistoryId;
@property (retain) tns1_ArrayOfRecRecruitmentPhase * RecRecruitmentPhases;
@property (retain) tns1_RecRecruitmentPlan * RecRecruitmentPlan;
@property (retain) NSNumber * RecRecruitmentPlanId;
@property (retain) NSNumber * RecRecruitmentRequirementId;
@property (retain) tns1_ArrayOfRecRequirementApproveHistory * RecRequirementApproveHistories;
@property (retain) tns1_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
@property (retain) tns1_ArrayOfRecRequirementJobPosition * RecRequirementJobPositions;
@property (retain) tns1_RecRequirementStatu * RecRequirementStatu;
@property (retain) NSNumber * RecRequirementStatusId;
@property (retain) NSNumber * RecruitedQuantity;
@property (retain) NSString * RecruitmentPurpose;
@property (retain) NSString * Remark;
@property (retain) NSString * RequirementCode;
@property (retain) NSDate * RequirementDate;
@property (retain) NSString * RequirementName;
@property (retain) NSNumber * TotalQuantity;
@property (retain) NSNumber * UrgentLevel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecRecruitmentPhase : NSObject {
	
/* elements */
	NSDate * ApplicationDate;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	USBoolean * IsNotified;
	NSDate * ModifiedDate;
	NSString * PhaseName;
	tns1_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
	tns1_RecPhaseStatu * RecPhaseStatu;
	NSNumber * RecPhaseStatusId;
	NSString * RecRecruitmentPhaseCode;
	NSNumber * RecRecruitmentPhaseId;
	tns1_ArrayOfRecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPositions;
	tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
	NSNumber * RecRecruitmentRequirementId;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecRecruitmentPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ApplicationDate;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsNotified;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * PhaseName;
@property (retain) tns1_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
@property (retain) tns1_RecPhaseStatu * RecPhaseStatu;
@property (retain) NSNumber * RecPhaseStatusId;
@property (retain) NSString * RecRecruitmentPhaseCode;
@property (retain) NSNumber * RecRecruitmentPhaseId;
@property (retain) tns1_ArrayOfRecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPositions;
@property (retain) tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
@property (retain) NSNumber * RecRecruitmentRequirementId;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecRecruitmentPhaseJobPosition : NSObject {
	
/* elements */
	NSString * AgeLimitation;
	NSString * BeginSalaryLevel;
	NSString * BenefitAllowance;
	NSNumber * CompanyId;
	NSString * ContractTerm;
	NSDate * CreatedDate;
	NSNumber * DirectLeader;
	NSDate * FromDate;
	NSString * GenderLimitation;
	USBoolean * HasProbation;
	USBoolean * IsDeleted;
	USBoolean * IsDisplacement;
	NSString * JobDescription;
	NSDate * ModifiedDate;
	tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSString * ProbationTime;
	tns1_ArrayOfRecCandidateApplication * RecCandidateApplications;
	tns1_RecRecruitmentPhase * RecRecruitmentPhase;
	NSNumber * RecRecruitmentPhaseId;
	NSNumber * RecRecruitmentPhaseJobPositionId;
	NSNumber * RecruitedQuantity;
	NSNumber * RequirementQuantity;
	NSDate * StartWorkDate;
	NSDate * ToDate;
	NSString * WorkingLocation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecRecruitmentPhaseJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AgeLimitation;
@property (retain) NSString * BeginSalaryLevel;
@property (retain) NSString * BenefitAllowance;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ContractTerm;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * DirectLeader;
@property (retain) NSDate * FromDate;
@property (retain) NSString * GenderLimitation;
@property (retain) USBoolean * HasProbation;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDisplacement;
@property (retain) NSString * JobDescription;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * ProbationTime;
@property (retain) tns1_ArrayOfRecCandidateApplication * RecCandidateApplications;
@property (retain) tns1_RecRecruitmentPhase * RecRecruitmentPhase;
@property (retain) NSNumber * RecRecruitmentPhaseId;
@property (retain) NSNumber * RecRecruitmentPhaseJobPositionId;
@property (retain) NSNumber * RecruitedQuantity;
@property (retain) NSNumber * RequirementQuantity;
@property (retain) NSDate * StartWorkDate;
@property (retain) NSDate * ToDate;
@property (retain) NSString * WorkingLocation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecCandidateApplication : NSObject {
	
/* elements */
	NSNumber * AveragePoint;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * CurrentInterviewPhaseId;
	NSNumber * CurrentProcessPhaseId;
	NSString * HRRemark;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSDate * NegotiateDate;
	tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateApplicationId;
	NSNumber * RecCandidateId;
	tns1_RecCandidateProfileStatu * RecCandidateProfileStatu;
	NSNumber * RecCandidateProfileStatusId;
	tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
	tns1_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
	tns1_RecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPosition;
	NSNumber * RecRecruitmentPhaseJobPositionId;
	NSDate * StartWorkingDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecCandidateApplication *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AveragePoint;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * CurrentInterviewPhaseId;
@property (retain) NSNumber * CurrentProcessPhaseId;
@property (retain) NSString * HRRemark;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * NegotiateDate;
@property (retain) tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateApplicationId;
@property (retain) NSNumber * RecCandidateId;
@property (retain) tns1_RecCandidateProfileStatu * RecCandidateProfileStatu;
@property (retain) NSNumber * RecCandidateProfileStatusId;
@property (retain) tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
@property (retain) tns1_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
@property (retain) tns1_RecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPosition;
@property (retain) NSNumber * RecRecruitmentPhaseJobPositionId;
@property (retain) NSDate * StartWorkingDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecCandidateApplication : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateApplication;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecCandidateApplication *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateApplication:(tns1_RecCandidateApplication *)toAdd;
@property (readonly) NSMutableArray * RecCandidateApplication;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecCandidateCompetencyRating : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateCompetencyRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecCandidateCompetencyRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateCompetencyRating:(tns1_RecCandidateCompetencyRating *)toAdd;
@property (readonly) NSMutableArray * RecCandidateCompetencyRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CLogDegree : NSObject {
	
/* elements */
	NSNumber * CLogDegreeId;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * Note;
	tns1_ArrayOfRecCandidateDegree * RecCandidateDegrees;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CLogDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogDegreeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) tns1_ArrayOfRecCandidateDegree * RecCandidateDegrees;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecCandidateDegree : NSObject {
	
/* elements */
	tns1_CLogDegree * CLogDegree;
	NSNumber * CLogDegreeId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	USBoolean * IsDeleted;
	USBoolean * IsValId;
	NSDate * ModifiedDate;
	NSString * Other;
	NSString * PlaceIssue;
	tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateDegreeId;
	NSNumber * RecCandidateId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecCandidateDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_CLogDegree * CLogDegree;
@property (retain) NSNumber * CLogDegreeId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateDegreeId;
@property (retain) NSNumber * RecCandidateId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecCandidateDegree : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateDegree;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecCandidateDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateDegree:(tns1_RecCandidateDegree *)toAdd;
@property (readonly) NSMutableArray * RecCandidateDegree;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfOrgExperience : NSObject {
	
/* elements */
	NSMutableArray *OrgExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfOrgExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgExperience:(tns1_OrgExperience *)toAdd;
@property (readonly) NSMutableArray * OrgExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_OrgProjectType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	tns1_ArrayOfOrgExperience * OrgExperiences;
	NSNumber * OrgProjectTypeId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_OrgProjectType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) tns1_ArrayOfOrgExperience * OrgExperiences;
@property (retain) NSNumber * OrgProjectTypeId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_OrgTimeInCharge : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_ArrayOfOrgExperience * OrgExperiences;
	NSNumber * OrgTimeInChargeId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_OrgTimeInCharge *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_ArrayOfOrgExperience * OrgExperiences;
@property (retain) NSNumber * OrgTimeInChargeId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_OrgExperience : NSObject {
	
/* elements */
	NSString * Achievement;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgExperienceId;
	tns1_OrgProjectType * OrgProjectType;
	NSNumber * OrgProjectTypeId;
	tns1_OrgTimeInCharge * OrgTimeInCharge;
	NSNumber * OrgTimeInChargeId;
	tns1_ArrayOfRecCandidateExperience * RecCandidateExperiences;
	NSString * RoleDescription;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_OrgExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Achievement;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgExperienceId;
@property (retain) tns1_OrgProjectType * OrgProjectType;
@property (retain) NSNumber * OrgProjectTypeId;
@property (retain) tns1_OrgTimeInCharge * OrgTimeInCharge;
@property (retain) NSNumber * OrgTimeInChargeId;
@property (retain) tns1_ArrayOfRecCandidateExperience * RecCandidateExperiences;
@property (retain) NSString * RoleDescription;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecCandidateExperience : NSObject {
	
/* elements */
	tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompanyName;
	NSDate * CreatedDate;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	USBoolean * IsInternal;
	NSString * JobPosition;
	NSDate * ModifiedDate;
	NSString * NotableAchievements;
	NSString * Note;
	tns1_OrgExperience * OrgExperience;
	NSNumber * OrgExperienceId;
	tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateExperienceId;
	NSNumber * RecCandidateId;
	NSString * Responsibilities;
	NSDate * StartDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecCandidateExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompanyName;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInternal;
@property (retain) NSString * JobPosition;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * NotableAchievements;
@property (retain) NSString * Note;
@property (retain) tns1_OrgExperience * OrgExperience;
@property (retain) NSNumber * OrgExperienceId;
@property (retain) tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateExperienceId;
@property (retain) NSNumber * RecCandidateId;
@property (retain) NSString * Responsibilities;
@property (retain) NSDate * StartDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecCandidateExperience : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecCandidateExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateExperience:(tns1_RecCandidateExperience *)toAdd;
@property (readonly) NSMutableArray * RecCandidateExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CBPITDependent : NSObject {
	
/* elements */
	NSNumber * CBPITDependentId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DependentBeginDate;
	NSString * DependentDemonstrative;
	NSDate * DependentEndDate;
	tns1_EmpProfileFamilyRelationship * EmpProfileFamilyRelationship;
	NSNumber * EmpProfileFamilyRelationshipId;
	USBoolean * IsDeleted;
	USBoolean * IsDependent;
	NSDate * ModifiedDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CBPITDependent *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBPITDependentId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DependentBeginDate;
@property (retain) NSString * DependentDemonstrative;
@property (retain) NSDate * DependentEndDate;
@property (retain) tns1_EmpProfileFamilyRelationship * EmpProfileFamilyRelationship;
@property (retain) NSNumber * EmpProfileFamilyRelationshipId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDependent;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfCBPITDependent : NSObject {
	
/* elements */
	NSMutableArray *CBPITDependent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfCBPITDependent *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBPITDependent:(tns1_CBPITDependent *)toAdd;
@property (readonly) NSMutableArray * CBPITDependent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpBeneficiary : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpBeneficiaryId;
	NSNumber * EmpFamilyRelationshipId;
	tns1_EmpProfileFamilyRelationship * EmpProfileFamilyRelationship;
	NSDate * ImplementationDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpBeneficiary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpBeneficiaryId;
@property (retain) NSNumber * EmpFamilyRelationshipId;
@property (retain) tns1_EmpProfileFamilyRelationship * EmpProfileFamilyRelationship;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpBeneficiary : NSObject {
	
/* elements */
	NSMutableArray *EmpBeneficiary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpBeneficiary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpBeneficiary:(tns1_EmpBeneficiary *)toAdd;
@property (readonly) NSMutableArray * EmpBeneficiary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileFamilyRelationship : NSObject {
	
/* elements */
	NSString * Address;
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSDate * BirthDay;
	tns1_ArrayOfCBPITDependent * CBPITDependents;
	tns1_CLogFamilyRelationship * CLogFamilyRelationship;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DeathDate;
	tns1_ArrayOfEmpBeneficiary * EmpBeneficiaries;
	NSNumber * EmpProfileFamilyRelationshipId;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * HomeTown;
	USBoolean * IsDeleted;
	USBoolean * IsEmergencyContact;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	USBoolean * Nourish;
	NSString * Occupation;
	NSString * OccupationPlace;
	NSString * PersonFirstName;
	NSString * PersonFullName;
	NSString * PersonLastName;
	NSNumber * PersonSameOccupationId;
	NSString * Phone;
	NSNumber * RelateRowId;
	NSNumber * RelationshipId;
	NSString * RelationshipName;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSDate * BirthDay;
@property (retain) tns1_ArrayOfCBPITDependent * CBPITDependents;
@property (retain) tns1_CLogFamilyRelationship * CLogFamilyRelationship;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DeathDate;
@property (retain) tns1_ArrayOfEmpBeneficiary * EmpBeneficiaries;
@property (retain) NSNumber * EmpProfileFamilyRelationshipId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * HomeTown;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEmergencyContact;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) USBoolean * Nourish;
@property (retain) NSString * Occupation;
@property (retain) NSString * OccupationPlace;
@property (retain) NSString * PersonFirstName;
@property (retain) NSString * PersonFullName;
@property (retain) NSString * PersonLastName;
@property (retain) NSNumber * PersonSameOccupationId;
@property (retain) NSString * Phone;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSNumber * RelationshipId;
@property (retain) NSString * RelationshipName;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileFamilyRelationship : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileFamilyRelationship;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileFamilyRelationship:(tns1_EmpProfileFamilyRelationship *)toAdd;
@property (readonly) NSMutableArray * EmpProfileFamilyRelationship;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CLogFamilyRelationship : NSObject {
	
/* elements */
	NSNumber * CLogFamilyRelationshipId;
	NSNumber * CompanyId;
	tns1_ArrayOfEmpProfileFamilyRelationship * EmpProfileFamilyRelationships;
	NSString * FamilyRelationshipCode;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
	tns1_ArrayOfRecCandidateFamilyRelationship * RecCandidateFamilyRelationships;
	USBoolean * Sex;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CLogFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogFamilyRelationshipId;
@property (retain) NSNumber * CompanyId;
@property (retain) tns1_ArrayOfEmpProfileFamilyRelationship * EmpProfileFamilyRelationships;
@property (retain) NSString * FamilyRelationshipCode;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
@property (retain) tns1_ArrayOfRecCandidateFamilyRelationship * RecCandidateFamilyRelationships;
@property (retain) USBoolean * Sex;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecCandidateFamilyRelationship : NSObject {
	
/* elements */
	NSString * Address;
	NSDate * BirthDay;
	tns1_CLogFamilyRelationship * CLogFamilyRelationship;
	NSNumber * CLogFamilyRelationshipId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DeathDate;
	NSString * HomeTown;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	USBoolean * Nourish;
	NSString * Occupation;
	NSString * OccupationPlace;
	NSString * PersonFirstName;
	NSString * PersonFullName;
	NSString * PersonLastName;
	NSNumber * PersonSameOccupationId;
	NSString * Phone;
	tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateFamilyRelationshipId;
	NSNumber * RecCandidateId;
	NSString * RelationshipName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecCandidateFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSDate * BirthDay;
@property (retain) tns1_CLogFamilyRelationship * CLogFamilyRelationship;
@property (retain) NSNumber * CLogFamilyRelationshipId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DeathDate;
@property (retain) NSString * HomeTown;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) USBoolean * Nourish;
@property (retain) NSString * Occupation;
@property (retain) NSString * OccupationPlace;
@property (retain) NSString * PersonFirstName;
@property (retain) NSString * PersonFullName;
@property (retain) NSString * PersonLastName;
@property (retain) NSNumber * PersonSameOccupationId;
@property (retain) NSString * Phone;
@property (retain) tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateFamilyRelationshipId;
@property (retain) NSNumber * RecCandidateId;
@property (retain) NSString * RelationshipName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecCandidateFamilyRelationship : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateFamilyRelationship;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecCandidateFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateFamilyRelationship:(tns1_RecCandidateFamilyRelationship *)toAdd;
@property (readonly) NSMutableArray * RecCandidateFamilyRelationship;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecCandidateForeignLanguage : NSObject {
	
/* elements */
	NSString * AttachmenttUrl;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Degree;
	NSDate * Duration_;
	NSDate * EffectiveDate;
	USBoolean * IsDeleted;
	NSString * Language;
	NSString * LanguageSkill;
	NSDate * ModifiedDate;
	NSString * Note;
	tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateForeignLanguageId;
	NSNumber * RecCandidateId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecCandidateForeignLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AttachmenttUrl;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Degree;
@property (retain) NSDate * Duration_;
@property (retain) NSDate * EffectiveDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Language;
@property (retain) NSString * LanguageSkill;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateForeignLanguageId;
@property (retain) NSNumber * RecCandidateId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecCandidateForeignLanguage : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateForeignLanguage;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecCandidateForeignLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateForeignLanguage:(tns1_RecCandidateForeignLanguage *)toAdd;
@property (readonly) NSMutableArray * RecCandidateForeignLanguage;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileQualification : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	NSNumber * EmpProfileQualificationId;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsValId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * OrgDegreeRankId;
	tns1_OrgQualification * OrgQualification;
	NSNumber * OrgQualificationId;
	NSString * Other;
	NSString * PlaceIssue;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) NSNumber * EmpProfileQualificationId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgDegreeRankId;
@property (retain) tns1_OrgQualification * OrgQualification;
@property (retain) NSNumber * OrgQualificationId;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileQualification : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileQualification;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileQualification:(tns1_EmpProfileQualification *)toAdd;
@property (readonly) NSMutableArray * EmpProfileQualification;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_OrgQualification : NSObject {
	
/* elements */
	NSNumber * CLogEducationLevelId;
	NSNumber * CLogMajorId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * Description;
	tns1_ArrayOfEmpProfileQualification * EmpProfileQualifications;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * OrgQualificationCode;
	NSNumber * OrgQualificationId;
	NSString * OrgQualificationName;
	tns1_ArrayOfRecCandidateQualification * RecCandidateQualifications;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_OrgQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogEducationLevelId;
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) tns1_ArrayOfEmpProfileQualification * EmpProfileQualifications;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OrgQualificationCode;
@property (retain) NSNumber * OrgQualificationId;
@property (retain) NSString * OrgQualificationName;
@property (retain) tns1_ArrayOfRecCandidateQualification * RecCandidateQualifications;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecCandidateQualification : NSObject {
	
/* elements */
	tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	USBoolean * IsDeleted;
	USBoolean * IsValId;
	NSDate * ModifiedDate;
	tns1_OrgQualification * OrgQualification;
	NSNumber * OrgQualificationId;
	NSString * Other;
	NSString * PlaceIssue;
	tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateId;
	NSNumber * RecCandidateQualificationId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecCandidateQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValId;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_OrgQualification * OrgQualification;
@property (retain) NSNumber * OrgQualificationId;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateId;
@property (retain) NSNumber * RecCandidateQualificationId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecCandidateQualification : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateQualification;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecCandidateQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateQualification:(tns1_RecCandidateQualification *)toAdd;
@property (readonly) NSMutableArray * RecCandidateQualification;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileSkill : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	tns1_CLogRating * CLogRating;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * DocumentRef;
	NSNumber * EmpProfileSkillId;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	tns1_OrgSkill * OrgSkill;
	NSNumber * OrgSkillId;
	NSNumber * RatingId;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DocumentRef;
@property (retain) NSNumber * EmpProfileSkillId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_OrgSkill * OrgSkill;
@property (retain) NSNumber * OrgSkillId;
@property (retain) NSNumber * RatingId;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileSkill : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileSkill:(tns1_EmpProfileSkill *)toAdd;
@property (readonly) NSMutableArray * EmpProfileSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfOrgSkill : NSObject {
	
/* elements */
	NSMutableArray *OrgSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfOrgSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgSkill:(tns1_OrgSkill *)toAdd;
@property (readonly) NSMutableArray * OrgSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_OrgSkillType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * OrgSkillTypeId;
	tns1_ArrayOfOrgSkill * OrgSkills;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_OrgSkillType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * OrgSkillTypeId;
@property (retain) tns1_ArrayOfOrgSkill * OrgSkills;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfTrainingCourseSchedule : NSObject {
	
/* elements */
	NSMutableArray *TrainingCourseSchedule;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfTrainingCourseSchedule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingCourseSchedule:(tns1_TrainingCourseSchedule *)toAdd;
@property (readonly) NSMutableArray * TrainingCourseSchedule;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CLogCourseSchedule : NSObject {
	
/* elements */
	NSNumber * ClogCourseScheduleId;
	NSNumber * DayOfWeek;
	NSDate * EndTime;
	USBoolean * IsDeleted;
	NSNumber * Shift;
	NSDate * StartTime;
	tns1_ArrayOfTrainingCourseSchedule * TrainingCourseSchedules;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CLogCourseSchedule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ClogCourseScheduleId;
@property (retain) NSNumber * DayOfWeek;
@property (retain) NSDate * EndTime;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Shift;
@property (retain) NSDate * StartTime;
@property (retain) tns1_ArrayOfTrainingCourseSchedule * TrainingCourseSchedules;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfTrainingCourse : NSObject {
	
/* elements */
	NSMutableArray *TrainingCourse;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfTrainingCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingCourse:(tns1_TrainingCourse *)toAdd;
@property (readonly) NSMutableArray * TrainingCourse;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CLogTrainingCenter : NSObject {
	
/* elements */
	NSNumber * CLogTrainingCenterId;
	USBoolean * IsDeleted;
	NSString * Name;
	tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CLogTrainingCenter *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogTrainingCenterId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_TrainingCategory : NSObject {
	
/* elements */
	NSDate * CreateDate;
	NSString * Description;
	NSString * ImgUrl;
	USBoolean * IsDeleted;
	NSString * Name;
	NSNumber * TrainingCategoryId;
	tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_TrainingCategory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) NSString * ImgUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSNumber * TrainingCategoryId;
@property (retain) tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_TrainingCourseChapter : NSObject {
	
/* elements */
	NSNumber * Chapter;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * Session;
	tns1_TrainingCourse * TrainingCourse;
	NSNumber * TrainingCourseChapterId;
	NSNumber * TrainingCourseId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_TrainingCourseChapter *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Chapter;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * Session;
@property (retain) tns1_TrainingCourse * TrainingCourse;
@property (retain) NSNumber * TrainingCourseChapterId;
@property (retain) NSNumber * TrainingCourseId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfTrainingCourseChapter : NSObject {
	
/* elements */
	NSMutableArray *TrainingCourseChapter;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfTrainingCourseChapter *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingCourseChapter:(tns1_TrainingCourseChapter *)toAdd;
@property (readonly) NSMutableArray * TrainingCourseChapter;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_TrainingCoursePeriod : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
	NSNumber * Session;
	NSString * Target;
	NSNumber * TrainingCoursePeriodId;
	tns1_ArrayOfTrainingCourse * TrainingCourses;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_TrainingCoursePeriod *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) NSNumber * Session;
@property (retain) NSString * Target;
@property (retain) NSNumber * TrainingCoursePeriodId;
@property (retain) tns1_ArrayOfTrainingCourse * TrainingCourses;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_TrainingCourseType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * Note;
	NSNumber * TrainingCourseTypeId;
	tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_TrainingCourseType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) NSNumber * TrainingCourseTypeId;
@property (retain) tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_TrainingCourseUnit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * OrgUnitId;
	tns1_TrainingCourse * TrainingCourse;
	NSNumber * TrainingCourseId;
	NSNumber * TrainingCourseUnitId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_TrainingCourseUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * OrgUnitId;
@property (retain) tns1_TrainingCourse * TrainingCourse;
@property (retain) NSNumber * TrainingCourseId;
@property (retain) NSNumber * TrainingCourseUnitId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfTrainingCourseUnit : NSObject {
	
/* elements */
	NSMutableArray *TrainingCourseUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfTrainingCourseUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingCourseUnit:(tns1_TrainingCourseUnit *)toAdd;
@property (readonly) NSMutableArray * TrainingCourseUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_TrainingPlanDegree : NSObject {
	
/* elements */
	NSNumber * CLogDegreeId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_TrainingCourse * TrainingCourse;
	NSNumber * TrainingPlanDegreeId;
	NSNumber * TraningCourseId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_TrainingPlanDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogDegreeId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_TrainingCourse * TrainingCourse;
@property (retain) NSNumber * TrainingPlanDegreeId;
@property (retain) NSNumber * TraningCourseId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfTrainingPlanDegree : NSObject {
	
/* elements */
	NSMutableArray *TrainingPlanDegree;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfTrainingPlanDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingPlanDegree:(tns1_TrainingPlanDegree *)toAdd;
@property (readonly) NSMutableArray * TrainingPlanDegree;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileEducation : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	tns1_CLogMajor * CLogMajor;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileEducationId;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EndYear;
	USBoolean * IsDeleted;
	NSString * Major;
	NSNumber * MajorId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * RelateRowId;
	NSNumber * StartYear;
	NSString * University;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileEducation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) tns1_CLogMajor * CLogMajor;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileEducationId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EndYear;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Major;
@property (retain) NSNumber * MajorId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSNumber * StartYear;
@property (retain) NSString * University;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileEducation : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileEducation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileEducation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileEducation:(tns1_EmpProfileEducation *)toAdd;
@property (readonly) NSMutableArray * EmpProfileEducation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfTrainingPlanRequest : NSObject {
	
/* elements */
	NSMutableArray *TrainingPlanRequest;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfTrainingPlanRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingPlanRequest:(tns1_TrainingPlanRequest *)toAdd;
@property (readonly) NSMutableArray * TrainingPlanRequest;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CLogMajor : NSObject {
	
/* elements */
	NSNumber * CLogCareerId;
	NSString * CLogMajorCode;
	NSNumber * CLogMajorId;
	tns1_ArrayOfEmpProfileEducation * EmpProfileEducations;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	tns1_ArrayOfTrainingPlanRequest * TrainingPlanRequests;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CLogMajor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogCareerId;
@property (retain) NSString * CLogMajorCode;
@property (retain) NSNumber * CLogMajorId;
@property (retain) tns1_ArrayOfEmpProfileEducation * EmpProfileEducations;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) tns1_ArrayOfTrainingPlanRequest * TrainingPlanRequests;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_TrainingPlanEmployee : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSDate * CreatedDate;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * TrainingPlanEmployeeId;
	tns1_TrainingPlanRequest * TrainingPlanRequest;
	NSNumber * TrainingPlanRequestId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_TrainingPlanEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * TrainingPlanEmployeeId;
@property (retain) tns1_TrainingPlanRequest * TrainingPlanRequest;
@property (retain) NSNumber * TrainingPlanRequestId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfTrainingPlanEmployee : NSObject {
	
/* elements */
	NSMutableArray *TrainingPlanEmployee;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfTrainingPlanEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingPlanEmployee:(tns1_TrainingPlanEmployee *)toAdd;
@property (readonly) NSMutableArray * TrainingPlanEmployee;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_TrainingPlanRequest : NSObject {
	
/* elements */
	NSNumber * ApproverId;
	tns1_CLogMajor * CLogMajor;
	NSNumber * CLogMajorId;
	NSNumber * CompanyId;
	NSString * Content;
	NSNumber * Cost;
	NSDate * CreatedDate;
	NSDate * EndDate;
	USBoolean * IsApproved;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSDate * StartDate;
	NSNumber * StrategyGoalId;
	NSString * Target;
	NSString * Title;
	tns1_ArrayOfTrainingCourse * TrainingCourses;
	tns1_ArrayOfTrainingPlanEmployee * TrainingPlanEmployees;
	NSNumber * TrainingPlanRequestId;
	NSNumber * TrainingPlanRequestTypeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_TrainingPlanRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproverId;
@property (retain) tns1_CLogMajor * CLogMajor;
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Content;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsApproved;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * StartDate;
@property (retain) NSNumber * StrategyGoalId;
@property (retain) NSString * Target;
@property (retain) NSString * Title;
@property (retain) tns1_ArrayOfTrainingCourse * TrainingCourses;
@property (retain) tns1_ArrayOfTrainingPlanEmployee * TrainingPlanEmployees;
@property (retain) NSNumber * TrainingPlanRequestId;
@property (retain) NSNumber * TrainingPlanRequestTypeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_TrainingProficencyExpected : NSObject {
	
/* elements */
	tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	USBoolean * IsDeleted;
	NSNumber * ProficencyId;
	NSNumber * ProficencyType;
	tns1_TrainingCourse * TrainingCourse;
	NSNumber * TrainingCourseId;
	NSNumber * TrainingProficencyExpectedId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_TrainingProficencyExpected *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ProficencyId;
@property (retain) NSNumber * ProficencyType;
@property (retain) tns1_TrainingCourse * TrainingCourse;
@property (retain) NSNumber * TrainingCourseId;
@property (retain) NSNumber * TrainingProficencyExpectedId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfTrainingProficencyExpected : NSObject {
	
/* elements */
	NSMutableArray *TrainingProficencyExpected;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfTrainingProficencyExpected *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingProficencyExpected:(tns1_TrainingProficencyExpected *)toAdd;
@property (readonly) NSMutableArray * TrainingProficencyExpected;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_TrainingProficencyRequire : NSObject {
	
/* elements */
	tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	USBoolean * IsDeleted;
	NSNumber * ProficencyId;
	NSNumber * ProficencyType;
	tns1_TrainingCourse * TrainingCourse;
	NSNumber * TrainingCourseId;
	NSNumber * TrainingProficencyRequireId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_TrainingProficencyRequire *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ProficencyId;
@property (retain) NSNumber * ProficencyType;
@property (retain) tns1_TrainingCourse * TrainingCourse;
@property (retain) NSNumber * TrainingCourseId;
@property (retain) NSNumber * TrainingProficencyRequireId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfTrainingProficencyRequire : NSObject {
	
/* elements */
	NSMutableArray *TrainingProficencyRequire;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfTrainingProficencyRequire *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingProficencyRequire:(tns1_TrainingProficencyRequire *)toAdd;
@property (readonly) NSMutableArray * TrainingProficencyRequire;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_TrainingCourse : NSObject {
	
/* elements */
	tns1_CLogTrainer * CLogTrainer;
	NSNumber * CLogTrainerId;
	tns1_CLogTrainingCenter * CLogTrainingCenter;
	NSNumber * CLogTrainingCenterId;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSDate * CreatedDate;
	NSString * Email;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	USBoolean * IsExpired;
	USBoolean * IsLongTrainingCourse;
	USBoolean * IsNecessitated;
	USBoolean * IsPublish;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * Number;
	NSString * PhoneNumber;
	NSString * Result;
	NSDate * StartDate;
	NSNumber * StrategyGoalId;
	tns1_TrainingCategory * TrainingCategory;
	NSNumber * TrainingCategoryId;
	tns1_ArrayOfTrainingCourseChapter * TrainingCourseChapters;
	NSNumber * TrainingCourseId;
	tns1_TrainingCoursePeriod * TrainingCoursePeriod;
	NSNumber * TrainingCoursePeriodId;
	tns1_ArrayOfTrainingCourseSchedule * TrainingCourseSchedules;
	tns1_TrainingCourseType * TrainingCourseType;
	NSNumber * TrainingCourseTypeId;
	tns1_ArrayOfTrainingCourseUnit * TrainingCourseUnits;
	tns1_ArrayOfTrainingPlanDegree * TrainingPlanDegrees;
	tns1_TrainingPlanRequest * TrainingPlanRequest;
	NSNumber * TrainingPlanRequestId;
	tns1_ArrayOfTrainingProficencyExpected * TrainingProficencyExpecteds;
	tns1_ArrayOfTrainingProficencyRequire * TrainingProficencyRequires;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_TrainingCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_CLogTrainer * CLogTrainer;
@property (retain) NSNumber * CLogTrainerId;
@property (retain) tns1_CLogTrainingCenter * CLogTrainingCenter;
@property (retain) NSNumber * CLogTrainingCenterId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Email;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsExpired;
@property (retain) USBoolean * IsLongTrainingCourse;
@property (retain) USBoolean * IsNecessitated;
@property (retain) USBoolean * IsPublish;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * Number;
@property (retain) NSString * PhoneNumber;
@property (retain) NSString * Result;
@property (retain) NSDate * StartDate;
@property (retain) NSNumber * StrategyGoalId;
@property (retain) tns1_TrainingCategory * TrainingCategory;
@property (retain) NSNumber * TrainingCategoryId;
@property (retain) tns1_ArrayOfTrainingCourseChapter * TrainingCourseChapters;
@property (retain) NSNumber * TrainingCourseId;
@property (retain) tns1_TrainingCoursePeriod * TrainingCoursePeriod;
@property (retain) NSNumber * TrainingCoursePeriodId;
@property (retain) tns1_ArrayOfTrainingCourseSchedule * TrainingCourseSchedules;
@property (retain) tns1_TrainingCourseType * TrainingCourseType;
@property (retain) NSNumber * TrainingCourseTypeId;
@property (retain) tns1_ArrayOfTrainingCourseUnit * TrainingCourseUnits;
@property (retain) tns1_ArrayOfTrainingPlanDegree * TrainingPlanDegrees;
@property (retain) tns1_TrainingPlanRequest * TrainingPlanRequest;
@property (retain) NSNumber * TrainingPlanRequestId;
@property (retain) tns1_ArrayOfTrainingProficencyExpected * TrainingProficencyExpecteds;
@property (retain) tns1_ArrayOfTrainingProficencyRequire * TrainingProficencyRequires;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfTrainingCourseEmployee : NSObject {
	
/* elements */
	NSMutableArray *TrainingCourseEmployee;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfTrainingCourseEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingCourseEmployee:(tns1_TrainingCourseEmployee *)toAdd;
@property (readonly) NSMutableArray * TrainingCourseEmployee;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_TrainingCourseSchedule : NSObject {
	
/* elements */
	tns1_CLogCourseSchedule * CLogCourseSchedule;
	NSNumber * ClogCourseScheduleId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_TrainingCourse * TrainingCourse;
	tns1_ArrayOfTrainingCourseEmployee * TrainingCourseEmployees;
	NSNumber * TrainingCourseId;
	NSNumber * TrainingCourseScheduleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_TrainingCourseSchedule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_CLogCourseSchedule * CLogCourseSchedule;
@property (retain) NSNumber * ClogCourseScheduleId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_TrainingCourse * TrainingCourse;
@property (retain) tns1_ArrayOfTrainingCourseEmployee * TrainingCourseEmployees;
@property (retain) NSNumber * TrainingCourseId;
@property (retain) NSNumber * TrainingCourseScheduleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_TrainingCourseEmployee : NSObject {
	
/* elements */
	NSNumber * Absence;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSDate * CreatedDate;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsGraduated;
	NSNumber * Mark;
	NSDate * ModifiedDate;
	NSNumber * TrainingCourseEmployeeId;
	tns1_TrainingCourseSchedule * TrainingCourseSchedule;
	NSNumber * TrainingCourseScheduleId;
	tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_TrainingCourseEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Absence;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsGraduated;
@property (retain) NSNumber * Mark;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * TrainingCourseEmployeeId;
@property (retain) tns1_TrainingCourseSchedule * TrainingCourseSchedule;
@property (retain) NSNumber * TrainingCourseScheduleId;
@property (retain) tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_TrainingEmpProficency : NSObject {
	
/* elements */
	NSNumber * CLogEmpRatingId;
	tns1_CLogRating * CLogRating;
	tns1_CLogRating * CLogRating1;
	NSNumber * ClogEmpManagerId;
	USBoolean * IsDeleted;
	NSNumber * ManagerId;
	tns1_OrgSkill * OrgSkill;
	NSNumber * ProficencyId;
	NSNumber * ProficencyType;
	tns1_TrainingCourseEmployee * TrainingCourseEmployee;
	NSNumber * TrainingCourseEmployeeId;
	NSNumber * TrainingEmpProficencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_TrainingEmpProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogEmpRatingId;
@property (retain) tns1_CLogRating * CLogRating;
@property (retain) tns1_CLogRating * CLogRating1;
@property (retain) NSNumber * ClogEmpManagerId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ManagerId;
@property (retain) tns1_OrgSkill * OrgSkill;
@property (retain) NSNumber * ProficencyId;
@property (retain) NSNumber * ProficencyType;
@property (retain) tns1_TrainingCourseEmployee * TrainingCourseEmployee;
@property (retain) NSNumber * TrainingCourseEmployeeId;
@property (retain) NSNumber * TrainingEmpProficencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfTrainingEmpProficency : NSObject {
	
/* elements */
	NSMutableArray *TrainingEmpProficency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfTrainingEmpProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingEmpProficency:(tns1_TrainingEmpProficency *)toAdd;
@property (readonly) NSMutableArray * TrainingEmpProficency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_OrgSkill : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * Description;
	tns1_ArrayOfEmpProfileSkill * EmpProfileSkills;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * OrgSkillCode;
	NSNumber * OrgSkillId;
	tns1_OrgSkillType * OrgSkillType;
	NSNumber * OrgSkillTypeId;
	NSNumber * Priority;
	NSString * PriorityName;
	tns1_ArrayOfRecCandidateSkill * RecCandidateSkills;
	tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_OrgSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) tns1_ArrayOfEmpProfileSkill * EmpProfileSkills;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * OrgSkillCode;
@property (retain) NSNumber * OrgSkillId;
@property (retain) tns1_OrgSkillType * OrgSkillType;
@property (retain) NSNumber * OrgSkillTypeId;
@property (retain) NSNumber * Priority;
@property (retain) NSString * PriorityName;
@property (retain) tns1_ArrayOfRecCandidateSkill * RecCandidateSkills;
@property (retain) tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecCandidateSkill : NSObject {
	
/* elements */
	tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * DocumentRef;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_OrgSkill * OrgSkill;
	NSNumber * OrgSkillId;
	tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateId;
	NSNumber * RecCandidateSkillId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecCandidateSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DocumentRef;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_OrgSkill * OrgSkill;
@property (retain) NSNumber * OrgSkillId;
@property (retain) tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateId;
@property (retain) NSNumber * RecCandidateSkillId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecCandidateSkill : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecCandidateSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateSkill:(tns1_RecCandidateSkill *)toAdd;
@property (readonly) NSMutableArray * RecCandidateSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecCandidate : NSObject {
	
/* elements */
	NSMutableArray *RecCandidate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecCandidate *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidate:(tns1_RecCandidate *)toAdd;
@property (readonly) NSMutableArray * RecCandidate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecCandidateStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * RecCandidateStatusId;
	tns1_ArrayOfRecCandidate * RecCandidates;
	NSString * StatusName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecCandidateStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecCandidateStatusId;
@property (retain) tns1_ArrayOfRecCandidate * RecCandidates;
@property (retain) NSString * StatusName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecCandidateSupplier : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateSupplier;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecCandidateSupplier *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateSupplier:(tns1_RecCandidateSupplier *)toAdd;
@property (readonly) NSMutableArray * RecCandidateSupplier;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecCandidateTypeSupplier : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * RecCadidateTypeSupplierId;
	tns1_ArrayOfRecCandidateSupplier * RecCandidateSuppliers;
	NSString * TypeSupplierName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecCandidateTypeSupplier *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecCadidateTypeSupplierId;
@property (retain) tns1_ArrayOfRecCandidateSupplier * RecCandidateSuppliers;
@property (retain) NSString * TypeSupplierName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecCandidateSupplier : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * RecCadidateSupplierId;
	NSNumber * RecCadidateTypeSupplierId;
	tns1_RecCandidateTypeSupplier * RecCandidateTypeSupplier;
	tns1_ArrayOfRecCandidate * RecCandidates;
	NSString * SupplierAddress;
	NSString * SupplierEmail;
	NSString * SupplierName;
	NSString * SupplierPhone;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecCandidateSupplier *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecCadidateSupplierId;
@property (retain) NSNumber * RecCadidateTypeSupplierId;
@property (retain) tns1_RecCandidateTypeSupplier * RecCandidateTypeSupplier;
@property (retain) tns1_ArrayOfRecCandidate * RecCandidates;
@property (retain) NSString * SupplierAddress;
@property (retain) NSString * SupplierEmail;
@property (retain) NSString * SupplierName;
@property (retain) NSString * SupplierPhone;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecCandidate : NSObject {
	
/* elements */
	NSString * Address1;
	NSNumber * Address1Id;
	NSString * Address1Phone;
	NSString * Address2;
	NSNumber * Address2Id;
	NSString * Address2Phone;
	NSDate * ApplyDate;
	NSDate * BirthDay;
	NSString * BirthPlace;
	NSNumber * BirthPlaceId;
	NSString * CVFileType;
	NSString * CVUrl;
	USBoolean * CanBusinessTrip;
	USBoolean * CanOT;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * CulturalLevelId;
	NSString * CurrentSalary;
	NSNumber * EducationLevelId;
	NSString * Email;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * Ethnicity;
	NSNumber * EthnicityId;
	NSString * ExpectedSalary;
	NSString * FirstName;
	NSString * Forte;
	NSString * FullName;
	USBoolean * Gender;
	NSString * HomeTown;
	NSNumber * HomeTownId;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	NSString * ImageUrl;
	USBoolean * IsDeleted;
	USBoolean * IsManager;
	USBoolean * IsOnlyYearOfBirthday;
	NSString * LastName;
	NSString * MainLanguage;
	NSNumber * MainLanguageId;
	NSNumber * MajorId;
	NSNumber * MaritalStatusId;
	NSString * Mobile;
	NSDate * ModifiedDate;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSNumber * NumEmpManaged;
	NSString * OfficePhone;
	NSNumber * RecCadidateSupplierId;
	tns1_ArrayOfRecCandidateApplication * RecCandidateApplications;
	NSString * RecCandidateCode;
	tns1_ArrayOfRecCandidateCompetencyRating * RecCandidateCompetencyRatings;
	tns1_ArrayOfRecCandidateDegree * RecCandidateDegrees;
	tns1_ArrayOfRecCandidateExperience * RecCandidateExperiences;
	tns1_ArrayOfRecCandidateFamilyRelationship * RecCandidateFamilyRelationships;
	tns1_ArrayOfRecCandidateForeignLanguage * RecCandidateForeignLanguages;
	NSNumber * RecCandidateId;
	tns1_ArrayOfRecCandidateQualification * RecCandidateQualifications;
	tns1_ArrayOfRecCandidateSkill * RecCandidateSkills;
	tns1_RecCandidateStatu * RecCandidateStatu;
	NSNumber * RecCandidateStatusId;
	tns1_RecCandidateSupplier * RecCandidateSupplier;
	tns1_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
	NSString * Religion;
	NSNumber * ReligionId;
	NSString * Weaknesses;
	NSNumber * YearsExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecCandidate *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address1;
@property (retain) NSNumber * Address1Id;
@property (retain) NSString * Address1Phone;
@property (retain) NSString * Address2;
@property (retain) NSNumber * Address2Id;
@property (retain) NSString * Address2Phone;
@property (retain) NSDate * ApplyDate;
@property (retain) NSDate * BirthDay;
@property (retain) NSString * BirthPlace;
@property (retain) NSNumber * BirthPlaceId;
@property (retain) NSString * CVFileType;
@property (retain) NSString * CVUrl;
@property (retain) USBoolean * CanBusinessTrip;
@property (retain) USBoolean * CanOT;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * CulturalLevelId;
@property (retain) NSString * CurrentSalary;
@property (retain) NSNumber * EducationLevelId;
@property (retain) NSString * Email;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * Ethnicity;
@property (retain) NSNumber * EthnicityId;
@property (retain) NSString * ExpectedSalary;
@property (retain) NSString * FirstName;
@property (retain) NSString * Forte;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * HomeTown;
@property (retain) NSNumber * HomeTownId;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsManager;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) NSString * LastName;
@property (retain) NSString * MainLanguage;
@property (retain) NSNumber * MainLanguageId;
@property (retain) NSNumber * MajorId;
@property (retain) NSNumber * MaritalStatusId;
@property (retain) NSString * Mobile;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSNumber * NumEmpManaged;
@property (retain) NSString * OfficePhone;
@property (retain) NSNumber * RecCadidateSupplierId;
@property (retain) tns1_ArrayOfRecCandidateApplication * RecCandidateApplications;
@property (retain) NSString * RecCandidateCode;
@property (retain) tns1_ArrayOfRecCandidateCompetencyRating * RecCandidateCompetencyRatings;
@property (retain) tns1_ArrayOfRecCandidateDegree * RecCandidateDegrees;
@property (retain) tns1_ArrayOfRecCandidateExperience * RecCandidateExperiences;
@property (retain) tns1_ArrayOfRecCandidateFamilyRelationship * RecCandidateFamilyRelationships;
@property (retain) tns1_ArrayOfRecCandidateForeignLanguage * RecCandidateForeignLanguages;
@property (retain) NSNumber * RecCandidateId;
@property (retain) tns1_ArrayOfRecCandidateQualification * RecCandidateQualifications;
@property (retain) tns1_ArrayOfRecCandidateSkill * RecCandidateSkills;
@property (retain) tns1_RecCandidateStatu * RecCandidateStatu;
@property (retain) NSNumber * RecCandidateStatusId;
@property (retain) tns1_RecCandidateSupplier * RecCandidateSupplier;
@property (retain) tns1_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
@property (retain) NSString * Religion;
@property (retain) NSNumber * ReligionId;
@property (retain) NSString * Weaknesses;
@property (retain) NSNumber * YearsExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecCandidateProficencyLevelRating : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateProficencyLevelRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecCandidateProficencyLevelRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateProficencyLevelRating:(tns1_RecCandidateProficencyLevelRating *)toAdd;
@property (readonly) NSMutableArray * RecCandidateProficencyLevelRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecCandidateCompetencyRating : NSObject {
	
/* elements */
	tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * Mark;
	NSDate * ModifiedDate;
	NSString * Note;
	tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateCompetencyRatingId;
	NSNumber * RecCandidateId;
	tns1_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecCandidateCompetencyRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Mark;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateCompetencyRatingId;
@property (retain) NSNumber * RecCandidateId;
@property (retain) tns1_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecCandidateProficencyLevelRating : NSObject {
	
/* elements */
	tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * Mark;
	NSDate * ModifiedDate;
	NSString * Note;
	tns1_OrgProficencyLevel * OrgProficencyLevel;
	NSNumber * OrgProficencyLevelId;
	tns1_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
	tns1_RecCandidateCompetencyRating * RecCandidateCompetencyRating;
	NSNumber * RecCandidateCompetencyRatingId;
	NSNumber * RecCandidateProficencyLevelRatingId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecCandidateProficencyLevelRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Mark;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) tns1_OrgProficencyLevel * OrgProficencyLevel;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) tns1_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
@property (retain) tns1_RecCandidateCompetencyRating * RecCandidateCompetencyRating;
@property (retain) NSNumber * RecCandidateCompetencyRatingId;
@property (retain) NSNumber * RecCandidateProficencyLevelRatingId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecCanProficencyLevelDetailRating : NSObject {
	
/* elements */
	tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * Mark;
	NSDate * ModifiedDate;
	NSString * Note;
	tns1_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
	NSNumber * OrgProficencyLevelDetailId;
	tns1_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
	NSNumber * RecCanProficencyLevelDetailRatingId;
	tns1_RecCandidateProficencyLevelRating * RecCandidateProficencyLevelRating;
	NSNumber * RecCandidateProficencyLevelRatingId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecCanProficencyLevelDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Mark;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) tns1_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) tns1_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
@property (retain) NSNumber * RecCanProficencyLevelDetailRatingId;
@property (retain) tns1_RecCandidateProficencyLevelRating * RecCandidateProficencyLevelRating;
@property (retain) NSNumber * RecCandidateProficencyLevelRatingId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_RecCanProficencyDetailRating : NSObject {
	
/* elements */
	tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * Mark;
	NSDate * ModifiedDate;
	NSString * Note;
	tns1_OrgProficencyDetail * OrgProficencyDetail;
	NSNumber * OrgProficencyDetailId;
	NSNumber * RecCanProficencyDetailRatingId;
	tns1_RecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRating;
	NSNumber * RecCanProficencyLevelDetailRatingId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_RecCanProficencyDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Mark;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) tns1_OrgProficencyDetail * OrgProficencyDetail;
@property (retain) NSNumber * OrgProficencyDetailId;
@property (retain) NSNumber * RecCanProficencyDetailRatingId;
@property (retain) tns1_RecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRating;
@property (retain) NSNumber * RecCanProficencyLevelDetailRatingId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfRecCanProficencyDetailRating : NSObject {
	
/* elements */
	NSMutableArray *RecCanProficencyDetailRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfRecCanProficencyDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCanProficencyDetailRating:(tns1_RecCanProficencyDetailRating *)toAdd;
@property (readonly) NSMutableArray * RecCanProficencyDetailRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_OrgProficencyDetail : NSObject {
	
/* elements */
	tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgCompetencyTypeId;
	NSNumber * OrgProficencyDetailId;
	tns1_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
	NSNumber * OrgProficencyLevelDetailId;
	tns1_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_OrgProficencyDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgCompetencyTypeId;
@property (retain) NSNumber * OrgProficencyDetailId;
@property (retain) tns1_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) tns1_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfOrgProficencyDetail : NSObject {
	
/* elements */
	NSMutableArray *OrgProficencyDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfOrgProficencyDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgProficencyDetail:(tns1_OrgProficencyDetail *)toAdd;
@property (readonly) NSMutableArray * OrgProficencyDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfOrgProficencyLevelDetail : NSObject {
	
/* elements */
	NSMutableArray *OrgProficencyLevelDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfOrgProficencyLevelDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgProficencyLevelDetail:(tns1_OrgProficencyLevelDetail *)toAdd;
@property (readonly) NSMutableArray * OrgProficencyLevelDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_TrainingPlanProfiency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_OrgProficencyType * OrgProficencyType;
	NSNumber * OrgProficencyTypeId;
	NSNumber * ProficencyId;
	NSNumber * TrainingPlanProfiencyId;
	NSNumber * TrainingPlanRequestId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_TrainingPlanProfiency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_OrgProficencyType * OrgProficencyType;
@property (retain) NSNumber * OrgProficencyTypeId;
@property (retain) NSNumber * ProficencyId;
@property (retain) NSNumber * TrainingPlanProfiencyId;
@property (retain) NSNumber * TrainingPlanRequestId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfTrainingPlanProfiency : NSObject {
	
/* elements */
	NSMutableArray *TrainingPlanProfiency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfTrainingPlanProfiency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingPlanProfiency:(tns1_TrainingPlanProfiency *)toAdd;
@property (readonly) NSMutableArray * TrainingPlanProfiency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_OrgProficencyType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
	NSNumber * OrgProficencyTypeId;
	NSString * ProficencyTypeCode;
	NSString * ProficencyTypeName;
	tns1_ArrayOfTrainingPlanProfiency * TrainingPlanProfiencies;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_OrgProficencyType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
@property (retain) NSNumber * OrgProficencyTypeId;
@property (retain) NSString * ProficencyTypeCode;
@property (retain) NSString * ProficencyTypeName;
@property (retain) tns1_ArrayOfTrainingPlanProfiency * TrainingPlanProfiencies;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_OrgProficencyLevelDetail : NSObject {
	
/* elements */
	tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_ArrayOfOrgProficencyDetail * OrgProficencyDetails;
	tns1_OrgProficencyLevel * OrgProficencyLevel;
	NSNumber * OrgProficencyLevelDetailId;
	NSNumber * OrgProficencyLevelId;
	tns1_OrgProficencyType * OrgProficencyType;
	NSNumber * OrgProficencyTypeId;
	tns1_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
	NSString * Statement;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_OrgProficencyLevelDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_ArrayOfOrgProficencyDetail * OrgProficencyDetails;
@property (retain) tns1_OrgProficencyLevel * OrgProficencyLevel;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) tns1_OrgProficencyType * OrgProficencyType;
@property (retain) NSNumber * OrgProficencyTypeId;
@property (retain) tns1_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
@property (retain) NSString * Statement;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProficencyLevelDetailRating : NSObject {
	
/* elements */
	NSDate * AppraisalDate;
	tns1_CLogRating * CLogRating;
	NSDate * CompanyId;
	NSDate * CreatedDate;
	tns1_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
	NSNumber * EmpProficencyLevelDetailRatingId;
	tns1_EmpProficencyLevelRating * EmpProficencyLevelRating;
	NSNumber * EmpProficencyLevelRatingId;
	NSString * ManagerNote;
	NSNumber * ManagerRating;
	tns1_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
	NSNumber * OrgProficencyLevelDetailId;
	NSNumber * PeriodicallyAssessment;
	NSNumber * Score;
	NSString * SelfNote;
	NSNumber * SelfRatingId;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProficencyLevelDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AppraisalDate;
@property (retain) tns1_CLogRating * CLogRating;
@property (retain) NSDate * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
@property (retain) NSNumber * EmpProficencyLevelDetailRatingId;
@property (retain) tns1_EmpProficencyLevelRating * EmpProficencyLevelRating;
@property (retain) NSNumber * EmpProficencyLevelRatingId;
@property (retain) NSString * ManagerNote;
@property (retain) NSNumber * ManagerRating;
@property (retain) tns1_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) NSNumber * PeriodicallyAssessment;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfNote;
@property (retain) NSNumber * SelfRatingId;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProficencyDetailRating : NSObject {
	
/* elements */
	NSDate * AppraisalDate;
	tns1_CLogRating * CLogRating;
	NSDate * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpProficencyDetailRatingId;
	tns1_EmpProficencyLevelDetailRating * EmpProficencyLevelDetailRating;
	NSNumber * EmpProficencyLevelDetailRatingId;
	NSString * ManagerNote;
	NSNumber * ManagerRating;
	tns1_OrgProficencyDetail * OrgProficencyDetail;
	NSNumber * OrgProficencyDetailId;
	NSNumber * PeriodicallyAssessment;
	NSString * SelfNote;
	NSNumber * SelfRating;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProficencyDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AppraisalDate;
@property (retain) tns1_CLogRating * CLogRating;
@property (retain) NSDate * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProficencyDetailRatingId;
@property (retain) tns1_EmpProficencyLevelDetailRating * EmpProficencyLevelDetailRating;
@property (retain) NSNumber * EmpProficencyLevelDetailRatingId;
@property (retain) NSString * ManagerNote;
@property (retain) NSNumber * ManagerRating;
@property (retain) tns1_OrgProficencyDetail * OrgProficencyDetail;
@property (retain) NSNumber * OrgProficencyDetailId;
@property (retain) NSNumber * PeriodicallyAssessment;
@property (retain) NSString * SelfNote;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProficencyDetailRating : NSObject {
	
/* elements */
	NSMutableArray *EmpProficencyDetailRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProficencyDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProficencyDetailRating:(tns1_EmpProficencyDetailRating *)toAdd;
@property (readonly) NSMutableArray * EmpProficencyDetailRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileWorkingExperience : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	tns1_CLogRating * CLogRating;
	NSNumber * CompanyId;
	NSString * CompanyName;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileWorkingExperienceId;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	USBoolean * IsInternal;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * NotableAchievements;
	NSString * Note;
	NSNumber * OrgProjectTypeId;
	NSNumber * OrgTimeInChargeId;
	NSString * ProjectName;
	NSNumber * RatingId;
	NSNumber * RelateRowId;
	NSString * Responsibilities;
	NSString * Role;
	NSDate * StartDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileWorkingExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompanyName;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileWorkingExperienceId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInternal;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * NotableAchievements;
@property (retain) NSString * Note;
@property (retain) NSNumber * OrgProjectTypeId;
@property (retain) NSNumber * OrgTimeInChargeId;
@property (retain) NSString * ProjectName;
@property (retain) NSNumber * RatingId;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * Responsibilities;
@property (retain) NSString * Role;
@property (retain) NSDate * StartDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileWorkingExperience : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileWorkingExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileWorkingExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileWorkingExperience:(tns1_EmpProfileWorkingExperience *)toAdd;
@property (readonly) NSMutableArray * EmpProfileWorkingExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfLMSCourse : NSObject {
	
/* elements */
	NSMutableArray *LMSCourse;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfLMSCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourse:(tns1_LMSCourse *)toAdd;
@property (readonly) NSMutableArray * LMSCourse;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CLogCourseStatu : NSObject {
	
/* elements */
	NSNumber * CourseStatusId;
	USBoolean * IsDeleted;
	tns1_ArrayOfLMSCourse * LMSCourses;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CLogCourseStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CourseStatusId;
@property (retain) USBoolean * IsDeleted;
@property (retain) tns1_ArrayOfLMSCourse * LMSCourses;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_GPEmployeeScoreCardAppraisal : NSObject {
	
/* elements */
	NSNumber * AppraisalTime;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
	NSNumber * GPEmployeeScoreCardAppraisalId;
	NSNumber * GPEmployeeScoreCardId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * Score;
	NSString * SelfComment;
	NSNumber * SelfRating;
	NSNumber * SelfScore;
	NSNumber * Status;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_GPEmployeeScoreCardAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AppraisalTime;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
@property (retain) NSNumber * GPEmployeeScoreCardAppraisalId;
@property (retain) NSNumber * GPEmployeeScoreCardId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * SelfScore;
@property (retain) NSNumber * Status;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfGPEmployeeScoreCardAppraisal : NSObject {
	
/* elements */
	NSMutableArray *GPEmployeeScoreCardAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfGPEmployeeScoreCardAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPEmployeeScoreCardAppraisal:(tns1_GPEmployeeScoreCardAppraisal *)toAdd;
@property (readonly) NSMutableArray * GPEmployeeScoreCardAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_GPEmployeeScoreCardProgress : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
	NSNumber * GPEmployeeScoreCardId;
	NSNumber * GPEmployeeScoreCardProgressId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * NewValue;
	NSNumber * OldValue;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_GPEmployeeScoreCardProgress *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
@property (retain) NSNumber * GPEmployeeScoreCardId;
@property (retain) NSNumber * GPEmployeeScoreCardProgressId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NewValue;
@property (retain) NSNumber * OldValue;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfGPEmployeeScoreCardProgress : NSObject {
	
/* elements */
	NSMutableArray *GPEmployeeScoreCardProgress;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfGPEmployeeScoreCardProgress *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPEmployeeScoreCardProgress:(tns1_GPEmployeeScoreCardProgress *)toAdd;
@property (readonly) NSMutableArray * GPEmployeeScoreCardProgress;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_GPAdditionAppraisal : NSObject {
	
/* elements */
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_Employee * Employee;
	tns1_Employee * Employee1;
	NSNumber * EmployeeId;
	NSNumber * GPAdditionAppraisalId;
	tns1_GPExecutionPlan * GPExecutionPlan;
	NSNumber * GPExecutionPlanId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * SupervisorId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_GPAdditionAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_Employee * Employee;
@property (retain) tns1_Employee * Employee1;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * GPAdditionAppraisalId;
@property (retain) tns1_GPExecutionPlan * GPExecutionPlan;
@property (retain) NSNumber * GPExecutionPlanId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * SupervisorId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfGPAdditionAppraisal : NSObject {
	
/* elements */
	NSMutableArray *GPAdditionAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfGPAdditionAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPAdditionAppraisal:(tns1_GPAdditionAppraisal *)toAdd;
@property (readonly) NSMutableArray * GPAdditionAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_GPEmployeeWholePlanAppraisal : NSObject {
	
/* elements */
	NSNumber * AppraisalTime;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * GPEmployeeWholePlanAppraisalId;
	tns1_GPExecutionPlan * GPExecutionPlan;
	NSNumber * GPExecutionPlanId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * Result;
	NSNumber * Score;
	NSString * SelfComment;
	NSNumber * SelfRating;
	NSNumber * SelfScore;
	NSNumber * Status;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_GPEmployeeWholePlanAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AppraisalTime;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * GPEmployeeWholePlanAppraisalId;
@property (retain) tns1_GPExecutionPlan * GPExecutionPlan;
@property (retain) NSNumber * GPExecutionPlanId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * Result;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * SelfScore;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfGPEmployeeWholePlanAppraisal : NSObject {
	
/* elements */
	NSMutableArray *GPEmployeeWholePlanAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfGPEmployeeWholePlanAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPEmployeeWholePlanAppraisal:(tns1_GPEmployeeWholePlanAppraisal *)toAdd;
@property (readonly) NSMutableArray * GPEmployeeWholePlanAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_GPPerformanceYearEndResult : NSObject {
	
/* elements */
	NSString * ActivityName;
	NSNumber * Actual;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	tns1_GPExecutionPlan * GPExecutionPlan;
	NSNumber * GPPerformancePlanId;
	NSNumber * GPPerformanceYearEndResultId;
	USBoolean * HalfYearResult;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Progress;
	NSNumber * Rate;
	NSString * Result;
	NSNumber * Target;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_GPPerformanceYearEndResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ActivityName;
@property (retain) NSNumber * Actual;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) tns1_GPExecutionPlan * GPExecutionPlan;
@property (retain) NSNumber * GPPerformancePlanId;
@property (retain) NSNumber * GPPerformanceYearEndResultId;
@property (retain) USBoolean * HalfYearResult;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Progress;
@property (retain) NSNumber * Rate;
@property (retain) NSString * Result;
@property (retain) NSNumber * Target;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfGPPerformanceYearEndResult : NSObject {
	
/* elements */
	NSMutableArray *GPPerformanceYearEndResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfGPPerformanceYearEndResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPPerformanceYearEndResult:(tns1_GPPerformanceYearEndResult *)toAdd;
@property (readonly) NSMutableArray * GPPerformanceYearEndResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_GPExecutionPlan : NSObject {
	
/* elements */
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmployeeId;
	tns1_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals;
	tns1_ArrayOfGPEmployeeWholePlanAppraisal * GPEmployeeWholePlanAppraisals;
	tns1_ArrayOfGPExecutionPlanDetail * GPExecutionPlanDetails;
	NSNumber * GPExecutionPlanId;
	tns1_ArrayOfGPPerformanceYearEndResult * GPPerformanceYearEndResults;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * Period;
	NSNumber * Status;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_GPExecutionPlan *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmployeeId;
@property (retain) tns1_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals;
@property (retain) tns1_ArrayOfGPEmployeeWholePlanAppraisal * GPEmployeeWholePlanAppraisals;
@property (retain) tns1_ArrayOfGPExecutionPlanDetail * GPExecutionPlanDetails;
@property (retain) NSNumber * GPExecutionPlanId;
@property (retain) tns1_ArrayOfGPPerformanceYearEndResult * GPPerformanceYearEndResults;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * Period;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_GPExecutionPlanActivityAppraisal : NSObject {
	
/* elements */
	NSNumber * AppraisalTime;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_GPExecutionPlanActivity * GPExecutionPlanActivity;
	NSNumber * GPExecutionPlanActivityAppraisalId;
	NSNumber * GPExecutionPlanActivityId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * Score;
	NSString * SelfComment;
	NSNumber * SelfRating;
	NSNumber * SelfScore;
	NSNumber * Status;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_GPExecutionPlanActivityAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AppraisalTime;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_GPExecutionPlanActivity * GPExecutionPlanActivity;
@property (retain) NSNumber * GPExecutionPlanActivityAppraisalId;
@property (retain) NSNumber * GPExecutionPlanActivityId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * SelfScore;
@property (retain) NSNumber * Status;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfGPExecutionPlanActivityAppraisal : NSObject {
	
/* elements */
	NSMutableArray *GPExecutionPlanActivityAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfGPExecutionPlanActivityAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPExecutionPlanActivityAppraisal:(tns1_GPExecutionPlanActivityAppraisal *)toAdd;
@property (readonly) NSMutableArray * GPExecutionPlanActivityAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_GPPeriodicReport : NSObject {
	
/* elements */
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_GPExecutionPlanActivity * GPExecutionPlanActivity;
	NSNumber * GPExecutionPlanActivityId;
	NSNumber * GPPeriodicReportId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSDate * ReportDateTime;
	NSNumber * ReportTimes;
	NSNumber * Status;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_GPPeriodicReport *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_GPExecutionPlanActivity * GPExecutionPlanActivity;
@property (retain) NSNumber * GPExecutionPlanActivityId;
@property (retain) NSNumber * GPPeriodicReportId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * ReportDateTime;
@property (retain) NSNumber * ReportTimes;
@property (retain) NSNumber * Status;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfGPPeriodicReport : NSObject {
	
/* elements */
	NSMutableArray *GPPeriodicReport;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfGPPeriodicReport *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPPeriodicReport:(tns1_GPPeriodicReport *)toAdd;
@property (readonly) NSMutableArray * GPPeriodicReport;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_GPExecutionPlanActivity : NSObject {
	
/* elements */
	NSString * AchievementLevel;
	NSString * ActivityGroup;
	NSString * ActivityName;
	NSNumber * Budget;
	NSString * BudgetUnit;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * EndDate;
	tns1_ArrayOfGPExecutionPlanActivityAppraisal * GPExecutionPlanActivityAppraisals;
	NSNumber * GPExecutionPlanActivityId;
	tns1_GPExecutionPlanDetail * GPExecutionPlanDetail;
	NSNumber * GPExecutionPlanDetailId;
	tns1_ArrayOfGPPeriodicReport * GPPeriodicReports;
	USBoolean * IsDeleted;
	NSNumber * Measure;
	NSString * MeasureUnit;
	NSDate * ModifiedDate;
	NSDate * StartDate;
	NSString * Status;
	NSNumber * Target;
	NSString * TargetUnit;
	NSNumber * Variance;
	NSString * VarianceUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_GPExecutionPlanActivity *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AchievementLevel;
@property (retain) NSString * ActivityGroup;
@property (retain) NSString * ActivityName;
@property (retain) NSNumber * Budget;
@property (retain) NSString * BudgetUnit;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) tns1_ArrayOfGPExecutionPlanActivityAppraisal * GPExecutionPlanActivityAppraisals;
@property (retain) NSNumber * GPExecutionPlanActivityId;
@property (retain) tns1_GPExecutionPlanDetail * GPExecutionPlanDetail;
@property (retain) NSNumber * GPExecutionPlanDetailId;
@property (retain) tns1_ArrayOfGPPeriodicReport * GPPeriodicReports;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Measure;
@property (retain) NSString * MeasureUnit;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * StartDate;
@property (retain) NSString * Status;
@property (retain) NSNumber * Target;
@property (retain) NSString * TargetUnit;
@property (retain) NSNumber * Variance;
@property (retain) NSString * VarianceUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfGPExecutionPlanActivity : NSObject {
	
/* elements */
	NSMutableArray *GPExecutionPlanActivity;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfGPExecutionPlanActivity *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPExecutionPlanActivity:(tns1_GPExecutionPlanActivity *)toAdd;
@property (readonly) NSMutableArray * GPExecutionPlanActivity;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_GPExecutionPlanDetail : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_GPExecutionPlan * GPExecutionPlan;
	tns1_ArrayOfGPExecutionPlanActivity * GPExecutionPlanActivities;
	NSNumber * GPExecutionPlanDetailId;
	NSNumber * GPExecutionPlanId;
	tns1_GPIndividualYearlyObjective * GPIndividualYearlyObjective;
	NSNumber * GPIndividualYearlyObjectiveId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_GPExecutionPlanDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_GPExecutionPlan * GPExecutionPlan;
@property (retain) tns1_ArrayOfGPExecutionPlanActivity * GPExecutionPlanActivities;
@property (retain) NSNumber * GPExecutionPlanDetailId;
@property (retain) NSNumber * GPExecutionPlanId;
@property (retain) tns1_GPIndividualYearlyObjective * GPIndividualYearlyObjective;
@property (retain) NSNumber * GPIndividualYearlyObjectiveId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfGPExecutionPlanDetail : NSObject {
	
/* elements */
	NSMutableArray *GPExecutionPlanDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfGPExecutionPlanDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPExecutionPlanDetail:(tns1_GPExecutionPlanDetail *)toAdd;
@property (readonly) NSMutableArray * GPExecutionPlanDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_GPIndividualYearlyObjectiveAppraisal : NSObject {
	
/* elements */
	NSNumber * AppraisalTime;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_GPIndividualYearlyObjective * GPIndividualYearlyObjective;
	NSNumber * GPIndividualYearlyObjectiveAppraisalId;
	NSNumber * GPIndividualYearlyObjectiveId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * Score;
	NSString * SelfComment;
	NSNumber * SelfRating;
	NSNumber * SelfScore;
	NSNumber * Status;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_GPIndividualYearlyObjectiveAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AppraisalTime;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_GPIndividualYearlyObjective * GPIndividualYearlyObjective;
@property (retain) NSNumber * GPIndividualYearlyObjectiveAppraisalId;
@property (retain) NSNumber * GPIndividualYearlyObjectiveId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * SelfScore;
@property (retain) NSNumber * Status;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfGPIndividualYearlyObjectiveAppraisal : NSObject {
	
/* elements */
	NSMutableArray *GPIndividualYearlyObjectiveAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfGPIndividualYearlyObjectiveAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPIndividualYearlyObjectiveAppraisal:(tns1_GPIndividualYearlyObjectiveAppraisal *)toAdd;
@property (readonly) NSMutableArray * GPIndividualYearlyObjectiveAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_GPPerspectiveValueDetail : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_GPPerspectiveValue * GPPerspectiveValue;
	tns1_GPPerspectiveValue * GPPerspectiveValue1;
	NSNumber * GPPerspectiveValueDetailId;
	NSNumber * GPStrategyMapObjectId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * PerspectiveValueDestinationId;
	NSNumber * PerspectiveValueSourceId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_GPPerspectiveValueDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_GPPerspectiveValue * GPPerspectiveValue;
@property (retain) tns1_GPPerspectiveValue * GPPerspectiveValue1;
@property (retain) NSNumber * GPPerspectiveValueDetailId;
@property (retain) NSNumber * GPStrategyMapObjectId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * PerspectiveValueDestinationId;
@property (retain) NSNumber * PerspectiveValueSourceId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfGPPerspectiveValueDetail : NSObject {
	
/* elements */
	NSMutableArray *GPPerspectiveValueDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfGPPerspectiveValueDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPPerspectiveValueDetail:(tns1_GPPerspectiveValueDetail *)toAdd;
@property (readonly) NSMutableArray * GPPerspectiveValueDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_GPPerspectiveValue : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	tns1_ArrayOfGPPerspectiveValueDetail * GPPerspectiveValueDetails;
	tns1_ArrayOfGPPerspectiveValueDetail * GPPerspectiveValueDetails1;
	NSNumber * GPPerspectiveValueId;
	tns1_GPStrategyMapObject * GPStrategyMapObject;
	NSNumber * GPStrategyMapObjectId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * PositionX;
	NSNumber * PositionY;
	NSString * ValueType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_GPPerspectiveValue *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) tns1_ArrayOfGPPerspectiveValueDetail * GPPerspectiveValueDetails;
@property (retain) tns1_ArrayOfGPPerspectiveValueDetail * GPPerspectiveValueDetails1;
@property (retain) NSNumber * GPPerspectiveValueId;
@property (retain) tns1_GPStrategyMapObject * GPStrategyMapObject;
@property (retain) NSNumber * GPStrategyMapObjectId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * PositionX;
@property (retain) NSNumber * PositionY;
@property (retain) NSString * ValueType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfGPPerspectiveValue : NSObject {
	
/* elements */
	NSMutableArray *GPPerspectiveValue;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfGPPerspectiveValue *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPPerspectiveValue:(tns1_GPPerspectiveValue *)toAdd;
@property (readonly) NSMutableArray * GPPerspectiveValue;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfGPCompanyYearlyObjective : NSObject {
	
/* elements */
	NSMutableArray *GPCompanyYearlyObjective;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfGPCompanyYearlyObjective *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPCompanyYearlyObjective:(tns1_GPCompanyYearlyObjective *)toAdd;
@property (readonly) NSMutableArray * GPCompanyYearlyObjective;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfGPStrategyMapObject : NSObject {
	
/* elements */
	NSMutableArray *GPStrategyMapObject;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfGPStrategyMapObject *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPStrategyMapObject:(tns1_GPStrategyMapObject *)toAdd;
@property (readonly) NSMutableArray * GPStrategyMapObject;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_GPStrategy : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EndYear;
	tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
	NSNumber * GPStrategyId;
	tns1_ArrayOfGPStrategyMapObject * GPStrategyMapObjects;
	USBoolean * IsActived;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * StartYear;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_GPStrategy *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EndYear;
@property (retain) tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
@property (retain) NSNumber * GPStrategyId;
@property (retain) tns1_ArrayOfGPStrategyMapObject * GPStrategyMapObjects;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * StartYear;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_GPStrategyMapObject : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	tns1_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails;
	tns1_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
	tns1_ArrayOfGPPerspectiveValue * GPPerspectiveValues;
	tns1_GPStrategy * GPStrategy;
	NSNumber * GPStrategyId;
	NSNumber * GPStrategyMapObjectId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_GPStrategyMapObject *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) tns1_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails;
@property (retain) tns1_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
@property (retain) tns1_ArrayOfGPPerspectiveValue * GPPerspectiveValues;
@property (retain) tns1_GPStrategy * GPStrategy;
@property (retain) NSNumber * GPStrategyId;
@property (retain) NSNumber * GPStrategyMapObjectId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_GPCompanyStrategicGoalDetail : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_GPCompanyStrategicGoal * GPCompanyStrategicGoal;
	tns1_GPCompanyStrategicGoal * GPCompanyStrategicGoal1;
	NSNumber * GPCompanyStrategicGoalDestinationId;
	NSNumber * GPCompanyStrategicGoalDetailId;
	NSNumber * GPCompanyStrategicGoalSourceId;
	tns1_GPStrategyMapObject * GPStrategyMapObject;
	NSNumber * GPStrategyMapObjectId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_GPCompanyStrategicGoalDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_GPCompanyStrategicGoal * GPCompanyStrategicGoal;
@property (retain) tns1_GPCompanyStrategicGoal * GPCompanyStrategicGoal1;
@property (retain) NSNumber * GPCompanyStrategicGoalDestinationId;
@property (retain) NSNumber * GPCompanyStrategicGoalDetailId;
@property (retain) NSNumber * GPCompanyStrategicGoalSourceId;
@property (retain) tns1_GPStrategyMapObject * GPStrategyMapObject;
@property (retain) NSNumber * GPStrategyMapObjectId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfGPCompanyStrategicGoalDetail : NSObject {
	
/* elements */
	NSMutableArray *GPCompanyStrategicGoalDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfGPCompanyStrategicGoalDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPCompanyStrategicGoalDetail:(tns1_GPCompanyStrategicGoalDetail *)toAdd;
@property (readonly) NSMutableArray * GPCompanyStrategicGoalDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_GPCompanyStrategicGoal : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_Employee * Employee;
	tns1_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails;
	tns1_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails1;
	NSNumber * GPCompanyStrategicGoalId;
	tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
	tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	NSNumber * GPStrategyId;
	tns1_GPStrategyMapObject * GPStrategyMapObject;
	NSNumber * GPStrategyMapObjectId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OwnerId;
	NSString * PeriodStrategy;
	NSString * SmartAttainableGoal;
	NSString * SmartMeasureGoal;
	NSString * SmartRelevantGoal;
	NSString * SmartSpecificGoal;
	NSString * SmartTimeGoal;
	NSString * Statement;
	NSNumber * YearBegin;
	NSNumber * YearEnd;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_GPCompanyStrategicGoal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_Employee * Employee;
@property (retain) tns1_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails;
@property (retain) tns1_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails1;
@property (retain) NSNumber * GPCompanyStrategicGoalId;
@property (retain) tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
@property (retain) tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) NSNumber * GPStrategyId;
@property (retain) tns1_GPStrategyMapObject * GPStrategyMapObject;
@property (retain) NSNumber * GPStrategyMapObjectId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OwnerId;
@property (retain) NSString * PeriodStrategy;
@property (retain) NSString * SmartAttainableGoal;
@property (retain) NSString * SmartMeasureGoal;
@property (retain) NSString * SmartRelevantGoal;
@property (retain) NSString * SmartSpecificGoal;
@property (retain) NSString * SmartTimeGoal;
@property (retain) NSString * Statement;
@property (retain) NSNumber * YearBegin;
@property (retain) NSNumber * YearEnd;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfGPCompanyStrategicGoal : NSObject {
	
/* elements */
	NSMutableArray *GPCompanyStrategicGoal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfGPCompanyStrategicGoal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPCompanyStrategicGoal:(tns1_GPCompanyStrategicGoal *)toAdd;
@property (readonly) NSMutableArray * GPCompanyStrategicGoal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_GPObjectiveInitiative : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_Employee * Employee;
	tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
	NSNumber * GPObjectiveInitiativeId;
	tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	USBoolean * IsDeleted;
	tns1_ArrayOfLMSCourse * LMSCourses;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * OwnerId;
	NSNumber * Progress;
	NSString * Purpose;
	NSNumber * YearBegin;
	NSNumber * YearEnd;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_GPObjectiveInitiative *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_Employee * Employee;
@property (retain) tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
@property (retain) NSNumber * GPObjectiveInitiativeId;
@property (retain) tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) USBoolean * IsDeleted;
@property (retain) tns1_ArrayOfLMSCourse * LMSCourses;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * OwnerId;
@property (retain) NSNumber * Progress;
@property (retain) NSString * Purpose;
@property (retain) NSNumber * YearBegin;
@property (retain) NSNumber * YearEnd;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfGPObjectiveInitiative : NSObject {
	
/* elements */
	NSMutableArray *GPObjectiveInitiative;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfGPObjectiveInitiative *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPObjectiveInitiative:(tns1_GPObjectiveInitiative *)toAdd;
@property (readonly) NSMutableArray * GPObjectiveInitiative;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_GPPerspectiveMeasure : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
	tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
	tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	NSNumber * GPPerspectiveMeasureId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_GPPerspectiveMeasure *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
@property (retain) tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
@property (retain) tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) NSNumber * GPPerspectiveMeasureId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfGPPerspectiveMeasure : NSObject {
	
/* elements */
	NSMutableArray *GPPerspectiveMeasure;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfGPPerspectiveMeasure *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPPerspectiveMeasure:(tns1_GPPerspectiveMeasure *)toAdd;
@property (readonly) NSMutableArray * GPPerspectiveMeasure;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_GPPerspective : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
	tns1_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
	tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
	tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
	tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
	tns1_ArrayOfGPObjectiveInitiative * GPObjectiveInitiatives;
	NSNumber * GPPerspectiveId;
	tns1_ArrayOfGPPerspectiveMeasure * GPPerspectiveMeasures;
	tns1_ArrayOfGPPerspectiveValue * GPPerspectiveValues;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_GPPerspective *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
@property (retain) tns1_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
@property (retain) tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
@property (retain) tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
@property (retain) tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
@property (retain) tns1_ArrayOfGPObjectiveInitiative * GPObjectiveInitiatives;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) tns1_ArrayOfGPPerspectiveMeasure * GPPerspectiveMeasures;
@property (retain) tns1_ArrayOfGPPerspectiveValue * GPPerspectiveValues;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_GPIndividualYearlyObjective : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_Employee * Employee;
	tns1_Employee * Employee1;
	NSNumber * EmployeeId;
	tns1_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
	NSNumber * GPCompanyYearlyObjectiveId;
	tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
	NSNumber * GPEmployeeScoreCardId;
	tns1_ArrayOfGPExecutionPlanDetail * GPExecutionPlanDetails;
	tns1_ArrayOfGPIndividualYearlyObjectiveAppraisal * GPIndividualYearlyObjectiveAppraisals;
	NSNumber * GPIndividualYearlyObjectiveId;
	tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * ObjectiveType;
	NSNumber * Owner;
	NSString * SmartAttainableGoal;
	NSString * SmartMeasureGoal;
	NSString * SmartRelevantGoal;
	NSString * SmartSpecificGoal;
	NSString * SmartTimeGoal;
	NSString * Statement;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_GPIndividualYearlyObjective *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_Employee * Employee;
@property (retain) tns1_Employee * Employee1;
@property (retain) NSNumber * EmployeeId;
@property (retain) tns1_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
@property (retain) NSNumber * GPCompanyYearlyObjectiveId;
@property (retain) tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
@property (retain) NSNumber * GPEmployeeScoreCardId;
@property (retain) tns1_ArrayOfGPExecutionPlanDetail * GPExecutionPlanDetails;
@property (retain) tns1_ArrayOfGPIndividualYearlyObjectiveAppraisal * GPIndividualYearlyObjectiveAppraisals;
@property (retain) NSNumber * GPIndividualYearlyObjectiveId;
@property (retain) tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * ObjectiveType;
@property (retain) NSNumber * Owner;
@property (retain) NSString * SmartAttainableGoal;
@property (retain) NSString * SmartMeasureGoal;
@property (retain) NSString * SmartRelevantGoal;
@property (retain) NSString * SmartSpecificGoal;
@property (retain) NSString * SmartTimeGoal;
@property (retain) NSString * Statement;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfGPIndividualYearlyObjective : NSObject {
	
/* elements */
	NSMutableArray *GPIndividualYearlyObjective;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfGPIndividualYearlyObjective *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPIndividualYearlyObjective:(tns1_GPIndividualYearlyObjective *)toAdd;
@property (readonly) NSMutableArray * GPIndividualYearlyObjective;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_GPPerformanceExecutionReport : NSObject {
	
/* elements */
	NSString * ActivityName;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
	NSNumber * GPEmployeeScoreCardId;
	NSNumber * GPPerformanceExecutionReportId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Progress;
	NSString * ReportNumber;
	NSDate * ReportTime;
	NSNumber * Reporter;
	NSNumber * Status;
	NSNumber * Target;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_GPPerformanceExecutionReport *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ActivityName;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
@property (retain) NSNumber * GPEmployeeScoreCardId;
@property (retain) NSNumber * GPPerformanceExecutionReportId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Progress;
@property (retain) NSString * ReportNumber;
@property (retain) NSDate * ReportTime;
@property (retain) NSNumber * Reporter;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * Target;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_GPEmployeeScoreCard : NSObject {
	
/* elements */
	NSNumber * Actual;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	tns1_GPCompanyScoreCard * GPCompanyScoreCard;
	NSNumber * GPCompanyScoreCardId;
	tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCard1;
	tns1_GPEmployeeScoreCard * GPEmployeeScoreCard2;
	tns1_ArrayOfGPEmployeeScoreCardAppraisal * GPEmployeeScoreCardAppraisals;
	NSNumber * GPEmployeeScoreCardId;
	tns1_ArrayOfGPEmployeeScoreCardProgress * GPEmployeeScoreCardProgresses;
	tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
	tns1_GPPerformanceExecutionReport * GPPerformanceExecutionReport;
	tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	tns1_GPPerspectiveMeasure * GPPerspectiveMeasure;
	NSNumber * GPPerspectiveMeasureId;
	USBoolean * IsDeleted;
	NSString * Metric;
	NSDate * ModifiedDate;
	NSNumber * ParentId;
	NSNumber * PercentageOwnership;
	NSNumber * Progress;
	NSString * ScoreCardGroup;
	NSString * ScoreCardSubGroup;
	NSString * Statement;
	NSNumber * Status;
	NSNumber * Target;
	NSNumber * Weight;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_GPEmployeeScoreCard *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Actual;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) tns1_GPCompanyScoreCard * GPCompanyScoreCard;
@property (retain) NSNumber * GPCompanyScoreCardId;
@property (retain) tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCard1;
@property (retain) tns1_GPEmployeeScoreCard * GPEmployeeScoreCard2;
@property (retain) tns1_ArrayOfGPEmployeeScoreCardAppraisal * GPEmployeeScoreCardAppraisals;
@property (retain) NSNumber * GPEmployeeScoreCardId;
@property (retain) tns1_ArrayOfGPEmployeeScoreCardProgress * GPEmployeeScoreCardProgresses;
@property (retain) tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
@property (retain) tns1_GPPerformanceExecutionReport * GPPerformanceExecutionReport;
@property (retain) tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) tns1_GPPerspectiveMeasure * GPPerspectiveMeasure;
@property (retain) NSNumber * GPPerspectiveMeasureId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Metric;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * ParentId;
@property (retain) NSNumber * PercentageOwnership;
@property (retain) NSNumber * Progress;
@property (retain) NSString * ScoreCardGroup;
@property (retain) NSString * ScoreCardSubGroup;
@property (retain) NSString * Statement;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * Target;
@property (retain) NSNumber * Weight;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfGPEmployeeScoreCard : NSObject {
	
/* elements */
	NSMutableArray *GPEmployeeScoreCard;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfGPEmployeeScoreCard *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPEmployeeScoreCard:(tns1_GPEmployeeScoreCard *)toAdd;
@property (readonly) NSMutableArray * GPEmployeeScoreCard;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_GPCompanyScoreCard : NSObject {
	
/* elements */
	NSNumber * Actual;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_Employee * Employee;
	NSNumber * GPCompanyScoreCardId;
	tns1_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
	NSNumber * GPCompanyYearlyObjectiveId;
	tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
	tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	tns1_GPPerspectiveMeasure * GPPerspectiveMeasure;
	NSNumber * GPPerspectiveMeasureId;
	USBoolean * IsDeleted;
	NSString * Metric;
	NSDate * ModifiedDate;
	NSNumber * Progress;
	NSString * ReportingRythm;
	NSNumber * ScoreCardCreator;
	NSString * Statement;
	NSNumber * Status;
	NSNumber * Target;
	NSNumber * Weight;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_GPCompanyScoreCard *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Actual;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * GPCompanyScoreCardId;
@property (retain) tns1_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
@property (retain) NSNumber * GPCompanyYearlyObjectiveId;
@property (retain) tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
@property (retain) tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) tns1_GPPerspectiveMeasure * GPPerspectiveMeasure;
@property (retain) NSNumber * GPPerspectiveMeasureId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Metric;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Progress;
@property (retain) NSString * ReportingRythm;
@property (retain) NSNumber * ScoreCardCreator;
@property (retain) NSString * Statement;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * Target;
@property (retain) NSNumber * Weight;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfGPCompanyScoreCard : NSObject {
	
/* elements */
	NSMutableArray *GPCompanyScoreCard;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfGPCompanyScoreCard *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPCompanyScoreCard:(tns1_GPCompanyScoreCard *)toAdd;
@property (readonly) NSMutableArray * GPCompanyScoreCard;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_GPCompanyYearlyObjective : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_Employee * Employee;
	tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
	tns1_GPCompanyStrategicGoal * GPCompanyStrategicGoal;
	NSNumber * GPCompanyStrategicGoalId;
	NSNumber * GPCompanyYearlyObjectiveId;
	tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
	tns1_GPObjectiveInitiative * GPObjectiveInitiative;
	NSNumber * GPObjectiveInitiativeId;
	tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	tns1_GPStrategy * GPStrategy;
	NSNumber * GPStrategyId;
	USBoolean * IsDeleted;
	tns1_ArrayOfLMSCourse * LMSCourses;
	NSDate * ModifiedDate;
	NSNumber * Owner;
	NSString * SmartAttainableGoal;
	NSString * SmartMeasureGoal;
	NSString * SmartRelevantGoal;
	NSString * SmartSpecificGoal;
	NSString * SmartTimeGoal;
	NSString * Statement;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_GPCompanyYearlyObjective *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_Employee * Employee;
@property (retain) tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
@property (retain) tns1_GPCompanyStrategicGoal * GPCompanyStrategicGoal;
@property (retain) NSNumber * GPCompanyStrategicGoalId;
@property (retain) NSNumber * GPCompanyYearlyObjectiveId;
@property (retain) tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
@property (retain) tns1_GPObjectiveInitiative * GPObjectiveInitiative;
@property (retain) NSNumber * GPObjectiveInitiativeId;
@property (retain) tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) tns1_GPStrategy * GPStrategy;
@property (retain) NSNumber * GPStrategyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) tns1_ArrayOfLMSCourse * LMSCourses;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Owner;
@property (retain) NSString * SmartAttainableGoal;
@property (retain) NSString * SmartMeasureGoal;
@property (retain) NSString * SmartRelevantGoal;
@property (retain) NSString * SmartSpecificGoal;
@property (retain) NSString * SmartTimeGoal;
@property (retain) NSString * Statement;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_LMSCourseCompetency : NSObject {
	
/* elements */
	tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompetencyName;
	NSNumber * CompetencyTypeId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	tns1_LMSCourse * LMSCourse;
	NSNumber * LMSCourseCompetencyId;
	NSNumber * LMSCourseId;
	NSDate * ModifiedDate;
	tns1_OrgCompetency * OrgCompetency;
	NSNumber * OrgCompetencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_LMSCourseCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompetencyName;
@property (retain) NSNumber * CompetencyTypeId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) tns1_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseCompetencyId;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_OrgCompetency * OrgCompetency;
@property (retain) NSNumber * OrgCompetencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfLMSCourseCompetency : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfLMSCourseCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseCompetency:(tns1_LMSCourseCompetency *)toAdd;
@property (readonly) NSMutableArray * LMSCourseCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_LMSCourseRequiredCompetency : NSObject {
	
/* elements */
	tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompetencyName;
	NSString * CompetencyType;
	NSNumber * CourseRequiredCompetencyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	tns1_LMSCourse * LMSCourse;
	NSNumber * LMSCourseId;
	NSDate * ModifiedDate;
	tns1_OrgCompetency * OrgCompetency;
	NSNumber * OrgCompetencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_LMSCourseRequiredCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompetencyName;
@property (retain) NSString * CompetencyType;
@property (retain) NSNumber * CourseRequiredCompetencyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) tns1_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_OrgCompetency * OrgCompetency;
@property (retain) NSNumber * OrgCompetencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfLMSCourseRequiredCompetency : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseRequiredCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfLMSCourseRequiredCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseRequiredCompetency:(tns1_LMSCourseRequiredCompetency *)toAdd;
@property (readonly) NSMutableArray * LMSCourseRequiredCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfOrgCompetency : NSObject {
	
/* elements */
	NSMutableArray *OrgCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfOrgCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgCompetency:(tns1_OrgCompetency *)toAdd;
@property (readonly) NSMutableArray * OrgCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_OrgCompetencyGroup : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * GroupName;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_ArrayOfOrgCompetency * OrgCompetencies;
	NSNumber * OrgCompetencyGroupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_OrgCompetencyGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * GroupName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_ArrayOfOrgCompetency * OrgCompetencies;
@property (retain) NSNumber * OrgCompetencyGroupId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_OrgJobSpecificCompetency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_OrgCompetency * OrgCompetency;
	NSNumber * OrgCompetencyId;
	tns1_OrgJob * OrgJob;
	NSNumber * OrgJobId;
	NSNumber * OrgJobSpecificCompetencyId;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_OrgJobSpecificCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_OrgCompetency * OrgCompetency;
@property (retain) NSNumber * OrgCompetencyId;
@property (retain) tns1_OrgJob * OrgJob;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSNumber * OrgJobSpecificCompetencyId;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfOrgJobSpecificCompetency : NSObject {
	
/* elements */
	NSMutableArray *OrgJobSpecificCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfOrgJobSpecificCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgJobSpecificCompetency:(tns1_OrgJobSpecificCompetency *)toAdd;
@property (readonly) NSMutableArray * OrgJobSpecificCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfOrgProficencyLevel : NSObject {
	
/* elements */
	NSMutableArray *OrgProficencyLevel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfOrgProficencyLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgProficencyLevel:(tns1_OrgProficencyLevel *)toAdd;
@property (readonly) NSMutableArray * OrgProficencyLevel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_OrgCompetency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Definition;
	USBoolean * IsCore;
	USBoolean * IsDeleted;
	tns1_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
	tns1_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
	tns1_ArrayOfLMSTopicCompetency * LMSTopicCompetencies;
	NSDate * ModifiedDate;
	NSString * Name;
	tns1_OrgCompetencyGroup * OrgCompetencyGroup;
	NSNumber * OrgCompetencyGroupId;
	NSNumber * OrgCompetencyId;
	tns1_ArrayOfOrgJobSpecificCompetency * OrgJobSpecificCompetencies;
	tns1_ArrayOfOrgProficencyLevel * OrgProficencyLevels;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_OrgCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Definition;
@property (retain) USBoolean * IsCore;
@property (retain) USBoolean * IsDeleted;
@property (retain) tns1_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
@property (retain) tns1_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
@property (retain) tns1_ArrayOfLMSTopicCompetency * LMSTopicCompetencies;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) tns1_OrgCompetencyGroup * OrgCompetencyGroup;
@property (retain) NSNumber * OrgCompetencyGroupId;
@property (retain) NSNumber * OrgCompetencyId;
@property (retain) tns1_ArrayOfOrgJobSpecificCompetency * OrgJobSpecificCompetencies;
@property (retain) tns1_ArrayOfOrgProficencyLevel * OrgProficencyLevels;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_LMSTopicCompetency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	tns1_LMSContentTopic * LMSContentTopic;
	NSNumber * LMSContentTopicId;
	NSNumber * LMSTopicCompetencyId;
	NSDate * ModifiedDate;
	tns1_OrgCompetency * OrgCompetency;
	NSNumber * OrgCompetencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_LMSTopicCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) tns1_LMSContentTopic * LMSContentTopic;
@property (retain) NSNumber * LMSContentTopicId;
@property (retain) NSNumber * LMSTopicCompetencyId;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_OrgCompetency * OrgCompetency;
@property (retain) NSNumber * OrgCompetencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfLMSTopicCompetency : NSObject {
	
/* elements */
	NSMutableArray *LMSTopicCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfLMSTopicCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSTopicCompetency:(tns1_LMSTopicCompetency *)toAdd;
@property (readonly) NSMutableArray * LMSTopicCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfLMSContentTopic : NSObject {
	
/* elements */
	NSMutableArray *LMSContentTopic;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfLMSContentTopic *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSContentTopic:(tns1_LMSContentTopic *)toAdd;
@property (readonly) NSMutableArray * LMSContentTopic;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_LMSTopicGroup : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	tns1_ArrayOfLMSContentTopic * LMSContentTopics;
	NSNumber * LMSTopicGroupId;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_LMSTopicGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) tns1_ArrayOfLMSContentTopic * LMSContentTopics;
@property (retain) NSNumber * LMSTopicGroupId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_LMSContentTopic : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * LMSContentTopicId;
	tns1_ArrayOfLMSCourse * LMSCourses;
	tns1_ArrayOfLMSTopicCompetency * LMSTopicCompetencies;
	tns1_LMSTopicGroup * LMSTopicGroup;
	NSNumber * LMSTopicGroupId;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_LMSContentTopic *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * LMSContentTopicId;
@property (retain) tns1_ArrayOfLMSCourse * LMSCourses;
@property (retain) tns1_ArrayOfLMSTopicCompetency * LMSTopicCompetencies;
@property (retain) tns1_LMSTopicGroup * LMSTopicGroup;
@property (retain) NSNumber * LMSTopicGroupId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CLogLMSContentType : NSObject {
	
/* elements */
	NSNumber * CLogLMSContentTypeId;
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	tns1_ArrayOfLMSCourseContent * LMSCourseContents;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CLogLMSContentType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogLMSContentTypeId;
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) tns1_ArrayOfLMSCourseContent * LMSCourseContents;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_LMSCourseContent : NSObject {
	
/* elements */
	tns1_CLogLMSContentType * CLogLMSContentType;
	NSNumber * CLogLMSContentType_CLogLMSContentTypeId;
	tns1_LMSCourse * LMSCourse;
	tns1_LMSCourseContent * LMSCourseContent1;
	NSNumber * LMSCourseContent1_LMSCourseContentId;
	NSNumber * LMSCourseContentId;
	tns1_LMSCourseContent * LMSCourseContents1;
	NSNumber * LMSCourse_LMSCourseId;
	NSString * Name;
	NSString * Statement;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_LMSCourseContent *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_CLogLMSContentType * CLogLMSContentType;
@property (retain) NSNumber * CLogLMSContentType_CLogLMSContentTypeId;
@property (retain) tns1_LMSCourse * LMSCourse;
@property (retain) tns1_LMSCourseContent * LMSCourseContent1;
@property (retain) NSNumber * LMSCourseContent1_LMSCourseContentId;
@property (retain) NSNumber * LMSCourseContentId;
@property (retain) tns1_LMSCourseContent * LMSCourseContents1;
@property (retain) NSNumber * LMSCourse_LMSCourseId;
@property (retain) NSString * Name;
@property (retain) NSString * Statement;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfLMSCourseContent : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseContent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfLMSCourseContent *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseContent:(tns1_LMSCourseContent *)toAdd;
@property (readonly) NSMutableArray * LMSCourseContent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_LMSCourseDetail : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CourseContentTypeId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	tns1_LMSCourse * LMSCourse;
	NSNumber * LMSCourseDetailId;
	NSNumber * LMSCourseId;
	NSNumber * Level;
	NSDate * ModifiedDate;
	NSString * Resource;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_LMSCourseDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CourseContentTypeId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) tns1_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseDetailId;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSNumber * Level;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Resource;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfLMSCourseDetail : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfLMSCourseDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseDetail:(tns1_LMSCourseDetail *)toAdd;
@property (readonly) NSMutableArray * LMSCourseDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfOrgUnit : NSObject {
	
/* elements */
	NSMutableArray *OrgUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfOrgUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgUnit:(tns1_OrgUnit *)toAdd;
@property (readonly) NSMutableArray * OrgUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_OrgUnit : NSObject {
	
/* elements */
	NSString * Address;
	tns1_Address * Address1;
	NSNumber * AddressId;
	NSString * CommissionDesc;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	NSString * FunctionDesc;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	tns1_ArrayOfLMSCourseProgressUnit * LMSCourseProgressUnits;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
	tns1_ArrayOfOrgUnit * OrgUnit1;
	tns1_OrgUnit * OrgUnit2;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSNumber * OrgUnitLevelId;
	NSString * ParentCode;
	NSNumber * ParentId;
	NSNumber * TotalEmployees;
	NSString * WorkLocation;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_OrgUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) tns1_Address * Address1;
@property (retain) NSNumber * AddressId;
@property (retain) NSString * CommissionDesc;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) NSString * FunctionDesc;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) tns1_ArrayOfLMSCourseProgressUnit * LMSCourseProgressUnits;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) tns1_ArrayOfOrgUnit * OrgUnit1;
@property (retain) tns1_OrgUnit * OrgUnit2;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * OrgUnitLevelId;
@property (retain) NSString * ParentCode;
@property (retain) NSNumber * ParentId;
@property (retain) NSNumber * TotalEmployees;
@property (retain) NSString * WorkLocation;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_LMSCourseProgressUnit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	tns1_LMSCourse * LMSCourse;
	NSNumber * LMSCourseId;
	NSNumber * LMSCourseProgressUnitId;
	NSDate * ModifiedDate;
	NSNumber * NumOfEmpFinish;
	tns1_OrgUnit * OrgUnit;
	NSNumber * OrgUnitId;
	USBoolean * Status;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_LMSCourseProgressUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) tns1_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSNumber * LMSCourseProgressUnitId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NumOfEmpFinish;
@property (retain) tns1_OrgUnit * OrgUnit;
@property (retain) NSNumber * OrgUnitId;
@property (retain) USBoolean * Status;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfLMSCourseProgressUnit : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseProgressUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfLMSCourseProgressUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseProgressUnit:(tns1_LMSCourseProgressUnit *)toAdd;
@property (readonly) NSMutableArray * LMSCourseProgressUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_LMSCourseTranscript : NSObject {
	
/* elements */
	tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSString * Comment;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	tns1_Employee * Employee;
	tns1_Employee * Employee1;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsPass;
	tns1_LMSCourse * LMSCourse;
	NSNumber * LMSCourseId;
	NSNumber * LMSCourseTranscriptId;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_LMSCourseTranscript *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_Employee * Employee;
@property (retain) tns1_Employee * Employee1;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsPass;
@property (retain) tns1_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSNumber * LMSCourseTranscriptId;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfLMSCourseTranscript : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseTranscript;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfLMSCourseTranscript *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseTranscript:(tns1_LMSCourseTranscript *)toAdd;
@property (readonly) NSMutableArray * LMSCourseTranscript;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_LMSCourseType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * LMSCourseTypeId;
	NSString * LMSCourseTypeName;
	tns1_ArrayOfLMSCourse * LMSCourses;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_LMSCourseType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * LMSCourseTypeId;
@property (retain) NSString * LMSCourseTypeName;
@property (retain) tns1_ArrayOfLMSCourse * LMSCourses;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_LMSCourse : NSObject {
	
/* elements */
	NSNumber * ApprovedBy;
	tns1_CLogCourseStatu * CLogCourseStatu;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSString * CourseIcon;
	NSNumber * CourseStatusId;
	NSNumber * CourseTypeId;
	NSDate * CreatedDate;
	NSDate * EndDate;
	tns1_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
	tns1_GPObjectiveInitiative * GPObjectiveInitiative;
	USBoolean * IsApproved;
	USBoolean * IsDeleted;
	USBoolean * IsRequired;
	NSString * Keyword;
	tns1_LMSContentTopic * LMSContentTopic;
	tns1_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
	NSString * LMSCourseCode;
	tns1_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
	tns1_ArrayOfLMSCourseContent * LMSCourseContents;
	tns1_ArrayOfLMSCourseDetail * LMSCourseDetails;
	NSNumber * LMSCourseId;
	tns1_ArrayOfLMSCourseProgressUnit * LMSCourseProgressUnits;
	tns1_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
	tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
	tns1_LMSCourseType * LMSCourseType;
	NSNumber * LMSInitiativeObjectiveId;
	NSNumber * LMSTopicGroupId;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
	NSNumber * Requestedby;
	NSDate * StartDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_LMSCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApprovedBy;
@property (retain) tns1_CLogCourseStatu * CLogCourseStatu;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSString * CourseIcon;
@property (retain) NSNumber * CourseStatusId;
@property (retain) NSNumber * CourseTypeId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) tns1_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
@property (retain) tns1_GPObjectiveInitiative * GPObjectiveInitiative;
@property (retain) USBoolean * IsApproved;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsRequired;
@property (retain) NSString * Keyword;
@property (retain) tns1_LMSContentTopic * LMSContentTopic;
@property (retain) tns1_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
@property (retain) NSString * LMSCourseCode;
@property (retain) tns1_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
@property (retain) tns1_ArrayOfLMSCourseContent * LMSCourseContents;
@property (retain) tns1_ArrayOfLMSCourseDetail * LMSCourseDetails;
@property (retain) NSNumber * LMSCourseId;
@property (retain) tns1_ArrayOfLMSCourseProgressUnit * LMSCourseProgressUnits;
@property (retain) tns1_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
@property (retain) tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
@property (retain) tns1_LMSCourseType * LMSCourseType;
@property (retain) NSNumber * LMSInitiativeObjectiveId;
@property (retain) NSNumber * LMSTopicGroupId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) NSNumber * Requestedby;
@property (retain) NSDate * StartDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_LMSCourseAttendee : NSObject {
	
/* elements */
	tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsApproved;
	USBoolean * IsDeleted;
	USBoolean * IsPassed;
	tns1_LMSCourse * LMSCourse;
	NSNumber * LMSCourseAttendeeId;
	NSNumber * LMSCourseId;
	NSNumber * LMSDevelopmentPlanId;
	NSDate * ModifiedDate;
	NSNumber * NumberOfWarning;
	NSNumber * OverallScore;
	NSNumber * ParticipationStatus;
	NSNumber * ProgressStatus;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_LMSCourseAttendee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsApproved;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsPassed;
@property (retain) tns1_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseAttendeeId;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSNumber * LMSDevelopmentPlanId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NumberOfWarning;
@property (retain) NSNumber * OverallScore;
@property (retain) NSNumber * ParticipationStatus;
@property (retain) NSNumber * ProgressStatus;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfLMSCourseAttendee : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseAttendee;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfLMSCourseAttendee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseAttendee:(tns1_LMSCourseAttendee *)toAdd;
@property (readonly) NSMutableArray * LMSCourseAttendee;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfOrgCoreCompetency : NSObject {
	
/* elements */
	NSMutableArray *OrgCoreCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfOrgCoreCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgCoreCompetency:(tns1_OrgCoreCompetency *)toAdd;
@property (readonly) NSMutableArray * OrgCoreCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CLogRating : NSObject {
	
/* elements */
	NSNumber * CLogRatingId;
	NSString * Description;
	tns1_ArrayOfEmpCompetency * EmpCompetencies;
	tns1_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
	tns1_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
	tns1_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
	tns1_ArrayOfEmpProfileSkill * EmpProfileSkills;
	tns1_ArrayOfEmpProfileWorkingExperience * EmpProfileWorkingExperiences;
	USBoolean * IsDeleted;
	tns1_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
	tns1_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
	tns1_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
	tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
	tns1_ArrayOfOrgCoreCompetency * OrgCoreCompetencies;
	tns1_ArrayOfOrgProficencyDetail * OrgProficencyDetails;
	tns1_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
	tns1_ArrayOfOrgProficencyLevel * OrgProficencyLevels;
	tns1_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
	tns1_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
	tns1_ArrayOfRecCandidateCompetencyRating * RecCandidateCompetencyRatings;
	tns1_ArrayOfRecCandidateExperience * RecCandidateExperiences;
	tns1_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
	tns1_ArrayOfRecCandidateQualification * RecCandidateQualifications;
	tns1_ArrayOfRecCandidateSkill * RecCandidateSkills;
	tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
	tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies1;
	tns1_ArrayOfTrainingProficencyExpected * TrainingProficencyExpecteds;
	tns1_ArrayOfTrainingProficencyRequire * TrainingProficencyRequires;
	NSNumber * Value;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CLogRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSString * Description;
@property (retain) tns1_ArrayOfEmpCompetency * EmpCompetencies;
@property (retain) tns1_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
@property (retain) tns1_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
@property (retain) tns1_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
@property (retain) tns1_ArrayOfEmpProfileSkill * EmpProfileSkills;
@property (retain) tns1_ArrayOfEmpProfileWorkingExperience * EmpProfileWorkingExperiences;
@property (retain) USBoolean * IsDeleted;
@property (retain) tns1_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
@property (retain) tns1_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
@property (retain) tns1_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
@property (retain) tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
@property (retain) tns1_ArrayOfOrgCoreCompetency * OrgCoreCompetencies;
@property (retain) tns1_ArrayOfOrgProficencyDetail * OrgProficencyDetails;
@property (retain) tns1_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
@property (retain) tns1_ArrayOfOrgProficencyLevel * OrgProficencyLevels;
@property (retain) tns1_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
@property (retain) tns1_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
@property (retain) tns1_ArrayOfRecCandidateCompetencyRating * RecCandidateCompetencyRatings;
@property (retain) tns1_ArrayOfRecCandidateExperience * RecCandidateExperiences;
@property (retain) tns1_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
@property (retain) tns1_ArrayOfRecCandidateQualification * RecCandidateQualifications;
@property (retain) tns1_ArrayOfRecCandidateSkill * RecCandidateSkills;
@property (retain) tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
@property (retain) tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies1;
@property (retain) tns1_ArrayOfTrainingProficencyExpected * TrainingProficencyExpecteds;
@property (retain) tns1_ArrayOfTrainingProficencyRequire * TrainingProficencyRequires;
@property (retain) NSNumber * Value;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_OrgProficencyLevel : NSObject {
	
/* elements */
	tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompetencyLevelName;
	NSDate * CreatedDate;
	tns1_ArrayOfEmpCompetency * EmpCompetencies;
	tns1_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_OrgCompetency * OrgCompetency;
	NSNumber * OrgCompetencyId;
	tns1_ArrayOfOrgJobPositionSpecificCompetency * OrgJobPositionSpecificCompetencies;
	tns1_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
	NSNumber * OrgProficencyLevelId;
	tns1_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
	NSString * Statement;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_OrgProficencyLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompetencyLevelName;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_ArrayOfEmpCompetency * EmpCompetencies;
@property (retain) tns1_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_OrgCompetency * OrgCompetency;
@property (retain) NSNumber * OrgCompetencyId;
@property (retain) tns1_ArrayOfOrgJobPositionSpecificCompetency * OrgJobPositionSpecificCompetencies;
@property (retain) tns1_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) tns1_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
@property (retain) NSString * Statement;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_OrgJobPositionSpecificCompetency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSNumber * OrgJobPositionSpecificCompetencyId;
	tns1_OrgProficencyLevel * OrgProficencyLevel;
	NSNumber * OrgProficencyLevelId;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_OrgJobPositionSpecificCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * OrgJobPositionSpecificCompetencyId;
@property (retain) tns1_OrgProficencyLevel * OrgProficencyLevel;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfOrgJobPositionSpecificCompetency : NSObject {
	
/* elements */
	NSMutableArray *OrgJobPositionSpecificCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfOrgJobPositionSpecificCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgJobPositionSpecificCompetency:(tns1_OrgJobPositionSpecificCompetency *)toAdd;
@property (readonly) NSMutableArray * OrgJobPositionSpecificCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_OrgJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	tns1_ArrayOfEmpContract * EmpContracts;
	tns1_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
	tns1_OrgJob * OrgJob;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	tns1_ArrayOfOrgJobPositionRequiredProficency * OrgJobPositionRequiredProficencies;
	tns1_ArrayOfOrgJobPositionSpecificCompetency * OrgJobPositionSpecificCompetencies;
	NSString * OrgJobPositionTitle;
	NSString * OrgJobTitleCode;
	NSNumber * OrgJobTitleId;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgWorkLevelCode;
	NSNumber * OrgWorkLevelId;
	tns1_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
	tns1_ArrayOfRecPlanJobPosition * RecPlanJobPositions;
	tns1_ArrayOfRecProcessApplyForJobPosition * RecProcessApplyForJobPositions;
	tns1_ArrayOfRecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPositions;
	tns1_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
	tns1_ArrayOfRecRequirementJobPosition * RecRequirementJobPositions;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_OrgJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_ArrayOfEmpContract * EmpContracts;
@property (retain) tns1_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) tns1_OrgJob * OrgJob;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) tns1_ArrayOfOrgJobPositionRequiredProficency * OrgJobPositionRequiredProficencies;
@property (retain) tns1_ArrayOfOrgJobPositionSpecificCompetency * OrgJobPositionSpecificCompetencies;
@property (retain) NSString * OrgJobPositionTitle;
@property (retain) NSString * OrgJobTitleCode;
@property (retain) NSNumber * OrgJobTitleId;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) tns1_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
@property (retain) tns1_ArrayOfRecPlanJobPosition * RecPlanJobPositions;
@property (retain) tns1_ArrayOfRecProcessApplyForJobPosition * RecProcessApplyForJobPositions;
@property (retain) tns1_ArrayOfRecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPositions;
@property (retain) tns1_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
@property (retain) tns1_ArrayOfRecRequirementJobPosition * RecRequirementJobPositions;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfOrgJobPosition : NSObject {
	
/* elements */
	NSMutableArray *OrgJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfOrgJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgJobPosition:(tns1_OrgJobPosition *)toAdd;
@property (readonly) NSMutableArray * OrgJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_OrgUnitJob : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_OrgJob * OrgJob;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSNumber * OrgUnitJobId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_OrgUnitJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_OrgJob * OrgJob;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * OrgUnitJobId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfOrgUnitJob : NSObject {
	
/* elements */
	NSMutableArray *OrgUnitJob;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfOrgUnitJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgUnitJob:(tns1_OrgUnitJob *)toAdd;
@property (readonly) NSMutableArray * OrgUnitJob;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_OrgJob : NSObject {
	
/* elements */
	NSString * CLogCareerCode;
	NSNumber * CLogCareerId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_ArrayOfEmpContract * EmpContracts;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	tns1_ArrayOfOrgJobPosition * OrgJobPositions;
	tns1_ArrayOfOrgJobSpecificCompetency * OrgJobSpecificCompetencies;
	tns1_ArrayOfOrgUnitJob * OrgUnitJobs;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_OrgJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogCareerCode;
@property (retain) NSNumber * CLogCareerId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_ArrayOfEmpContract * EmpContracts;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) tns1_ArrayOfOrgJobPosition * OrgJobPositions;
@property (retain) tns1_ArrayOfOrgJobSpecificCompetency * OrgJobSpecificCompetencies;
@property (retain) tns1_ArrayOfOrgUnitJob * OrgUnitJobs;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpContract : NSObject {
	
/* elements */
	tns1_Address * Address;
	NSString * AllocationEquipmentDescription;
	NSString * Allowances;
	NSString * AttachFile;
	NSNumber * BaseWage;
	NSString * Bonuses;
	NSString * CLogContractTermCode;
	NSString * CLogContractTypeCode;
	NSNumber * CompanyId;
	NSString * CompensationDescription;
	NSString * ContractCode;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DateSigned;
	NSDate * EmpAppraiseDate;
	NSString * EmpAppraiseNote;
	NSNumber * EmpAppraiseRating;
	NSNumber * EmpContractId;
	tns1_Employee * Employee;
	NSString * EmployeeRepresentativeAddress;
	NSNumber * EmployeeRepresentativeAddressId;
	NSDate * EmployeeRepresentativeBirthDay;
	NSString * EmployeeRepresentativeBirthPlace;
	NSString * EmployeeRepresentativeCode;
	NSString * EmployeeRepresentativeFullName;
	NSNumber * EmployeeRepresentativeId;
	NSString * EmployeeRepresentativeMajor;
	NSString * EmployeeRepresentativeNationality;
	tns1_Employer * Employer;
	NSNumber * EmployerRepresentativeId;
	NSDate * EndDate;
	NSDate * ExpirationDate;
	NSDate * HRAppraiseDate;
	NSString * HRAppraiseNote;
	NSNumber * HRAppraiseRating;
	NSString * HolIdayPolicy;
	NSNumber * HoursOfOfficialWork;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	USBoolean * IsDeleted;
	USBoolean * IsEnd;
	USBoolean * IsProbation;
	USBoolean * IsSigned;
	NSString * JobResponsibilitiesDescription;
	NSString * LaborSupportEquipments;
	NSDate * LeaderAppraiseDate;
	NSString * LeaderAppraiseNote;
	NSNumber * LeaderAppraiseRating;
	NSString * MedicalInsurance;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSDate * OfficialWorkEndTime;
	NSDate * OfficialWorkStartTime;
	tns1_OrgJob * OrgJob;
	NSNumber * OrgJobId;
	tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSNumber * OrgUnitId;
	NSString * OrganizationAddress;
	NSNumber * OrganizationAddressId;
	NSString * OrganizationName;
	NSString * OrganizationPhone;
	NSString * OtherPolicy;
	NSString * PayDay;
	NSString * PayingMethod;
	NSString * PlaceSigned;
	NSDate * ProbationaryEndDate;
	NSDate * ProbationaryStartDate;
	NSNumber * ProbationaryTime;
	NSString * ProvidedEquipment;
	NSNumber * RelationEmpContractId;
	NSString * SalaryIncreasePolicy;
	NSString * SocialInsurance;
	NSDate * StartDate;
	NSString * TrainingPolicy;
	NSString * Transportation;
	NSString * WorkingAddress;
	NSNumber * WorkingAddressId;
	NSNumber * WorkingDay;
	NSString * WorkingTime;
	NSString * WorkingTimePolicy;
	NSString * cAllowance;
	NSString * cBaseSalary;
	NSString * cSocialInsuranceSalary;
	NSNumber * nAllowance;
	NSNumber * nSocialInsuranceSalary;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_Address * Address;
@property (retain) NSString * AllocationEquipmentDescription;
@property (retain) NSString * Allowances;
@property (retain) NSString * AttachFile;
@property (retain) NSNumber * BaseWage;
@property (retain) NSString * Bonuses;
@property (retain) NSString * CLogContractTermCode;
@property (retain) NSString * CLogContractTypeCode;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompensationDescription;
@property (retain) NSString * ContractCode;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateSigned;
@property (retain) NSDate * EmpAppraiseDate;
@property (retain) NSString * EmpAppraiseNote;
@property (retain) NSNumber * EmpAppraiseRating;
@property (retain) NSNumber * EmpContractId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeRepresentativeAddress;
@property (retain) NSNumber * EmployeeRepresentativeAddressId;
@property (retain) NSDate * EmployeeRepresentativeBirthDay;
@property (retain) NSString * EmployeeRepresentativeBirthPlace;
@property (retain) NSString * EmployeeRepresentativeCode;
@property (retain) NSString * EmployeeRepresentativeFullName;
@property (retain) NSNumber * EmployeeRepresentativeId;
@property (retain) NSString * EmployeeRepresentativeMajor;
@property (retain) NSString * EmployeeRepresentativeNationality;
@property (retain) tns1_Employer * Employer;
@property (retain) NSNumber * EmployerRepresentativeId;
@property (retain) NSDate * EndDate;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSDate * HRAppraiseDate;
@property (retain) NSString * HRAppraiseNote;
@property (retain) NSNumber * HRAppraiseRating;
@property (retain) NSString * HolIdayPolicy;
@property (retain) NSNumber * HoursOfOfficialWork;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEnd;
@property (retain) USBoolean * IsProbation;
@property (retain) USBoolean * IsSigned;
@property (retain) NSString * JobResponsibilitiesDescription;
@property (retain) NSString * LaborSupportEquipments;
@property (retain) NSDate * LeaderAppraiseDate;
@property (retain) NSString * LeaderAppraiseNote;
@property (retain) NSNumber * LeaderAppraiseRating;
@property (retain) NSString * MedicalInsurance;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * OfficialWorkEndTime;
@property (retain) NSDate * OfficialWorkStartTime;
@property (retain) tns1_OrgJob * OrgJob;
@property (retain) NSNumber * OrgJobId;
@property (retain) tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrganizationAddress;
@property (retain) NSNumber * OrganizationAddressId;
@property (retain) NSString * OrganizationName;
@property (retain) NSString * OrganizationPhone;
@property (retain) NSString * OtherPolicy;
@property (retain) NSString * PayDay;
@property (retain) NSString * PayingMethod;
@property (retain) NSString * PlaceSigned;
@property (retain) NSDate * ProbationaryEndDate;
@property (retain) NSDate * ProbationaryStartDate;
@property (retain) NSNumber * ProbationaryTime;
@property (retain) NSString * ProvidedEquipment;
@property (retain) NSNumber * RelationEmpContractId;
@property (retain) NSString * SalaryIncreasePolicy;
@property (retain) NSString * SocialInsurance;
@property (retain) NSDate * StartDate;
@property (retain) NSString * TrainingPolicy;
@property (retain) NSString * Transportation;
@property (retain) NSString * WorkingAddress;
@property (retain) NSNumber * WorkingAddressId;
@property (retain) NSNumber * WorkingDay;
@property (retain) NSString * WorkingTime;
@property (retain) NSString * WorkingTimePolicy;
@property (retain) NSString * cAllowance;
@property (retain) NSString * cBaseSalary;
@property (retain) NSString * cSocialInsuranceSalary;
@property (retain) NSNumber * nAllowance;
@property (retain) NSNumber * nSocialInsuranceSalary;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpContract : NSObject {
	
/* elements */
	NSMutableArray *EmpContract;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpContract:(tns1_EmpContract *)toAdd;
@property (readonly) NSMutableArray * EmpContract;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_Address : NSObject {
	
/* elements */
	NSNumber * AddressId;
	NSString * AddressNumber;
	NSNumber * CLogCityId;
	NSNumber * CLogCountryId;
	NSNumber * CLogDistrictId;
	tns1_ArrayOfCLogTrainer * CLogTrainers;
	tns1_ArrayOfEmpContract * EmpContracts;
	NSString * FullAddress;
	tns1_ArrayOfOrgUnit * OrgUnits;
	NSString * PostalCode;
	NSString * Street;
	NSString * Ward;
	NSString * ZipCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_Address *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AddressId;
@property (retain) NSString * AddressNumber;
@property (retain) NSNumber * CLogCityId;
@property (retain) NSNumber * CLogCountryId;
@property (retain) NSNumber * CLogDistrictId;
@property (retain) tns1_ArrayOfCLogTrainer * CLogTrainers;
@property (retain) tns1_ArrayOfEmpContract * EmpContracts;
@property (retain) NSString * FullAddress;
@property (retain) tns1_ArrayOfOrgUnit * OrgUnits;
@property (retain) NSString * PostalCode;
@property (retain) NSString * Street;
@property (retain) NSString * Ward;
@property (retain) NSString * ZipCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CLogTrainer : NSObject {
	
/* elements */
	NSString * Address;
	tns1_Address * Address1;
	NSNumber * AddressId;
	NSNumber * CLogTrainerId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * EducationUnit;
	NSString * Email;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSString * FirstName;
	NSString * FullName;
	USBoolean * IsDeleted;
	USBoolean * IsEmployee;
	NSString * LastName;
	NSDate * ModifiedDate;
	NSString * PhoneNumber;
	tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CLogTrainer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) tns1_Address * Address1;
@property (retain) NSNumber * AddressId;
@property (retain) NSNumber * CLogTrainerId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * EducationUnit;
@property (retain) NSString * Email;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEmployee;
@property (retain) NSString * LastName;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * PhoneNumber;
@property (retain) tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfCLogTrainer : NSObject {
	
/* elements */
	NSMutableArray *CLogTrainer;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfCLogTrainer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogTrainer:(tns1_CLogTrainer *)toAdd;
@property (readonly) NSMutableArray * CLogTrainer;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CLogEmpWorkingStatu : NSObject {
	
/* elements */
	NSNumber * CLogEmpWorkingStatusId;
	NSNumber * CompanyId;
	tns1_ArrayOfEmpBasicProfile * EmpBasicProfiles;
	NSString * EmpWorkingStatusCode;
	NSString * EmpWorkingStatusName;
	NSString * EmpWorkingStatusNameEN;
	USBoolean * IsDeleted;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CLogEmpWorkingStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogEmpWorkingStatusId;
@property (retain) NSNumber * CompanyId;
@property (retain) tns1_ArrayOfEmpBasicProfile * EmpBasicProfiles;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmpWorkingStatusNameEN;
@property (retain) USBoolean * IsDeleted;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpBasicProfile : NSObject {
	
/* elements */
	NSString * BankAccountNo;
	NSString * BankBranch;
	NSString * BankName;
	NSNumber * CLogBankBranchId;
	tns1_CLogEmpWorkingStatu * CLogEmpWorkingStatu;
	NSString * CVUrl;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * CulturalLevel;
	NSNumber * CulturalLevelId;
	NSString * EducationLevel;
	NSNumber * EducationLevelId;
	NSNumber * EmpBasicProfileId;
	NSNumber * EmpWorkingStatusId;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardPlaceOfIssue;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	NSString * Major;
	NSNumber * MajorId;
	NSString * MaritalStatus;
	NSNumber * MaritalStatusId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSString * PITCode;
	NSString * PITIssuedBy;
	NSDate * PassportDateOfExpire;
	NSDate * PassportDateOfIssue;
	NSString * PassportNo;
	NSString * PassportPlaceOfIssue;
	NSNumber * ProbationarySalary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpBasicProfile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * BankAccountNo;
@property (retain) NSString * BankBranch;
@property (retain) NSString * BankName;
@property (retain) NSNumber * CLogBankBranchId;
@property (retain) tns1_CLogEmpWorkingStatu * CLogEmpWorkingStatu;
@property (retain) NSString * CVUrl;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * CulturalLevel;
@property (retain) NSNumber * CulturalLevelId;
@property (retain) NSString * EducationLevel;
@property (retain) NSNumber * EducationLevelId;
@property (retain) NSNumber * EmpBasicProfileId;
@property (retain) NSNumber * EmpWorkingStatusId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Major;
@property (retain) NSNumber * MajorId;
@property (retain) NSString * MaritalStatus;
@property (retain) NSNumber * MaritalStatusId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSString * PITCode;
@property (retain) NSString * PITIssuedBy;
@property (retain) NSDate * PassportDateOfExpire;
@property (retain) NSDate * PassportDateOfIssue;
@property (retain) NSString * PassportNo;
@property (retain) NSString * PassportPlaceOfIssue;
@property (retain) NSNumber * ProbationarySalary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpBasicProfile : NSObject {
	
/* elements */
	NSMutableArray *EmpBasicProfile;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpBasicProfile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpBasicProfile:(tns1_EmpBasicProfile *)toAdd;
@property (readonly) NSMutableArray * EmpBasicProfile;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpCompetencyRating : NSObject {
	
/* elements */
	NSMutableArray *EmpCompetencyRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpCompetencyRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpCompetencyRating:(tns1_EmpCompetencyRating *)toAdd;
@property (readonly) NSMutableArray * EmpCompetencyRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpPerformanceAppraisal : NSObject {
	
/* elements */
	NSDate * AppraisalDate;
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * Comment;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpPerformanceAppraisalId;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsEndYear;
	USBoolean * IsHalfYear;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * RelateRowId;
	NSNumber * Score;
	NSString * SelfComment;
	NSNumber * SelfRating;
	NSNumber * SelfScore;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpPerformanceAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AppraisalDate;
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpPerformanceAppraisalId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEndYear;
@property (retain) USBoolean * IsHalfYear;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * SelfScore;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpPerformanceAppraisal : NSObject {
	
/* elements */
	NSMutableArray *EmpPerformanceAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpPerformanceAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpPerformanceAppraisal:(tns1_EmpPerformanceAppraisal *)toAdd;
@property (readonly) NSMutableArray * EmpPerformanceAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileAllowance : NSObject {
	
/* elements */
	NSDate * ActivedDate;
	NSString * AllowanceCode;
	NSString * AllowanceName;
	NSNumber * AllowanceValue;
	NSNumber * CLogCurrency;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpProfileAllowanceId;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsActived;
	USBoolean * IsDeactived;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileAllowance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ActivedDate;
@property (retain) NSString * AllowanceCode;
@property (retain) NSString * AllowanceName;
@property (retain) NSNumber * AllowanceValue;
@property (retain) NSNumber * CLogCurrency;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileAllowanceId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeactived;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileAllowance : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileAllowance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileAllowance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileAllowance:(tns1_EmpProfileAllowance *)toAdd;
@property (readonly) NSMutableArray * EmpProfileAllowance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileBaseSalary : NSObject {
	
/* elements */
	NSDate * ActivedDate;
	NSString * BasicPayCode;
	NSNumber * CBSalaryCofficientId;
	NSNumber * CBSalaryGradeId;
	NSNumber * CBSalaryLevelId;
	NSNumber * CBSalaryScaleId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpProfileBaseSalaryId;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsActived;
	USBoolean * IsDeactived;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * NonPayScaleSalaryValue;
	NSString * Note;
	NSNumber * PayScaleSalaryValue;
	NSString * SalaryCode;
	NSNumber * SalaryId;
	NSString * SalaryName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileBaseSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ActivedDate;
@property (retain) NSString * BasicPayCode;
@property (retain) NSNumber * CBSalaryCofficientId;
@property (retain) NSNumber * CBSalaryGradeId;
@property (retain) NSNumber * CBSalaryLevelId;
@property (retain) NSNumber * CBSalaryScaleId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileBaseSalaryId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeactived;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NonPayScaleSalaryValue;
@property (retain) NSString * Note;
@property (retain) NSNumber * PayScaleSalaryValue;
@property (retain) NSString * SalaryCode;
@property (retain) NSNumber * SalaryId;
@property (retain) NSString * SalaryName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileBaseSalary : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileBaseSalary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileBaseSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileBaseSalary:(tns1_EmpProfileBaseSalary *)toAdd;
@property (readonly) NSMutableArray * EmpProfileBaseSalary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileBenefit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileBenefitId;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * HealthInsuranceDateExpire;
	NSDate * HealthInsuranceDateIssue;
	NSNumber * HealthInsuranceId;
	NSString * HealthInsuranceNumber;
	NSString * HealthInsuranceRegPlace;
	USBoolean * IsDeleted;
	NSDate * LabourBookDateOfIssue;
	NSString * LabourBookNo;
	NSString * LabourBookPlaceOfIssue;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * OtherInsuranceCompanyName;
	NSDate * OtherInsuranceDateExpire;
	NSDate * OtherInsuranceDateIssue;
	NSString * OtherInsuranceNumber;
	NSDate * SocialInsuranceDateIssue;
	NSNumber * SocialInsuranceId;
	NSNumber * SocialInsuranceJoiningBefore;
	NSString * SocialInsuranceNumber;
	NSDate * UnemploymentInsuranceDateIssue;
	NSNumber * UnemploymentInsuranceId;
	NSString * UnemploymentInsuranceNumber;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileBenefitId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * HealthInsuranceDateExpire;
@property (retain) NSDate * HealthInsuranceDateIssue;
@property (retain) NSNumber * HealthInsuranceId;
@property (retain) NSString * HealthInsuranceNumber;
@property (retain) NSString * HealthInsuranceRegPlace;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * LabourBookDateOfIssue;
@property (retain) NSString * LabourBookNo;
@property (retain) NSString * LabourBookPlaceOfIssue;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OtherInsuranceCompanyName;
@property (retain) NSDate * OtherInsuranceDateExpire;
@property (retain) NSDate * OtherInsuranceDateIssue;
@property (retain) NSString * OtherInsuranceNumber;
@property (retain) NSDate * SocialInsuranceDateIssue;
@property (retain) NSNumber * SocialInsuranceId;
@property (retain) NSNumber * SocialInsuranceJoiningBefore;
@property (retain) NSString * SocialInsuranceNumber;
@property (retain) NSDate * UnemploymentInsuranceDateIssue;
@property (retain) NSNumber * UnemploymentInsuranceId;
@property (retain) NSString * UnemploymentInsuranceNumber;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileBenefit : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileBenefit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileBenefit:(tns1_EmpProfileBenefit *)toAdd;
@property (readonly) NSMutableArray * EmpProfileBenefit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileComment : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSString * Content;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfileCommentId;
	tns1_Employee * Employee;
	tns1_Employee * Employee1;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * RelateRowId;
	NSString * ReviewerCode;
	NSNumber * ReviewerId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileComment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Content;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfileCommentId;
@property (retain) tns1_Employee * Employee;
@property (retain) tns1_Employee * Employee1;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * ReviewerCode;
@property (retain) NSNumber * ReviewerId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileComment : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileComment;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileComment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileComment:(tns1_EmpProfileComment *)toAdd;
@property (readonly) NSMutableArray * EmpProfileComment;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileComputingSkill : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSString * ComputingSkillName;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	NSNumber * EmpProfileComputingSkillId;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsValid;
	NSDate * ModifiedDate;
	NSString * Other;
	NSString * PlaceIssue;
	NSNumber * RatingId;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileComputingSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ComputingSkillName;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) NSNumber * EmpProfileComputingSkillId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValid;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) NSNumber * RatingId;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileComputingSkill : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileComputingSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileComputingSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileComputingSkill:(tns1_EmpProfileComputingSkill *)toAdd;
@property (readonly) NSMutableArray * EmpProfileComputingSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileContact : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSString * ContactAddress;
	NSNumber * ContactAddressId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * Email;
	NSNumber * EmpProfileContactId;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSString * Fax;
	NSString * HomePhone;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	NSString * MobileNumber;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * OfficePhone;
	NSString * PermanentAddress;
	NSNumber * PermanentAddressId;
	NSNumber * RelateRowId;
	NSString * TemporaryAddress;
	NSNumber * TemporaryAddressId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileContact *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ContactAddress;
@property (retain) NSNumber * ContactAddressId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Email;
@property (retain) NSNumber * EmpProfileContactId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * Fax;
@property (retain) NSString * HomePhone;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * MobileNumber;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OfficePhone;
@property (retain) NSString * PermanentAddress;
@property (retain) NSNumber * PermanentAddressId;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * TemporaryAddress;
@property (retain) NSNumber * TemporaryAddressId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileContact : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileContact;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileContact *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileContact:(tns1_EmpProfileContact *)toAdd;
@property (readonly) NSMutableArray * EmpProfileContact;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_OrgDegree : NSObject {
	
/* elements */
	NSNumber * CLogMajorId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * Description;
	tns1_ArrayOfEmpProfileDegree * EmpProfileDegrees;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * OrgDegreeId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_OrgDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) tns1_ArrayOfEmpProfileDegree * EmpProfileDegrees;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * OrgDegreeId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileDegree : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	NSNumber * EmpProfileDegreeId;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsValId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	tns1_OrgDegree * OrgDegree;
	NSNumber * OrgDegreeId;
	NSNumber * OrgDegreeRankId;
	NSString * Other;
	NSString * PlaceIssue;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) NSNumber * EmpProfileDegreeId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_OrgDegree * OrgDegree;
@property (retain) NSNumber * OrgDegreeId;
@property (retain) NSNumber * OrgDegreeRankId;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileDegree : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileDegree;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileDegree:(tns1_EmpProfileDegree *)toAdd;
@property (readonly) NSMutableArray * EmpProfileDegree;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_OrgDisciplineForUnit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgDisciplineForUnitId;
	NSNumber * OrgDisciplineId;
	tns1_OrgDisciplineMaster * OrgDisciplineMaster;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_OrgDisciplineForUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgDisciplineForUnitId;
@property (retain) NSNumber * OrgDisciplineId;
@property (retain) tns1_OrgDisciplineMaster * OrgDisciplineMaster;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfOrgDisciplineForUnit : NSObject {
	
/* elements */
	NSMutableArray *OrgDisciplineForUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfOrgDisciplineForUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgDisciplineForUnit:(tns1_OrgDisciplineForUnit *)toAdd;
@property (readonly) NSMutableArray * OrgDisciplineForUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_OrgDisciplineMaster : NSObject {
	
/* elements */
	NSNumber * ApproveId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSDate * DateOfIssue;
	NSString * DecisionNo;
	tns1_ArrayOfEmpProfileDiscipline * EmpProfileDisciplines;
	USBoolean * IsDeleted;
	USBoolean * IsForEmployee;
	NSDate * ModifiedDate;
	NSNumber * MonetaryValue;
	tns1_ArrayOfOrgDisciplineForUnit * OrgDisciplineForUnits;
	NSNumber * OrgDisciplineMasterId;
	NSString * OrgDisciplineTypeCode;
	USBoolean * Status;
	NSString * Title;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_OrgDisciplineMaster *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproveId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSDate * DateOfIssue;
@property (retain) NSString * DecisionNo;
@property (retain) tns1_ArrayOfEmpProfileDiscipline * EmpProfileDisciplines;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsForEmployee;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * MonetaryValue;
@property (retain) tns1_ArrayOfOrgDisciplineForUnit * OrgDisciplineForUnits;
@property (retain) NSNumber * OrgDisciplineMasterId;
@property (retain) NSString * OrgDisciplineTypeCode;
@property (retain) USBoolean * Status;
@property (retain) NSString * Title;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileDiscipline : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfileDisciplineId;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	tns1_OrgDisciplineMaster * OrgDisciplineMaster;
	NSNumber * OrgDisciplineMasterId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileDiscipline *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfileDisciplineId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_OrgDisciplineMaster * OrgDisciplineMaster;
@property (retain) NSNumber * OrgDisciplineMasterId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileDiscipline : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileDiscipline;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileDiscipline *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileDiscipline:(tns1_EmpProfileDiscipline *)toAdd;
@property (readonly) NSMutableArray * EmpProfileDiscipline;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CLogEquipment : NSObject {
	
/* elements */
	NSDate * AddDate;
	NSString * CLogAssetsTypeCode;
	NSNumber * CLogCurrencyId;
	NSNumber * CLogEquipmentId;
	NSString * CLogEquipmentTypeCode;
	NSString * Code;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * Depreciation;
	tns1_ArrayOfEmpProfileEquipment * EmpProfileEquipments;
	USBoolean * IsBigValue;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
	NSNumber * Number;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CLogEquipment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AddDate;
@property (retain) NSString * CLogAssetsTypeCode;
@property (retain) NSNumber * CLogCurrencyId;
@property (retain) NSNumber * CLogEquipmentId;
@property (retain) NSString * CLogEquipmentTypeCode;
@property (retain) NSString * Code;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * Depreciation;
@property (retain) tns1_ArrayOfEmpProfileEquipment * EmpProfileEquipments;
@property (retain) USBoolean * IsBigValue;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) NSNumber * Number;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileEquipment : NSObject {
	
/* elements */
	tns1_CLogEquipment * CLogEquipment;
	NSNumber * CLogEquipmentId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DateHandover;
	NSDate * DateReceived;
	NSString * Description;
	NSNumber * EmpProfileEquipmentId;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * HandoverStatus;
	USBoolean * IsDeleted;
	USBoolean * IsHandover;
	USBoolean * IsReturnToComp;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	USBoolean * MustHandover;
	NSNumber * Number;
	NSString * ReceiverCode;
	NSNumber * ReceiverId;
	NSNumber * RequestDetailId;
	NSString * StateHandover;
	NSString * StateReceived;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileEquipment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_CLogEquipment * CLogEquipment;
@property (retain) NSNumber * CLogEquipmentId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateHandover;
@property (retain) NSDate * DateReceived;
@property (retain) NSString * Description;
@property (retain) NSNumber * EmpProfileEquipmentId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * HandoverStatus;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsHandover;
@property (retain) USBoolean * IsReturnToComp;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) USBoolean * MustHandover;
@property (retain) NSNumber * Number;
@property (retain) NSString * ReceiverCode;
@property (retain) NSNumber * ReceiverId;
@property (retain) NSNumber * RequestDetailId;
@property (retain) NSString * StateHandover;
@property (retain) NSString * StateReceived;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileEquipment : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileEquipment;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileEquipment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileEquipment:(tns1_EmpProfileEquipment *)toAdd;
@property (readonly) NSMutableArray * EmpProfileEquipment;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileExperience : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * AttachedDocumentUrl;
	NSString * Company;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * Customer;
	NSNumber * EmpProfileExperienceId;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * ProjectName;
	NSString * ProjectResult;
	NSNumber * RelateRowId;
	NSString * Roles;
	NSDate * StartDate;
	NSString * TeamSize;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * AttachedDocumentUrl;
@property (retain) NSString * Company;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Customer;
@property (retain) NSNumber * EmpProfileExperienceId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * ProjectName;
@property (retain) NSString * ProjectResult;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * Roles;
@property (retain) NSDate * StartDate;
@property (retain) NSString * TeamSize;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileExperience : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileExperience:(tns1_EmpProfileExperience *)toAdd;
@property (readonly) NSMutableArray * EmpProfileExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CLogLanguage : NSObject {
	
/* elements */
	NSNumber * CLogLanguageId;
	tns1_ArrayOfEmpProfileForeignLanguage * EmpProfileForeignLanguages;
	USBoolean * IsDeleted;
	NSString * Language;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CLogLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogLanguageId;
@property (retain) tns1_ArrayOfEmpProfileForeignLanguage * EmpProfileForeignLanguages;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Language;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileForeignLanguage : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * AttachmenttUrl;
	tns1_CLogLanguage * CLogLanguage;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DateOfIssue;
	NSString * DegreeName;
	NSNumber * EffectiveTime;
	NSNumber * EmpProfileForeignLanguageId;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * LanguageId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * OrgDegreeRankId;
	NSString * PlaceOfIssue;
	NSNumber * RelateRowId;
	NSString * TrainingCenter;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileForeignLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * AttachmenttUrl;
@property (retain) tns1_CLogLanguage * CLogLanguage;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateOfIssue;
@property (retain) NSString * DegreeName;
@property (retain) NSNumber * EffectiveTime;
@property (retain) NSNumber * EmpProfileForeignLanguageId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * LanguageId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * OrgDegreeRankId;
@property (retain) NSString * PlaceOfIssue;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * TrainingCenter;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileForeignLanguage : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileForeignLanguage;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileForeignLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileForeignLanguage:(tns1_EmpProfileForeignLanguage *)toAdd;
@property (readonly) NSMutableArray * EmpProfileForeignLanguage;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileHealthInsurance : NSObject {
	
/* elements */
	NSNumber * CLogHospitalId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * HICompanyRate;
	NSNumber * HIEmployeeRate;
	NSDate * HealthInsuranceDateEffect;
	NSDate * HealthInsuranceDateExpire;
	NSNumber * HealthInsuranceId;
	NSString * HealthInsuranceNumber;
	NSDate * ImplementDate;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	USBoolean * IsDifference;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileHealthInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogHospitalId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * HICompanyRate;
@property (retain) NSNumber * HIEmployeeRate;
@property (retain) NSDate * HealthInsuranceDateEffect;
@property (retain) NSDate * HealthInsuranceDateExpire;
@property (retain) NSNumber * HealthInsuranceId;
@property (retain) NSString * HealthInsuranceNumber;
@property (retain) NSDate * ImplementDate;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDifference;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileHealthInsurance : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileHealthInsurance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileHealthInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileHealthInsurance:(tns1_EmpProfileHealthInsurance *)toAdd;
@property (readonly) NSMutableArray * EmpProfileHealthInsurance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileHealthy : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * BloodGroup;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DiseaseAbsenceEndDay;
	NSDate * DiseaseAbsenceStartDay;
	NSNumber * DiseaseBenefit;
	NSString * DiseaseName;
	NSNumber * EmpProfileHealthyId;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	USBoolean * IsDiseaseAbsence;
	USBoolean * IsEnoughHealthToWork;
	USBoolean * IsGoodHealth;
	USBoolean * IsMaternityAbsence;
	USBoolean * IsMedicalHistory;
	USBoolean * IsPeopleWithDisability;
	NSDate * MaternityAbsenceEndDay;
	NSDate * MaternityAbsenceStartDay;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileHealthy *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * BloodGroup;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DiseaseAbsenceEndDay;
@property (retain) NSDate * DiseaseAbsenceStartDay;
@property (retain) NSNumber * DiseaseBenefit;
@property (retain) NSString * DiseaseName;
@property (retain) NSNumber * EmpProfileHealthyId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDiseaseAbsence;
@property (retain) USBoolean * IsEnoughHealthToWork;
@property (retain) USBoolean * IsGoodHealth;
@property (retain) USBoolean * IsMaternityAbsence;
@property (retain) USBoolean * IsMedicalHistory;
@property (retain) USBoolean * IsPeopleWithDisability;
@property (retain) NSDate * MaternityAbsenceEndDay;
@property (retain) NSDate * MaternityAbsenceStartDay;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileHealthy : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileHealthy;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileHealthy *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileHealthy:(tns1_EmpProfileHealthy *)toAdd;
@property (readonly) NSMutableArray * EmpProfileHealthy;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CLogHobby : NSObject {
	
/* elements */
	NSNumber * CLogHobbyId;
	NSString * Description;
	tns1_ArrayOfEmpProfileHobby * EmpProfileHobbies;
	USBoolean * IsDeleted;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CLogHobby *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogHobbyId;
@property (retain) NSString * Description;
@property (retain) tns1_ArrayOfEmpProfileHobby * EmpProfileHobbies;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileHobby : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	tns1_CLogHobby * CLogHobby;
	NSNumber * CLogHobbyId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfileHobbyId;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileHobby *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) tns1_CLogHobby * CLogHobby;
@property (retain) NSNumber * CLogHobbyId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfileHobbyId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileHobby : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileHobby;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileHobby *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileHobby:(tns1_EmpProfileHobby *)toAdd;
@property (readonly) NSMutableArray * EmpProfileHobby;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileInternalCourse : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpInternalCourseId;
	NSNumber * EmpProfileInternalCourseId;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Result;
	NSNumber * Status;
	NSString * Support;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileInternalCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpInternalCourseId;
@property (retain) NSNumber * EmpProfileInternalCourseId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Result;
@property (retain) NSNumber * Status;
@property (retain) NSString * Support;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileInternalCourse : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileInternalCourse;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileInternalCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileInternalCourse:(tns1_EmpProfileInternalCourse *)toAdd;
@property (readonly) NSMutableArray * EmpProfileInternalCourse;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileLeaveRegime : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CompensatoryLeave;
	NSNumber * CompensatoryLeaveOfYear;
	NSNumber * CompensatoryRemainedLeave;
	NSNumber * CompensatoryUsedLeave;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileLeaveRegimeId;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * MonthlyLeave;
	NSNumber * PlusMinusLeave;
	NSNumber * PreviousLeave;
	NSNumber * RemainedLeave;
	NSNumber * SeniorityLeave;
	NSNumber * TotalUsedLeave;
	NSNumber * Year;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileLeaveRegime *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CompensatoryLeave;
@property (retain) NSNumber * CompensatoryLeaveOfYear;
@property (retain) NSNumber * CompensatoryRemainedLeave;
@property (retain) NSNumber * CompensatoryUsedLeave;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileLeaveRegimeId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * MonthlyLeave;
@property (retain) NSNumber * PlusMinusLeave;
@property (retain) NSNumber * PreviousLeave;
@property (retain) NSNumber * RemainedLeave;
@property (retain) NSNumber * SeniorityLeave;
@property (retain) NSNumber * TotalUsedLeave;
@property (retain) NSNumber * Year;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileLeaveRegime : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileLeaveRegime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileLeaveRegime *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileLeaveRegime:(tns1_EmpProfileLeaveRegime *)toAdd;
@property (readonly) NSMutableArray * EmpProfileLeaveRegime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpOtherBenefit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	NSNumber * EmpOtherBenefitId;
	tns1_ArrayOfEmpProfileOtherBenefit * EmpProfileOtherBenefits;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpOtherBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) NSNumber * EmpOtherBenefitId;
@property (retain) tns1_ArrayOfEmpProfileOtherBenefit * EmpProfileOtherBenefits;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileOtherBenefit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	tns1_EmpOtherBenefit * EmpOtherBenefit;
	NSNumber * EmpOtherBenefitId;
	NSNumber * EmpProfileOtherBenefitId;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileOtherBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_EmpOtherBenefit * EmpOtherBenefit;
@property (retain) NSNumber * EmpOtherBenefitId;
@property (retain) NSNumber * EmpProfileOtherBenefitId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileOtherBenefit : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileOtherBenefit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileOtherBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileOtherBenefit:(tns1_EmpProfileOtherBenefit *)toAdd;
@property (readonly) NSMutableArray * EmpProfileOtherBenefit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileParticipation : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileParticipationId;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSDate * PartyFeeEndDate;
	NSNumber * PartyFeePercent;
	NSString * PartyFeeRole;
	NSDate * PartyFeeStartDate;
	NSNumber * RelateRowId;
	NSDate * UnionFeeEndDate;
	NSNumber * UnionFeePercent;
	NSString * UnionFeeRole;
	NSDate * UnionFeeStartDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileParticipation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileParticipationId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * PartyFeeEndDate;
@property (retain) NSNumber * PartyFeePercent;
@property (retain) NSString * PartyFeeRole;
@property (retain) NSDate * PartyFeeStartDate;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSDate * UnionFeeEndDate;
@property (retain) NSNumber * UnionFeePercent;
@property (retain) NSString * UnionFeeRole;
@property (retain) NSDate * UnionFeeStartDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileParticipation : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileParticipation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileParticipation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileParticipation:(tns1_EmpProfileParticipation *)toAdd;
@property (readonly) NSMutableArray * EmpProfileParticipation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CLogPersonality : NSObject {
	
/* elements */
	NSNumber * CLogPersonalityId;
	NSString * Description;
	tns1_ArrayOfEmpProfilePersonality * EmpProfilePersonalities;
	USBoolean * IsDeleted;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CLogPersonality *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogPersonalityId;
@property (retain) NSString * Description;
@property (retain) tns1_ArrayOfEmpProfilePersonality * EmpProfilePersonalities;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfilePersonality : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	tns1_CLogPersonality * CLogPersonality;
	NSNumber * CLogPersonalityId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfilePersonalityId;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfilePersonality *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) tns1_CLogPersonality * CLogPersonality;
@property (retain) NSNumber * CLogPersonalityId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfilePersonalityId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfilePersonality : NSObject {
	
/* elements */
	NSMutableArray *EmpProfilePersonality;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfilePersonality *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfilePersonality:(tns1_EmpProfilePersonality *)toAdd;
@property (readonly) NSMutableArray * EmpProfilePersonality;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileProcessOfWork : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * DecisionFile;
	NSString * DecisionName;
	NSString * DecisionNo;
	NSNumber * EmpProfileJobPositionId;
	NSNumber * EmpProfileProcessOfWorkId;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ExpirationDate;
	NSDate * ImplementationDate;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * OrgUnitAddress;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgUnitName;
	NSString * OrgUnitNameEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileProcessOfWork *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DecisionFile;
@property (retain) NSString * DecisionName;
@property (retain) NSString * DecisionNo;
@property (retain) NSNumber * EmpProfileJobPositionId;
@property (retain) NSNumber * EmpProfileProcessOfWorkId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OrgUnitAddress;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitName;
@property (retain) NSString * OrgUnitNameEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileProcessOfWork : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileProcessOfWork;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileProcessOfWork *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileProcessOfWork:(tns1_EmpProfileProcessOfWork *)toAdd;
@property (readonly) NSMutableArray * EmpProfileProcessOfWork;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_OrgRewardForUnit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgRewardForUnitId;
	NSNumber * OrgRewardId;
	tns1_OrgRewardMaster * OrgRewardMaster;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_OrgRewardForUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgRewardForUnitId;
@property (retain) NSNumber * OrgRewardId;
@property (retain) tns1_OrgRewardMaster * OrgRewardMaster;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfOrgRewardForUnit : NSObject {
	
/* elements */
	NSMutableArray *OrgRewardForUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfOrgRewardForUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgRewardForUnit:(tns1_OrgRewardForUnit *)toAdd;
@property (readonly) NSMutableArray * OrgRewardForUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_OrgRewardMaster : NSObject {
	
/* elements */
	NSNumber * ApproveId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSDate * DateOfIssue;
	NSString * DecisionNo;
	tns1_ArrayOfEmpProfileReward * EmpProfileRewards;
	USBoolean * IsAllCompany;
	USBoolean * IsDeleted;
	USBoolean * IsForEmployee;
	NSDate * ModifiedDate;
	NSNumber * MonetaryValue;
	tns1_ArrayOfOrgRewardForUnit * OrgRewardForUnits;
	NSNumber * OrgRewardMasterId;
	NSString * OrgRewardTypeCode;
	USBoolean * Status;
	NSString * Title;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_OrgRewardMaster *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproveId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSDate * DateOfIssue;
@property (retain) NSString * DecisionNo;
@property (retain) tns1_ArrayOfEmpProfileReward * EmpProfileRewards;
@property (retain) USBoolean * IsAllCompany;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsForEmployee;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * MonetaryValue;
@property (retain) tns1_ArrayOfOrgRewardForUnit * OrgRewardForUnits;
@property (retain) NSNumber * OrgRewardMasterId;
@property (retain) NSString * OrgRewardTypeCode;
@property (retain) USBoolean * Status;
@property (retain) NSString * Title;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileReward : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfileRewardId;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	tns1_OrgRewardMaster * OrgRewardMaster;
	NSNumber * OrgRewardMasterId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileReward *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfileRewardId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_OrgRewardMaster * OrgRewardMaster;
@property (retain) NSNumber * OrgRewardMasterId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileReward : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileReward;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileReward *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileReward:(tns1_EmpProfileReward *)toAdd;
@property (readonly) NSMutableArray * EmpProfileReward;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileTotalIncome : NSObject {
	
/* elements */
	NSDate * CompanyId;
	NSDate * CreatedDate;
	NSNumber * DependancyDeduction;
	NSNumber * EmpProfileTotalIncomeId;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSNumber * GrossPayment;
	NSDate * ModifiedDate;
	NSNumber * Month;
	NSNumber * NetPayment;
	NSNumber * PIT;
	NSNumber * TaxableAmount;
	NSNumber * TotalDeduction;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileTotalIncome *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * DependancyDeduction;
@property (retain) NSNumber * EmpProfileTotalIncomeId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * GrossPayment;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Month;
@property (retain) NSNumber * NetPayment;
@property (retain) NSNumber * PIT;
@property (retain) NSNumber * TaxableAmount;
@property (retain) NSNumber * TotalDeduction;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileTotalIncome : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileTotalIncome;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileTotalIncome *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileTotalIncome:(tns1_EmpProfileTotalIncome *)toAdd;
@property (readonly) NSMutableArray * EmpProfileTotalIncome;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileTraining : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * AttachedDocumentUrl;
	NSString * Certifications;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileTrainingId;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	NSDate * ExpiredDate;
	USBoolean * IsDeleted;
	USBoolean * IsInternal;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * RelateRowId;
	NSString * Result;
	NSDate * StartDate;
	NSNumber * Status;
	NSString * TrainingProgram;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileTraining *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * AttachedDocumentUrl;
@property (retain) NSString * Certifications;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileTrainingId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) NSDate * ExpiredDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInternal;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * Result;
@property (retain) NSDate * StartDate;
@property (retain) NSNumber * Status;
@property (retain) NSString * TrainingProgram;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileTraining : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileTraining;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileTraining *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileTraining:(tns1_EmpProfileTraining *)toAdd;
@property (readonly) NSMutableArray * EmpProfileTraining;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileWageType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileWageTypeId;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ImplementationDate;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * TimeWageTypeCode;
	NSNumber * TimeWageTypeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileWageType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileWageTypeId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * TimeWageTypeCode;
@property (retain) NSNumber * TimeWageTypeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileWageType : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileWageType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileWageType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileWageType:(tns1_EmpProfileWageType *)toAdd;
@property (readonly) NSMutableArray * EmpProfileWageType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpProfileWorkingForm : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileWorkingFormId;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ImplementationDate;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * TimeWorkingFormCode;
	NSNumber * TimeWorkingFormId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpProfileWorkingForm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileWorkingFormId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * TimeWorkingFormCode;
@property (retain) NSNumber * TimeWorkingFormId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpProfileWorkingForm : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileWorkingForm;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpProfileWorkingForm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileWorkingForm:(tns1_EmpProfileWorkingForm *)toAdd;
@property (readonly) NSMutableArray * EmpProfileWorkingForm;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_EmpSocialInsuranceSalary : NSObject {
	
/* elements */
	NSNumber * CLogCurrencyId;
	NSNumber * CLogCurrencyRateId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * DecisionCode;
	NSString * DecisionName;
	NSNumber * EmpSocialInsuranceSalaryId;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSDate * ExpirationDate;
	NSString * FileDecisionUrl;
	NSDate * ImplementationDate;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	USBoolean * IsIncrease;
	USBoolean * IsRecall;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * Reason;
	NSDate * RecallMonth;
	NSDate * SocialInsuranceDate;
	NSString * cEmpSISalary;
	NSString * cEmpSISalaryCoeficient;
	NSString * cEmpSISalaryGrade;
	NSNumber * nEmpSISalary;
	NSNumber * nEmpSISalaryCoeficient;
	NSNumber * nEmpSISalaryGrade;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_EmpSocialInsuranceSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogCurrencyId;
@property (retain) NSNumber * CLogCurrencyRateId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DecisionCode;
@property (retain) NSString * DecisionName;
@property (retain) NSNumber * EmpSocialInsuranceSalaryId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSString * FileDecisionUrl;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsIncrease;
@property (retain) USBoolean * IsRecall;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * Reason;
@property (retain) NSDate * RecallMonth;
@property (retain) NSDate * SocialInsuranceDate;
@property (retain) NSString * cEmpSISalary;
@property (retain) NSString * cEmpSISalaryCoeficient;
@property (retain) NSString * cEmpSISalaryGrade;
@property (retain) NSNumber * nEmpSISalary;
@property (retain) NSNumber * nEmpSISalaryCoeficient;
@property (retain) NSNumber * nEmpSISalaryGrade;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfEmpSocialInsuranceSalary : NSObject {
	
/* elements */
	NSMutableArray *EmpSocialInsuranceSalary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfEmpSocialInsuranceSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpSocialInsuranceSalary:(tns1_EmpSocialInsuranceSalary *)toAdd;
@property (readonly) NSMutableArray * EmpSocialInsuranceSalary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_NCClientConnection : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * ConnectionId;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSNumber * NCClientConnectionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_NCClientConnection *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ConnectionId;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * NCClientConnectionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfNCClientConnection : NSObject {
	
/* elements */
	NSMutableArray *NCClientConnection;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfNCClientConnection *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addNCClientConnection:(tns1_NCClientConnection *)toAdd;
@property (readonly) NSMutableArray * NCClientConnection;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_NCMessageType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * NCMessageTypeId;
	tns1_ArrayOfNCMessage * NCMessages;
	NSString * Name;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_NCMessageType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NCMessageTypeId;
@property (retain) tns1_ArrayOfNCMessage * NCMessages;
@property (retain) NSString * Name;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_NCMessage : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_Employee * Employee;
	tns1_Employee * Employee1;
	NSString * FullMessage;
	USBoolean * IsDeleted;
	USBoolean * IsNew;
	NSDate * ModifiedDate;
	NSNumber * NCMessageId;
	tns1_NCMessageType * NCMessageType;
	NSNumber * NCMessageTypeId;
	NSNumber * ReceiverEmployeeId;
	NSNumber * SenderEmployeeId;
	NSString * ShortcutMessage;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_NCMessage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_Employee * Employee;
@property (retain) tns1_Employee * Employee1;
@property (retain) NSString * FullMessage;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsNew;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NCMessageId;
@property (retain) tns1_NCMessageType * NCMessageType;
@property (retain) NSNumber * NCMessageTypeId;
@property (retain) NSNumber * ReceiverEmployeeId;
@property (retain) NSNumber * SenderEmployeeId;
@property (retain) NSString * ShortcutMessage;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfNCMessage : NSObject {
	
/* elements */
	NSMutableArray *NCMessage;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfNCMessage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addNCMessage:(tns1_NCMessage *)toAdd;
@property (readonly) NSMutableArray * NCMessage;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_Project : NSObject {
	
/* elements */
	NSString * Description;
	tns1_Employee * Employee;
	NSDate * EndDate;
	NSNumber * Id_;
	NSString * Name;
	NSNumber * OwnerId;
	NSNumber * Progress;
	tns1_ArrayOfProjectMember * ProjectMembers;
	NSDate * StartDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_Project *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Description;
@property (retain) tns1_Employee * Employee;
@property (retain) NSDate * EndDate;
@property (retain) NSNumber * Id_;
@property (retain) NSString * Name;
@property (retain) NSNumber * OwnerId;
@property (retain) NSNumber * Progress;
@property (retain) tns1_ArrayOfProjectMember * ProjectMembers;
@property (retain) NSDate * StartDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ProjectMember : NSObject {
	
/* elements */
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSNumber * Id_;
	tns1_Project * Project;
	NSNumber * ProjectId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ProjectMember *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * Id_;
@property (retain) tns1_Project * Project;
@property (retain) NSNumber * ProjectId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfProjectMember : NSObject {
	
/* elements */
	NSMutableArray *ProjectMember;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfProjectMember *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addProjectMember:(tns1_ProjectMember *)toAdd;
@property (readonly) NSMutableArray * ProjectMember;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfProject : NSObject {
	
/* elements */
	NSMutableArray *Project;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfProject *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addProject:(tns1_Project *)toAdd;
@property (readonly) NSMutableArray * Project;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ReportEmployeeRequestedBook : NSObject {
	
/* elements */
	NSString * Address1;
	NSString * Benefit;
	NSDate * BirthDay;
	NSString * CLogCityNoIns;
	NSString * CLogHospitalCode;
	tns1_Employee * Employee;
	NSString * Ethnicity;
	NSString * FullName;
	NSString * Gender;
	USBoolean * HadSIBook;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	NSString * Note;
	NSString * OrgUnitName;
	NSNumber * ReportEmployeeRequestedBookId;
	NSDate * RequestedDay;
	NSString * SICode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ReportEmployeeRequestedBook *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address1;
@property (retain) NSString * Benefit;
@property (retain) NSDate * BirthDay;
@property (retain) NSString * CLogCityNoIns;
@property (retain) NSString * CLogHospitalCode;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * Ethnicity;
@property (retain) NSString * FullName;
@property (retain) NSString * Gender;
@property (retain) USBoolean * HadSIBook;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) NSString * Note;
@property (retain) NSString * OrgUnitName;
@property (retain) NSNumber * ReportEmployeeRequestedBookId;
@property (retain) NSDate * RequestedDay;
@property (retain) NSString * SICode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SelfLeaveRequest : NSObject {
	
/* elements */
	NSString * Address;
	NSNumber * ApprovalStatus;
	NSString * ApproverCode;
	NSNumber * ApproverId;
	NSDate * CreatedDate;
	tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	USBoolean * IsCompensatoryLeave;
	USBoolean * IsDeleted;
	USBoolean * IsLeaveWithoutPay;
	USBoolean * IsPaidLeave;
	USBoolean * IsUnpaidLeave;
	NSDate * ModifiedDate;
	NSNumber * NumberDaysOff;
	NSNumber * OrganizationId;
	NSString * PhoneNumber;
	NSString * Reason;
	NSNumber * SalaryPenaltyValue;
	NSNumber * SelfLeaveRequestId;
	NSDate * StartDate;
	NSNumber * TimeAbsenceTypeId;
	NSString * WorkflowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SelfLeaveRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSNumber * ApprovalStatus;
@property (retain) NSString * ApproverCode;
@property (retain) NSNumber * ApproverId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsCompensatoryLeave;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsLeaveWithoutPay;
@property (retain) USBoolean * IsPaidLeave;
@property (retain) USBoolean * IsUnpaidLeave;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NumberDaysOff;
@property (retain) NSNumber * OrganizationId;
@property (retain) NSString * PhoneNumber;
@property (retain) NSString * Reason;
@property (retain) NSNumber * SalaryPenaltyValue;
@property (retain) NSNumber * SelfLeaveRequestId;
@property (retain) NSDate * StartDate;
@property (retain) NSNumber * TimeAbsenceTypeId;
@property (retain) NSString * WorkflowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSelfLeaveRequest : NSObject {
	
/* elements */
	NSMutableArray *SelfLeaveRequest;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSelfLeaveRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSelfLeaveRequest:(tns1_SelfLeaveRequest *)toAdd;
@property (readonly) NSMutableArray * SelfLeaveRequest;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysRecPlanApprover : NSObject {
	
/* elements */
	NSMutableArray *SysRecPlanApprover;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysRecPlanApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysRecPlanApprover:(tns1_SysRecPlanApprover *)toAdd;
@property (readonly) NSMutableArray * SysRecPlanApprover;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysRecRequirementApprover : NSObject {
	
/* elements */
	NSMutableArray *SysRecRequirementApprover;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysRecRequirementApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysRecRequirementApprover:(tns1_SysRecRequirementApprover *)toAdd;
@property (readonly) NSMutableArray * SysRecRequirementApprover;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_TrainingEmpPlan : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsSubmited;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * State;
	NSNumber * StrategyGoalId;
	NSString * Target;
	tns1_ArrayOfTrainingEmpPlanApproved * TrainingEmpPlanApproveds;
	NSNumber * TrainingEmpPlanId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_TrainingEmpPlan *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsSubmited;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * State;
@property (retain) NSNumber * StrategyGoalId;
@property (retain) NSString * Target;
@property (retain) tns1_ArrayOfTrainingEmpPlanApproved * TrainingEmpPlanApproveds;
@property (retain) NSNumber * TrainingEmpPlanId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_TrainingEmpPlanApproved : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	tns1_SysTrainingApprover * SysTrainingApprover;
	NSNumber * SysTrainingApproverId;
	tns1_TrainingEmpPlan * TrainingEmpPlan;
	NSNumber * TrainingEmpPlanApprovedId;
	NSNumber * TrainingEmpPlanId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_TrainingEmpPlanApproved *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_SysTrainingApprover * SysTrainingApprover;
@property (retain) NSNumber * SysTrainingApproverId;
@property (retain) tns1_TrainingEmpPlan * TrainingEmpPlan;
@property (retain) NSNumber * TrainingEmpPlanApprovedId;
@property (retain) NSNumber * TrainingEmpPlanId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfTrainingEmpPlanApproved : NSObject {
	
/* elements */
	NSMutableArray *TrainingEmpPlanApproved;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfTrainingEmpPlanApproved *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingEmpPlanApproved:(tns1_TrainingEmpPlanApproved *)toAdd;
@property (readonly) NSMutableArray * TrainingEmpPlanApproved;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysTrainingApprover : NSObject {
	
/* elements */
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	tns1_Employee * Employee;
	USBoolean * IsDeleted;
	NSNumber * Number;
	NSNumber * SysTrainingApproverId;
	tns1_ArrayOfTrainingEmpPlanApproved * TrainingEmpPlanApproveds;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysTrainingApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) tns1_Employee * Employee;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Number;
@property (retain) NSNumber * SysTrainingApproverId;
@property (retain) tns1_ArrayOfTrainingEmpPlanApproved * TrainingEmpPlanApproveds;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysTrainingApprover : NSObject {
	
/* elements */
	NSMutableArray *SysTrainingApprover;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysTrainingApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysTrainingApprover:(tns1_SysTrainingApprover *)toAdd;
@property (readonly) NSMutableArray * SysTrainingApprover;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_Task : NSObject {
	
/* elements */
	NSString * Description;
	tns1_Employee * Employee;
	NSDate * FromDate;
	NSNumber * Id_;
	NSString * Name;
	NSNumber * Process;
	NSNumber * ProjectId;
	NSNumber * Status;
	NSNumber * TaskOwnerId;
	NSDate * ToDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_Task *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Description;
@property (retain) tns1_Employee * Employee;
@property (retain) NSDate * FromDate;
@property (retain) NSNumber * Id_;
@property (retain) NSString * Name;
@property (retain) NSNumber * Process;
@property (retain) NSNumber * ProjectId;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * TaskOwnerId;
@property (retain) NSDate * ToDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfTask : NSObject {
	
/* elements */
	NSMutableArray *Task;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfTask *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTask:(tns1_Task *)toAdd;
@property (readonly) NSMutableArray * Task;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfTrainingEmpPlan : NSObject {
	
/* elements */
	NSMutableArray *TrainingEmpPlan;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfTrainingEmpPlan *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingEmpPlan:(tns1_TrainingEmpPlan *)toAdd;
@property (readonly) NSMutableArray * TrainingEmpPlan;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_Employee : NSObject {
	
/* elements */
	NSDate * BirthDay;
	NSString * BirthPlace;
	NSNumber * BirthPlaceId;
	tns1_ArrayOfCBAccidentInsurance * CBAccidentInsurances;
	tns1_ArrayOfCBCompensationEmployeeGroupDetail * CBCompensationEmployeeGroupDetails;
	tns1_ArrayOfCBConvalescence * CBConvalescences;
	tns1_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
	tns1_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
	tns1_ArrayOfCBHealthInsuranceDetail * CBHealthInsuranceDetails;
	tns1_CLogCity * CLogCity;
	tns1_CLogCity * CLogCity1;
	tns1_CLogEmployeeType * CLogEmployeeType;
	tns1_ArrayOfCLogTrainer * CLogTrainers;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	tns1_ArrayOfEmpBasicProfile * EmpBasicProfiles;
	tns1_ArrayOfEmpCompetency * EmpCompetencies;
	tns1_ArrayOfEmpCompetencyRating * EmpCompetencyRatings;
	tns1_ArrayOfEmpContract * EmpContracts;
	tns1_ArrayOfEmpPerformanceAppraisal * EmpPerformanceAppraisals;
	tns1_ArrayOfEmpProfileAllowance * EmpProfileAllowances;
	tns1_ArrayOfEmpProfileBaseSalary * EmpProfileBaseSalaries;
	tns1_ArrayOfEmpProfileBenefit * EmpProfileBenefits;
	tns1_ArrayOfEmpProfileComment * EmpProfileComments;
	tns1_ArrayOfEmpProfileComment * EmpProfileComments1;
	tns1_ArrayOfEmpProfileComputingSkill * EmpProfileComputingSkills;
	tns1_ArrayOfEmpProfileContact * EmpProfileContacts;
	tns1_ArrayOfEmpProfileDegree * EmpProfileDegrees;
	tns1_ArrayOfEmpProfileDiscipline * EmpProfileDisciplines;
	tns1_ArrayOfEmpProfileEducation * EmpProfileEducations;
	tns1_ArrayOfEmpProfileEquipment * EmpProfileEquipments;
	tns1_ArrayOfEmpProfileExperience * EmpProfileExperiences;
	tns1_ArrayOfEmpProfileFamilyRelationship * EmpProfileFamilyRelationships;
	tns1_ArrayOfEmpProfileForeignLanguage * EmpProfileForeignLanguages;
	tns1_ArrayOfEmpProfileHealthInsurance * EmpProfileHealthInsurances;
	tns1_ArrayOfEmpProfileHealthy * EmpProfileHealthies;
	tns1_ArrayOfEmpProfileHobby * EmpProfileHobbies;
	tns1_ArrayOfEmpProfileInternalCourse * EmpProfileInternalCourses;
	tns1_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions;
	tns1_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions1;
	tns1_ArrayOfEmpProfileLeaveRegime * EmpProfileLeaveRegimes;
	tns1_ArrayOfEmpProfileOtherBenefit * EmpProfileOtherBenefits;
	tns1_ArrayOfEmpProfileParticipation * EmpProfileParticipations;
	tns1_ArrayOfEmpProfilePersonality * EmpProfilePersonalities;
	tns1_ArrayOfEmpProfileProcessOfWork * EmpProfileProcessOfWorks;
	tns1_ArrayOfEmpProfileQualification * EmpProfileQualifications;
	tns1_ArrayOfEmpProfileReward * EmpProfileRewards;
	tns1_ArrayOfEmpProfileSkill * EmpProfileSkills;
	tns1_ArrayOfEmpProfileTotalIncome * EmpProfileTotalIncomes;
	tns1_ArrayOfEmpProfileTraining * EmpProfileTrainings;
	tns1_ArrayOfEmpProfileWageType * EmpProfileWageTypes;
	tns1_ArrayOfEmpProfileWorkingExperience * EmpProfileWorkingExperiences;
	tns1_ArrayOfEmpProfileWorkingForm * EmpProfileWorkingForms;
	tns1_ArrayOfEmpSocialInsuranceSalary * EmpSocialInsuranceSalaries;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * EmployeeTKCode;
	NSNumber * EmployeeTypeId;
	NSDate * EndWorkingDate;
	NSDate * EntryDate;
	NSString * Ethnicity;
	NSNumber * EthnicityId;
	NSString * FirstName;
	NSString * FullName;
	tns1_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals;
	tns1_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals1;
	tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
	tns1_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
	tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
	tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
	tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
	tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives1;
	tns1_ArrayOfGPObjectiveInitiative * GPObjectiveInitiatives;
	tns1_ArrayOfGPPerformanceYearEndResult * GPPerformanceYearEndResults;
	USBoolean * Gender;
	NSString * HomeTown;
	NSNumber * HomeTownId;
	NSString * IdentityCardNo;
	NSString * ImageUrl;
	USBoolean * IsDeleted;
	USBoolean * IsOnlyYearOfBirthday;
	tns1_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
	tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
	tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts1;
	NSString * LastName;
	NSString * MainLanguage;
	NSNumber * MainLanguageId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	tns1_ArrayOfNCClientConnection * NCClientConnections;
	tns1_ArrayOfNCMessage * NCMessages;
	tns1_ArrayOfNCMessage * NCMessages1;
	NSDate * OfficialDate;
	NSString * OrgGroupCd;
	NSNumber * OrgGroupId;
	NSDate * ProbationaryDate;
	NSDate * ProbationaryEndDate;
	tns1_ArrayOfProjectMember * ProjectMembers;
	tns1_ArrayOfProject * Projects;
	tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
	tns1_ArrayOfRecInterviewer * RecInterviewers;
	tns1_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
	tns1_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
	tns1_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
	NSString * Religion;
	NSNumber * ReligionId;
	tns1_ReportEmployeeRequestedBook * ReportEmployeeRequestedBook;
	tns1_ArrayOfSelfLeaveRequest * SelfLeaveRequests;
	tns1_ArrayOfSysRecPlanApprover * SysRecPlanApprovers;
	tns1_ArrayOfSysRecRequirementApprover * SysRecRequirementApprovers;
	tns1_ArrayOfSysTrainingApprover * SysTrainingApprovers;
	tns1_ArrayOfTask * Tasks;
	tns1_ArrayOfTrainingCourseEmployee * TrainingCourseEmployees;
	tns1_ArrayOfTrainingEmpPlan * TrainingEmpPlans;
	tns1_ArrayOfTrainingPlanEmployee * TrainingPlanEmployees;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_Employee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * BirthDay;
@property (retain) NSString * BirthPlace;
@property (retain) NSNumber * BirthPlaceId;
@property (retain) tns1_ArrayOfCBAccidentInsurance * CBAccidentInsurances;
@property (retain) tns1_ArrayOfCBCompensationEmployeeGroupDetail * CBCompensationEmployeeGroupDetails;
@property (retain) tns1_ArrayOfCBConvalescence * CBConvalescences;
@property (retain) tns1_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
@property (retain) tns1_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
@property (retain) tns1_ArrayOfCBHealthInsuranceDetail * CBHealthInsuranceDetails;
@property (retain) tns1_CLogCity * CLogCity;
@property (retain) tns1_CLogCity * CLogCity1;
@property (retain) tns1_CLogEmployeeType * CLogEmployeeType;
@property (retain) tns1_ArrayOfCLogTrainer * CLogTrainers;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) tns1_ArrayOfEmpBasicProfile * EmpBasicProfiles;
@property (retain) tns1_ArrayOfEmpCompetency * EmpCompetencies;
@property (retain) tns1_ArrayOfEmpCompetencyRating * EmpCompetencyRatings;
@property (retain) tns1_ArrayOfEmpContract * EmpContracts;
@property (retain) tns1_ArrayOfEmpPerformanceAppraisal * EmpPerformanceAppraisals;
@property (retain) tns1_ArrayOfEmpProfileAllowance * EmpProfileAllowances;
@property (retain) tns1_ArrayOfEmpProfileBaseSalary * EmpProfileBaseSalaries;
@property (retain) tns1_ArrayOfEmpProfileBenefit * EmpProfileBenefits;
@property (retain) tns1_ArrayOfEmpProfileComment * EmpProfileComments;
@property (retain) tns1_ArrayOfEmpProfileComment * EmpProfileComments1;
@property (retain) tns1_ArrayOfEmpProfileComputingSkill * EmpProfileComputingSkills;
@property (retain) tns1_ArrayOfEmpProfileContact * EmpProfileContacts;
@property (retain) tns1_ArrayOfEmpProfileDegree * EmpProfileDegrees;
@property (retain) tns1_ArrayOfEmpProfileDiscipline * EmpProfileDisciplines;
@property (retain) tns1_ArrayOfEmpProfileEducation * EmpProfileEducations;
@property (retain) tns1_ArrayOfEmpProfileEquipment * EmpProfileEquipments;
@property (retain) tns1_ArrayOfEmpProfileExperience * EmpProfileExperiences;
@property (retain) tns1_ArrayOfEmpProfileFamilyRelationship * EmpProfileFamilyRelationships;
@property (retain) tns1_ArrayOfEmpProfileForeignLanguage * EmpProfileForeignLanguages;
@property (retain) tns1_ArrayOfEmpProfileHealthInsurance * EmpProfileHealthInsurances;
@property (retain) tns1_ArrayOfEmpProfileHealthy * EmpProfileHealthies;
@property (retain) tns1_ArrayOfEmpProfileHobby * EmpProfileHobbies;
@property (retain) tns1_ArrayOfEmpProfileInternalCourse * EmpProfileInternalCourses;
@property (retain) tns1_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions;
@property (retain) tns1_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions1;
@property (retain) tns1_ArrayOfEmpProfileLeaveRegime * EmpProfileLeaveRegimes;
@property (retain) tns1_ArrayOfEmpProfileOtherBenefit * EmpProfileOtherBenefits;
@property (retain) tns1_ArrayOfEmpProfileParticipation * EmpProfileParticipations;
@property (retain) tns1_ArrayOfEmpProfilePersonality * EmpProfilePersonalities;
@property (retain) tns1_ArrayOfEmpProfileProcessOfWork * EmpProfileProcessOfWorks;
@property (retain) tns1_ArrayOfEmpProfileQualification * EmpProfileQualifications;
@property (retain) tns1_ArrayOfEmpProfileReward * EmpProfileRewards;
@property (retain) tns1_ArrayOfEmpProfileSkill * EmpProfileSkills;
@property (retain) tns1_ArrayOfEmpProfileTotalIncome * EmpProfileTotalIncomes;
@property (retain) tns1_ArrayOfEmpProfileTraining * EmpProfileTrainings;
@property (retain) tns1_ArrayOfEmpProfileWageType * EmpProfileWageTypes;
@property (retain) tns1_ArrayOfEmpProfileWorkingExperience * EmpProfileWorkingExperiences;
@property (retain) tns1_ArrayOfEmpProfileWorkingForm * EmpProfileWorkingForms;
@property (retain) tns1_ArrayOfEmpSocialInsuranceSalary * EmpSocialInsuranceSalaries;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * EmployeeTKCode;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSDate * EndWorkingDate;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * Ethnicity;
@property (retain) NSNumber * EthnicityId;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) tns1_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals;
@property (retain) tns1_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals1;
@property (retain) tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
@property (retain) tns1_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
@property (retain) tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
@property (retain) tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
@property (retain) tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
@property (retain) tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives1;
@property (retain) tns1_ArrayOfGPObjectiveInitiative * GPObjectiveInitiatives;
@property (retain) tns1_ArrayOfGPPerformanceYearEndResult * GPPerformanceYearEndResults;
@property (retain) USBoolean * Gender;
@property (retain) NSString * HomeTown;
@property (retain) NSNumber * HomeTownId;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) tns1_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
@property (retain) tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
@property (retain) tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts1;
@property (retain) NSString * LastName;
@property (retain) NSString * MainLanguage;
@property (retain) NSNumber * MainLanguageId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) tns1_ArrayOfNCClientConnection * NCClientConnections;
@property (retain) tns1_ArrayOfNCMessage * NCMessages;
@property (retain) tns1_ArrayOfNCMessage * NCMessages1;
@property (retain) NSDate * OfficialDate;
@property (retain) NSString * OrgGroupCd;
@property (retain) NSNumber * OrgGroupId;
@property (retain) NSDate * ProbationaryDate;
@property (retain) NSDate * ProbationaryEndDate;
@property (retain) tns1_ArrayOfProjectMember * ProjectMembers;
@property (retain) tns1_ArrayOfProject * Projects;
@property (retain) tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
@property (retain) tns1_ArrayOfRecInterviewer * RecInterviewers;
@property (retain) tns1_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
@property (retain) tns1_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
@property (retain) tns1_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
@property (retain) NSString * Religion;
@property (retain) NSNumber * ReligionId;
@property (retain) tns1_ReportEmployeeRequestedBook * ReportEmployeeRequestedBook;
@property (retain) tns1_ArrayOfSelfLeaveRequest * SelfLeaveRequests;
@property (retain) tns1_ArrayOfSysRecPlanApprover * SysRecPlanApprovers;
@property (retain) tns1_ArrayOfSysRecRequirementApprover * SysRecRequirementApprovers;
@property (retain) tns1_ArrayOfSysTrainingApprover * SysTrainingApprovers;
@property (retain) tns1_ArrayOfTask * Tasks;
@property (retain) tns1_ArrayOfTrainingCourseEmployee * TrainingCourseEmployees;
@property (retain) tns1_ArrayOfTrainingEmpPlan * TrainingEmpPlans;
@property (retain) tns1_ArrayOfTrainingPlanEmployee * TrainingPlanEmployees;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_V_SysUserEmployee : NSObject {
	
/* elements */
	NSString * CAREER_NAME_EN;
	NSString * CAREER_NAME_VN;
	NSString * CLOG_CAREER_CODE;
	NSString * CompanyCode;
	NSNumber * CompanyId;
	NSString * CompanyName;
	NSString * DomainName;
	NSString * Email;
	NSString * EmpWorkingStatusCode;
	NSString * EmpWorkingStatusName;
	NSString * EmpWorkingStatusNameEN;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EmployeeTypeId;
	NSString * EmployeeTypeName;
	NSString * EmployeeTypeNameEN;
	NSDate * EntryDate;
	NSString * FullName;
	USBoolean * Gender;
	NSString * IdentityCardNo;
	NSString * ImageUrl;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSString * MobileNumber;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobNameEN;
	NSNumber * OrgJobPositionId;
	NSString * OrgJobPositionName;
	NSString * OrgJobPositionNameEN;
	NSString * OrgJobTitleCode;
	NSNumber * OrgJobTitleId;
	NSString * OrgJobTitleLevelCode;
	NSString * OrgJobTitleLevelNameEN;
	NSString * OrgJobTitleLevelNameVN;
	NSString * OrgJobTitleNameEN;
	NSString * OrgJobTitleNameVN;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSNumber * OrgWorkLevelId;
	NSNumber * SysUserId;
	NSString * UnitName;
	NSString * UnitNameEN;
	NSString * Username;
	NSString * WorkLevelName;
	NSString * WorkLevelNameEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_V_SysUserEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CAREER_NAME_EN;
@property (retain) NSString * CAREER_NAME_VN;
@property (retain) NSString * CLOG_CAREER_CODE;
@property (retain) NSString * CompanyCode;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompanyName;
@property (retain) NSString * DomainName;
@property (retain) NSString * Email;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmpWorkingStatusNameEN;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSString * EmployeeTypeName;
@property (retain) NSString * EmployeeTypeNameEN;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * MobileNumber;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobNameEN;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSString * OrgJobPositionNameEN;
@property (retain) NSString * OrgJobTitleCode;
@property (retain) NSNumber * OrgJobTitleId;
@property (retain) NSString * OrgJobTitleLevelCode;
@property (retain) NSString * OrgJobTitleLevelNameEN;
@property (retain) NSString * OrgJobTitleLevelNameVN;
@property (retain) NSString * OrgJobTitleNameEN;
@property (retain) NSString * OrgJobTitleNameVN;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSNumber * SysUserId;
@property (retain) NSString * UnitName;
@property (retain) NSString * UnitNameEN;
@property (retain) NSString * Username;
@property (retain) NSString * WorkLevelName;
@property (retain) NSString * WorkLevelNameEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfV_SysUserEmployee : NSObject {
	
/* elements */
	NSMutableArray *V_SysUserEmployee;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfV_SysUserEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_SysUserEmployee:(tns1_V_SysUserEmployee *)toAdd;
@property (readonly) NSMutableArray * V_SysUserEmployee;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_Company : NSObject {
	
/* elements */
	NSString * AccountNo;
	NSString * Address;
	NSNumber * AddressId;
	NSString * BankBranch;
	NSString * CompanyCode;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Fax;
	NSNumber * Female;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * PhoneNumber;
	NSNumber * TSalaryFund;
	NSNumber * TotalEmployees;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_Company *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AccountNo;
@property (retain) NSString * Address;
@property (retain) NSNumber * AddressId;
@property (retain) NSString * BankBranch;
@property (retain) NSString * CompanyCode;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Fax;
@property (retain) NSNumber * Female;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * PhoneNumber;
@property (retain) NSNumber * TSalaryFund;
@property (retain) NSNumber * TotalEmployees;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfCompany : NSObject {
	
/* elements */
	NSMutableArray *Company;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfCompany *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCompany:(tns1_Company *)toAdd;
@property (readonly) NSMutableArray * Company;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysGroupMenu : NSObject {
	
/* elements */
	NSString * Action;
	NSString * ApplicationCodeAction;
	NSString * Controller;
	NSString * Icon;
	USBoolean * IsDeleted;
	NSNumber * ParentId;
	NSNumber * Priority;
	tns1_SysApplication * SysApplication;
	NSString * SysApplicationCode;
	NSNumber * SysApplicationId;
	NSNumber * SysGroupMenuId;
	NSString * SysGroupMenuName;
	NSString * Url;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysGroupMenu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Action;
@property (retain) NSString * ApplicationCodeAction;
@property (retain) NSString * Controller;
@property (retain) NSString * Icon;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ParentId;
@property (retain) NSNumber * Priority;
@property (retain) tns1_SysApplication * SysApplication;
@property (retain) NSString * SysApplicationCode;
@property (retain) NSNumber * SysApplicationId;
@property (retain) NSNumber * SysGroupMenuId;
@property (retain) NSString * SysGroupMenuName;
@property (retain) NSString * Url;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysGroupMenu : NSObject {
	
/* elements */
	NSMutableArray *SysGroupMenu;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysGroupMenu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysGroupMenu:(tns1_SysGroupMenu *)toAdd;
@property (readonly) NSMutableArray * SysGroupMenu;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysApplication : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * Description;
	NSString * Icon;
	NSString * Name;
	NSString * ProductCode;
	NSString * SysApplicationCode;
	NSNumber * SysApplicationId;
	tns1_ArrayOfSysGroupMenu * SysGroupMenus;
	NSString * Url;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysApplication *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Description;
@property (retain) NSString * Icon;
@property (retain) NSString * Name;
@property (retain) NSString * ProductCode;
@property (retain) NSString * SysApplicationCode;
@property (retain) NSNumber * SysApplicationId;
@property (retain) tns1_ArrayOfSysGroupMenu * SysGroupMenus;
@property (retain) NSString * Url;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysApplication : NSObject {
	
/* elements */
	NSMutableArray *SysApplication;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysApplication *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysApplication:(tns1_SysApplication *)toAdd;
@property (readonly) NSMutableArray * SysApplication;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysModule : NSObject {
	
/* elements */
	NSString * Description;
	NSString * Name;
	NSNumber * SysApplicationId;
	NSNumber * SysGroupMenuId;
	NSString * SysModuleCode;
	NSNumber * SysModuleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysModule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Description;
@property (retain) NSString * Name;
@property (retain) NSNumber * SysApplicationId;
@property (retain) NSNumber * SysGroupMenuId;
@property (retain) NSString * SysModuleCode;
@property (retain) NSNumber * SysModuleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysModule : NSObject {
	
/* elements */
	NSMutableArray *SysModule;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysModule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysModule:(tns1_SysModule *)toAdd;
@property (readonly) NSMutableArray * SysModule;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysFeature : NSObject {
	
/* elements */
	NSString * Description;
	NSString * Name;
	NSString * SysFeatureCode;
	NSNumber * SysFeatureId;
	NSNumber * SysMenuId;
	NSNumber * SysModuleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysFeature *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Description;
@property (retain) NSString * Name;
@property (retain) NSString * SysFeatureCode;
@property (retain) NSNumber * SysFeatureId;
@property (retain) NSNumber * SysMenuId;
@property (retain) NSNumber * SysModuleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysFeature : NSObject {
	
/* elements */
	NSMutableArray *SysFeature;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysFeature *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysFeature:(tns1_SysFeature *)toAdd;
@property (readonly) NSMutableArray * SysFeature;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysAction : NSObject {
	
/* elements */
	NSNumber * SysActionId;
	NSNumber * SysActionTypeId;
	NSNumber * SysFeatureId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysAction *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * SysActionId;
@property (retain) NSNumber * SysActionTypeId;
@property (retain) NSNumber * SysFeatureId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysAction : NSObject {
	
/* elements */
	NSMutableArray *SysAction;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysAction *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysAction:(tns1_SysAction *)toAdd;
@property (readonly) NSMutableArray * SysAction;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_V_SysAction : NSObject {
	
/* elements */
	NSString * ActionTypeName;
	NSNumber * SysActionId;
	NSNumber * SysActionTypeId;
	NSNumber * SysFeatureId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_V_SysAction *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ActionTypeName;
@property (retain) NSNumber * SysActionId;
@property (retain) NSNumber * SysActionTypeId;
@property (retain) NSNumber * SysFeatureId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfV_SysAction : NSObject {
	
/* elements */
	NSMutableArray *V_SysAction;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfV_SysAction *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_SysAction:(tns1_V_SysAction *)toAdd;
@property (readonly) NSMutableArray * V_SysAction;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysLink : NSObject {
	
/* elements */
	NSString * Action;
	NSString * Controller;
	NSNumber * SysActionId;
	NSNumber * SysLinkId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysLink *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Action;
@property (retain) NSString * Controller;
@property (retain) NSNumber * SysActionId;
@property (retain) NSNumber * SysLinkId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysLink : NSObject {
	
/* elements */
	NSMutableArray *SysLink;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysLink *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysLink:(tns1_SysLink *)toAdd;
@property (readonly) NSMutableArray * SysLink;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysActionType : NSObject {
	
/* elements */
	NSString * Name;
	NSString * SysActionTypeCode;
	NSNumber * SysActionTypeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysActionType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Name;
@property (retain) NSString * SysActionTypeCode;
@property (retain) NSNumber * SysActionTypeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysActionType : NSObject {
	
/* elements */
	NSMutableArray *SysActionType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysActionType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysActionType:(tns1_SysActionType *)toAdd;
@property (readonly) NSMutableArray * SysActionType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysUserLog : NSObject {
	
/* elements */
	NSString * HostName;
	NSString * IPAddress;
	NSDate * LogTime;
	NSString * SessionId;
	NSNumber * SysUserId;
	NSNumber * SysUserLogId;
	NSString * Type;
	NSString * Username;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysUserLog *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * HostName;
@property (retain) NSString * IPAddress;
@property (retain) NSDate * LogTime;
@property (retain) NSString * SessionId;
@property (retain) NSNumber * SysUserId;
@property (retain) NSNumber * SysUserLogId;
@property (retain) NSString * Type;
@property (retain) NSString * Username;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysUserPermission : NSObject {
	
/* elements */
	NSNumber * SysActionId;
	NSNumber * SysUserId;
	NSNumber * SysUserPermissionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysUserPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * SysActionId;
@property (retain) NSNumber * SysUserId;
@property (retain) NSNumber * SysUserPermissionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysUserPermission : NSObject {
	
/* elements */
	NSMutableArray *SysUserPermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysUserPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysUserPermission:(tns1_SysUserPermission *)toAdd;
@property (readonly) NSMutableArray * SysUserPermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysRolePermission : NSObject {
	
/* elements */
	NSNumber * SysActionId;
	NSNumber * SysRoleId;
	NSNumber * SysRolePermissionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysRolePermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * SysActionId;
@property (retain) NSNumber * SysRoleId;
@property (retain) NSNumber * SysRolePermissionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysRolePermission : NSObject {
	
/* elements */
	NSMutableArray *SysRolePermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysRolePermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysRolePermission:(tns1_SysRolePermission *)toAdd;
@property (readonly) NSMutableArray * SysRolePermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysGroupPermission : NSObject {
	
/* elements */
	NSNumber * SysActionId;
	NSNumber * SysGroupId;
	NSNumber * SysGroupPermissionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysGroupPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * SysActionId;
@property (retain) NSNumber * SysGroupId;
@property (retain) NSNumber * SysGroupPermissionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysGroupPermission : NSObject {
	
/* elements */
	NSMutableArray *SysGroupPermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysGroupPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysGroupPermission:(tns1_SysGroupPermission *)toAdd;
@property (readonly) NSMutableArray * SysGroupPermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysUserInRole : NSObject {
	
/* elements */
	NSNumber * SysRoleId;
	NSNumber * SysUserId;
	NSNumber * SysUserInRoleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysUserInRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * SysRoleId;
@property (retain) NSNumber * SysUserId;
@property (retain) NSNumber * SysUserInRoleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysUserInRole : NSObject {
	
/* elements */
	NSMutableArray *SysUserInRole;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysUserInRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysUserInRole:(tns1_SysUserInRole *)toAdd;
@property (readonly) NSMutableArray * SysUserInRole;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysGroupInRole : NSObject {
	
/* elements */
	NSNumber * SysGroupId;
	NSNumber * SysGroupInRoleId;
	NSNumber * SysRoleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysGroupInRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * SysGroupId;
@property (retain) NSNumber * SysGroupInRoleId;
@property (retain) NSNumber * SysRoleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysGroupInRole : NSObject {
	
/* elements */
	NSMutableArray *SysGroupInRole;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysGroupInRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysGroupInRole:(tns1_SysGroupInRole *)toAdd;
@property (readonly) NSMutableArray * SysGroupInRole;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysUserInGroup : NSObject {
	
/* elements */
	NSNumber * SysGroupId;
	NSNumber * SysUserId;
	NSNumber * SysUserInGroupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysUserInGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * SysGroupId;
@property (retain) NSNumber * SysUserId;
@property (retain) NSNumber * SysUserInGroupId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysUserInGroup : NSObject {
	
/* elements */
	NSMutableArray *SysUserInGroup;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysUserInGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysUserInGroup:(tns1_SysUserInGroup *)toAdd;
@property (readonly) NSMutableArray * SysUserInGroup;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysUserOrgUnitPermission : NSObject {
	
/* elements */
	NSNumber * OrgUnitId;
	NSNumber * SysUserId;
	NSNumber * SysUserOrgUnitPermissionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysUserOrgUnitPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * SysUserId;
@property (retain) NSNumber * SysUserOrgUnitPermissionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysUserOrgUnitPermission : NSObject {
	
/* elements */
	NSMutableArray *SysUserOrgUnitPermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysUserOrgUnitPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysUserOrgUnitPermission:(tns1_SysUserOrgUnitPermission *)toAdd;
@property (readonly) NSMutableArray * SysUserOrgUnitPermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysRoleOrgUnitPermission : NSObject {
	
/* elements */
	NSNumber * OrgUnitId;
	NSNumber * SysRoleId;
	NSNumber * SysRoleOrgUnitPermissionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysRoleOrgUnitPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * SysRoleId;
@property (retain) NSNumber * SysRoleOrgUnitPermissionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysRoleOrgUnitPermission : NSObject {
	
/* elements */
	NSMutableArray *SysRoleOrgUnitPermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysRoleOrgUnitPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysRoleOrgUnitPermission:(tns1_SysRoleOrgUnitPermission *)toAdd;
@property (readonly) NSMutableArray * SysRoleOrgUnitPermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SysGroupOrgUnitPermission : NSObject {
	
/* elements */
	NSNumber * OrgUnitId;
	NSNumber * SysGroupId;
	NSNumber * SysGroupOrgUnitPermissionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SysGroupOrgUnitPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * SysGroupId;
@property (retain) NSNumber * SysGroupOrgUnitPermissionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysGroupOrgUnitPermission : NSObject {
	
/* elements */
	NSMutableArray *SysGroupOrgUnitPermission;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysGroupOrgUnitPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysGroupOrgUnitPermission:(tns1_SysGroupOrgUnitPermission *)toAdd;
@property (readonly) NSMutableArray * SysGroupOrgUnitPermission;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SP_ORG_UNIT_TREE_BY_USER_Result : NSObject {
	
/* elements */
	NSString * Address;
	NSNumber * AddressId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsActive;
	NSNumber * MasterId;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSNumber * OrgUnitLevelId;
	NSString * ParentCode;
	NSNumber * ParentId;
	NSNumber * TotalEmployees;
	NSString * WorkLocation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SP_ORG_UNIT_TREE_BY_USER_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSNumber * AddressId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsActive;
@property (retain) NSNumber * MasterId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * OrgUnitLevelId;
@property (retain) NSString * ParentCode;
@property (retain) NSNumber * ParentId;
@property (retain) NSNumber * TotalEmployees;
@property (retain) NSString * WorkLocation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSP_ORG_UNIT_TREE_BY_USER_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_ORG_UNIT_TREE_BY_USER_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSP_ORG_UNIT_TREE_BY_USER_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_ORG_UNIT_TREE_BY_USER_Result:(tns1_SP_ORG_UNIT_TREE_BY_USER_Result *)toAdd;
@property (readonly) NSMutableArray * SP_ORG_UNIT_TREE_BY_USER_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSysEmpProfileGroupLayer : NSObject {
	
/* elements */
	NSMutableArray *SysEmpProfileGroupLayer;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSysEmpProfileGroupLayer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysEmpProfileGroupLayer:(tns1_SysEmpProfileGroupLayer *)toAdd;
@property (readonly) NSMutableArray * SysEmpProfileGroupLayer;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SP_ESS_ALL_PERMIS_ON_PROFILE_LAYER_Result : NSObject {
	
/* elements */
	NSString * ADD_ACTION;
	NSString * ADD_CONTROLLER;
	NSString * ADD_PARA;
	USBoolean * ADD_PER;
	USBoolean * ADD_PER_INHERIT;
	NSString * APPROVE_ACTION;
	NSString * APPROVE_CONTROLLER;
	NSString * APPROVE_PARA;
	USBoolean * APPROVE_PER;
	USBoolean * APPROVE_PER_INHERIT;
	NSString * DEL_ACTION;
	NSString * DEL_CONTROLLER;
	NSString * DEL_PARA;
	USBoolean * DEL_PER;
	USBoolean * DEL_PER_INHERIT;
	NSString * EDIT_ACTION;
	NSString * EDIT_CONTROLLER;
	NSString * EDIT_PARA;
	USBoolean * EDIT_PER;
	USBoolean * EDIT_PER_INHERIT;
	NSString * LAYER_NAME_EN;
	NSString * LAYER_NAME_VN;
	NSString * SYS_PROFILE_LAYER_CODE;
	NSString * VIEW_ACTION;
	NSString * VIEW_CONTROLLER;
	NSString * VIEW_PARA;
	USBoolean * VIEW_PER;
	USBoolean * VIEW_PER_INHERIT;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SP_ESS_ALL_PERMIS_ON_PROFILE_LAYER_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ADD_ACTION;
@property (retain) NSString * ADD_CONTROLLER;
@property (retain) NSString * ADD_PARA;
@property (retain) USBoolean * ADD_PER;
@property (retain) USBoolean * ADD_PER_INHERIT;
@property (retain) NSString * APPROVE_ACTION;
@property (retain) NSString * APPROVE_CONTROLLER;
@property (retain) NSString * APPROVE_PARA;
@property (retain) USBoolean * APPROVE_PER;
@property (retain) USBoolean * APPROVE_PER_INHERIT;
@property (retain) NSString * DEL_ACTION;
@property (retain) NSString * DEL_CONTROLLER;
@property (retain) NSString * DEL_PARA;
@property (retain) USBoolean * DEL_PER;
@property (retain) USBoolean * DEL_PER_INHERIT;
@property (retain) NSString * EDIT_ACTION;
@property (retain) NSString * EDIT_CONTROLLER;
@property (retain) NSString * EDIT_PARA;
@property (retain) USBoolean * EDIT_PER;
@property (retain) USBoolean * EDIT_PER_INHERIT;
@property (retain) NSString * LAYER_NAME_EN;
@property (retain) NSString * LAYER_NAME_VN;
@property (retain) NSString * SYS_PROFILE_LAYER_CODE;
@property (retain) NSString * VIEW_ACTION;
@property (retain) NSString * VIEW_CONTROLLER;
@property (retain) NSString * VIEW_PARA;
@property (retain) USBoolean * VIEW_PER;
@property (retain) USBoolean * VIEW_PER_INHERIT;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSP_ESS_ALL_PERMIS_ON_PROFILE_LAYER_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_ESS_ALL_PERMIS_ON_PROFILE_LAYER_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSP_ESS_ALL_PERMIS_ON_PROFILE_LAYER_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_ESS_ALL_PERMIS_ON_PROFILE_LAYER_Result:(tns1_SP_ESS_ALL_PERMIS_ON_PROFILE_LAYER_Result *)toAdd;
@property (readonly) NSMutableArray * SP_ESS_ALL_PERMIS_ON_PROFILE_LAYER_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SP_GET_PROF_LAYER_ALL_PERMIS_Result : NSObject {
	
/* elements */
	NSString * ADD_ACTION;
	NSString * ADD_CONTROLLER;
	NSString * ADD_PARA;
	USBoolean * ADD_PER;
	USBoolean * ADD_PER_INHERIT;
	NSString * APPROVE_ACTION;
	NSString * APPROVE_CONTROLLER;
	NSString * APPROVE_PARA;
	USBoolean * APPROVE_PER;
	USBoolean * APPROVE_PER_INHERIT;
	NSString * DEL_ACTION;
	NSString * DEL_CONTROLLER;
	NSString * DEL_PARA;
	USBoolean * DEL_PER;
	USBoolean * DEL_PER_INHERIT;
	NSString * EDIT_ACTION;
	NSString * EDIT_CONTROLLER;
	NSString * EDIT_PARA;
	USBoolean * EDIT_PER;
	USBoolean * EDIT_PER_INHERIT;
	NSNumber * PROFILE_LAYER_PRIORITY;
	NSString * SYS_PROFILE_CODE;
	NSString * SYS_PROFILE_LAYER_CODE;
	NSNumber * SYS_PROFILE_LAYER_ID;
	NSString * SYS_PROFILE_LAYER_NAME_EN;
	NSString * SYS_PROFILE_LAYER_NAME_VN;
	NSString * SYS_SUB_PROFILE_CODE;
	NSString * VIEW_ACTION;
	NSString * VIEW_CONTROLLER;
	NSString * VIEW_PARA;
	USBoolean * VIEW_PER;
	USBoolean * VIEW_PER_INHERIT;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SP_GET_PROF_LAYER_ALL_PERMIS_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ADD_ACTION;
@property (retain) NSString * ADD_CONTROLLER;
@property (retain) NSString * ADD_PARA;
@property (retain) USBoolean * ADD_PER;
@property (retain) USBoolean * ADD_PER_INHERIT;
@property (retain) NSString * APPROVE_ACTION;
@property (retain) NSString * APPROVE_CONTROLLER;
@property (retain) NSString * APPROVE_PARA;
@property (retain) USBoolean * APPROVE_PER;
@property (retain) USBoolean * APPROVE_PER_INHERIT;
@property (retain) NSString * DEL_ACTION;
@property (retain) NSString * DEL_CONTROLLER;
@property (retain) NSString * DEL_PARA;
@property (retain) USBoolean * DEL_PER;
@property (retain) USBoolean * DEL_PER_INHERIT;
@property (retain) NSString * EDIT_ACTION;
@property (retain) NSString * EDIT_CONTROLLER;
@property (retain) NSString * EDIT_PARA;
@property (retain) USBoolean * EDIT_PER;
@property (retain) USBoolean * EDIT_PER_INHERIT;
@property (retain) NSNumber * PROFILE_LAYER_PRIORITY;
@property (retain) NSString * SYS_PROFILE_CODE;
@property (retain) NSString * SYS_PROFILE_LAYER_CODE;
@property (retain) NSNumber * SYS_PROFILE_LAYER_ID;
@property (retain) NSString * SYS_PROFILE_LAYER_NAME_EN;
@property (retain) NSString * SYS_PROFILE_LAYER_NAME_VN;
@property (retain) NSString * SYS_SUB_PROFILE_CODE;
@property (retain) NSString * VIEW_ACTION;
@property (retain) NSString * VIEW_CONTROLLER;
@property (retain) NSString * VIEW_PARA;
@property (retain) USBoolean * VIEW_PER;
@property (retain) USBoolean * VIEW_PER_INHERIT;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSP_GET_PROF_LAYER_ALL_PERMIS_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_GET_PROF_LAYER_ALL_PERMIS_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSP_GET_PROF_LAYER_ALL_PERMIS_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_GET_PROF_LAYER_ALL_PERMIS_Result:(tns1_SP_GET_PROF_LAYER_ALL_PERMIS_Result *)toAdd;
@property (readonly) NSMutableArray * SP_GET_PROF_LAYER_ALL_PERMIS_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_SP_ESS_PRO_LAYER_VIEW_PER_TREE_Result : NSObject {
	
/* elements */
	NSString * ACTION;
	NSString * CODE;
	NSNumber * COMPANY_ID;
	NSString * CONTROLLER;
	NSNumber * ID_;
	NSNumber * M_ORDER;
	NSString * NAME_EN;
	NSString * NAME_VN;
	NSString * PARAMETERS;
	NSString * PARENT_CODE;
	NSNumber * PRIORITY;
	NSString * TYPE;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_SP_ESS_PRO_LAYER_VIEW_PER_TREE_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ACTION;
@property (retain) NSString * CODE;
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSString * CONTROLLER;
@property (retain) NSNumber * ID_;
@property (retain) NSNumber * M_ORDER;
@property (retain) NSString * NAME_EN;
@property (retain) NSString * NAME_VN;
@property (retain) NSString * PARAMETERS;
@property (retain) NSString * PARENT_CODE;
@property (retain) NSNumber * PRIORITY;
@property (retain) NSString * TYPE;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfSP_ESS_PRO_LAYER_VIEW_PER_TREE_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_ESS_PRO_LAYER_VIEW_PER_TREE_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfSP_ESS_PRO_LAYER_VIEW_PER_TREE_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_ESS_PRO_LAYER_VIEW_PER_TREE_Result:(tns1_SP_ESS_PRO_LAYER_VIEW_PER_TREE_Result *)toAdd;
@property (readonly) NSMutableArray * SP_ESS_PRO_LAYER_VIEW_PER_TREE_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
