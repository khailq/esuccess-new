#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class SecurityServiceSvc_GetAllUnits;
@class SecurityServiceSvc_GetAllUnitsResponse;
@class SecurityServiceSvc_CreateMembership;
@class SecurityServiceSvc_CreateMembershipResponse;
@class SecurityServiceSvc_CreateUserByObject;
@class SecurityServiceSvc_CreateUserByObjectResponse;
@class SecurityServiceSvc_EditUser;
@class SecurityServiceSvc_EditUserResponse;
@class SecurityServiceSvc_DeactiveUser;
@class SecurityServiceSvc_DeactiveUserResponse;
@class SecurityServiceSvc_CleanLoginSession;
@class SecurityServiceSvc_CleanLoginSessionResponse;
@class SecurityServiceSvc_SuccessLogout;
@class SecurityServiceSvc_SuccessLogoutResponse;
@class SecurityServiceSvc_UpdateUser;
@class SecurityServiceSvc_UpdateUserResponse;
@class SecurityServiceSvc_UpdateMembership;
@class SecurityServiceSvc_UpdateMembershipResponse;
@class SecurityServiceSvc_DeleteUser;
@class SecurityServiceSvc_DeleteUserResponse;
@class SecurityServiceSvc_GetUserByUserName;
@class SecurityServiceSvc_GetUserByUserNameResponse;
@class SecurityServiceSvc_GetUserbyUserId;
@class SecurityServiceSvc_GetUserbyUserIdResponse;
@class SecurityServiceSvc_GetUserMembershipByUserId;
@class SecurityServiceSvc_GetUserMembershipByUserIdResponse;
@class SecurityServiceSvc_GetUserMembershipByUserName;
@class SecurityServiceSvc_GetUserMembershipByUserNameResponse;
@class SecurityServiceSvc_GetAllUsers;
@class SecurityServiceSvc_GetAllUsersResponse;
@class SecurityServiceSvc_GetUsersByRoleId;
@class SecurityServiceSvc_GetUsersByRoleIdResponse;
@class SecurityServiceSvc_GetUsersByCompanyId;
@class SecurityServiceSvc_GetUsersByCompanyIdResponse;
@class SecurityServiceSvc_GetUsersByGroupId;
@class SecurityServiceSvc_GetUsersByGroupIdResponse;
@class SecurityServiceSvc_GetGroupsByRoleId;
@class SecurityServiceSvc_GetGroupsByRoleIdResponse;
@class SecurityServiceSvc_GetGroupsByUserId;
@class SecurityServiceSvc_GetGroupsByUserIdResponse;
@class SecurityServiceSvc_GetGroupsByCompanyId;
@class SecurityServiceSvc_GetGroupsByCompanyIdResponse;
@class SecurityServiceSvc_GetRolesByGroupId;
@class SecurityServiceSvc_GetRolesByGroupIdResponse;
@class SecurityServiceSvc_GetRolesByCompanyId;
@class SecurityServiceSvc_GetRolesByCompanyIdResponse;
@class SecurityServiceSvc_GetRolesByUserId;
@class SecurityServiceSvc_GetRolesByUserIdResponse;
@class SecurityServiceSvc_GetMembershipByUserId;
@class SecurityServiceSvc_GetMembershipByUserIdResponse;
@class SecurityServiceSvc_GetEmployeeById;
@class SecurityServiceSvc_GetEmployeeByIdResponse;
@class SecurityServiceSvc_GetUsersByCondition;
@class SecurityServiceSvc_GetUsersByConditionResponse;
@class SecurityServiceSvc_GetUsersSuggestByCondition;
@class SecurityServiceSvc_GetUsersSuggestByConditionResponse;
@class SecurityServiceSvc_CreateRole;
@class SecurityServiceSvc_CreateRoleResponse;
@class SecurityServiceSvc_UpdateRole;
@class SecurityServiceSvc_UpdateRoleResponse;
@class SecurityServiceSvc_DeleteRole;
@class SecurityServiceSvc_DeleteRoleResponse;
@class SecurityServiceSvc_GetRole;
@class SecurityServiceSvc_GetRoleResponse;
@class SecurityServiceSvc_GetRolesByName;
@class SecurityServiceSvc_GetRolesByNameResponse;
@class SecurityServiceSvc_GetGroupsByName;
@class SecurityServiceSvc_GetGroupsByNameResponse;
@class SecurityServiceSvc_GetAllRoles;
@class SecurityServiceSvc_GetAllRolesResponse;
@class SecurityServiceSvc_CreateGroup;
@class SecurityServiceSvc_CreateGroupResponse;
@class SecurityServiceSvc_UpdateGroup;
@class SecurityServiceSvc_UpdateGroupResponse;
@class SecurityServiceSvc_DeleteGroup;
@class SecurityServiceSvc_DeleteGroupResponse;
@class SecurityServiceSvc_GetGroup;
@class SecurityServiceSvc_GetGroupResponse;
@class SecurityServiceSvc_GetAllGroups;
@class SecurityServiceSvc_GetAllGroupsResponse;
@class SecurityServiceSvc_GetAllCompanies;
@class SecurityServiceSvc_GetAllCompaniesResponse;
@class SecurityServiceSvc_GetAllApplications;
@class SecurityServiceSvc_GetAllApplicationsResponse;
@class SecurityServiceSvc_GetApplicationById;
@class SecurityServiceSvc_GetApplicationByIdResponse;
@class SecurityServiceSvc_GetAllModules;
@class SecurityServiceSvc_GetAllModulesResponse;
@class SecurityServiceSvc_GetModulesByAppId;
@class SecurityServiceSvc_GetModulesByAppIdResponse;
@class SecurityServiceSvc_GetModuleById;
@class SecurityServiceSvc_GetModuleByIdResponse;
@class SecurityServiceSvc_GetAllFeatures;
@class SecurityServiceSvc_GetAllFeaturesResponse;
@class SecurityServiceSvc_GetFeaturesByModuleId;
@class SecurityServiceSvc_GetFeaturesByModuleIdResponse;
@class SecurityServiceSvc_GetFeatureById;
@class SecurityServiceSvc_GetFeatureByIdResponse;
@class SecurityServiceSvc_GetAllActionsByFeatureId;
@class SecurityServiceSvc_GetAllActionsByFeatureIdResponse;
@class SecurityServiceSvc_GetV_ActionByActionId;
@class SecurityServiceSvc_GetV_ActionByActionIdResponse;
@class SecurityServiceSvc_GetAllV_ActionsByFeatureId;
@class SecurityServiceSvc_GetAllV_ActionsByFeatureIdResponse;
@class SecurityServiceSvc_GetLinksByActionId;
@class SecurityServiceSvc_GetLinksByActionIdResponse;
@class SecurityServiceSvc_GetAllActionTypes;
@class SecurityServiceSvc_GetAllActionTypesResponse;
@class SecurityServiceSvc_GetActionTypeById;
@class SecurityServiceSvc_GetActionTypeByIdResponse;
@class SecurityServiceSvc_AssignActionToRole;
@class SecurityServiceSvc_AssignActionToRoleResponse;
@class SecurityServiceSvc_AssignActionToUser;
@class SecurityServiceSvc_AssignActionToUserResponse;
@class SecurityServiceSvc_AssignActionToGroup;
@class SecurityServiceSvc_AssignActionToGroupResponse;
@class SecurityServiceSvc_RemoveActionFromRole;
@class SecurityServiceSvc_RemoveActionFromRoleResponse;
@class SecurityServiceSvc_RemoveActionFromUser;
@class SecurityServiceSvc_RemoveActionFromUserResponse;
@class SecurityServiceSvc_RemoveActionFromGroupr;
@class SecurityServiceSvc_RemoveActionFromGrouprResponse;
@class SecurityServiceSvc_AssignRoleToUser;
@class SecurityServiceSvc_AssignRoleToUserResponse;
@class SecurityServiceSvc_RemoveRoleFromUser;
@class SecurityServiceSvc_RemoveRoleFromUserResponse;
@class SecurityServiceSvc_AssignRoleToGroup;
@class SecurityServiceSvc_AssignRoleToGroupResponse;
@class SecurityServiceSvc_RemoveRoleFromGroup;
@class SecurityServiceSvc_RemoveRoleFromGroupResponse;
@class SecurityServiceSvc_AssignGroupToUser;
@class SecurityServiceSvc_AssignGroupToUserResponse;
@class SecurityServiceSvc_RemoveGroupFromUser;
@class SecurityServiceSvc_RemoveGroupFromUserResponse;
@class SecurityServiceSvc_ChangePassword;
@class SecurityServiceSvc_ChangePasswordResponse;
@class SecurityServiceSvc_SuccessLogin;
@class SecurityServiceSvc_SuccessLoginResponse;
@class SecurityServiceSvc_CheckActiveUser;
@class SecurityServiceSvc_CheckActiveUserResponse;
@class SecurityServiceSvc_AddSysUserLog;
@class SecurityServiceSvc_AddSysUserLogResponse;
@class SecurityServiceSvc_GetAllUserPermissions;
@class SecurityServiceSvc_GetAllUserPermissionsResponse;
@class SecurityServiceSvc_GetAllRolePermissions;
@class SecurityServiceSvc_GetAllRolePermissionsResponse;
@class SecurityServiceSvc_GetAllGroupPermissions;
@class SecurityServiceSvc_GetAllGroupPermissionsResponse;
@class SecurityServiceSvc_HavePermission;
@class SecurityServiceSvc_HavePermissionResponse;
@class SecurityServiceSvc_CreateModule;
@class SecurityServiceSvc_CreateModuleResponse;
@class SecurityServiceSvc_UpdateModule;
@class SecurityServiceSvc_UpdateModuleResponse;
@class SecurityServiceSvc_DeleteModule;
@class SecurityServiceSvc_DeleteModuleResponse;
@class SecurityServiceSvc_CreateFeature;
@class SecurityServiceSvc_CreateFeatureResponse;
@class SecurityServiceSvc_UpdateFeature;
@class SecurityServiceSvc_UpdateFeatureResponse;
@class SecurityServiceSvc_DeleteFeature;
@class SecurityServiceSvc_DeleteFeatureResponse;
@class SecurityServiceSvc_CreateAction;
@class SecurityServiceSvc_CreateActionResponse;
@class SecurityServiceSvc_GetActionByFeatureIdAndActionTypId;
@class SecurityServiceSvc_GetActionByFeatureIdAndActionTypIdResponse;
@class SecurityServiceSvc_DeleteAction;
@class SecurityServiceSvc_DeleteActionResponse;
@class SecurityServiceSvc_CreateLink;
@class SecurityServiceSvc_CreateLinkResponse;
@class SecurityServiceSvc_UpdateLink;
@class SecurityServiceSvc_UpdateLinkResponse;
@class SecurityServiceSvc_DeleteLink;
@class SecurityServiceSvc_DeleteLinkResponse;
@class SecurityServiceSvc_CheckUserIsInRole;
@class SecurityServiceSvc_CheckUserIsInRoleResponse;
@class SecurityServiceSvc_AssignUsersToRole;
@class SecurityServiceSvc_AssignUsersToRoleResponse;
@class SecurityServiceSvc_CheckGroupIsInRole;
@class SecurityServiceSvc_CheckGroupIsInRoleResponse;
@class SecurityServiceSvc_AssignGroupsToRole;
@class SecurityServiceSvc_AssignGroupsToRoleResponse;
@class SecurityServiceSvc_CheckUserIsInGroup;
@class SecurityServiceSvc_CheckUserIsInGroupResponse;
@class SecurityServiceSvc_AssignUsersToGroup;
@class SecurityServiceSvc_AssignUsersToGroupResponse;
@class SecurityServiceSvc_GetSelectedUsersById;
@class SecurityServiceSvc_GetSelectedUsersByIdResponse;
@class SecurityServiceSvc_GetUserEmployeeByUserId;
@class SecurityServiceSvc_GetUserEmployeeByUserIdResponse;
@class SecurityServiceSvc_GetUserMembershipByUserDomain;
@class SecurityServiceSvc_GetUserMembershipByUserDomainResponse;
@class SecurityServiceSvc_AssignOrgUnitPermissionToUser;
@class SecurityServiceSvc_AssignOrgUnitPermissionToUserResponse;
@class SecurityServiceSvc_RemoveOrgUnitPermissionFromUser;
@class SecurityServiceSvc_RemoveOrgUnitPermissionFromUserResponse;
@class SecurityServiceSvc_AssignOrgUnitPermissionToRole;
@class SecurityServiceSvc_AssignOrgUnitPermissionToRoleResponse;
@class SecurityServiceSvc_RemoveOrgUnitPermissionFromRole;
@class SecurityServiceSvc_RemoveOrgUnitPermissionFromRoleResponse;
@class SecurityServiceSvc_AssignOrgUnitPermissionToGroup;
@class SecurityServiceSvc_AssignOrgUnitPermissionToGroupResponse;
@class SecurityServiceSvc_RemoveOrgUnitPermissionFromGroup;
@class SecurityServiceSvc_RemoveOrgUnitPermissionFromGroupResponse;
@class SecurityServiceSvc_GetAllUserOrgUnitPermissions;
@class SecurityServiceSvc_GetAllUserOrgUnitPermissionsResponse;
@class SecurityServiceSvc_GetAllRoleOrgUnitPermissions;
@class SecurityServiceSvc_GetAllRoleOrgUnitPermissionsResponse;
@class SecurityServiceSvc_GetAllGroupOrgUnitPermissions;
@class SecurityServiceSvc_GetAllGroupOrgUnitPermissionsResponse;
@class SecurityServiceSvc_GetAllOrgUnitByUser;
@class SecurityServiceSvc_GetAllOrgUnitByUserResponse;
@class SecurityServiceSvc_CheckPermisionOnEmployee;
@class SecurityServiceSvc_CheckPermisionOnEmployeeResponse;
@class SecurityServiceSvc_GetAllSysEmpProfileGroupLayer;
@class SecurityServiceSvc_GetAllSysEmpProfileGroupLayerResponse;
@class SecurityServiceSvc_GetSysEmpProfileLayersByGroupId;
@class SecurityServiceSvc_GetSysEmpProfileLayersByGroupIdResponse;
@class SecurityServiceSvc_GetSysEmpProfileLayerUrlsByLayerId;
@class SecurityServiceSvc_GetSysEmpProfileLayerUrlsByLayerIdResponse;
@class SecurityServiceSvc_GetSysEmpProfileLayerUrlsByTypeByLayerId;
@class SecurityServiceSvc_GetSysEmpProfileLayerUrlsByTypeByLayerIdResponse;
@class SecurityServiceSvc_GetAllRoleEmpProfilePermission;
@class SecurityServiceSvc_GetAllRoleEmpProfilePermissionResponse;
@class SecurityServiceSvc_GetAllUserEmpProfilePermission;
@class SecurityServiceSvc_GetAllUserEmpProfilePermissionResponse;
@class SecurityServiceSvc_GetAllGroupEmpProfilePermission;
@class SecurityServiceSvc_GetAllGroupEmpProfilePermissionResponse;
@class SecurityServiceSvc_AssignEmpProfileLayerPermissonToUser;
@class SecurityServiceSvc_AssignEmpProfileLayerPermissonToUserResponse;
@class SecurityServiceSvc_AssignEmpProfileLayerPermissonToRole;
@class SecurityServiceSvc_AssignEmpProfileLayerPermissonToRoleResponse;
@class SecurityServiceSvc_AssignEmpProfileLayerPermissonToGroup;
@class SecurityServiceSvc_AssignEmpProfileLayerPermissonToGroupResponse;
@class SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromUser;
@class SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromUserResponse;
@class SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromGroup;
@class SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromGroupResponse;
@class SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromRole;
@class SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromRoleResponse;
@class SecurityServiceSvc_GetAllLayerOfAuthorizedSubAndProfile;
@class SecurityServiceSvc_GetAllLayerOfAuthorizedSubAndProfileResponse;
@class SecurityServiceSvc_GetLayerAndPermissOfSubAndProfile;
@class SecurityServiceSvc_GetLayerAndPermissOfSubAndProfileResponse;
@class SecurityServiceSvc_GetAllLayerOfAuthorizedIncludedSubAndProfileForTree;
@class SecurityServiceSvc_GetAllLayerOfAuthorizedIncludedSubAndProfileForTreeResponse;
@class SecurityServiceSvc_GetOnePermissOfUserOnProfileLayer;
@class SecurityServiceSvc_GetOnePermissOfUserOnProfileLayerResponse;
@class SecurityServiceSvc_AssignProfileLayerPermissonToUserGroupRole;
@class SecurityServiceSvc_AssignProfileLayerPermissonToUserGroupRoleResponse;
@class SecurityServiceSvc_RemoveProfileLayerPermissonFromUserGroupRole;
@class SecurityServiceSvc_RemoveProfileLayerPermissonFromUserGroupRoleResponse;
#import "tns1.h"
#import "tns2.h"
#import "tns3.h"
@interface SecurityServiceSvc_GetAllUnits : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllUnits *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllUnitsResponse : NSObject {
	
/* elements */
	tns1_ArrayOfV_OrgUnit * GetAllUnitsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllUnitsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfV_OrgUnit * GetAllUnitsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CreateMembership : NSObject {
	
/* elements */
	tns1_SysMembership * mb;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CreateMembership *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysMembership * mb;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CreateMembershipResponse : NSObject {
	
/* elements */
	tns1_SysMembership * CreateMembershipResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CreateMembershipResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysMembership * CreateMembershipResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CreateUserByObject : NSObject {
	
/* elements */
	tns1_SysUser * user;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CreateUserByObject *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysUser * user;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CreateUserByObjectResponse : NSObject {
	
/* elements */
	tns1_SysUser * CreateUserByObjectResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CreateUserByObjectResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysUser * CreateUserByObjectResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_EditUser : NSObject {
	
/* elements */
	NSNumber * userId;
	NSString * userName;
	NSNumber * employeeId;
	NSString * password;
	USBoolean * isActive;
	NSNumber * companyId;
	NSNumber * groupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_EditUser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * userId;
@property (retain) NSString * userName;
@property (retain) NSNumber * employeeId;
@property (retain) NSString * password;
@property (retain) USBoolean * isActive;
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * groupId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_EditUserResponse : NSObject {
	
/* elements */
	tns2_MsgNotifyModel * EditUserResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_EditUserResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns2_MsgNotifyModel * EditUserResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_DeactiveUser : NSObject {
	
/* elements */
	NSNumber * UserId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_DeactiveUser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * UserId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_DeactiveUserResponse : NSObject {
	
/* elements */
	tns2_MsgNotifyModel * DeactiveUserResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_DeactiveUserResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns2_MsgNotifyModel * DeactiveUserResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CleanLoginSession : NSObject {
	
/* elements */
	NSNumber * UserId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CleanLoginSession *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * UserId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CleanLoginSessionResponse : NSObject {
	
/* elements */
	tns2_MsgNotifyModel * CleanLoginSessionResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CleanLoginSessionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns2_MsgNotifyModel * CleanLoginSessionResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_SuccessLogout : NSObject {
	
/* elements */
	NSNumber * UserId;
	NSString * UserName;
	NSString * HostName;
	NSString * IPAddress;
	NSString * SessionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_SuccessLogout *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * UserId;
@property (retain) NSString * UserName;
@property (retain) NSString * HostName;
@property (retain) NSString * IPAddress;
@property (retain) NSString * SessionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_SuccessLogoutResponse : NSObject {
	
/* elements */
	USBoolean * SuccessLogoutResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_SuccessLogoutResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * SuccessLogoutResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_UpdateUser : NSObject {
	
/* elements */
	tns1_SysUser * user;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_UpdateUser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysUser * user;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_UpdateUserResponse : NSObject {
	
/* elements */
	USBoolean * UpdateUserResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_UpdateUserResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateUserResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_UpdateMembership : NSObject {
	
/* elements */
	tns1_SysMembership * mem;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_UpdateMembership *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysMembership * mem;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_UpdateMembershipResponse : NSObject {
	
/* elements */
	USBoolean * UpdateMembershipResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_UpdateMembershipResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateMembershipResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_DeleteUser : NSObject {
	
/* elements */
	NSNumber * userId;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_DeleteUser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * userId;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_DeleteUserResponse : NSObject {
	
/* elements */
	USBoolean * DeleteUserResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_DeleteUserResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteUserResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetUserByUserName : NSObject {
	
/* elements */
	NSString * userName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetUserByUserName *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * userName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetUserByUserNameResponse : NSObject {
	
/* elements */
	tns1_SysUser * GetUserByUserNameResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetUserByUserNameResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysUser * GetUserByUserNameResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetUserbyUserId : NSObject {
	
/* elements */
	NSNumber * userId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetUserbyUserId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * userId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetUserbyUserIdResponse : NSObject {
	
/* elements */
	tns1_SysUser * GetUserbyUserIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetUserbyUserIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysUser * GetUserbyUserIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetUserMembershipByUserId : NSObject {
	
/* elements */
	NSNumber * userId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetUserMembershipByUserId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * userId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetUserMembershipByUserIdResponse : NSObject {
	
/* elements */
	tns1_V_SysUserMembership * GetUserMembershipByUserIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetUserMembershipByUserIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_V_SysUserMembership * GetUserMembershipByUserIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetUserMembershipByUserName : NSObject {
	
/* elements */
	NSString * userName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetUserMembershipByUserName *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * userName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetUserMembershipByUserNameResponse : NSObject {
	
/* elements */
	tns1_V_SysUserMembership * GetUserMembershipByUserNameResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetUserMembershipByUserNameResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_V_SysUserMembership * GetUserMembershipByUserNameResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllUsers : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllUsers *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllUsersResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysUser * GetAllUsersResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllUsersResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysUser * GetAllUsersResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetUsersByRoleId : NSObject {
	
/* elements */
	NSNumber * roleId;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetUsersByRoleId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * roleId;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetUsersByRoleIdResponse : NSObject {
	
/* elements */
	tns1_ArrayOfV_SysUserInRole * GetUsersByRoleIdResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetUsersByRoleIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfV_SysUserInRole * GetUsersByRoleIdResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetUsersByCompanyId : NSObject {
	
/* elements */
	NSNumber * companyId;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetUsersByCompanyId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetUsersByCompanyIdResponse : NSObject {
	
/* elements */
	tns1_ArrayOfV_SysUserMembership * GetUsersByCompanyIdResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetUsersByCompanyIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfV_SysUserMembership * GetUsersByCompanyIdResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetUsersByGroupId : NSObject {
	
/* elements */
	NSNumber * groupId;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetUsersByGroupId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * groupId;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetUsersByGroupIdResponse : NSObject {
	
/* elements */
	tns1_ArrayOfV_SysUserInGroup * GetUsersByGroupIdResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetUsersByGroupIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfV_SysUserInGroup * GetUsersByGroupIdResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetGroupsByRoleId : NSObject {
	
/* elements */
	NSNumber * roleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetGroupsByRoleId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * roleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetGroupsByRoleIdResponse : NSObject {
	
/* elements */
	tns1_ArrayOfV_SysGroupInRole * GetGroupsByRoleIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetGroupsByRoleIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfV_SysGroupInRole * GetGroupsByRoleIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetGroupsByUserId : NSObject {
	
/* elements */
	NSNumber * userId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetGroupsByUserId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * userId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetGroupsByUserIdResponse : NSObject {
	
/* elements */
	tns1_ArrayOfV_SysUserInGroup * GetGroupsByUserIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetGroupsByUserIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfV_SysUserInGroup * GetGroupsByUserIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetGroupsByCompanyId : NSObject {
	
/* elements */
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetGroupsByCompanyId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetGroupsByCompanyIdResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysGroup * GetGroupsByCompanyIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetGroupsByCompanyIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysGroup * GetGroupsByCompanyIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetRolesByGroupId : NSObject {
	
/* elements */
	NSNumber * groupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetRolesByGroupId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * groupId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetRolesByGroupIdResponse : NSObject {
	
/* elements */
	tns1_ArrayOfV_SysRoleInGroup * GetRolesByGroupIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetRolesByGroupIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfV_SysRoleInGroup * GetRolesByGroupIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetRolesByCompanyId : NSObject {
	
/* elements */
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetRolesByCompanyId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetRolesByCompanyIdResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysRole * GetRolesByCompanyIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetRolesByCompanyIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysRole * GetRolesByCompanyIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetRolesByUserId : NSObject {
	
/* elements */
	NSNumber * userId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetRolesByUserId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * userId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetRolesByUserIdResponse : NSObject {
	
/* elements */
	tns1_ArrayOfV_SysUserInRole * GetRolesByUserIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetRolesByUserIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfV_SysUserInRole * GetRolesByUserIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetMembershipByUserId : NSObject {
	
/* elements */
	NSNumber * userId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetMembershipByUserId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * userId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetMembershipByUserIdResponse : NSObject {
	
/* elements */
	tns1_SysMembership * GetMembershipByUserIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetMembershipByUserIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysMembership * GetMembershipByUserIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetEmployeeById : NSObject {
	
/* elements */
	NSNumber * employeeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetEmployeeById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * employeeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetEmployeeByIdResponse : NSObject {
	
/* elements */
	tns1_Employee * GetEmployeeByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetEmployeeByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_Employee * GetEmployeeByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetUsersByCondition : NSObject {
	
/* elements */
	NSString * condition;
	NSNumber * companyId;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetUsersByCondition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * condition;
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetUsersByConditionResponse : NSObject {
	
/* elements */
	tns1_ArrayOfV_SysUserEmployee * GetUsersByConditionResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetUsersByConditionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfV_SysUserEmployee * GetUsersByConditionResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetUsersSuggestByCondition : NSObject {
	
/* elements */
	NSString * condition;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetUsersSuggestByCondition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * condition;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetUsersSuggestByConditionResponse : NSObject {
	
/* elements */
	tns1_ArrayOfV_SysUserEmployee * GetUsersSuggestByConditionResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetUsersSuggestByConditionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfV_SysUserEmployee * GetUsersSuggestByConditionResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CreateRole : NSObject {
	
/* elements */
	tns1_SysRole * role;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CreateRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysRole * role;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CreateRoleResponse : NSObject {
	
/* elements */
	tns1_SysRole * CreateRoleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CreateRoleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysRole * CreateRoleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_UpdateRole : NSObject {
	
/* elements */
	tns1_SysRole * role;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_UpdateRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysRole * role;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_UpdateRoleResponse : NSObject {
	
/* elements */
	USBoolean * UpdateRoleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_UpdateRoleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateRoleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_DeleteRole : NSObject {
	
/* elements */
	NSNumber * roleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_DeleteRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * roleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_DeleteRoleResponse : NSObject {
	
/* elements */
	USBoolean * DeleteRoleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_DeleteRoleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteRoleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetRole : NSObject {
	
/* elements */
	NSNumber * id_;
	NSString * name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
@property (retain) NSString * name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetRoleResponse : NSObject {
	
/* elements */
	tns1_SysRole * GetRoleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetRoleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysRole * GetRoleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetRolesByName : NSObject {
	
/* elements */
	NSString * name;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetRolesByName *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * name;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetRolesByNameResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysRole * GetRolesByNameResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetRolesByNameResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysRole * GetRolesByNameResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetGroupsByName : NSObject {
	
/* elements */
	NSString * name;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetGroupsByName *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * name;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetGroupsByNameResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysGroup * GetGroupsByNameResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetGroupsByNameResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysGroup * GetGroupsByNameResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllRoles : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllRoles *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllRolesResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysRole * GetAllRolesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllRolesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysRole * GetAllRolesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CreateGroup : NSObject {
	
/* elements */
	tns1_SysGroup * group;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CreateGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysGroup * group;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CreateGroupResponse : NSObject {
	
/* elements */
	tns1_SysGroup * CreateGroupResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CreateGroupResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysGroup * CreateGroupResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_UpdateGroup : NSObject {
	
/* elements */
	tns1_SysGroup * group;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_UpdateGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysGroup * group;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_UpdateGroupResponse : NSObject {
	
/* elements */
	USBoolean * UpdateGroupResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_UpdateGroupResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateGroupResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_DeleteGroup : NSObject {
	
/* elements */
	NSNumber * groupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_DeleteGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * groupId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_DeleteGroupResponse : NSObject {
	
/* elements */
	USBoolean * DeleteGroupResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_DeleteGroupResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteGroupResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetGroup : NSObject {
	
/* elements */
	NSNumber * id_;
	NSString * name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
@property (retain) NSString * name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetGroupResponse : NSObject {
	
/* elements */
	tns1_SysGroup * GetGroupResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetGroupResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysGroup * GetGroupResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllGroups : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllGroups *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllGroupsResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysGroup * GetAllGroupsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllGroupsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysGroup * GetAllGroupsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllCompanies : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllCompanies *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllCompaniesResponse : NSObject {
	
/* elements */
	tns1_ArrayOfCompany * GetAllCompaniesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllCompaniesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfCompany * GetAllCompaniesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllApplications : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllApplications *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllApplicationsResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysApplication * GetAllApplicationsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllApplicationsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysApplication * GetAllApplicationsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetApplicationById : NSObject {
	
/* elements */
	NSNumber * id_;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetApplicationById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * id_;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetApplicationByIdResponse : NSObject {
	
/* elements */
	tns1_SysApplication * GetApplicationByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetApplicationByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysApplication * GetApplicationByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllModules : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllModules *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllModulesResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysModule * GetAllModulesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllModulesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysModule * GetAllModulesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetModulesByAppId : NSObject {
	
/* elements */
	NSNumber * appId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetModulesByAppId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * appId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetModulesByAppIdResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysModule * GetModulesByAppIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetModulesByAppIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysModule * GetModulesByAppIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetModuleById : NSObject {
	
/* elements */
	NSNumber * moduleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetModuleById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * moduleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetModuleByIdResponse : NSObject {
	
/* elements */
	tns1_SysModule * GetModuleByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetModuleByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysModule * GetModuleByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllFeatures : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllFeatures *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllFeaturesResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysFeature * GetAllFeaturesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllFeaturesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysFeature * GetAllFeaturesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetFeaturesByModuleId : NSObject {
	
/* elements */
	NSNumber * moduleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetFeaturesByModuleId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * moduleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetFeaturesByModuleIdResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysFeature * GetFeaturesByModuleIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetFeaturesByModuleIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysFeature * GetFeaturesByModuleIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetFeatureById : NSObject {
	
/* elements */
	NSNumber * featureId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetFeatureById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * featureId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetFeatureByIdResponse : NSObject {
	
/* elements */
	tns1_SysFeature * GetFeatureByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetFeatureByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysFeature * GetFeatureByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllActionsByFeatureId : NSObject {
	
/* elements */
	NSNumber * featureId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllActionsByFeatureId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * featureId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllActionsByFeatureIdResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysAction * GetAllActionsByFeatureIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllActionsByFeatureIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysAction * GetAllActionsByFeatureIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetV_ActionByActionId : NSObject {
	
/* elements */
	NSNumber * actionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetV_ActionByActionId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * actionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetV_ActionByActionIdResponse : NSObject {
	
/* elements */
	tns1_V_SysAction * GetV_ActionByActionIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetV_ActionByActionIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_V_SysAction * GetV_ActionByActionIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllV_ActionsByFeatureId : NSObject {
	
/* elements */
	NSNumber * featureId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllV_ActionsByFeatureId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * featureId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllV_ActionsByFeatureIdResponse : NSObject {
	
/* elements */
	tns1_ArrayOfV_SysAction * GetAllV_ActionsByFeatureIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllV_ActionsByFeatureIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfV_SysAction * GetAllV_ActionsByFeatureIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetLinksByActionId : NSObject {
	
/* elements */
	NSNumber * actionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetLinksByActionId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * actionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetLinksByActionIdResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysLink * GetLinksByActionIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetLinksByActionIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysLink * GetLinksByActionIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllActionTypes : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllActionTypes *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllActionTypesResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysActionType * GetAllActionTypesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllActionTypesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysActionType * GetAllActionTypesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetActionTypeById : NSObject {
	
/* elements */
	NSNumber * actionTypeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetActionTypeById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * actionTypeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetActionTypeByIdResponse : NSObject {
	
/* elements */
	tns1_SysActionType * GetActionTypeByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetActionTypeByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysActionType * GetActionTypeByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignActionToRole : NSObject {
	
/* elements */
	NSNumber * actionId;
	NSNumber * roleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignActionToRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * actionId;
@property (retain) NSNumber * roleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignActionToRoleResponse : NSObject {
	
/* elements */
	USBoolean * AssignActionToRoleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignActionToRoleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AssignActionToRoleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignActionToUser : NSObject {
	
/* elements */
	NSNumber * actionId;
	NSNumber * userId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignActionToUser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * actionId;
@property (retain) NSNumber * userId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignActionToUserResponse : NSObject {
	
/* elements */
	USBoolean * AssignActionToUserResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignActionToUserResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AssignActionToUserResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignActionToGroup : NSObject {
	
/* elements */
	NSNumber * actionId;
	NSNumber * groupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignActionToGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * actionId;
@property (retain) NSNumber * groupId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignActionToGroupResponse : NSObject {
	
/* elements */
	USBoolean * AssignActionToGroupResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignActionToGroupResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AssignActionToGroupResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveActionFromRole : NSObject {
	
/* elements */
	NSNumber * actionId;
	NSNumber * roleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveActionFromRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * actionId;
@property (retain) NSNumber * roleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveActionFromRoleResponse : NSObject {
	
/* elements */
	USBoolean * RemoveActionFromRoleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveActionFromRoleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * RemoveActionFromRoleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveActionFromUser : NSObject {
	
/* elements */
	NSNumber * actionId;
	NSNumber * userId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveActionFromUser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * actionId;
@property (retain) NSNumber * userId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveActionFromUserResponse : NSObject {
	
/* elements */
	USBoolean * RemoveActionFromUserResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveActionFromUserResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * RemoveActionFromUserResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveActionFromGroupr : NSObject {
	
/* elements */
	NSNumber * actionId;
	NSNumber * groupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveActionFromGroupr *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * actionId;
@property (retain) NSNumber * groupId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveActionFromGrouprResponse : NSObject {
	
/* elements */
	USBoolean * RemoveActionFromGrouprResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveActionFromGrouprResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * RemoveActionFromGrouprResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignRoleToUser : NSObject {
	
/* elements */
	NSNumber * roleId;
	NSNumber * userId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignRoleToUser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * roleId;
@property (retain) NSNumber * userId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignRoleToUserResponse : NSObject {
	
/* elements */
	USBoolean * AssignRoleToUserResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignRoleToUserResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AssignRoleToUserResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveRoleFromUser : NSObject {
	
/* elements */
	NSNumber * roleId;
	NSNumber * userId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveRoleFromUser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * roleId;
@property (retain) NSNumber * userId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveRoleFromUserResponse : NSObject {
	
/* elements */
	USBoolean * RemoveRoleFromUserResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveRoleFromUserResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * RemoveRoleFromUserResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignRoleToGroup : NSObject {
	
/* elements */
	NSNumber * roleId;
	NSNumber * groupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignRoleToGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * roleId;
@property (retain) NSNumber * groupId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignRoleToGroupResponse : NSObject {
	
/* elements */
	USBoolean * AssignRoleToGroupResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignRoleToGroupResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AssignRoleToGroupResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveRoleFromGroup : NSObject {
	
/* elements */
	NSNumber * roleId;
	NSNumber * groupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveRoleFromGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * roleId;
@property (retain) NSNumber * groupId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveRoleFromGroupResponse : NSObject {
	
/* elements */
	USBoolean * RemoveRoleFromGroupResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveRoleFromGroupResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * RemoveRoleFromGroupResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignGroupToUser : NSObject {
	
/* elements */
	NSNumber * groupId;
	NSNumber * userId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignGroupToUser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * groupId;
@property (retain) NSNumber * userId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignGroupToUserResponse : NSObject {
	
/* elements */
	USBoolean * AssignGroupToUserResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignGroupToUserResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AssignGroupToUserResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveGroupFromUser : NSObject {
	
/* elements */
	NSNumber * groupId;
	NSNumber * userId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveGroupFromUser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * groupId;
@property (retain) NSNumber * userId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveGroupFromUserResponse : NSObject {
	
/* elements */
	USBoolean * RemoveGroupFromUserResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveGroupFromUserResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * RemoveGroupFromUserResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_ChangePassword : NSObject {
	
/* elements */
	NSString * userName;
	NSString * oldPassword;
	NSString * newPassword;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_ChangePassword *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * userName;
@property (retain) NSString * oldPassword;
@property (retain) NSString * newPassword;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_ChangePasswordResponse : NSObject {
	
/* elements */
	tns2_MsgNotifyModel * ChangePasswordResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_ChangePasswordResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns2_MsgNotifyModel * ChangePasswordResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_SuccessLogin : NSObject {
	
/* elements */
	NSString * userName;
	NSString * password;
	NSString * HostName;
	NSString * IPAddress;
	NSString * SessionId;
	NSString * error;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_SuccessLogin *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * userName;
@property (retain) NSString * password;
@property (retain) NSString * HostName;
@property (retain) NSString * IPAddress;
@property (retain) NSString * SessionId;
@property (retain) NSString * error;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_SuccessLoginResponse : NSObject {
	
/* elements */
	USBoolean * SuccessLoginResult;
	NSString * error;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_SuccessLoginResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * SuccessLoginResult;
@property (retain) NSString * error;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CheckActiveUser : NSObject {
	
/* elements */
	NSNumber * UserId;
	NSString * SessionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CheckActiveUser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * UserId;
@property (retain) NSString * SessionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CheckActiveUserResponse : NSObject {
	
/* elements */
	USBoolean * CheckActiveUserResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CheckActiveUserResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * CheckActiveUserResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AddSysUserLog : NSObject {
	
/* elements */
	tns1_SysUserLog * log;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AddSysUserLog *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysUserLog * log;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AddSysUserLogResponse : NSObject {
	
/* elements */
	USBoolean * AddSysUserLogResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AddSysUserLogResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddSysUserLogResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllUserPermissions : NSObject {
	
/* elements */
	NSNumber * userId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllUserPermissions *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * userId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllUserPermissionsResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysUserPermission * GetAllUserPermissionsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllUserPermissionsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysUserPermission * GetAllUserPermissionsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllRolePermissions : NSObject {
	
/* elements */
	NSNumber * roleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllRolePermissions *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * roleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllRolePermissionsResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysRolePermission * GetAllRolePermissionsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllRolePermissionsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysRolePermission * GetAllRolePermissionsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllGroupPermissions : NSObject {
	
/* elements */
	NSNumber * groupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllGroupPermissions *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * groupId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllGroupPermissionsResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysGroupPermission * GetAllGroupPermissionsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllGroupPermissionsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysGroupPermission * GetAllGroupPermissionsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_HavePermission : NSObject {
	
/* elements */
	NSNumber * userId;
	NSString * controller;
	NSString * action;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_HavePermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * userId;
@property (retain) NSString * controller;
@property (retain) NSString * action;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_HavePermissionResponse : NSObject {
	
/* elements */
	USBoolean * HavePermissionResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_HavePermissionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * HavePermissionResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CreateModule : NSObject {
	
/* elements */
	tns1_SysModule * module;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CreateModule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysModule * module;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CreateModuleResponse : NSObject {
	
/* elements */
	tns1_SysModule * CreateModuleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CreateModuleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysModule * CreateModuleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_UpdateModule : NSObject {
	
/* elements */
	tns1_SysModule * module;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_UpdateModule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysModule * module;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_UpdateModuleResponse : NSObject {
	
/* elements */
	USBoolean * UpdateModuleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_UpdateModuleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateModuleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_DeleteModule : NSObject {
	
/* elements */
	NSNumber * moduleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_DeleteModule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * moduleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_DeleteModuleResponse : NSObject {
	
/* elements */
	USBoolean * DeleteModuleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_DeleteModuleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteModuleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CreateFeature : NSObject {
	
/* elements */
	tns1_SysFeature * feature;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CreateFeature *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysFeature * feature;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CreateFeatureResponse : NSObject {
	
/* elements */
	tns1_SysFeature * CreateFeatureResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CreateFeatureResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysFeature * CreateFeatureResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_UpdateFeature : NSObject {
	
/* elements */
	tns1_SysFeature * feature;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_UpdateFeature *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysFeature * feature;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_UpdateFeatureResponse : NSObject {
	
/* elements */
	USBoolean * UpdateFeatureResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_UpdateFeatureResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateFeatureResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_DeleteFeature : NSObject {
	
/* elements */
	NSNumber * featureId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_DeleteFeature *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * featureId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_DeleteFeatureResponse : NSObject {
	
/* elements */
	USBoolean * DeleteFeatureResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_DeleteFeatureResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteFeatureResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CreateAction : NSObject {
	
/* elements */
	tns1_SysAction * action;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CreateAction *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysAction * action;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CreateActionResponse : NSObject {
	
/* elements */
	tns1_SysAction * CreateActionResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CreateActionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysAction * CreateActionResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetActionByFeatureIdAndActionTypId : NSObject {
	
/* elements */
	NSNumber * featureId;
	NSNumber * actionTypeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetActionByFeatureIdAndActionTypId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * featureId;
@property (retain) NSNumber * actionTypeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetActionByFeatureIdAndActionTypIdResponse : NSObject {
	
/* elements */
	tns1_SysAction * GetActionByFeatureIdAndActionTypIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetActionByFeatureIdAndActionTypIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysAction * GetActionByFeatureIdAndActionTypIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_DeleteAction : NSObject {
	
/* elements */
	NSNumber * actionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_DeleteAction *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * actionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_DeleteActionResponse : NSObject {
	
/* elements */
	USBoolean * DeleteActionResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_DeleteActionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteActionResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CreateLink : NSObject {
	
/* elements */
	tns1_SysLink * link;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CreateLink *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysLink * link;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CreateLinkResponse : NSObject {
	
/* elements */
	tns1_SysLink * CreateLinkResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CreateLinkResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysLink * CreateLinkResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_UpdateLink : NSObject {
	
/* elements */
	tns1_SysLink * link;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_UpdateLink *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_SysLink * link;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_UpdateLinkResponse : NSObject {
	
/* elements */
	USBoolean * UpdateLinkResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_UpdateLinkResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateLinkResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_DeleteLink : NSObject {
	
/* elements */
	NSNumber * linkId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_DeleteLink *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * linkId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_DeleteLinkResponse : NSObject {
	
/* elements */
	USBoolean * DeleteLinkResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_DeleteLinkResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteLinkResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CheckUserIsInRole : NSObject {
	
/* elements */
	NSNumber * userId;
	NSNumber * roleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CheckUserIsInRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * userId;
@property (retain) NSNumber * roleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CheckUserIsInRoleResponse : NSObject {
	
/* elements */
	USBoolean * CheckUserIsInRoleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CheckUserIsInRoleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * CheckUserIsInRoleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignUsersToRole : NSObject {
	
/* elements */
	tns1_ArrayOfSysUserInRole * users;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignUsersToRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysUserInRole * users;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignUsersToRoleResponse : NSObject {
	
/* elements */
	USBoolean * AssignUsersToRoleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignUsersToRoleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AssignUsersToRoleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CheckGroupIsInRole : NSObject {
	
/* elements */
	NSNumber * groupId;
	NSNumber * roleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CheckGroupIsInRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * groupId;
@property (retain) NSNumber * roleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CheckGroupIsInRoleResponse : NSObject {
	
/* elements */
	USBoolean * CheckGroupIsInRoleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CheckGroupIsInRoleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * CheckGroupIsInRoleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignGroupsToRole : NSObject {
	
/* elements */
	tns1_ArrayOfSysGroupInRole * groups;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignGroupsToRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysGroupInRole * groups;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignGroupsToRoleResponse : NSObject {
	
/* elements */
	USBoolean * AssignGroupsToRoleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignGroupsToRoleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AssignGroupsToRoleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CheckUserIsInGroup : NSObject {
	
/* elements */
	NSNumber * userId;
	NSNumber * groupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CheckUserIsInGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * userId;
@property (retain) NSNumber * groupId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CheckUserIsInGroupResponse : NSObject {
	
/* elements */
	USBoolean * CheckUserIsInGroupResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CheckUserIsInGroupResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * CheckUserIsInGroupResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignUsersToGroup : NSObject {
	
/* elements */
	tns1_ArrayOfSysUserInGroup * users;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignUsersToGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysUserInGroup * users;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignUsersToGroupResponse : NSObject {
	
/* elements */
	USBoolean * AssignUsersToGroupResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignUsersToGroupResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AssignUsersToGroupResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetSelectedUsersById : NSObject {
	
/* elements */
	tns3_ArrayOfint * usedIds;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetSelectedUsersById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns3_ArrayOfint * usedIds;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetSelectedUsersByIdResponse : NSObject {
	
/* elements */
	tns1_ArrayOfV_SysUserEmployee * GetSelectedUsersByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetSelectedUsersByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfV_SysUserEmployee * GetSelectedUsersByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetUserEmployeeByUserId : NSObject {
	
/* elements */
	NSNumber * userId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetUserEmployeeByUserId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * userId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetUserEmployeeByUserIdResponse : NSObject {
	
/* elements */
	tns1_V_SysUserEmployee * GetUserEmployeeByUserIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetUserEmployeeByUserIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_V_SysUserEmployee * GetUserEmployeeByUserIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetUserMembershipByUserDomain : NSObject {
	
/* elements */
	NSString * userDomain;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetUserMembershipByUserDomain *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * userDomain;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetUserMembershipByUserDomainResponse : NSObject {
	
/* elements */
	tns1_V_SysUserMembership * GetUserMembershipByUserDomainResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetUserMembershipByUserDomainResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_V_SysUserMembership * GetUserMembershipByUserDomainResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignOrgUnitPermissionToUser : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSNumber * userId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignOrgUnitPermissionToUser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSNumber * userId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignOrgUnitPermissionToUserResponse : NSObject {
	
/* elements */
	USBoolean * AssignOrgUnitPermissionToUserResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignOrgUnitPermissionToUserResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AssignOrgUnitPermissionToUserResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveOrgUnitPermissionFromUser : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSNumber * userId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveOrgUnitPermissionFromUser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSNumber * userId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveOrgUnitPermissionFromUserResponse : NSObject {
	
/* elements */
	USBoolean * RemoveOrgUnitPermissionFromUserResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveOrgUnitPermissionFromUserResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * RemoveOrgUnitPermissionFromUserResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignOrgUnitPermissionToRole : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSNumber * roleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignOrgUnitPermissionToRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSNumber * roleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignOrgUnitPermissionToRoleResponse : NSObject {
	
/* elements */
	USBoolean * AssignOrgUnitPermissionToRoleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignOrgUnitPermissionToRoleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AssignOrgUnitPermissionToRoleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveOrgUnitPermissionFromRole : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSNumber * roleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveOrgUnitPermissionFromRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSNumber * roleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveOrgUnitPermissionFromRoleResponse : NSObject {
	
/* elements */
	USBoolean * RemoveOrgUnitPermissionFromRoleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveOrgUnitPermissionFromRoleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * RemoveOrgUnitPermissionFromRoleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignOrgUnitPermissionToGroup : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSNumber * groupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignOrgUnitPermissionToGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSNumber * groupId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignOrgUnitPermissionToGroupResponse : NSObject {
	
/* elements */
	USBoolean * AssignOrgUnitPermissionToGroupResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignOrgUnitPermissionToGroupResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AssignOrgUnitPermissionToGroupResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveOrgUnitPermissionFromGroup : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSNumber * groupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveOrgUnitPermissionFromGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSNumber * groupId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveOrgUnitPermissionFromGroupResponse : NSObject {
	
/* elements */
	USBoolean * RemoveOrgUnitPermissionFromGroupResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveOrgUnitPermissionFromGroupResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * RemoveOrgUnitPermissionFromGroupResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllUserOrgUnitPermissions : NSObject {
	
/* elements */
	NSNumber * userId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllUserOrgUnitPermissions *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * userId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllUserOrgUnitPermissionsResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysUserOrgUnitPermission * GetAllUserOrgUnitPermissionsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllUserOrgUnitPermissionsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysUserOrgUnitPermission * GetAllUserOrgUnitPermissionsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllRoleOrgUnitPermissions : NSObject {
	
/* elements */
	NSNumber * roleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllRoleOrgUnitPermissions *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * roleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllRoleOrgUnitPermissionsResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysRoleOrgUnitPermission * GetAllRoleOrgUnitPermissionsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllRoleOrgUnitPermissionsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysRoleOrgUnitPermission * GetAllRoleOrgUnitPermissionsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllGroupOrgUnitPermissions : NSObject {
	
/* elements */
	NSNumber * groupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllGroupOrgUnitPermissions *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * groupId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllGroupOrgUnitPermissionsResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysGroupOrgUnitPermission * GetAllGroupOrgUnitPermissionsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllGroupOrgUnitPermissionsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysGroupOrgUnitPermission * GetAllGroupOrgUnitPermissionsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllOrgUnitByUser : NSObject {
	
/* elements */
	NSNumber * userId;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllOrgUnitByUser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * userId;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllOrgUnitByUserResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSP_ORG_UNIT_TREE_BY_USER_Result * GetAllOrgUnitByUserResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllOrgUnitByUserResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSP_ORG_UNIT_TREE_BY_USER_Result * GetAllOrgUnitByUserResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CheckPermisionOnEmployee : NSObject {
	
/* elements */
	NSNumber * userId;
	NSNumber * companyId;
	NSNumber * employeeId;
	NSString * employeeCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CheckPermisionOnEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * userId;
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * employeeId;
@property (retain) NSString * employeeCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_CheckPermisionOnEmployeeResponse : NSObject {
	
/* elements */
	USBoolean * CheckPermisionOnEmployeeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_CheckPermisionOnEmployeeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * CheckPermisionOnEmployeeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllSysEmpProfileGroupLayer : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllSysEmpProfileGroupLayer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllSysEmpProfileGroupLayerResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysEmpProfileGroupLayer * GetAllSysEmpProfileGroupLayerResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllSysEmpProfileGroupLayerResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysEmpProfileGroupLayer * GetAllSysEmpProfileGroupLayerResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetSysEmpProfileLayersByGroupId : NSObject {
	
/* elements */
	NSNumber * groupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetSysEmpProfileLayersByGroupId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * groupId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetSysEmpProfileLayersByGroupIdResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysEmpProfileLayer * GetSysEmpProfileLayersByGroupIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetSysEmpProfileLayersByGroupIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysEmpProfileLayer * GetSysEmpProfileLayersByGroupIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetSysEmpProfileLayerUrlsByLayerId : NSObject {
	
/* elements */
	NSNumber * layerId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetSysEmpProfileLayerUrlsByLayerId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * layerId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetSysEmpProfileLayerUrlsByLayerIdResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysEmpProfileLayerUrl * GetSysEmpProfileLayerUrlsByLayerIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetSysEmpProfileLayerUrlsByLayerIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysEmpProfileLayerUrl * GetSysEmpProfileLayerUrlsByLayerIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetSysEmpProfileLayerUrlsByTypeByLayerId : NSObject {
	
/* elements */
	NSString * typeName;
	NSNumber * layerId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetSysEmpProfileLayerUrlsByTypeByLayerId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * typeName;
@property (retain) NSNumber * layerId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetSysEmpProfileLayerUrlsByTypeByLayerIdResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysEmpProfileLayerUrl * GetSysEmpProfileLayerUrlsByTypeByLayerIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetSysEmpProfileLayerUrlsByTypeByLayerIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysEmpProfileLayerUrl * GetSysEmpProfileLayerUrlsByTypeByLayerIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllRoleEmpProfilePermission : NSObject {
	
/* elements */
	NSNumber * SysRoleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllRoleEmpProfilePermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * SysRoleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllRoleEmpProfilePermissionResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysRoleEmpProfilePermission * GetAllRoleEmpProfilePermissionResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllRoleEmpProfilePermissionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysRoleEmpProfilePermission * GetAllRoleEmpProfilePermissionResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllUserEmpProfilePermission : NSObject {
	
/* elements */
	NSNumber * SysUserId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllUserEmpProfilePermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * SysUserId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllUserEmpProfilePermissionResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysUserEmpProfilePermission * GetAllUserEmpProfilePermissionResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllUserEmpProfilePermissionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysUserEmpProfilePermission * GetAllUserEmpProfilePermissionResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllGroupEmpProfilePermission : NSObject {
	
/* elements */
	NSNumber * SysGroupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllGroupEmpProfilePermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * SysGroupId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllGroupEmpProfilePermissionResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSysGroupEmpProfilePermission * GetAllGroupEmpProfilePermissionResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllGroupEmpProfilePermissionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSysGroupEmpProfilePermission * GetAllGroupEmpProfilePermissionResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignEmpProfileLayerPermissonToUser : NSObject {
	
/* elements */
	NSNumber * SysEmpProfileLayerId;
	NSNumber * userId;
	NSString * type;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignEmpProfileLayerPermissonToUser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * SysEmpProfileLayerId;
@property (retain) NSNumber * userId;
@property (retain) NSString * type;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignEmpProfileLayerPermissonToUserResponse : NSObject {
	
/* elements */
	USBoolean * AssignEmpProfileLayerPermissonToUserResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignEmpProfileLayerPermissonToUserResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AssignEmpProfileLayerPermissonToUserResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignEmpProfileLayerPermissonToRole : NSObject {
	
/* elements */
	NSNumber * SysEmpProfileLayerId;
	NSNumber * roleId;
	NSString * type;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignEmpProfileLayerPermissonToRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * SysEmpProfileLayerId;
@property (retain) NSNumber * roleId;
@property (retain) NSString * type;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignEmpProfileLayerPermissonToRoleResponse : NSObject {
	
/* elements */
	USBoolean * AssignEmpProfileLayerPermissonToRoleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignEmpProfileLayerPermissonToRoleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AssignEmpProfileLayerPermissonToRoleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignEmpProfileLayerPermissonToGroup : NSObject {
	
/* elements */
	NSNumber * SysEmpProfileLayerId;
	NSNumber * groupId;
	NSString * type;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignEmpProfileLayerPermissonToGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * SysEmpProfileLayerId;
@property (retain) NSNumber * groupId;
@property (retain) NSString * type;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignEmpProfileLayerPermissonToGroupResponse : NSObject {
	
/* elements */
	USBoolean * AssignEmpProfileLayerPermissonToGroupResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignEmpProfileLayerPermissonToGroupResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AssignEmpProfileLayerPermissonToGroupResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromUser : NSObject {
	
/* elements */
	NSNumber * SysEmpProfileLayerId;
	NSNumber * userId;
	NSString * type;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromUser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * SysEmpProfileLayerId;
@property (retain) NSNumber * userId;
@property (retain) NSString * type;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromUserResponse : NSObject {
	
/* elements */
	USBoolean * RemoveEmpProfileLayerPermissonFromUserResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromUserResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * RemoveEmpProfileLayerPermissonFromUserResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromGroup : NSObject {
	
/* elements */
	NSNumber * SysEmpProfileLayerId;
	NSNumber * groupId;
	NSString * type;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * SysEmpProfileLayerId;
@property (retain) NSNumber * groupId;
@property (retain) NSString * type;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromGroupResponse : NSObject {
	
/* elements */
	USBoolean * RemoveEmpProfileLayerPermissonFromGroupResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromGroupResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * RemoveEmpProfileLayerPermissonFromGroupResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromRole : NSObject {
	
/* elements */
	NSNumber * SysEmpProfileLayerId;
	NSNumber * roleId;
	NSString * type;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * SysEmpProfileLayerId;
@property (retain) NSNumber * roleId;
@property (retain) NSString * type;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromRoleResponse : NSObject {
	
/* elements */
	USBoolean * RemoveEmpProfileLayerPermissonFromRoleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromRoleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * RemoveEmpProfileLayerPermissonFromRoleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllLayerOfAuthorizedSubAndProfile : NSObject {
	
/* elements */
	tns3_ArrayOfstring * lsLayerCode;
	NSNumber * typeObj;
	NSNumber * objId;
	NSNumber * CompanyId;
	NSNumber * userId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllLayerOfAuthorizedSubAndProfile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns3_ArrayOfstring * lsLayerCode;
@property (retain) NSNumber * typeObj;
@property (retain) NSNumber * objId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * userId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllLayerOfAuthorizedSubAndProfileResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSP_ESS_ALL_PERMIS_ON_PROFILE_LAYER_Result * GetAllLayerOfAuthorizedSubAndProfileResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllLayerOfAuthorizedSubAndProfileResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSP_ESS_ALL_PERMIS_ON_PROFILE_LAYER_Result * GetAllLayerOfAuthorizedSubAndProfileResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetLayerAndPermissOfSubAndProfile : NSObject {
	
/* elements */
	NSString * profileCode;
	NSString * subProfileCode;
	NSNumber * typeObj;
	NSNumber * objId;
	NSNumber * CompanyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetLayerAndPermissOfSubAndProfile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * profileCode;
@property (retain) NSString * subProfileCode;
@property (retain) NSNumber * typeObj;
@property (retain) NSNumber * objId;
@property (retain) NSNumber * CompanyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetLayerAndPermissOfSubAndProfileResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSP_GET_PROF_LAYER_ALL_PERMIS_Result * GetLayerAndPermissOfSubAndProfileResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetLayerAndPermissOfSubAndProfileResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSP_GET_PROF_LAYER_ALL_PERMIS_Result * GetLayerAndPermissOfSubAndProfileResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllLayerOfAuthorizedIncludedSubAndProfileForTree : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * UserId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllLayerOfAuthorizedIncludedSubAndProfileForTree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * UserId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetAllLayerOfAuthorizedIncludedSubAndProfileForTreeResponse : NSObject {
	
/* elements */
	tns1_ArrayOfSP_ESS_PRO_LAYER_VIEW_PER_TREE_Result * GetAllLayerOfAuthorizedIncludedSubAndProfileForTreeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetAllLayerOfAuthorizedIncludedSubAndProfileForTreeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfSP_ESS_PRO_LAYER_VIEW_PER_TREE_Result * GetAllLayerOfAuthorizedIncludedSubAndProfileForTreeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetOnePermissOfUserOnProfileLayer : NSObject {
	
/* elements */
	NSString * layerCode;
	NSString * type;
	NSNumber * UserId;
	NSNumber * CompanyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetOnePermissOfUserOnProfileLayer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * layerCode;
@property (retain) NSString * type;
@property (retain) NSNumber * UserId;
@property (retain) NSNumber * CompanyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_GetOnePermissOfUserOnProfileLayerResponse : NSObject {
	
/* elements */
	USBoolean * GetOnePermissOfUserOnProfileLayerResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_GetOnePermissOfUserOnProfileLayerResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * GetOnePermissOfUserOnProfileLayerResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignProfileLayerPermissonToUserGroupRole : NSObject {
	
/* elements */
	NSString * layerCode;
	NSNumber * typeObj;
	NSNumber * objId;
	NSString * typePermiss;
	NSNumber * companyId;
	NSNumber * userId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignProfileLayerPermissonToUserGroupRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * layerCode;
@property (retain) NSNumber * typeObj;
@property (retain) NSNumber * objId;
@property (retain) NSString * typePermiss;
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * userId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_AssignProfileLayerPermissonToUserGroupRoleResponse : NSObject {
	
/* elements */
	USBoolean * AssignProfileLayerPermissonToUserGroupRoleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_AssignProfileLayerPermissonToUserGroupRoleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AssignProfileLayerPermissonToUserGroupRoleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveProfileLayerPermissonFromUserGroupRole : NSObject {
	
/* elements */
	NSString * layerCode;
	NSNumber * typeObj;
	NSNumber * objId;
	NSString * typePermiss;
	NSNumber * companyId;
	NSNumber * userId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveProfileLayerPermissonFromUserGroupRole *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * layerCode;
@property (retain) NSNumber * typeObj;
@property (retain) NSNumber * objId;
@property (retain) NSString * typePermiss;
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * userId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface SecurityServiceSvc_RemoveProfileLayerPermissonFromUserGroupRoleResponse : NSObject {
	
/* elements */
	USBoolean * RemoveProfileLayerPermissonFromUserGroupRoleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (SecurityServiceSvc_RemoveProfileLayerPermissonFromUserGroupRoleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * RemoveProfileLayerPermissonFromUserGroupRoleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
/* Cookies handling provided by http://en.wikibooks.org/wiki/Programming:WebObjects/Web_Services/Web_Service_Provider */
#import <libxml/parser.h>
#import "xs.h"
#import "SecurityServiceSvc.h"
#import "ns1.h"
#import "tns1.h"
#import "tns4.h"
#import "tns2.h"
#import "tns3.h"
@class BasicHttpBinding_ISecurityServiceBinding;
@interface SecurityServiceSvc : NSObject {
	
}
+ (BasicHttpBinding_ISecurityServiceBinding *)BasicHttpBinding_ISecurityServiceBinding;
@end
@class BasicHttpBinding_ISecurityServiceBindingResponse;
@class BasicHttpBinding_ISecurityServiceBindingOperation;
@protocol BasicHttpBinding_ISecurityServiceBindingResponseDelegate <NSObject>
- (void) operation:(BasicHttpBinding_ISecurityServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_ISecurityServiceBindingResponse *)response;
@end
@interface BasicHttpBinding_ISecurityServiceBinding : NSObject <BasicHttpBinding_ISecurityServiceBindingResponseDelegate> {
	NSURL *address;
	NSTimeInterval defaultTimeout;
	NSMutableArray *cookies;
	BOOL logXMLInOut;
	BOOL synchronousOperationComplete;
	NSString *authUsername;
	NSString *authPassword;
}
@property (copy) NSURL *address;
@property (assign) BOOL logXMLInOut;
@property (assign) NSTimeInterval defaultTimeout;
@property (nonatomic, retain) NSMutableArray *cookies;
@property (nonatomic, retain) NSString *authUsername;
@property (nonatomic, retain) NSString *authPassword;
- (id)initWithAddress:(NSString *)anAddress;
- (void)sendHTTPCallUsingBody:(NSString *)body soapAction:(NSString *)soapAction forOperation:(BasicHttpBinding_ISecurityServiceBindingOperation *)operation;
- (void)addCookie:(NSHTTPCookie *)toAdd;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetAllUnitsUsingParameters:(SecurityServiceSvc_GetAllUnits *)aParameters ;
- (void)GetAllUnitsAsyncUsingParameters:(SecurityServiceSvc_GetAllUnits *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)CreateMembershipUsingParameters:(SecurityServiceSvc_CreateMembership *)aParameters ;
- (void)CreateMembershipAsyncUsingParameters:(SecurityServiceSvc_CreateMembership *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)CreateUserByObjectUsingParameters:(SecurityServiceSvc_CreateUserByObject *)aParameters ;
- (void)CreateUserByObjectAsyncUsingParameters:(SecurityServiceSvc_CreateUserByObject *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)EditUserUsingParameters:(SecurityServiceSvc_EditUser *)aParameters ;
- (void)EditUserAsyncUsingParameters:(SecurityServiceSvc_EditUser *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)DeactiveUserUsingParameters:(SecurityServiceSvc_DeactiveUser *)aParameters ;
- (void)DeactiveUserAsyncUsingParameters:(SecurityServiceSvc_DeactiveUser *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)CleanLoginSessionUsingParameters:(SecurityServiceSvc_CleanLoginSession *)aParameters ;
- (void)CleanLoginSessionAsyncUsingParameters:(SecurityServiceSvc_CleanLoginSession *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)SuccessLogoutUsingParameters:(SecurityServiceSvc_SuccessLogout *)aParameters ;
- (void)SuccessLogoutAsyncUsingParameters:(SecurityServiceSvc_SuccessLogout *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)UpdateUserUsingParameters:(SecurityServiceSvc_UpdateUser *)aParameters ;
- (void)UpdateUserAsyncUsingParameters:(SecurityServiceSvc_UpdateUser *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)UpdateMembershipUsingParameters:(SecurityServiceSvc_UpdateMembership *)aParameters ;
- (void)UpdateMembershipAsyncUsingParameters:(SecurityServiceSvc_UpdateMembership *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)DeleteUserUsingParameters:(SecurityServiceSvc_DeleteUser *)aParameters ;
- (void)DeleteUserAsyncUsingParameters:(SecurityServiceSvc_DeleteUser *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetUserByUserNameUsingParameters:(SecurityServiceSvc_GetUserByUserName *)aParameters ;
- (void)GetUserByUserNameAsyncUsingParameters:(SecurityServiceSvc_GetUserByUserName *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetUserbyUserIdUsingParameters:(SecurityServiceSvc_GetUserbyUserId *)aParameters ;
- (void)GetUserbyUserIdAsyncUsingParameters:(SecurityServiceSvc_GetUserbyUserId *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetUserMembershipByUserIdUsingParameters:(SecurityServiceSvc_GetUserMembershipByUserId *)aParameters ;
- (void)GetUserMembershipByUserIdAsyncUsingParameters:(SecurityServiceSvc_GetUserMembershipByUserId *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetUserMembershipByUserNameUsingParameters:(SecurityServiceSvc_GetUserMembershipByUserName *)aParameters ;
- (void)GetUserMembershipByUserNameAsyncUsingParameters:(SecurityServiceSvc_GetUserMembershipByUserName *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetAllUsersUsingParameters:(SecurityServiceSvc_GetAllUsers *)aParameters ;
- (void)GetAllUsersAsyncUsingParameters:(SecurityServiceSvc_GetAllUsers *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetUsersByRoleIdUsingParameters:(SecurityServiceSvc_GetUsersByRoleId *)aParameters ;
- (void)GetUsersByRoleIdAsyncUsingParameters:(SecurityServiceSvc_GetUsersByRoleId *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetUsersByCompanyIdUsingParameters:(SecurityServiceSvc_GetUsersByCompanyId *)aParameters ;
- (void)GetUsersByCompanyIdAsyncUsingParameters:(SecurityServiceSvc_GetUsersByCompanyId *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetUsersByGroupIdUsingParameters:(SecurityServiceSvc_GetUsersByGroupId *)aParameters ;
- (void)GetUsersByGroupIdAsyncUsingParameters:(SecurityServiceSvc_GetUsersByGroupId *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetGroupsByRoleIdUsingParameters:(SecurityServiceSvc_GetGroupsByRoleId *)aParameters ;
- (void)GetGroupsByRoleIdAsyncUsingParameters:(SecurityServiceSvc_GetGroupsByRoleId *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetGroupsByUserIdUsingParameters:(SecurityServiceSvc_GetGroupsByUserId *)aParameters ;
- (void)GetGroupsByUserIdAsyncUsingParameters:(SecurityServiceSvc_GetGroupsByUserId *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetGroupsByCompanyIdUsingParameters:(SecurityServiceSvc_GetGroupsByCompanyId *)aParameters ;
- (void)GetGroupsByCompanyIdAsyncUsingParameters:(SecurityServiceSvc_GetGroupsByCompanyId *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetRolesByGroupIdUsingParameters:(SecurityServiceSvc_GetRolesByGroupId *)aParameters ;
- (void)GetRolesByGroupIdAsyncUsingParameters:(SecurityServiceSvc_GetRolesByGroupId *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetRolesByCompanyIdUsingParameters:(SecurityServiceSvc_GetRolesByCompanyId *)aParameters ;
- (void)GetRolesByCompanyIdAsyncUsingParameters:(SecurityServiceSvc_GetRolesByCompanyId *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetRolesByUserIdUsingParameters:(SecurityServiceSvc_GetRolesByUserId *)aParameters ;
- (void)GetRolesByUserIdAsyncUsingParameters:(SecurityServiceSvc_GetRolesByUserId *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetMembershipByUserIdUsingParameters:(SecurityServiceSvc_GetMembershipByUserId *)aParameters ;
- (void)GetMembershipByUserIdAsyncUsingParameters:(SecurityServiceSvc_GetMembershipByUserId *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetEmployeeByIdUsingParameters:(SecurityServiceSvc_GetEmployeeById *)aParameters ;
- (void)GetEmployeeByIdAsyncUsingParameters:(SecurityServiceSvc_GetEmployeeById *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetUsersByConditionUsingParameters:(SecurityServiceSvc_GetUsersByCondition *)aParameters ;
- (void)GetUsersByConditionAsyncUsingParameters:(SecurityServiceSvc_GetUsersByCondition *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetUsersSuggestByConditionUsingParameters:(SecurityServiceSvc_GetUsersSuggestByCondition *)aParameters ;
- (void)GetUsersSuggestByConditionAsyncUsingParameters:(SecurityServiceSvc_GetUsersSuggestByCondition *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)CreateRoleUsingParameters:(SecurityServiceSvc_CreateRole *)aParameters ;
- (void)CreateRoleAsyncUsingParameters:(SecurityServiceSvc_CreateRole *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)UpdateRoleUsingParameters:(SecurityServiceSvc_UpdateRole *)aParameters ;
- (void)UpdateRoleAsyncUsingParameters:(SecurityServiceSvc_UpdateRole *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)DeleteRoleUsingParameters:(SecurityServiceSvc_DeleteRole *)aParameters ;
- (void)DeleteRoleAsyncUsingParameters:(SecurityServiceSvc_DeleteRole *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetRoleUsingParameters:(SecurityServiceSvc_GetRole *)aParameters ;
- (void)GetRoleAsyncUsingParameters:(SecurityServiceSvc_GetRole *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetRolesByNameUsingParameters:(SecurityServiceSvc_GetRolesByName *)aParameters ;
- (void)GetRolesByNameAsyncUsingParameters:(SecurityServiceSvc_GetRolesByName *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetGroupsByNameUsingParameters:(SecurityServiceSvc_GetGroupsByName *)aParameters ;
- (void)GetGroupsByNameAsyncUsingParameters:(SecurityServiceSvc_GetGroupsByName *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetAllRolesUsingParameters:(SecurityServiceSvc_GetAllRoles *)aParameters ;
- (void)GetAllRolesAsyncUsingParameters:(SecurityServiceSvc_GetAllRoles *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)CreateGroupUsingParameters:(SecurityServiceSvc_CreateGroup *)aParameters ;
- (void)CreateGroupAsyncUsingParameters:(SecurityServiceSvc_CreateGroup *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)UpdateGroupUsingParameters:(SecurityServiceSvc_UpdateGroup *)aParameters ;
- (void)UpdateGroupAsyncUsingParameters:(SecurityServiceSvc_UpdateGroup *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)DeleteGroupUsingParameters:(SecurityServiceSvc_DeleteGroup *)aParameters ;
- (void)DeleteGroupAsyncUsingParameters:(SecurityServiceSvc_DeleteGroup *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetGroupUsingParameters:(SecurityServiceSvc_GetGroup *)aParameters ;
- (void)GetGroupAsyncUsingParameters:(SecurityServiceSvc_GetGroup *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetAllGroupsUsingParameters:(SecurityServiceSvc_GetAllGroups *)aParameters ;
- (void)GetAllGroupsAsyncUsingParameters:(SecurityServiceSvc_GetAllGroups *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetAllCompaniesUsingParameters:(SecurityServiceSvc_GetAllCompanies *)aParameters ;
- (void)GetAllCompaniesAsyncUsingParameters:(SecurityServiceSvc_GetAllCompanies *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetAllApplicationsUsingParameters:(SecurityServiceSvc_GetAllApplications *)aParameters ;
- (void)GetAllApplicationsAsyncUsingParameters:(SecurityServiceSvc_GetAllApplications *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetApplicationByIdUsingParameters:(SecurityServiceSvc_GetApplicationById *)aParameters ;
- (void)GetApplicationByIdAsyncUsingParameters:(SecurityServiceSvc_GetApplicationById *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetAllModulesUsingParameters:(SecurityServiceSvc_GetAllModules *)aParameters ;
- (void)GetAllModulesAsyncUsingParameters:(SecurityServiceSvc_GetAllModules *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetModulesByAppIdUsingParameters:(SecurityServiceSvc_GetModulesByAppId *)aParameters ;
- (void)GetModulesByAppIdAsyncUsingParameters:(SecurityServiceSvc_GetModulesByAppId *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetModuleByIdUsingParameters:(SecurityServiceSvc_GetModuleById *)aParameters ;
- (void)GetModuleByIdAsyncUsingParameters:(SecurityServiceSvc_GetModuleById *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetAllFeaturesUsingParameters:(SecurityServiceSvc_GetAllFeatures *)aParameters ;
- (void)GetAllFeaturesAsyncUsingParameters:(SecurityServiceSvc_GetAllFeatures *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetFeaturesByModuleIdUsingParameters:(SecurityServiceSvc_GetFeaturesByModuleId *)aParameters ;
- (void)GetFeaturesByModuleIdAsyncUsingParameters:(SecurityServiceSvc_GetFeaturesByModuleId *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetFeatureByIdUsingParameters:(SecurityServiceSvc_GetFeatureById *)aParameters ;
- (void)GetFeatureByIdAsyncUsingParameters:(SecurityServiceSvc_GetFeatureById *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetAllActionsByFeatureIdUsingParameters:(SecurityServiceSvc_GetAllActionsByFeatureId *)aParameters ;
- (void)GetAllActionsByFeatureIdAsyncUsingParameters:(SecurityServiceSvc_GetAllActionsByFeatureId *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetV_ActionByActionIdUsingParameters:(SecurityServiceSvc_GetV_ActionByActionId *)aParameters ;
- (void)GetV_ActionByActionIdAsyncUsingParameters:(SecurityServiceSvc_GetV_ActionByActionId *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetAllV_ActionsByFeatureIdUsingParameters:(SecurityServiceSvc_GetAllV_ActionsByFeatureId *)aParameters ;
- (void)GetAllV_ActionsByFeatureIdAsyncUsingParameters:(SecurityServiceSvc_GetAllV_ActionsByFeatureId *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetLinksByActionIdUsingParameters:(SecurityServiceSvc_GetLinksByActionId *)aParameters ;
- (void)GetLinksByActionIdAsyncUsingParameters:(SecurityServiceSvc_GetLinksByActionId *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetAllActionTypesUsingParameters:(SecurityServiceSvc_GetAllActionTypes *)aParameters ;
- (void)GetAllActionTypesAsyncUsingParameters:(SecurityServiceSvc_GetAllActionTypes *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetActionTypeByIdUsingParameters:(SecurityServiceSvc_GetActionTypeById *)aParameters ;
- (void)GetActionTypeByIdAsyncUsingParameters:(SecurityServiceSvc_GetActionTypeById *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)AssignActionToRoleUsingParameters:(SecurityServiceSvc_AssignActionToRole *)aParameters ;
- (void)AssignActionToRoleAsyncUsingParameters:(SecurityServiceSvc_AssignActionToRole *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)AssignActionToUserUsingParameters:(SecurityServiceSvc_AssignActionToUser *)aParameters ;
- (void)AssignActionToUserAsyncUsingParameters:(SecurityServiceSvc_AssignActionToUser *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)AssignActionToGroupUsingParameters:(SecurityServiceSvc_AssignActionToGroup *)aParameters ;
- (void)AssignActionToGroupAsyncUsingParameters:(SecurityServiceSvc_AssignActionToGroup *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)RemoveActionFromRoleUsingParameters:(SecurityServiceSvc_RemoveActionFromRole *)aParameters ;
- (void)RemoveActionFromRoleAsyncUsingParameters:(SecurityServiceSvc_RemoveActionFromRole *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)RemoveActionFromUserUsingParameters:(SecurityServiceSvc_RemoveActionFromUser *)aParameters ;
- (void)RemoveActionFromUserAsyncUsingParameters:(SecurityServiceSvc_RemoveActionFromUser *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)RemoveActionFromGrouprUsingParameters:(SecurityServiceSvc_RemoveActionFromGroupr *)aParameters ;
- (void)RemoveActionFromGrouprAsyncUsingParameters:(SecurityServiceSvc_RemoveActionFromGroupr *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)AssignRoleToUserUsingParameters:(SecurityServiceSvc_AssignRoleToUser *)aParameters ;
- (void)AssignRoleToUserAsyncUsingParameters:(SecurityServiceSvc_AssignRoleToUser *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)RemoveRoleFromUserUsingParameters:(SecurityServiceSvc_RemoveRoleFromUser *)aParameters ;
- (void)RemoveRoleFromUserAsyncUsingParameters:(SecurityServiceSvc_RemoveRoleFromUser *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)AssignRoleToGroupUsingParameters:(SecurityServiceSvc_AssignRoleToGroup *)aParameters ;
- (void)AssignRoleToGroupAsyncUsingParameters:(SecurityServiceSvc_AssignRoleToGroup *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)RemoveRoleFromGroupUsingParameters:(SecurityServiceSvc_RemoveRoleFromGroup *)aParameters ;
- (void)RemoveRoleFromGroupAsyncUsingParameters:(SecurityServiceSvc_RemoveRoleFromGroup *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)AssignGroupToUserUsingParameters:(SecurityServiceSvc_AssignGroupToUser *)aParameters ;
- (void)AssignGroupToUserAsyncUsingParameters:(SecurityServiceSvc_AssignGroupToUser *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)RemoveGroupFromUserUsingParameters:(SecurityServiceSvc_RemoveGroupFromUser *)aParameters ;
- (void)RemoveGroupFromUserAsyncUsingParameters:(SecurityServiceSvc_RemoveGroupFromUser *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)ChangePasswordUsingParameters:(SecurityServiceSvc_ChangePassword *)aParameters ;
- (void)ChangePasswordAsyncUsingParameters:(SecurityServiceSvc_ChangePassword *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)SuccessLoginUsingParameters:(SecurityServiceSvc_SuccessLogin *)aParameters ;
- (void)SuccessLoginAsyncUsingParameters:(SecurityServiceSvc_SuccessLogin *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)CheckActiveUserUsingParameters:(SecurityServiceSvc_CheckActiveUser *)aParameters ;
- (void)CheckActiveUserAsyncUsingParameters:(SecurityServiceSvc_CheckActiveUser *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)AddSysUserLogUsingParameters:(SecurityServiceSvc_AddSysUserLog *)aParameters ;
- (void)AddSysUserLogAsyncUsingParameters:(SecurityServiceSvc_AddSysUserLog *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetAllUserPermissionsUsingParameters:(SecurityServiceSvc_GetAllUserPermissions *)aParameters ;
- (void)GetAllUserPermissionsAsyncUsingParameters:(SecurityServiceSvc_GetAllUserPermissions *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetAllRolePermissionsUsingParameters:(SecurityServiceSvc_GetAllRolePermissions *)aParameters ;
- (void)GetAllRolePermissionsAsyncUsingParameters:(SecurityServiceSvc_GetAllRolePermissions *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetAllGroupPermissionsUsingParameters:(SecurityServiceSvc_GetAllGroupPermissions *)aParameters ;
- (void)GetAllGroupPermissionsAsyncUsingParameters:(SecurityServiceSvc_GetAllGroupPermissions *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)HavePermissionUsingParameters:(SecurityServiceSvc_HavePermission *)aParameters ;
- (void)HavePermissionAsyncUsingParameters:(SecurityServiceSvc_HavePermission *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)CreateModuleUsingParameters:(SecurityServiceSvc_CreateModule *)aParameters ;
- (void)CreateModuleAsyncUsingParameters:(SecurityServiceSvc_CreateModule *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)UpdateModuleUsingParameters:(SecurityServiceSvc_UpdateModule *)aParameters ;
- (void)UpdateModuleAsyncUsingParameters:(SecurityServiceSvc_UpdateModule *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)DeleteModuleUsingParameters:(SecurityServiceSvc_DeleteModule *)aParameters ;
- (void)DeleteModuleAsyncUsingParameters:(SecurityServiceSvc_DeleteModule *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)CreateFeatureUsingParameters:(SecurityServiceSvc_CreateFeature *)aParameters ;
- (void)CreateFeatureAsyncUsingParameters:(SecurityServiceSvc_CreateFeature *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)UpdateFeatureUsingParameters:(SecurityServiceSvc_UpdateFeature *)aParameters ;
- (void)UpdateFeatureAsyncUsingParameters:(SecurityServiceSvc_UpdateFeature *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)DeleteFeatureUsingParameters:(SecurityServiceSvc_DeleteFeature *)aParameters ;
- (void)DeleteFeatureAsyncUsingParameters:(SecurityServiceSvc_DeleteFeature *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)CreateActionUsingParameters:(SecurityServiceSvc_CreateAction *)aParameters ;
- (void)CreateActionAsyncUsingParameters:(SecurityServiceSvc_CreateAction *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetActionByFeatureIdAndActionTypIdUsingParameters:(SecurityServiceSvc_GetActionByFeatureIdAndActionTypId *)aParameters ;
- (void)GetActionByFeatureIdAndActionTypIdAsyncUsingParameters:(SecurityServiceSvc_GetActionByFeatureIdAndActionTypId *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)DeleteActionUsingParameters:(SecurityServiceSvc_DeleteAction *)aParameters ;
- (void)DeleteActionAsyncUsingParameters:(SecurityServiceSvc_DeleteAction *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)CreateLinkUsingParameters:(SecurityServiceSvc_CreateLink *)aParameters ;
- (void)CreateLinkAsyncUsingParameters:(SecurityServiceSvc_CreateLink *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)UpdateLinkUsingParameters:(SecurityServiceSvc_UpdateLink *)aParameters ;
- (void)UpdateLinkAsyncUsingParameters:(SecurityServiceSvc_UpdateLink *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)DeleteLinkUsingParameters:(SecurityServiceSvc_DeleteLink *)aParameters ;
- (void)DeleteLinkAsyncUsingParameters:(SecurityServiceSvc_DeleteLink *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)CheckUserIsInRoleUsingParameters:(SecurityServiceSvc_CheckUserIsInRole *)aParameters ;
- (void)CheckUserIsInRoleAsyncUsingParameters:(SecurityServiceSvc_CheckUserIsInRole *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)AssignUsersToRoleUsingParameters:(SecurityServiceSvc_AssignUsersToRole *)aParameters ;
- (void)AssignUsersToRoleAsyncUsingParameters:(SecurityServiceSvc_AssignUsersToRole *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)CheckGroupIsInRoleUsingParameters:(SecurityServiceSvc_CheckGroupIsInRole *)aParameters ;
- (void)CheckGroupIsInRoleAsyncUsingParameters:(SecurityServiceSvc_CheckGroupIsInRole *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)AssignGroupsToRoleUsingParameters:(SecurityServiceSvc_AssignGroupsToRole *)aParameters ;
- (void)AssignGroupsToRoleAsyncUsingParameters:(SecurityServiceSvc_AssignGroupsToRole *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)CheckUserIsInGroupUsingParameters:(SecurityServiceSvc_CheckUserIsInGroup *)aParameters ;
- (void)CheckUserIsInGroupAsyncUsingParameters:(SecurityServiceSvc_CheckUserIsInGroup *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)AssignUsersToGroupUsingParameters:(SecurityServiceSvc_AssignUsersToGroup *)aParameters ;
- (void)AssignUsersToGroupAsyncUsingParameters:(SecurityServiceSvc_AssignUsersToGroup *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetSelectedUsersByIdUsingParameters:(SecurityServiceSvc_GetSelectedUsersById *)aParameters ;
- (void)GetSelectedUsersByIdAsyncUsingParameters:(SecurityServiceSvc_GetSelectedUsersById *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetUserEmployeeByUserIdUsingParameters:(SecurityServiceSvc_GetUserEmployeeByUserId *)aParameters ;
- (void)GetUserEmployeeByUserIdAsyncUsingParameters:(SecurityServiceSvc_GetUserEmployeeByUserId *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetUserMembershipByUserDomainUsingParameters:(SecurityServiceSvc_GetUserMembershipByUserDomain *)aParameters ;
- (void)GetUserMembershipByUserDomainAsyncUsingParameters:(SecurityServiceSvc_GetUserMembershipByUserDomain *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)AssignOrgUnitPermissionToUserUsingParameters:(SecurityServiceSvc_AssignOrgUnitPermissionToUser *)aParameters ;
- (void)AssignOrgUnitPermissionToUserAsyncUsingParameters:(SecurityServiceSvc_AssignOrgUnitPermissionToUser *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)RemoveOrgUnitPermissionFromUserUsingParameters:(SecurityServiceSvc_RemoveOrgUnitPermissionFromUser *)aParameters ;
- (void)RemoveOrgUnitPermissionFromUserAsyncUsingParameters:(SecurityServiceSvc_RemoveOrgUnitPermissionFromUser *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)AssignOrgUnitPermissionToRoleUsingParameters:(SecurityServiceSvc_AssignOrgUnitPermissionToRole *)aParameters ;
- (void)AssignOrgUnitPermissionToRoleAsyncUsingParameters:(SecurityServiceSvc_AssignOrgUnitPermissionToRole *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)RemoveOrgUnitPermissionFromRoleUsingParameters:(SecurityServiceSvc_RemoveOrgUnitPermissionFromRole *)aParameters ;
- (void)RemoveOrgUnitPermissionFromRoleAsyncUsingParameters:(SecurityServiceSvc_RemoveOrgUnitPermissionFromRole *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)AssignOrgUnitPermissionToGroupUsingParameters:(SecurityServiceSvc_AssignOrgUnitPermissionToGroup *)aParameters ;
- (void)AssignOrgUnitPermissionToGroupAsyncUsingParameters:(SecurityServiceSvc_AssignOrgUnitPermissionToGroup *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)RemoveOrgUnitPermissionFromGroupUsingParameters:(SecurityServiceSvc_RemoveOrgUnitPermissionFromGroup *)aParameters ;
- (void)RemoveOrgUnitPermissionFromGroupAsyncUsingParameters:(SecurityServiceSvc_RemoveOrgUnitPermissionFromGroup *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetAllUserOrgUnitPermissionsUsingParameters:(SecurityServiceSvc_GetAllUserOrgUnitPermissions *)aParameters ;
- (void)GetAllUserOrgUnitPermissionsAsyncUsingParameters:(SecurityServiceSvc_GetAllUserOrgUnitPermissions *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetAllRoleOrgUnitPermissionsUsingParameters:(SecurityServiceSvc_GetAllRoleOrgUnitPermissions *)aParameters ;
- (void)GetAllRoleOrgUnitPermissionsAsyncUsingParameters:(SecurityServiceSvc_GetAllRoleOrgUnitPermissions *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetAllGroupOrgUnitPermissionsUsingParameters:(SecurityServiceSvc_GetAllGroupOrgUnitPermissions *)aParameters ;
- (void)GetAllGroupOrgUnitPermissionsAsyncUsingParameters:(SecurityServiceSvc_GetAllGroupOrgUnitPermissions *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetAllOrgUnitByUserUsingParameters:(SecurityServiceSvc_GetAllOrgUnitByUser *)aParameters ;
- (void)GetAllOrgUnitByUserAsyncUsingParameters:(SecurityServiceSvc_GetAllOrgUnitByUser *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)CheckPermisionOnEmployeeUsingParameters:(SecurityServiceSvc_CheckPermisionOnEmployee *)aParameters ;
- (void)CheckPermisionOnEmployeeAsyncUsingParameters:(SecurityServiceSvc_CheckPermisionOnEmployee *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetAllSysEmpProfileGroupLayerUsingParameters:(SecurityServiceSvc_GetAllSysEmpProfileGroupLayer *)aParameters ;
- (void)GetAllSysEmpProfileGroupLayerAsyncUsingParameters:(SecurityServiceSvc_GetAllSysEmpProfileGroupLayer *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetSysEmpProfileLayersByGroupIdUsingParameters:(SecurityServiceSvc_GetSysEmpProfileLayersByGroupId *)aParameters ;
- (void)GetSysEmpProfileLayersByGroupIdAsyncUsingParameters:(SecurityServiceSvc_GetSysEmpProfileLayersByGroupId *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetSysEmpProfileLayerUrlsByLayerIdUsingParameters:(SecurityServiceSvc_GetSysEmpProfileLayerUrlsByLayerId *)aParameters ;
- (void)GetSysEmpProfileLayerUrlsByLayerIdAsyncUsingParameters:(SecurityServiceSvc_GetSysEmpProfileLayerUrlsByLayerId *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetSysEmpProfileLayerUrlsByTypeByLayerIdUsingParameters:(SecurityServiceSvc_GetSysEmpProfileLayerUrlsByTypeByLayerId *)aParameters ;
- (void)GetSysEmpProfileLayerUrlsByTypeByLayerIdAsyncUsingParameters:(SecurityServiceSvc_GetSysEmpProfileLayerUrlsByTypeByLayerId *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetAllRoleEmpProfilePermissionUsingParameters:(SecurityServiceSvc_GetAllRoleEmpProfilePermission *)aParameters ;
- (void)GetAllRoleEmpProfilePermissionAsyncUsingParameters:(SecurityServiceSvc_GetAllRoleEmpProfilePermission *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetAllUserEmpProfilePermissionUsingParameters:(SecurityServiceSvc_GetAllUserEmpProfilePermission *)aParameters ;
- (void)GetAllUserEmpProfilePermissionAsyncUsingParameters:(SecurityServiceSvc_GetAllUserEmpProfilePermission *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetAllGroupEmpProfilePermissionUsingParameters:(SecurityServiceSvc_GetAllGroupEmpProfilePermission *)aParameters ;
- (void)GetAllGroupEmpProfilePermissionAsyncUsingParameters:(SecurityServiceSvc_GetAllGroupEmpProfilePermission *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)AssignEmpProfileLayerPermissonToUserUsingParameters:(SecurityServiceSvc_AssignEmpProfileLayerPermissonToUser *)aParameters ;
- (void)AssignEmpProfileLayerPermissonToUserAsyncUsingParameters:(SecurityServiceSvc_AssignEmpProfileLayerPermissonToUser *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)AssignEmpProfileLayerPermissonToRoleUsingParameters:(SecurityServiceSvc_AssignEmpProfileLayerPermissonToRole *)aParameters ;
- (void)AssignEmpProfileLayerPermissonToRoleAsyncUsingParameters:(SecurityServiceSvc_AssignEmpProfileLayerPermissonToRole *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)AssignEmpProfileLayerPermissonToGroupUsingParameters:(SecurityServiceSvc_AssignEmpProfileLayerPermissonToGroup *)aParameters ;
- (void)AssignEmpProfileLayerPermissonToGroupAsyncUsingParameters:(SecurityServiceSvc_AssignEmpProfileLayerPermissonToGroup *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)RemoveEmpProfileLayerPermissonFromUserUsingParameters:(SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromUser *)aParameters ;
- (void)RemoveEmpProfileLayerPermissonFromUserAsyncUsingParameters:(SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromUser *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)RemoveEmpProfileLayerPermissonFromGroupUsingParameters:(SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromGroup *)aParameters ;
- (void)RemoveEmpProfileLayerPermissonFromGroupAsyncUsingParameters:(SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromGroup *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)RemoveEmpProfileLayerPermissonFromRoleUsingParameters:(SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromRole *)aParameters ;
- (void)RemoveEmpProfileLayerPermissonFromRoleAsyncUsingParameters:(SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromRole *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetAllLayerOfAuthorizedSubAndProfileUsingParameters:(SecurityServiceSvc_GetAllLayerOfAuthorizedSubAndProfile *)aParameters ;
- (void)GetAllLayerOfAuthorizedSubAndProfileAsyncUsingParameters:(SecurityServiceSvc_GetAllLayerOfAuthorizedSubAndProfile *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetLayerAndPermissOfSubAndProfileUsingParameters:(SecurityServiceSvc_GetLayerAndPermissOfSubAndProfile *)aParameters ;
- (void)GetLayerAndPermissOfSubAndProfileAsyncUsingParameters:(SecurityServiceSvc_GetLayerAndPermissOfSubAndProfile *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetAllLayerOfAuthorizedIncludedSubAndProfileForTreeUsingParameters:(SecurityServiceSvc_GetAllLayerOfAuthorizedIncludedSubAndProfileForTree *)aParameters ;
- (void)GetAllLayerOfAuthorizedIncludedSubAndProfileForTreeAsyncUsingParameters:(SecurityServiceSvc_GetAllLayerOfAuthorizedIncludedSubAndProfileForTree *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)GetOnePermissOfUserOnProfileLayerUsingParameters:(SecurityServiceSvc_GetOnePermissOfUserOnProfileLayer *)aParameters ;
- (void)GetOnePermissOfUserOnProfileLayerAsyncUsingParameters:(SecurityServiceSvc_GetOnePermissOfUserOnProfileLayer *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)AssignProfileLayerPermissonToUserGroupRoleUsingParameters:(SecurityServiceSvc_AssignProfileLayerPermissonToUserGroupRole *)aParameters ;
- (void)AssignProfileLayerPermissonToUserGroupRoleAsyncUsingParameters:(SecurityServiceSvc_AssignProfileLayerPermissonToUserGroupRole *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_ISecurityServiceBindingResponse *)RemoveProfileLayerPermissonFromUserGroupRoleUsingParameters:(SecurityServiceSvc_RemoveProfileLayerPermissonFromUserGroupRole *)aParameters ;
- (void)RemoveProfileLayerPermissonFromUserGroupRoleAsyncUsingParameters:(SecurityServiceSvc_RemoveProfileLayerPermissonFromUserGroupRole *)aParameters  delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)responseDelegate;
@end
@interface BasicHttpBinding_ISecurityServiceBindingOperation : NSOperation {
	BasicHttpBinding_ISecurityServiceBinding *binding;
	BasicHttpBinding_ISecurityServiceBindingResponse *response;
	id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate> delegate;
	NSMutableData *responseData;
	NSURLConnection *urlConnection;
}
@property (retain) BasicHttpBinding_ISecurityServiceBinding *binding;
@property (readonly) BasicHttpBinding_ISecurityServiceBindingResponse *response;
@property (nonatomic, assign) id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate> delegate;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, retain) NSURLConnection *urlConnection;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetAllUnits : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetAllUnits * parameters;
}
@property (retain) SecurityServiceSvc_GetAllUnits * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetAllUnits *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_CreateMembership : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_CreateMembership * parameters;
}
@property (retain) SecurityServiceSvc_CreateMembership * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_CreateMembership *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_CreateUserByObject : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_CreateUserByObject * parameters;
}
@property (retain) SecurityServiceSvc_CreateUserByObject * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_CreateUserByObject *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_EditUser : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_EditUser * parameters;
}
@property (retain) SecurityServiceSvc_EditUser * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_EditUser *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_DeactiveUser : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_DeactiveUser * parameters;
}
@property (retain) SecurityServiceSvc_DeactiveUser * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_DeactiveUser *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_CleanLoginSession : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_CleanLoginSession * parameters;
}
@property (retain) SecurityServiceSvc_CleanLoginSession * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_CleanLoginSession *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_SuccessLogout : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_SuccessLogout * parameters;
}
@property (retain) SecurityServiceSvc_SuccessLogout * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_SuccessLogout *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_UpdateUser : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_UpdateUser * parameters;
}
@property (retain) SecurityServiceSvc_UpdateUser * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_UpdateUser *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_UpdateMembership : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_UpdateMembership * parameters;
}
@property (retain) SecurityServiceSvc_UpdateMembership * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_UpdateMembership *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_DeleteUser : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_DeleteUser * parameters;
}
@property (retain) SecurityServiceSvc_DeleteUser * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_DeleteUser *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetUserByUserName : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetUserByUserName * parameters;
}
@property (retain) SecurityServiceSvc_GetUserByUserName * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetUserByUserName *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetUserbyUserId : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetUserbyUserId * parameters;
}
@property (retain) SecurityServiceSvc_GetUserbyUserId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetUserbyUserId *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetUserMembershipByUserId : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetUserMembershipByUserId * parameters;
}
@property (retain) SecurityServiceSvc_GetUserMembershipByUserId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetUserMembershipByUserId *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetUserMembershipByUserName : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetUserMembershipByUserName * parameters;
}
@property (retain) SecurityServiceSvc_GetUserMembershipByUserName * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetUserMembershipByUserName *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetAllUsers : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetAllUsers * parameters;
}
@property (retain) SecurityServiceSvc_GetAllUsers * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetAllUsers *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetUsersByRoleId : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetUsersByRoleId * parameters;
}
@property (retain) SecurityServiceSvc_GetUsersByRoleId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetUsersByRoleId *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetUsersByCompanyId : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetUsersByCompanyId * parameters;
}
@property (retain) SecurityServiceSvc_GetUsersByCompanyId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetUsersByCompanyId *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetUsersByGroupId : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetUsersByGroupId * parameters;
}
@property (retain) SecurityServiceSvc_GetUsersByGroupId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetUsersByGroupId *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetGroupsByRoleId : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetGroupsByRoleId * parameters;
}
@property (retain) SecurityServiceSvc_GetGroupsByRoleId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetGroupsByRoleId *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetGroupsByUserId : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetGroupsByUserId * parameters;
}
@property (retain) SecurityServiceSvc_GetGroupsByUserId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetGroupsByUserId *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetGroupsByCompanyId : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetGroupsByCompanyId * parameters;
}
@property (retain) SecurityServiceSvc_GetGroupsByCompanyId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetGroupsByCompanyId *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetRolesByGroupId : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetRolesByGroupId * parameters;
}
@property (retain) SecurityServiceSvc_GetRolesByGroupId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetRolesByGroupId *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetRolesByCompanyId : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetRolesByCompanyId * parameters;
}
@property (retain) SecurityServiceSvc_GetRolesByCompanyId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetRolesByCompanyId *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetRolesByUserId : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetRolesByUserId * parameters;
}
@property (retain) SecurityServiceSvc_GetRolesByUserId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetRolesByUserId *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetMembershipByUserId : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetMembershipByUserId * parameters;
}
@property (retain) SecurityServiceSvc_GetMembershipByUserId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetMembershipByUserId *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetEmployeeById : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetEmployeeById * parameters;
}
@property (retain) SecurityServiceSvc_GetEmployeeById * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetEmployeeById *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetUsersByCondition : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetUsersByCondition * parameters;
}
@property (retain) SecurityServiceSvc_GetUsersByCondition * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetUsersByCondition *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetUsersSuggestByCondition : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetUsersSuggestByCondition * parameters;
}
@property (retain) SecurityServiceSvc_GetUsersSuggestByCondition * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetUsersSuggestByCondition *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_CreateRole : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_CreateRole * parameters;
}
@property (retain) SecurityServiceSvc_CreateRole * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_CreateRole *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_UpdateRole : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_UpdateRole * parameters;
}
@property (retain) SecurityServiceSvc_UpdateRole * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_UpdateRole *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_DeleteRole : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_DeleteRole * parameters;
}
@property (retain) SecurityServiceSvc_DeleteRole * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_DeleteRole *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetRole : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetRole * parameters;
}
@property (retain) SecurityServiceSvc_GetRole * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetRole *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetRolesByName : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetRolesByName * parameters;
}
@property (retain) SecurityServiceSvc_GetRolesByName * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetRolesByName *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetGroupsByName : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetGroupsByName * parameters;
}
@property (retain) SecurityServiceSvc_GetGroupsByName * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetGroupsByName *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetAllRoles : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetAllRoles * parameters;
}
@property (retain) SecurityServiceSvc_GetAllRoles * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetAllRoles *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_CreateGroup : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_CreateGroup * parameters;
}
@property (retain) SecurityServiceSvc_CreateGroup * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_CreateGroup *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_UpdateGroup : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_UpdateGroup * parameters;
}
@property (retain) SecurityServiceSvc_UpdateGroup * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_UpdateGroup *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_DeleteGroup : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_DeleteGroup * parameters;
}
@property (retain) SecurityServiceSvc_DeleteGroup * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_DeleteGroup *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetGroup : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetGroup * parameters;
}
@property (retain) SecurityServiceSvc_GetGroup * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetGroup *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetAllGroups : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetAllGroups * parameters;
}
@property (retain) SecurityServiceSvc_GetAllGroups * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetAllGroups *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetAllCompanies : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetAllCompanies * parameters;
}
@property (retain) SecurityServiceSvc_GetAllCompanies * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetAllCompanies *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetAllApplications : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetAllApplications * parameters;
}
@property (retain) SecurityServiceSvc_GetAllApplications * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetAllApplications *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetApplicationById : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetApplicationById * parameters;
}
@property (retain) SecurityServiceSvc_GetApplicationById * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetApplicationById *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetAllModules : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetAllModules * parameters;
}
@property (retain) SecurityServiceSvc_GetAllModules * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetAllModules *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetModulesByAppId : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetModulesByAppId * parameters;
}
@property (retain) SecurityServiceSvc_GetModulesByAppId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetModulesByAppId *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetModuleById : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetModuleById * parameters;
}
@property (retain) SecurityServiceSvc_GetModuleById * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetModuleById *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetAllFeatures : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetAllFeatures * parameters;
}
@property (retain) SecurityServiceSvc_GetAllFeatures * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetAllFeatures *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetFeaturesByModuleId : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetFeaturesByModuleId * parameters;
}
@property (retain) SecurityServiceSvc_GetFeaturesByModuleId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetFeaturesByModuleId *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetFeatureById : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetFeatureById * parameters;
}
@property (retain) SecurityServiceSvc_GetFeatureById * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetFeatureById *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetAllActionsByFeatureId : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetAllActionsByFeatureId * parameters;
}
@property (retain) SecurityServiceSvc_GetAllActionsByFeatureId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetAllActionsByFeatureId *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetV_ActionByActionId : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetV_ActionByActionId * parameters;
}
@property (retain) SecurityServiceSvc_GetV_ActionByActionId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetV_ActionByActionId *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetAllV_ActionsByFeatureId : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetAllV_ActionsByFeatureId * parameters;
}
@property (retain) SecurityServiceSvc_GetAllV_ActionsByFeatureId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetAllV_ActionsByFeatureId *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetLinksByActionId : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetLinksByActionId * parameters;
}
@property (retain) SecurityServiceSvc_GetLinksByActionId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetLinksByActionId *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetAllActionTypes : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetAllActionTypes * parameters;
}
@property (retain) SecurityServiceSvc_GetAllActionTypes * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetAllActionTypes *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetActionTypeById : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetActionTypeById * parameters;
}
@property (retain) SecurityServiceSvc_GetActionTypeById * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetActionTypeById *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_AssignActionToRole : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_AssignActionToRole * parameters;
}
@property (retain) SecurityServiceSvc_AssignActionToRole * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_AssignActionToRole *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_AssignActionToUser : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_AssignActionToUser * parameters;
}
@property (retain) SecurityServiceSvc_AssignActionToUser * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_AssignActionToUser *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_AssignActionToGroup : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_AssignActionToGroup * parameters;
}
@property (retain) SecurityServiceSvc_AssignActionToGroup * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_AssignActionToGroup *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_RemoveActionFromRole : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_RemoveActionFromRole * parameters;
}
@property (retain) SecurityServiceSvc_RemoveActionFromRole * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_RemoveActionFromRole *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_RemoveActionFromUser : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_RemoveActionFromUser * parameters;
}
@property (retain) SecurityServiceSvc_RemoveActionFromUser * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_RemoveActionFromUser *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_RemoveActionFromGroupr : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_RemoveActionFromGroupr * parameters;
}
@property (retain) SecurityServiceSvc_RemoveActionFromGroupr * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_RemoveActionFromGroupr *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_AssignRoleToUser : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_AssignRoleToUser * parameters;
}
@property (retain) SecurityServiceSvc_AssignRoleToUser * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_AssignRoleToUser *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_RemoveRoleFromUser : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_RemoveRoleFromUser * parameters;
}
@property (retain) SecurityServiceSvc_RemoveRoleFromUser * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_RemoveRoleFromUser *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_AssignRoleToGroup : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_AssignRoleToGroup * parameters;
}
@property (retain) SecurityServiceSvc_AssignRoleToGroup * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_AssignRoleToGroup *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_RemoveRoleFromGroup : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_RemoveRoleFromGroup * parameters;
}
@property (retain) SecurityServiceSvc_RemoveRoleFromGroup * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_RemoveRoleFromGroup *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_AssignGroupToUser : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_AssignGroupToUser * parameters;
}
@property (retain) SecurityServiceSvc_AssignGroupToUser * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_AssignGroupToUser *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_RemoveGroupFromUser : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_RemoveGroupFromUser * parameters;
}
@property (retain) SecurityServiceSvc_RemoveGroupFromUser * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_RemoveGroupFromUser *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_ChangePassword : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_ChangePassword * parameters;
}
@property (retain) SecurityServiceSvc_ChangePassword * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_ChangePassword *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_SuccessLogin : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_SuccessLogin * parameters;
}
@property (retain) SecurityServiceSvc_SuccessLogin * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_SuccessLogin *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_CheckActiveUser : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_CheckActiveUser * parameters;
}
@property (retain) SecurityServiceSvc_CheckActiveUser * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_CheckActiveUser *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_AddSysUserLog : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_AddSysUserLog * parameters;
}
@property (retain) SecurityServiceSvc_AddSysUserLog * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_AddSysUserLog *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetAllUserPermissions : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetAllUserPermissions * parameters;
}
@property (retain) SecurityServiceSvc_GetAllUserPermissions * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetAllUserPermissions *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetAllRolePermissions : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetAllRolePermissions * parameters;
}
@property (retain) SecurityServiceSvc_GetAllRolePermissions * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetAllRolePermissions *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetAllGroupPermissions : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetAllGroupPermissions * parameters;
}
@property (retain) SecurityServiceSvc_GetAllGroupPermissions * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetAllGroupPermissions *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_HavePermission : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_HavePermission * parameters;
}
@property (retain) SecurityServiceSvc_HavePermission * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_HavePermission *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_CreateModule : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_CreateModule * parameters;
}
@property (retain) SecurityServiceSvc_CreateModule * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_CreateModule *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_UpdateModule : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_UpdateModule * parameters;
}
@property (retain) SecurityServiceSvc_UpdateModule * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_UpdateModule *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_DeleteModule : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_DeleteModule * parameters;
}
@property (retain) SecurityServiceSvc_DeleteModule * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_DeleteModule *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_CreateFeature : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_CreateFeature * parameters;
}
@property (retain) SecurityServiceSvc_CreateFeature * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_CreateFeature *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_UpdateFeature : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_UpdateFeature * parameters;
}
@property (retain) SecurityServiceSvc_UpdateFeature * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_UpdateFeature *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_DeleteFeature : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_DeleteFeature * parameters;
}
@property (retain) SecurityServiceSvc_DeleteFeature * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_DeleteFeature *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_CreateAction : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_CreateAction * parameters;
}
@property (retain) SecurityServiceSvc_CreateAction * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_CreateAction *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetActionByFeatureIdAndActionTypId : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetActionByFeatureIdAndActionTypId * parameters;
}
@property (retain) SecurityServiceSvc_GetActionByFeatureIdAndActionTypId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetActionByFeatureIdAndActionTypId *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_DeleteAction : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_DeleteAction * parameters;
}
@property (retain) SecurityServiceSvc_DeleteAction * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_DeleteAction *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_CreateLink : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_CreateLink * parameters;
}
@property (retain) SecurityServiceSvc_CreateLink * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_CreateLink *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_UpdateLink : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_UpdateLink * parameters;
}
@property (retain) SecurityServiceSvc_UpdateLink * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_UpdateLink *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_DeleteLink : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_DeleteLink * parameters;
}
@property (retain) SecurityServiceSvc_DeleteLink * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_DeleteLink *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_CheckUserIsInRole : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_CheckUserIsInRole * parameters;
}
@property (retain) SecurityServiceSvc_CheckUserIsInRole * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_CheckUserIsInRole *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_AssignUsersToRole : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_AssignUsersToRole * parameters;
}
@property (retain) SecurityServiceSvc_AssignUsersToRole * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_AssignUsersToRole *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_CheckGroupIsInRole : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_CheckGroupIsInRole * parameters;
}
@property (retain) SecurityServiceSvc_CheckGroupIsInRole * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_CheckGroupIsInRole *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_AssignGroupsToRole : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_AssignGroupsToRole * parameters;
}
@property (retain) SecurityServiceSvc_AssignGroupsToRole * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_AssignGroupsToRole *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_CheckUserIsInGroup : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_CheckUserIsInGroup * parameters;
}
@property (retain) SecurityServiceSvc_CheckUserIsInGroup * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_CheckUserIsInGroup *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_AssignUsersToGroup : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_AssignUsersToGroup * parameters;
}
@property (retain) SecurityServiceSvc_AssignUsersToGroup * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_AssignUsersToGroup *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetSelectedUsersById : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetSelectedUsersById * parameters;
}
@property (retain) SecurityServiceSvc_GetSelectedUsersById * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetSelectedUsersById *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetUserEmployeeByUserId : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetUserEmployeeByUserId * parameters;
}
@property (retain) SecurityServiceSvc_GetUserEmployeeByUserId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetUserEmployeeByUserId *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetUserMembershipByUserDomain : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetUserMembershipByUserDomain * parameters;
}
@property (retain) SecurityServiceSvc_GetUserMembershipByUserDomain * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetUserMembershipByUserDomain *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_AssignOrgUnitPermissionToUser : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_AssignOrgUnitPermissionToUser * parameters;
}
@property (retain) SecurityServiceSvc_AssignOrgUnitPermissionToUser * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_AssignOrgUnitPermissionToUser *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_RemoveOrgUnitPermissionFromUser : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_RemoveOrgUnitPermissionFromUser * parameters;
}
@property (retain) SecurityServiceSvc_RemoveOrgUnitPermissionFromUser * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_RemoveOrgUnitPermissionFromUser *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_AssignOrgUnitPermissionToRole : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_AssignOrgUnitPermissionToRole * parameters;
}
@property (retain) SecurityServiceSvc_AssignOrgUnitPermissionToRole * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_AssignOrgUnitPermissionToRole *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_RemoveOrgUnitPermissionFromRole : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_RemoveOrgUnitPermissionFromRole * parameters;
}
@property (retain) SecurityServiceSvc_RemoveOrgUnitPermissionFromRole * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_RemoveOrgUnitPermissionFromRole *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_AssignOrgUnitPermissionToGroup : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_AssignOrgUnitPermissionToGroup * parameters;
}
@property (retain) SecurityServiceSvc_AssignOrgUnitPermissionToGroup * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_AssignOrgUnitPermissionToGroup *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_RemoveOrgUnitPermissionFromGroup : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_RemoveOrgUnitPermissionFromGroup * parameters;
}
@property (retain) SecurityServiceSvc_RemoveOrgUnitPermissionFromGroup * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_RemoveOrgUnitPermissionFromGroup *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetAllUserOrgUnitPermissions : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetAllUserOrgUnitPermissions * parameters;
}
@property (retain) SecurityServiceSvc_GetAllUserOrgUnitPermissions * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetAllUserOrgUnitPermissions *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetAllRoleOrgUnitPermissions : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetAllRoleOrgUnitPermissions * parameters;
}
@property (retain) SecurityServiceSvc_GetAllRoleOrgUnitPermissions * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetAllRoleOrgUnitPermissions *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetAllGroupOrgUnitPermissions : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetAllGroupOrgUnitPermissions * parameters;
}
@property (retain) SecurityServiceSvc_GetAllGroupOrgUnitPermissions * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetAllGroupOrgUnitPermissions *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetAllOrgUnitByUser : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetAllOrgUnitByUser * parameters;
}
@property (retain) SecurityServiceSvc_GetAllOrgUnitByUser * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetAllOrgUnitByUser *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_CheckPermisionOnEmployee : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_CheckPermisionOnEmployee * parameters;
}
@property (retain) SecurityServiceSvc_CheckPermisionOnEmployee * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_CheckPermisionOnEmployee *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetAllSysEmpProfileGroupLayer : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetAllSysEmpProfileGroupLayer * parameters;
}
@property (retain) SecurityServiceSvc_GetAllSysEmpProfileGroupLayer * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetAllSysEmpProfileGroupLayer *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetSysEmpProfileLayersByGroupId : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetSysEmpProfileLayersByGroupId * parameters;
}
@property (retain) SecurityServiceSvc_GetSysEmpProfileLayersByGroupId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetSysEmpProfileLayersByGroupId *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetSysEmpProfileLayerUrlsByLayerId : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetSysEmpProfileLayerUrlsByLayerId * parameters;
}
@property (retain) SecurityServiceSvc_GetSysEmpProfileLayerUrlsByLayerId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetSysEmpProfileLayerUrlsByLayerId *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetSysEmpProfileLayerUrlsByTypeByLayerId : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetSysEmpProfileLayerUrlsByTypeByLayerId * parameters;
}
@property (retain) SecurityServiceSvc_GetSysEmpProfileLayerUrlsByTypeByLayerId * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetSysEmpProfileLayerUrlsByTypeByLayerId *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetAllRoleEmpProfilePermission : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetAllRoleEmpProfilePermission * parameters;
}
@property (retain) SecurityServiceSvc_GetAllRoleEmpProfilePermission * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetAllRoleEmpProfilePermission *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetAllUserEmpProfilePermission : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetAllUserEmpProfilePermission * parameters;
}
@property (retain) SecurityServiceSvc_GetAllUserEmpProfilePermission * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetAllUserEmpProfilePermission *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetAllGroupEmpProfilePermission : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetAllGroupEmpProfilePermission * parameters;
}
@property (retain) SecurityServiceSvc_GetAllGroupEmpProfilePermission * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetAllGroupEmpProfilePermission *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_AssignEmpProfileLayerPermissonToUser : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_AssignEmpProfileLayerPermissonToUser * parameters;
}
@property (retain) SecurityServiceSvc_AssignEmpProfileLayerPermissonToUser * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_AssignEmpProfileLayerPermissonToUser *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_AssignEmpProfileLayerPermissonToRole : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_AssignEmpProfileLayerPermissonToRole * parameters;
}
@property (retain) SecurityServiceSvc_AssignEmpProfileLayerPermissonToRole * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_AssignEmpProfileLayerPermissonToRole *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_AssignEmpProfileLayerPermissonToGroup : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_AssignEmpProfileLayerPermissonToGroup * parameters;
}
@property (retain) SecurityServiceSvc_AssignEmpProfileLayerPermissonToGroup * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_AssignEmpProfileLayerPermissonToGroup *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_RemoveEmpProfileLayerPermissonFromUser : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromUser * parameters;
}
@property (retain) SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromUser * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromUser *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_RemoveEmpProfileLayerPermissonFromGroup : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromGroup * parameters;
}
@property (retain) SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromGroup * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromGroup *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_RemoveEmpProfileLayerPermissonFromRole : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromRole * parameters;
}
@property (retain) SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromRole * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_RemoveEmpProfileLayerPermissonFromRole *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetAllLayerOfAuthorizedSubAndProfile : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetAllLayerOfAuthorizedSubAndProfile * parameters;
}
@property (retain) SecurityServiceSvc_GetAllLayerOfAuthorizedSubAndProfile * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetAllLayerOfAuthorizedSubAndProfile *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetLayerAndPermissOfSubAndProfile : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetLayerAndPermissOfSubAndProfile * parameters;
}
@property (retain) SecurityServiceSvc_GetLayerAndPermissOfSubAndProfile * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetLayerAndPermissOfSubAndProfile *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetAllLayerOfAuthorizedIncludedSubAndProfileForTree : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetAllLayerOfAuthorizedIncludedSubAndProfileForTree * parameters;
}
@property (retain) SecurityServiceSvc_GetAllLayerOfAuthorizedIncludedSubAndProfileForTree * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetAllLayerOfAuthorizedIncludedSubAndProfileForTree *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_GetOnePermissOfUserOnProfileLayer : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_GetOnePermissOfUserOnProfileLayer * parameters;
}
@property (retain) SecurityServiceSvc_GetOnePermissOfUserOnProfileLayer * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_GetOnePermissOfUserOnProfileLayer *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_AssignProfileLayerPermissonToUserGroupRole : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_AssignProfileLayerPermissonToUserGroupRole * parameters;
}
@property (retain) SecurityServiceSvc_AssignProfileLayerPermissonToUserGroupRole * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_AssignProfileLayerPermissonToUserGroupRole *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_RemoveProfileLayerPermissonFromUserGroupRole : BasicHttpBinding_ISecurityServiceBindingOperation {
	SecurityServiceSvc_RemoveProfileLayerPermissonFromUserGroupRole * parameters;
}
@property (retain) SecurityServiceSvc_RemoveProfileLayerPermissonFromUserGroupRole * parameters;
- (id)initWithBinding:(BasicHttpBinding_ISecurityServiceBinding *)aBinding delegate:(id<BasicHttpBinding_ISecurityServiceBindingResponseDelegate>)aDelegate
	parameters:(SecurityServiceSvc_RemoveProfileLayerPermissonFromUserGroupRole *)aParameters
;
@end
@interface BasicHttpBinding_ISecurityServiceBinding_envelope : NSObject {
}
+ (BasicHttpBinding_ISecurityServiceBinding_envelope *)sharedInstance;
- (NSString *)serializedFormUsingHeaderElements:(NSDictionary *)headerElements bodyElements:(NSDictionary *)bodyElements;
@end
@interface BasicHttpBinding_ISecurityServiceBindingResponse : NSObject {
	NSArray *headers;
	NSArray *bodyParts;
	NSError *error;
}
@property (retain) NSArray *headers;
@property (retain) NSArray *bodyParts;
@property (retain) NSError *error;
@end
