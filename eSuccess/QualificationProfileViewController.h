//
//  QualificationProfileViewController.h
//  eSuccess
//
//  Created by HPTVIETNAM on 6/26/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"
#import "EmpProfileLayerServiceSvc.h"
@interface QualificationProfileViewController : BaseDetailTableViewController<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>
@property(nonatomic, strong) NSMutableArray *qualificationArr;
@property(nonatomic, strong) NSMutableArray *qualiNameArr;

@end
