//
//  HistoryProfileViewController.h
//  eSuccess
//
//  Created by admin on 5/4/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "EmpProfileLayerServiceSvc.h"
#import "BaseDetailTableViewController.h"
@interface HistoryProfileViewController : BaseDetailTableViewController<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>
@property(nonatomic, retain) NSMutableArray *profileArray;
@property(nonatomic, retain) NSMutableArray *timeArray;
@end
