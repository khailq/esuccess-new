//
//  HealthStatusViewController.m
//  eSuccess
//
//  Created by admin on 5/4/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "HealthStatusViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "DayLeaveDetailCell.h"
#import "MedicalHistoryCell.h"
@interface HealthStatusViewController ()

@end

@implementation HealthStatusViewController
@synthesize medicalArr;
static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";
static NSString *medicalHistoryIdentifier = @"MedicalHistoryCell";
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Hồ sơ sức khoẻ";
    [self getData];
	// Do any additional setup after loading the view.
    
    UINib *nib = [UINib nibWithNibName:@"MedicalHistoryCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:medicalHistoryIdentifier];
    
    nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (int)setStatus
{
    
    return 0;
}

- (void)HealthyStatus:(DayLeaveDetailCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        cell.lbLeft.text = @"Khuyết tật";
        
    }
    if (indexPath.row == 1) {
        cell.lbLeft.text = @"Đủ sức khoẻ";
        
    }
}

- (void)MaternityStatus:(DayLeaveDetailCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MM/yyyy"];
    
    if (indexPath.row == 0) {
        cell.lbLeft.text = @"Ngày bắt đầu";
        
    }
    if (indexPath.row == 1) {
        cell.lbLeft.text = @"Ngày kết thúc";
        
    }
}

- (void)DiseaseStatus:(DayLeaveDetailCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    if (indexPath.row == 0) {
        cell.lbLeft.text = @"Tên bệnh";
    }
    if (indexPath.row == 1) {
        cell.lbLeft.text = @"Thời gian";
        
    }
    if (indexPath.row == 2) {
        cell.lbLeft.text = @"Trợ cấp";
    }
}

- (void)MedicalHistoryStatus:(MedicalHistoryCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    if (indexPath.row == 0) {
        cell.lblDiseaseName.text = @"Tên bệnh";
        cell.lblTime.text = @"Thời gian";
        cell.lblBenefit.text = @"Phụ cấp";
    } else{
        
    }
}

#pragma mark - Table view data source
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 130;
//}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    int i = [self setStatus];
    if (i == 1) {
        if (section == 0) {
            return @"Sức khoẻ hiện tại";
        }
        if (section == 1) {
            if ([medicalArr count] > 0) {
                return @"Tiền sử bệnh";
            } else
                return nil;
        }
    } else if( i == 2){
        if (section == 0) {
            return @"Sức khoẻ hiện tại";
        }
        if (section == 1) {
            return @"Giai đoạn dưỡng bệnh";
        } if (section == 2) {
            if ([medicalArr count] > 0) {
                return @"Tiền sử bệnh";
            } else
                return nil;
        }
    } else if(i == 3){
        if (section == 0) {
            return @"Sức khoẻ hiện tại";
        }
        if (section == 1) {
            return @"Giai đoạn thai sản";
        } if (section == 2) {
            if ([medicalArr count] > 0) {
                return @"Tiền sử bệnh";
            } else
                return nil;
        }
    } else {
        if (section == 0) {
            return @"Sức khoẻ hiện tại";
        }
        if (section == 1) {
            return @"Giai đoạn thai sản";
        } if (section == 2) {
            return @"Giai đoạn dưỡng bệnh";
        } if (section == 3) {
            if ([medicalArr count] > 0) {
                return @"Tiền sử bệnh";
            } else
                return nil;
        }
    }
    return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    int i = [self setStatus];
    if (i == 1) {
        return 2;
    } else if(i == 4){
        return 4;
    }
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    int i = [self setStatus];
    if (i == 1) {
        if (section == 0) {
            return 2;
        }
        if (section == 1) {
            return [medicalArr count];
        }
    } else if( i == 2){
        if (section == 0) {
            return 2;
        }
        if (section == 1) {
            return 3;
        } if (section == 2) {
            return [medicalArr count ];
        }
    } else if(i == 3){
        if (section == 0) {
            return 2;
        }
        if (section == 1) {
            return 2;
        } if (section == 2) {
            return [medicalArr count];
        }
    } else {
        if (section == 0) {
            return 2;
        }
        if (section == 1) {
            return 2;
        } if (section == 2) {
            return 3;
        } if (section == 3) {
            return [medicalArr count];
        }
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int section = indexPath.section;
    
    DayLeaveDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
    if (cell == nil) {
        cell = [[DayLeaveDetailCell alloc]init];
    }
    
    cell.lbLeft.font = [UIFont systemFontOfSize:14];
    cell.lbLeft.backgroundColor = [UIColor clearColor];
    cell.lbRight.font = [UIFont boldSystemFontOfSize:16];
    cell.lbRight.backgroundColor = [UIColor clearColor];
    
    
    int i = [self setStatus];
    if (i == 0) {
        [self getData];
    }
    if (i == 1) {
        if (section == 0) {
            //return @"Sức khoẻ hiện tại";
            [self HealthyStatus:cell cellForRowAtIndexPath:indexPath];
        }
        if (section == 1) {
             //return @"Tiền sử bệnh";
            MedicalHistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:medicalHistoryIdentifier];
            [self MedicalHistoryStatus:cell cellForRowAtIndexPath:indexPath];
        }
    } else if( i == 2){
        if (section == 0) {
            //return @"Sức khoẻ hiện tại";
            [self HealthyStatus:cell cellForRowAtIndexPath:indexPath];
        }
        if (section == 1) {
            //return @"Giai đoạn dưỡng bệnh";
            [self DiseaseStatus:cell cellForRowAtIndexPath:indexPath];
        } if (section == 2) {
            //return @"Tiền sử bệnh";
            MedicalHistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:medicalHistoryIdentifier];
            [self MedicalHistoryStatus:cell cellForRowAtIndexPath:indexPath];
        }
    } else if(i == 3){
        if (section == 0) {
            //return @"Sức khoẻ hiện tại";
            [self HealthyStatus:cell cellForRowAtIndexPath:indexPath];
        }
        if (section == 1) {
            //return @"Giai đoạn thai sản";
            [self MaternityStatus:cell cellForRowAtIndexPath:indexPath];
            return cell;
        } if (section == 2) {
            //return @"Tiền sử bệnh";
            MedicalHistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:medicalHistoryIdentifier];
            [self MedicalHistoryStatus:cell cellForRowAtIndexPath:indexPath];
        }
    } else {
        if (section == 0) {
            //return @"Sức khoẻ hiện tại";
            [self HealthyStatus:cell cellForRowAtIndexPath:indexPath];
        }
        if (section == 1) {
            //return @"Giai đoạn thai sản";
            [self MaternityStatus:cell cellForRowAtIndexPath:indexPath];
        } if (section == 2) {
            //return @"Giai đoạn dưỡng bệnh";
            [self DiseaseStatus:cell cellForRowAtIndexPath:indexPath];
        } if (section == 3) {
            //return @"Tiền sử bệnh";
            MedicalHistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:medicalHistoryIdentifier];
            [self MedicalHistoryStatus:cell cellForRowAtIndexPath:indexPath];
        }
    }
    
    return cell;
}

#pragma get Data Profile
- (void)getData
{
    
}

@end
