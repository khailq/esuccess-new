//
//  ForeginLanguageViewController.h
//  eSuccess
//
//  Created by admin on 5/7/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"

@interface ForeignLanguageViewController : BaseDetailTableViewController
@property(nonatomic, retain)NSMutableArray *languageArr;
@property(nonatomic, retain)NSMutableArray *langNameArr;
@end
