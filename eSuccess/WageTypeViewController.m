//
//  WageTypeViewController.m
//  eSuccess
//
//  Created by admin on 6/15/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "WageTypeViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "DayLeaveDetailCell.h"
@interface WageTypeViewController ()

@end

@implementation WageTypeViewController
@synthesize wageTypeArr;
@synthesize typeNameArr;
static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Loại công";
    [self getData];
    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 130;
//}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([typeNameArr count] > 0) {
        return [typeNameArr objectAtIndex:section];
    }
    return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [wageTypeArr count] > 0 ? wageTypeArr.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //return [profileArray count];
    return [wageTypeArr count] > 0? 3:1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (wageTypeArr.count == 0)
        return [self getDefaultEmptyCell];
    
    DayLeaveDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
    if (cell == nil) {
        cell = [[DayLeaveDetailCell alloc]init];
    }
    cell.lbLeft.font = [UIFont systemFontOfSize:14];
    cell.lbLeft.backgroundColor = [UIColor clearColor];
    cell.lbRight.font = [UIFont boldSystemFontOfSize:16];
    cell.lbRight.backgroundColor = [UIColor clearColor];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    int section = indexPath.section;
    empl_tns1_V_EmpProfileWageType *wageType = [wageTypeArr objectAtIndex:section];
    if (indexPath.row == 0) {
        cell.lbLeft.text = @"Ngày bắt đầu";
        cell.lbRight.text = [formatter stringFromDate:wageType.ImplementationDate];
    }
    if (indexPath.row == 1) {
        cell.lbLeft.text = @"Số ngày";
        cell.lbRight.text = [NSString stringWithFormat:@"%d",[wageType.WorkingDay integerValue]];
    }
    if (indexPath.row == 2) {
        cell.lbLeft.text = @"Ghi chú";
        cell.lbRight.text = wageType.Note;
    }
    return cell;
}

#pragma getData
- (void)getData
{
    wageTypeArr = [[NSMutableArray alloc]init];
    typeNameArr = [[NSMutableArray alloc]init];
    
    [self getEmpProfileWageTyeById];
}

- (void)getEmpProfileWageTyeById
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Đang cập nhật";
    
    BasicHttpBinding_IEmpProfileLayerServiceBinding *binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
    EmpProfileLayerServiceSvc_GetViewEmpProfileWageTypeById *request = [[EmpProfileLayerServiceSvc_GetViewEmpProfileWageTypeById alloc] init];
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    if (app.otherEmpId != nil)
        request.employeeId = app.otherEmpId;
    else
        request.employeeId = app.sysUser.EmployeeId;
    [binding GetViewEmpProfileWageTypeByIdAsyncUsingParameters:request delegate:self];
}

- (void)operation:(BasicHttpBinding_IEmpProfileLayerServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)response
{
    if ([response.bodyParts count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi kết nối" message:@"Không thể kết nối đến server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    for (id mine in response.bodyParts){
        if ([mine isKindOfClass:[EmpProfileLayerServiceSvc_GetViewEmpProfileWageTypeByIdResponse class]]) {
            empl_tns1_ArrayOfV_EmpProfileWageType *result = [mine GetViewEmpProfileWageTypeByIdResult];
            wageTypeArr = result.V_EmpProfileWageType;
            for (empl_tns1_V_EmpProfileWageType  *wageType in wageTypeArr) {
                [typeNameArr addObject:wageType.NameVN];
            }
        }
        //        if ([mine isKindOfClass:[SOAPFault class]])
        //        {
        //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Server error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //            [alert show];
        //        }
    }
    
    if (!self.viewDidDisappear)
        [self.tableView reloadData];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

@end
