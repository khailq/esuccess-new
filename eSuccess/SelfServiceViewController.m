//
//  SelfServiceViewController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 5/25/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "SelfServiceViewController.h"
@interface SelfServiceViewController ()

@end

@implementation SelfServiceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSString *fileName = SelfServiceName;
    
    self.title = @"Self Service";
    self.name = fileName;
    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
