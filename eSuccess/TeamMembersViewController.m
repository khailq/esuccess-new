//
//  TeamMembersViewController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 5/28/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "TeamMembersViewController.h"
#import "TeamMemberCell.h"
#import "AppDelegate.h"
#import "UIImageView+AFNetworking.h"

static NSString *CellIdentifier = @"TeamMemberCell";
@interface TeamMembersViewController ()
{
    NSMutableArray *members;
}
@end

@implementation TeamMembersViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self.tableView registerClass:[TeamMemberCell class] forCellReuseIdentifier:CellIdentifier];
    //[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:CellIdentifier];
    
    [self getData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getData
{
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return members.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

        TeamMemberCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            return cell;

}

#pragma mark - Table view delegate

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

@end
