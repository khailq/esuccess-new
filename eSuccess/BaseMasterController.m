    //
//  BaseMasterController.m
//  eSuccess
//
//  Created by Scott on 5/2/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseMasterController.h"
#import "CustomSplitViewController.h"
#import "SearchUsersViewController.h"
#import "MenuItem.h"

static NSString *searchStoryboard = @"SearchController";
@interface BaseMasterController ()
{
    NSDictionary *mainDictionary;
    NSArray *mainArray;
    UIView *myView;
    NSArray *menus;
}
@end

@implementation BaseMasterController

-(id)init
{
    self = [super init];
    if (self)
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedMasterNotification:) name:MasterChangedNotification object:nil];
    
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.clearsSelectionOnViewWillAppear = NO;
    //self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    self.navigationController.navigationBar.translucent = NO;
    
    if (self.name != nil)
    {
        NSString *fileName = [[NSBundle mainBundle] pathForResource:self.name ofType:@"plist"];
        if (fileName != nil)
        {
            @try {
                //mainDictionary = [NSDictionary dictionaryWithContentsOfFile:fileName];
                mainArray = [NSArray arrayWithContentsOfFile:fileName];
                NSMutableArray *tmpArr = [NSMutableArray array];
                for (NSDictionary *dict in mainArray)
                {
                    MenuItem *item = [MenuItem parseMenuItem:dict];
                    [tmpArr addObject:item];
                }
                
                menus = [tmpArr sortedArrayUsingSelector:@selector(compare:)];
            }
            @catch (NSException *exception) {
                NSLog(@"baseMaster - nib awake. Reason: %@", exception.reason);
            }
        }
    }
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:NormalCellIdentifier];

    UIBarButtonItem* right = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(onSearchButtonClicked:)];
    self.navigationItem.leftBarButtonItem = right;
    
    CGRect tableRect = self.tableView.frame;
    UIView *emptyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableRect.size.width, 100)];

    self.tableView.tableFooterView = emptyView;
}

-(void)onSearchButtonClicked:(id)sender
{
    SearchUsersViewController *searchController = [self.storyboard instantiateViewControllerWithIdentifier:searchStoryboard];
    if (searchController != nil)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedMasterNotification:) name:MasterChangedNotification object:nil];
        [self.navigationController pushViewController:searchController animated:YES];
    }
}

-(void)selectFirstRow
{
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView selectRowAtIndexPath:index animated:NO scrollPosition:UITableViewScrollPositionNone];
}

- (void)performSegue:(NSString *)segueId
{
    @try {
        [self performSegueWithIdentifier:segueId sender:self];
    }
    @catch (NSException *exception) {
        NSLog(@"Segue %@ not found", segueId);
    }
}

-(void)receivedMasterNotification:(NSNotification*)notification
{    
    if ([notification.name isEqualToString:MasterChangedNotification])
    {
        AppDelegate *app = [UIApplication sharedApplication].delegate;
        NSIndexPath *selectedIndex = [app.selectedIndexDictionary valueForKey:NSStringFromClass([self class])];
        if (selectedIndex == nil)
        {
            [self selectFirstRow];
            NSString *segueId = [NSString stringWithFormat:@"%d%d",0, 0];
            [self performSegue:segueId];
        }
        else
        {
            NSString *segueId = [NSString stringWithFormat:@"%d%d",selectedIndex.section, selectedIndex.row];
            [self performSegue:segueId];
        }
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:notification.name object:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - table view data source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (menus != nil)
        return menus.count;
    
    return [super numberOfSectionsInTableView:tableView];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (menus != nil)
    {
        MenuItem *menu = menus[section];
        return menu.items.count;
    }
    
    return [super tableView:tableView numberOfRowsInSection:section];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (menus != nil)
    {
        MenuItem *menu = menus[section];
        return menu.title;
    }
    
    
    return [super tableView:tableView titleForHeaderInSection:section];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (menus != nil)
    {
        MenuItem *menu = menus[indexPath.section];
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NormalCellIdentifier];
        cell.textLabel.text = menu.items[indexPath.row];
        
        return cell;
    }
    
    return [super tableView:tableView cellForRowAtIndexPath:indexPath];
}

-(BOOL)performSegueId:(NSString *)segueId
{
    @try {
        [self performSegueWithIdentifier:segueId sender:self];
        return true;
    }
    @catch (NSException *exception)
    {
        return false;
    }
}

#pragma mark - table view delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *key = NSStringFromClass([self class]);
    NSIndexPath *selectedIndex = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
    
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    [app.selectedIndexDictionary setValue:selectedIndex forKey:key];
    
    CustomSplitViewController *splitVC = (CustomSplitViewController*)app.window.rootViewController;
    if (!splitVC.isLandscape && splitVC.isShowingMaster)
    {
        [splitVC toggleMasterView:self];
    }
    
    NSString *identifier;
    identifier = [NSString stringWithFormat:@"%d%d", indexPath.section, indexPath.row]
    ;
    if (mainDictionary != nil || mainArray != nil)
    {
        if (![self performSegueId:identifier])
        {
            identifier = [identifier stringByReplacingOccurrencesOfString:@"m-" withString:@""];
            [self performSegueId:identifier];
        }
    }
//    else
//        [super tableView:tableView didSelectRowAtIndexPath:indexPath];
}

@end
