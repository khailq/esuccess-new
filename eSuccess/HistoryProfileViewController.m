//
//  HistoryProfileViewController.m
//  eSuccess
//
//  Created by admin on 5/4/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "HistoryProfileViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "DayLeaveDetailCell.h"
@interface HistoryProfileViewController ()

@end

@implementation HistoryProfileViewController
@synthesize profileArray;
@synthesize timeArray;
static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.title = @"Lịch sử bản thân";
    [self getData];
    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([timeArray count] > 0) {
        return [timeArray objectAtIndex:section];
    }
    return nil;
}
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 130;
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [timeArray count] > 0 ? timeArray.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //return [profileArray count];
    if([timeArray count] > 0){
        return 3;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (timeArray.count == 0)
        return [self getDefaultEmptyCell];
    
    DayLeaveDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
    if (cell == nil) {
        cell = [[DayLeaveDetailCell alloc]init];
    }
    cell.lbLeft.font = [UIFont systemFontOfSize:14];
    cell.lbLeft.backgroundColor = [UIColor clearColor];
    cell.lbRight.font = [UIFont boldSystemFontOfSize:16];
    cell.lbRight.backgroundColor = [UIColor clearColor];
    int section = indexPath.section;
    empl_tns1_EmpProfileBiography *biography = profileArray[section];
    if (indexPath.row == 0) {
        cell.lbLeft.text = @"Nghề nghiệp";
        cell.lbRight.text = biography.JobName;
    }
    if (indexPath.row == 1) {
        
        cell.lbLeft.text = @"Nơi ở";
        cell.lbRight.text = biography.PlaceIn;
    }
    if (indexPath.row == 2) {
        
        cell.lbLeft.text = @"Ghi chú";
        cell.lbRight.text = biography.Other;
    }
    return cell;
}

#pragma mark - Table view delegate


#pragma get Data Profile
- (void)getData
{
    profileArray = [[NSMutableArray alloc]init];
    timeArray = [[NSMutableArray alloc]init];
    
    [self getEmpWorkingExperienceById];
}

- (void)getEmpWorkingExperienceById
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Đang cập nhật";
    BasicHttpBinding_IEmpProfileLayerServiceBinding *binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
    EmpProfileLayerServiceSvc_GetViewEmpProfileBiographyById *request = [[EmpProfileLayerServiceSvc_GetViewEmpProfileBiographyById alloc] init];
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    if (app.otherEmpId != nil)
        request.employeeId = app.otherEmpId;
    else
        request.employeeId = app.sysUser.EmployeeId ;
    //request.EmployeeId = [NSNumber numberWithInt:2];
    [binding GetViewEmpProfileBiographyByIdAsyncUsingParameters:request delegate:self];
}

- (void)operation:(BasicHttpBinding_IEmpProfileLayerServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)response
{
    if ([response.bodyParts count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi kết nối" message:@"Không thể kết nối đến server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    for (id mine in response.bodyParts){
        if ([mine isKindOfClass:[EmpProfileLayerServiceSvc_GetViewEmpProfileBiographyByIdResponse class]]) {
            empl_tns1_ArrayOfEmpProfileBiography *result = [mine GetViewEmpProfileBiographyByIdResult];
            profileArray = result.EmpProfileBiography;
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"dd/MM/yyyy"];
            for (empl_tns1_EmpProfileBiography *profile in profileArray) {
                if (profile.StartDate == nil && profile.EndDate == nil) {
                    [timeArray addObject:@""];
                } else
                    [timeArray addObject:[NSString stringWithFormat:@"%@ - %@", [formatter stringFromDate:profile.StartDate],[formatter stringFromDate:profile.EndDate]]];
            }
        }
        //        if ([mine isKindOfClass:[SOAPFault class]])
        //        {
        //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Server error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //            [alert show];
        //        }
    }
    
    if (!self.viewDidDisappear)
        [self.tableView reloadData];
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

@end
