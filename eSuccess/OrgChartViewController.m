//
//  OrgChartViewController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 8/12/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "OrgChartViewController.h"
#import "Org Service/OrganizationServiceSvc.h"
#import "ProfilePanelView.h"
#import "OrgScrollView.h"
#import "NSString+FontAwesome.h"
#import "FontAwesomeKit.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "HomeDetailViewController.h"


#define TopMargin 30
#define RightMargin 30

@interface OrgChartViewController ()<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate, ProfilePanelProtocol>
{
    NSNumber *targetId;
    __weak ProfilePanelView *selectedPanel;
    UIBarButtonItem *btnUp;
    UIBarButtonItem *btnDown;
    UIBarButtonItem *btnViewProfile;
}
@end

@implementation OrgChartViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    btnViewProfile = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(onViewProfileClicked:)];
    
    btnUp = [[UIBarButtonItem alloc] initWithTitle:[NSString fontAwesomeIconStringForEnum:FAIconArrowUp] style:UIBarButtonItemStyleBordered target:self action:@selector(onViewUpperLevelClicked:)];
    [btnUp setTitleTextAttributes:@{UITextAttributeFont:[FontAwesomeKit fontWithSize:20]}
                                                          forState:UIControlStateNormal];
    btnDown = [[UIBarButtonItem alloc] initWithTitle:[NSString fontAwesomeIconStringForEnum:FAIconArrowDown] style:UIBarButtonItemStyleBordered target:self action:@selector(onViewLowerLevelClicked:)];
    [btnDown setTitleTextAttributes:@{UITextAttributeFont:[FontAwesomeKit fontWithSize:20]}
                         forState:UIControlStateNormal];
    
    [btnViewProfile setEnabled:NO];
    [btnUp setEnabled:NO];
    [btnDown setEnabled:NO];
    self.navigationItem.rightBarButtonItems = @[btnViewProfile, btnDown, btnUp];
    
    self.title = @"Cây tổ chức";
    
//    [self reloadOrgTree:[self createTestData]];
//    targetId = [NSNumber numberWithInt:1];
    
    AppDelegate *app = [UIApplication sharedApplication].delegate;
//    [self getDirectReportOfEmployeeId:app.sysUser.EmployeeId];
    [self getDirectReportOfEmployeeId:[NSNumber numberWithInt:1]];
}

-(void)onViewProfileClicked:(id)sender
{
    if (selectedPanel == nil)
        return;
    
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    app.otherEmpId = ((org_tns2_V_ESS_ORG_DIRECT_REPORT*)selectedPanel.dataContext).ChildId;
    HomeDetailViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeDetailViewController"];
    [self.navigationController pushViewController:home animated:YES];
    
}

-(void)onViewUpperLevelClicked:(id)sender
{
    if (selectedPanel != nil)
    {
        org_tns2_V_ESS_ORG_DIRECT_REPORT *reporter = selectedPanel.dataContext;
        [self getDirectReportOfEmployeeId:reporter.ParentId];
    }
}

-(void)onViewLowerLevelClicked:(id)sender
{
    if (selectedPanel != nil)
    {
        org_tns2_V_ESS_ORG_DIRECT_REPORT *reporter = selectedPanel.dataContext;
        [self getDirectReportOfEmployeeId:reporter.ChildId];
    }
}

-(void)reloadOrgTree:(NSArray *)array
{
    for(id view in self.view.subviews)
    {
        if ([view isKindOfClass:[UIScrollView class]])
        {
            [view removeFromSuperview];
            break;
        }
    }
    
    CGFloat totalWidth = array.count * (H_PanelWidth + RightMargin);
    totalWidth = totalWidth > 768 ? totalWidth : 768;
    OrgScrollView *panelView = [[OrgScrollView alloc] initWithDirectReportList:array frame:CGRectMake(0, 0, totalWidth, self.view.frame.size.height) parent:self];
    
    panelView.backgroundColor = [UIColor whiteColor];
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    [scrollView addSubview:panelView];
    scrollView.contentSize = CGSizeMake(totalWidth, self.view.frame.size.height);
    
    [self.view addSubview:scrollView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getDirectReportOfEmployeeId:(NSNumber *)empId
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    BasicHttpBinding_IOrganizationServiceBinding *binding = [OrganizationServiceSvc BasicHttpBinding_IOrganizationServiceBinding];
    OrganizationServiceSvc_GetDirecReportById *request = [OrganizationServiceSvc_GetDirecReportById new];
//    request.parentId = [NSNumber numberWithInt:1];
//    targetId = [NSNumber numberWithInt:1];
    request.parentId = empId;
    targetId = empId;
    
    [binding GetDirecReportByIdAsyncUsingParameters:request delegate:self];
}

-(void)operation:(BasicHttpBinding_IOrganizationServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IOrganizationServiceBindingResponse *)response
{
    if (self == nil || self.viewDidDisappear)
        return;
    
    for (id mine in response.bodyParts)
    {
        if ([mine isKindOfClass:[OrganizationServiceSvc_GetDirecReportByIdResponse class]])
        {
            org_tns2_ArrayOfV_ESS_ORG_DIRECT_REPORT *result = [mine GetDirecReportByIdResult];
            NSMutableArray *array = [NSMutableArray array];
            for (org_tns2_V_ESS_ORG_DIRECT_REPORT *reporter in result.V_ESS_ORG_DIRECT_REPORT)
            {
                if ([reporter.ChildId isEqualToNumber:targetId])
                    [array insertObject:reporter atIndex:0];
                else
                    [array addObject:reporter];
            }
            
            if (array.count <= 1)
            {
                [btnDown setEnabled:NO];
            }
            else
                [btnDown setEnabled:YES];
            
            [self reloadOrgTree:array];
        }
        if ([mine isKindOfClass:[SOAPFault class]])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi đường truyền" message:@"Không lấy được dữ liệu" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    }
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

-(void)touchInPanel:(ProfilePanelView *)panel
{
    if (selectedPanel != panel)
    {
        selectedPanel.selected = NO;
        panel.selected = YES;
        selectedPanel = panel;
        
        org_tns2_V_ESS_ORG_DIRECT_REPORT *reporter = panel.dataContext;
        if (reporter.ParentId == nil || [reporter.ParentId isEqualToNumber:targetId] || reporter.ParentId.intValue == 0)
            [btnUp setEnabled:NO];
        else
            [btnUp setEnabled:YES];
        
        if ([reporter.ChildId isEqualToNumber:targetId])
            [btnDown setEnabled:NO];
        else
            [btnDown setEnabled:YES];
        
        [btnViewProfile setEnabled:YES];
    }
}

-(NSArray *)createTestData
{
    NSString *unitName = @"HPT Việt Nam";
    NSString *positionName = @"Staff";
    
    org_tns2_V_ESS_ORG_DIRECT_REPORT *first = [org_tns2_V_ESS_ORG_DIRECT_REPORT new];
    first.ChildId = [NSNumber numberWithInt:1];
    first.ChildName = @"Hai Le Thanh";
    first.ChildUnitName = unitName;
    first.ChildJobPositionName = positionName;
    first.ImageUrl = @"70.jpg";
    
    org_tns2_V_ESS_ORG_DIRECT_REPORT *sec = [org_tns2_V_ESS_ORG_DIRECT_REPORT new];
    sec.ChildId = [NSNumber numberWithInt:4];
    sec.ChildName = @"Hoang Minh An";
    sec.ChildUnitName = unitName;
    sec.ChildJobPositionName = positionName;
    sec.ImageUrl = @"AnHM.jpg";
    sec.ParentId = [NSNumber numberWithInt:1];
    sec.ParentName = @"Hai Le Thanh";
    
    org_tns2_V_ESS_ORG_DIRECT_REPORT *third = [org_tns2_V_ESS_ORG_DIRECT_REPORT new];
    third.ChildId = [NSNumber numberWithInt:9];
    third.ChildName = @"Pham Minh Chien";
    third.ChildUnitName = unitName;
    third.ChildJobPositionName = positionName;
    third.ImageUrl = @"DinhLB.jpg";
    third.ParentId = [NSNumber numberWithInt:1];
    third.ParentName = @"Hai Le Thanh";
     
    org_tns2_V_ESS_ORG_DIRECT_REPORT *forth = [org_tns2_V_ESS_ORG_DIRECT_REPORT new];
    forth.ChildId = [NSNumber numberWithInt:14];
    forth.ChildName = @"Pham Minh Bang";
    forth.ChildUnitName = unitName;
    forth.ChildJobPositionName = positionName;
    forth.ImageUrl = @"PMBang.jpg";
    forth.ParentId = [NSNumber numberWithInt:1];
    forth.ParentName = @"Hai Le Thanh";
    
    org_tns2_V_ESS_ORG_DIRECT_REPORT *fifth = [org_tns2_V_ESS_ORG_DIRECT_REPORT new];
    fifth.ChildId = [NSNumber numberWithInt:14];
    fifth.ChildName = @"Pham Minh Bang";
    fifth.ChildUnitName = unitName;
    fifth.ChildJobPositionName = positionName;
    fifth.ImageUrl = @"PMBang.jpg";
    fifth.ParentId = [NSNumber numberWithInt:1];
    fifth.ParentName = @"Hai Le Thanh";
    
    org_tns2_V_ESS_ORG_DIRECT_REPORT *sixth = [org_tns2_V_ESS_ORG_DIRECT_REPORT new];
    sixth.ChildId = [NSNumber numberWithInt:14];
    sixth.ChildName = @"Pham Minh Bang";
    sixth.ChildUnitName = unitName;
    sixth.ChildJobPositionName = positionName;
    sixth.ImageUrl = @"PMBang.jpg";
    sixth.ParentId = [NSNumber numberWithInt:1];
    sixth.ParentName = @"Hai Le Thanh";
    
    return @[first, sec, third, forth, fifth, sixth];
}
@end
