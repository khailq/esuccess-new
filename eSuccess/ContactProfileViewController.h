//
//  ContactProfileViewController.h
//  eSuccess
//
//  Created by admin on 6/15/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"
#import "EmpProfileLayerServiceSvc.h"
@interface ContactProfileViewController : BaseDetailTableViewController
{
    __weak IBOutlet UILabel *lblTemporaryAddress;
    __weak IBOutlet UILabel *lblPermanentAddress;
    __weak IBOutlet UILabel *lblAddressContact;
    __weak IBOutlet UILabel *lblEmail;
    __weak IBOutlet UILabel *lblFax;
    __weak IBOutlet UILabel *lblHomePhone;
    __weak IBOutlet UILabel *lblCompanyPhone;
    __weak IBOutlet UILabel *lblMobile;

}
@property(nonatomic, strong) NSMutableArray *contactArr;
@end
