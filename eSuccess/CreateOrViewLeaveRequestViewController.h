//
//  CreateOrViewLeaveRequestViewController.h
//  eSuccess
//
//  Created by HPTVIETNAM on 9/25/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"

@interface CreateOrViewLeaveRequestViewController : BaseDetailTableViewController

@property (assign, nonatomic) BOOL isViewMode;

@end
