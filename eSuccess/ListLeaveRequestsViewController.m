//
//  ListLeaveRequestsViewController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 9/24/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "ListLeaveRequestsViewController.h"
#import "TimeServieSvc.h"
#import "LeaveRequestCell.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>

@interface ListLeaveRequestsViewController ()
{
    NSArray *pendingRequests;
    NSArray *approvedRequests;
    NSArray *deniedRequests;
    __weak IBOutlet UISegmentedControl *segmentControl;
    
    //__weak IBOutlet UITableView *oTableView;
    NSDateFormatter *dateFormatter;
}
@end

@implementation ListLeaveRequestsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self onSegmentSelectionChanged:segmentControl];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onSegmentSelectionChanged:(id)sender {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        AppDelegate *app = [UIApplication sharedApplication].delegate;
        
        switch (segmentControl.selectedSegmentIndex) {
            case 0:
                [self getPendingRequestsForEmployee:app.sysUser.EmployeeId];
                break;
            case 1:
                [self getApprovedRequestsForEmployee:app.sysUser.EmployeeId];
                break;
            default:
                [self getDeniedRequestsForEmployee:app.sysUser.EmployeeId];
                break;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        });
    });
}

#pragma mark- Table View Data Source

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *list = [self getRequestList];
    
    if (list.count == 0)
    {
        UITableViewCell * cell = [self getDefaultEmptyCell];        
        return cell;
    }
    
    time_tns1_V_TimeLeaveRequest *request  = list[indexPath.row];
    LeaveRequestCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LeaveRequestCell"];
    cell.lb_name.text = request.FullName;
    cell.lb_unitName.text = request.UnitName;
    cell.lb_absenceType.text = request.LeaveTypeNameVN;
    cell.lb_reason.text = request.Flex3;
    
    if (dateFormatter == nil)
    {
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"dd/MM/yyyy";
    }
    
    cell.lb_fromDate.text = [dateFormatter stringFromDate:request.Flex28];
    cell.lb_toDate.text = [dateFormatter stringFromDate:request.Flex29];
    
    
    UIView *bgView = [[UIView alloc] initWithFrame:cell.frame];
    indexPath.row % 2 == 0 ? [bgView setBackgroundColor:[UIColor colorWithRed:209.0/255.0 green:209.0/255.0 blue:209.0/255.0 alpha:1.0]] : [bgView setBackgroundColor:[UIColor colorWithRed:196.0/255.0 green:196.0/255.0 blue:196.0/255.0 alpha:1.0]];
    [cell setBackgroundView:bgView];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 111;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *selectedList = [self getRequestList];
    return selectedList.count == 0 ? 1 : selectedList.count;
}

#pragma mark- TableView delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark-

-(NSArray*)getRequestList
{
    switch (segmentControl.selectedSegmentIndex) {
        case 0:
            return pendingRequests;
        case 1:
            return approvedRequests;            
        default:
            return deniedRequests;
    }
}

-(void)getPendingRequestsForEmployee:(NSNumber*)empId
{
    if (pendingRequests != nil)
        return;
    
    BasicHttpBinding_ITimeServieBinding *binding = [TimeServieSvc BasicHttpBinding_ITimeServieBinding];
    TimeServieSvc_GetWaitingLeaveRequestsByEmpId *request = [TimeServieSvc_GetWaitingLeaveRequestsByEmpId new];
    request.empId = empId;
    BasicHttpBinding_ITimeServieBindingResponse *response = [binding GetWaitingLeaveRequestsByEmpIdUsingParameters:request];
    for(id part in response.bodyParts)
    {
        if ([part isKindOfClass:[TimeServieSvc_GetWaitingLeaveRequestsByEmpIdResponse class]])
        {
            time_tns1_ArrayOfV_TimeLeaveRequest *result = [part GetWaitingLeaveRequestsByEmpIdResult];
            pendingRequests = result.V_TimeLeaveRequest;
        }
    }
}

-(void)getApprovedRequestsForEmployee:(NSNumber*)empId
{
    if (approvedRequests != nil)
        return;
    
    BasicHttpBinding_ITimeServieBinding *binding = [TimeServieSvc BasicHttpBinding_ITimeServieBinding];
    TimeServieSvc_GetApprovedLeaveRequestsByEmpId *request = [TimeServieSvc_GetApprovedLeaveRequestsByEmpId new];
    request.empId = empId;
    BasicHttpBinding_ITimeServieBindingResponse *response = [binding GetApprovedLeaveRequestsByEmpIdUsingParameters:request];
    for(id part in response.bodyParts)
    {
        if ([part isKindOfClass:[TimeServieSvc_GetApprovedLeaveRequestsByEmpIdResponse class]])
        {
            time_tns1_ArrayOfV_TimeLeaveRequest *result = [part GetApprovedLeaveRequestsByEmpIdResult];
            approvedRequests = result.V_TimeLeaveRequest;
        }
    }
}

-(void)getDeniedRequestsForEmployee:(NSNumber*)empId
{
    if (deniedRequests != nil)
        return;
    
    BasicHttpBinding_ITimeServieBinding *binding = [TimeServieSvc BasicHttpBinding_ITimeServieBinding];
    TimeServieSvc_GetRejectedLeaveRequestsByEmpId *request = [TimeServieSvc_GetRejectedLeaveRequestsByEmpId new];
    request.empId = empId;
    BasicHttpBinding_ITimeServieBindingResponse *response = [binding GetRejectedLeaveRequestsByEmpIdUsingParameters:request];
    for(id part in response.bodyParts)
    {
        if ([part isKindOfClass:[TimeServieSvc_GetRejectedLeaveRequestsByEmpIdResponse class]])
        {
            time_tns1_ArrayOfV_TimeLeaveRequest *result = [part GetRejectedLeaveRequestsByEmpIdResult];
            deniedRequests = result.V_TimeLeaveRequest;
        }
    }
}



#pragma mark-
@end
