//
//  JobPositionViewController.m
//  eSuccess
//
//  Created by admin on 6/14/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "JobPositionViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "DayLeaveDetailCell.h"
@interface JobPositionViewController ()

@end

@implementation JobPositionViewController
@synthesize jobPostionArr;
@synthesize jobNameArr;
static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Chức danh- Chức vụ";
    [self getData];
    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 130;
//}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([jobNameArr count] > 0) {
        return [jobNameArr objectAtIndex:section];
    }
    return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [jobPostionArr count] > 0 ? jobPostionArr.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //return [profileArray count];
    if([jobPostionArr count] > 0){
        return 7;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (jobPostionArr.count == 0) {
        return [self getDefaultEmptyCell];
    }
    
    DayLeaveDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
    if (cell == nil) {
        cell = [[DayLeaveDetailCell alloc]init];
    }
    cell.lbLeft.font = [UIFont systemFontOfSize:14];
    cell.lbLeft.backgroundColor = [UIColor clearColor];
    cell.lbRight.font = [UIFont boldSystemFontOfSize:16];
    cell.lbRight.backgroundColor = [UIColor clearColor];
    int section = indexPath.section;
    
    empl_tns1_V_EmpProfileJobPosition *job = [jobPostionArr objectAtIndex:section];
    if (indexPath.row == 0) {
        cell.lbLeft.text = @"Vị trí";
        cell.lbRight.text = job.OrgJobPositionName;
    }
    if (indexPath.row == 1) {
        cell.lbLeft.text = @"Phòng ban";
        cell.lbRight.text = job.OrgUnitName;
    }
    if (indexPath.row == 2) {
        cell.lbLeft.text = @"Mã công việc";
        cell.lbRight.text = job.OrgJobName;
    }
    if (indexPath.row == 3) {
        cell.lbLeft.text = @"Cấp độ công việc";
        cell.lbRight.text = job.OrgWorkLevelName;
    }
    if (indexPath.row == 4) {
        cell.lbLeft.text = @"Báo cáo trực tiếp";
        cell.lbRight.text = job.DirectReportToEmployeeFullName;
    }
    
    if (indexPath.row == 5) {
        cell.lbLeft.text = @"Lý do";
        cell.lbRight.text = job.Reason;
    }
    
    if (indexPath.row == 6) {
        cell.lbLeft.text = @"Ngày thực hiện";
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        cell.lbRight.text = [dateFormatter stringFromDate:job.ImplementationDate];
    }
    return cell;
}

#pragma get Job Positon
-(void)getData
{
    jobPostionArr = [[NSMutableArray alloc]init];
    jobNameArr = [[NSMutableArray alloc]init];
    
    [self getEmpProfileJobPositionById];
}

- (void)getEmpProfileJobPositionById
{
    BasicHttpBinding_IEmpProfileLayerServiceBinding * binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
    EmpProfileLayerServiceSvc_GetViewEmpProfileJobPositions *request = [[EmpProfileLayerServiceSvc_GetViewEmpProfileJobPositions alloc] init];
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    if (app.otherEmpId != nil) {
        request.EmployeeId = app.otherEmpId;
    } else{
        request.employeeId = app.sysUser.EmployeeId;
    }
    //request.EmployeeId = [NSNumber numberWithInt:2];
    [binding GetViewEmpProfileJobPositionsAsyncUsingParameters:request delegate:self];
}

- (void)operation:(BasicHttpBinding_IEmpProfileLayerServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)response
{
    if ([response.bodyParts count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi kết nối" message:@"Không thể kết nối đến server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    for (id mine in response.bodyParts) {
        if ([mine isKindOfClass:[EmpProfileLayerServiceSvc_GetViewEmpProfileJobPositionsResponse class]]) {
            empl_tns1_ArrayOfV_EmpProfileJobPosition *result = [mine GetViewEmpProfileJobPositionsResult];
            jobPostionArr = result.V_EmpProfileJobPosition;
            for (empl_tns1_V_EmpProfileJobPosition *job in jobPostionArr)  {
                [jobNameArr addObject:job.OrgJobName];
            }
        }
        
        //        if ([mine isKindOfClass:[SOAPFault class]])
        //        {
        //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Server error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //            [alert show];
        //        }
    }
    
    if (!self.viewDidDisappear)
        [self.tableView reloadData];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
}

@end
