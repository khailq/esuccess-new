#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class time_tns3_ArrayOfWorkingShiftScheduleModel;
@class time_tns3_WorkingShiftScheduleModel;
@class time_tns3_ArrayOfTimeStandardScheduleOutputModel;
@class time_tns3_TimeStandardScheduleOutputModel;
@class time_tns3_SearchingConditionsModel;
@class time_tns3_ArrayOfWorkingRecordWorkSheet;
@class time_tns3_WorkingRecordWorkSheet;
@class time_tns3_ArrayOfWorkingRecordAdjustmentModel;
@class time_tns3_WorkingRecordAdjustmentModel;
@class time_tns3_ArrayOfTimeEmpOTDetailModel;
@class time_tns3_TimeEmpOTDetailModel;
@class time_tns3_TimeValidELAndTotalWTModel;
@class time_tns3_MsgNotifyModel;
@class time_tns3_ArrayOfTimeOTReqMstModel;
@class time_tns3_TimeOTReqMstModel;
@class time_tns3_ArrayOfTimeOTReqEmpModel;
@class time_tns3_TimeOTReqEmpModel;
@class time_tns3_ArrayOfTimeOTReqDetailModel;
@class time_tns3_TimeOTReqDetailModel;
@class time_tns3_ArrayOfTimeParameter;
@class time_tns3_TimeParameter;
@class time_tns3_TimeCalculatedDataModel;
@class time_tns3_ArrayOfTimeOutputPayValueModel;
@class time_tns3_TimeOutputPayValueModel;
@class time_tns3_ArrayOfTimeWRGMappingModel;
@class time_tns3_TimeWRGMappingModel;
@class time_tns3_ArrayOfTimeInOutAdjustmentModel;
@class time_tns3_TimeInOutAdjustmentModel;
@class time_tns3_ArrayOfSetupWizardResultModel;
@class time_tns3_SetupWizardResultModel;
@class time_tns3_ArrayOfEventObjectCalendarBSModel;
@class time_tns3_EventObjectCalendarBSModel;
@class time_tns3_SelfServiceEmployeeScreenModel;
@class time_tns3_SelfServiceManagerScreenModel;
@class time_tns3_EmployeeInfoRequest;
@class time_tns3_ArrayOfEmployeeInfoRequest;
@class time_tns3_ArrayOfTimeOTCoefficientModel;
@class time_tns3_TimeOTCoefficientModel;
@class time_tns3_ArrayOfTimeAllowanceModel;
@class time_tns3_TimeAllowanceModel;
@class time_tns3_ArrayOfTimeReqInOutModel;
@class time_tns3_TimeReqInOutModel;
@class time_tns3_ArrayOfTimeReqInOutStatusModel;
@class time_tns3_TimeReqInOutStatusModel;
@class time_tns3_ArrayOfTimeLogRuleModel;
@class time_tns3_TimeLogRuleModel;
@class time_tns3_ArrayOfTimeAllowInOutModel;
@class time_tns3_TimeAllowInOutModel;
@class time_tns3_ArrayOfTimeOTStartTypeModel;
@class time_tns3_TimeOTStartTypeModel;
@interface time_tns3_WorkingShiftScheduleModel : NSObject {
	
/* elements */
	NSString * C01;
	NSString * C02;
	NSString * C03;
	NSString * C04;
	NSString * C05;
	NSString * C06;
	NSString * C07;
	NSString * C08;
	NSString * C09;
	NSString * C10;
	NSString * C11;
	NSString * C12;
	NSString * C13;
	NSString * C14;
	NSString * C15;
	NSString * C16;
	NSString * C17;
	NSString * C18;
	NSString * C19;
	NSString * C20;
	NSString * C21;
	NSString * C22;
	NSString * C23;
	NSString * C24;
	NSString * C25;
	NSString * C26;
	NSString * C27;
	NSString * C28;
	NSString * C29;
	NSString * C30;
	NSString * C31;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * Day01;
	NSString * Day02;
	NSString * Day03;
	NSString * Day04;
	NSString * Day05;
	NSString * Day06;
	NSString * Day07;
	NSString * Day08;
	NSString * Day09;
	NSString * Day10;
	NSString * Day11;
	NSString * Day12;
	NSString * Day13;
	NSString * Day14;
	NSString * Day15;
	NSString * Day16;
	NSString * Day17;
	NSString * Day18;
	NSString * Day19;
	NSString * Day20;
	NSString * Day21;
	NSString * Day22;
	NSString * Day23;
	NSString * Day24;
	NSString * Day25;
	NSString * Day26;
	NSString * Day27;
	NSString * Day28;
	NSString * Day29;
	NSString * Day30;
	NSString * Day31;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * EmployeeName;
	NSString * FLex1;
	NSNumber * FLex10;
	NSDate * FLex11;
	NSDate * FLex12;
	NSDate * FLex13;
	USBoolean * FLex14;
	USBoolean * FLex15;
	USBoolean * FLex16;
	NSString * FLex2;
	NSString * FLex3;
	NSString * FLex4;
	NSString * FLex5;
	NSNumber * FLex6;
	NSNumber * FLex7;
	NSNumber * FLex8;
	NSNumber * FLex9;
	NSString * GroupCode;
	NSString * GroupName;
	USBoolean * IsDeleted;
	USBoolean * IsSubstitutive;
	USBoolean * IsWorkSheetChecked;
	NSNumber * LastDayOff;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgUnitName;
	NSNumber * TimeWorkingShiftId;
	NSString * WageType;
	NSNumber * WorkingMonth;
	NSString * WorkingType;
	NSNumber * WorkingYear;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_WorkingShiftScheduleModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * C01;
@property (retain) NSString * C02;
@property (retain) NSString * C03;
@property (retain) NSString * C04;
@property (retain) NSString * C05;
@property (retain) NSString * C06;
@property (retain) NSString * C07;
@property (retain) NSString * C08;
@property (retain) NSString * C09;
@property (retain) NSString * C10;
@property (retain) NSString * C11;
@property (retain) NSString * C12;
@property (retain) NSString * C13;
@property (retain) NSString * C14;
@property (retain) NSString * C15;
@property (retain) NSString * C16;
@property (retain) NSString * C17;
@property (retain) NSString * C18;
@property (retain) NSString * C19;
@property (retain) NSString * C20;
@property (retain) NSString * C21;
@property (retain) NSString * C22;
@property (retain) NSString * C23;
@property (retain) NSString * C24;
@property (retain) NSString * C25;
@property (retain) NSString * C26;
@property (retain) NSString * C27;
@property (retain) NSString * C28;
@property (retain) NSString * C29;
@property (retain) NSString * C30;
@property (retain) NSString * C31;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Day01;
@property (retain) NSString * Day02;
@property (retain) NSString * Day03;
@property (retain) NSString * Day04;
@property (retain) NSString * Day05;
@property (retain) NSString * Day06;
@property (retain) NSString * Day07;
@property (retain) NSString * Day08;
@property (retain) NSString * Day09;
@property (retain) NSString * Day10;
@property (retain) NSString * Day11;
@property (retain) NSString * Day12;
@property (retain) NSString * Day13;
@property (retain) NSString * Day14;
@property (retain) NSString * Day15;
@property (retain) NSString * Day16;
@property (retain) NSString * Day17;
@property (retain) NSString * Day18;
@property (retain) NSString * Day19;
@property (retain) NSString * Day20;
@property (retain) NSString * Day21;
@property (retain) NSString * Day22;
@property (retain) NSString * Day23;
@property (retain) NSString * Day24;
@property (retain) NSString * Day25;
@property (retain) NSString * Day26;
@property (retain) NSString * Day27;
@property (retain) NSString * Day28;
@property (retain) NSString * Day29;
@property (retain) NSString * Day30;
@property (retain) NSString * Day31;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * EmployeeName;
@property (retain) NSString * FLex1;
@property (retain) NSNumber * FLex10;
@property (retain) NSDate * FLex11;
@property (retain) NSDate * FLex12;
@property (retain) NSDate * FLex13;
@property (retain) USBoolean * FLex14;
@property (retain) USBoolean * FLex15;
@property (retain) USBoolean * FLex16;
@property (retain) NSString * FLex2;
@property (retain) NSString * FLex3;
@property (retain) NSString * FLex4;
@property (retain) NSString * FLex5;
@property (retain) NSNumber * FLex6;
@property (retain) NSNumber * FLex7;
@property (retain) NSNumber * FLex8;
@property (retain) NSNumber * FLex9;
@property (retain) NSString * GroupCode;
@property (retain) NSString * GroupName;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsSubstitutive;
@property (retain) USBoolean * IsWorkSheetChecked;
@property (retain) NSNumber * LastDayOff;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitName;
@property (retain) NSNumber * TimeWorkingShiftId;
@property (retain) NSString * WageType;
@property (retain) NSNumber * WorkingMonth;
@property (retain) NSString * WorkingType;
@property (retain) NSNumber * WorkingYear;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_ArrayOfWorkingShiftScheduleModel : NSObject {
	
/* elements */
	NSMutableArray *WorkingShiftScheduleModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_ArrayOfWorkingShiftScheduleModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addWorkingShiftScheduleModel:(time_tns3_WorkingShiftScheduleModel *)toAdd;
@property (readonly) NSMutableArray * WorkingShiftScheduleModel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_TimeStandardScheduleOutputModel : NSObject {
	
/* elements */
	NSNumber * COMPANY_ID;
	NSNumber * CREATED_BY;
	NSDate * CREATED_DATE;
	NSString * DAY_01;
	NSString * DAY_02;
	NSString * DAY_03;
	NSString * DAY_04;
	NSString * DAY_05;
	NSString * DAY_06;
	NSString * DAY_07;
	NSString * DAY_08;
	NSString * DAY_09;
	NSString * DAY_10;
	NSString * DAY_11;
	NSString * DAY_12;
	NSString * DAY_13;
	NSString * DAY_14;
	NSString * DAY_15;
	NSString * DAY_16;
	NSString * DAY_17;
	NSString * DAY_18;
	NSString * DAY_19;
	NSString * DAY_20;
	NSString * DAY_21;
	NSString * DAY_22;
	NSString * DAY_23;
	NSString * DAY_24;
	NSString * DAY_25;
	NSString * DAY_26;
	NSString * DAY_27;
	NSString * DAY_28;
	NSString * DAY_29;
	NSString * DAY_30;
	NSString * DAY_31;
	NSString * GROUP_CODE;
	USBoolean * IS_CHECKED;
	USBoolean * IS_DELETED;
	NSNumber * MODIFIED_BY;
	NSDate * MODIFIED_DATE;
	NSString * WAGE_TYPE;
	NSString * WORKING_FORM_GROUP;
	NSString * WORKING_NAME_EN;
	NSString * WORKING_NAME_VN;
	NSNumber * WORKING_SHIFT_ID;
	NSNumber * WORKING_SHIFT_MONTH;
	NSNumber * WORKING_SHIFT_YEAR;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_TimeStandardScheduleOutputModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSNumber * CREATED_BY;
@property (retain) NSDate * CREATED_DATE;
@property (retain) NSString * DAY_01;
@property (retain) NSString * DAY_02;
@property (retain) NSString * DAY_03;
@property (retain) NSString * DAY_04;
@property (retain) NSString * DAY_05;
@property (retain) NSString * DAY_06;
@property (retain) NSString * DAY_07;
@property (retain) NSString * DAY_08;
@property (retain) NSString * DAY_09;
@property (retain) NSString * DAY_10;
@property (retain) NSString * DAY_11;
@property (retain) NSString * DAY_12;
@property (retain) NSString * DAY_13;
@property (retain) NSString * DAY_14;
@property (retain) NSString * DAY_15;
@property (retain) NSString * DAY_16;
@property (retain) NSString * DAY_17;
@property (retain) NSString * DAY_18;
@property (retain) NSString * DAY_19;
@property (retain) NSString * DAY_20;
@property (retain) NSString * DAY_21;
@property (retain) NSString * DAY_22;
@property (retain) NSString * DAY_23;
@property (retain) NSString * DAY_24;
@property (retain) NSString * DAY_25;
@property (retain) NSString * DAY_26;
@property (retain) NSString * DAY_27;
@property (retain) NSString * DAY_28;
@property (retain) NSString * DAY_29;
@property (retain) NSString * DAY_30;
@property (retain) NSString * DAY_31;
@property (retain) NSString * GROUP_CODE;
@property (retain) USBoolean * IS_CHECKED;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSNumber * MODIFIED_BY;
@property (retain) NSDate * MODIFIED_DATE;
@property (retain) NSString * WAGE_TYPE;
@property (retain) NSString * WORKING_FORM_GROUP;
@property (retain) NSString * WORKING_NAME_EN;
@property (retain) NSString * WORKING_NAME_VN;
@property (retain) NSNumber * WORKING_SHIFT_ID;
@property (retain) NSNumber * WORKING_SHIFT_MONTH;
@property (retain) NSNumber * WORKING_SHIFT_YEAR;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_ArrayOfTimeStandardScheduleOutputModel : NSObject {
	
/* elements */
	NSMutableArray *TimeStandardScheduleOutputModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_ArrayOfTimeStandardScheduleOutputModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeStandardScheduleOutputModel:(time_tns3_TimeStandardScheduleOutputModel *)toAdd;
@property (readonly) NSMutableArray * TimeStandardScheduleOutputModel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_SearchingConditionsModel : NSObject {
	
/* elements */
	NSString * dateOffFrom;
	NSString * dateOffTo;
	NSNumber * intEmpId;
	NSNumber * intOrgGroupId;
	NSNumber * intOrgJobPosId;
	NSNumber * intOrgUnitId;
	NSNumber * status;
	NSString * strEmpCd;
	NSString * strEmpNm;
	NSString * strEntryDate;
	NSString * strFrom;
	NSString * strOfficealDate;
	NSString * strOrgGroupCd;
	NSString * strOrgGroupNm;
	NSString * strOrgJobPosCd;
	NSString * strOrgJobPosNm;
	NSString * strOrgUnitCd;
	NSString * strOrgUnitNm;
	NSString * strTo;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_SearchingConditionsModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * dateOffFrom;
@property (retain) NSString * dateOffTo;
@property (retain) NSNumber * intEmpId;
@property (retain) NSNumber * intOrgGroupId;
@property (retain) NSNumber * intOrgJobPosId;
@property (retain) NSNumber * intOrgUnitId;
@property (retain) NSNumber * status;
@property (retain) NSString * strEmpCd;
@property (retain) NSString * strEmpNm;
@property (retain) NSString * strEntryDate;
@property (retain) NSString * strFrom;
@property (retain) NSString * strOfficealDate;
@property (retain) NSString * strOrgGroupCd;
@property (retain) NSString * strOrgGroupNm;
@property (retain) NSString * strOrgJobPosCd;
@property (retain) NSString * strOrgJobPosNm;
@property (retain) NSString * strOrgUnitCd;
@property (retain) NSString * strOrgUnitNm;
@property (retain) NSString * strTo;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_WorkingRecordWorkSheet : NSObject {
	
/* elements */
	NSNumber * ALLOWANCE_HOURS;
	NSNumber * COMPANY_ID;
	NSNumber * COUNCIDED_SHIFT_HOURS;
	NSNumber * CREATED_BY;
	NSDate * CREATED_DATE;
	NSNumber * DILIGENCE_HOURS;
	NSNumber * EARLY_IN_MINUTES;
	NSNumber * EARLY_IN_TIMES;
	NSNumber * EARLY_OUT_MINUTES;
	NSNumber * EARLY_OUT_TIMES;
	NSString * EMPLOYEE_CODE;
	NSNumber * EMPLOYEE_ID;
	NSString * EMPLOYEE_NAME;
	NSString * EMPLOYEE_NAME_FOREIGN;
	NSString * FLEX_1;
	NSNumber * FLEX_10;
	NSDate * FLEX_11;
	NSDate * FLEX_12;
	NSDate * FLEX_13;
	NSDate * FLEX_14;
	NSDate * FLEX_15;
	NSString * FLEX_2;
	NSString * FLEX_3;
	NSString * FLEX_4;
	NSString * FLEX_5;
	NSNumber * FLEX_6;
	NSNumber * FLEX_7;
	NSNumber * FLEX_8;
	NSNumber * FLEX_9;
	NSDate * FROM_DATE;
	NSString * GROUP_CODE;
	NSString * GROUP_NAME;
	NSString * GROUP_NAME_FOREIGN;
	NSNumber * HOLIDAY_DAYS;
	USBoolean * IS_CHECKED;
	USBoolean * IS_DELETED;
	NSNumber * LATE_IN_MINUTES;
	NSNumber * LATE_IN_TIMES;
	NSNumber * LATE_OUT_MINUTES;
	NSNumber * LATE_OUT_TIMES;
	NSNumber * LEAVE_TYPE_CL;
	NSNumber * LEAVE_TYPE_CP1;
	NSNumber * LEAVE_TYPE_CP2;
	NSNumber * LEAVE_TYPE_CP3;
	NSNumber * LEAVE_TYPE_CP4;
	NSNumber * LEAVE_TYPE_CP5;
	NSNumber * LEAVE_TYPE_CP6;
	NSNumber * LEAVE_TYPE_CP7;
	NSNumber * LEAVE_TYPE_IP1;
	NSNumber * LEAVE_TYPE_IP2;
	NSNumber * LEAVE_TYPE_IP3;
	NSNumber * LEAVE_TYPE_IP4;
	NSNumber * LEAVE_TYPE_IP5;
	NSNumber * LEAVE_TYPE_IP6;
	NSNumber * LEAVE_TYPE_UP1;
	NSNumber * LEAVE_TYPE_UP2;
	NSNumber * MODIFIED_BY;
	NSDate * MODIFIED_DATE;
	NSDate * MONTH_YEAR;
	NSString * ORG_UNIT_CODE;
	NSNumber * ORG_UNIT_ID;
	NSString * ORG_UNIT_NAME;
	NSString * ORG_UNIT_NAME_FOREIGN;
	NSNumber * PAID_LEAVE_DAYS;
	NSNumber * REASON_LEAVE_DAYS;
	NSString * REMARK;
	NSNumber * TB_ESS_TIME_EMP_DAILY_SHIFT_ID;
	NSNumber * TOTAL_NON_OT_HOURS;
	NSNumber * TOTAL_OT10_HOURS;
	NSNumber * TOTAL_OT1_HOURS;
	NSNumber * TOTAL_OT2_HOURS;
	NSNumber * TOTAL_OT3_HOURS;
	NSNumber * TOTAL_OT4_HOURS;
	NSNumber * TOTAL_OT5_HOURS;
	NSNumber * TOTAL_OT6_HOURS;
	NSNumber * TOTAL_OT7_HOURS;
	NSNumber * TOTAL_OT8_HOURS;
	NSNumber * TOTAL_OT9_HOURS;
	NSNumber * TOTAL_OT_HOURS;
	NSNumber * TOTAL_UNPAID_HOURS;
	NSNumber * TOTAL_WORKING_HOURS;
	NSDate * TO_DATE;
	NSNumber * UNDILIGENCE_HOURS;
	NSNumber * UNPAID_LEAVE_DAYS;
	NSNumber * UNREASON_LEAVE_DAYS;
	NSNumber * WORKING_HOURS_IN_PLAN;
	NSNumber * WORKING_MONTH;
	NSNumber * WORKING_YEAR;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_WorkingRecordWorkSheet *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ALLOWANCE_HOURS;
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSNumber * COUNCIDED_SHIFT_HOURS;
@property (retain) NSNumber * CREATED_BY;
@property (retain) NSDate * CREATED_DATE;
@property (retain) NSNumber * DILIGENCE_HOURS;
@property (retain) NSNumber * EARLY_IN_MINUTES;
@property (retain) NSNumber * EARLY_IN_TIMES;
@property (retain) NSNumber * EARLY_OUT_MINUTES;
@property (retain) NSNumber * EARLY_OUT_TIMES;
@property (retain) NSString * EMPLOYEE_CODE;
@property (retain) NSNumber * EMPLOYEE_ID;
@property (retain) NSString * EMPLOYEE_NAME;
@property (retain) NSString * EMPLOYEE_NAME_FOREIGN;
@property (retain) NSString * FLEX_1;
@property (retain) NSNumber * FLEX_10;
@property (retain) NSDate * FLEX_11;
@property (retain) NSDate * FLEX_12;
@property (retain) NSDate * FLEX_13;
@property (retain) NSDate * FLEX_14;
@property (retain) NSDate * FLEX_15;
@property (retain) NSString * FLEX_2;
@property (retain) NSString * FLEX_3;
@property (retain) NSString * FLEX_4;
@property (retain) NSString * FLEX_5;
@property (retain) NSNumber * FLEX_6;
@property (retain) NSNumber * FLEX_7;
@property (retain) NSNumber * FLEX_8;
@property (retain) NSNumber * FLEX_9;
@property (retain) NSDate * FROM_DATE;
@property (retain) NSString * GROUP_CODE;
@property (retain) NSString * GROUP_NAME;
@property (retain) NSString * GROUP_NAME_FOREIGN;
@property (retain) NSNumber * HOLIDAY_DAYS;
@property (retain) USBoolean * IS_CHECKED;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSNumber * LATE_IN_MINUTES;
@property (retain) NSNumber * LATE_IN_TIMES;
@property (retain) NSNumber * LATE_OUT_MINUTES;
@property (retain) NSNumber * LATE_OUT_TIMES;
@property (retain) NSNumber * LEAVE_TYPE_CL;
@property (retain) NSNumber * LEAVE_TYPE_CP1;
@property (retain) NSNumber * LEAVE_TYPE_CP2;
@property (retain) NSNumber * LEAVE_TYPE_CP3;
@property (retain) NSNumber * LEAVE_TYPE_CP4;
@property (retain) NSNumber * LEAVE_TYPE_CP5;
@property (retain) NSNumber * LEAVE_TYPE_CP6;
@property (retain) NSNumber * LEAVE_TYPE_CP7;
@property (retain) NSNumber * LEAVE_TYPE_IP1;
@property (retain) NSNumber * LEAVE_TYPE_IP2;
@property (retain) NSNumber * LEAVE_TYPE_IP3;
@property (retain) NSNumber * LEAVE_TYPE_IP4;
@property (retain) NSNumber * LEAVE_TYPE_IP5;
@property (retain) NSNumber * LEAVE_TYPE_IP6;
@property (retain) NSNumber * LEAVE_TYPE_UP1;
@property (retain) NSNumber * LEAVE_TYPE_UP2;
@property (retain) NSNumber * MODIFIED_BY;
@property (retain) NSDate * MODIFIED_DATE;
@property (retain) NSDate * MONTH_YEAR;
@property (retain) NSString * ORG_UNIT_CODE;
@property (retain) NSNumber * ORG_UNIT_ID;
@property (retain) NSString * ORG_UNIT_NAME;
@property (retain) NSString * ORG_UNIT_NAME_FOREIGN;
@property (retain) NSNumber * PAID_LEAVE_DAYS;
@property (retain) NSNumber * REASON_LEAVE_DAYS;
@property (retain) NSString * REMARK;
@property (retain) NSNumber * TB_ESS_TIME_EMP_DAILY_SHIFT_ID;
@property (retain) NSNumber * TOTAL_NON_OT_HOURS;
@property (retain) NSNumber * TOTAL_OT10_HOURS;
@property (retain) NSNumber * TOTAL_OT1_HOURS;
@property (retain) NSNumber * TOTAL_OT2_HOURS;
@property (retain) NSNumber * TOTAL_OT3_HOURS;
@property (retain) NSNumber * TOTAL_OT4_HOURS;
@property (retain) NSNumber * TOTAL_OT5_HOURS;
@property (retain) NSNumber * TOTAL_OT6_HOURS;
@property (retain) NSNumber * TOTAL_OT7_HOURS;
@property (retain) NSNumber * TOTAL_OT8_HOURS;
@property (retain) NSNumber * TOTAL_OT9_HOURS;
@property (retain) NSNumber * TOTAL_OT_HOURS;
@property (retain) NSNumber * TOTAL_UNPAID_HOURS;
@property (retain) NSNumber * TOTAL_WORKING_HOURS;
@property (retain) NSDate * TO_DATE;
@property (retain) NSNumber * UNDILIGENCE_HOURS;
@property (retain) NSNumber * UNPAID_LEAVE_DAYS;
@property (retain) NSNumber * UNREASON_LEAVE_DAYS;
@property (retain) NSNumber * WORKING_HOURS_IN_PLAN;
@property (retain) NSNumber * WORKING_MONTH;
@property (retain) NSNumber * WORKING_YEAR;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_ArrayOfWorkingRecordWorkSheet : NSObject {
	
/* elements */
	NSMutableArray *WorkingRecordWorkSheet;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_ArrayOfWorkingRecordWorkSheet *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addWorkingRecordWorkSheet:(time_tns3_WorkingRecordWorkSheet *)toAdd;
@property (readonly) NSMutableArray * WorkingRecordWorkSheet;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_TimeEmpOTDetailModel : NSObject {
	
/* elements */
	NSDate * ACTUAL_END_TIME;
	NSNumber * ACTUAL_OT_MINUTES;
	NSDate * ACTUAL_START_TIME;
	NSNumber * COMPANY_ID;
	NSNumber * CREATED_BY;
	NSDate * CREATED_DATE;
	NSString * EMP_CODE;
	NSDate * END_TIME_IN_PLAN;
	USBoolean * IS_DELETED;
	NSNumber * MODIFIED_BY;
	NSDate * MODIFIED_DATE;
	NSDate * OT_DATE;
	NSNumber * OT_INDEX;
	NSString * OT_REQ_CODE;
	NSString * OT_TYPE;
	NSDate * START_TIME_IN_PLAN;
	NSNumber * TIME_ADJUSTMENT_ID;
	NSString * str_actual_end_time;
	NSString * str_actual_start_time;
	NSString * str_end_time_in_plan;
	NSString * str_ot_date;
	NSString * str_start_time_in_plan;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_TimeEmpOTDetailModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ACTUAL_END_TIME;
@property (retain) NSNumber * ACTUAL_OT_MINUTES;
@property (retain) NSDate * ACTUAL_START_TIME;
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSNumber * CREATED_BY;
@property (retain) NSDate * CREATED_DATE;
@property (retain) NSString * EMP_CODE;
@property (retain) NSDate * END_TIME_IN_PLAN;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSNumber * MODIFIED_BY;
@property (retain) NSDate * MODIFIED_DATE;
@property (retain) NSDate * OT_DATE;
@property (retain) NSNumber * OT_INDEX;
@property (retain) NSString * OT_REQ_CODE;
@property (retain) NSString * OT_TYPE;
@property (retain) NSDate * START_TIME_IN_PLAN;
@property (retain) NSNumber * TIME_ADJUSTMENT_ID;
@property (retain) NSString * str_actual_end_time;
@property (retain) NSString * str_actual_start_time;
@property (retain) NSString * str_end_time_in_plan;
@property (retain) NSString * str_ot_date;
@property (retain) NSString * str_start_time_in_plan;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_ArrayOfTimeEmpOTDetailModel : NSObject {
	
/* elements */
	NSMutableArray *TimeEmpOTDetailModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_ArrayOfTimeEmpOTDetailModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeEmpOTDetailModel:(time_tns3_TimeEmpOTDetailModel *)toAdd;
@property (readonly) NSMutableArray * TimeEmpOTDetailModel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_WorkingRecordAdjustmentModel : NSObject {
	
/* elements */
	NSDate * ACTUAL_END_WORKING;
	NSDate * ACTUAL_START_WORKING;
	NSDate * ADJUST_END_WORKING;
	NSDate * ADJUST_START_WORKING;
	NSNumber * ALLOWANCE_MINUTES;
	USBoolean * CHECKED;
	NSNumber * COMPANY_ID;
	NSNumber * COUNCIDED_SHIFT_MINUTES;
	NSNumber * CREATED_BY;
	NSDate * CREATED_DATE;
	NSNumber * EARLY_IN_MIN;
	NSNumber * EARLY_OUT_MIN;
	NSString * EMPLOYEE_CODE;
	NSString * EMPLOYEE_CODE_TK;
	NSDate * END_BREAK_TIME_OUT;
	NSString * FULL_NAME;
	USBoolean * IS_BREAK_TIME_OUT;
	USBoolean * IS_DELETED;
	USBoolean * IS_LEAVE;
	NSNumber * LATE_IN_MIN;
	NSNumber * LATE_OUT_MIN;
	NSNumber * LEAVE_TYPE_CL;
	NSNumber * LEAVE_TYPE_CP1;
	NSNumber * LEAVE_TYPE_CP2;
	NSNumber * LEAVE_TYPE_CP3;
	NSNumber * LEAVE_TYPE_CP4;
	NSNumber * LEAVE_TYPE_CP5;
	NSNumber * LEAVE_TYPE_CP6;
	NSNumber * LEAVE_TYPE_CP7;
	NSNumber * LEAVE_TYPE_IP1;
	NSNumber * LEAVE_TYPE_IP2;
	NSNumber * LEAVE_TYPE_IP3;
	NSNumber * LEAVE_TYPE_IP4;
	NSNumber * LEAVE_TYPE_IP5;
	NSNumber * LEAVE_TYPE_IP6;
	NSNumber * LEAVE_TYPE_UP1;
	NSNumber * LEAVE_TYPE_UP2;
	NSNumber * MODIFIED_BY;
	NSDate * MODIFIED_DATE;
	NSString * ORG_GROUP_CD;
	NSString * ORG_GROUP_NAME;
	NSString * ORG_GROUP_NAME_FOREIGN;
	NSString * ORG_UNIT_CODE;
	NSNumber * ORG_UNIT_ID;
	NSString * ORG_UNIT_NAME;
	NSString * REMARK;
	NSDate * START_BREAK_TIME_OUT;
	NSNumber * TIME_ADJUSTMENT_ID;
	time_tns3_ArrayOfTimeEmpOTDetailModel * TIME_EMP_OT_DETAIL;
	NSNumber * TOTAL_NON_OT_MINUTE;
	NSNumber * TOTAL_OT1;
	NSNumber * TOTAL_OT10;
	NSNumber * TOTAL_OT2;
	NSNumber * TOTAL_OT3;
	NSNumber * TOTAL_OT4;
	NSNumber * TOTAL_OT5;
	NSNumber * TOTAL_OT6;
	NSNumber * TOTAL_OT7;
	NSNumber * TOTAL_OT8;
	NSNumber * TOTAL_OT9;
	NSNumber * TOTAL_OT_MINUTE;
	NSNumber * TOTAL_UNPAID_MINUTE;
	NSNumber * TOTAL_WORKING_MINUTE;
	NSDate * WORKING_DATE;
	NSString * WORKING_FORM;
	NSString * str_actual_end_working;
	NSString * str_actual_start_working;
	NSString * str_adjust_end_working;
	NSString * str_adjust_start_working;
	NSString * str_end_break_time_out;
	NSString * str_start_break_time_out;
	NSString * str_working_date;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_WorkingRecordAdjustmentModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ACTUAL_END_WORKING;
@property (retain) NSDate * ACTUAL_START_WORKING;
@property (retain) NSDate * ADJUST_END_WORKING;
@property (retain) NSDate * ADJUST_START_WORKING;
@property (retain) NSNumber * ALLOWANCE_MINUTES;
@property (retain) USBoolean * CHECKED;
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSNumber * COUNCIDED_SHIFT_MINUTES;
@property (retain) NSNumber * CREATED_BY;
@property (retain) NSDate * CREATED_DATE;
@property (retain) NSNumber * EARLY_IN_MIN;
@property (retain) NSNumber * EARLY_OUT_MIN;
@property (retain) NSString * EMPLOYEE_CODE;
@property (retain) NSString * EMPLOYEE_CODE_TK;
@property (retain) NSDate * END_BREAK_TIME_OUT;
@property (retain) NSString * FULL_NAME;
@property (retain) USBoolean * IS_BREAK_TIME_OUT;
@property (retain) USBoolean * IS_DELETED;
@property (retain) USBoolean * IS_LEAVE;
@property (retain) NSNumber * LATE_IN_MIN;
@property (retain) NSNumber * LATE_OUT_MIN;
@property (retain) NSNumber * LEAVE_TYPE_CL;
@property (retain) NSNumber * LEAVE_TYPE_CP1;
@property (retain) NSNumber * LEAVE_TYPE_CP2;
@property (retain) NSNumber * LEAVE_TYPE_CP3;
@property (retain) NSNumber * LEAVE_TYPE_CP4;
@property (retain) NSNumber * LEAVE_TYPE_CP5;
@property (retain) NSNumber * LEAVE_TYPE_CP6;
@property (retain) NSNumber * LEAVE_TYPE_CP7;
@property (retain) NSNumber * LEAVE_TYPE_IP1;
@property (retain) NSNumber * LEAVE_TYPE_IP2;
@property (retain) NSNumber * LEAVE_TYPE_IP3;
@property (retain) NSNumber * LEAVE_TYPE_IP4;
@property (retain) NSNumber * LEAVE_TYPE_IP5;
@property (retain) NSNumber * LEAVE_TYPE_IP6;
@property (retain) NSNumber * LEAVE_TYPE_UP1;
@property (retain) NSNumber * LEAVE_TYPE_UP2;
@property (retain) NSNumber * MODIFIED_BY;
@property (retain) NSDate * MODIFIED_DATE;
@property (retain) NSString * ORG_GROUP_CD;
@property (retain) NSString * ORG_GROUP_NAME;
@property (retain) NSString * ORG_GROUP_NAME_FOREIGN;
@property (retain) NSString * ORG_UNIT_CODE;
@property (retain) NSNumber * ORG_UNIT_ID;
@property (retain) NSString * ORG_UNIT_NAME;
@property (retain) NSString * REMARK;
@property (retain) NSDate * START_BREAK_TIME_OUT;
@property (retain) NSNumber * TIME_ADJUSTMENT_ID;
@property (retain) time_tns3_ArrayOfTimeEmpOTDetailModel * TIME_EMP_OT_DETAIL;
@property (retain) NSNumber * TOTAL_NON_OT_MINUTE;
@property (retain) NSNumber * TOTAL_OT1;
@property (retain) NSNumber * TOTAL_OT10;
@property (retain) NSNumber * TOTAL_OT2;
@property (retain) NSNumber * TOTAL_OT3;
@property (retain) NSNumber * TOTAL_OT4;
@property (retain) NSNumber * TOTAL_OT5;
@property (retain) NSNumber * TOTAL_OT6;
@property (retain) NSNumber * TOTAL_OT7;
@property (retain) NSNumber * TOTAL_OT8;
@property (retain) NSNumber * TOTAL_OT9;
@property (retain) NSNumber * TOTAL_OT_MINUTE;
@property (retain) NSNumber * TOTAL_UNPAID_MINUTE;
@property (retain) NSNumber * TOTAL_WORKING_MINUTE;
@property (retain) NSDate * WORKING_DATE;
@property (retain) NSString * WORKING_FORM;
@property (retain) NSString * str_actual_end_working;
@property (retain) NSString * str_actual_start_working;
@property (retain) NSString * str_adjust_end_working;
@property (retain) NSString * str_adjust_start_working;
@property (retain) NSString * str_end_break_time_out;
@property (retain) NSString * str_start_break_time_out;
@property (retain) NSString * str_working_date;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_ArrayOfWorkingRecordAdjustmentModel : NSObject {
	
/* elements */
	NSMutableArray *WorkingRecordAdjustmentModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_ArrayOfWorkingRecordAdjustmentModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addWorkingRecordAdjustmentModel:(time_tns3_WorkingRecordAdjustmentModel *)toAdd;
@property (readonly) NSMutableArray * WorkingRecordAdjustmentModel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_MsgNotifyModel : NSObject {
	
/* elements */
	NSNumber * Id_;
	USBoolean * IsSuccess;
	USBoolean * IsValid;
	NSString * NotifyMsg;
	NSString * NotifyMsgEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_MsgNotifyModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Id_;
@property (retain) USBoolean * IsSuccess;
@property (retain) USBoolean * IsValid;
@property (retain) NSString * NotifyMsg;
@property (retain) NSString * NotifyMsgEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_TimeValidELAndTotalWTModel : NSObject {
	
/* elements */
	time_tns3_MsgNotifyModel * MsgNotify;
	NSDate * P_ACTUAL_TIME_IN;
	NSDate * P_ACTUAL_TIME_OUT;
	NSDate * P_BREAK_TIME_INT;
	NSDate * P_BREAK_TIME_OUT;
	NSNumber * P_COMPANY_ID;
	NSString * P_EMP_CODE;
	NSDate * P_IN_TIME_IN_PLAN;
	NSDate * P_OUT_TIME_IN_PLAN;
	NSDate * P_WORKING_DATE;
	NSString * P_WORKING_FORM;
	NSNumber * V_BREAK_TIME_MINUTE;
	NSNumber * V_EARLY_IN_TIME;
	NSNumber * V_EARLY_OUT_TIME;
	NSNumber * V_LATE_IN_TIME;
	NSNumber * V_LATE_OUT_TIME;
	NSNumber * V_TOTAL_WORKING_MINUTE;
	NSString * str_p_actual_time_in;
	NSString * str_p_actual_time_out;
	NSString * str_p_break_time_int;
	NSString * str_p_break_time_out;
	NSString * str_p_in_time_in_plan;
	NSString * str_p_out_time_in_plan;
	NSString * str_p_working_date;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_TimeValidELAndTotalWTModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns3_MsgNotifyModel * MsgNotify;
@property (retain) NSDate * P_ACTUAL_TIME_IN;
@property (retain) NSDate * P_ACTUAL_TIME_OUT;
@property (retain) NSDate * P_BREAK_TIME_INT;
@property (retain) NSDate * P_BREAK_TIME_OUT;
@property (retain) NSNumber * P_COMPANY_ID;
@property (retain) NSString * P_EMP_CODE;
@property (retain) NSDate * P_IN_TIME_IN_PLAN;
@property (retain) NSDate * P_OUT_TIME_IN_PLAN;
@property (retain) NSDate * P_WORKING_DATE;
@property (retain) NSString * P_WORKING_FORM;
@property (retain) NSNumber * V_BREAK_TIME_MINUTE;
@property (retain) NSNumber * V_EARLY_IN_TIME;
@property (retain) NSNumber * V_EARLY_OUT_TIME;
@property (retain) NSNumber * V_LATE_IN_TIME;
@property (retain) NSNumber * V_LATE_OUT_TIME;
@property (retain) NSNumber * V_TOTAL_WORKING_MINUTE;
@property (retain) NSString * str_p_actual_time_in;
@property (retain) NSString * str_p_actual_time_out;
@property (retain) NSString * str_p_break_time_int;
@property (retain) NSString * str_p_break_time_out;
@property (retain) NSString * str_p_in_time_in_plan;
@property (retain) NSString * str_p_out_time_in_plan;
@property (retain) NSString * str_p_working_date;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_TimeOTReqMstModel : NSObject {
	
/* elements */
	NSNumber * COMPANY_ID;
	NSNumber * CREATED_BY;
	NSDate * CREATED_DATE;
	USBoolean * IS_DELETED;
	NSNumber * MODIFYED_BY;
	NSDate * MODIFYED_DATE;
	NSString * OT_APPLY_EMP_CODE;
	NSDate * OT_APPROVE_DATE;
	NSString * OT_APPROVE_EMP_CODE;
	NSString * OT_CREATE_EMP_CODE;
	NSString * OT_DESCRIPTION;
	NSDate * OT_FROM_DATE;
	NSString * OT_REQUEST_CODE;
	NSNumber * OT_REQUEST_ID;
	NSNumber * OT_REQUEST_STATUS;
	NSDate * OT_TO_DATE;
	NSString * str_ot_approve_date;
	NSString * str_ot_from_date;
	NSString * str_ot_to_date;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_TimeOTReqMstModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSNumber * CREATED_BY;
@property (retain) NSDate * CREATED_DATE;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSNumber * MODIFYED_BY;
@property (retain) NSDate * MODIFYED_DATE;
@property (retain) NSString * OT_APPLY_EMP_CODE;
@property (retain) NSDate * OT_APPROVE_DATE;
@property (retain) NSString * OT_APPROVE_EMP_CODE;
@property (retain) NSString * OT_CREATE_EMP_CODE;
@property (retain) NSString * OT_DESCRIPTION;
@property (retain) NSDate * OT_FROM_DATE;
@property (retain) NSString * OT_REQUEST_CODE;
@property (retain) NSNumber * OT_REQUEST_ID;
@property (retain) NSNumber * OT_REQUEST_STATUS;
@property (retain) NSDate * OT_TO_DATE;
@property (retain) NSString * str_ot_approve_date;
@property (retain) NSString * str_ot_from_date;
@property (retain) NSString * str_ot_to_date;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_ArrayOfTimeOTReqMstModel : NSObject {
	
/* elements */
	NSMutableArray *TimeOTReqMstModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_ArrayOfTimeOTReqMstModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeOTReqMstModel:(time_tns3_TimeOTReqMstModel *)toAdd;
@property (readonly) NSMutableArray * TimeOTReqMstModel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_TimeOTReqEmpModel : NSObject {
	
/* elements */
	NSNumber * COMPANY_ID;
	NSNumber * CREATED_BY;
	NSDate * CREATED_DATE;
	NSString * EMP_CODE;
	NSString * EMP_FULL_NAME;
	NSString * EMP_GROUP_CODE;
	NSString * EMP_GROUP_NAME;
	NSString * EMP_ORG_CODE;
	NSString * EMP_ORG_NAME;
	USBoolean * IS_DELETED;
	NSNumber * MODIFYED_BY;
	NSDate * MODIFYED_DATE;
	NSNumber * OT_EMP_REQ_ID;
	NSString * OT_REQUEST_CODE;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_TimeOTReqEmpModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSNumber * CREATED_BY;
@property (retain) NSDate * CREATED_DATE;
@property (retain) NSString * EMP_CODE;
@property (retain) NSString * EMP_FULL_NAME;
@property (retain) NSString * EMP_GROUP_CODE;
@property (retain) NSString * EMP_GROUP_NAME;
@property (retain) NSString * EMP_ORG_CODE;
@property (retain) NSString * EMP_ORG_NAME;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSNumber * MODIFYED_BY;
@property (retain) NSDate * MODIFYED_DATE;
@property (retain) NSNumber * OT_EMP_REQ_ID;
@property (retain) NSString * OT_REQUEST_CODE;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_ArrayOfTimeOTReqEmpModel : NSObject {
	
/* elements */
	NSMutableArray *TimeOTReqEmpModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_ArrayOfTimeOTReqEmpModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeOTReqEmpModel:(time_tns3_TimeOTReqEmpModel *)toAdd;
@property (readonly) NSMutableArray * TimeOTReqEmpModel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_TimeOTReqDetailModel : NSObject {
	
/* elements */
	NSNumber * COMPANY_ID;
	NSNumber * CREATED_BY;
	NSDate * CREATED_DATE;
	USBoolean * IS_DELETED;
	NSNumber * MODIFYED_BY;
	NSDate * MODIFYED_DATE;
	NSDate * OT_DETAIL_FROM_TIME;
	NSNumber * OT_DETAIL_ID;
	NSDate * OT_DETAIL_TO_TIME;
	NSString * OT_REQUEST_CODE;
	NSString * OT_START_TYPE;
	NSString * OT_TYPE;
	NSString * str_ot_detail_from_time;
	NSString * str_ot_detail_to_time;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_TimeOTReqDetailModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSNumber * CREATED_BY;
@property (retain) NSDate * CREATED_DATE;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSNumber * MODIFYED_BY;
@property (retain) NSDate * MODIFYED_DATE;
@property (retain) NSDate * OT_DETAIL_FROM_TIME;
@property (retain) NSNumber * OT_DETAIL_ID;
@property (retain) NSDate * OT_DETAIL_TO_TIME;
@property (retain) NSString * OT_REQUEST_CODE;
@property (retain) NSString * OT_START_TYPE;
@property (retain) NSString * OT_TYPE;
@property (retain) NSString * str_ot_detail_from_time;
@property (retain) NSString * str_ot_detail_to_time;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_ArrayOfTimeOTReqDetailModel : NSObject {
	
/* elements */
	NSMutableArray *TimeOTReqDetailModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_ArrayOfTimeOTReqDetailModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeOTReqDetailModel:(time_tns3_TimeOTReqDetailModel *)toAdd;
@property (readonly) NSMutableArray * TimeOTReqDetailModel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_TimeParameter : NSObject {
	
/* elements */
	NSString * EmployeeCode;
	NSDate * EndDate;
	NSString * ParametorCode;
	NSDate * StartDate;
	NSNumber * Value;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_TimeParameter *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * EmployeeCode;
@property (retain) NSDate * EndDate;
@property (retain) NSString * ParametorCode;
@property (retain) NSDate * StartDate;
@property (retain) NSNumber * Value;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_ArrayOfTimeParameter : NSObject {
	
/* elements */
	NSMutableArray *TimeParameter;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_ArrayOfTimeParameter *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeParameter:(time_tns3_TimeParameter *)toAdd;
@property (readonly) NSMutableArray * TimeParameter;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_TimeOutputPayValueModel : NSObject {
	
/* elements */
	NSString * EMPLOYEE_CODE;
	NSString * GROUP_CODE;
	NSString * ORG_UNIT_CODE;
	NSNumber * PAY_VALUES;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_TimeOutputPayValueModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * EMPLOYEE_CODE;
@property (retain) NSString * GROUP_CODE;
@property (retain) NSString * ORG_UNIT_CODE;
@property (retain) NSNumber * PAY_VALUES;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_ArrayOfTimeOutputPayValueModel : NSObject {
	
/* elements */
	NSMutableArray *TimeOutputPayValueModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_ArrayOfTimeOutputPayValueModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeOutputPayValueModel:(time_tns3_TimeOutputPayValueModel *)toAdd;
@property (readonly) NSMutableArray * TimeOutputPayValueModel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_TimeCalculatedDataModel : NSObject {
	
/* elements */
	USBoolean * isValid;
	time_tns3_ArrayOfTimeOutputPayValueModel * lsOutputParam;
	NSString * strMsgResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_TimeCalculatedDataModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * isValid;
@property (retain) time_tns3_ArrayOfTimeOutputPayValueModel * lsOutputParam;
@property (retain) NSString * strMsgResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_TimeWRGMappingModel : NSObject {
	
/* elements */
	NSNumber * COMPANY_ID;
	NSNumber * CREATED_BY;
	NSDate * CREATED_DATE;
	USBoolean * IS_DELETED;
	NSNumber * MODIFIED_BY;
	NSDate * MODIFIED_DATE;
	NSString * WORKING_GROUP_CODE;
	NSString * WORKING_GROUP_EN;
	NSString * WORKING_GROUP_VN;
	NSString * WORKING_REGIME_CODE;
	NSString * WORKING_REGIME_EN;
	NSString * WORKING_REGIME_VN;
	NSNumber * WRG_MAPPING_ID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_TimeWRGMappingModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSNumber * CREATED_BY;
@property (retain) NSDate * CREATED_DATE;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSNumber * MODIFIED_BY;
@property (retain) NSDate * MODIFIED_DATE;
@property (retain) NSString * WORKING_GROUP_CODE;
@property (retain) NSString * WORKING_GROUP_EN;
@property (retain) NSString * WORKING_GROUP_VN;
@property (retain) NSString * WORKING_REGIME_CODE;
@property (retain) NSString * WORKING_REGIME_EN;
@property (retain) NSString * WORKING_REGIME_VN;
@property (retain) NSNumber * WRG_MAPPING_ID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_ArrayOfTimeWRGMappingModel : NSObject {
	
/* elements */
	NSMutableArray *TimeWRGMappingModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_ArrayOfTimeWRGMappingModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeWRGMappingModel:(time_tns3_TimeWRGMappingModel *)toAdd;
@property (readonly) NSMutableArray * TimeWRGMappingModel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_TimeInOutAdjustmentModel : NSObject {
	
/* elements */
	NSString * AT_INDEX;
	NSNumber * CHECKED;
	NSNumber * COMPANY_ID;
	NSNumber * CREATED_BY;
	NSDate * CREATED_DATE;
	NSString * EMP_AT_CODE;
	NSString * EMP_CODE;
	NSString * EMP_GROUP_CODE;
	NSNumber * IN_OUT_ID;
	NSDate * IN_TIME;
	USBoolean * IS_DELETED;
	NSNumber * IS_LEAVE;
	NSNumber * IS_OT;
	NSNumber * MODIFYED_BY;
	NSDate * MODIFYED_DATE;
	NSString * NOTE;
	NSDate * OUT_TIME;
	NSString * WAGE_TYPE;
	NSDate * WORKING_DATE;
	NSString * WORKING_FORM;
	NSString * str_in_time;
	NSString * str_out_time;
	NSString * str_working_date;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_TimeInOutAdjustmentModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AT_INDEX;
@property (retain) NSNumber * CHECKED;
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSNumber * CREATED_BY;
@property (retain) NSDate * CREATED_DATE;
@property (retain) NSString * EMP_AT_CODE;
@property (retain) NSString * EMP_CODE;
@property (retain) NSString * EMP_GROUP_CODE;
@property (retain) NSNumber * IN_OUT_ID;
@property (retain) NSDate * IN_TIME;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSNumber * IS_LEAVE;
@property (retain) NSNumber * IS_OT;
@property (retain) NSNumber * MODIFYED_BY;
@property (retain) NSDate * MODIFYED_DATE;
@property (retain) NSString * NOTE;
@property (retain) NSDate * OUT_TIME;
@property (retain) NSString * WAGE_TYPE;
@property (retain) NSDate * WORKING_DATE;
@property (retain) NSString * WORKING_FORM;
@property (retain) NSString * str_in_time;
@property (retain) NSString * str_out_time;
@property (retain) NSString * str_working_date;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_ArrayOfTimeInOutAdjustmentModel : NSObject {
	
/* elements */
	NSMutableArray *TimeInOutAdjustmentModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_ArrayOfTimeInOutAdjustmentModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeInOutAdjustmentModel:(time_tns3_TimeInOutAdjustmentModel *)toAdd;
@property (readonly) NSMutableArray * TimeInOutAdjustmentModel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_SetupWizardResultModel : NSObject {
	
/* elements */
	NSNumber * intSetupWizardQtyByNm;
	NSString * strSetupWizardConfirmedDate;
	NSString * strSetupWizardName;
	NSString * strSetupWizardPeriodByNm;
	NSString * strSetupWizardStatusByNm;
	NSString * strSetupWizardVersionNo;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_SetupWizardResultModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * intSetupWizardQtyByNm;
@property (retain) NSString * strSetupWizardConfirmedDate;
@property (retain) NSString * strSetupWizardName;
@property (retain) NSString * strSetupWizardPeriodByNm;
@property (retain) NSString * strSetupWizardStatusByNm;
@property (retain) NSString * strSetupWizardVersionNo;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_ArrayOfSetupWizardResultModel : NSObject {
	
/* elements */
	NSMutableArray *SetupWizardResultModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_ArrayOfSetupWizardResultModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSetupWizardResultModel:(time_tns3_SetupWizardResultModel *)toAdd;
@property (readonly) NSMutableArray * SetupWizardResultModel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_EventObjectCalendarBSModel : NSObject {
	
/* elements */
	USBoolean * allDay;
	NSString * backgroundColor;
	NSString * className;
	USBoolean * editable;
	NSString * end;
	NSString * id_;
	NSString * start;
	NSString * textColor;
	NSString * title;
	NSString * url;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_EventObjectCalendarBSModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * allDay;
@property (retain) NSString * backgroundColor;
@property (retain) NSString * className;
@property (retain) USBoolean * editable;
@property (retain) NSString * end;
@property (retain) NSString * id_;
@property (retain) NSString * start;
@property (retain) NSString * textColor;
@property (retain) NSString * title;
@property (retain) NSString * url;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_ArrayOfEventObjectCalendarBSModel : NSObject {
	
/* elements */
	NSMutableArray *EventObjectCalendarBSModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_ArrayOfEventObjectCalendarBSModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEventObjectCalendarBSModel:(time_tns3_EventObjectCalendarBSModel *)toAdd;
@property (readonly) NSMutableArray * EventObjectCalendarBSModel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_SelfServiceEmployeeScreenModel : NSObject {
	
/* elements */
	NSNumber * NumOfEquipmentRequestNotApproved;
	NSNumber * NumOfLateEarlyNotApproved;
	NSNumber * NumOfLeaveRequestNotApproved;
	NSNumber * TotalLeaveRegim;
	NSNumber * TotalRemainedLeave;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_SelfServiceEmployeeScreenModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * NumOfEquipmentRequestNotApproved;
@property (retain) NSNumber * NumOfLateEarlyNotApproved;
@property (retain) NSNumber * NumOfLeaveRequestNotApproved;
@property (retain) NSNumber * TotalLeaveRegim;
@property (retain) NSNumber * TotalRemainedLeave;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_EmployeeInfoRequest : NSObject {
	
/* elements */
	NSNumber * Count;
	NSString * Description;
	NSString * EmployeeName;
	NSString * ImageUrl;
	NSString * JobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_EmployeeInfoRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Count;
@property (retain) NSString * Description;
@property (retain) NSString * EmployeeName;
@property (retain) NSString * ImageUrl;
@property (retain) NSString * JobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_ArrayOfEmployeeInfoRequest : NSObject {
	
/* elements */
	NSMutableArray *EmployeeInfoRequest;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_ArrayOfEmployeeInfoRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmployeeInfoRequest:(time_tns3_EmployeeInfoRequest *)toAdd;
@property (readonly) NSMutableArray * EmployeeInfoRequest;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_SelfServiceManagerScreenModel : NSObject {
	
/* elements */
	time_tns3_EmployeeInfoRequest * LastedEquipmentRequest;
	time_tns3_EmployeeInfoRequest * LastedLeaveRequest;
	time_tns3_ArrayOfEmployeeInfoRequest * LstEmpLateEarlyRequest;
	NSNumber * NumOfEquipmentRequestNeedApproved;
	NSNumber * NumOfEquipmentRequestNotApproved;
	NSNumber * NumOfLateEarlyNeedApproved;
	NSNumber * NumOfLateEarlyNotApproved;
	NSNumber * NumOfLeaveRequestNeedApproved;
	NSNumber * NumOfLeaveRequestNotApproved;
	NSNumber * TotalLeaveRegim;
	NSNumber * TotalRemainedLeave;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_SelfServiceManagerScreenModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns3_EmployeeInfoRequest * LastedEquipmentRequest;
@property (retain) time_tns3_EmployeeInfoRequest * LastedLeaveRequest;
@property (retain) time_tns3_ArrayOfEmployeeInfoRequest * LstEmpLateEarlyRequest;
@property (retain) NSNumber * NumOfEquipmentRequestNeedApproved;
@property (retain) NSNumber * NumOfEquipmentRequestNotApproved;
@property (retain) NSNumber * NumOfLateEarlyNeedApproved;
@property (retain) NSNumber * NumOfLateEarlyNotApproved;
@property (retain) NSNumber * NumOfLeaveRequestNeedApproved;
@property (retain) NSNumber * NumOfLeaveRequestNotApproved;
@property (retain) NSNumber * TotalLeaveRegim;
@property (retain) NSNumber * TotalRemainedLeave;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_TimeOTCoefficientModel : NSObject {
	
/* elements */
	NSString * COEFFICIENT;
	NSNumber * COMPANY_ID;
	USBoolean * IS_DELETED;
	NSString * NAME_EN;
	NSString * NAME_VN;
	NSString * OT_CODE;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_TimeOTCoefficientModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * COEFFICIENT;
@property (retain) NSNumber * COMPANY_ID;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSString * NAME_EN;
@property (retain) NSString * NAME_VN;
@property (retain) NSString * OT_CODE;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_ArrayOfTimeOTCoefficientModel : NSObject {
	
/* elements */
	NSMutableArray *TimeOTCoefficientModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_ArrayOfTimeOTCoefficientModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeOTCoefficientModel:(time_tns3_TimeOTCoefficientModel *)toAdd;
@property (readonly) NSMutableArray * TimeOTCoefficientModel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_TimeAllowanceModel : NSObject {
	
/* elements */
	NSString * COEFFICIENT;
	NSNumber * COMPANY_ID;
	USBoolean * IS_DELETED;
	NSString * NAME_EN;
	NSString * NAME_VN;
	NSString * OT_CODE;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_TimeAllowanceModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * COEFFICIENT;
@property (retain) NSNumber * COMPANY_ID;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSString * NAME_EN;
@property (retain) NSString * NAME_VN;
@property (retain) NSString * OT_CODE;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_ArrayOfTimeAllowanceModel : NSObject {
	
/* elements */
	NSMutableArray *TimeAllowanceModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_ArrayOfTimeAllowanceModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeAllowanceModel:(time_tns3_TimeAllowanceModel *)toAdd;
@property (readonly) NSMutableArray * TimeAllowanceModel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_TimeReqInOutModel : NSObject {
	
/* elements */
	NSNumber * COMPANY_ID;
	USBoolean * IS_DELETED;
	NSString * NAME_EN;
	NSString * NAME_VN;
	NSString * OT_CODE;
	NSString * REQ_MINUTES;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_TimeReqInOutModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * COMPANY_ID;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSString * NAME_EN;
@property (retain) NSString * NAME_VN;
@property (retain) NSString * OT_CODE;
@property (retain) NSString * REQ_MINUTES;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_ArrayOfTimeReqInOutModel : NSObject {
	
/* elements */
	NSMutableArray *TimeReqInOutModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_ArrayOfTimeReqInOutModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeReqInOutModel:(time_tns3_TimeReqInOutModel *)toAdd;
@property (readonly) NSMutableArray * TimeReqInOutModel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_TimeReqInOutStatusModel : NSObject {
	
/* elements */
	NSNumber * COMPANY_ID;
	USBoolean * IS_DELETED;
	NSString * NAME_EN;
	NSString * NAME_VN;
	NSString * OT_CODE;
	NSString * OT_VALUES;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_TimeReqInOutStatusModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * COMPANY_ID;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSString * NAME_EN;
@property (retain) NSString * NAME_VN;
@property (retain) NSString * OT_CODE;
@property (retain) NSString * OT_VALUES;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_ArrayOfTimeReqInOutStatusModel : NSObject {
	
/* elements */
	NSMutableArray *TimeReqInOutStatusModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_ArrayOfTimeReqInOutStatusModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeReqInOutStatusModel:(time_tns3_TimeReqInOutStatusModel *)toAdd;
@property (readonly) NSMutableArray * TimeReqInOutStatusModel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_TimeLogRuleModel : NSObject {
	
/* elements */
	NSNumber * AFTER_BREAK_TIME_IN;
	NSNumber * AFTER_TIME_IN;
	NSNumber * AFTER_TIME_OUT;
	NSNumber * BEFORE_BREAK_TIME_OUT;
	NSNumber * BEFORE_TIME_IN;
	NSNumber * BEFORE_TIME_OUT;
	NSNumber * COMPANY_ID;
	USBoolean * IS_DELETED;
	NSString * NAME_EN;
	NSString * NAME_VN;
	NSString * TIME_LOG_CODE;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_TimeLogRuleModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AFTER_BREAK_TIME_IN;
@property (retain) NSNumber * AFTER_TIME_IN;
@property (retain) NSNumber * AFTER_TIME_OUT;
@property (retain) NSNumber * BEFORE_BREAK_TIME_OUT;
@property (retain) NSNumber * BEFORE_TIME_IN;
@property (retain) NSNumber * BEFORE_TIME_OUT;
@property (retain) NSNumber * COMPANY_ID;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSString * NAME_EN;
@property (retain) NSString * NAME_VN;
@property (retain) NSString * TIME_LOG_CODE;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_ArrayOfTimeLogRuleModel : NSObject {
	
/* elements */
	NSMutableArray *TimeLogRuleModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_ArrayOfTimeLogRuleModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeLogRuleModel:(time_tns3_TimeLogRuleModel *)toAdd;
@property (readonly) NSMutableArray * TimeLogRuleModel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_TimeAllowInOutModel : NSObject {
	
/* elements */
	NSNumber * ALLOW_IN_EARLY;
	NSNumber * ALLOW_IN_LATE;
	NSNumber * ALLOW_OUT_EARLY;
	NSNumber * ALLOW_OUT_LATE;
	NSNumber * COMPANY_ID;
	USBoolean * IN_TOO_LATE_IS_LEAVE;
	USBoolean * IS_DELETED;
	NSNumber * MINUTE_IN_TOO_LATE;
	NSNumber * MINUTE_OUT_TOO_EARLY;
	NSString * NAME_EN;
	NSString * NAME_VN;
	NSString * OT_CODE;
	USBoolean * OUT_TOO_EARLY_IS_LEAVE;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_TimeAllowInOutModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ALLOW_IN_EARLY;
@property (retain) NSNumber * ALLOW_IN_LATE;
@property (retain) NSNumber * ALLOW_OUT_EARLY;
@property (retain) NSNumber * ALLOW_OUT_LATE;
@property (retain) NSNumber * COMPANY_ID;
@property (retain) USBoolean * IN_TOO_LATE_IS_LEAVE;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSNumber * MINUTE_IN_TOO_LATE;
@property (retain) NSNumber * MINUTE_OUT_TOO_EARLY;
@property (retain) NSString * NAME_EN;
@property (retain) NSString * NAME_VN;
@property (retain) NSString * OT_CODE;
@property (retain) USBoolean * OUT_TOO_EARLY_IS_LEAVE;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_ArrayOfTimeAllowInOutModel : NSObject {
	
/* elements */
	NSMutableArray *TimeAllowInOutModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_ArrayOfTimeAllowInOutModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeAllowInOutModel:(time_tns3_TimeAllowInOutModel *)toAdd;
@property (readonly) NSMutableArray * TimeAllowInOutModel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_TimeOTStartTypeModel : NSObject {
	
/* elements */
	NSNumber * COMPANY_ID;
	USBoolean * IS_DELETED;
	NSString * NAME_EN;
	NSString * NAME_VN;
	NSString * OT_START_CODE;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_TimeOTStartTypeModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * COMPANY_ID;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSString * NAME_EN;
@property (retain) NSString * NAME_VN;
@property (retain) NSString * OT_START_CODE;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns3_ArrayOfTimeOTStartTypeModel : NSObject {
	
/* elements */
	NSMutableArray *TimeOTStartTypeModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns3_ArrayOfTimeOTStartTypeModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeOTStartTypeModel:(time_tns3_TimeOTStartTypeModel *)toAdd;
@property (readonly) NSMutableArray * TimeOTStartTypeModel;
/* attributes */
- (NSDictionary *)attributes;
@end
