#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class time_tns1_ArrayOfV_ESS_GET_WORKING_SHIFT;
@class time_tns1_V_ESS_GET_WORKING_SHIFT;
@class time_tns1_ArrayOfTimeWorkingForm;
@class time_tns1_TimeWorkingForm;
@class time_tns1_ArrayOfSP_ESS_GET_WORKINGSHIFT_BY_CON_Result;
@class time_tns1_SP_ESS_GET_WORKINGSHIFT_BY_CON_Result;
@class time_tns1_ArrayOfV_TimeLeaveRequest;
@class time_tns1_V_TimeLeaveRequest;
@class time_tns1_ArrayOfV_EmpBasicInformation;
@class time_tns1_V_EmpBasicInformation;
@class time_tns1_ArrayOfV_TimeLaterEarly;
@class time_tns1_V_TimeLaterEarly;
@class time_tns1_ArrayOfTimeWageType;
@class time_tns1_TimeWageType;
@class time_tns1_ArrayOfV_TimeDateOfSalaryCalculationForm;
@class time_tns1_V_TimeDateOfSalaryCalculationForm;
@class time_tns1_TimeDateOfSalaryCalculation;
@class time_tns1_TimeSysConfigParam;
@class time_tns1_ArrayOfSP_ESS_USER_REQUISITION_LIST_Result;
@class time_tns1_SP_ESS_USER_REQUISITION_LIST_Result;
@class time_tns1_ArrayOfTB_ESS_SYSTEM_PARAMETER;
@class time_tns1_TB_ESS_SYSTEM_PARAMETER;
@class time_tns1_ArrayOfSP_ESS_GET_APPROVE_ORG_Result;
@class time_tns1_SP_ESS_GET_APPROVE_ORG_Result;
@class time_tns1_ArrayOfTimeLeaveRequestDetail;
@class time_tns1_TimeLeaveRequestDetail;
@class time_tns1_EmpProfileLeaveRegime;
@class time_tns1_Employee;
@class time_tns1_ArrayOfCBAccidentInsurance;
@class time_tns1_ArrayOfCBCompensationEmployeeGroupDetail;
@class time_tns1_ArrayOfCBConvalescence;
@class time_tns1_ArrayOfCBDayOffSocialInsurance;
@class time_tns1_ArrayOfCBFactorEmployeeMetaData;
@class time_tns1_ArrayOfCBHealthInsuranceDetail;
@class time_tns1_CLogCity;
@class time_tns1_CLogEmployeeType;
@class time_tns1_ArrayOfCLogTrainer;
@class time_tns1_ArrayOfEmpBasicProfile;
@class time_tns1_ArrayOfEmpCompetency;
@class time_tns1_ArrayOfEmpCompetencyRating;
@class time_tns1_ArrayOfEmpContract;
@class time_tns1_ArrayOfEmpPerformanceAppraisal;
@class time_tns1_ArrayOfEmpProfileAllowance;
@class time_tns1_ArrayOfEmpProfileBaseSalary;
@class time_tns1_ArrayOfEmpProfileBenefit;
@class time_tns1_ArrayOfEmpProfileComment;
@class time_tns1_ArrayOfEmpProfileComputingSkill;
@class time_tns1_ArrayOfEmpProfileContact;
@class time_tns1_ArrayOfEmpProfileDegree;
@class time_tns1_ArrayOfEmpProfileDiscipline;
@class time_tns1_ArrayOfEmpProfileEducation;
@class time_tns1_ArrayOfEmpProfileEquipment;
@class time_tns1_ArrayOfEmpProfileExperience;
@class time_tns1_ArrayOfEmpProfileFamilyRelationship;
@class time_tns1_ArrayOfEmpProfileForeignLanguage;
@class time_tns1_ArrayOfEmpProfileHealthInsurance;
@class time_tns1_ArrayOfEmpProfileHealthy;
@class time_tns1_ArrayOfEmpProfileHobby;
@class time_tns1_ArrayOfEmpProfileInternalCourse;
@class time_tns1_ArrayOfEmpProfileJobPosition;
@class time_tns1_ArrayOfEmpProfileLeaveRegime;
@class time_tns1_ArrayOfEmpProfileOtherBenefit;
@class time_tns1_ArrayOfEmpProfileParticipation;
@class time_tns1_ArrayOfEmpProfilePersonality;
@class time_tns1_ArrayOfEmpProfileProcessOfWork;
@class time_tns1_ArrayOfEmpProfileQualification;
@class time_tns1_ArrayOfEmpProfileReward;
@class time_tns1_ArrayOfEmpProfileSkill;
@class time_tns1_ArrayOfEmpProfileTotalIncome;
@class time_tns1_ArrayOfEmpProfileTraining;
@class time_tns1_ArrayOfEmpProfileWageType;
@class time_tns1_ArrayOfEmpProfileWorkingExperience;
@class time_tns1_ArrayOfEmpProfileWorkingForm;
@class time_tns1_ArrayOfEmpSocialInsuranceSalary;
@class time_tns1_ArrayOfGPAdditionAppraisal;
@class time_tns1_ArrayOfGPCompanyScoreCard;
@class time_tns1_ArrayOfGPCompanyStrategicGoal;
@class time_tns1_ArrayOfGPCompanyYearlyObjective;
@class time_tns1_ArrayOfGPEmployeeScoreCard;
@class time_tns1_ArrayOfGPIndividualYearlyObjective;
@class time_tns1_ArrayOfGPObjectiveInitiative;
@class time_tns1_ArrayOfGPPerformanceYearEndResult;
@class time_tns1_ArrayOfLMSCourseAttendee;
@class time_tns1_ArrayOfLMSCourseTranscript;
@class time_tns1_ArrayOfNCClientConnection;
@class time_tns1_ArrayOfNCMessage;
@class time_tns1_ArrayOfProjectMember;
@class time_tns1_ArrayOfProject;
@class time_tns1_ArrayOfRecInterviewSchedule;
@class time_tns1_ArrayOfRecInterviewer;
@class time_tns1_ArrayOfRecPhaseEmpDisplaced;
@class time_tns1_ArrayOfRecRecruitmentRequirement;
@class time_tns1_ArrayOfRecRequirementEmpDisplaced;
@class time_tns1_ReportEmployeeRequestedBook;
@class time_tns1_ArrayOfSelfLeaveRequest;
@class time_tns1_ArrayOfSysRecPlanApprover;
@class time_tns1_ArrayOfSysRecRequirementApprover;
@class time_tns1_ArrayOfSysTrainingApprover;
@class time_tns1_ArrayOfTask;
@class time_tns1_ArrayOfTrainingCourseEmployee;
@class time_tns1_ArrayOfTrainingEmpPlan;
@class time_tns1_ArrayOfTrainingPlanEmployee;
@class time_tns1_CBAccidentInsurance;
@class time_tns1_CBCompensationEmployeeGroupDetail;
@class time_tns1_CBCompensationEmployeeGroup;
@class time_tns1_CBFactorEmployeeMetaData;
@class time_tns1_CLogCBFactor;
@class time_tns1_ArrayOfCBCompensationTypeFactor;
@class time_tns1_CLogCBCompensationCategory;
@class time_tns1_CBCompensationTypeFactor;
@class time_tns1_ArrayOfCLogCBFactor;
@class time_tns1_CBConvalescence;
@class time_tns1_CBDayOffSocialInsurance;
@class time_tns1_CLogCBDisease;
@class time_tns1_CLogHospital;
@class time_tns1_ArrayOfCBHealthInsurance;
@class time_tns1_CBHealthInsurance;
@class time_tns1_CLogCountry;
@class time_tns1_ArrayOfCLogDistrict;
@class time_tns1_ArrayOfCLogHospital;
@class time_tns1_ArrayOfEmployee;
@class time_tns1_ArrayOfCLogCity;
@class time_tns1_CLogDistrict;
@class time_tns1_CBHealthInsuranceDetail;
@class time_tns1_CLogTrainer;
@class time_tns1_Address;
@class time_tns1_ArrayOfTrainingCourse;
@class time_tns1_ArrayOfOrgUnit;
@class time_tns1_EmpContract;
@class time_tns1_Employer;
@class time_tns1_OrgJob;
@class time_tns1_OrgJobPosition;
@class time_tns1_ArrayOfOrgJobPosition;
@class time_tns1_ArrayOfOrgJobSpecificCompetency;
@class time_tns1_ArrayOfOrgUnitJob;
@class time_tns1_ArrayOfOrgJobPositionRequiredProficency;
@class time_tns1_ArrayOfOrgJobPositionSpecificCompetency;
@class time_tns1_ArrayOfRecPlanJobPosition;
@class time_tns1_ArrayOfRecProcessApplyForJobPosition;
@class time_tns1_ArrayOfRecRecruitmentPhaseJobPosition;
@class time_tns1_ArrayOfRecRequirementJobPosition;
@class time_tns1_EmpProfileJobPosition;
@class time_tns1_OrgJobPositionRequiredProficency;
@class time_tns1_OrgJobPositionSpecificCompetency;
@class time_tns1_OrgProficencyLevel;
@class time_tns1_CLogRating;
@class time_tns1_ArrayOfEmpProficencyLevelRating;
@class time_tns1_OrgCompetency;
@class time_tns1_ArrayOfOrgProficencyLevelDetail;
@class time_tns1_ArrayOfRecCandidateProficencyLevelRating;
@class time_tns1_ArrayOfEmpProficencyDetailRating;
@class time_tns1_ArrayOfEmpProficencyLevelDetailRating;
@class time_tns1_ArrayOfLMSCourseCompetency;
@class time_tns1_ArrayOfLMSCourseRequiredCompetency;
@class time_tns1_ArrayOfOrgCoreCompetency;
@class time_tns1_ArrayOfOrgProficencyDetail;
@class time_tns1_ArrayOfOrgProficencyLevel;
@class time_tns1_ArrayOfRecCanProficencyDetailRating;
@class time_tns1_ArrayOfRecCanProficencyLevelDetailRating;
@class time_tns1_ArrayOfRecCandidateCompetencyRating;
@class time_tns1_ArrayOfRecCandidateExperience;
@class time_tns1_ArrayOfRecCandidateQualification;
@class time_tns1_ArrayOfRecCandidateSkill;
@class time_tns1_ArrayOfTrainingEmpProficency;
@class time_tns1_ArrayOfTrainingProficencyExpected;
@class time_tns1_ArrayOfTrainingProficencyRequire;
@class time_tns1_EmpCompetency;
@class time_tns1_OrgCoreCompetency;
@class time_tns1_EmpProficencyDetailRating;
@class time_tns1_EmpProficencyLevelDetailRating;
@class time_tns1_OrgProficencyDetail;
@class time_tns1_EmpProficencyLevelRating;
@class time_tns1_OrgProficencyLevelDetail;
@class time_tns1_EmpCompetencyRating;
@class time_tns1_OrgProficencyType;
@class time_tns1_RecCanProficencyDetailRating;
@class time_tns1_RecCanProficencyLevelDetailRating;
@class time_tns1_RecCandidateProficencyLevelRating;
@class time_tns1_RecCandidateCompetencyRating;
@class time_tns1_RecCandidate;
@class time_tns1_ArrayOfRecCandidateApplication;
@class time_tns1_ArrayOfRecCandidateDegree;
@class time_tns1_ArrayOfRecCandidateFamilyRelationship;
@class time_tns1_ArrayOfRecCandidateForeignLanguage;
@class time_tns1_RecCandidateStatu;
@class time_tns1_RecCandidateSupplier;
@class time_tns1_ArrayOfRecProcessPhaseResult;
@class time_tns1_RecCandidateApplication;
@class time_tns1_RecCandidateProfileStatu;
@class time_tns1_RecRecruitmentPhaseJobPosition;
@class time_tns1_RecInterviewSchedule;
@class time_tns1_RecGroupInterviewer;
@class time_tns1_RecInterviewPhase;
@class time_tns1_RecInterviewScheduleStatu;
@class time_tns1_ArrayOfRecScheduleInterviewerResult;
@class time_tns1_RecInterviewer;
@class time_tns1_RecScheduleInterviewerResult;
@class time_tns1_RecInterviewPhaseEvaluation;
@class time_tns1_RecEvaluationCriterion;
@class time_tns1_RecGroupEvaluationCriterion;
@class time_tns1_ArrayOfRecInterviewPhaseEvaluation;
@class time_tns1_ArrayOfRecEvaluationCriterion;
@class time_tns1_RecRecruitmentProcessPhase;
@class time_tns1_ArrayOfRecInterviewPhase;
@class time_tns1_RecRecruitmentProcess;
@class time_tns1_RecProcessPhaseResult;
@class time_tns1_ArrayOfRecRecruitmentProcessPhase;
@class time_tns1_RecProcessApplyForJobPosition;
@class time_tns1_RecRecruitmentPhase;
@class time_tns1_RecPhaseStatu;
@class time_tns1_RecRecruitmentRequirement;
@class time_tns1_RecPhaseEmpDisplaced;
@class time_tns1_ArrayOfRecRecruitmentPhase;
@class time_tns1_RecRecruitmentPlan;
@class time_tns1_ArrayOfRecRequirementApproveHistory;
@class time_tns1_RecRequirementStatu;
@class time_tns1_ArrayOfRecPlanApproveHistory;
@class time_tns1_RecPlanStatu;
@class time_tns1_RecPlanApproveHistory;
@class time_tns1_SysRecPlanApprover;
@class time_tns1_RecPlanJobPosition;
@class time_tns1_ArrayOfRecRecruitmentPlan;
@class time_tns1_RecRequirementApproveHistory;
@class time_tns1_SysRecRequirementApprover;
@class time_tns1_RecRequirementEmpDisplaced;
@class time_tns1_RecRequirementJobPosition;
@class time_tns1_RecCandidateDegree;
@class time_tns1_CLogDegree;
@class time_tns1_RecCandidateExperience;
@class time_tns1_OrgExperience;
@class time_tns1_OrgProjectType;
@class time_tns1_OrgTimeInCharge;
@class time_tns1_ArrayOfOrgExperience;
@class time_tns1_RecCandidateFamilyRelationship;
@class time_tns1_CLogFamilyRelationship;
@class time_tns1_EmpProfileFamilyRelationship;
@class time_tns1_ArrayOfCBPITDependent;
@class time_tns1_ArrayOfEmpBeneficiary;
@class time_tns1_CBPITDependent;
@class time_tns1_EmpBeneficiary;
@class time_tns1_RecCandidateForeignLanguage;
@class time_tns1_RecCandidateQualification;
@class time_tns1_OrgQualification;
@class time_tns1_EmpProfileQualification;
@class time_tns1_RecCandidateSkill;
@class time_tns1_OrgSkill;
@class time_tns1_OrgSkillType;
@class time_tns1_EmpProfileSkill;
@class time_tns1_ArrayOfOrgSkill;
@class time_tns1_TrainingEmpProficency;
@class time_tns1_TrainingCourseEmployee;
@class time_tns1_TrainingCourseSchedule;
@class time_tns1_CLogCourseSchedule;
@class time_tns1_TrainingCourse;
@class time_tns1_ArrayOfTrainingCourseSchedule;
@class time_tns1_CLogTrainingCenter;
@class time_tns1_TrainingCategory;
@class time_tns1_ArrayOfTrainingCourseChapter;
@class time_tns1_TrainingCoursePeriod;
@class time_tns1_TrainingCourseType;
@class time_tns1_ArrayOfTrainingCourseUnit;
@class time_tns1_ArrayOfTrainingPlanDegree;
@class time_tns1_TrainingPlanRequest;
@class time_tns1_TrainingCourseChapter;
@class time_tns1_TrainingCourseUnit;
@class time_tns1_TrainingPlanDegree;
@class time_tns1_CLogMajor;
@class time_tns1_ArrayOfTrainingPlanRequest;
@class time_tns1_EmpProfileEducation;
@class time_tns1_TrainingPlanEmployee;
@class time_tns1_TrainingProficencyExpected;
@class time_tns1_TrainingProficencyRequire;
@class time_tns1_ArrayOfRecCandidate;
@class time_tns1_RecCandidateTypeSupplier;
@class time_tns1_ArrayOfRecCandidateSupplier;
@class time_tns1_ArrayOfTrainingPlanProfiency;
@class time_tns1_TrainingPlanProfiency;
@class time_tns1_EmpProfileWorkingExperience;
@class time_tns1_LMSCourseAttendee;
@class time_tns1_LMSCourse;
@class time_tns1_CLogCourseStatu;
@class time_tns1_GPCompanyYearlyObjective;
@class time_tns1_GPObjectiveInitiative;
@class time_tns1_LMSContentTopic;
@class time_tns1_ArrayOfLMSCourseContent;
@class time_tns1_ArrayOfLMSCourseDetail;
@class time_tns1_ArrayOfLMSCourseProgressUnit;
@class time_tns1_LMSCourseType;
@class time_tns1_ArrayOfLMSCourse;
@class time_tns1_GPCompanyStrategicGoal;
@class time_tns1_GPPerspective;
@class time_tns1_GPStrategy;
@class time_tns1_GPCompanyScoreCard;
@class time_tns1_GPPerspectiveMeasure;
@class time_tns1_GPEmployeeScoreCard;
@class time_tns1_ArrayOfGPEmployeeScoreCardAppraisal;
@class time_tns1_ArrayOfGPEmployeeScoreCardProgress;
@class time_tns1_GPPerformanceExecutionReport;
@class time_tns1_GPEmployeeScoreCardAppraisal;
@class time_tns1_GPEmployeeScoreCardProgress;
@class time_tns1_GPIndividualYearlyObjective;
@class time_tns1_ArrayOfGPExecutionPlanDetail;
@class time_tns1_ArrayOfGPIndividualYearlyObjectiveAppraisal;
@class time_tns1_GPExecutionPlanDetail;
@class time_tns1_GPExecutionPlan;
@class time_tns1_ArrayOfGPExecutionPlanActivity;
@class time_tns1_ArrayOfGPEmployeeWholePlanAppraisal;
@class time_tns1_GPAdditionAppraisal;
@class time_tns1_GPEmployeeWholePlanAppraisal;
@class time_tns1_GPPerformanceYearEndResult;
@class time_tns1_GPExecutionPlanActivity;
@class time_tns1_ArrayOfGPExecutionPlanActivityAppraisal;
@class time_tns1_ArrayOfGPPeriodicReport;
@class time_tns1_GPExecutionPlanActivityAppraisal;
@class time_tns1_GPPeriodicReport;
@class time_tns1_GPIndividualYearlyObjectiveAppraisal;
@class time_tns1_ArrayOfGPPerspectiveMeasure;
@class time_tns1_ArrayOfGPPerspectiveValue;
@class time_tns1_ArrayOfGPCompanyStrategicGoalDetail;
@class time_tns1_GPStrategyMapObject;
@class time_tns1_GPCompanyStrategicGoalDetail;
@class time_tns1_GPPerspectiveValue;
@class time_tns1_ArrayOfGPPerspectiveValueDetail;
@class time_tns1_GPPerspectiveValueDetail;
@class time_tns1_ArrayOfGPStrategyMapObject;
@class time_tns1_ArrayOfLMSTopicCompetency;
@class time_tns1_LMSTopicGroup;
@class time_tns1_LMSTopicCompetency;
@class time_tns1_OrgCompetencyGroup;
@class time_tns1_LMSCourseCompetency;
@class time_tns1_LMSCourseRequiredCompetency;
@class time_tns1_ArrayOfOrgCompetency;
@class time_tns1_OrgJobSpecificCompetency;
@class time_tns1_ArrayOfLMSContentTopic;
@class time_tns1_LMSCourseContent;
@class time_tns1_CLogLMSContentType;
@class time_tns1_LMSCourseDetail;
@class time_tns1_LMSCourseProgressUnit;
@class time_tns1_OrgUnit;
@class time_tns1_LMSCourseTranscript;
@class time_tns1_OrgUnitJob;
@class time_tns1_EmpBasicProfile;
@class time_tns1_CLogEmpWorkingStatu;
@class time_tns1_EmpPerformanceAppraisal;
@class time_tns1_EmpProfileAllowance;
@class time_tns1_EmpProfileBaseSalary;
@class time_tns1_EmpProfileBenefit;
@class time_tns1_EmpProfileComment;
@class time_tns1_EmpProfileComputingSkill;
@class time_tns1_EmpProfileContact;
@class time_tns1_EmpProfileDegree;
@class time_tns1_OrgDegree;
@class time_tns1_EmpProfileDiscipline;
@class time_tns1_OrgDisciplineMaster;
@class time_tns1_ArrayOfOrgDisciplineForUnit;
@class time_tns1_OrgDisciplineForUnit;
@class time_tns1_EmpProfileEquipment;
@class time_tns1_CLogEquipment;
@class time_tns1_EmpProfileExperience;
@class time_tns1_EmpProfileForeignLanguage;
@class time_tns1_CLogLanguage;
@class time_tns1_EmpProfileHealthInsurance;
@class time_tns1_EmpProfileHealthy;
@class time_tns1_EmpProfileHobby;
@class time_tns1_CLogHobby;
@class time_tns1_EmpProfileInternalCourse;
@class time_tns1_EmpProfileOtherBenefit;
@class time_tns1_EmpOtherBenefit;
@class time_tns1_EmpProfileParticipation;
@class time_tns1_EmpProfilePersonality;
@class time_tns1_CLogPersonality;
@class time_tns1_EmpProfileProcessOfWork;
@class time_tns1_EmpProfileReward;
@class time_tns1_OrgRewardMaster;
@class time_tns1_ArrayOfOrgRewardForUnit;
@class time_tns1_OrgRewardForUnit;
@class time_tns1_EmpProfileTotalIncome;
@class time_tns1_EmpProfileTraining;
@class time_tns1_EmpProfileWageType;
@class time_tns1_EmpProfileWorkingForm;
@class time_tns1_EmpSocialInsuranceSalary;
@class time_tns1_NCClientConnection;
@class time_tns1_NCMessage;
@class time_tns1_NCMessageType;
@class time_tns1_ProjectMember;
@class time_tns1_Project;
@class time_tns1_SelfLeaveRequest;
@class time_tns1_SysTrainingApprover;
@class time_tns1_ArrayOfTrainingEmpPlanApproved;
@class time_tns1_TrainingEmpPlanApproved;
@class time_tns1_TrainingEmpPlan;
@class time_tns1_Task;
@class time_tns1_ArrayOfTimeRequestMasTer;
@class time_tns1_TimeRequestMasTer;
@class time_tns1_ArrayOfTB_ESS_TIME_REQ_APP_MAPPING;
@class time_tns1_TB_ESS_TIME_REQ_APP_MAPPING;
@class time_tns1_ArrayOfSP_ESS_USER_REQ_EQUIP_Result;
@class time_tns1_SP_ESS_USER_REQ_EQUIP_Result;
@class time_tns1_ArrayOfSP_ESS_USER_APP_REQ_EQUIP_Result;
@class time_tns1_SP_ESS_USER_APP_REQ_EQUIP_Result;
@class time_tns1_ArrayOfTimeSysConfigParam;
@class time_tns1_ArrayOfV_OrgUnit;
@class time_tns1_V_OrgUnit;
@class time_tns1_SP_ESS_GET_EMP_HOLIDAY_Result;
@class time_tns1_ArrayOfV_EMP_CONTRACT_WOKING_HOURS;
@class time_tns1_V_EMP_CONTRACT_WOKING_HOURS;
@class time_tns1_ArrayOfTimeDailyWorkSheet;
@class time_tns1_TimeDailyWorkSheet;
@class time_tns1_ArrayOfTimeLeaveCalculationForm;
@class time_tns1_TimeLeaveCalculationForm;
@class time_tns1_ArrayOfTimeEmployeeNotCheckWorkingSheet;
@class time_tns1_TimeEmployeeNotCheckWorkingSheet;
@class time_tns1_ArrayOfTimeEmployeeWorkingDifferentTime;
@class time_tns1_TimeEmployeeWorkingDifferentTime;
#import "time_tns4.h"
@interface time_tns1_V_ESS_GET_WORKING_SHIFT : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * D01;
	NSString * D02;
	NSString * D03;
	NSString * D04;
	NSString * D05;
	NSString * D06;
	NSString * D07;
	NSString * D08;
	NSString * D09;
	NSString * D10;
	NSString * D11;
	NSString * D12;
	NSString * D13;
	NSString * D14;
	NSString * D15;
	NSString * D16;
	NSString * D17;
	NSString * D18;
	NSString * D19;
	NSString * D20;
	NSString * D21;
	NSString * D22;
	NSString * D23;
	NSString * D24;
	NSString * D25;
	NSString * D26;
	NSString * D27;
	NSString * D28;
	NSString * D29;
	NSString * D30;
	NSString * D31;
	NSString * Day01;
	NSString * Day02;
	NSString * Day03;
	NSString * Day04;
	NSString * Day05;
	NSString * Day06;
	NSString * Day07;
	NSString * Day08;
	NSString * Day09;
	NSString * Day10;
	NSString * Day11;
	NSString * Day12;
	NSString * Day13;
	NSString * Day14;
	NSString * Day15;
	NSString * Day16;
	NSString * Day17;
	NSString * Day18;
	NSString * Day19;
	NSString * Day20;
	NSString * Day21;
	NSString * Day22;
	NSString * Day23;
	NSString * Day24;
	NSString * Day25;
	NSString * Day26;
	NSString * Day27;
	NSString * Day28;
	NSString * Day29;
	NSString * Day30;
	NSString * Day31;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * EmployeeName;
	NSString * GroupCode;
	NSString * GroupName;
	USBoolean * IsDeleted;
	USBoolean * IsSubstitutive;
	USBoolean * IsWorkSheetChecked;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgUnitName;
	NSNumber * TimeWorkingShiftId;
	NSString * WageType;
	NSNumber * WorkingMonth;
	NSString * WorkingType;
	NSNumber * WorkingYear;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_V_ESS_GET_WORKING_SHIFT *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * D01;
@property (retain) NSString * D02;
@property (retain) NSString * D03;
@property (retain) NSString * D04;
@property (retain) NSString * D05;
@property (retain) NSString * D06;
@property (retain) NSString * D07;
@property (retain) NSString * D08;
@property (retain) NSString * D09;
@property (retain) NSString * D10;
@property (retain) NSString * D11;
@property (retain) NSString * D12;
@property (retain) NSString * D13;
@property (retain) NSString * D14;
@property (retain) NSString * D15;
@property (retain) NSString * D16;
@property (retain) NSString * D17;
@property (retain) NSString * D18;
@property (retain) NSString * D19;
@property (retain) NSString * D20;
@property (retain) NSString * D21;
@property (retain) NSString * D22;
@property (retain) NSString * D23;
@property (retain) NSString * D24;
@property (retain) NSString * D25;
@property (retain) NSString * D26;
@property (retain) NSString * D27;
@property (retain) NSString * D28;
@property (retain) NSString * D29;
@property (retain) NSString * D30;
@property (retain) NSString * D31;
@property (retain) NSString * Day01;
@property (retain) NSString * Day02;
@property (retain) NSString * Day03;
@property (retain) NSString * Day04;
@property (retain) NSString * Day05;
@property (retain) NSString * Day06;
@property (retain) NSString * Day07;
@property (retain) NSString * Day08;
@property (retain) NSString * Day09;
@property (retain) NSString * Day10;
@property (retain) NSString * Day11;
@property (retain) NSString * Day12;
@property (retain) NSString * Day13;
@property (retain) NSString * Day14;
@property (retain) NSString * Day15;
@property (retain) NSString * Day16;
@property (retain) NSString * Day17;
@property (retain) NSString * Day18;
@property (retain) NSString * Day19;
@property (retain) NSString * Day20;
@property (retain) NSString * Day21;
@property (retain) NSString * Day22;
@property (retain) NSString * Day23;
@property (retain) NSString * Day24;
@property (retain) NSString * Day25;
@property (retain) NSString * Day26;
@property (retain) NSString * Day27;
@property (retain) NSString * Day28;
@property (retain) NSString * Day29;
@property (retain) NSString * Day30;
@property (retain) NSString * Day31;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * EmployeeName;
@property (retain) NSString * GroupCode;
@property (retain) NSString * GroupName;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsSubstitutive;
@property (retain) USBoolean * IsWorkSheetChecked;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitName;
@property (retain) NSNumber * TimeWorkingShiftId;
@property (retain) NSString * WageType;
@property (retain) NSNumber * WorkingMonth;
@property (retain) NSString * WorkingType;
@property (retain) NSNumber * WorkingYear;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfV_ESS_GET_WORKING_SHIFT : NSObject {
	
/* elements */
	NSMutableArray *V_ESS_GET_WORKING_SHIFT;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfV_ESS_GET_WORKING_SHIFT *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ESS_GET_WORKING_SHIFT:(time_tns1_V_ESS_GET_WORKING_SHIFT *)toAdd;
@property (readonly) NSMutableArray * V_ESS_GET_WORKING_SHIFT;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TimeWorkingForm : NSObject {
	
/* elements */
	NSString * After_OT_Type;
	NSString * Allowance_Type;
	NSString * Before_OT_Type;
	NSString * Color;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * EndOfBreakTime;
	NSDate * EndTime;
	NSString * FLex1;
	NSNumber * FLex10;
	NSDate * FLex11;
	NSDate * FLex12;
	NSDate * FLex13;
	USBoolean * FLex14;
	USBoolean * FLex15;
	USBoolean * FLex16;
	NSString * FLex2;
	NSString * FLex3;
	NSString * FLex4;
	NSString * FLex5;
	NSNumber * FLex6;
	NSNumber * FLex7;
	NSNumber * FLex8;
	NSNumber * FLex9;
	NSNumber * HoursOfOfficialWork;
	USBoolean * IsDeleted;
	USBoolean * IsWorkingTimeIncludeBreakTime;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * NameEN;
	NSString * NameVN;
	NSString * Note;
	NSNumber * NumberOfBreakTime;
	NSString * ParentCode;
	USBoolean * SpecialShift;
	NSNumber * Standard_Working_Hours;
	NSDate * StartOfBreakTime;
	NSDate * StartTime;
	NSString * TimeWorkingFormCode;
	NSNumber * TimeWorkingFormId;
	NSString * Time_In_Out_Rule;
	NSString * Time_Log_Rule;
	NSString * WorkingFormTypeCode;
	NSString * Working_Form_Group;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TimeWorkingForm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * After_OT_Type;
@property (retain) NSString * Allowance_Type;
@property (retain) NSString * Before_OT_Type;
@property (retain) NSString * Color;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndOfBreakTime;
@property (retain) NSDate * EndTime;
@property (retain) NSString * FLex1;
@property (retain) NSNumber * FLex10;
@property (retain) NSDate * FLex11;
@property (retain) NSDate * FLex12;
@property (retain) NSDate * FLex13;
@property (retain) USBoolean * FLex14;
@property (retain) USBoolean * FLex15;
@property (retain) USBoolean * FLex16;
@property (retain) NSString * FLex2;
@property (retain) NSString * FLex3;
@property (retain) NSString * FLex4;
@property (retain) NSString * FLex5;
@property (retain) NSNumber * FLex6;
@property (retain) NSNumber * FLex7;
@property (retain) NSNumber * FLex8;
@property (retain) NSNumber * FLex9;
@property (retain) NSNumber * HoursOfOfficialWork;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsWorkingTimeIncludeBreakTime;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * NameEN;
@property (retain) NSString * NameVN;
@property (retain) NSString * Note;
@property (retain) NSNumber * NumberOfBreakTime;
@property (retain) NSString * ParentCode;
@property (retain) USBoolean * SpecialShift;
@property (retain) NSNumber * Standard_Working_Hours;
@property (retain) NSDate * StartOfBreakTime;
@property (retain) NSDate * StartTime;
@property (retain) NSString * TimeWorkingFormCode;
@property (retain) NSNumber * TimeWorkingFormId;
@property (retain) NSString * Time_In_Out_Rule;
@property (retain) NSString * Time_Log_Rule;
@property (retain) NSString * WorkingFormTypeCode;
@property (retain) NSString * Working_Form_Group;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTimeWorkingForm : NSObject {
	
/* elements */
	NSMutableArray *TimeWorkingForm;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTimeWorkingForm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeWorkingForm:(time_tns1_TimeWorkingForm *)toAdd;
@property (readonly) NSMutableArray * TimeWorkingForm;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_SP_ESS_GET_WORKINGSHIFT_BY_CON_Result : NSObject {
	
/* elements */
	NSDate * ACTUAL_END_WORKING;
	NSDate * ACTUAL_START_WORKING;
	NSNumber * ACTUAL_WORKING_MINUTE;
	USBoolean * ALLOW_LEAVE_EARLY;
	NSNumber * BREAK_TIME_OUT_MINUTE;
	NSString * EMPLOYEE_CODE;
	NSNumber * EMPLOYEE_ID;
	NSString * EMPLOYEE_NAME;
	NSDate * END_BREAK_TIME_OUT;
	NSDate * END_WORKING_OVER_TIME;
	NSString * GROUP_CODE;
	NSDate * GROUP_CONDITION;
	NSString * GROUP_NAME;
	USBoolean * IS_BREAK_TIME_OUT;
	USBoolean * IS_CHECKED;
	USBoolean * IS_LEAVE;
	USBoolean * IS_OVER_TIME;
	NSNumber * LEAVE_TYPE_CP1;
	NSNumber * LEAVE_TYPE_CP2;
	NSNumber * LEAVE_TYPE_CP3;
	NSNumber * LEAVE_TYPE_CP4;
	NSNumber * LEAVE_TYPE_CP5;
	NSNumber * LEAVE_TYPE_CP6;
	NSNumber * LEAVE_TYPE_CP7;
	NSNumber * LEAVE_TYPE_IP1;
	NSNumber * LEAVE_TYPE_IP2;
	NSNumber * LEAVE_TYPE_IP3;
	NSNumber * LEAVE_TYPE_IP4;
	NSNumber * LEAVE_TYPE_IP5;
	NSNumber * LEAVE_TYPE_IP6;
	NSNumber * LEAVE_TYPE_LC;
	NSNumber * LEAVE_TYPE_UP1;
	NSNumber * LEAVE_TYPE_UP2;
	NSString * ORG_CODE;
	NSString * ORG_NAME;
	NSNumber * OVER_TIME_WORKING_MINUTE;
	NSString * POSITION_CODE;
	NSString * POSITION_NAME;
	USBoolean * REQUIRE_WORKING_EARLY;
	NSDate * START_BREAK_TIME_OUT;
	NSDate * START_WORKING_OVER_TIME;
	NSNumber * SUM_OF_UNPAID_LEAVE;
	NSNumber * UNPAID_LEAVE;
	NSString * WAGE_TYPE_CODE;
	NSString * WORKING_FORM_CODE;
	NSNumber * WORK_SHIFT_DAY;
	NSNumber * WORK_SHIFT_MONTH;
	NSNumber * WORK_SHIFT_YEAR;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_SP_ESS_GET_WORKINGSHIFT_BY_CON_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ACTUAL_END_WORKING;
@property (retain) NSDate * ACTUAL_START_WORKING;
@property (retain) NSNumber * ACTUAL_WORKING_MINUTE;
@property (retain) USBoolean * ALLOW_LEAVE_EARLY;
@property (retain) NSNumber * BREAK_TIME_OUT_MINUTE;
@property (retain) NSString * EMPLOYEE_CODE;
@property (retain) NSNumber * EMPLOYEE_ID;
@property (retain) NSString * EMPLOYEE_NAME;
@property (retain) NSDate * END_BREAK_TIME_OUT;
@property (retain) NSDate * END_WORKING_OVER_TIME;
@property (retain) NSString * GROUP_CODE;
@property (retain) NSDate * GROUP_CONDITION;
@property (retain) NSString * GROUP_NAME;
@property (retain) USBoolean * IS_BREAK_TIME_OUT;
@property (retain) USBoolean * IS_CHECKED;
@property (retain) USBoolean * IS_LEAVE;
@property (retain) USBoolean * IS_OVER_TIME;
@property (retain) NSNumber * LEAVE_TYPE_CP1;
@property (retain) NSNumber * LEAVE_TYPE_CP2;
@property (retain) NSNumber * LEAVE_TYPE_CP3;
@property (retain) NSNumber * LEAVE_TYPE_CP4;
@property (retain) NSNumber * LEAVE_TYPE_CP5;
@property (retain) NSNumber * LEAVE_TYPE_CP6;
@property (retain) NSNumber * LEAVE_TYPE_CP7;
@property (retain) NSNumber * LEAVE_TYPE_IP1;
@property (retain) NSNumber * LEAVE_TYPE_IP2;
@property (retain) NSNumber * LEAVE_TYPE_IP3;
@property (retain) NSNumber * LEAVE_TYPE_IP4;
@property (retain) NSNumber * LEAVE_TYPE_IP5;
@property (retain) NSNumber * LEAVE_TYPE_IP6;
@property (retain) NSNumber * LEAVE_TYPE_LC;
@property (retain) NSNumber * LEAVE_TYPE_UP1;
@property (retain) NSNumber * LEAVE_TYPE_UP2;
@property (retain) NSString * ORG_CODE;
@property (retain) NSString * ORG_NAME;
@property (retain) NSNumber * OVER_TIME_WORKING_MINUTE;
@property (retain) NSString * POSITION_CODE;
@property (retain) NSString * POSITION_NAME;
@property (retain) USBoolean * REQUIRE_WORKING_EARLY;
@property (retain) NSDate * START_BREAK_TIME_OUT;
@property (retain) NSDate * START_WORKING_OVER_TIME;
@property (retain) NSNumber * SUM_OF_UNPAID_LEAVE;
@property (retain) NSNumber * UNPAID_LEAVE;
@property (retain) NSString * WAGE_TYPE_CODE;
@property (retain) NSString * WORKING_FORM_CODE;
@property (retain) NSNumber * WORK_SHIFT_DAY;
@property (retain) NSNumber * WORK_SHIFT_MONTH;
@property (retain) NSNumber * WORK_SHIFT_YEAR;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfSP_ESS_GET_WORKINGSHIFT_BY_CON_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_ESS_GET_WORKINGSHIFT_BY_CON_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfSP_ESS_GET_WORKINGSHIFT_BY_CON_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_ESS_GET_WORKINGSHIFT_BY_CON_Result:(time_tns1_SP_ESS_GET_WORKINGSHIFT_BY_CON_Result *)toAdd;
@property (readonly) NSMutableArray * SP_ESS_GET_WORKINGSHIFT_BY_CON_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_V_TimeLeaveRequest : NSObject {
	
/* elements */
	NSDate * ApprovalDate;
	NSString * ApproverCode;
	NSString * ApproverFullName;
	NSNumber * ApproverId;
	NSString * Code;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * FirstName;
	NSString * Flex1;
	NSNumber * Flex10;
	NSNumber * Flex11;
	NSNumber * Flex12;
	NSNumber * Flex13;
	USBoolean * Flex14;
	USBoolean * Flex15;
	USBoolean * Flex16;
	USBoolean * Flex17;
	NSDate * Flex18;
	NSDate * Flex19;
	NSString * Flex2;
	NSDate * Flex20;
	NSDate * Flex21;
	NSNumber * Flex22;
	NSNumber * Flex23;
	NSString * Flex24;
	NSString * Flex25;
	NSString * Flex26;
	NSString * Flex27;
	NSDate * Flex28;
	NSDate * Flex29;
	NSString * Flex3;
	NSString * Flex30;
	NSString * Flex31;
	NSString * Flex32;
	NSString * Flex33;
	NSString * Flex34;
	NSString * Flex35;
	NSString * Flex36;
	NSString * Flex37;
	NSString * Flex38;
	NSString * Flex39;
	NSDate * Flex4;
	NSNumber * Flex40;
	NSNumber * Flex41;
	NSNumber * Flex42;
	NSNumber * Flex43;
	NSNumber * Flex44;
	NSNumber * Flex5;
	NSNumber * Flex6;
	NSNumber * Flex7;
	NSNumber * Flex8;
	NSNumber * Flex9;
	NSString * FullName;
	USBoolean * IsDeleted;
	NSString * LastName;
	NSString * LeaveTypeDes;
	NSString * LeaveTypeNameEN;
	NSString * LeaveTypeNameVN;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgWorkLevelCode;
	NSNumber * OrgWorkLevelId;
	NSString * PositionName;
	NSNumber * Status;
	NSNumber * TimeRequestMasTerId;
	NSString * Type;
	NSString * UnitName;
	NSString * WorkLevelName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_V_TimeLeaveRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ApprovalDate;
@property (retain) NSString * ApproverCode;
@property (retain) NSString * ApproverFullName;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * Code;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * FirstName;
@property (retain) NSString * Flex1;
@property (retain) NSNumber * Flex10;
@property (retain) NSNumber * Flex11;
@property (retain) NSNumber * Flex12;
@property (retain) NSNumber * Flex13;
@property (retain) USBoolean * Flex14;
@property (retain) USBoolean * Flex15;
@property (retain) USBoolean * Flex16;
@property (retain) USBoolean * Flex17;
@property (retain) NSDate * Flex18;
@property (retain) NSDate * Flex19;
@property (retain) NSString * Flex2;
@property (retain) NSDate * Flex20;
@property (retain) NSDate * Flex21;
@property (retain) NSNumber * Flex22;
@property (retain) NSNumber * Flex23;
@property (retain) NSString * Flex24;
@property (retain) NSString * Flex25;
@property (retain) NSString * Flex26;
@property (retain) NSString * Flex27;
@property (retain) NSDate * Flex28;
@property (retain) NSDate * Flex29;
@property (retain) NSString * Flex3;
@property (retain) NSString * Flex30;
@property (retain) NSString * Flex31;
@property (retain) NSString * Flex32;
@property (retain) NSString * Flex33;
@property (retain) NSString * Flex34;
@property (retain) NSString * Flex35;
@property (retain) NSString * Flex36;
@property (retain) NSString * Flex37;
@property (retain) NSString * Flex38;
@property (retain) NSString * Flex39;
@property (retain) NSDate * Flex4;
@property (retain) NSNumber * Flex40;
@property (retain) NSNumber * Flex41;
@property (retain) NSNumber * Flex42;
@property (retain) NSNumber * Flex43;
@property (retain) NSNumber * Flex44;
@property (retain) NSNumber * Flex5;
@property (retain) NSNumber * Flex6;
@property (retain) NSNumber * Flex7;
@property (retain) NSNumber * Flex8;
@property (retain) NSNumber * Flex9;
@property (retain) NSString * FullName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * LastName;
@property (retain) NSString * LeaveTypeDes;
@property (retain) NSString * LeaveTypeNameEN;
@property (retain) NSString * LeaveTypeNameVN;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * PositionName;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * TimeRequestMasTerId;
@property (retain) NSString * Type;
@property (retain) NSString * UnitName;
@property (retain) NSString * WorkLevelName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfV_TimeLeaveRequest : NSObject {
	
/* elements */
	NSMutableArray *V_TimeLeaveRequest;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfV_TimeLeaveRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_TimeLeaveRequest:(time_tns1_V_TimeLeaveRequest *)toAdd;
@property (readonly) NSMutableArray * V_TimeLeaveRequest;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_V_EmpBasicInformation : NSObject {
	
/* elements */
	NSString * Address;
	NSNumber * AddressId;
	NSString * BankAccountNo;
	NSString * BankBranch;
	NSString * BankName;
	NSDate * BirthDay;
	NSString * BirthPlace;
	NSNumber * BirthPlaceId;
	NSString * CVUrl;
	NSString * CodeAndFullName;
	NSNumber * CompanyId;
	NSString * ContactAddress;
	NSNumber * ContactAddressId;
	NSString * CulturalLevel;
	NSNumber * CulturalLevelId;
	NSNumber * EducationLevelId;
	NSString * EducationLevelName;
	NSString * EducationLevelNameEN;
	NSString * Email;
	NSNumber * EmpBasicProfileId;
	NSString * EmpWorkingStatusCode;
	NSString * EmpWorkingStatusName;
	NSString * EmpWorkingStatusNameEN;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EmployeeTypeId;
	NSString * EmployeeTypeName;
	NSString * EmployeeTypeNameEN;
	NSDate * EntryDate;
	NSString * Ethnicity;
	NSNumber * EthnicityId;
	NSString * FirstName;
	NSString * FullName;
	USBoolean * Gender;
	NSString * HomePhone;
	NSString * HomeTown;
	NSNumber * HomeTownId;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	NSString * ImageUrl;
	USBoolean * IsConcurrentPost;
	USBoolean * IsDeleted;
	USBoolean * IsOnlyYearOfBirthday;
	NSString * LastName;
	NSString * MainLanguage;
	NSNumber * MainLanguageId;
	NSString * Major;
	NSNumber * MajorId;
	NSNumber * MaritalStatusId;
	NSString * MaritalStatusValue;
	NSString * MobileNumber;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSString * OfficePhone;
	NSDate * OfficialDate;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobNameEN;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgJobPositionName;
	NSNumber * OrgJobTitleId;
	NSString * OrgJobTitleLevelCode;
	NSString * OrgJobTitleNameEN;
	NSString * OrgJobTitleNameVN;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgUnitName;
	NSString * OrgWorkLevelCode;
	NSString * OrgWorkLevelDescription;
	NSNumber * OrgWorkLevelId;
	NSString * OrgWorkLevelName;
	NSString * PITCode;
	NSString * PITDateOfIssue;
	NSString * PITIssuedBy;
	NSDate * PassportDateOfExpire;
	NSDate * PassportDateOfIssue;
	NSString * PassportNo;
	NSString * PassportPlaceOfIssue;
	NSString * PermanentAddress;
	NSNumber * PermanentAddressId;
	NSString * PositionName;
	NSString * PositionNameEN;
	NSDate * ProbationaryDate;
	NSDate * ProbationaryEndDate;
	NSNumber * ProbationarySalary;
	NSString * Religion;
	NSNumber * ReligionId;
	NSString * TemporaryAddress;
	NSNumber * TemporaryAddressId;
	NSString * TimeWorkingFormCode;
	NSNumber * TimeWorkingFormId;
	NSString * UnitName;
	NSString * UnitNameEN;
	NSString * WorkLevelDescription;
	NSString * WorkLevelName;
	NSString * WorkLevelNameEN;
	NSString * WorkLocation;
	NSDate * WorkingFormImplementationDate;
	NSString * WorkingFormTypeCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_V_EmpBasicInformation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSNumber * AddressId;
@property (retain) NSString * BankAccountNo;
@property (retain) NSString * BankBranch;
@property (retain) NSString * BankName;
@property (retain) NSDate * BirthDay;
@property (retain) NSString * BirthPlace;
@property (retain) NSNumber * BirthPlaceId;
@property (retain) NSString * CVUrl;
@property (retain) NSString * CodeAndFullName;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ContactAddress;
@property (retain) NSNumber * ContactAddressId;
@property (retain) NSString * CulturalLevel;
@property (retain) NSNumber * CulturalLevelId;
@property (retain) NSNumber * EducationLevelId;
@property (retain) NSString * EducationLevelName;
@property (retain) NSString * EducationLevelNameEN;
@property (retain) NSString * Email;
@property (retain) NSNumber * EmpBasicProfileId;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmpWorkingStatusNameEN;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSString * EmployeeTypeName;
@property (retain) NSString * EmployeeTypeNameEN;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * Ethnicity;
@property (retain) NSNumber * EthnicityId;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * HomePhone;
@property (retain) NSString * HomeTown;
@property (retain) NSNumber * HomeTownId;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsConcurrentPost;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) NSString * LastName;
@property (retain) NSString * MainLanguage;
@property (retain) NSNumber * MainLanguageId;
@property (retain) NSString * Major;
@property (retain) NSNumber * MajorId;
@property (retain) NSNumber * MaritalStatusId;
@property (retain) NSString * MaritalStatusValue;
@property (retain) NSString * MobileNumber;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSString * OfficePhone;
@property (retain) NSDate * OfficialDate;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobNameEN;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSNumber * OrgJobTitleId;
@property (retain) NSString * OrgJobTitleLevelCode;
@property (retain) NSString * OrgJobTitleNameEN;
@property (retain) NSString * OrgJobTitleNameVN;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitName;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSString * OrgWorkLevelDescription;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * OrgWorkLevelName;
@property (retain) NSString * PITCode;
@property (retain) NSString * PITDateOfIssue;
@property (retain) NSString * PITIssuedBy;
@property (retain) NSDate * PassportDateOfExpire;
@property (retain) NSDate * PassportDateOfIssue;
@property (retain) NSString * PassportNo;
@property (retain) NSString * PassportPlaceOfIssue;
@property (retain) NSString * PermanentAddress;
@property (retain) NSNumber * PermanentAddressId;
@property (retain) NSString * PositionName;
@property (retain) NSString * PositionNameEN;
@property (retain) NSDate * ProbationaryDate;
@property (retain) NSDate * ProbationaryEndDate;
@property (retain) NSNumber * ProbationarySalary;
@property (retain) NSString * Religion;
@property (retain) NSNumber * ReligionId;
@property (retain) NSString * TemporaryAddress;
@property (retain) NSNumber * TemporaryAddressId;
@property (retain) NSString * TimeWorkingFormCode;
@property (retain) NSNumber * TimeWorkingFormId;
@property (retain) NSString * UnitName;
@property (retain) NSString * UnitNameEN;
@property (retain) NSString * WorkLevelDescription;
@property (retain) NSString * WorkLevelName;
@property (retain) NSString * WorkLevelNameEN;
@property (retain) NSString * WorkLocation;
@property (retain) NSDate * WorkingFormImplementationDate;
@property (retain) NSString * WorkingFormTypeCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfV_EmpBasicInformation : NSObject {
	
/* elements */
	NSMutableArray *V_EmpBasicInformation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfV_EmpBasicInformation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EmpBasicInformation:(time_tns1_V_EmpBasicInformation *)toAdd;
@property (readonly) NSMutableArray * V_EmpBasicInformation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_V_TimeLaterEarly : NSObject {
	
/* elements */
	NSDate * ApprovalDate;
	NSString * ApproverCode;
	NSString * ApproverFullName;
	NSNumber * ApproverId;
	NSString * Code;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * FirstName;
	NSString * Flex1;
	NSNumber * Flex10;
	NSNumber * Flex11;
	NSNumber * Flex12;
	NSNumber * Flex13;
	USBoolean * Flex14;
	USBoolean * Flex15;
	USBoolean * Flex16;
	USBoolean * Flex17;
	NSDate * Flex18;
	NSDate * Flex19;
	NSString * Flex2;
	NSDate * Flex20;
	NSDate * Flex21;
	NSNumber * Flex22;
	NSNumber * Flex23;
	NSString * Flex24;
	NSString * Flex25;
	NSString * Flex26;
	NSString * Flex27;
	NSDate * Flex28;
	NSDate * Flex29;
	NSString * Flex3;
	NSString * Flex30;
	NSString * Flex31;
	NSString * Flex32;
	NSString * Flex33;
	NSString * Flex34;
	NSString * Flex35;
	NSString * Flex36;
	NSString * Flex37;
	NSString * Flex38;
	NSString * Flex39;
	NSDate * Flex4;
	NSNumber * Flex40;
	NSNumber * Flex41;
	NSNumber * Flex42;
	NSNumber * Flex43;
	NSNumber * Flex44;
	NSNumber * Flex5;
	NSNumber * Flex6;
	NSNumber * Flex7;
	NSNumber * Flex8;
	NSNumber * Flex9;
	NSString * FullName;
	USBoolean * IsDeleted;
	NSString * LastName;
	NSString * LeaveTypeDes;
	NSString * LeaveTypeEN;
	NSString * LeaveTypeVN;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgWorkLevelCode;
	NSNumber * OrgWorkLevelId;
	NSString * PositionName;
	NSNumber * Status;
	NSNumber * TimeRequestMasTerId;
	NSString * Type;
	NSString * UnitName;
	NSString * WorkLevelName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_V_TimeLaterEarly *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ApprovalDate;
@property (retain) NSString * ApproverCode;
@property (retain) NSString * ApproverFullName;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * Code;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * FirstName;
@property (retain) NSString * Flex1;
@property (retain) NSNumber * Flex10;
@property (retain) NSNumber * Flex11;
@property (retain) NSNumber * Flex12;
@property (retain) NSNumber * Flex13;
@property (retain) USBoolean * Flex14;
@property (retain) USBoolean * Flex15;
@property (retain) USBoolean * Flex16;
@property (retain) USBoolean * Flex17;
@property (retain) NSDate * Flex18;
@property (retain) NSDate * Flex19;
@property (retain) NSString * Flex2;
@property (retain) NSDate * Flex20;
@property (retain) NSDate * Flex21;
@property (retain) NSNumber * Flex22;
@property (retain) NSNumber * Flex23;
@property (retain) NSString * Flex24;
@property (retain) NSString * Flex25;
@property (retain) NSString * Flex26;
@property (retain) NSString * Flex27;
@property (retain) NSDate * Flex28;
@property (retain) NSDate * Flex29;
@property (retain) NSString * Flex3;
@property (retain) NSString * Flex30;
@property (retain) NSString * Flex31;
@property (retain) NSString * Flex32;
@property (retain) NSString * Flex33;
@property (retain) NSString * Flex34;
@property (retain) NSString * Flex35;
@property (retain) NSString * Flex36;
@property (retain) NSString * Flex37;
@property (retain) NSString * Flex38;
@property (retain) NSString * Flex39;
@property (retain) NSDate * Flex4;
@property (retain) NSNumber * Flex40;
@property (retain) NSNumber * Flex41;
@property (retain) NSNumber * Flex42;
@property (retain) NSNumber * Flex43;
@property (retain) NSNumber * Flex44;
@property (retain) NSNumber * Flex5;
@property (retain) NSNumber * Flex6;
@property (retain) NSNumber * Flex7;
@property (retain) NSNumber * Flex8;
@property (retain) NSNumber * Flex9;
@property (retain) NSString * FullName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * LastName;
@property (retain) NSString * LeaveTypeDes;
@property (retain) NSString * LeaveTypeEN;
@property (retain) NSString * LeaveTypeVN;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * PositionName;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * TimeRequestMasTerId;
@property (retain) NSString * Type;
@property (retain) NSString * UnitName;
@property (retain) NSString * WorkLevelName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfV_TimeLaterEarly : NSObject {
	
/* elements */
	NSMutableArray *V_TimeLaterEarly;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfV_TimeLaterEarly *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_TimeLaterEarly:(time_tns1_V_TimeLaterEarly *)toAdd;
@property (readonly) NSMutableArray * V_TimeLaterEarly;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TimeWageType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * FLex1;
	NSNumber * FLex10;
	NSDate * FLex11;
	NSDate * FLex12;
	NSDate * FLex13;
	USBoolean * FLex14;
	USBoolean * FLex15;
	USBoolean * FLex16;
	NSString * FLex2;
	NSString * FLex3;
	NSString * FLex4;
	NSString * FLex5;
	NSNumber * FLex6;
	NSNumber * FLex7;
	NSNumber * FLex8;
	NSNumber * FLex9;
	NSString * Friday;
	USBoolean * IsApproveOvertime;
	USBoolean * IsDefault;
	USBoolean * IsDeleted;
	USBoolean * IsFollowPeriod;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Monday;
	NSString * NameEN;
	NSString * NameVN;
	NSNumber * NumberOfDaysInAPeriod;
	NSNumber * NumberOfDaysOffInAPeriod;
	NSString * Saturday;
	NSString * Sunday;
	NSString * Thursday;
	NSString * TimeWageTypeCode;
	NSNumber * TimeWageTypeId;
	NSString * Tuesday;
	NSString * Wednesday;
	NSNumber * WorkingDay;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TimeWageType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * FLex1;
@property (retain) NSNumber * FLex10;
@property (retain) NSDate * FLex11;
@property (retain) NSDate * FLex12;
@property (retain) NSDate * FLex13;
@property (retain) USBoolean * FLex14;
@property (retain) USBoolean * FLex15;
@property (retain) USBoolean * FLex16;
@property (retain) NSString * FLex2;
@property (retain) NSString * FLex3;
@property (retain) NSString * FLex4;
@property (retain) NSString * FLex5;
@property (retain) NSNumber * FLex6;
@property (retain) NSNumber * FLex7;
@property (retain) NSNumber * FLex8;
@property (retain) NSNumber * FLex9;
@property (retain) NSString * Friday;
@property (retain) USBoolean * IsApproveOvertime;
@property (retain) USBoolean * IsDefault;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsFollowPeriod;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Monday;
@property (retain) NSString * NameEN;
@property (retain) NSString * NameVN;
@property (retain) NSNumber * NumberOfDaysInAPeriod;
@property (retain) NSNumber * NumberOfDaysOffInAPeriod;
@property (retain) NSString * Saturday;
@property (retain) NSString * Sunday;
@property (retain) NSString * Thursday;
@property (retain) NSString * TimeWageTypeCode;
@property (retain) NSNumber * TimeWageTypeId;
@property (retain) NSString * Tuesday;
@property (retain) NSString * Wednesday;
@property (retain) NSNumber * WorkingDay;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTimeWageType : NSObject {
	
/* elements */
	NSMutableArray *TimeWageType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTimeWageType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeWageType:(time_tns1_TimeWageType *)toAdd;
@property (readonly) NSMutableArray * TimeWageType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_V_TimeDateOfSalaryCalculationForm : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * DateOfSalaryCalculation;
	NSNumber * EndDate;
	NSString * FLex1;
	NSNumber * FLex10;
	USBoolean * FLex14;
	USBoolean * FLex15;
	USBoolean * FLex16;
	NSString * FLex2;
	NSString * FLex3;
	NSString * FLex4;
	NSString * FLex5;
	NSNumber * FLex6;
	NSNumber * FLex7;
	NSNumber * FLex8;
	NSNumber * FLex9;
	NSDate * ImplementationDate;
	USBoolean * IsByActualWorkingDay;
	USBoolean * IsDeleted;
	NSString * NameEN;
	NSString * NameVN;
	NSNumber * StartDate;
	NSString * TimeDateOfSalaryCalculationCode;
	NSNumber * TimeDateOfSalaryCalculationId;
	NSNumber * TimeWageTypeId;
	NSString * TimeWageTypeNameEN;
	NSString * TimeWageTypeNameVN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_V_TimeDateOfSalaryCalculationForm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * DateOfSalaryCalculation;
@property (retain) NSNumber * EndDate;
@property (retain) NSString * FLex1;
@property (retain) NSNumber * FLex10;
@property (retain) USBoolean * FLex14;
@property (retain) USBoolean * FLex15;
@property (retain) USBoolean * FLex16;
@property (retain) NSString * FLex2;
@property (retain) NSString * FLex3;
@property (retain) NSString * FLex4;
@property (retain) NSString * FLex5;
@property (retain) NSNumber * FLex6;
@property (retain) NSNumber * FLex7;
@property (retain) NSNumber * FLex8;
@property (retain) NSNumber * FLex9;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsByActualWorkingDay;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * NameEN;
@property (retain) NSString * NameVN;
@property (retain) NSNumber * StartDate;
@property (retain) NSString * TimeDateOfSalaryCalculationCode;
@property (retain) NSNumber * TimeDateOfSalaryCalculationId;
@property (retain) NSNumber * TimeWageTypeId;
@property (retain) NSString * TimeWageTypeNameEN;
@property (retain) NSString * TimeWageTypeNameVN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfV_TimeDateOfSalaryCalculationForm : NSObject {
	
/* elements */
	NSMutableArray *V_TimeDateOfSalaryCalculationForm;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfV_TimeDateOfSalaryCalculationForm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_TimeDateOfSalaryCalculationForm:(time_tns1_V_TimeDateOfSalaryCalculationForm *)toAdd;
@property (readonly) NSMutableArray * V_TimeDateOfSalaryCalculationForm;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TimeDateOfSalaryCalculation : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * DateOfSalaryCalculation;
	NSNumber * EndDate;
	NSString * FLex1;
	NSNumber * FLex10;
	NSDate * FLex11;
	NSDate * FLex12;
	NSDate * FLex13;
	USBoolean * FLex14;
	USBoolean * FLex15;
	USBoolean * FLex16;
	NSString * FLex2;
	NSString * FLex3;
	NSString * FLex4;
	NSString * FLex5;
	NSNumber * FLex6;
	NSNumber * FLex7;
	NSNumber * FLex8;
	NSNumber * FLex9;
	NSDate * ImplementationDate;
	USBoolean * IsByActualWorkingDay;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * NameEN;
	NSString * NameVN;
	NSNumber * StartDate;
	NSString * TimeDateOfSalaryCalculationCode;
	NSNumber * TimeDateOfSalaryCalculationId;
	NSNumber * TimeWageTypeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TimeDateOfSalaryCalculation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * DateOfSalaryCalculation;
@property (retain) NSNumber * EndDate;
@property (retain) NSString * FLex1;
@property (retain) NSNumber * FLex10;
@property (retain) NSDate * FLex11;
@property (retain) NSDate * FLex12;
@property (retain) NSDate * FLex13;
@property (retain) USBoolean * FLex14;
@property (retain) USBoolean * FLex15;
@property (retain) USBoolean * FLex16;
@property (retain) NSString * FLex2;
@property (retain) NSString * FLex3;
@property (retain) NSString * FLex4;
@property (retain) NSString * FLex5;
@property (retain) NSNumber * FLex6;
@property (retain) NSNumber * FLex7;
@property (retain) NSNumber * FLex8;
@property (retain) NSNumber * FLex9;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsByActualWorkingDay;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * NameEN;
@property (retain) NSString * NameVN;
@property (retain) NSNumber * StartDate;
@property (retain) NSString * TimeDateOfSalaryCalculationCode;
@property (retain) NSNumber * TimeDateOfSalaryCalculationId;
@property (retain) NSNumber * TimeWageTypeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TimeSysConfigParam : NSObject {
	
/* elements */
	NSString * Code;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * Description;
	NSString * Flex1;
	NSNumber * Flex10;
	USBoolean * Flex11;
	USBoolean * Flex12;
	USBoolean * Flex13;
	USBoolean * Flex14;
	NSString * Flex2;
	NSString * Flex3;
	NSDate * Flex4;
	NSDate * Flex5;
	NSDate * Flex6;
	NSNumber * Flex7;
	NSNumber * Flex8;
	NSNumber * Flex9;
	USBoolean * IsDeleted;
	NSString * Key;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
	NSNumber * TimeSysConfigParamId;
	NSString * Type;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TimeSysConfigParam *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Code;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) NSString * Flex1;
@property (retain) NSNumber * Flex10;
@property (retain) USBoolean * Flex11;
@property (retain) USBoolean * Flex12;
@property (retain) USBoolean * Flex13;
@property (retain) USBoolean * Flex14;
@property (retain) NSString * Flex2;
@property (retain) NSString * Flex3;
@property (retain) NSDate * Flex4;
@property (retain) NSDate * Flex5;
@property (retain) NSDate * Flex6;
@property (retain) NSNumber * Flex7;
@property (retain) NSNumber * Flex8;
@property (retain) NSNumber * Flex9;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Key;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) NSNumber * TimeSysConfigParamId;
@property (retain) NSString * Type;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_SP_ESS_USER_REQUISITION_LIST_Result : NSObject {
	
/* elements */
	NSString * AbsentCode;
	NSDate * ApprovalDate;
	NSString * ApproverCode;
	NSNumber * ApproverId;
	NSString * ApproverName;
	NSString * Code;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * DateOffState;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndOff;
	NSString * Flex1;
	NSNumber * Flex10;
	NSNumber * Flex11;
	NSNumber * Flex12;
	NSNumber * Flex13;
	USBoolean * Flex14;
	USBoolean * Flex15;
	USBoolean * Flex16;
	USBoolean * Flex17;
	NSDate * Flex18;
	NSDate * Flex19;
	NSString * Flex2;
	NSDate * Flex20;
	NSDate * Flex21;
	NSNumber * Flex22;
	NSString * Flex25;
	NSString * Flex26;
	NSString * Flex27;
	NSString * Flex3;
	NSString * Flex30;
	NSString * Flex31;
	NSString * Flex32;
	NSString * Flex33;
	NSString * Flex34;
	NSString * Flex35;
	NSString * Flex36;
	NSString * Flex37;
	NSString * Flex38;
	NSString * Flex39;
	NSDate * Flex4;
	NSNumber * Flex40;
	NSNumber * Flex41;
	NSNumber * Flex42;
	NSNumber * Flex43;
	NSNumber * Flex44;
	NSNumber * Flex6;
	NSNumber * Flex7;
	NSNumber * Flex8;
	NSNumber * Flex9;
	NSString * FullName;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * NumDateOff;
	NSString * OrgJobPositionCode;
	NSString * OrgJobPositionName;
	NSString * OrgUnitCode;
	NSString * OrgUnitName;
	NSNumber * Priority;
	NSDate * StartOff;
	NSNumber * Status;
	NSString * StatusName;
	NSString * SysParamNameEN;
	NSString * SysParamNameVN;
	NSNumber * TimeRequestMasTerId;
	NSString * Type;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_SP_ESS_USER_REQUISITION_LIST_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AbsentCode;
@property (retain) NSDate * ApprovalDate;
@property (retain) NSString * ApproverCode;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * ApproverName;
@property (retain) NSString * Code;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * DateOffState;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndOff;
@property (retain) NSString * Flex1;
@property (retain) NSNumber * Flex10;
@property (retain) NSNumber * Flex11;
@property (retain) NSNumber * Flex12;
@property (retain) NSNumber * Flex13;
@property (retain) USBoolean * Flex14;
@property (retain) USBoolean * Flex15;
@property (retain) USBoolean * Flex16;
@property (retain) USBoolean * Flex17;
@property (retain) NSDate * Flex18;
@property (retain) NSDate * Flex19;
@property (retain) NSString * Flex2;
@property (retain) NSDate * Flex20;
@property (retain) NSDate * Flex21;
@property (retain) NSNumber * Flex22;
@property (retain) NSString * Flex25;
@property (retain) NSString * Flex26;
@property (retain) NSString * Flex27;
@property (retain) NSString * Flex3;
@property (retain) NSString * Flex30;
@property (retain) NSString * Flex31;
@property (retain) NSString * Flex32;
@property (retain) NSString * Flex33;
@property (retain) NSString * Flex34;
@property (retain) NSString * Flex35;
@property (retain) NSString * Flex36;
@property (retain) NSString * Flex37;
@property (retain) NSString * Flex38;
@property (retain) NSString * Flex39;
@property (retain) NSDate * Flex4;
@property (retain) NSNumber * Flex40;
@property (retain) NSNumber * Flex41;
@property (retain) NSNumber * Flex42;
@property (retain) NSNumber * Flex43;
@property (retain) NSNumber * Flex44;
@property (retain) NSNumber * Flex6;
@property (retain) NSNumber * Flex7;
@property (retain) NSNumber * Flex8;
@property (retain) NSNumber * Flex9;
@property (retain) NSString * FullName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NumDateOff;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSString * OrgUnitName;
@property (retain) NSNumber * Priority;
@property (retain) NSDate * StartOff;
@property (retain) NSNumber * Status;
@property (retain) NSString * StatusName;
@property (retain) NSString * SysParamNameEN;
@property (retain) NSString * SysParamNameVN;
@property (retain) NSNumber * TimeRequestMasTerId;
@property (retain) NSString * Type;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfSP_ESS_USER_REQUISITION_LIST_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_ESS_USER_REQUISITION_LIST_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfSP_ESS_USER_REQUISITION_LIST_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_ESS_USER_REQUISITION_LIST_Result:(time_tns1_SP_ESS_USER_REQUISITION_LIST_Result *)toAdd;
@property (readonly) NSMutableArray * SP_ESS_USER_REQUISITION_LIST_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TB_ESS_SYSTEM_PARAMETER : NSObject {
	
/* elements */
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * Flex1;
	NSNumber * Flex10;
	USBoolean * Flex11;
	USBoolean * Flex12;
	USBoolean * Flex13;
	USBoolean * Flex14;
	NSNumber * Flex15;
	NSNumber * Flex16;
	NSNumber * Flex17;
	NSNumber * Flex18;
	NSString * Flex19;
	NSString * Flex2;
	NSString * Flex20;
	NSString * Flex3;
	NSDate * Flex4;
	NSDate * Flex5;
	NSDate * Flex6;
	NSNumber * Flex7;
	NSNumber * Flex8;
	NSNumber * Flex9;
	NSNumber * IntFlex1;
	NSNumber * IntFlex2;
	NSNumber * IntFlex3;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * SysParamCode;
	NSNumber * SysParamCompanyId;
	NSString * SysParamDesc;
	NSNumber * SysParamId;
	NSString * SysParamKey;
	NSString * SysParamNameEN;
	NSString * SysParamNameVN;
	NSNumber * SysParamOrder;
	NSString * SysParamType;
	NSString * SysParamValue;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TB_ESS_SYSTEM_PARAMETER *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Flex1;
@property (retain) NSNumber * Flex10;
@property (retain) USBoolean * Flex11;
@property (retain) USBoolean * Flex12;
@property (retain) USBoolean * Flex13;
@property (retain) USBoolean * Flex14;
@property (retain) NSNumber * Flex15;
@property (retain) NSNumber * Flex16;
@property (retain) NSNumber * Flex17;
@property (retain) NSNumber * Flex18;
@property (retain) NSString * Flex19;
@property (retain) NSString * Flex2;
@property (retain) NSString * Flex20;
@property (retain) NSString * Flex3;
@property (retain) NSDate * Flex4;
@property (retain) NSDate * Flex5;
@property (retain) NSDate * Flex6;
@property (retain) NSNumber * Flex7;
@property (retain) NSNumber * Flex8;
@property (retain) NSNumber * Flex9;
@property (retain) NSNumber * IntFlex1;
@property (retain) NSNumber * IntFlex2;
@property (retain) NSNumber * IntFlex3;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * SysParamCode;
@property (retain) NSNumber * SysParamCompanyId;
@property (retain) NSString * SysParamDesc;
@property (retain) NSNumber * SysParamId;
@property (retain) NSString * SysParamKey;
@property (retain) NSString * SysParamNameEN;
@property (retain) NSString * SysParamNameVN;
@property (retain) NSNumber * SysParamOrder;
@property (retain) NSString * SysParamType;
@property (retain) NSString * SysParamValue;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTB_ESS_SYSTEM_PARAMETER : NSObject {
	
/* elements */
	NSMutableArray *TB_ESS_SYSTEM_PARAMETER;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTB_ESS_SYSTEM_PARAMETER *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTB_ESS_SYSTEM_PARAMETER:(time_tns1_TB_ESS_SYSTEM_PARAMETER *)toAdd;
@property (readonly) NSMutableArray * TB_ESS_SYSTEM_PARAMETER;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_SP_ESS_GET_APPROVE_ORG_Result : NSObject {
	
/* elements */
	NSString * NAME;
	NSString * ORGJOBPOSITIONCODE;
	NSNumber * ORGJOBPOSITIONID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_SP_ESS_GET_APPROVE_ORG_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * NAME;
@property (retain) NSString * ORGJOBPOSITIONCODE;
@property (retain) NSNumber * ORGJOBPOSITIONID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfSP_ESS_GET_APPROVE_ORG_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_ESS_GET_APPROVE_ORG_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfSP_ESS_GET_APPROVE_ORG_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_ESS_GET_APPROVE_ORG_Result:(time_tns1_SP_ESS_GET_APPROVE_ORG_Result *)toAdd;
@property (readonly) NSMutableArray * SP_ESS_GET_APPROVE_ORG_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TimeLeaveRequestDetail : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DayOff;
	NSString * FLex1;
	NSString * FLex2;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OffWorkType;
	NSNumber * TimeLeaveRequestDetailId;
	NSString * TimeRequestMasTerCode;
	NSNumber * TimeRequestMasTerId;
	NSString * TimeRequestMasTerType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TimeLeaveRequestDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DayOff;
@property (retain) NSString * FLex1;
@property (retain) NSString * FLex2;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OffWorkType;
@property (retain) NSNumber * TimeLeaveRequestDetailId;
@property (retain) NSString * TimeRequestMasTerCode;
@property (retain) NSNumber * TimeRequestMasTerId;
@property (retain) NSString * TimeRequestMasTerType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTimeLeaveRequestDetail : NSObject {
	
/* elements */
	NSMutableArray *TimeLeaveRequestDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTimeLeaveRequestDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeLeaveRequestDetail:(time_tns1_TimeLeaveRequestDetail *)toAdd;
@property (readonly) NSMutableArray * TimeLeaveRequestDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CBAccidentInsurance : NSObject {
	
/* elements */
	NSString * AccidentInsuranceContractCode;
	NSString * AccidentInsuranceContractNumber;
	NSString * AccidentName;
	NSNumber * CBAccidentInsuranceId;
	NSNumber * Charge;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * EffectDate;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSDate * ExpireDate;
	NSString * InsuranceCompanyName;
	NSString * InsuranceKind;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CBAccidentInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AccidentInsuranceContractCode;
@property (retain) NSString * AccidentInsuranceContractNumber;
@property (retain) NSString * AccidentName;
@property (retain) NSNumber * CBAccidentInsuranceId;
@property (retain) NSNumber * Charge;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EffectDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpireDate;
@property (retain) NSString * InsuranceCompanyName;
@property (retain) NSString * InsuranceKind;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfCBAccidentInsurance : NSObject {
	
/* elements */
	NSMutableArray *CBAccidentInsurance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfCBAccidentInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBAccidentInsurance:(time_tns1_CBAccidentInsurance *)toAdd;
@property (readonly) NSMutableArray * CBAccidentInsurance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CBCompensationTypeFactor : NSObject {
	
/* elements */
	NSNumber * CBCompensationTypeFactorId;
	NSNumber * CLogCBCompensationTypeId;
	time_tns1_CLogCBFactor * CLogCBFactor;
	NSNumber * CLogCBFactorId;
	USBoolean * IsDeleted;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CBCompensationTypeFactor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBCompensationTypeFactorId;
@property (retain) NSNumber * CLogCBCompensationTypeId;
@property (retain) time_tns1_CLogCBFactor * CLogCBFactor;
@property (retain) NSNumber * CLogCBFactorId;
@property (retain) USBoolean * IsDeleted;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfCBCompensationTypeFactor : NSObject {
	
/* elements */
	NSMutableArray *CBCompensationTypeFactor;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfCBCompensationTypeFactor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBCompensationTypeFactor:(time_tns1_CBCompensationTypeFactor *)toAdd;
@property (readonly) NSMutableArray * CBCompensationTypeFactor;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfCLogCBFactor : NSObject {
	
/* elements */
	NSMutableArray *CLogCBFactor;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfCLogCBFactor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogCBFactor:(time_tns1_CLogCBFactor *)toAdd;
@property (readonly) NSMutableArray * CLogCBFactor;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CLogCBCompensationCategory : NSObject {
	
/* elements */
	NSNumber * CLogCBCompensationCategoryId;
	time_tns1_ArrayOfCLogCBFactor * CLogCBFactors;
	NSString * CLogCompensationCategoryCode;
	NSString * Description;
	USBoolean * IsDeleted;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CLogCBCompensationCategory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogCBCompensationCategoryId;
@property (retain) time_tns1_ArrayOfCLogCBFactor * CLogCBFactors;
@property (retain) NSString * CLogCompensationCategoryCode;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CLogCBFactor : NSObject {
	
/* elements */
	time_tns1_ArrayOfCBCompensationTypeFactor * CBCompensationTypeFactors;
	time_tns1_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
	time_tns1_CLogCBCompensationCategory * CLogCBCompensationCategory;
	NSNumber * CLogCBCompensationCategoryId;
	NSString * CLogCBFactorCode;
	NSNumber * CLogCBFactorGroupId;
	NSNumber * CLogCBFactorId;
	NSString * Description;
	NSString * FormulaExpression;
	USBoolean * IsActived;
	USBoolean * IsDeactived;
	USBoolean * IsDeleted;
	USBoolean * IsInitial;
	NSString * Name;
	NSNumber * Priority;
	NSString * ServiceReferenceEndPointAddress;
	NSString * ServiceReferenceEndPointOperationContract;
	NSString * TableReference;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CLogCBFactor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_ArrayOfCBCompensationTypeFactor * CBCompensationTypeFactors;
@property (retain) time_tns1_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
@property (retain) time_tns1_CLogCBCompensationCategory * CLogCBCompensationCategory;
@property (retain) NSNumber * CLogCBCompensationCategoryId;
@property (retain) NSString * CLogCBFactorCode;
@property (retain) NSNumber * CLogCBFactorGroupId;
@property (retain) NSNumber * CLogCBFactorId;
@property (retain) NSString * Description;
@property (retain) NSString * FormulaExpression;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeactived;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInitial;
@property (retain) NSString * Name;
@property (retain) NSNumber * Priority;
@property (retain) NSString * ServiceReferenceEndPointAddress;
@property (retain) NSString * ServiceReferenceEndPointOperationContract;
@property (retain) NSString * TableReference;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CBFactorEmployeeMetaData : NSObject {
	
/* elements */
	NSDate * BeginDate;
	time_tns1_CBCompensationEmployeeGroup * CBCompensationEmployeeGroup;
	NSNumber * CBCompensationEmployeeGroupId;
	NSString * CBFactorEmployeeMetaDataDescription;
	NSNumber * CBFactorEmployeeMetaDataId;
	time_tns1_CLogCBFactor * CLogCBFactor;
	NSNumber * CLogCBFactorId;
	NSString * ChangeReason;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	NSNumber * FactorValue;
	NSString * GroupGuid;
	USBoolean * IsActivated;
	USBoolean * IsAppliedForCompany;
	USBoolean * IsChanged;
	USBoolean * IsCurrent;
	USBoolean * IsDeactivated;
	USBoolean * IsDeleted;
	USBoolean * IsInitialed;
	NSDate * ModifiedDate;
	NSNumber * OldFactorValue;
	NSString * ReferenceColunmName;
	NSString * ReferenceRowGuid;
	NSString * ReferenceTableName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CBFactorEmployeeMetaData *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * BeginDate;
@property (retain) time_tns1_CBCompensationEmployeeGroup * CBCompensationEmployeeGroup;
@property (retain) NSNumber * CBCompensationEmployeeGroupId;
@property (retain) NSString * CBFactorEmployeeMetaDataDescription;
@property (retain) NSNumber * CBFactorEmployeeMetaDataId;
@property (retain) time_tns1_CLogCBFactor * CLogCBFactor;
@property (retain) NSNumber * CLogCBFactorId;
@property (retain) NSString * ChangeReason;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) NSNumber * FactorValue;
@property (retain) NSString * GroupGuid;
@property (retain) USBoolean * IsActivated;
@property (retain) USBoolean * IsAppliedForCompany;
@property (retain) USBoolean * IsChanged;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeactivated;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInitialed;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OldFactorValue;
@property (retain) NSString * ReferenceColunmName;
@property (retain) NSString * ReferenceRowGuid;
@property (retain) NSString * ReferenceTableName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfCBFactorEmployeeMetaData : NSObject {
	
/* elements */
	NSMutableArray *CBFactorEmployeeMetaData;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfCBFactorEmployeeMetaData *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBFactorEmployeeMetaData:(time_tns1_CBFactorEmployeeMetaData *)toAdd;
@property (readonly) NSMutableArray * CBFactorEmployeeMetaData;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CBCompensationEmployeeGroup : NSObject {
	
/* elements */
	time_tns1_ArrayOfCBCompensationEmployeeGroupDetail * CBCompensationEmployeeGroupDetails;
	NSNumber * CBCompensationEmployeeGroupId;
	time_tns1_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
	NSNumber * CompanyId;
	NSString * Description;
	USBoolean * IsActived;
	USBoolean * IsDeactived;
	USBoolean * IsDeleted;
	USBoolean * IsInitial;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CBCompensationEmployeeGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_ArrayOfCBCompensationEmployeeGroupDetail * CBCompensationEmployeeGroupDetails;
@property (retain) NSNumber * CBCompensationEmployeeGroupId;
@property (retain) time_tns1_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeactived;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInitial;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CBCompensationEmployeeGroupDetail : NSObject {
	
/* elements */
	time_tns1_CBCompensationEmployeeGroup * CBCompensationEmployeeGroup;
	NSNumber * CBCompensationEmployeeGroupDetailId;
	NSNumber * CBCompensationEmployeeGroupId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CBCompensationEmployeeGroupDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_CBCompensationEmployeeGroup * CBCompensationEmployeeGroup;
@property (retain) NSNumber * CBCompensationEmployeeGroupDetailId;
@property (retain) NSNumber * CBCompensationEmployeeGroupId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfCBCompensationEmployeeGroupDetail : NSObject {
	
/* elements */
	NSMutableArray *CBCompensationEmployeeGroupDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfCBCompensationEmployeeGroupDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBCompensationEmployeeGroupDetail:(time_tns1_CBCompensationEmployeeGroupDetail *)toAdd;
@property (readonly) NSMutableArray * CBCompensationEmployeeGroupDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CBConvalescence : NSObject {
	
/* elements */
	NSNumber * AllowanceAmount;
	NSNumber * CBConvalescenceId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSDate * FromDate;
	NSNumber * HealthLessLevel;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * NumberDayOff;
	NSDate * NumberOfMounthApplied;
	NSDate * ToDate;
	NSNumber * TypeId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CBConvalescence *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AllowanceAmount;
@property (retain) NSNumber * CBConvalescenceId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * FromDate;
@property (retain) NSNumber * HealthLessLevel;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * NumberDayOff;
@property (retain) NSDate * NumberOfMounthApplied;
@property (retain) NSDate * ToDate;
@property (retain) NSNumber * TypeId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfCBConvalescence : NSObject {
	
/* elements */
	NSMutableArray *CBConvalescence;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfCBConvalescence *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBConvalescence:(time_tns1_CBConvalescence *)toAdd;
@property (readonly) NSMutableArray * CBConvalescence;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CLogCBDisease : NSObject {
	
/* elements */
	time_tns1_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
	NSNumber * CLogCBDiseaseId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	NSString * DiseaseCode;
	NSString * DiseaseGroupCode;
	USBoolean * IsChronic;
	USBoolean * IsDeleted;
	USBoolean * IsOccupational;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CLogCBDisease *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
@property (retain) NSNumber * CLogCBDiseaseId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) NSString * DiseaseCode;
@property (retain) NSString * DiseaseGroupCode;
@property (retain) USBoolean * IsChronic;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOccupational;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CBHealthInsurance : NSObject {
	
/* elements */
	NSNumber * CBHealthInsuranceId;
	time_tns1_CLogHospital * CLogHospital;
	NSNumber * CLogHospitalId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * EffectDate;
	NSDate * ExpireDate;
	NSNumber * GrossCharge;
	NSString * HealthInsuranceContractCode;
	NSString * HealthInsuranceContractNumber;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Remark;
	NSNumber * SignerId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CBHealthInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBHealthInsuranceId;
@property (retain) time_tns1_CLogHospital * CLogHospital;
@property (retain) NSNumber * CLogHospitalId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EffectDate;
@property (retain) NSDate * ExpireDate;
@property (retain) NSNumber * GrossCharge;
@property (retain) NSString * HealthInsuranceContractCode;
@property (retain) NSString * HealthInsuranceContractNumber;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Remark;
@property (retain) NSNumber * SignerId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfCBHealthInsurance : NSObject {
	
/* elements */
	NSMutableArray *CBHealthInsurance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfCBHealthInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBHealthInsurance:(time_tns1_CBHealthInsurance *)toAdd;
@property (readonly) NSMutableArray * CBHealthInsurance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfCLogCity : NSObject {
	
/* elements */
	NSMutableArray *CLogCity;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfCLogCity *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogCity:(time_tns1_CLogCity *)toAdd;
@property (readonly) NSMutableArray * CLogCity;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CLogCountry : NSObject {
	
/* elements */
	time_tns1_ArrayOfCLogCity * CLogCities;
	NSString * CLogCountryCode;
	NSNumber * CLogCountryId;
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CLogCountry *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_ArrayOfCLogCity * CLogCities;
@property (retain) NSString * CLogCountryCode;
@property (retain) NSNumber * CLogCountryId;
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CLogDistrict : NSObject {
	
/* elements */
	time_tns1_CLogCity * CLogCity;
	NSString * CLogCityCode;
	NSNumber * CLogCityId;
	NSString * CLogDistrictCode;
	NSNumber * CLogDistrictId;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CLogDistrict *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_CLogCity * CLogCity;
@property (retain) NSString * CLogCityCode;
@property (retain) NSNumber * CLogCityId;
@property (retain) NSString * CLogDistrictCode;
@property (retain) NSNumber * CLogDistrictId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfCLogDistrict : NSObject {
	
/* elements */
	NSMutableArray *CLogDistrict;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfCLogDistrict *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogDistrict:(time_tns1_CLogDistrict *)toAdd;
@property (readonly) NSMutableArray * CLogDistrict;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfCLogHospital : NSObject {
	
/* elements */
	NSMutableArray *CLogHospital;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfCLogHospital *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogHospital:(time_tns1_CLogHospital *)toAdd;
@property (readonly) NSMutableArray * CLogHospital;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmployee : NSObject {
	
/* elements */
	NSMutableArray *Employee;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmployee:(time_tns1_Employee *)toAdd;
@property (readonly) NSMutableArray * Employee;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CLogCity : NSObject {
	
/* elements */
	NSString * CLogCityCode;
	NSNumber * CLogCityId;
	NSString * CLogCityNoIns;
	time_tns1_CLogCountry * CLogCountry;
	NSString * CLogCountryCode;
	NSNumber * CLogCountryId;
	time_tns1_ArrayOfCLogDistrict * CLogDistricts;
	time_tns1_ArrayOfCLogHospital * CLogHospitals;
	NSNumber * CompanyId;
	time_tns1_ArrayOfEmployee * Employees;
	time_tns1_ArrayOfEmployee * Employees1;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
	NSString * SysNo;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CLogCity *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogCityCode;
@property (retain) NSNumber * CLogCityId;
@property (retain) NSString * CLogCityNoIns;
@property (retain) time_tns1_CLogCountry * CLogCountry;
@property (retain) NSString * CLogCountryCode;
@property (retain) NSNumber * CLogCountryId;
@property (retain) time_tns1_ArrayOfCLogDistrict * CLogDistricts;
@property (retain) time_tns1_ArrayOfCLogHospital * CLogHospitals;
@property (retain) NSNumber * CompanyId;
@property (retain) time_tns1_ArrayOfEmployee * Employees;
@property (retain) time_tns1_ArrayOfEmployee * Employees1;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
@property (retain) NSString * SysNo;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CLogHospital : NSObject {
	
/* elements */
	NSString * Address;
	time_tns1_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
	time_tns1_ArrayOfCBHealthInsurance * CBHealthInsurances;
	time_tns1_CLogCity * CLogCity;
	NSNumber * CLogCityId;
	NSString * CLogCityNoIns;
	NSString * CLogHospitalCode;
	NSNumber * CLogHospitalId;
	NSNumber * CompanyId;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CLogHospital *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) time_tns1_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
@property (retain) time_tns1_ArrayOfCBHealthInsurance * CBHealthInsurances;
@property (retain) time_tns1_CLogCity * CLogCity;
@property (retain) NSNumber * CLogCityId;
@property (retain) NSString * CLogCityNoIns;
@property (retain) NSString * CLogHospitalCode;
@property (retain) NSNumber * CLogHospitalId;
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CBDayOffSocialInsurance : NSObject {
	
/* elements */
	NSNumber * CBDayOffSocialInsuranceId;
	time_tns1_CLogCBDisease * CLogCBDisease;
	NSNumber * CLogCBDiseaseId;
	time_tns1_CLogHospital * CLogHospital;
	NSNumber * CLogHospitalId;
	NSNumber * ClogCBDayOffTypeId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * DoctorName;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSDate * FromDate;
	USBoolean * IsDeleted;
	USBoolean * IsPaid;
	USBoolean * IsWorkAccident;
	NSDate * ModifiedDate;
	NSNumber * NumberDayOff;
	NSNumber * NumberDayOffByDoctor;
	NSNumber * NumberPaidDay;
	NSString * Reason;
	NSDate * ToDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CBDayOffSocialInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBDayOffSocialInsuranceId;
@property (retain) time_tns1_CLogCBDisease * CLogCBDisease;
@property (retain) NSNumber * CLogCBDiseaseId;
@property (retain) time_tns1_CLogHospital * CLogHospital;
@property (retain) NSNumber * CLogHospitalId;
@property (retain) NSNumber * ClogCBDayOffTypeId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DoctorName;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * FromDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsPaid;
@property (retain) USBoolean * IsWorkAccident;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NumberDayOff;
@property (retain) NSNumber * NumberDayOffByDoctor;
@property (retain) NSNumber * NumberPaidDay;
@property (retain) NSString * Reason;
@property (retain) NSDate * ToDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfCBDayOffSocialInsurance : NSObject {
	
/* elements */
	NSMutableArray *CBDayOffSocialInsurance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfCBDayOffSocialInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBDayOffSocialInsurance:(time_tns1_CBDayOffSocialInsurance *)toAdd;
@property (readonly) NSMutableArray * CBDayOffSocialInsurance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CBHealthInsuranceDetail : NSObject {
	
/* elements */
	NSNumber * CBHealthInsuranceDetailId;
	NSNumber * CBHealthInsuranceId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CBHealthInsuranceDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBHealthInsuranceDetailId;
@property (retain) NSNumber * CBHealthInsuranceId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfCBHealthInsuranceDetail : NSObject {
	
/* elements */
	NSMutableArray *CBHealthInsuranceDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfCBHealthInsuranceDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBHealthInsuranceDetail:(time_tns1_CBHealthInsuranceDetail *)toAdd;
@property (readonly) NSMutableArray * CBHealthInsuranceDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CLogEmployeeType : NSObject {
	
/* elements */
	NSString * CLogEmployeeTypeCode;
	NSNumber * CLogEmployeeTypeId;
	time_tns1_ArrayOfEmployee * Employees;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CLogEmployeeType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogEmployeeTypeCode;
@property (retain) NSNumber * CLogEmployeeTypeId;
@property (retain) time_tns1_ArrayOfEmployee * Employees;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_Employer : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_ArrayOfEmpContract * EmpContracts;
	NSString * EmployerCode;
	NSNumber * EmployerId;
	NSString * FullName;
	USBoolean * IsDeleted;
	NSString * JobPositionName;
	NSDate * ModifiedDate;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_Employer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_ArrayOfEmpContract * EmpContracts;
@property (retain) NSString * EmployerCode;
@property (retain) NSNumber * EmployerId;
@property (retain) NSString * FullName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * JobPositionName;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * DecisionFile;
	NSString * DecisionName;
	NSString * DecisionNo;
	NSString * DirectReportToEmployeeCode;
	NSNumber * DirectReportToEmployeeId;
	NSString * EmpJobPositionCode;
	NSNumber * EmpProfileJobPositionId;
	time_tns1_Employee * Employee;
	time_tns1_Employee * Employee1;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ExpirationDate;
	NSDate * ImplementationDate;
	USBoolean * IsActive;
	USBoolean * IsConcurrentPost;
	USBoolean * IsDeleted;
	USBoolean * IsMainPost;
	USBoolean * IsPromoted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * OrgJobExternalTitleCode;
	time_tns1_OrgJobPosition * OrgJobPosition;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSNumber * PercentParticipation;
	NSString * Reason;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DecisionFile;
@property (retain) NSString * DecisionName;
@property (retain) NSString * DecisionNo;
@property (retain) NSString * DirectReportToEmployeeCode;
@property (retain) NSNumber * DirectReportToEmployeeId;
@property (retain) NSString * EmpJobPositionCode;
@property (retain) NSNumber * EmpProfileJobPositionId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) time_tns1_Employee * Employee1;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsConcurrentPost;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainPost;
@property (retain) USBoolean * IsPromoted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * OrgJobExternalTitleCode;
@property (retain) time_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * PercentParticipation;
@property (retain) NSString * Reason;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileJobPosition : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileJobPosition:(time_tns1_EmpProfileJobPosition *)toAdd;
@property (readonly) NSMutableArray * EmpProfileJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_OrgJobPositionRequiredProficency : NSObject {
	
/* elements */
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	time_tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSNumber * OrgJobPositionRequiredProficencyId;
	NSString * ProficiencyTypeCode;
	NSNumber * RecordId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_OrgJobPositionRequiredProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * OrgJobPositionRequiredProficencyId;
@property (retain) NSString * ProficiencyTypeCode;
@property (retain) NSNumber * RecordId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfOrgJobPositionRequiredProficency : NSObject {
	
/* elements */
	NSMutableArray *OrgJobPositionRequiredProficency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfOrgJobPositionRequiredProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgJobPositionRequiredProficency:(time_tns1_OrgJobPositionRequiredProficency *)toAdd;
@property (readonly) NSMutableArray * OrgJobPositionRequiredProficency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_OrgCoreCompetency : NSObject {
	
/* elements */
	time_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompetencyLevel;
	NSDate * CreatedDate;
	NSString * Definition;
	time_tns1_ArrayOfEmpCompetency * EmpCompetencies;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * OrgCoreCompetencyId;
	NSString * Statement;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_OrgCoreCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompetencyLevel;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Definition;
@property (retain) time_tns1_ArrayOfEmpCompetency * EmpCompetencies;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * OrgCoreCompetencyId;
@property (retain) NSString * Statement;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpCompetency : NSObject {
	
/* elements */
	time_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpCompetencyId;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	time_tns1_OrgCoreCompetency * OrgCoreCompetency;
	NSNumber * OrgCoreCompetencyId;
	time_tns1_OrgProficencyLevel * OrgProficencyLevel;
	NSNumber * OrgProficencyLevelId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpCompetencyId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_OrgCoreCompetency * OrgCoreCompetency;
@property (retain) NSNumber * OrgCoreCompetencyId;
@property (retain) time_tns1_OrgProficencyLevel * OrgProficencyLevel;
@property (retain) NSNumber * OrgProficencyLevelId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpCompetency : NSObject {
	
/* elements */
	NSMutableArray *EmpCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpCompetency:(time_tns1_EmpCompetency *)toAdd;
@property (readonly) NSMutableArray * EmpCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProficencyLevelRating : NSObject {
	
/* elements */
	NSMutableArray *EmpProficencyLevelRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProficencyLevelRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProficencyLevelRating:(time_tns1_EmpProficencyLevelRating *)toAdd;
@property (readonly) NSMutableArray * EmpProficencyLevelRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpCompetencyRating : NSObject {
	
/* elements */
	NSDate * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpCompetencyRatingId;
	time_tns1_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpCompetencyRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpCompetencyRatingId;
@property (retain) time_tns1_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProficencyLevelDetailRating : NSObject {
	
/* elements */
	NSMutableArray *EmpProficencyLevelDetailRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProficencyLevelDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProficencyLevelDetailRating:(time_tns1_EmpProficencyLevelDetailRating *)toAdd;
@property (readonly) NSMutableArray * EmpProficencyLevelDetailRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProficencyLevelRating : NSObject {
	
/* elements */
	NSDate * AppraisalDate;
	time_tns1_CLogRating * CLogRating;
	NSDate * CreatedDate;
	time_tns1_EmpCompetencyRating * EmpCompetencyRating;
	NSNumber * EmpCompetencyRatingId;
	time_tns1_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
	NSNumber * EmpProficencyLevelRatingId;
	NSString * ManagerNote;
	NSNumber * ManagerRating;
	time_tns1_OrgProficencyLevel * OrgProficencyLevel;
	NSNumber * OrgProficencyLevelId;
	NSNumber * PeriodicallyAssessment;
	NSNumber * Score;
	NSString * SelfNote;
	NSNumber * SelfRating;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProficencyLevelRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AppraisalDate;
@property (retain) time_tns1_CLogRating * CLogRating;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_EmpCompetencyRating * EmpCompetencyRating;
@property (retain) NSNumber * EmpCompetencyRatingId;
@property (retain) time_tns1_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
@property (retain) NSNumber * EmpProficencyLevelRatingId;
@property (retain) NSString * ManagerNote;
@property (retain) NSNumber * ManagerRating;
@property (retain) time_tns1_OrgProficencyLevel * OrgProficencyLevel;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) NSNumber * PeriodicallyAssessment;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfNote;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecCanProficencyLevelDetailRating : NSObject {
	
/* elements */
	NSMutableArray *RecCanProficencyLevelDetailRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecCanProficencyLevelDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCanProficencyLevelDetailRating:(time_tns1_RecCanProficencyLevelDetailRating *)toAdd;
@property (readonly) NSMutableArray * RecCanProficencyLevelDetailRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecCandidateProfileStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * Priority;
	NSString * ProfileStatusCode;
	NSString * ProfileStatusName;
	time_tns1_ArrayOfRecCandidateApplication * RecCandidateApplications;
	NSNumber * RecCandidateProfileStatusId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecCandidateProfileStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Priority;
@property (retain) NSString * ProfileStatusCode;
@property (retain) NSString * ProfileStatusName;
@property (retain) time_tns1_ArrayOfRecCandidateApplication * RecCandidateApplications;
@property (retain) NSNumber * RecCandidateProfileStatusId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecEvaluationCriterion : NSObject {
	
/* elements */
	NSMutableArray *RecEvaluationCriterion;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecEvaluationCriterion *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecEvaluationCriterion:(time_tns1_RecEvaluationCriterion *)toAdd;
@property (readonly) NSMutableArray * RecEvaluationCriterion;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecGroupEvaluationCriterion : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * Priority;
	time_tns1_ArrayOfRecEvaluationCriterion * RecEvaluationCriterions;
	NSString * RecGroupEvalDesc;
	NSString * RecGroupEvalName;
	NSNumber * RecGroupEvaluationCriterionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecGroupEvaluationCriterion *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Priority;
@property (retain) time_tns1_ArrayOfRecEvaluationCriterion * RecEvaluationCriterions;
@property (retain) NSString * RecGroupEvalDesc;
@property (retain) NSString * RecGroupEvalName;
@property (retain) NSNumber * RecGroupEvaluationCriterionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecInterviewPhaseEvaluation : NSObject {
	
/* elements */
	NSMutableArray *RecInterviewPhaseEvaluation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecInterviewPhaseEvaluation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecInterviewPhaseEvaluation:(time_tns1_RecInterviewPhaseEvaluation *)toAdd;
@property (readonly) NSMutableArray * RecInterviewPhaseEvaluation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecEvaluationCriterion : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * MaxPoint;
	NSNumber * Priority;
	NSString * RecEvalDesc;
	NSString * RecEvalName;
	NSNumber * RecEvaluationCriterionId;
	time_tns1_RecGroupEvaluationCriterion * RecGroupEvaluationCriterion;
	NSNumber * RecGroupEvaluationCriterionId;
	time_tns1_ArrayOfRecInterviewPhaseEvaluation * RecInterviewPhaseEvaluations;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecEvaluationCriterion *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * MaxPoint;
@property (retain) NSNumber * Priority;
@property (retain) NSString * RecEvalDesc;
@property (retain) NSString * RecEvalName;
@property (retain) NSNumber * RecEvaluationCriterionId;
@property (retain) time_tns1_RecGroupEvaluationCriterion * RecGroupEvaluationCriterion;
@property (retain) NSNumber * RecGroupEvaluationCriterionId;
@property (retain) time_tns1_ArrayOfRecInterviewPhaseEvaluation * RecInterviewPhaseEvaluations;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecInterviewPhase : NSObject {
	
/* elements */
	NSMutableArray *RecInterviewPhase;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecInterviewPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecInterviewPhase:(time_tns1_RecInterviewPhase *)toAdd;
@property (readonly) NSMutableArray * RecInterviewPhase;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecProcessPhaseResult : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Point_;
	time_tns1_RecCandidate * RecCandidate;
	time_tns1_RecCandidateApplication * RecCandidateApplication;
	NSNumber * RecCandidateApplicationId;
	NSNumber * RecCandidateId;
	NSNumber * RecProcessPhaseResultId;
	time_tns1_RecRecruitmentProcessPhase * RecRecruitmentProcessPhase;
	NSNumber * RecRecruitmentProcessPhaseId;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecProcessPhaseResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Point_;
@property (retain) time_tns1_RecCandidate * RecCandidate;
@property (retain) time_tns1_RecCandidateApplication * RecCandidateApplication;
@property (retain) NSNumber * RecCandidateApplicationId;
@property (retain) NSNumber * RecCandidateId;
@property (retain) NSNumber * RecProcessPhaseResultId;
@property (retain) time_tns1_RecRecruitmentProcessPhase * RecRecruitmentProcessPhase;
@property (retain) NSNumber * RecRecruitmentProcessPhaseId;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecProcessPhaseResult : NSObject {
	
/* elements */
	NSMutableArray *RecProcessPhaseResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecProcessPhaseResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecProcessPhaseResult:(time_tns1_RecProcessPhaseResult *)toAdd;
@property (readonly) NSMutableArray * RecProcessPhaseResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecProcessApplyForJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	time_tns1_OrgJobPosition * OrgJobPosition;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSNumber * RecProcessApplyForJobPositionId;
	time_tns1_RecRecruitmentProcess * RecRecruitmentProcess;
	NSNumber * RecRecruitmentProcessId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecProcessApplyForJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * RecProcessApplyForJobPositionId;
@property (retain) time_tns1_RecRecruitmentProcess * RecRecruitmentProcess;
@property (retain) NSNumber * RecRecruitmentProcessId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecProcessApplyForJobPosition : NSObject {
	
/* elements */
	NSMutableArray *RecProcessApplyForJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecProcessApplyForJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecProcessApplyForJobPosition:(time_tns1_RecProcessApplyForJobPosition *)toAdd;
@property (readonly) NSMutableArray * RecProcessApplyForJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecRecruitmentProcessPhase : NSObject {
	
/* elements */
	NSMutableArray *RecRecruitmentProcessPhase;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecRecruitmentProcessPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRecruitmentProcessPhase:(time_tns1_RecRecruitmentProcessPhase *)toAdd;
@property (readonly) NSMutableArray * RecRecruitmentProcessPhase;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecRecruitmentProcess : NSObject {
	
/* elements */
	NSDate * ApplyDate;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	time_tns1_ArrayOfRecProcessApplyForJobPosition * RecProcessApplyForJobPositions;
	NSString * RecProcessDescription;
	NSString * RecProcessName;
	NSNumber * RecRecruitmentProcessId;
	time_tns1_ArrayOfRecRecruitmentProcessPhase * RecRecruitmentProcessPhases;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecRecruitmentProcess *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ApplyDate;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_ArrayOfRecProcessApplyForJobPosition * RecProcessApplyForJobPositions;
@property (retain) NSString * RecProcessDescription;
@property (retain) NSString * RecProcessName;
@property (retain) NSNumber * RecRecruitmentProcessId;
@property (retain) time_tns1_ArrayOfRecRecruitmentProcessPhase * RecRecruitmentProcessPhases;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecRecruitmentProcessPhase : NSObject {
	
/* elements */
	NSNumber * CoefficientPoint;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Priority;
	time_tns1_ArrayOfRecInterviewPhase * RecInterviewPhases;
	NSString * RecProcessPhaseName;
	time_tns1_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
	time_tns1_RecRecruitmentProcess * RecRecruitmentProcess;
	NSNumber * RecRecruitmentProcessId;
	NSNumber * RecRecruitmentProcessPhaseId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecRecruitmentProcessPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CoefficientPoint;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Priority;
@property (retain) time_tns1_ArrayOfRecInterviewPhase * RecInterviewPhases;
@property (retain) NSString * RecProcessPhaseName;
@property (retain) time_tns1_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
@property (retain) time_tns1_RecRecruitmentProcess * RecRecruitmentProcess;
@property (retain) NSNumber * RecRecruitmentProcessId;
@property (retain) NSNumber * RecRecruitmentProcessPhaseId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecInterviewPhase : NSObject {
	
/* elements */
	NSNumber * CoefficientPoint;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * InterviewPhaseName;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Priority;
	time_tns1_ArrayOfRecInterviewPhaseEvaluation * RecInterviewPhaseEvaluations;
	NSNumber * RecInterviewPhaseId;
	time_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
	NSNumber * RecProcessPhaseId;
	time_tns1_RecRecruitmentProcessPhase * RecRecruitmentProcessPhase;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecInterviewPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CoefficientPoint;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * InterviewPhaseName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Priority;
@property (retain) time_tns1_ArrayOfRecInterviewPhaseEvaluation * RecInterviewPhaseEvaluations;
@property (retain) NSNumber * RecInterviewPhaseId;
@property (retain) time_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
@property (retain) NSNumber * RecProcessPhaseId;
@property (retain) time_tns1_RecRecruitmentProcessPhase * RecRecruitmentProcessPhase;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecInterviewPhaseEvaluation : NSObject {
	
/* elements */
	NSNumber * ComanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	time_tns1_RecEvaluationCriterion * RecEvaluationCriterion;
	NSNumber * RecEvaluationCriterionId;
	time_tns1_RecInterviewPhase * RecInterviewPhase;
	NSNumber * RecInterviewPhaseEvaluationId;
	NSNumber * RecInterviewPhaseId;
	time_tns1_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecInterviewPhaseEvaluation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ComanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_RecEvaluationCriterion * RecEvaluationCriterion;
@property (retain) NSNumber * RecEvaluationCriterionId;
@property (retain) time_tns1_RecInterviewPhase * RecInterviewPhase;
@property (retain) NSNumber * RecInterviewPhaseEvaluationId;
@property (retain) NSNumber * RecInterviewPhaseId;
@property (retain) time_tns1_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecScheduleInterviewerResult : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Point_;
	time_tns1_RecInterviewPhaseEvaluation * RecInterviewPhaseEvaluation;
	NSNumber * RecInterviewPhaseEvaluationId;
	time_tns1_RecInterviewSchedule * RecInterviewSchedule;
	NSNumber * RecInterviewScheduleId;
	time_tns1_RecInterviewer * RecInterviewer;
	NSNumber * RecInterviewerId;
	NSNumber * RecScheduleInterviewerResultId;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecScheduleInterviewerResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Point_;
@property (retain) time_tns1_RecInterviewPhaseEvaluation * RecInterviewPhaseEvaluation;
@property (retain) NSNumber * RecInterviewPhaseEvaluationId;
@property (retain) time_tns1_RecInterviewSchedule * RecInterviewSchedule;
@property (retain) NSNumber * RecInterviewScheduleId;
@property (retain) time_tns1_RecInterviewer * RecInterviewer;
@property (retain) NSNumber * RecInterviewerId;
@property (retain) NSNumber * RecScheduleInterviewerResultId;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecScheduleInterviewerResult : NSObject {
	
/* elements */
	NSMutableArray *RecScheduleInterviewerResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecScheduleInterviewerResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecScheduleInterviewerResult:(time_tns1_RecScheduleInterviewerResult *)toAdd;
@property (readonly) NSMutableArray * RecScheduleInterviewerResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecInterviewer : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsMainInterviewer;
	NSDate * ModifiedDate;
	time_tns1_RecGroupInterviewer * RecGroupInterviewer;
	NSNumber * RecGroupInterviewerId;
	NSNumber * RecInterviewerId;
	time_tns1_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecInterviewer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainInterviewer;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_RecGroupInterviewer * RecGroupInterviewer;
@property (retain) NSNumber * RecGroupInterviewerId;
@property (retain) NSNumber * RecInterviewerId;
@property (retain) time_tns1_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecInterviewer : NSObject {
	
/* elements */
	NSMutableArray *RecInterviewer;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecInterviewer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecInterviewer:(time_tns1_RecInterviewer *)toAdd;
@property (readonly) NSMutableArray * RecInterviewer;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecGroupInterviewer : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * GroupName;
	USBoolean * IsDeleted;
	NSNumber * RecGroupInterviewerId;
	time_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
	time_tns1_ArrayOfRecInterviewer * RecInterviewers;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecGroupInterviewer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * GroupName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecGroupInterviewerId;
@property (retain) time_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
@property (retain) time_tns1_ArrayOfRecInterviewer * RecInterviewers;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecInterviewScheduleStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * Priority;
	NSNumber * RecInterviewScheduleStatusId;
	time_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
	NSString * StatusCode;
	NSString * StatusName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecInterviewScheduleStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Priority;
@property (retain) NSNumber * RecInterviewScheduleStatusId;
@property (retain) time_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
@property (retain) NSString * StatusCode;
@property (retain) NSString * StatusName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecInterviewSchedule : NSObject {
	
/* elements */
	NSNumber * AveragePoint;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeCreateId;
	NSDate * InterviewDate;
	NSString * InterviewPlace;
	NSDate * InterviewTime;
	USBoolean * IsDeleted;
	USBoolean * IsIntervieweeNotified;
	USBoolean * IsInterviewerNotified;
	NSDate * ModifiedDate;
	time_tns1_RecCandidateApplication * RecCandidateApplication;
	NSNumber * RecCandidateApplicationId;
	time_tns1_RecGroupInterviewer * RecGroupInterviewer;
	NSNumber * RecGroupInterviewerId;
	time_tns1_RecInterviewPhase * RecInterviewPhase;
	NSNumber * RecInterviewPhaseId;
	NSNumber * RecInterviewScheduleId;
	time_tns1_RecInterviewScheduleStatu * RecInterviewScheduleStatu;
	NSNumber * RecInterviewScheduleStatusId;
	time_tns1_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecInterviewSchedule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AveragePoint;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeCreateId;
@property (retain) NSDate * InterviewDate;
@property (retain) NSString * InterviewPlace;
@property (retain) NSDate * InterviewTime;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsIntervieweeNotified;
@property (retain) USBoolean * IsInterviewerNotified;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_RecCandidateApplication * RecCandidateApplication;
@property (retain) NSNumber * RecCandidateApplicationId;
@property (retain) time_tns1_RecGroupInterviewer * RecGroupInterviewer;
@property (retain) NSNumber * RecGroupInterviewerId;
@property (retain) time_tns1_RecInterviewPhase * RecInterviewPhase;
@property (retain) NSNumber * RecInterviewPhaseId;
@property (retain) NSNumber * RecInterviewScheduleId;
@property (retain) time_tns1_RecInterviewScheduleStatu * RecInterviewScheduleStatu;
@property (retain) NSNumber * RecInterviewScheduleStatusId;
@property (retain) time_tns1_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecInterviewSchedule : NSObject {
	
/* elements */
	NSMutableArray *RecInterviewSchedule;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecInterviewSchedule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecInterviewSchedule:(time_tns1_RecInterviewSchedule *)toAdd;
@property (readonly) NSMutableArray * RecInterviewSchedule;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecPhaseEmpDisplaced : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	time_tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSString * Reason;
	NSNumber * RecPhaseEmpDisplaced1;
	time_tns1_RecRecruitmentPhase * RecRecruitmentPhase;
	NSNumber * RecRecruitmentPhaseId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecPhaseEmpDisplaced *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * Reason;
@property (retain) NSNumber * RecPhaseEmpDisplaced1;
@property (retain) time_tns1_RecRecruitmentPhase * RecRecruitmentPhase;
@property (retain) NSNumber * RecRecruitmentPhaseId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecPhaseEmpDisplaced : NSObject {
	
/* elements */
	NSMutableArray *RecPhaseEmpDisplaced;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecPhaseEmpDisplaced *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecPhaseEmpDisplaced:(time_tns1_RecPhaseEmpDisplaced *)toAdd;
@property (readonly) NSMutableArray * RecPhaseEmpDisplaced;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecRecruitmentPhase : NSObject {
	
/* elements */
	NSMutableArray *RecRecruitmentPhase;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecRecruitmentPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRecruitmentPhase:(time_tns1_RecRecruitmentPhase *)toAdd;
@property (readonly) NSMutableArray * RecRecruitmentPhase;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecPhaseStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSString * PhaseStatusName;
	NSNumber * RecPhaseStatusId;
	time_tns1_ArrayOfRecRecruitmentPhase * RecRecruitmentPhases;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecPhaseStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * PhaseStatusName;
@property (retain) NSNumber * RecPhaseStatusId;
@property (retain) time_tns1_ArrayOfRecRecruitmentPhase * RecRecruitmentPhases;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecRecruitmentPhaseJobPosition : NSObject {
	
/* elements */
	NSMutableArray *RecRecruitmentPhaseJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecRecruitmentPhaseJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRecruitmentPhaseJobPosition:(time_tns1_RecRecruitmentPhaseJobPosition *)toAdd;
@property (readonly) NSMutableArray * RecRecruitmentPhaseJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_SysRecPlanApprover : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsMainApprover;
	NSNumber * Priority;
	time_tns1_ArrayOfRecPlanApproveHistory * RecPlanApproveHistories;
	NSNumber * SysRecPlanApproverId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_SysRecPlanApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainApprover;
@property (retain) NSNumber * Priority;
@property (retain) time_tns1_ArrayOfRecPlanApproveHistory * RecPlanApproveHistories;
@property (retain) NSNumber * SysRecPlanApproverId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecPlanApproveHistory : NSObject {
	
/* elements */
	NSDate * Date;
	USBoolean * IsApproved;
	USBoolean * IsDeleted;
	NSNumber * RecPlanApproveHistoryId;
	time_tns1_RecRecruitmentPlan * RecRecruitmentPlan;
	NSNumber * RecRecruitmentPlanId;
	NSString * Remark;
	time_tns1_SysRecPlanApprover * SysRecPlanApprover;
	NSNumber * SysRecPlanApproverId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecPlanApproveHistory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * Date;
@property (retain) USBoolean * IsApproved;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecPlanApproveHistoryId;
@property (retain) time_tns1_RecRecruitmentPlan * RecRecruitmentPlan;
@property (retain) NSNumber * RecRecruitmentPlanId;
@property (retain) NSString * Remark;
@property (retain) time_tns1_SysRecPlanApprover * SysRecPlanApprover;
@property (retain) NSNumber * SysRecPlanApproverId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecPlanApproveHistory : NSObject {
	
/* elements */
	NSMutableArray *RecPlanApproveHistory;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecPlanApproveHistory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecPlanApproveHistory:(time_tns1_RecPlanApproveHistory *)toAdd;
@property (readonly) NSMutableArray * RecPlanApproveHistory;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecPlanJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	time_tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSNumber * RecPlanJobPositionId;
	NSString * RecReason;
	time_tns1_RecRecruitmentPlan * RecRecruitmentPlan;
	NSNumber * RecRecruitmentPlanId;
	NSNumber * RecruitedQuantity;
	NSNumber * RequirementQuantity;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecPlanJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * RecPlanJobPositionId;
@property (retain) NSString * RecReason;
@property (retain) time_tns1_RecRecruitmentPlan * RecRecruitmentPlan;
@property (retain) NSNumber * RecRecruitmentPlanId;
@property (retain) NSNumber * RecruitedQuantity;
@property (retain) NSNumber * RequirementQuantity;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecPlanJobPosition : NSObject {
	
/* elements */
	NSMutableArray *RecPlanJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecPlanJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecPlanJobPosition:(time_tns1_RecPlanJobPosition *)toAdd;
@property (readonly) NSMutableArray * RecPlanJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecRecruitmentPlan : NSObject {
	
/* elements */
	NSMutableArray *RecRecruitmentPlan;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecRecruitmentPlan *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRecruitmentPlan:(time_tns1_RecRecruitmentPlan *)toAdd;
@property (readonly) NSMutableArray * RecRecruitmentPlan;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecPlanStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * RecPlanStatusId;
	time_tns1_ArrayOfRecRecruitmentPlan * RecRecruitmentPlans;
	NSString * StatusName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecPlanStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecPlanStatusId;
@property (retain) time_tns1_ArrayOfRecRecruitmentPlan * RecRecruitmentPlans;
@property (retain) NSString * StatusName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecRecruitmentRequirement : NSObject {
	
/* elements */
	NSMutableArray *RecRecruitmentRequirement;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecRecruitmentRequirement *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRecruitmentRequirement:(time_tns1_RecRecruitmentRequirement *)toAdd;
@property (readonly) NSMutableArray * RecRecruitmentRequirement;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecRecruitmentPlan : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * PlanDescription;
	time_tns1_ArrayOfRecPlanApproveHistory * RecPlanApproveHistories;
	NSNumber * RecPlanApproveHistoryId;
	time_tns1_ArrayOfRecPlanJobPosition * RecPlanJobPositions;
	time_tns1_RecPlanStatu * RecPlanStatu;
	NSNumber * RecPlanStatusId;
	NSString * RecRecruitmentPlanCode;
	NSNumber * RecRecruitmentPlanId;
	time_tns1_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
	NSDate * StartDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecRecruitmentPlan *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * PlanDescription;
@property (retain) time_tns1_ArrayOfRecPlanApproveHistory * RecPlanApproveHistories;
@property (retain) NSNumber * RecPlanApproveHistoryId;
@property (retain) time_tns1_ArrayOfRecPlanJobPosition * RecPlanJobPositions;
@property (retain) time_tns1_RecPlanStatu * RecPlanStatu;
@property (retain) NSNumber * RecPlanStatusId;
@property (retain) NSString * RecRecruitmentPlanCode;
@property (retain) NSNumber * RecRecruitmentPlanId;
@property (retain) time_tns1_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
@property (retain) NSDate * StartDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_SysRecRequirementApprover : NSObject {
	
/* elements */
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsMainApprover;
	NSNumber * Priority;
	time_tns1_ArrayOfRecRequirementApproveHistory * RecRequirementApproveHistories;
	NSNumber * SysRecRecruitmentApproverId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_SysRecRequirementApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainApprover;
@property (retain) NSNumber * Priority;
@property (retain) time_tns1_ArrayOfRecRequirementApproveHistory * RecRequirementApproveHistories;
@property (retain) NSNumber * SysRecRecruitmentApproverId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecRequirementApproveHistory : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * Date;
	USBoolean * IsApproved;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * RecRecruitmentApproveHistoryId;
	time_tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
	NSNumber * RecRequirementId;
	NSString * Remark;
	NSNumber * SysRecRecruitmentApproverId;
	time_tns1_SysRecRequirementApprover * SysRecRequirementApprover;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecRequirementApproveHistory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * Date;
@property (retain) USBoolean * IsApproved;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * RecRecruitmentApproveHistoryId;
@property (retain) time_tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
@property (retain) NSNumber * RecRequirementId;
@property (retain) NSString * Remark;
@property (retain) NSNumber * SysRecRecruitmentApproverId;
@property (retain) time_tns1_SysRecRequirementApprover * SysRecRequirementApprover;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecRequirementApproveHistory : NSObject {
	
/* elements */
	NSMutableArray *RecRequirementApproveHistory;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecRequirementApproveHistory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRequirementApproveHistory:(time_tns1_RecRequirementApproveHistory *)toAdd;
@property (readonly) NSMutableArray * RecRequirementApproveHistory;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecRequirementEmpDisplaced : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	time_tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSString * Reason;
	time_tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
	NSNumber * RecRecruitmentRequirementId;
	NSNumber * RecRequirementEmpDisplaced1;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecRequirementEmpDisplaced *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * Reason;
@property (retain) time_tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
@property (retain) NSNumber * RecRecruitmentRequirementId;
@property (retain) NSNumber * RecRequirementEmpDisplaced1;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecRequirementEmpDisplaced : NSObject {
	
/* elements */
	NSMutableArray *RecRequirementEmpDisplaced;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecRequirementEmpDisplaced *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRequirementEmpDisplaced:(time_tns1_RecRequirementEmpDisplaced *)toAdd;
@property (readonly) NSMutableArray * RecRequirementEmpDisplaced;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecRequirementJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	USBoolean * IsDisplacement;
	NSString * JobDescription;
	NSNumber * JobDescriptionId;
	NSDate * ModifiedDate;
	time_tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSString * RecReason;
	time_tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
	NSNumber * RecRecruitmentRequirementId;
	NSNumber * RecRequirementJobPositionId;
	NSNumber * RecruitedQuantity;
	NSNumber * RequirementQuantity;
	NSString * WorkingLocation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecRequirementJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDisplacement;
@property (retain) NSString * JobDescription;
@property (retain) NSNumber * JobDescriptionId;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * RecReason;
@property (retain) time_tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
@property (retain) NSNumber * RecRecruitmentRequirementId;
@property (retain) NSNumber * RecRequirementJobPositionId;
@property (retain) NSNumber * RecruitedQuantity;
@property (retain) NSNumber * RequirementQuantity;
@property (retain) NSString * WorkingLocation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecRequirementJobPosition : NSObject {
	
/* elements */
	NSMutableArray *RecRequirementJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecRequirementJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRequirementJobPosition:(time_tns1_RecRequirementJobPosition *)toAdd;
@property (readonly) NSMutableArray * RecRequirementJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecRequirementStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	time_tns1_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
	NSNumber * RecRequirementStatusId;
	NSString * StatusName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecRequirementStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) time_tns1_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
@property (retain) NSNumber * RecRequirementStatusId;
@property (retain) NSString * StatusName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecRecruitmentRequirement : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgUnitId;
	NSDate * RecBeginDate;
	NSDate * RecEndDate;
	NSNumber * RecRecruitmentApproveHistoryId;
	time_tns1_ArrayOfRecRecruitmentPhase * RecRecruitmentPhases;
	time_tns1_RecRecruitmentPlan * RecRecruitmentPlan;
	NSNumber * RecRecruitmentPlanId;
	NSNumber * RecRecruitmentRequirementId;
	time_tns1_ArrayOfRecRequirementApproveHistory * RecRequirementApproveHistories;
	time_tns1_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
	time_tns1_ArrayOfRecRequirementJobPosition * RecRequirementJobPositions;
	time_tns1_RecRequirementStatu * RecRequirementStatu;
	NSNumber * RecRequirementStatusId;
	NSNumber * RecruitedQuantity;
	NSString * RecruitmentPurpose;
	NSString * Remark;
	NSString * RequirementCode;
	NSDate * RequirementDate;
	NSString * RequirementName;
	NSNumber * TotalQuantity;
	NSNumber * UrgentLevel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecRecruitmentRequirement *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSDate * RecBeginDate;
@property (retain) NSDate * RecEndDate;
@property (retain) NSNumber * RecRecruitmentApproveHistoryId;
@property (retain) time_tns1_ArrayOfRecRecruitmentPhase * RecRecruitmentPhases;
@property (retain) time_tns1_RecRecruitmentPlan * RecRecruitmentPlan;
@property (retain) NSNumber * RecRecruitmentPlanId;
@property (retain) NSNumber * RecRecruitmentRequirementId;
@property (retain) time_tns1_ArrayOfRecRequirementApproveHistory * RecRequirementApproveHistories;
@property (retain) time_tns1_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
@property (retain) time_tns1_ArrayOfRecRequirementJobPosition * RecRequirementJobPositions;
@property (retain) time_tns1_RecRequirementStatu * RecRequirementStatu;
@property (retain) NSNumber * RecRequirementStatusId;
@property (retain) NSNumber * RecruitedQuantity;
@property (retain) NSString * RecruitmentPurpose;
@property (retain) NSString * Remark;
@property (retain) NSString * RequirementCode;
@property (retain) NSDate * RequirementDate;
@property (retain) NSString * RequirementName;
@property (retain) NSNumber * TotalQuantity;
@property (retain) NSNumber * UrgentLevel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecRecruitmentPhase : NSObject {
	
/* elements */
	NSDate * ApplicationDate;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	USBoolean * IsNotified;
	NSDate * ModifiedDate;
	NSString * PhaseName;
	time_tns1_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
	time_tns1_RecPhaseStatu * RecPhaseStatu;
	NSNumber * RecPhaseStatusId;
	NSString * RecRecruitmentPhaseCode;
	NSNumber * RecRecruitmentPhaseId;
	time_tns1_ArrayOfRecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPositions;
	time_tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
	NSNumber * RecRecruitmentRequirementId;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecRecruitmentPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ApplicationDate;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsNotified;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * PhaseName;
@property (retain) time_tns1_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
@property (retain) time_tns1_RecPhaseStatu * RecPhaseStatu;
@property (retain) NSNumber * RecPhaseStatusId;
@property (retain) NSString * RecRecruitmentPhaseCode;
@property (retain) NSNumber * RecRecruitmentPhaseId;
@property (retain) time_tns1_ArrayOfRecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPositions;
@property (retain) time_tns1_RecRecruitmentRequirement * RecRecruitmentRequirement;
@property (retain) NSNumber * RecRecruitmentRequirementId;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecRecruitmentPhaseJobPosition : NSObject {
	
/* elements */
	NSString * AgeLimitation;
	NSString * BeginSalaryLevel;
	NSString * BenefitAllowance;
	NSNumber * CompanyId;
	NSString * ContractTerm;
	NSDate * CreatedDate;
	NSNumber * DirectLeader;
	NSDate * FromDate;
	NSString * GenderLimitation;
	USBoolean * HasProbation;
	USBoolean * IsDeleted;
	USBoolean * IsDisplacement;
	NSString * JobDescription;
	NSDate * ModifiedDate;
	time_tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSString * ProbationTime;
	time_tns1_ArrayOfRecCandidateApplication * RecCandidateApplications;
	time_tns1_RecRecruitmentPhase * RecRecruitmentPhase;
	NSNumber * RecRecruitmentPhaseId;
	NSNumber * RecRecruitmentPhaseJobPositionId;
	NSNumber * RecruitedQuantity;
	NSNumber * RequirementQuantity;
	NSDate * StartWorkDate;
	NSDate * ToDate;
	NSString * WorkingLocation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecRecruitmentPhaseJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AgeLimitation;
@property (retain) NSString * BeginSalaryLevel;
@property (retain) NSString * BenefitAllowance;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ContractTerm;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * DirectLeader;
@property (retain) NSDate * FromDate;
@property (retain) NSString * GenderLimitation;
@property (retain) USBoolean * HasProbation;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDisplacement;
@property (retain) NSString * JobDescription;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * ProbationTime;
@property (retain) time_tns1_ArrayOfRecCandidateApplication * RecCandidateApplications;
@property (retain) time_tns1_RecRecruitmentPhase * RecRecruitmentPhase;
@property (retain) NSNumber * RecRecruitmentPhaseId;
@property (retain) NSNumber * RecRecruitmentPhaseJobPositionId;
@property (retain) NSNumber * RecruitedQuantity;
@property (retain) NSNumber * RequirementQuantity;
@property (retain) NSDate * StartWorkDate;
@property (retain) NSDate * ToDate;
@property (retain) NSString * WorkingLocation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecCandidateApplication : NSObject {
	
/* elements */
	NSNumber * AveragePoint;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * CurrentInterviewPhaseId;
	NSNumber * CurrentProcessPhaseId;
	NSString * HRRemark;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSDate * NegotiateDate;
	time_tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateApplicationId;
	NSNumber * RecCandidateId;
	time_tns1_RecCandidateProfileStatu * RecCandidateProfileStatu;
	NSNumber * RecCandidateProfileStatusId;
	time_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
	time_tns1_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
	time_tns1_RecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPosition;
	NSNumber * RecRecruitmentPhaseJobPositionId;
	NSDate * StartWorkingDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecCandidateApplication *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AveragePoint;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * CurrentInterviewPhaseId;
@property (retain) NSNumber * CurrentProcessPhaseId;
@property (retain) NSString * HRRemark;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * NegotiateDate;
@property (retain) time_tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateApplicationId;
@property (retain) NSNumber * RecCandidateId;
@property (retain) time_tns1_RecCandidateProfileStatu * RecCandidateProfileStatu;
@property (retain) NSNumber * RecCandidateProfileStatusId;
@property (retain) time_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
@property (retain) time_tns1_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
@property (retain) time_tns1_RecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPosition;
@property (retain) NSNumber * RecRecruitmentPhaseJobPositionId;
@property (retain) NSDate * StartWorkingDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecCandidateApplication : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateApplication;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecCandidateApplication *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateApplication:(time_tns1_RecCandidateApplication *)toAdd;
@property (readonly) NSMutableArray * RecCandidateApplication;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecCandidateCompetencyRating : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateCompetencyRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecCandidateCompetencyRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateCompetencyRating:(time_tns1_RecCandidateCompetencyRating *)toAdd;
@property (readonly) NSMutableArray * RecCandidateCompetencyRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CLogDegree : NSObject {
	
/* elements */
	NSNumber * CLogDegreeId;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * Note;
	time_tns1_ArrayOfRecCandidateDegree * RecCandidateDegrees;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CLogDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogDegreeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) time_tns1_ArrayOfRecCandidateDegree * RecCandidateDegrees;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecCandidateDegree : NSObject {
	
/* elements */
	time_tns1_CLogDegree * CLogDegree;
	NSNumber * CLogDegreeId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	USBoolean * IsDeleted;
	USBoolean * IsValId;
	NSDate * ModifiedDate;
	NSString * Other;
	NSString * PlaceIssue;
	time_tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateDegreeId;
	NSNumber * RecCandidateId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecCandidateDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_CLogDegree * CLogDegree;
@property (retain) NSNumber * CLogDegreeId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) time_tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateDegreeId;
@property (retain) NSNumber * RecCandidateId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecCandidateDegree : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateDegree;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecCandidateDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateDegree:(time_tns1_RecCandidateDegree *)toAdd;
@property (readonly) NSMutableArray * RecCandidateDegree;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfOrgExperience : NSObject {
	
/* elements */
	NSMutableArray *OrgExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfOrgExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgExperience:(time_tns1_OrgExperience *)toAdd;
@property (readonly) NSMutableArray * OrgExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_OrgProjectType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	time_tns1_ArrayOfOrgExperience * OrgExperiences;
	NSNumber * OrgProjectTypeId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_OrgProjectType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) time_tns1_ArrayOfOrgExperience * OrgExperiences;
@property (retain) NSNumber * OrgProjectTypeId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_OrgTimeInCharge : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	time_tns1_ArrayOfOrgExperience * OrgExperiences;
	NSNumber * OrgTimeInChargeId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_OrgTimeInCharge *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_ArrayOfOrgExperience * OrgExperiences;
@property (retain) NSNumber * OrgTimeInChargeId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_OrgExperience : NSObject {
	
/* elements */
	NSString * Achievement;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgExperienceId;
	time_tns1_OrgProjectType * OrgProjectType;
	NSNumber * OrgProjectTypeId;
	time_tns1_OrgTimeInCharge * OrgTimeInCharge;
	NSNumber * OrgTimeInChargeId;
	time_tns1_ArrayOfRecCandidateExperience * RecCandidateExperiences;
	NSString * RoleDescription;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_OrgExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Achievement;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgExperienceId;
@property (retain) time_tns1_OrgProjectType * OrgProjectType;
@property (retain) NSNumber * OrgProjectTypeId;
@property (retain) time_tns1_OrgTimeInCharge * OrgTimeInCharge;
@property (retain) NSNumber * OrgTimeInChargeId;
@property (retain) time_tns1_ArrayOfRecCandidateExperience * RecCandidateExperiences;
@property (retain) NSString * RoleDescription;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecCandidateExperience : NSObject {
	
/* elements */
	time_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompanyName;
	NSDate * CreatedDate;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	USBoolean * IsInternal;
	NSString * JobPosition;
	NSDate * ModifiedDate;
	NSString * NotableAchievements;
	NSString * Note;
	time_tns1_OrgExperience * OrgExperience;
	NSNumber * OrgExperienceId;
	time_tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateExperienceId;
	NSNumber * RecCandidateId;
	NSString * Responsibilities;
	NSDate * StartDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecCandidateExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompanyName;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInternal;
@property (retain) NSString * JobPosition;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * NotableAchievements;
@property (retain) NSString * Note;
@property (retain) time_tns1_OrgExperience * OrgExperience;
@property (retain) NSNumber * OrgExperienceId;
@property (retain) time_tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateExperienceId;
@property (retain) NSNumber * RecCandidateId;
@property (retain) NSString * Responsibilities;
@property (retain) NSDate * StartDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecCandidateExperience : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecCandidateExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateExperience:(time_tns1_RecCandidateExperience *)toAdd;
@property (readonly) NSMutableArray * RecCandidateExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CBPITDependent : NSObject {
	
/* elements */
	NSNumber * CBPITDependentId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DependentBeginDate;
	NSString * DependentDemonstrative;
	NSDate * DependentEndDate;
	time_tns1_EmpProfileFamilyRelationship * EmpProfileFamilyRelationship;
	NSNumber * EmpProfileFamilyRelationshipId;
	USBoolean * IsDeleted;
	USBoolean * IsDependent;
	NSDate * ModifiedDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CBPITDependent *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBPITDependentId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DependentBeginDate;
@property (retain) NSString * DependentDemonstrative;
@property (retain) NSDate * DependentEndDate;
@property (retain) time_tns1_EmpProfileFamilyRelationship * EmpProfileFamilyRelationship;
@property (retain) NSNumber * EmpProfileFamilyRelationshipId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDependent;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfCBPITDependent : NSObject {
	
/* elements */
	NSMutableArray *CBPITDependent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfCBPITDependent *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBPITDependent:(time_tns1_CBPITDependent *)toAdd;
@property (readonly) NSMutableArray * CBPITDependent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpBeneficiary : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpBeneficiaryId;
	NSNumber * EmpFamilyRelationshipId;
	time_tns1_EmpProfileFamilyRelationship * EmpProfileFamilyRelationship;
	NSDate * ImplementationDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpBeneficiary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpBeneficiaryId;
@property (retain) NSNumber * EmpFamilyRelationshipId;
@property (retain) time_tns1_EmpProfileFamilyRelationship * EmpProfileFamilyRelationship;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpBeneficiary : NSObject {
	
/* elements */
	NSMutableArray *EmpBeneficiary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpBeneficiary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpBeneficiary:(time_tns1_EmpBeneficiary *)toAdd;
@property (readonly) NSMutableArray * EmpBeneficiary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileFamilyRelationship : NSObject {
	
/* elements */
	NSString * Address;
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSDate * BirthDay;
	time_tns1_ArrayOfCBPITDependent * CBPITDependents;
	time_tns1_CLogFamilyRelationship * CLogFamilyRelationship;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DeathDate;
	time_tns1_ArrayOfEmpBeneficiary * EmpBeneficiaries;
	NSNumber * EmpProfileFamilyRelationshipId;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * HomeTown;
	USBoolean * IsDeleted;
	USBoolean * IsEmergencyContact;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	USBoolean * Nourish;
	NSString * Occupation;
	NSString * OccupationPlace;
	NSString * PersonFirstName;
	NSString * PersonFullName;
	NSString * PersonLastName;
	NSNumber * PersonSameOccupationId;
	NSString * Phone;
	NSNumber * RelateRowId;
	NSNumber * RelationshipId;
	NSString * RelationshipName;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSDate * BirthDay;
@property (retain) time_tns1_ArrayOfCBPITDependent * CBPITDependents;
@property (retain) time_tns1_CLogFamilyRelationship * CLogFamilyRelationship;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DeathDate;
@property (retain) time_tns1_ArrayOfEmpBeneficiary * EmpBeneficiaries;
@property (retain) NSNumber * EmpProfileFamilyRelationshipId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * HomeTown;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEmergencyContact;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) USBoolean * Nourish;
@property (retain) NSString * Occupation;
@property (retain) NSString * OccupationPlace;
@property (retain) NSString * PersonFirstName;
@property (retain) NSString * PersonFullName;
@property (retain) NSString * PersonLastName;
@property (retain) NSNumber * PersonSameOccupationId;
@property (retain) NSString * Phone;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSNumber * RelationshipId;
@property (retain) NSString * RelationshipName;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileFamilyRelationship : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileFamilyRelationship;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileFamilyRelationship:(time_tns1_EmpProfileFamilyRelationship *)toAdd;
@property (readonly) NSMutableArray * EmpProfileFamilyRelationship;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CLogFamilyRelationship : NSObject {
	
/* elements */
	NSNumber * CLogFamilyRelationshipId;
	NSNumber * CompanyId;
	time_tns1_ArrayOfEmpProfileFamilyRelationship * EmpProfileFamilyRelationships;
	NSString * FamilyRelationshipCode;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
	time_tns1_ArrayOfRecCandidateFamilyRelationship * RecCandidateFamilyRelationships;
	USBoolean * Sex;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CLogFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogFamilyRelationshipId;
@property (retain) NSNumber * CompanyId;
@property (retain) time_tns1_ArrayOfEmpProfileFamilyRelationship * EmpProfileFamilyRelationships;
@property (retain) NSString * FamilyRelationshipCode;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
@property (retain) time_tns1_ArrayOfRecCandidateFamilyRelationship * RecCandidateFamilyRelationships;
@property (retain) USBoolean * Sex;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecCandidateFamilyRelationship : NSObject {
	
/* elements */
	NSString * Address;
	NSDate * BirthDay;
	time_tns1_CLogFamilyRelationship * CLogFamilyRelationship;
	NSNumber * CLogFamilyRelationshipId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DeathDate;
	NSString * HomeTown;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	USBoolean * Nourish;
	NSString * Occupation;
	NSString * OccupationPlace;
	NSString * PersonFirstName;
	NSString * PersonFullName;
	NSString * PersonLastName;
	NSNumber * PersonSameOccupationId;
	NSString * Phone;
	time_tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateFamilyRelationshipId;
	NSNumber * RecCandidateId;
	NSString * RelationshipName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecCandidateFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSDate * BirthDay;
@property (retain) time_tns1_CLogFamilyRelationship * CLogFamilyRelationship;
@property (retain) NSNumber * CLogFamilyRelationshipId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DeathDate;
@property (retain) NSString * HomeTown;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) USBoolean * Nourish;
@property (retain) NSString * Occupation;
@property (retain) NSString * OccupationPlace;
@property (retain) NSString * PersonFirstName;
@property (retain) NSString * PersonFullName;
@property (retain) NSString * PersonLastName;
@property (retain) NSNumber * PersonSameOccupationId;
@property (retain) NSString * Phone;
@property (retain) time_tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateFamilyRelationshipId;
@property (retain) NSNumber * RecCandidateId;
@property (retain) NSString * RelationshipName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecCandidateFamilyRelationship : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateFamilyRelationship;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecCandidateFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateFamilyRelationship:(time_tns1_RecCandidateFamilyRelationship *)toAdd;
@property (readonly) NSMutableArray * RecCandidateFamilyRelationship;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecCandidateForeignLanguage : NSObject {
	
/* elements */
	NSString * AttachmenttUrl;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Degree;
	NSDate * Duration_;
	NSDate * EffectiveDate;
	USBoolean * IsDeleted;
	NSString * Language;
	NSString * LanguageSkill;
	NSDate * ModifiedDate;
	NSString * Note;
	time_tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateForeignLanguageId;
	NSNumber * RecCandidateId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecCandidateForeignLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AttachmenttUrl;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Degree;
@property (retain) NSDate * Duration_;
@property (retain) NSDate * EffectiveDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Language;
@property (retain) NSString * LanguageSkill;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) time_tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateForeignLanguageId;
@property (retain) NSNumber * RecCandidateId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecCandidateForeignLanguage : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateForeignLanguage;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecCandidateForeignLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateForeignLanguage:(time_tns1_RecCandidateForeignLanguage *)toAdd;
@property (readonly) NSMutableArray * RecCandidateForeignLanguage;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileQualification : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	NSNumber * EmpProfileQualificationId;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsValId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * OrgDegreeRankId;
	time_tns1_OrgQualification * OrgQualification;
	NSNumber * OrgQualificationId;
	NSString * Other;
	NSString * PlaceIssue;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) NSNumber * EmpProfileQualificationId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgDegreeRankId;
@property (retain) time_tns1_OrgQualification * OrgQualification;
@property (retain) NSNumber * OrgQualificationId;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileQualification : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileQualification;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileQualification:(time_tns1_EmpProfileQualification *)toAdd;
@property (readonly) NSMutableArray * EmpProfileQualification;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_OrgQualification : NSObject {
	
/* elements */
	NSNumber * CLogEducationLevelId;
	NSNumber * CLogMajorId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * Description;
	time_tns1_ArrayOfEmpProfileQualification * EmpProfileQualifications;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * OrgQualificationCode;
	NSNumber * OrgQualificationId;
	NSString * OrgQualificationName;
	time_tns1_ArrayOfRecCandidateQualification * RecCandidateQualifications;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_OrgQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogEducationLevelId;
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) time_tns1_ArrayOfEmpProfileQualification * EmpProfileQualifications;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OrgQualificationCode;
@property (retain) NSNumber * OrgQualificationId;
@property (retain) NSString * OrgQualificationName;
@property (retain) time_tns1_ArrayOfRecCandidateQualification * RecCandidateQualifications;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecCandidateQualification : NSObject {
	
/* elements */
	time_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	USBoolean * IsDeleted;
	USBoolean * IsValId;
	NSDate * ModifiedDate;
	time_tns1_OrgQualification * OrgQualification;
	NSNumber * OrgQualificationId;
	NSString * Other;
	NSString * PlaceIssue;
	time_tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateId;
	NSNumber * RecCandidateQualificationId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecCandidateQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValId;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_OrgQualification * OrgQualification;
@property (retain) NSNumber * OrgQualificationId;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) time_tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateId;
@property (retain) NSNumber * RecCandidateQualificationId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecCandidateQualification : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateQualification;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecCandidateQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateQualification:(time_tns1_RecCandidateQualification *)toAdd;
@property (readonly) NSMutableArray * RecCandidateQualification;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileSkill : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	time_tns1_CLogRating * CLogRating;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * DocumentRef;
	NSNumber * EmpProfileSkillId;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	time_tns1_OrgSkill * OrgSkill;
	NSNumber * OrgSkillId;
	NSNumber * RatingId;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) time_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DocumentRef;
@property (retain) NSNumber * EmpProfileSkillId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_OrgSkill * OrgSkill;
@property (retain) NSNumber * OrgSkillId;
@property (retain) NSNumber * RatingId;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileSkill : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileSkill:(time_tns1_EmpProfileSkill *)toAdd;
@property (readonly) NSMutableArray * EmpProfileSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfOrgSkill : NSObject {
	
/* elements */
	NSMutableArray *OrgSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfOrgSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgSkill:(time_tns1_OrgSkill *)toAdd;
@property (readonly) NSMutableArray * OrgSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_OrgSkillType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * OrgSkillTypeId;
	time_tns1_ArrayOfOrgSkill * OrgSkills;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_OrgSkillType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * OrgSkillTypeId;
@property (retain) time_tns1_ArrayOfOrgSkill * OrgSkills;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTrainingCourseSchedule : NSObject {
	
/* elements */
	NSMutableArray *TrainingCourseSchedule;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTrainingCourseSchedule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingCourseSchedule:(time_tns1_TrainingCourseSchedule *)toAdd;
@property (readonly) NSMutableArray * TrainingCourseSchedule;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CLogCourseSchedule : NSObject {
	
/* elements */
	NSNumber * ClogCourseScheduleId;
	NSNumber * DayOfWeek;
	NSDate * EndTime;
	USBoolean * IsDeleted;
	NSNumber * Shift;
	NSDate * StartTime;
	time_tns1_ArrayOfTrainingCourseSchedule * TrainingCourseSchedules;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CLogCourseSchedule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ClogCourseScheduleId;
@property (retain) NSNumber * DayOfWeek;
@property (retain) NSDate * EndTime;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Shift;
@property (retain) NSDate * StartTime;
@property (retain) time_tns1_ArrayOfTrainingCourseSchedule * TrainingCourseSchedules;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTrainingCourse : NSObject {
	
/* elements */
	NSMutableArray *TrainingCourse;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTrainingCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingCourse:(time_tns1_TrainingCourse *)toAdd;
@property (readonly) NSMutableArray * TrainingCourse;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CLogTrainingCenter : NSObject {
	
/* elements */
	NSNumber * CLogTrainingCenterId;
	USBoolean * IsDeleted;
	NSString * Name;
	time_tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CLogTrainingCenter *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogTrainingCenterId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) time_tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TrainingCategory : NSObject {
	
/* elements */
	NSDate * CreateDate;
	NSString * Description;
	NSString * ImgUrl;
	USBoolean * IsDeleted;
	NSString * Name;
	NSNumber * TrainingCategoryId;
	time_tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TrainingCategory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) NSString * ImgUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSNumber * TrainingCategoryId;
@property (retain) time_tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TrainingCourseChapter : NSObject {
	
/* elements */
	NSNumber * Chapter;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * Session;
	time_tns1_TrainingCourse * TrainingCourse;
	NSNumber * TrainingCourseChapterId;
	NSNumber * TrainingCourseId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TrainingCourseChapter *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Chapter;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * Session;
@property (retain) time_tns1_TrainingCourse * TrainingCourse;
@property (retain) NSNumber * TrainingCourseChapterId;
@property (retain) NSNumber * TrainingCourseId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTrainingCourseChapter : NSObject {
	
/* elements */
	NSMutableArray *TrainingCourseChapter;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTrainingCourseChapter *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingCourseChapter:(time_tns1_TrainingCourseChapter *)toAdd;
@property (readonly) NSMutableArray * TrainingCourseChapter;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TrainingCoursePeriod : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
	NSNumber * Session;
	NSString * Target;
	NSNumber * TrainingCoursePeriodId;
	time_tns1_ArrayOfTrainingCourse * TrainingCourses;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TrainingCoursePeriod *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) NSNumber * Session;
@property (retain) NSString * Target;
@property (retain) NSNumber * TrainingCoursePeriodId;
@property (retain) time_tns1_ArrayOfTrainingCourse * TrainingCourses;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TrainingCourseType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * Note;
	NSNumber * TrainingCourseTypeId;
	time_tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TrainingCourseType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) NSNumber * TrainingCourseTypeId;
@property (retain) time_tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TrainingCourseUnit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * OrgUnitId;
	time_tns1_TrainingCourse * TrainingCourse;
	NSNumber * TrainingCourseId;
	NSNumber * TrainingCourseUnitId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TrainingCourseUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * OrgUnitId;
@property (retain) time_tns1_TrainingCourse * TrainingCourse;
@property (retain) NSNumber * TrainingCourseId;
@property (retain) NSNumber * TrainingCourseUnitId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTrainingCourseUnit : NSObject {
	
/* elements */
	NSMutableArray *TrainingCourseUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTrainingCourseUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingCourseUnit:(time_tns1_TrainingCourseUnit *)toAdd;
@property (readonly) NSMutableArray * TrainingCourseUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TrainingPlanDegree : NSObject {
	
/* elements */
	NSNumber * CLogDegreeId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	time_tns1_TrainingCourse * TrainingCourse;
	NSNumber * TrainingPlanDegreeId;
	NSNumber * TraningCourseId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TrainingPlanDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogDegreeId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_TrainingCourse * TrainingCourse;
@property (retain) NSNumber * TrainingPlanDegreeId;
@property (retain) NSNumber * TraningCourseId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTrainingPlanDegree : NSObject {
	
/* elements */
	NSMutableArray *TrainingPlanDegree;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTrainingPlanDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingPlanDegree:(time_tns1_TrainingPlanDegree *)toAdd;
@property (readonly) NSMutableArray * TrainingPlanDegree;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileEducation : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	time_tns1_CLogMajor * CLogMajor;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileEducationId;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EndYear;
	USBoolean * IsDeleted;
	NSString * Major;
	NSNumber * MajorId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * RelateRowId;
	NSNumber * StartYear;
	NSString * University;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileEducation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) time_tns1_CLogMajor * CLogMajor;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileEducationId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EndYear;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Major;
@property (retain) NSNumber * MajorId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSNumber * StartYear;
@property (retain) NSString * University;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileEducation : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileEducation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileEducation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileEducation:(time_tns1_EmpProfileEducation *)toAdd;
@property (readonly) NSMutableArray * EmpProfileEducation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTrainingPlanRequest : NSObject {
	
/* elements */
	NSMutableArray *TrainingPlanRequest;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTrainingPlanRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingPlanRequest:(time_tns1_TrainingPlanRequest *)toAdd;
@property (readonly) NSMutableArray * TrainingPlanRequest;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CLogMajor : NSObject {
	
/* elements */
	NSNumber * CLogCareerId;
	NSString * CLogMajorCode;
	NSNumber * CLogMajorId;
	time_tns1_ArrayOfEmpProfileEducation * EmpProfileEducations;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	time_tns1_ArrayOfTrainingPlanRequest * TrainingPlanRequests;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CLogMajor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogCareerId;
@property (retain) NSString * CLogMajorCode;
@property (retain) NSNumber * CLogMajorId;
@property (retain) time_tns1_ArrayOfEmpProfileEducation * EmpProfileEducations;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) time_tns1_ArrayOfTrainingPlanRequest * TrainingPlanRequests;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TrainingPlanEmployee : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSDate * CreatedDate;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * TrainingPlanEmployeeId;
	time_tns1_TrainingPlanRequest * TrainingPlanRequest;
	NSNumber * TrainingPlanRequestId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TrainingPlanEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * TrainingPlanEmployeeId;
@property (retain) time_tns1_TrainingPlanRequest * TrainingPlanRequest;
@property (retain) NSNumber * TrainingPlanRequestId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTrainingPlanEmployee : NSObject {
	
/* elements */
	NSMutableArray *TrainingPlanEmployee;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTrainingPlanEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingPlanEmployee:(time_tns1_TrainingPlanEmployee *)toAdd;
@property (readonly) NSMutableArray * TrainingPlanEmployee;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TrainingPlanRequest : NSObject {
	
/* elements */
	NSNumber * ApproverId;
	time_tns1_CLogMajor * CLogMajor;
	NSNumber * CLogMajorId;
	NSNumber * CompanyId;
	NSString * Content;
	NSNumber * Cost;
	NSDate * CreatedDate;
	NSDate * EndDate;
	USBoolean * IsApproved;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSDate * StartDate;
	NSNumber * StrategyGoalId;
	NSString * Target;
	NSString * Title;
	time_tns1_ArrayOfTrainingCourse * TrainingCourses;
	time_tns1_ArrayOfTrainingPlanEmployee * TrainingPlanEmployees;
	NSNumber * TrainingPlanRequestId;
	NSNumber * TrainingPlanRequestTypeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TrainingPlanRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproverId;
@property (retain) time_tns1_CLogMajor * CLogMajor;
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Content;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsApproved;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * StartDate;
@property (retain) NSNumber * StrategyGoalId;
@property (retain) NSString * Target;
@property (retain) NSString * Title;
@property (retain) time_tns1_ArrayOfTrainingCourse * TrainingCourses;
@property (retain) time_tns1_ArrayOfTrainingPlanEmployee * TrainingPlanEmployees;
@property (retain) NSNumber * TrainingPlanRequestId;
@property (retain) NSNumber * TrainingPlanRequestTypeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TrainingProficencyExpected : NSObject {
	
/* elements */
	time_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	USBoolean * IsDeleted;
	NSNumber * ProficencyId;
	NSNumber * ProficencyType;
	time_tns1_TrainingCourse * TrainingCourse;
	NSNumber * TrainingCourseId;
	NSNumber * TrainingProficencyExpectedId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TrainingProficencyExpected *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ProficencyId;
@property (retain) NSNumber * ProficencyType;
@property (retain) time_tns1_TrainingCourse * TrainingCourse;
@property (retain) NSNumber * TrainingCourseId;
@property (retain) NSNumber * TrainingProficencyExpectedId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTrainingProficencyExpected : NSObject {
	
/* elements */
	NSMutableArray *TrainingProficencyExpected;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTrainingProficencyExpected *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingProficencyExpected:(time_tns1_TrainingProficencyExpected *)toAdd;
@property (readonly) NSMutableArray * TrainingProficencyExpected;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TrainingProficencyRequire : NSObject {
	
/* elements */
	time_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	USBoolean * IsDeleted;
	NSNumber * ProficencyId;
	NSNumber * ProficencyType;
	time_tns1_TrainingCourse * TrainingCourse;
	NSNumber * TrainingCourseId;
	NSNumber * TrainingProficencyRequireId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TrainingProficencyRequire *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ProficencyId;
@property (retain) NSNumber * ProficencyType;
@property (retain) time_tns1_TrainingCourse * TrainingCourse;
@property (retain) NSNumber * TrainingCourseId;
@property (retain) NSNumber * TrainingProficencyRequireId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTrainingProficencyRequire : NSObject {
	
/* elements */
	NSMutableArray *TrainingProficencyRequire;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTrainingProficencyRequire *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingProficencyRequire:(time_tns1_TrainingProficencyRequire *)toAdd;
@property (readonly) NSMutableArray * TrainingProficencyRequire;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TrainingCourse : NSObject {
	
/* elements */
	time_tns1_CLogTrainer * CLogTrainer;
	NSNumber * CLogTrainerId;
	time_tns1_CLogTrainingCenter * CLogTrainingCenter;
	NSNumber * CLogTrainingCenterId;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSDate * CreatedDate;
	NSString * Email;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	USBoolean * IsExpired;
	USBoolean * IsLongTrainingCourse;
	USBoolean * IsNecessitated;
	USBoolean * IsPublish;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * Number;
	NSString * PhoneNumber;
	NSString * Result;
	NSDate * StartDate;
	NSNumber * StrategyGoalId;
	time_tns1_TrainingCategory * TrainingCategory;
	NSNumber * TrainingCategoryId;
	time_tns1_ArrayOfTrainingCourseChapter * TrainingCourseChapters;
	NSNumber * TrainingCourseId;
	time_tns1_TrainingCoursePeriod * TrainingCoursePeriod;
	NSNumber * TrainingCoursePeriodId;
	time_tns1_ArrayOfTrainingCourseSchedule * TrainingCourseSchedules;
	time_tns1_TrainingCourseType * TrainingCourseType;
	NSNumber * TrainingCourseTypeId;
	time_tns1_ArrayOfTrainingCourseUnit * TrainingCourseUnits;
	time_tns1_ArrayOfTrainingPlanDegree * TrainingPlanDegrees;
	time_tns1_TrainingPlanRequest * TrainingPlanRequest;
	NSNumber * TrainingPlanRequestId;
	time_tns1_ArrayOfTrainingProficencyExpected * TrainingProficencyExpecteds;
	time_tns1_ArrayOfTrainingProficencyRequire * TrainingProficencyRequires;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TrainingCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_CLogTrainer * CLogTrainer;
@property (retain) NSNumber * CLogTrainerId;
@property (retain) time_tns1_CLogTrainingCenter * CLogTrainingCenter;
@property (retain) NSNumber * CLogTrainingCenterId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Email;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsExpired;
@property (retain) USBoolean * IsLongTrainingCourse;
@property (retain) USBoolean * IsNecessitated;
@property (retain) USBoolean * IsPublish;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * Number;
@property (retain) NSString * PhoneNumber;
@property (retain) NSString * Result;
@property (retain) NSDate * StartDate;
@property (retain) NSNumber * StrategyGoalId;
@property (retain) time_tns1_TrainingCategory * TrainingCategory;
@property (retain) NSNumber * TrainingCategoryId;
@property (retain) time_tns1_ArrayOfTrainingCourseChapter * TrainingCourseChapters;
@property (retain) NSNumber * TrainingCourseId;
@property (retain) time_tns1_TrainingCoursePeriod * TrainingCoursePeriod;
@property (retain) NSNumber * TrainingCoursePeriodId;
@property (retain) time_tns1_ArrayOfTrainingCourseSchedule * TrainingCourseSchedules;
@property (retain) time_tns1_TrainingCourseType * TrainingCourseType;
@property (retain) NSNumber * TrainingCourseTypeId;
@property (retain) time_tns1_ArrayOfTrainingCourseUnit * TrainingCourseUnits;
@property (retain) time_tns1_ArrayOfTrainingPlanDegree * TrainingPlanDegrees;
@property (retain) time_tns1_TrainingPlanRequest * TrainingPlanRequest;
@property (retain) NSNumber * TrainingPlanRequestId;
@property (retain) time_tns1_ArrayOfTrainingProficencyExpected * TrainingProficencyExpecteds;
@property (retain) time_tns1_ArrayOfTrainingProficencyRequire * TrainingProficencyRequires;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTrainingCourseEmployee : NSObject {
	
/* elements */
	NSMutableArray *TrainingCourseEmployee;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTrainingCourseEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingCourseEmployee:(time_tns1_TrainingCourseEmployee *)toAdd;
@property (readonly) NSMutableArray * TrainingCourseEmployee;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TrainingCourseSchedule : NSObject {
	
/* elements */
	time_tns1_CLogCourseSchedule * CLogCourseSchedule;
	NSNumber * ClogCourseScheduleId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	time_tns1_TrainingCourse * TrainingCourse;
	time_tns1_ArrayOfTrainingCourseEmployee * TrainingCourseEmployees;
	NSNumber * TrainingCourseId;
	NSNumber * TrainingCourseScheduleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TrainingCourseSchedule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_CLogCourseSchedule * CLogCourseSchedule;
@property (retain) NSNumber * ClogCourseScheduleId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_TrainingCourse * TrainingCourse;
@property (retain) time_tns1_ArrayOfTrainingCourseEmployee * TrainingCourseEmployees;
@property (retain) NSNumber * TrainingCourseId;
@property (retain) NSNumber * TrainingCourseScheduleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TrainingCourseEmployee : NSObject {
	
/* elements */
	NSNumber * Absence;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSDate * CreatedDate;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsGraduated;
	NSNumber * Mark;
	NSDate * ModifiedDate;
	NSNumber * TrainingCourseEmployeeId;
	time_tns1_TrainingCourseSchedule * TrainingCourseSchedule;
	NSNumber * TrainingCourseScheduleId;
	time_tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TrainingCourseEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Absence;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsGraduated;
@property (retain) NSNumber * Mark;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * TrainingCourseEmployeeId;
@property (retain) time_tns1_TrainingCourseSchedule * TrainingCourseSchedule;
@property (retain) NSNumber * TrainingCourseScheduleId;
@property (retain) time_tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TrainingEmpProficency : NSObject {
	
/* elements */
	NSNumber * CLogEmpRatingId;
	time_tns1_CLogRating * CLogRating;
	time_tns1_CLogRating * CLogRating1;
	NSNumber * ClogEmpManagerId;
	USBoolean * IsDeleted;
	NSNumber * ManagerId;
	time_tns1_OrgSkill * OrgSkill;
	NSNumber * ProficencyId;
	NSNumber * ProficencyType;
	time_tns1_TrainingCourseEmployee * TrainingCourseEmployee;
	NSNumber * TrainingCourseEmployeeId;
	NSNumber * TrainingEmpProficencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TrainingEmpProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogEmpRatingId;
@property (retain) time_tns1_CLogRating * CLogRating;
@property (retain) time_tns1_CLogRating * CLogRating1;
@property (retain) NSNumber * ClogEmpManagerId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ManagerId;
@property (retain) time_tns1_OrgSkill * OrgSkill;
@property (retain) NSNumber * ProficencyId;
@property (retain) NSNumber * ProficencyType;
@property (retain) time_tns1_TrainingCourseEmployee * TrainingCourseEmployee;
@property (retain) NSNumber * TrainingCourseEmployeeId;
@property (retain) NSNumber * TrainingEmpProficencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTrainingEmpProficency : NSObject {
	
/* elements */
	NSMutableArray *TrainingEmpProficency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTrainingEmpProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingEmpProficency:(time_tns1_TrainingEmpProficency *)toAdd;
@property (readonly) NSMutableArray * TrainingEmpProficency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_OrgSkill : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * Description;
	time_tns1_ArrayOfEmpProfileSkill * EmpProfileSkills;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * OrgSkillCode;
	NSNumber * OrgSkillId;
	time_tns1_OrgSkillType * OrgSkillType;
	NSNumber * OrgSkillTypeId;
	NSNumber * Priority;
	NSString * PriorityName;
	time_tns1_ArrayOfRecCandidateSkill * RecCandidateSkills;
	time_tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_OrgSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) time_tns1_ArrayOfEmpProfileSkill * EmpProfileSkills;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * OrgSkillCode;
@property (retain) NSNumber * OrgSkillId;
@property (retain) time_tns1_OrgSkillType * OrgSkillType;
@property (retain) NSNumber * OrgSkillTypeId;
@property (retain) NSNumber * Priority;
@property (retain) NSString * PriorityName;
@property (retain) time_tns1_ArrayOfRecCandidateSkill * RecCandidateSkills;
@property (retain) time_tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecCandidateSkill : NSObject {
	
/* elements */
	time_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * DocumentRef;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	time_tns1_OrgSkill * OrgSkill;
	NSNumber * OrgSkillId;
	time_tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateId;
	NSNumber * RecCandidateSkillId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecCandidateSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DocumentRef;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_OrgSkill * OrgSkill;
@property (retain) NSNumber * OrgSkillId;
@property (retain) time_tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateId;
@property (retain) NSNumber * RecCandidateSkillId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecCandidateSkill : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecCandidateSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateSkill:(time_tns1_RecCandidateSkill *)toAdd;
@property (readonly) NSMutableArray * RecCandidateSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecCandidate : NSObject {
	
/* elements */
	NSMutableArray *RecCandidate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecCandidate *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidate:(time_tns1_RecCandidate *)toAdd;
@property (readonly) NSMutableArray * RecCandidate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecCandidateStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * RecCandidateStatusId;
	time_tns1_ArrayOfRecCandidate * RecCandidates;
	NSString * StatusName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecCandidateStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecCandidateStatusId;
@property (retain) time_tns1_ArrayOfRecCandidate * RecCandidates;
@property (retain) NSString * StatusName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecCandidateSupplier : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateSupplier;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecCandidateSupplier *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateSupplier:(time_tns1_RecCandidateSupplier *)toAdd;
@property (readonly) NSMutableArray * RecCandidateSupplier;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecCandidateTypeSupplier : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * RecCadidateTypeSupplierId;
	time_tns1_ArrayOfRecCandidateSupplier * RecCandidateSuppliers;
	NSString * TypeSupplierName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecCandidateTypeSupplier *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecCadidateTypeSupplierId;
@property (retain) time_tns1_ArrayOfRecCandidateSupplier * RecCandidateSuppliers;
@property (retain) NSString * TypeSupplierName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecCandidateSupplier : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * RecCadidateSupplierId;
	NSNumber * RecCadidateTypeSupplierId;
	time_tns1_RecCandidateTypeSupplier * RecCandidateTypeSupplier;
	time_tns1_ArrayOfRecCandidate * RecCandidates;
	NSString * SupplierAddress;
	NSString * SupplierEmail;
	NSString * SupplierName;
	NSString * SupplierPhone;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecCandidateSupplier *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecCadidateSupplierId;
@property (retain) NSNumber * RecCadidateTypeSupplierId;
@property (retain) time_tns1_RecCandidateTypeSupplier * RecCandidateTypeSupplier;
@property (retain) time_tns1_ArrayOfRecCandidate * RecCandidates;
@property (retain) NSString * SupplierAddress;
@property (retain) NSString * SupplierEmail;
@property (retain) NSString * SupplierName;
@property (retain) NSString * SupplierPhone;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecCandidate : NSObject {
	
/* elements */
	NSString * Address1;
	NSNumber * Address1Id;
	NSString * Address1Phone;
	NSString * Address2;
	NSNumber * Address2Id;
	NSString * Address2Phone;
	NSDate * ApplyDate;
	NSDate * BirthDay;
	NSString * BirthPlace;
	NSNumber * BirthPlaceId;
	NSString * CVFileType;
	NSString * CVUrl;
	USBoolean * CanBusinessTrip;
	USBoolean * CanOT;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * CulturalLevelId;
	NSString * CurrentSalary;
	NSNumber * EducationLevelId;
	NSString * Email;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * Ethnicity;
	NSNumber * EthnicityId;
	NSString * ExpectedSalary;
	NSString * FirstName;
	NSString * Forte;
	NSString * FullName;
	USBoolean * Gender;
	NSString * HomeTown;
	NSNumber * HomeTownId;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	NSString * ImageUrl;
	USBoolean * IsDeleted;
	USBoolean * IsManager;
	USBoolean * IsOnlyYearOfBirthday;
	NSString * LastName;
	NSString * MainLanguage;
	NSNumber * MainLanguageId;
	NSNumber * MajorId;
	NSNumber * MaritalStatusId;
	NSString * Mobile;
	NSDate * ModifiedDate;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSNumber * NumEmpManaged;
	NSString * OfficePhone;
	NSNumber * RecCadidateSupplierId;
	time_tns1_ArrayOfRecCandidateApplication * RecCandidateApplications;
	NSString * RecCandidateCode;
	time_tns1_ArrayOfRecCandidateCompetencyRating * RecCandidateCompetencyRatings;
	time_tns1_ArrayOfRecCandidateDegree * RecCandidateDegrees;
	time_tns1_ArrayOfRecCandidateExperience * RecCandidateExperiences;
	time_tns1_ArrayOfRecCandidateFamilyRelationship * RecCandidateFamilyRelationships;
	time_tns1_ArrayOfRecCandidateForeignLanguage * RecCandidateForeignLanguages;
	NSNumber * RecCandidateId;
	time_tns1_ArrayOfRecCandidateQualification * RecCandidateQualifications;
	time_tns1_ArrayOfRecCandidateSkill * RecCandidateSkills;
	time_tns1_RecCandidateStatu * RecCandidateStatu;
	NSNumber * RecCandidateStatusId;
	time_tns1_RecCandidateSupplier * RecCandidateSupplier;
	time_tns1_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
	NSString * Religion;
	NSNumber * ReligionId;
	NSString * Weaknesses;
	NSNumber * YearsExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecCandidate *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address1;
@property (retain) NSNumber * Address1Id;
@property (retain) NSString * Address1Phone;
@property (retain) NSString * Address2;
@property (retain) NSNumber * Address2Id;
@property (retain) NSString * Address2Phone;
@property (retain) NSDate * ApplyDate;
@property (retain) NSDate * BirthDay;
@property (retain) NSString * BirthPlace;
@property (retain) NSNumber * BirthPlaceId;
@property (retain) NSString * CVFileType;
@property (retain) NSString * CVUrl;
@property (retain) USBoolean * CanBusinessTrip;
@property (retain) USBoolean * CanOT;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * CulturalLevelId;
@property (retain) NSString * CurrentSalary;
@property (retain) NSNumber * EducationLevelId;
@property (retain) NSString * Email;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * Ethnicity;
@property (retain) NSNumber * EthnicityId;
@property (retain) NSString * ExpectedSalary;
@property (retain) NSString * FirstName;
@property (retain) NSString * Forte;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * HomeTown;
@property (retain) NSNumber * HomeTownId;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsManager;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) NSString * LastName;
@property (retain) NSString * MainLanguage;
@property (retain) NSNumber * MainLanguageId;
@property (retain) NSNumber * MajorId;
@property (retain) NSNumber * MaritalStatusId;
@property (retain) NSString * Mobile;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSNumber * NumEmpManaged;
@property (retain) NSString * OfficePhone;
@property (retain) NSNumber * RecCadidateSupplierId;
@property (retain) time_tns1_ArrayOfRecCandidateApplication * RecCandidateApplications;
@property (retain) NSString * RecCandidateCode;
@property (retain) time_tns1_ArrayOfRecCandidateCompetencyRating * RecCandidateCompetencyRatings;
@property (retain) time_tns1_ArrayOfRecCandidateDegree * RecCandidateDegrees;
@property (retain) time_tns1_ArrayOfRecCandidateExperience * RecCandidateExperiences;
@property (retain) time_tns1_ArrayOfRecCandidateFamilyRelationship * RecCandidateFamilyRelationships;
@property (retain) time_tns1_ArrayOfRecCandidateForeignLanguage * RecCandidateForeignLanguages;
@property (retain) NSNumber * RecCandidateId;
@property (retain) time_tns1_ArrayOfRecCandidateQualification * RecCandidateQualifications;
@property (retain) time_tns1_ArrayOfRecCandidateSkill * RecCandidateSkills;
@property (retain) time_tns1_RecCandidateStatu * RecCandidateStatu;
@property (retain) NSNumber * RecCandidateStatusId;
@property (retain) time_tns1_RecCandidateSupplier * RecCandidateSupplier;
@property (retain) time_tns1_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
@property (retain) NSString * Religion;
@property (retain) NSNumber * ReligionId;
@property (retain) NSString * Weaknesses;
@property (retain) NSNumber * YearsExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecCandidateProficencyLevelRating : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateProficencyLevelRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecCandidateProficencyLevelRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateProficencyLevelRating:(time_tns1_RecCandidateProficencyLevelRating *)toAdd;
@property (readonly) NSMutableArray * RecCandidateProficencyLevelRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecCandidateCompetencyRating : NSObject {
	
/* elements */
	time_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * Mark;
	NSDate * ModifiedDate;
	NSString * Note;
	time_tns1_RecCandidate * RecCandidate;
	NSNumber * RecCandidateCompetencyRatingId;
	NSNumber * RecCandidateId;
	time_tns1_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecCandidateCompetencyRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Mark;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) time_tns1_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateCompetencyRatingId;
@property (retain) NSNumber * RecCandidateId;
@property (retain) time_tns1_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecCandidateProficencyLevelRating : NSObject {
	
/* elements */
	time_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * Mark;
	NSDate * ModifiedDate;
	NSString * Note;
	time_tns1_OrgProficencyLevel * OrgProficencyLevel;
	NSNumber * OrgProficencyLevelId;
	time_tns1_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
	time_tns1_RecCandidateCompetencyRating * RecCandidateCompetencyRating;
	NSNumber * RecCandidateCompetencyRatingId;
	NSNumber * RecCandidateProficencyLevelRatingId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecCandidateProficencyLevelRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Mark;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) time_tns1_OrgProficencyLevel * OrgProficencyLevel;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) time_tns1_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
@property (retain) time_tns1_RecCandidateCompetencyRating * RecCandidateCompetencyRating;
@property (retain) NSNumber * RecCandidateCompetencyRatingId;
@property (retain) NSNumber * RecCandidateProficencyLevelRatingId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecCanProficencyLevelDetailRating : NSObject {
	
/* elements */
	time_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * Mark;
	NSDate * ModifiedDate;
	NSString * Note;
	time_tns1_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
	NSNumber * OrgProficencyLevelDetailId;
	time_tns1_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
	NSNumber * RecCanProficencyLevelDetailRatingId;
	time_tns1_RecCandidateProficencyLevelRating * RecCandidateProficencyLevelRating;
	NSNumber * RecCandidateProficencyLevelRatingId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecCanProficencyLevelDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Mark;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) time_tns1_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) time_tns1_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
@property (retain) NSNumber * RecCanProficencyLevelDetailRatingId;
@property (retain) time_tns1_RecCandidateProficencyLevelRating * RecCandidateProficencyLevelRating;
@property (retain) NSNumber * RecCandidateProficencyLevelRatingId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_RecCanProficencyDetailRating : NSObject {
	
/* elements */
	time_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * Mark;
	NSDate * ModifiedDate;
	NSString * Note;
	time_tns1_OrgProficencyDetail * OrgProficencyDetail;
	NSNumber * OrgProficencyDetailId;
	NSNumber * RecCanProficencyDetailRatingId;
	time_tns1_RecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRating;
	NSNumber * RecCanProficencyLevelDetailRatingId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_RecCanProficencyDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Mark;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) time_tns1_OrgProficencyDetail * OrgProficencyDetail;
@property (retain) NSNumber * OrgProficencyDetailId;
@property (retain) NSNumber * RecCanProficencyDetailRatingId;
@property (retain) time_tns1_RecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRating;
@property (retain) NSNumber * RecCanProficencyLevelDetailRatingId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfRecCanProficencyDetailRating : NSObject {
	
/* elements */
	NSMutableArray *RecCanProficencyDetailRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfRecCanProficencyDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCanProficencyDetailRating:(time_tns1_RecCanProficencyDetailRating *)toAdd;
@property (readonly) NSMutableArray * RecCanProficencyDetailRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_OrgProficencyDetail : NSObject {
	
/* elements */
	time_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgCompetencyTypeId;
	NSNumber * OrgProficencyDetailId;
	time_tns1_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
	NSNumber * OrgProficencyLevelDetailId;
	time_tns1_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_OrgProficencyDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgCompetencyTypeId;
@property (retain) NSNumber * OrgProficencyDetailId;
@property (retain) time_tns1_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) time_tns1_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfOrgProficencyDetail : NSObject {
	
/* elements */
	NSMutableArray *OrgProficencyDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfOrgProficencyDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgProficencyDetail:(time_tns1_OrgProficencyDetail *)toAdd;
@property (readonly) NSMutableArray * OrgProficencyDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfOrgProficencyLevelDetail : NSObject {
	
/* elements */
	NSMutableArray *OrgProficencyLevelDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfOrgProficencyLevelDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgProficencyLevelDetail:(time_tns1_OrgProficencyLevelDetail *)toAdd;
@property (readonly) NSMutableArray * OrgProficencyLevelDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TrainingPlanProfiency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	time_tns1_OrgProficencyType * OrgProficencyType;
	NSNumber * OrgProficencyTypeId;
	NSNumber * ProficencyId;
	NSNumber * TrainingPlanProfiencyId;
	NSNumber * TrainingPlanRequestId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TrainingPlanProfiency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_OrgProficencyType * OrgProficencyType;
@property (retain) NSNumber * OrgProficencyTypeId;
@property (retain) NSNumber * ProficencyId;
@property (retain) NSNumber * TrainingPlanProfiencyId;
@property (retain) NSNumber * TrainingPlanRequestId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTrainingPlanProfiency : NSObject {
	
/* elements */
	NSMutableArray *TrainingPlanProfiency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTrainingPlanProfiency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingPlanProfiency:(time_tns1_TrainingPlanProfiency *)toAdd;
@property (readonly) NSMutableArray * TrainingPlanProfiency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_OrgProficencyType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	time_tns1_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
	NSNumber * OrgProficencyTypeId;
	NSString * ProficencyTypeCode;
	NSString * ProficencyTypeName;
	time_tns1_ArrayOfTrainingPlanProfiency * TrainingPlanProfiencies;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_OrgProficencyType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
@property (retain) NSNumber * OrgProficencyTypeId;
@property (retain) NSString * ProficencyTypeCode;
@property (retain) NSString * ProficencyTypeName;
@property (retain) time_tns1_ArrayOfTrainingPlanProfiency * TrainingPlanProfiencies;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_OrgProficencyLevelDetail : NSObject {
	
/* elements */
	time_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	time_tns1_ArrayOfOrgProficencyDetail * OrgProficencyDetails;
	time_tns1_OrgProficencyLevel * OrgProficencyLevel;
	NSNumber * OrgProficencyLevelDetailId;
	NSNumber * OrgProficencyLevelId;
	time_tns1_OrgProficencyType * OrgProficencyType;
	NSNumber * OrgProficencyTypeId;
	time_tns1_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
	NSString * Statement;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_OrgProficencyLevelDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_ArrayOfOrgProficencyDetail * OrgProficencyDetails;
@property (retain) time_tns1_OrgProficencyLevel * OrgProficencyLevel;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) time_tns1_OrgProficencyType * OrgProficencyType;
@property (retain) NSNumber * OrgProficencyTypeId;
@property (retain) time_tns1_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
@property (retain) NSString * Statement;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProficencyLevelDetailRating : NSObject {
	
/* elements */
	NSDate * AppraisalDate;
	time_tns1_CLogRating * CLogRating;
	NSDate * CompanyId;
	NSDate * CreatedDate;
	time_tns1_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
	NSNumber * EmpProficencyLevelDetailRatingId;
	time_tns1_EmpProficencyLevelRating * EmpProficencyLevelRating;
	NSNumber * EmpProficencyLevelRatingId;
	NSString * ManagerNote;
	NSNumber * ManagerRating;
	time_tns1_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
	NSNumber * OrgProficencyLevelDetailId;
	NSNumber * PeriodicallyAssessment;
	NSNumber * Score;
	NSString * SelfNote;
	NSNumber * SelfRatingId;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProficencyLevelDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AppraisalDate;
@property (retain) time_tns1_CLogRating * CLogRating;
@property (retain) NSDate * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
@property (retain) NSNumber * EmpProficencyLevelDetailRatingId;
@property (retain) time_tns1_EmpProficencyLevelRating * EmpProficencyLevelRating;
@property (retain) NSNumber * EmpProficencyLevelRatingId;
@property (retain) NSString * ManagerNote;
@property (retain) NSNumber * ManagerRating;
@property (retain) time_tns1_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) NSNumber * PeriodicallyAssessment;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfNote;
@property (retain) NSNumber * SelfRatingId;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProficencyDetailRating : NSObject {
	
/* elements */
	NSDate * AppraisalDate;
	time_tns1_CLogRating * CLogRating;
	NSDate * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpProficencyDetailRatingId;
	time_tns1_EmpProficencyLevelDetailRating * EmpProficencyLevelDetailRating;
	NSNumber * EmpProficencyLevelDetailRatingId;
	NSString * ManagerNote;
	NSNumber * ManagerRating;
	time_tns1_OrgProficencyDetail * OrgProficencyDetail;
	NSNumber * OrgProficencyDetailId;
	NSNumber * PeriodicallyAssessment;
	NSString * SelfNote;
	NSNumber * SelfRating;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProficencyDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AppraisalDate;
@property (retain) time_tns1_CLogRating * CLogRating;
@property (retain) NSDate * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProficencyDetailRatingId;
@property (retain) time_tns1_EmpProficencyLevelDetailRating * EmpProficencyLevelDetailRating;
@property (retain) NSNumber * EmpProficencyLevelDetailRatingId;
@property (retain) NSString * ManagerNote;
@property (retain) NSNumber * ManagerRating;
@property (retain) time_tns1_OrgProficencyDetail * OrgProficencyDetail;
@property (retain) NSNumber * OrgProficencyDetailId;
@property (retain) NSNumber * PeriodicallyAssessment;
@property (retain) NSString * SelfNote;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProficencyDetailRating : NSObject {
	
/* elements */
	NSMutableArray *EmpProficencyDetailRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProficencyDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProficencyDetailRating:(time_tns1_EmpProficencyDetailRating *)toAdd;
@property (readonly) NSMutableArray * EmpProficencyDetailRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileWorkingExperience : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	time_tns1_CLogRating * CLogRating;
	NSNumber * CompanyId;
	NSString * CompanyName;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileWorkingExperienceId;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	USBoolean * IsInternal;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * NotableAchievements;
	NSString * Note;
	NSNumber * OrgProjectTypeId;
	NSNumber * OrgTimeInChargeId;
	NSString * ProjectName;
	NSNumber * RatingId;
	NSNumber * RelateRowId;
	NSString * Responsibilities;
	NSString * Role;
	NSDate * StartDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileWorkingExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) time_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompanyName;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileWorkingExperienceId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInternal;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * NotableAchievements;
@property (retain) NSString * Note;
@property (retain) NSNumber * OrgProjectTypeId;
@property (retain) NSNumber * OrgTimeInChargeId;
@property (retain) NSString * ProjectName;
@property (retain) NSNumber * RatingId;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * Responsibilities;
@property (retain) NSString * Role;
@property (retain) NSDate * StartDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileWorkingExperience : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileWorkingExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileWorkingExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileWorkingExperience:(time_tns1_EmpProfileWorkingExperience *)toAdd;
@property (readonly) NSMutableArray * EmpProfileWorkingExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfLMSCourse : NSObject {
	
/* elements */
	NSMutableArray *LMSCourse;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfLMSCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourse:(time_tns1_LMSCourse *)toAdd;
@property (readonly) NSMutableArray * LMSCourse;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CLogCourseStatu : NSObject {
	
/* elements */
	NSNumber * CourseStatusId;
	USBoolean * IsDeleted;
	time_tns1_ArrayOfLMSCourse * LMSCourses;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CLogCourseStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CourseStatusId;
@property (retain) USBoolean * IsDeleted;
@property (retain) time_tns1_ArrayOfLMSCourse * LMSCourses;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_GPEmployeeScoreCardAppraisal : NSObject {
	
/* elements */
	NSNumber * AppraisalTime;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
	NSNumber * GPEmployeeScoreCardAppraisalId;
	NSNumber * GPEmployeeScoreCardId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * Score;
	NSString * SelfComment;
	NSNumber * SelfRating;
	NSNumber * SelfScore;
	NSNumber * Status;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_GPEmployeeScoreCardAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AppraisalTime;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
@property (retain) NSNumber * GPEmployeeScoreCardAppraisalId;
@property (retain) NSNumber * GPEmployeeScoreCardId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * SelfScore;
@property (retain) NSNumber * Status;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfGPEmployeeScoreCardAppraisal : NSObject {
	
/* elements */
	NSMutableArray *GPEmployeeScoreCardAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfGPEmployeeScoreCardAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPEmployeeScoreCardAppraisal:(time_tns1_GPEmployeeScoreCardAppraisal *)toAdd;
@property (readonly) NSMutableArray * GPEmployeeScoreCardAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_GPEmployeeScoreCardProgress : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
	NSNumber * GPEmployeeScoreCardId;
	NSNumber * GPEmployeeScoreCardProgressId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * NewValue;
	NSNumber * OldValue;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_GPEmployeeScoreCardProgress *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
@property (retain) NSNumber * GPEmployeeScoreCardId;
@property (retain) NSNumber * GPEmployeeScoreCardProgressId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NewValue;
@property (retain) NSNumber * OldValue;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfGPEmployeeScoreCardProgress : NSObject {
	
/* elements */
	NSMutableArray *GPEmployeeScoreCardProgress;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfGPEmployeeScoreCardProgress *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPEmployeeScoreCardProgress:(time_tns1_GPEmployeeScoreCardProgress *)toAdd;
@property (readonly) NSMutableArray * GPEmployeeScoreCardProgress;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_GPAdditionAppraisal : NSObject {
	
/* elements */
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_Employee * Employee;
	time_tns1_Employee * Employee1;
	NSNumber * EmployeeId;
	NSNumber * GPAdditionAppraisalId;
	time_tns1_GPExecutionPlan * GPExecutionPlan;
	NSNumber * GPExecutionPlanId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * SupervisorId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_GPAdditionAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) time_tns1_Employee * Employee1;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * GPAdditionAppraisalId;
@property (retain) time_tns1_GPExecutionPlan * GPExecutionPlan;
@property (retain) NSNumber * GPExecutionPlanId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * SupervisorId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfGPAdditionAppraisal : NSObject {
	
/* elements */
	NSMutableArray *GPAdditionAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfGPAdditionAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPAdditionAppraisal:(time_tns1_GPAdditionAppraisal *)toAdd;
@property (readonly) NSMutableArray * GPAdditionAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_GPEmployeeWholePlanAppraisal : NSObject {
	
/* elements */
	NSNumber * AppraisalTime;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * GPEmployeeWholePlanAppraisalId;
	time_tns1_GPExecutionPlan * GPExecutionPlan;
	NSNumber * GPExecutionPlanId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * Result;
	NSNumber * Score;
	NSString * SelfComment;
	NSNumber * SelfRating;
	NSNumber * SelfScore;
	NSNumber * Status;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_GPEmployeeWholePlanAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AppraisalTime;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * GPEmployeeWholePlanAppraisalId;
@property (retain) time_tns1_GPExecutionPlan * GPExecutionPlan;
@property (retain) NSNumber * GPExecutionPlanId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * Result;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * SelfScore;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfGPEmployeeWholePlanAppraisal : NSObject {
	
/* elements */
	NSMutableArray *GPEmployeeWholePlanAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfGPEmployeeWholePlanAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPEmployeeWholePlanAppraisal:(time_tns1_GPEmployeeWholePlanAppraisal *)toAdd;
@property (readonly) NSMutableArray * GPEmployeeWholePlanAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_GPPerformanceYearEndResult : NSObject {
	
/* elements */
	NSString * ActivityName;
	NSNumber * Actual;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	time_tns1_GPExecutionPlan * GPExecutionPlan;
	NSNumber * GPPerformancePlanId;
	NSNumber * GPPerformanceYearEndResultId;
	USBoolean * HalfYearResult;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Progress;
	NSNumber * Rate;
	NSString * Result;
	NSNumber * Target;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_GPPerformanceYearEndResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ActivityName;
@property (retain) NSNumber * Actual;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) time_tns1_GPExecutionPlan * GPExecutionPlan;
@property (retain) NSNumber * GPPerformancePlanId;
@property (retain) NSNumber * GPPerformanceYearEndResultId;
@property (retain) USBoolean * HalfYearResult;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Progress;
@property (retain) NSNumber * Rate;
@property (retain) NSString * Result;
@property (retain) NSNumber * Target;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfGPPerformanceYearEndResult : NSObject {
	
/* elements */
	NSMutableArray *GPPerformanceYearEndResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfGPPerformanceYearEndResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPPerformanceYearEndResult:(time_tns1_GPPerformanceYearEndResult *)toAdd;
@property (readonly) NSMutableArray * GPPerformanceYearEndResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_GPExecutionPlan : NSObject {
	
/* elements */
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmployeeId;
	time_tns1_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals;
	time_tns1_ArrayOfGPEmployeeWholePlanAppraisal * GPEmployeeWholePlanAppraisals;
	time_tns1_ArrayOfGPExecutionPlanDetail * GPExecutionPlanDetails;
	NSNumber * GPExecutionPlanId;
	time_tns1_ArrayOfGPPerformanceYearEndResult * GPPerformanceYearEndResults;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * Period;
	NSNumber * Status;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_GPExecutionPlan *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmployeeId;
@property (retain) time_tns1_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals;
@property (retain) time_tns1_ArrayOfGPEmployeeWholePlanAppraisal * GPEmployeeWholePlanAppraisals;
@property (retain) time_tns1_ArrayOfGPExecutionPlanDetail * GPExecutionPlanDetails;
@property (retain) NSNumber * GPExecutionPlanId;
@property (retain) time_tns1_ArrayOfGPPerformanceYearEndResult * GPPerformanceYearEndResults;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * Period;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_GPExecutionPlanActivityAppraisal : NSObject {
	
/* elements */
	NSNumber * AppraisalTime;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_GPExecutionPlanActivity * GPExecutionPlanActivity;
	NSNumber * GPExecutionPlanActivityAppraisalId;
	NSNumber * GPExecutionPlanActivityId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * Score;
	NSString * SelfComment;
	NSNumber * SelfRating;
	NSNumber * SelfScore;
	NSNumber * Status;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_GPExecutionPlanActivityAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AppraisalTime;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_GPExecutionPlanActivity * GPExecutionPlanActivity;
@property (retain) NSNumber * GPExecutionPlanActivityAppraisalId;
@property (retain) NSNumber * GPExecutionPlanActivityId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * SelfScore;
@property (retain) NSNumber * Status;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfGPExecutionPlanActivityAppraisal : NSObject {
	
/* elements */
	NSMutableArray *GPExecutionPlanActivityAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfGPExecutionPlanActivityAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPExecutionPlanActivityAppraisal:(time_tns1_GPExecutionPlanActivityAppraisal *)toAdd;
@property (readonly) NSMutableArray * GPExecutionPlanActivityAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_GPPeriodicReport : NSObject {
	
/* elements */
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_GPExecutionPlanActivity * GPExecutionPlanActivity;
	NSNumber * GPExecutionPlanActivityId;
	NSNumber * GPPeriodicReportId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSDate * ReportDateTime;
	NSNumber * ReportTimes;
	NSNumber * Status;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_GPPeriodicReport *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_GPExecutionPlanActivity * GPExecutionPlanActivity;
@property (retain) NSNumber * GPExecutionPlanActivityId;
@property (retain) NSNumber * GPPeriodicReportId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * ReportDateTime;
@property (retain) NSNumber * ReportTimes;
@property (retain) NSNumber * Status;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfGPPeriodicReport : NSObject {
	
/* elements */
	NSMutableArray *GPPeriodicReport;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfGPPeriodicReport *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPPeriodicReport:(time_tns1_GPPeriodicReport *)toAdd;
@property (readonly) NSMutableArray * GPPeriodicReport;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_GPExecutionPlanActivity : NSObject {
	
/* elements */
	NSString * AchievementLevel;
	NSString * ActivityGroup;
	NSString * ActivityName;
	NSNumber * Budget;
	NSString * BudgetUnit;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * EndDate;
	time_tns1_ArrayOfGPExecutionPlanActivityAppraisal * GPExecutionPlanActivityAppraisals;
	NSNumber * GPExecutionPlanActivityId;
	time_tns1_GPExecutionPlanDetail * GPExecutionPlanDetail;
	NSNumber * GPExecutionPlanDetailId;
	time_tns1_ArrayOfGPPeriodicReport * GPPeriodicReports;
	USBoolean * IsDeleted;
	NSNumber * Measure;
	NSString * MeasureUnit;
	NSDate * ModifiedDate;
	NSDate * StartDate;
	NSString * Status;
	NSNumber * Target;
	NSString * TargetUnit;
	NSNumber * Variance;
	NSString * VarianceUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_GPExecutionPlanActivity *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AchievementLevel;
@property (retain) NSString * ActivityGroup;
@property (retain) NSString * ActivityName;
@property (retain) NSNumber * Budget;
@property (retain) NSString * BudgetUnit;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) time_tns1_ArrayOfGPExecutionPlanActivityAppraisal * GPExecutionPlanActivityAppraisals;
@property (retain) NSNumber * GPExecutionPlanActivityId;
@property (retain) time_tns1_GPExecutionPlanDetail * GPExecutionPlanDetail;
@property (retain) NSNumber * GPExecutionPlanDetailId;
@property (retain) time_tns1_ArrayOfGPPeriodicReport * GPPeriodicReports;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Measure;
@property (retain) NSString * MeasureUnit;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * StartDate;
@property (retain) NSString * Status;
@property (retain) NSNumber * Target;
@property (retain) NSString * TargetUnit;
@property (retain) NSNumber * Variance;
@property (retain) NSString * VarianceUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfGPExecutionPlanActivity : NSObject {
	
/* elements */
	NSMutableArray *GPExecutionPlanActivity;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfGPExecutionPlanActivity *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPExecutionPlanActivity:(time_tns1_GPExecutionPlanActivity *)toAdd;
@property (readonly) NSMutableArray * GPExecutionPlanActivity;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_GPExecutionPlanDetail : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_GPExecutionPlan * GPExecutionPlan;
	time_tns1_ArrayOfGPExecutionPlanActivity * GPExecutionPlanActivities;
	NSNumber * GPExecutionPlanDetailId;
	NSNumber * GPExecutionPlanId;
	time_tns1_GPIndividualYearlyObjective * GPIndividualYearlyObjective;
	NSNumber * GPIndividualYearlyObjectiveId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_GPExecutionPlanDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_GPExecutionPlan * GPExecutionPlan;
@property (retain) time_tns1_ArrayOfGPExecutionPlanActivity * GPExecutionPlanActivities;
@property (retain) NSNumber * GPExecutionPlanDetailId;
@property (retain) NSNumber * GPExecutionPlanId;
@property (retain) time_tns1_GPIndividualYearlyObjective * GPIndividualYearlyObjective;
@property (retain) NSNumber * GPIndividualYearlyObjectiveId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfGPExecutionPlanDetail : NSObject {
	
/* elements */
	NSMutableArray *GPExecutionPlanDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfGPExecutionPlanDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPExecutionPlanDetail:(time_tns1_GPExecutionPlanDetail *)toAdd;
@property (readonly) NSMutableArray * GPExecutionPlanDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_GPIndividualYearlyObjectiveAppraisal : NSObject {
	
/* elements */
	NSNumber * AppraisalTime;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_GPIndividualYearlyObjective * GPIndividualYearlyObjective;
	NSNumber * GPIndividualYearlyObjectiveAppraisalId;
	NSNumber * GPIndividualYearlyObjectiveId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * Score;
	NSString * SelfComment;
	NSNumber * SelfRating;
	NSNumber * SelfScore;
	NSNumber * Status;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_GPIndividualYearlyObjectiveAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AppraisalTime;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_GPIndividualYearlyObjective * GPIndividualYearlyObjective;
@property (retain) NSNumber * GPIndividualYearlyObjectiveAppraisalId;
@property (retain) NSNumber * GPIndividualYearlyObjectiveId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * SelfScore;
@property (retain) NSNumber * Status;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfGPIndividualYearlyObjectiveAppraisal : NSObject {
	
/* elements */
	NSMutableArray *GPIndividualYearlyObjectiveAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfGPIndividualYearlyObjectiveAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPIndividualYearlyObjectiveAppraisal:(time_tns1_GPIndividualYearlyObjectiveAppraisal *)toAdd;
@property (readonly) NSMutableArray * GPIndividualYearlyObjectiveAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_GPPerspectiveValueDetail : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_GPPerspectiveValue * GPPerspectiveValue;
	time_tns1_GPPerspectiveValue * GPPerspectiveValue1;
	NSNumber * GPPerspectiveValueDetailId;
	NSNumber * GPStrategyMapObjectId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * PerspectiveValueDestinationId;
	NSNumber * PerspectiveValueSourceId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_GPPerspectiveValueDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_GPPerspectiveValue * GPPerspectiveValue;
@property (retain) time_tns1_GPPerspectiveValue * GPPerspectiveValue1;
@property (retain) NSNumber * GPPerspectiveValueDetailId;
@property (retain) NSNumber * GPStrategyMapObjectId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * PerspectiveValueDestinationId;
@property (retain) NSNumber * PerspectiveValueSourceId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfGPPerspectiveValueDetail : NSObject {
	
/* elements */
	NSMutableArray *GPPerspectiveValueDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfGPPerspectiveValueDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPPerspectiveValueDetail:(time_tns1_GPPerspectiveValueDetail *)toAdd;
@property (readonly) NSMutableArray * GPPerspectiveValueDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_GPPerspectiveValue : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	time_tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	time_tns1_ArrayOfGPPerspectiveValueDetail * GPPerspectiveValueDetails;
	time_tns1_ArrayOfGPPerspectiveValueDetail * GPPerspectiveValueDetails1;
	NSNumber * GPPerspectiveValueId;
	time_tns1_GPStrategyMapObject * GPStrategyMapObject;
	NSNumber * GPStrategyMapObjectId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * PositionX;
	NSNumber * PositionY;
	NSString * ValueType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_GPPerspectiveValue *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) time_tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) time_tns1_ArrayOfGPPerspectiveValueDetail * GPPerspectiveValueDetails;
@property (retain) time_tns1_ArrayOfGPPerspectiveValueDetail * GPPerspectiveValueDetails1;
@property (retain) NSNumber * GPPerspectiveValueId;
@property (retain) time_tns1_GPStrategyMapObject * GPStrategyMapObject;
@property (retain) NSNumber * GPStrategyMapObjectId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * PositionX;
@property (retain) NSNumber * PositionY;
@property (retain) NSString * ValueType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfGPPerspectiveValue : NSObject {
	
/* elements */
	NSMutableArray *GPPerspectiveValue;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfGPPerspectiveValue *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPPerspectiveValue:(time_tns1_GPPerspectiveValue *)toAdd;
@property (readonly) NSMutableArray * GPPerspectiveValue;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfGPCompanyYearlyObjective : NSObject {
	
/* elements */
	NSMutableArray *GPCompanyYearlyObjective;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfGPCompanyYearlyObjective *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPCompanyYearlyObjective:(time_tns1_GPCompanyYearlyObjective *)toAdd;
@property (readonly) NSMutableArray * GPCompanyYearlyObjective;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfGPStrategyMapObject : NSObject {
	
/* elements */
	NSMutableArray *GPStrategyMapObject;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfGPStrategyMapObject *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPStrategyMapObject:(time_tns1_GPStrategyMapObject *)toAdd;
@property (readonly) NSMutableArray * GPStrategyMapObject;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_GPStrategy : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EndYear;
	time_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
	NSNumber * GPStrategyId;
	time_tns1_ArrayOfGPStrategyMapObject * GPStrategyMapObjects;
	USBoolean * IsActived;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * StartYear;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_GPStrategy *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EndYear;
@property (retain) time_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
@property (retain) NSNumber * GPStrategyId;
@property (retain) time_tns1_ArrayOfGPStrategyMapObject * GPStrategyMapObjects;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * StartYear;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_GPStrategyMapObject : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	time_tns1_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails;
	time_tns1_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
	time_tns1_ArrayOfGPPerspectiveValue * GPPerspectiveValues;
	time_tns1_GPStrategy * GPStrategy;
	NSNumber * GPStrategyId;
	NSNumber * GPStrategyMapObjectId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_GPStrategyMapObject *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) time_tns1_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails;
@property (retain) time_tns1_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
@property (retain) time_tns1_ArrayOfGPPerspectiveValue * GPPerspectiveValues;
@property (retain) time_tns1_GPStrategy * GPStrategy;
@property (retain) NSNumber * GPStrategyId;
@property (retain) NSNumber * GPStrategyMapObjectId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_GPCompanyStrategicGoalDetail : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_GPCompanyStrategicGoal * GPCompanyStrategicGoal;
	time_tns1_GPCompanyStrategicGoal * GPCompanyStrategicGoal1;
	NSNumber * GPCompanyStrategicGoalDestinationId;
	NSNumber * GPCompanyStrategicGoalDetailId;
	NSNumber * GPCompanyStrategicGoalSourceId;
	time_tns1_GPStrategyMapObject * GPStrategyMapObject;
	NSNumber * GPStrategyMapObjectId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_GPCompanyStrategicGoalDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_GPCompanyStrategicGoal * GPCompanyStrategicGoal;
@property (retain) time_tns1_GPCompanyStrategicGoal * GPCompanyStrategicGoal1;
@property (retain) NSNumber * GPCompanyStrategicGoalDestinationId;
@property (retain) NSNumber * GPCompanyStrategicGoalDetailId;
@property (retain) NSNumber * GPCompanyStrategicGoalSourceId;
@property (retain) time_tns1_GPStrategyMapObject * GPStrategyMapObject;
@property (retain) NSNumber * GPStrategyMapObjectId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfGPCompanyStrategicGoalDetail : NSObject {
	
/* elements */
	NSMutableArray *GPCompanyStrategicGoalDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfGPCompanyStrategicGoalDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPCompanyStrategicGoalDetail:(time_tns1_GPCompanyStrategicGoalDetail *)toAdd;
@property (readonly) NSMutableArray * GPCompanyStrategicGoalDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_GPCompanyStrategicGoal : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_Employee * Employee;
	time_tns1_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails;
	time_tns1_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails1;
	NSNumber * GPCompanyStrategicGoalId;
	time_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
	time_tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	NSNumber * GPStrategyId;
	time_tns1_GPStrategyMapObject * GPStrategyMapObject;
	NSNumber * GPStrategyMapObjectId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OwnerId;
	NSString * PeriodStrategy;
	NSString * SmartAttainableGoal;
	NSString * SmartMeasureGoal;
	NSString * SmartRelevantGoal;
	NSString * SmartSpecificGoal;
	NSString * SmartTimeGoal;
	NSString * Statement;
	NSNumber * YearBegin;
	NSNumber * YearEnd;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_GPCompanyStrategicGoal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) time_tns1_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails;
@property (retain) time_tns1_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails1;
@property (retain) NSNumber * GPCompanyStrategicGoalId;
@property (retain) time_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
@property (retain) time_tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) NSNumber * GPStrategyId;
@property (retain) time_tns1_GPStrategyMapObject * GPStrategyMapObject;
@property (retain) NSNumber * GPStrategyMapObjectId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OwnerId;
@property (retain) NSString * PeriodStrategy;
@property (retain) NSString * SmartAttainableGoal;
@property (retain) NSString * SmartMeasureGoal;
@property (retain) NSString * SmartRelevantGoal;
@property (retain) NSString * SmartSpecificGoal;
@property (retain) NSString * SmartTimeGoal;
@property (retain) NSString * Statement;
@property (retain) NSNumber * YearBegin;
@property (retain) NSNumber * YearEnd;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfGPCompanyStrategicGoal : NSObject {
	
/* elements */
	NSMutableArray *GPCompanyStrategicGoal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfGPCompanyStrategicGoal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPCompanyStrategicGoal:(time_tns1_GPCompanyStrategicGoal *)toAdd;
@property (readonly) NSMutableArray * GPCompanyStrategicGoal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_GPObjectiveInitiative : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_Employee * Employee;
	time_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
	NSNumber * GPObjectiveInitiativeId;
	time_tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	USBoolean * IsDeleted;
	time_tns1_ArrayOfLMSCourse * LMSCourses;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * OwnerId;
	NSNumber * Progress;
	NSString * Purpose;
	NSNumber * YearBegin;
	NSNumber * YearEnd;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_GPObjectiveInitiative *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) time_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
@property (retain) NSNumber * GPObjectiveInitiativeId;
@property (retain) time_tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) USBoolean * IsDeleted;
@property (retain) time_tns1_ArrayOfLMSCourse * LMSCourses;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * OwnerId;
@property (retain) NSNumber * Progress;
@property (retain) NSString * Purpose;
@property (retain) NSNumber * YearBegin;
@property (retain) NSNumber * YearEnd;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfGPObjectiveInitiative : NSObject {
	
/* elements */
	NSMutableArray *GPObjectiveInitiative;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfGPObjectiveInitiative *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPObjectiveInitiative:(time_tns1_GPObjectiveInitiative *)toAdd;
@property (readonly) NSMutableArray * GPObjectiveInitiative;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_GPPerspectiveMeasure : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
	time_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
	time_tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	NSNumber * GPPerspectiveMeasureId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_GPPerspectiveMeasure *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
@property (retain) time_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
@property (retain) time_tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) NSNumber * GPPerspectiveMeasureId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfGPPerspectiveMeasure : NSObject {
	
/* elements */
	NSMutableArray *GPPerspectiveMeasure;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfGPPerspectiveMeasure *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPPerspectiveMeasure:(time_tns1_GPPerspectiveMeasure *)toAdd;
@property (readonly) NSMutableArray * GPPerspectiveMeasure;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_GPPerspective : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	time_tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
	time_tns1_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
	time_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
	time_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
	time_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
	time_tns1_ArrayOfGPObjectiveInitiative * GPObjectiveInitiatives;
	NSNumber * GPPerspectiveId;
	time_tns1_ArrayOfGPPerspectiveMeasure * GPPerspectiveMeasures;
	time_tns1_ArrayOfGPPerspectiveValue * GPPerspectiveValues;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_GPPerspective *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) time_tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
@property (retain) time_tns1_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
@property (retain) time_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
@property (retain) time_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
@property (retain) time_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
@property (retain) time_tns1_ArrayOfGPObjectiveInitiative * GPObjectiveInitiatives;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) time_tns1_ArrayOfGPPerspectiveMeasure * GPPerspectiveMeasures;
@property (retain) time_tns1_ArrayOfGPPerspectiveValue * GPPerspectiveValues;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_GPIndividualYearlyObjective : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_Employee * Employee;
	time_tns1_Employee * Employee1;
	NSNumber * EmployeeId;
	time_tns1_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
	NSNumber * GPCompanyYearlyObjectiveId;
	time_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
	NSNumber * GPEmployeeScoreCardId;
	time_tns1_ArrayOfGPExecutionPlanDetail * GPExecutionPlanDetails;
	time_tns1_ArrayOfGPIndividualYearlyObjectiveAppraisal * GPIndividualYearlyObjectiveAppraisals;
	NSNumber * GPIndividualYearlyObjectiveId;
	time_tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * ObjectiveType;
	NSNumber * Owner;
	NSString * SmartAttainableGoal;
	NSString * SmartMeasureGoal;
	NSString * SmartRelevantGoal;
	NSString * SmartSpecificGoal;
	NSString * SmartTimeGoal;
	NSString * Statement;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_GPIndividualYearlyObjective *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) time_tns1_Employee * Employee1;
@property (retain) NSNumber * EmployeeId;
@property (retain) time_tns1_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
@property (retain) NSNumber * GPCompanyYearlyObjectiveId;
@property (retain) time_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
@property (retain) NSNumber * GPEmployeeScoreCardId;
@property (retain) time_tns1_ArrayOfGPExecutionPlanDetail * GPExecutionPlanDetails;
@property (retain) time_tns1_ArrayOfGPIndividualYearlyObjectiveAppraisal * GPIndividualYearlyObjectiveAppraisals;
@property (retain) NSNumber * GPIndividualYearlyObjectiveId;
@property (retain) time_tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * ObjectiveType;
@property (retain) NSNumber * Owner;
@property (retain) NSString * SmartAttainableGoal;
@property (retain) NSString * SmartMeasureGoal;
@property (retain) NSString * SmartRelevantGoal;
@property (retain) NSString * SmartSpecificGoal;
@property (retain) NSString * SmartTimeGoal;
@property (retain) NSString * Statement;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfGPIndividualYearlyObjective : NSObject {
	
/* elements */
	NSMutableArray *GPIndividualYearlyObjective;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfGPIndividualYearlyObjective *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPIndividualYearlyObjective:(time_tns1_GPIndividualYearlyObjective *)toAdd;
@property (readonly) NSMutableArray * GPIndividualYearlyObjective;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_GPPerformanceExecutionReport : NSObject {
	
/* elements */
	NSString * ActivityName;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
	NSNumber * GPEmployeeScoreCardId;
	NSNumber * GPPerformanceExecutionReportId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Progress;
	NSString * ReportNumber;
	NSDate * ReportTime;
	NSNumber * Reporter;
	NSNumber * Status;
	NSNumber * Target;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_GPPerformanceExecutionReport *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ActivityName;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard;
@property (retain) NSNumber * GPEmployeeScoreCardId;
@property (retain) NSNumber * GPPerformanceExecutionReportId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Progress;
@property (retain) NSString * ReportNumber;
@property (retain) NSDate * ReportTime;
@property (retain) NSNumber * Reporter;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * Target;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_GPEmployeeScoreCard : NSObject {
	
/* elements */
	NSNumber * Actual;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	time_tns1_GPCompanyScoreCard * GPCompanyScoreCard;
	NSNumber * GPCompanyScoreCardId;
	time_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCard1;
	time_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard2;
	time_tns1_ArrayOfGPEmployeeScoreCardAppraisal * GPEmployeeScoreCardAppraisals;
	NSNumber * GPEmployeeScoreCardId;
	time_tns1_ArrayOfGPEmployeeScoreCardProgress * GPEmployeeScoreCardProgresses;
	time_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
	time_tns1_GPPerformanceExecutionReport * GPPerformanceExecutionReport;
	time_tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	time_tns1_GPPerspectiveMeasure * GPPerspectiveMeasure;
	NSNumber * GPPerspectiveMeasureId;
	USBoolean * IsDeleted;
	NSString * Metric;
	NSDate * ModifiedDate;
	NSNumber * ParentId;
	NSNumber * PercentageOwnership;
	NSNumber * Progress;
	NSString * ScoreCardGroup;
	NSString * ScoreCardSubGroup;
	NSString * Statement;
	NSNumber * Status;
	NSNumber * Target;
	NSNumber * Weight;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_GPEmployeeScoreCard *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Actual;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) time_tns1_GPCompanyScoreCard * GPCompanyScoreCard;
@property (retain) NSNumber * GPCompanyScoreCardId;
@property (retain) time_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCard1;
@property (retain) time_tns1_GPEmployeeScoreCard * GPEmployeeScoreCard2;
@property (retain) time_tns1_ArrayOfGPEmployeeScoreCardAppraisal * GPEmployeeScoreCardAppraisals;
@property (retain) NSNumber * GPEmployeeScoreCardId;
@property (retain) time_tns1_ArrayOfGPEmployeeScoreCardProgress * GPEmployeeScoreCardProgresses;
@property (retain) time_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
@property (retain) time_tns1_GPPerformanceExecutionReport * GPPerformanceExecutionReport;
@property (retain) time_tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) time_tns1_GPPerspectiveMeasure * GPPerspectiveMeasure;
@property (retain) NSNumber * GPPerspectiveMeasureId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Metric;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * ParentId;
@property (retain) NSNumber * PercentageOwnership;
@property (retain) NSNumber * Progress;
@property (retain) NSString * ScoreCardGroup;
@property (retain) NSString * ScoreCardSubGroup;
@property (retain) NSString * Statement;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * Target;
@property (retain) NSNumber * Weight;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfGPEmployeeScoreCard : NSObject {
	
/* elements */
	NSMutableArray *GPEmployeeScoreCard;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfGPEmployeeScoreCard *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPEmployeeScoreCard:(time_tns1_GPEmployeeScoreCard *)toAdd;
@property (readonly) NSMutableArray * GPEmployeeScoreCard;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_GPCompanyScoreCard : NSObject {
	
/* elements */
	NSNumber * Actual;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_Employee * Employee;
	NSNumber * GPCompanyScoreCardId;
	time_tns1_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
	NSNumber * GPCompanyYearlyObjectiveId;
	time_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
	time_tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	time_tns1_GPPerspectiveMeasure * GPPerspectiveMeasure;
	NSNumber * GPPerspectiveMeasureId;
	USBoolean * IsDeleted;
	NSString * Metric;
	NSDate * ModifiedDate;
	NSNumber * Progress;
	NSString * ReportingRythm;
	NSNumber * ScoreCardCreator;
	NSString * Statement;
	NSNumber * Status;
	NSNumber * Target;
	NSNumber * Weight;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_GPCompanyScoreCard *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Actual;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * GPCompanyScoreCardId;
@property (retain) time_tns1_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
@property (retain) NSNumber * GPCompanyYearlyObjectiveId;
@property (retain) time_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
@property (retain) time_tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) time_tns1_GPPerspectiveMeasure * GPPerspectiveMeasure;
@property (retain) NSNumber * GPPerspectiveMeasureId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Metric;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Progress;
@property (retain) NSString * ReportingRythm;
@property (retain) NSNumber * ScoreCardCreator;
@property (retain) NSString * Statement;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * Target;
@property (retain) NSNumber * Weight;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfGPCompanyScoreCard : NSObject {
	
/* elements */
	NSMutableArray *GPCompanyScoreCard;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfGPCompanyScoreCard *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPCompanyScoreCard:(time_tns1_GPCompanyScoreCard *)toAdd;
@property (readonly) NSMutableArray * GPCompanyScoreCard;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_GPCompanyYearlyObjective : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_Employee * Employee;
	time_tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
	time_tns1_GPCompanyStrategicGoal * GPCompanyStrategicGoal;
	NSNumber * GPCompanyStrategicGoalId;
	NSNumber * GPCompanyYearlyObjectiveId;
	time_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
	time_tns1_GPObjectiveInitiative * GPObjectiveInitiative;
	NSNumber * GPObjectiveInitiativeId;
	time_tns1_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	time_tns1_GPStrategy * GPStrategy;
	NSNumber * GPStrategyId;
	USBoolean * IsDeleted;
	time_tns1_ArrayOfLMSCourse * LMSCourses;
	NSDate * ModifiedDate;
	NSNumber * Owner;
	NSString * SmartAttainableGoal;
	NSString * SmartMeasureGoal;
	NSString * SmartRelevantGoal;
	NSString * SmartSpecificGoal;
	NSString * SmartTimeGoal;
	NSString * Statement;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_GPCompanyYearlyObjective *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) time_tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
@property (retain) time_tns1_GPCompanyStrategicGoal * GPCompanyStrategicGoal;
@property (retain) NSNumber * GPCompanyStrategicGoalId;
@property (retain) NSNumber * GPCompanyYearlyObjectiveId;
@property (retain) time_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
@property (retain) time_tns1_GPObjectiveInitiative * GPObjectiveInitiative;
@property (retain) NSNumber * GPObjectiveInitiativeId;
@property (retain) time_tns1_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) time_tns1_GPStrategy * GPStrategy;
@property (retain) NSNumber * GPStrategyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) time_tns1_ArrayOfLMSCourse * LMSCourses;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Owner;
@property (retain) NSString * SmartAttainableGoal;
@property (retain) NSString * SmartMeasureGoal;
@property (retain) NSString * SmartRelevantGoal;
@property (retain) NSString * SmartSpecificGoal;
@property (retain) NSString * SmartTimeGoal;
@property (retain) NSString * Statement;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_LMSCourseCompetency : NSObject {
	
/* elements */
	time_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompetencyName;
	NSNumber * CompetencyTypeId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	time_tns1_LMSCourse * LMSCourse;
	NSNumber * LMSCourseCompetencyId;
	NSNumber * LMSCourseId;
	NSDate * ModifiedDate;
	time_tns1_OrgCompetency * OrgCompetency;
	NSNumber * OrgCompetencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_LMSCourseCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompetencyName;
@property (retain) NSNumber * CompetencyTypeId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) time_tns1_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseCompetencyId;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_OrgCompetency * OrgCompetency;
@property (retain) NSNumber * OrgCompetencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfLMSCourseCompetency : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfLMSCourseCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseCompetency:(time_tns1_LMSCourseCompetency *)toAdd;
@property (readonly) NSMutableArray * LMSCourseCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_LMSCourseRequiredCompetency : NSObject {
	
/* elements */
	time_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompetencyName;
	NSString * CompetencyType;
	NSNumber * CourseRequiredCompetencyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	time_tns1_LMSCourse * LMSCourse;
	NSNumber * LMSCourseId;
	NSDate * ModifiedDate;
	time_tns1_OrgCompetency * OrgCompetency;
	NSNumber * OrgCompetencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_LMSCourseRequiredCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompetencyName;
@property (retain) NSString * CompetencyType;
@property (retain) NSNumber * CourseRequiredCompetencyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) time_tns1_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_OrgCompetency * OrgCompetency;
@property (retain) NSNumber * OrgCompetencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfLMSCourseRequiredCompetency : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseRequiredCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfLMSCourseRequiredCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseRequiredCompetency:(time_tns1_LMSCourseRequiredCompetency *)toAdd;
@property (readonly) NSMutableArray * LMSCourseRequiredCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfOrgCompetency : NSObject {
	
/* elements */
	NSMutableArray *OrgCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfOrgCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgCompetency:(time_tns1_OrgCompetency *)toAdd;
@property (readonly) NSMutableArray * OrgCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_OrgCompetencyGroup : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * GroupName;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	time_tns1_ArrayOfOrgCompetency * OrgCompetencies;
	NSNumber * OrgCompetencyGroupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_OrgCompetencyGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * GroupName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_ArrayOfOrgCompetency * OrgCompetencies;
@property (retain) NSNumber * OrgCompetencyGroupId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_OrgJobSpecificCompetency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	time_tns1_OrgCompetency * OrgCompetency;
	NSNumber * OrgCompetencyId;
	time_tns1_OrgJob * OrgJob;
	NSNumber * OrgJobId;
	NSNumber * OrgJobSpecificCompetencyId;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_OrgJobSpecificCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_OrgCompetency * OrgCompetency;
@property (retain) NSNumber * OrgCompetencyId;
@property (retain) time_tns1_OrgJob * OrgJob;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSNumber * OrgJobSpecificCompetencyId;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfOrgJobSpecificCompetency : NSObject {
	
/* elements */
	NSMutableArray *OrgJobSpecificCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfOrgJobSpecificCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgJobSpecificCompetency:(time_tns1_OrgJobSpecificCompetency *)toAdd;
@property (readonly) NSMutableArray * OrgJobSpecificCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfOrgProficencyLevel : NSObject {
	
/* elements */
	NSMutableArray *OrgProficencyLevel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfOrgProficencyLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgProficencyLevel:(time_tns1_OrgProficencyLevel *)toAdd;
@property (readonly) NSMutableArray * OrgProficencyLevel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_OrgCompetency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Definition;
	USBoolean * IsCore;
	USBoolean * IsDeleted;
	time_tns1_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
	time_tns1_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
	time_tns1_ArrayOfLMSTopicCompetency * LMSTopicCompetencies;
	NSDate * ModifiedDate;
	NSString * Name;
	time_tns1_OrgCompetencyGroup * OrgCompetencyGroup;
	NSNumber * OrgCompetencyGroupId;
	NSNumber * OrgCompetencyId;
	time_tns1_ArrayOfOrgJobSpecificCompetency * OrgJobSpecificCompetencies;
	time_tns1_ArrayOfOrgProficencyLevel * OrgProficencyLevels;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_OrgCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Definition;
@property (retain) USBoolean * IsCore;
@property (retain) USBoolean * IsDeleted;
@property (retain) time_tns1_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
@property (retain) time_tns1_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
@property (retain) time_tns1_ArrayOfLMSTopicCompetency * LMSTopicCompetencies;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) time_tns1_OrgCompetencyGroup * OrgCompetencyGroup;
@property (retain) NSNumber * OrgCompetencyGroupId;
@property (retain) NSNumber * OrgCompetencyId;
@property (retain) time_tns1_ArrayOfOrgJobSpecificCompetency * OrgJobSpecificCompetencies;
@property (retain) time_tns1_ArrayOfOrgProficencyLevel * OrgProficencyLevels;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_LMSTopicCompetency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	time_tns1_LMSContentTopic * LMSContentTopic;
	NSNumber * LMSContentTopicId;
	NSNumber * LMSTopicCompetencyId;
	NSDate * ModifiedDate;
	time_tns1_OrgCompetency * OrgCompetency;
	NSNumber * OrgCompetencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_LMSTopicCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) time_tns1_LMSContentTopic * LMSContentTopic;
@property (retain) NSNumber * LMSContentTopicId;
@property (retain) NSNumber * LMSTopicCompetencyId;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_OrgCompetency * OrgCompetency;
@property (retain) NSNumber * OrgCompetencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfLMSTopicCompetency : NSObject {
	
/* elements */
	NSMutableArray *LMSTopicCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfLMSTopicCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSTopicCompetency:(time_tns1_LMSTopicCompetency *)toAdd;
@property (readonly) NSMutableArray * LMSTopicCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfLMSContentTopic : NSObject {
	
/* elements */
	NSMutableArray *LMSContentTopic;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfLMSContentTopic *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSContentTopic:(time_tns1_LMSContentTopic *)toAdd;
@property (readonly) NSMutableArray * LMSContentTopic;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_LMSTopicGroup : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	time_tns1_ArrayOfLMSContentTopic * LMSContentTopics;
	NSNumber * LMSTopicGroupId;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_LMSTopicGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) time_tns1_ArrayOfLMSContentTopic * LMSContentTopics;
@property (retain) NSNumber * LMSTopicGroupId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_LMSContentTopic : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * LMSContentTopicId;
	time_tns1_ArrayOfLMSCourse * LMSCourses;
	time_tns1_ArrayOfLMSTopicCompetency * LMSTopicCompetencies;
	time_tns1_LMSTopicGroup * LMSTopicGroup;
	NSNumber * LMSTopicGroupId;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_LMSContentTopic *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * LMSContentTopicId;
@property (retain) time_tns1_ArrayOfLMSCourse * LMSCourses;
@property (retain) time_tns1_ArrayOfLMSTopicCompetency * LMSTopicCompetencies;
@property (retain) time_tns1_LMSTopicGroup * LMSTopicGroup;
@property (retain) NSNumber * LMSTopicGroupId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CLogLMSContentType : NSObject {
	
/* elements */
	NSNumber * CLogLMSContentTypeId;
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	time_tns1_ArrayOfLMSCourseContent * LMSCourseContents;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CLogLMSContentType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogLMSContentTypeId;
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) time_tns1_ArrayOfLMSCourseContent * LMSCourseContents;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_LMSCourseContent : NSObject {
	
/* elements */
	time_tns1_CLogLMSContentType * CLogLMSContentType;
	NSNumber * CLogLMSContentType_CLogLMSContentTypeId;
	time_tns1_LMSCourse * LMSCourse;
	time_tns1_LMSCourseContent * LMSCourseContent1;
	NSNumber * LMSCourseContent1_LMSCourseContentId;
	NSNumber * LMSCourseContentId;
	time_tns1_LMSCourseContent * LMSCourseContents1;
	NSNumber * LMSCourse_LMSCourseId;
	NSString * Name;
	NSString * Statement;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_LMSCourseContent *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_CLogLMSContentType * CLogLMSContentType;
@property (retain) NSNumber * CLogLMSContentType_CLogLMSContentTypeId;
@property (retain) time_tns1_LMSCourse * LMSCourse;
@property (retain) time_tns1_LMSCourseContent * LMSCourseContent1;
@property (retain) NSNumber * LMSCourseContent1_LMSCourseContentId;
@property (retain) NSNumber * LMSCourseContentId;
@property (retain) time_tns1_LMSCourseContent * LMSCourseContents1;
@property (retain) NSNumber * LMSCourse_LMSCourseId;
@property (retain) NSString * Name;
@property (retain) NSString * Statement;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfLMSCourseContent : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseContent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfLMSCourseContent *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseContent:(time_tns1_LMSCourseContent *)toAdd;
@property (readonly) NSMutableArray * LMSCourseContent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_LMSCourseDetail : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CourseContentTypeId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	time_tns1_LMSCourse * LMSCourse;
	NSNumber * LMSCourseDetailId;
	NSNumber * LMSCourseId;
	NSNumber * Level;
	NSDate * ModifiedDate;
	NSString * Resource;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_LMSCourseDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CourseContentTypeId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) time_tns1_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseDetailId;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSNumber * Level;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Resource;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfLMSCourseDetail : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfLMSCourseDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseDetail:(time_tns1_LMSCourseDetail *)toAdd;
@property (readonly) NSMutableArray * LMSCourseDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfOrgUnit : NSObject {
	
/* elements */
	NSMutableArray *OrgUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfOrgUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgUnit:(time_tns1_OrgUnit *)toAdd;
@property (readonly) NSMutableArray * OrgUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_OrgUnit : NSObject {
	
/* elements */
	NSString * Address;
	time_tns1_Address * Address1;
	NSNumber * AddressId;
	NSString * CommissionDesc;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	NSString * FunctionDesc;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	time_tns1_ArrayOfLMSCourseProgressUnit * LMSCourseProgressUnits;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
	time_tns1_ArrayOfOrgUnit * OrgUnit1;
	time_tns1_OrgUnit * OrgUnit2;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSNumber * OrgUnitLevelId;
	NSString * ParentCode;
	NSNumber * ParentId;
	NSNumber * TotalEmployees;
	NSString * WorkLocation;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_OrgUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) time_tns1_Address * Address1;
@property (retain) NSNumber * AddressId;
@property (retain) NSString * CommissionDesc;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) NSString * FunctionDesc;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) time_tns1_ArrayOfLMSCourseProgressUnit * LMSCourseProgressUnits;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) time_tns1_ArrayOfOrgUnit * OrgUnit1;
@property (retain) time_tns1_OrgUnit * OrgUnit2;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * OrgUnitLevelId;
@property (retain) NSString * ParentCode;
@property (retain) NSNumber * ParentId;
@property (retain) NSNumber * TotalEmployees;
@property (retain) NSString * WorkLocation;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_LMSCourseProgressUnit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	time_tns1_LMSCourse * LMSCourse;
	NSNumber * LMSCourseId;
	NSNumber * LMSCourseProgressUnitId;
	NSDate * ModifiedDate;
	NSNumber * NumOfEmpFinish;
	time_tns1_OrgUnit * OrgUnit;
	NSNumber * OrgUnitId;
	USBoolean * Status;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_LMSCourseProgressUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) time_tns1_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSNumber * LMSCourseProgressUnitId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NumOfEmpFinish;
@property (retain) time_tns1_OrgUnit * OrgUnit;
@property (retain) NSNumber * OrgUnitId;
@property (retain) USBoolean * Status;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfLMSCourseProgressUnit : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseProgressUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfLMSCourseProgressUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseProgressUnit:(time_tns1_LMSCourseProgressUnit *)toAdd;
@property (readonly) NSMutableArray * LMSCourseProgressUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_LMSCourseTranscript : NSObject {
	
/* elements */
	time_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSString * Comment;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	time_tns1_Employee * Employee;
	time_tns1_Employee * Employee1;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsPass;
	time_tns1_LMSCourse * LMSCourse;
	NSNumber * LMSCourseId;
	NSNumber * LMSCourseTranscriptId;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_LMSCourseTranscript *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) time_tns1_Employee * Employee1;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsPass;
@property (retain) time_tns1_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSNumber * LMSCourseTranscriptId;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfLMSCourseTranscript : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseTranscript;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfLMSCourseTranscript *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseTranscript:(time_tns1_LMSCourseTranscript *)toAdd;
@property (readonly) NSMutableArray * LMSCourseTranscript;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_LMSCourseType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * LMSCourseTypeId;
	NSString * LMSCourseTypeName;
	time_tns1_ArrayOfLMSCourse * LMSCourses;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_LMSCourseType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * LMSCourseTypeId;
@property (retain) NSString * LMSCourseTypeName;
@property (retain) time_tns1_ArrayOfLMSCourse * LMSCourses;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_LMSCourse : NSObject {
	
/* elements */
	NSNumber * ApprovedBy;
	time_tns1_CLogCourseStatu * CLogCourseStatu;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSString * CourseIcon;
	NSNumber * CourseStatusId;
	NSNumber * CourseTypeId;
	NSDate * CreatedDate;
	NSDate * EndDate;
	time_tns1_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
	time_tns1_GPObjectiveInitiative * GPObjectiveInitiative;
	USBoolean * IsApproved;
	USBoolean * IsDeleted;
	USBoolean * IsRequired;
	NSString * Keyword;
	time_tns1_LMSContentTopic * LMSContentTopic;
	time_tns1_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
	NSString * LMSCourseCode;
	time_tns1_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
	time_tns1_ArrayOfLMSCourseContent * LMSCourseContents;
	time_tns1_ArrayOfLMSCourseDetail * LMSCourseDetails;
	NSNumber * LMSCourseId;
	time_tns1_ArrayOfLMSCourseProgressUnit * LMSCourseProgressUnits;
	time_tns1_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
	time_tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
	time_tns1_LMSCourseType * LMSCourseType;
	NSNumber * LMSInitiativeObjectiveId;
	NSNumber * LMSTopicGroupId;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
	NSNumber * Requestedby;
	NSDate * StartDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_LMSCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApprovedBy;
@property (retain) time_tns1_CLogCourseStatu * CLogCourseStatu;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSString * CourseIcon;
@property (retain) NSNumber * CourseStatusId;
@property (retain) NSNumber * CourseTypeId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) time_tns1_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
@property (retain) time_tns1_GPObjectiveInitiative * GPObjectiveInitiative;
@property (retain) USBoolean * IsApproved;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsRequired;
@property (retain) NSString * Keyword;
@property (retain) time_tns1_LMSContentTopic * LMSContentTopic;
@property (retain) time_tns1_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
@property (retain) NSString * LMSCourseCode;
@property (retain) time_tns1_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
@property (retain) time_tns1_ArrayOfLMSCourseContent * LMSCourseContents;
@property (retain) time_tns1_ArrayOfLMSCourseDetail * LMSCourseDetails;
@property (retain) NSNumber * LMSCourseId;
@property (retain) time_tns1_ArrayOfLMSCourseProgressUnit * LMSCourseProgressUnits;
@property (retain) time_tns1_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
@property (retain) time_tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
@property (retain) time_tns1_LMSCourseType * LMSCourseType;
@property (retain) NSNumber * LMSInitiativeObjectiveId;
@property (retain) NSNumber * LMSTopicGroupId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) NSNumber * Requestedby;
@property (retain) NSDate * StartDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_LMSCourseAttendee : NSObject {
	
/* elements */
	time_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsApproved;
	USBoolean * IsDeleted;
	USBoolean * IsPassed;
	time_tns1_LMSCourse * LMSCourse;
	NSNumber * LMSCourseAttendeeId;
	NSNumber * LMSCourseId;
	NSNumber * LMSDevelopmentPlanId;
	NSDate * ModifiedDate;
	NSNumber * NumberOfWarning;
	NSNumber * OverallScore;
	NSNumber * ParticipationStatus;
	NSNumber * ProgressStatus;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_LMSCourseAttendee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsApproved;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsPassed;
@property (retain) time_tns1_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseAttendeeId;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSNumber * LMSDevelopmentPlanId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NumberOfWarning;
@property (retain) NSNumber * OverallScore;
@property (retain) NSNumber * ParticipationStatus;
@property (retain) NSNumber * ProgressStatus;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfLMSCourseAttendee : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseAttendee;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfLMSCourseAttendee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseAttendee:(time_tns1_LMSCourseAttendee *)toAdd;
@property (readonly) NSMutableArray * LMSCourseAttendee;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfOrgCoreCompetency : NSObject {
	
/* elements */
	NSMutableArray *OrgCoreCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfOrgCoreCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgCoreCompetency:(time_tns1_OrgCoreCompetency *)toAdd;
@property (readonly) NSMutableArray * OrgCoreCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CLogRating : NSObject {
	
/* elements */
	NSNumber * CLogRatingId;
	NSString * Description;
	time_tns1_ArrayOfEmpCompetency * EmpCompetencies;
	time_tns1_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
	time_tns1_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
	time_tns1_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
	time_tns1_ArrayOfEmpProfileSkill * EmpProfileSkills;
	time_tns1_ArrayOfEmpProfileWorkingExperience * EmpProfileWorkingExperiences;
	USBoolean * IsDeleted;
	time_tns1_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
	time_tns1_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
	time_tns1_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
	time_tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
	time_tns1_ArrayOfOrgCoreCompetency * OrgCoreCompetencies;
	time_tns1_ArrayOfOrgProficencyDetail * OrgProficencyDetails;
	time_tns1_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
	time_tns1_ArrayOfOrgProficencyLevel * OrgProficencyLevels;
	time_tns1_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
	time_tns1_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
	time_tns1_ArrayOfRecCandidateCompetencyRating * RecCandidateCompetencyRatings;
	time_tns1_ArrayOfRecCandidateExperience * RecCandidateExperiences;
	time_tns1_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
	time_tns1_ArrayOfRecCandidateQualification * RecCandidateQualifications;
	time_tns1_ArrayOfRecCandidateSkill * RecCandidateSkills;
	time_tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
	time_tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies1;
	time_tns1_ArrayOfTrainingProficencyExpected * TrainingProficencyExpecteds;
	time_tns1_ArrayOfTrainingProficencyRequire * TrainingProficencyRequires;
	NSNumber * Value;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CLogRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSString * Description;
@property (retain) time_tns1_ArrayOfEmpCompetency * EmpCompetencies;
@property (retain) time_tns1_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
@property (retain) time_tns1_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
@property (retain) time_tns1_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
@property (retain) time_tns1_ArrayOfEmpProfileSkill * EmpProfileSkills;
@property (retain) time_tns1_ArrayOfEmpProfileWorkingExperience * EmpProfileWorkingExperiences;
@property (retain) USBoolean * IsDeleted;
@property (retain) time_tns1_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
@property (retain) time_tns1_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
@property (retain) time_tns1_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
@property (retain) time_tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
@property (retain) time_tns1_ArrayOfOrgCoreCompetency * OrgCoreCompetencies;
@property (retain) time_tns1_ArrayOfOrgProficencyDetail * OrgProficencyDetails;
@property (retain) time_tns1_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
@property (retain) time_tns1_ArrayOfOrgProficencyLevel * OrgProficencyLevels;
@property (retain) time_tns1_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
@property (retain) time_tns1_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
@property (retain) time_tns1_ArrayOfRecCandidateCompetencyRating * RecCandidateCompetencyRatings;
@property (retain) time_tns1_ArrayOfRecCandidateExperience * RecCandidateExperiences;
@property (retain) time_tns1_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
@property (retain) time_tns1_ArrayOfRecCandidateQualification * RecCandidateQualifications;
@property (retain) time_tns1_ArrayOfRecCandidateSkill * RecCandidateSkills;
@property (retain) time_tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
@property (retain) time_tns1_ArrayOfTrainingEmpProficency * TrainingEmpProficencies1;
@property (retain) time_tns1_ArrayOfTrainingProficencyExpected * TrainingProficencyExpecteds;
@property (retain) time_tns1_ArrayOfTrainingProficencyRequire * TrainingProficencyRequires;
@property (retain) NSNumber * Value;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_OrgProficencyLevel : NSObject {
	
/* elements */
	time_tns1_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompetencyLevelName;
	NSDate * CreatedDate;
	time_tns1_ArrayOfEmpCompetency * EmpCompetencies;
	time_tns1_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	time_tns1_OrgCompetency * OrgCompetency;
	NSNumber * OrgCompetencyId;
	time_tns1_ArrayOfOrgJobPositionSpecificCompetency * OrgJobPositionSpecificCompetencies;
	time_tns1_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
	NSNumber * OrgProficencyLevelId;
	time_tns1_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
	NSString * Statement;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_OrgProficencyLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompetencyLevelName;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_ArrayOfEmpCompetency * EmpCompetencies;
@property (retain) time_tns1_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_OrgCompetency * OrgCompetency;
@property (retain) NSNumber * OrgCompetencyId;
@property (retain) time_tns1_ArrayOfOrgJobPositionSpecificCompetency * OrgJobPositionSpecificCompetencies;
@property (retain) time_tns1_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) time_tns1_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
@property (retain) NSString * Statement;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_OrgJobPositionSpecificCompetency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	time_tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSNumber * OrgJobPositionSpecificCompetencyId;
	time_tns1_OrgProficencyLevel * OrgProficencyLevel;
	NSNumber * OrgProficencyLevelId;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_OrgJobPositionSpecificCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * OrgJobPositionSpecificCompetencyId;
@property (retain) time_tns1_OrgProficencyLevel * OrgProficencyLevel;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfOrgJobPositionSpecificCompetency : NSObject {
	
/* elements */
	NSMutableArray *OrgJobPositionSpecificCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfOrgJobPositionSpecificCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgJobPositionSpecificCompetency:(time_tns1_OrgJobPositionSpecificCompetency *)toAdd;
@property (readonly) NSMutableArray * OrgJobPositionSpecificCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_OrgJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	time_tns1_ArrayOfEmpContract * EmpContracts;
	time_tns1_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
	time_tns1_OrgJob * OrgJob;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	time_tns1_ArrayOfOrgJobPositionRequiredProficency * OrgJobPositionRequiredProficencies;
	time_tns1_ArrayOfOrgJobPositionSpecificCompetency * OrgJobPositionSpecificCompetencies;
	NSString * OrgJobPositionTitle;
	NSString * OrgJobTitleCode;
	NSNumber * OrgJobTitleId;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgWorkLevelCode;
	NSNumber * OrgWorkLevelId;
	time_tns1_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
	time_tns1_ArrayOfRecPlanJobPosition * RecPlanJobPositions;
	time_tns1_ArrayOfRecProcessApplyForJobPosition * RecProcessApplyForJobPositions;
	time_tns1_ArrayOfRecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPositions;
	time_tns1_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
	time_tns1_ArrayOfRecRequirementJobPosition * RecRequirementJobPositions;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_OrgJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_ArrayOfEmpContract * EmpContracts;
@property (retain) time_tns1_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) time_tns1_OrgJob * OrgJob;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) time_tns1_ArrayOfOrgJobPositionRequiredProficency * OrgJobPositionRequiredProficencies;
@property (retain) time_tns1_ArrayOfOrgJobPositionSpecificCompetency * OrgJobPositionSpecificCompetencies;
@property (retain) NSString * OrgJobPositionTitle;
@property (retain) NSString * OrgJobTitleCode;
@property (retain) NSNumber * OrgJobTitleId;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) time_tns1_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
@property (retain) time_tns1_ArrayOfRecPlanJobPosition * RecPlanJobPositions;
@property (retain) time_tns1_ArrayOfRecProcessApplyForJobPosition * RecProcessApplyForJobPositions;
@property (retain) time_tns1_ArrayOfRecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPositions;
@property (retain) time_tns1_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
@property (retain) time_tns1_ArrayOfRecRequirementJobPosition * RecRequirementJobPositions;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfOrgJobPosition : NSObject {
	
/* elements */
	NSMutableArray *OrgJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfOrgJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgJobPosition:(time_tns1_OrgJobPosition *)toAdd;
@property (readonly) NSMutableArray * OrgJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_OrgUnitJob : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	time_tns1_OrgJob * OrgJob;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSNumber * OrgUnitJobId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_OrgUnitJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_OrgJob * OrgJob;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * OrgUnitJobId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfOrgUnitJob : NSObject {
	
/* elements */
	NSMutableArray *OrgUnitJob;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfOrgUnitJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgUnitJob:(time_tns1_OrgUnitJob *)toAdd;
@property (readonly) NSMutableArray * OrgUnitJob;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_OrgJob : NSObject {
	
/* elements */
	NSString * CLogCareerCode;
	NSNumber * CLogCareerId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_ArrayOfEmpContract * EmpContracts;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	time_tns1_ArrayOfOrgJobPosition * OrgJobPositions;
	time_tns1_ArrayOfOrgJobSpecificCompetency * OrgJobSpecificCompetencies;
	time_tns1_ArrayOfOrgUnitJob * OrgUnitJobs;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_OrgJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogCareerCode;
@property (retain) NSNumber * CLogCareerId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_ArrayOfEmpContract * EmpContracts;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) time_tns1_ArrayOfOrgJobPosition * OrgJobPositions;
@property (retain) time_tns1_ArrayOfOrgJobSpecificCompetency * OrgJobSpecificCompetencies;
@property (retain) time_tns1_ArrayOfOrgUnitJob * OrgUnitJobs;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpContract : NSObject {
	
/* elements */
	time_tns1_Address * Address;
	NSString * AllocationEquipmentDescription;
	NSString * Allowances;
	NSString * AttachFile;
	NSNumber * BaseWage;
	NSString * Bonuses;
	NSString * CLogContractTermCode;
	NSString * CLogContractTypeCode;
	NSNumber * CompanyId;
	NSString * CompensationDescription;
	NSString * ContractCode;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DateSigned;
	NSDate * EmpAppraiseDate;
	NSString * EmpAppraiseNote;
	NSNumber * EmpAppraiseRating;
	NSNumber * EmpContractId;
	time_tns1_Employee * Employee;
	NSString * EmployeeRepresentativeAddress;
	NSNumber * EmployeeRepresentativeAddressId;
	NSDate * EmployeeRepresentativeBirthDay;
	NSString * EmployeeRepresentativeBirthPlace;
	NSString * EmployeeRepresentativeCode;
	NSString * EmployeeRepresentativeFullName;
	NSNumber * EmployeeRepresentativeId;
	NSString * EmployeeRepresentativeMajor;
	NSString * EmployeeRepresentativeNationality;
	time_tns1_Employer * Employer;
	NSNumber * EmployerRepresentativeId;
	NSDate * EndDate;
	NSDate * ExpirationDate;
	NSDate * HRAppraiseDate;
	NSString * HRAppraiseNote;
	NSNumber * HRAppraiseRating;
	NSString * HolIdayPolicy;
	NSNumber * HoursOfOfficialWork;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	USBoolean * IsDeleted;
	USBoolean * IsEnd;
	USBoolean * IsProbation;
	USBoolean * IsSigned;
	NSString * JobResponsibilitiesDescription;
	NSString * LaborSupportEquipments;
	NSDate * LeaderAppraiseDate;
	NSString * LeaderAppraiseNote;
	NSNumber * LeaderAppraiseRating;
	NSString * MedicalInsurance;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSDate * OfficialWorkEndTime;
	NSDate * OfficialWorkStartTime;
	time_tns1_OrgJob * OrgJob;
	NSNumber * OrgJobId;
	time_tns1_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSNumber * OrgUnitId;
	NSString * OrganizationAddress;
	NSNumber * OrganizationAddressId;
	NSString * OrganizationName;
	NSString * OrganizationPhone;
	NSString * OtherPolicy;
	NSString * PayDay;
	NSString * PayingMethod;
	NSString * PlaceSigned;
	NSDate * ProbationaryEndDate;
	NSDate * ProbationaryStartDate;
	NSNumber * ProbationaryTime;
	NSString * ProvidedEquipment;
	NSNumber * RelationEmpContractId;
	NSString * SalaryIncreasePolicy;
	NSString * SocialInsurance;
	NSDate * StartDate;
	NSString * TrainingPolicy;
	NSString * Transportation;
	NSString * WorkingAddress;
	NSNumber * WorkingAddressId;
	NSNumber * WorkingDay;
	NSString * WorkingTime;
	NSString * WorkingTimePolicy;
	NSString * cAllowance;
	NSString * cBaseSalary;
	NSString * cSocialInsuranceSalary;
	NSNumber * nAllowance;
	NSNumber * nSocialInsuranceSalary;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_Address * Address;
@property (retain) NSString * AllocationEquipmentDescription;
@property (retain) NSString * Allowances;
@property (retain) NSString * AttachFile;
@property (retain) NSNumber * BaseWage;
@property (retain) NSString * Bonuses;
@property (retain) NSString * CLogContractTermCode;
@property (retain) NSString * CLogContractTypeCode;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompensationDescription;
@property (retain) NSString * ContractCode;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateSigned;
@property (retain) NSDate * EmpAppraiseDate;
@property (retain) NSString * EmpAppraiseNote;
@property (retain) NSNumber * EmpAppraiseRating;
@property (retain) NSNumber * EmpContractId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeRepresentativeAddress;
@property (retain) NSNumber * EmployeeRepresentativeAddressId;
@property (retain) NSDate * EmployeeRepresentativeBirthDay;
@property (retain) NSString * EmployeeRepresentativeBirthPlace;
@property (retain) NSString * EmployeeRepresentativeCode;
@property (retain) NSString * EmployeeRepresentativeFullName;
@property (retain) NSNumber * EmployeeRepresentativeId;
@property (retain) NSString * EmployeeRepresentativeMajor;
@property (retain) NSString * EmployeeRepresentativeNationality;
@property (retain) time_tns1_Employer * Employer;
@property (retain) NSNumber * EmployerRepresentativeId;
@property (retain) NSDate * EndDate;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSDate * HRAppraiseDate;
@property (retain) NSString * HRAppraiseNote;
@property (retain) NSNumber * HRAppraiseRating;
@property (retain) NSString * HolIdayPolicy;
@property (retain) NSNumber * HoursOfOfficialWork;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEnd;
@property (retain) USBoolean * IsProbation;
@property (retain) USBoolean * IsSigned;
@property (retain) NSString * JobResponsibilitiesDescription;
@property (retain) NSString * LaborSupportEquipments;
@property (retain) NSDate * LeaderAppraiseDate;
@property (retain) NSString * LeaderAppraiseNote;
@property (retain) NSNumber * LeaderAppraiseRating;
@property (retain) NSString * MedicalInsurance;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * OfficialWorkEndTime;
@property (retain) NSDate * OfficialWorkStartTime;
@property (retain) time_tns1_OrgJob * OrgJob;
@property (retain) NSNumber * OrgJobId;
@property (retain) time_tns1_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrganizationAddress;
@property (retain) NSNumber * OrganizationAddressId;
@property (retain) NSString * OrganizationName;
@property (retain) NSString * OrganizationPhone;
@property (retain) NSString * OtherPolicy;
@property (retain) NSString * PayDay;
@property (retain) NSString * PayingMethod;
@property (retain) NSString * PlaceSigned;
@property (retain) NSDate * ProbationaryEndDate;
@property (retain) NSDate * ProbationaryStartDate;
@property (retain) NSNumber * ProbationaryTime;
@property (retain) NSString * ProvidedEquipment;
@property (retain) NSNumber * RelationEmpContractId;
@property (retain) NSString * SalaryIncreasePolicy;
@property (retain) NSString * SocialInsurance;
@property (retain) NSDate * StartDate;
@property (retain) NSString * TrainingPolicy;
@property (retain) NSString * Transportation;
@property (retain) NSString * WorkingAddress;
@property (retain) NSNumber * WorkingAddressId;
@property (retain) NSNumber * WorkingDay;
@property (retain) NSString * WorkingTime;
@property (retain) NSString * WorkingTimePolicy;
@property (retain) NSString * cAllowance;
@property (retain) NSString * cBaseSalary;
@property (retain) NSString * cSocialInsuranceSalary;
@property (retain) NSNumber * nAllowance;
@property (retain) NSNumber * nSocialInsuranceSalary;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpContract : NSObject {
	
/* elements */
	NSMutableArray *EmpContract;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpContract:(time_tns1_EmpContract *)toAdd;
@property (readonly) NSMutableArray * EmpContract;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_Address : NSObject {
	
/* elements */
	NSNumber * AddressId;
	NSString * AddressNumber;
	NSNumber * CLogCityId;
	NSNumber * CLogCountryId;
	NSNumber * CLogDistrictId;
	time_tns1_ArrayOfCLogTrainer * CLogTrainers;
	time_tns1_ArrayOfEmpContract * EmpContracts;
	NSString * FullAddress;
	time_tns1_ArrayOfOrgUnit * OrgUnits;
	NSString * PostalCode;
	NSString * Street;
	NSString * Ward;
	NSString * ZipCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_Address *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AddressId;
@property (retain) NSString * AddressNumber;
@property (retain) NSNumber * CLogCityId;
@property (retain) NSNumber * CLogCountryId;
@property (retain) NSNumber * CLogDistrictId;
@property (retain) time_tns1_ArrayOfCLogTrainer * CLogTrainers;
@property (retain) time_tns1_ArrayOfEmpContract * EmpContracts;
@property (retain) NSString * FullAddress;
@property (retain) time_tns1_ArrayOfOrgUnit * OrgUnits;
@property (retain) NSString * PostalCode;
@property (retain) NSString * Street;
@property (retain) NSString * Ward;
@property (retain) NSString * ZipCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CLogTrainer : NSObject {
	
/* elements */
	NSString * Address;
	time_tns1_Address * Address1;
	NSNumber * AddressId;
	NSNumber * CLogTrainerId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * EducationUnit;
	NSString * Email;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSString * FirstName;
	NSString * FullName;
	USBoolean * IsDeleted;
	USBoolean * IsEmployee;
	NSString * LastName;
	NSDate * ModifiedDate;
	NSString * PhoneNumber;
	time_tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CLogTrainer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) time_tns1_Address * Address1;
@property (retain) NSNumber * AddressId;
@property (retain) NSNumber * CLogTrainerId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * EducationUnit;
@property (retain) NSString * Email;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEmployee;
@property (retain) NSString * LastName;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * PhoneNumber;
@property (retain) time_tns1_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfCLogTrainer : NSObject {
	
/* elements */
	NSMutableArray *CLogTrainer;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfCLogTrainer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogTrainer:(time_tns1_CLogTrainer *)toAdd;
@property (readonly) NSMutableArray * CLogTrainer;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CLogEmpWorkingStatu : NSObject {
	
/* elements */
	NSNumber * CLogEmpWorkingStatusId;
	NSNumber * CompanyId;
	time_tns1_ArrayOfEmpBasicProfile * EmpBasicProfiles;
	NSString * EmpWorkingStatusCode;
	NSString * EmpWorkingStatusName;
	NSString * EmpWorkingStatusNameEN;
	USBoolean * IsDeleted;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CLogEmpWorkingStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogEmpWorkingStatusId;
@property (retain) NSNumber * CompanyId;
@property (retain) time_tns1_ArrayOfEmpBasicProfile * EmpBasicProfiles;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmpWorkingStatusNameEN;
@property (retain) USBoolean * IsDeleted;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpBasicProfile : NSObject {
	
/* elements */
	NSString * BankAccountNo;
	NSString * BankBranch;
	NSString * BankName;
	NSNumber * CLogBankBranchId;
	time_tns1_CLogEmpWorkingStatu * CLogEmpWorkingStatu;
	NSString * CVUrl;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * CulturalLevel;
	NSNumber * CulturalLevelId;
	NSString * EducationLevel;
	NSNumber * EducationLevelId;
	NSNumber * EmpBasicProfileId;
	NSNumber * EmpWorkingStatusId;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardPlaceOfIssue;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	NSString * Major;
	NSNumber * MajorId;
	NSString * MaritalStatus;
	NSNumber * MaritalStatusId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSString * PITCode;
	NSString * PITIssuedBy;
	NSDate * PassportDateOfExpire;
	NSDate * PassportDateOfIssue;
	NSString * PassportNo;
	NSString * PassportPlaceOfIssue;
	NSNumber * ProbationarySalary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpBasicProfile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * BankAccountNo;
@property (retain) NSString * BankBranch;
@property (retain) NSString * BankName;
@property (retain) NSNumber * CLogBankBranchId;
@property (retain) time_tns1_CLogEmpWorkingStatu * CLogEmpWorkingStatu;
@property (retain) NSString * CVUrl;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * CulturalLevel;
@property (retain) NSNumber * CulturalLevelId;
@property (retain) NSString * EducationLevel;
@property (retain) NSNumber * EducationLevelId;
@property (retain) NSNumber * EmpBasicProfileId;
@property (retain) NSNumber * EmpWorkingStatusId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Major;
@property (retain) NSNumber * MajorId;
@property (retain) NSString * MaritalStatus;
@property (retain) NSNumber * MaritalStatusId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSString * PITCode;
@property (retain) NSString * PITIssuedBy;
@property (retain) NSDate * PassportDateOfExpire;
@property (retain) NSDate * PassportDateOfIssue;
@property (retain) NSString * PassportNo;
@property (retain) NSString * PassportPlaceOfIssue;
@property (retain) NSNumber * ProbationarySalary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpBasicProfile : NSObject {
	
/* elements */
	NSMutableArray *EmpBasicProfile;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpBasicProfile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpBasicProfile:(time_tns1_EmpBasicProfile *)toAdd;
@property (readonly) NSMutableArray * EmpBasicProfile;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpCompetencyRating : NSObject {
	
/* elements */
	NSMutableArray *EmpCompetencyRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpCompetencyRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpCompetencyRating:(time_tns1_EmpCompetencyRating *)toAdd;
@property (readonly) NSMutableArray * EmpCompetencyRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpPerformanceAppraisal : NSObject {
	
/* elements */
	NSDate * AppraisalDate;
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * Comment;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpPerformanceAppraisalId;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsEndYear;
	USBoolean * IsHalfYear;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * RelateRowId;
	NSNumber * Score;
	NSString * SelfComment;
	NSNumber * SelfRating;
	NSNumber * SelfScore;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpPerformanceAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AppraisalDate;
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpPerformanceAppraisalId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEndYear;
@property (retain) USBoolean * IsHalfYear;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * SelfScore;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpPerformanceAppraisal : NSObject {
	
/* elements */
	NSMutableArray *EmpPerformanceAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpPerformanceAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpPerformanceAppraisal:(time_tns1_EmpPerformanceAppraisal *)toAdd;
@property (readonly) NSMutableArray * EmpPerformanceAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileAllowance : NSObject {
	
/* elements */
	NSDate * ActivedDate;
	NSString * AllowanceCode;
	NSString * AllowanceName;
	NSNumber * AllowanceValue;
	NSNumber * CLogCurrency;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpProfileAllowanceId;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsActived;
	USBoolean * IsDeactived;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileAllowance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ActivedDate;
@property (retain) NSString * AllowanceCode;
@property (retain) NSString * AllowanceName;
@property (retain) NSNumber * AllowanceValue;
@property (retain) NSNumber * CLogCurrency;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileAllowanceId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeactived;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileAllowance : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileAllowance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileAllowance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileAllowance:(time_tns1_EmpProfileAllowance *)toAdd;
@property (readonly) NSMutableArray * EmpProfileAllowance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileBaseSalary : NSObject {
	
/* elements */
	NSDate * ActivedDate;
	NSString * BasicPayCode;
	NSNumber * CBSalaryCofficientId;
	NSNumber * CBSalaryGradeId;
	NSNumber * CBSalaryLevelId;
	NSNumber * CBSalaryScaleId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpProfileBaseSalaryId;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsActived;
	USBoolean * IsDeactived;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * NonPayScaleSalaryValue;
	NSString * Note;
	NSNumber * PayScaleSalaryValue;
	NSString * SalaryCode;
	NSNumber * SalaryId;
	NSString * SalaryName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileBaseSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ActivedDate;
@property (retain) NSString * BasicPayCode;
@property (retain) NSNumber * CBSalaryCofficientId;
@property (retain) NSNumber * CBSalaryGradeId;
@property (retain) NSNumber * CBSalaryLevelId;
@property (retain) NSNumber * CBSalaryScaleId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileBaseSalaryId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeactived;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NonPayScaleSalaryValue;
@property (retain) NSString * Note;
@property (retain) NSNumber * PayScaleSalaryValue;
@property (retain) NSString * SalaryCode;
@property (retain) NSNumber * SalaryId;
@property (retain) NSString * SalaryName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileBaseSalary : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileBaseSalary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileBaseSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileBaseSalary:(time_tns1_EmpProfileBaseSalary *)toAdd;
@property (readonly) NSMutableArray * EmpProfileBaseSalary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileBenefit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileBenefitId;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * HealthInsuranceDateExpire;
	NSDate * HealthInsuranceDateIssue;
	NSNumber * HealthInsuranceId;
	NSString * HealthInsuranceNumber;
	NSString * HealthInsuranceRegPlace;
	USBoolean * IsDeleted;
	NSDate * LabourBookDateOfIssue;
	NSString * LabourBookNo;
	NSString * LabourBookPlaceOfIssue;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * OtherInsuranceCompanyName;
	NSDate * OtherInsuranceDateExpire;
	NSDate * OtherInsuranceDateIssue;
	NSString * OtherInsuranceNumber;
	NSDate * SocialInsuranceDateIssue;
	NSNumber * SocialInsuranceId;
	NSNumber * SocialInsuranceJoiningBefore;
	NSString * SocialInsuranceNumber;
	NSDate * UnemploymentInsuranceDateIssue;
	NSNumber * UnemploymentInsuranceId;
	NSString * UnemploymentInsuranceNumber;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileBenefitId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * HealthInsuranceDateExpire;
@property (retain) NSDate * HealthInsuranceDateIssue;
@property (retain) NSNumber * HealthInsuranceId;
@property (retain) NSString * HealthInsuranceNumber;
@property (retain) NSString * HealthInsuranceRegPlace;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * LabourBookDateOfIssue;
@property (retain) NSString * LabourBookNo;
@property (retain) NSString * LabourBookPlaceOfIssue;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OtherInsuranceCompanyName;
@property (retain) NSDate * OtherInsuranceDateExpire;
@property (retain) NSDate * OtherInsuranceDateIssue;
@property (retain) NSString * OtherInsuranceNumber;
@property (retain) NSDate * SocialInsuranceDateIssue;
@property (retain) NSNumber * SocialInsuranceId;
@property (retain) NSNumber * SocialInsuranceJoiningBefore;
@property (retain) NSString * SocialInsuranceNumber;
@property (retain) NSDate * UnemploymentInsuranceDateIssue;
@property (retain) NSNumber * UnemploymentInsuranceId;
@property (retain) NSString * UnemploymentInsuranceNumber;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileBenefit : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileBenefit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileBenefit:(time_tns1_EmpProfileBenefit *)toAdd;
@property (readonly) NSMutableArray * EmpProfileBenefit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileComment : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSString * Content;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfileCommentId;
	time_tns1_Employee * Employee;
	time_tns1_Employee * Employee1;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * RelateRowId;
	NSString * ReviewerCode;
	NSNumber * ReviewerId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileComment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Content;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfileCommentId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) time_tns1_Employee * Employee1;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * ReviewerCode;
@property (retain) NSNumber * ReviewerId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileComment : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileComment;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileComment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileComment:(time_tns1_EmpProfileComment *)toAdd;
@property (readonly) NSMutableArray * EmpProfileComment;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileComputingSkill : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSString * ComputingSkillName;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	NSNumber * EmpProfileComputingSkillId;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsValid;
	NSDate * ModifiedDate;
	NSString * Other;
	NSString * PlaceIssue;
	NSNumber * RatingId;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileComputingSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ComputingSkillName;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) NSNumber * EmpProfileComputingSkillId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValid;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) NSNumber * RatingId;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileComputingSkill : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileComputingSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileComputingSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileComputingSkill:(time_tns1_EmpProfileComputingSkill *)toAdd;
@property (readonly) NSMutableArray * EmpProfileComputingSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileContact : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSString * ContactAddress;
	NSNumber * ContactAddressId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * Email;
	NSNumber * EmpProfileContactId;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSString * Fax;
	NSString * HomePhone;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	NSString * MobileNumber;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * OfficePhone;
	NSString * PermanentAddress;
	NSNumber * PermanentAddressId;
	NSNumber * RelateRowId;
	NSString * TemporaryAddress;
	NSNumber * TemporaryAddressId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileContact *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ContactAddress;
@property (retain) NSNumber * ContactAddressId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Email;
@property (retain) NSNumber * EmpProfileContactId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * Fax;
@property (retain) NSString * HomePhone;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * MobileNumber;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OfficePhone;
@property (retain) NSString * PermanentAddress;
@property (retain) NSNumber * PermanentAddressId;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * TemporaryAddress;
@property (retain) NSNumber * TemporaryAddressId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileContact : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileContact;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileContact *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileContact:(time_tns1_EmpProfileContact *)toAdd;
@property (readonly) NSMutableArray * EmpProfileContact;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_OrgDegree : NSObject {
	
/* elements */
	NSNumber * CLogMajorId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * Description;
	time_tns1_ArrayOfEmpProfileDegree * EmpProfileDegrees;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * OrgDegreeId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_OrgDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) time_tns1_ArrayOfEmpProfileDegree * EmpProfileDegrees;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * OrgDegreeId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileDegree : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	NSNumber * EmpProfileDegreeId;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsValId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	time_tns1_OrgDegree * OrgDegree;
	NSNumber * OrgDegreeId;
	NSNumber * OrgDegreeRankId;
	NSString * Other;
	NSString * PlaceIssue;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) NSNumber * EmpProfileDegreeId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_OrgDegree * OrgDegree;
@property (retain) NSNumber * OrgDegreeId;
@property (retain) NSNumber * OrgDegreeRankId;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileDegree : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileDegree;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileDegree:(time_tns1_EmpProfileDegree *)toAdd;
@property (readonly) NSMutableArray * EmpProfileDegree;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_OrgDisciplineForUnit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgDisciplineForUnitId;
	NSNumber * OrgDisciplineId;
	time_tns1_OrgDisciplineMaster * OrgDisciplineMaster;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_OrgDisciplineForUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgDisciplineForUnitId;
@property (retain) NSNumber * OrgDisciplineId;
@property (retain) time_tns1_OrgDisciplineMaster * OrgDisciplineMaster;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfOrgDisciplineForUnit : NSObject {
	
/* elements */
	NSMutableArray *OrgDisciplineForUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfOrgDisciplineForUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgDisciplineForUnit:(time_tns1_OrgDisciplineForUnit *)toAdd;
@property (readonly) NSMutableArray * OrgDisciplineForUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_OrgDisciplineMaster : NSObject {
	
/* elements */
	NSNumber * ApproveId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSDate * DateOfIssue;
	NSString * DecisionNo;
	time_tns1_ArrayOfEmpProfileDiscipline * EmpProfileDisciplines;
	USBoolean * IsDeleted;
	USBoolean * IsForEmployee;
	NSDate * ModifiedDate;
	NSNumber * MonetaryValue;
	time_tns1_ArrayOfOrgDisciplineForUnit * OrgDisciplineForUnits;
	NSNumber * OrgDisciplineMasterId;
	NSString * OrgDisciplineTypeCode;
	USBoolean * Status;
	NSString * Title;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_OrgDisciplineMaster *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproveId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSDate * DateOfIssue;
@property (retain) NSString * DecisionNo;
@property (retain) time_tns1_ArrayOfEmpProfileDiscipline * EmpProfileDisciplines;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsForEmployee;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * MonetaryValue;
@property (retain) time_tns1_ArrayOfOrgDisciplineForUnit * OrgDisciplineForUnits;
@property (retain) NSNumber * OrgDisciplineMasterId;
@property (retain) NSString * OrgDisciplineTypeCode;
@property (retain) USBoolean * Status;
@property (retain) NSString * Title;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileDiscipline : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfileDisciplineId;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	time_tns1_OrgDisciplineMaster * OrgDisciplineMaster;
	NSNumber * OrgDisciplineMasterId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileDiscipline *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfileDisciplineId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_OrgDisciplineMaster * OrgDisciplineMaster;
@property (retain) NSNumber * OrgDisciplineMasterId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileDiscipline : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileDiscipline;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileDiscipline *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileDiscipline:(time_tns1_EmpProfileDiscipline *)toAdd;
@property (readonly) NSMutableArray * EmpProfileDiscipline;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CLogEquipment : NSObject {
	
/* elements */
	NSDate * AddDate;
	NSString * CLogAssetsTypeCode;
	NSNumber * CLogCurrencyId;
	NSNumber * CLogEquipmentId;
	NSString * CLogEquipmentTypeCode;
	NSString * Code;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * Depreciation;
	time_tns1_ArrayOfEmpProfileEquipment * EmpProfileEquipments;
	USBoolean * IsBigValue;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
	NSNumber * Number;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CLogEquipment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AddDate;
@property (retain) NSString * CLogAssetsTypeCode;
@property (retain) NSNumber * CLogCurrencyId;
@property (retain) NSNumber * CLogEquipmentId;
@property (retain) NSString * CLogEquipmentTypeCode;
@property (retain) NSString * Code;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * Depreciation;
@property (retain) time_tns1_ArrayOfEmpProfileEquipment * EmpProfileEquipments;
@property (retain) USBoolean * IsBigValue;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) NSNumber * Number;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileEquipment : NSObject {
	
/* elements */
	time_tns1_CLogEquipment * CLogEquipment;
	NSNumber * CLogEquipmentId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DateHandover;
	NSDate * DateReceived;
	NSString * Description;
	NSNumber * EmpProfileEquipmentId;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * HandoverStatus;
	USBoolean * IsDeleted;
	USBoolean * IsHandover;
	USBoolean * IsReturnToComp;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	USBoolean * MustHandover;
	NSNumber * Number;
	NSString * ReceiverCode;
	NSNumber * ReceiverId;
	NSNumber * RequestDetailId;
	NSString * StateHandover;
	NSString * StateReceived;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileEquipment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_CLogEquipment * CLogEquipment;
@property (retain) NSNumber * CLogEquipmentId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateHandover;
@property (retain) NSDate * DateReceived;
@property (retain) NSString * Description;
@property (retain) NSNumber * EmpProfileEquipmentId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * HandoverStatus;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsHandover;
@property (retain) USBoolean * IsReturnToComp;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) USBoolean * MustHandover;
@property (retain) NSNumber * Number;
@property (retain) NSString * ReceiverCode;
@property (retain) NSNumber * ReceiverId;
@property (retain) NSNumber * RequestDetailId;
@property (retain) NSString * StateHandover;
@property (retain) NSString * StateReceived;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileEquipment : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileEquipment;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileEquipment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileEquipment:(time_tns1_EmpProfileEquipment *)toAdd;
@property (readonly) NSMutableArray * EmpProfileEquipment;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileExperience : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * AttachedDocumentUrl;
	NSString * Company;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * Customer;
	NSNumber * EmpProfileExperienceId;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * ProjectName;
	NSString * ProjectResult;
	NSNumber * RelateRowId;
	NSString * Roles;
	NSDate * StartDate;
	NSString * TeamSize;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * AttachedDocumentUrl;
@property (retain) NSString * Company;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Customer;
@property (retain) NSNumber * EmpProfileExperienceId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * ProjectName;
@property (retain) NSString * ProjectResult;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * Roles;
@property (retain) NSDate * StartDate;
@property (retain) NSString * TeamSize;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileExperience : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileExperience:(time_tns1_EmpProfileExperience *)toAdd;
@property (readonly) NSMutableArray * EmpProfileExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CLogLanguage : NSObject {
	
/* elements */
	NSNumber * CLogLanguageId;
	time_tns1_ArrayOfEmpProfileForeignLanguage * EmpProfileForeignLanguages;
	USBoolean * IsDeleted;
	NSString * Language;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CLogLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogLanguageId;
@property (retain) time_tns1_ArrayOfEmpProfileForeignLanguage * EmpProfileForeignLanguages;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Language;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileForeignLanguage : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * AttachmenttUrl;
	time_tns1_CLogLanguage * CLogLanguage;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DateOfIssue;
	NSString * DegreeName;
	NSNumber * EffectiveTime;
	NSNumber * EmpProfileForeignLanguageId;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * LanguageId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * OrgDegreeRankId;
	NSString * PlaceOfIssue;
	NSNumber * RelateRowId;
	NSString * TrainingCenter;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileForeignLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * AttachmenttUrl;
@property (retain) time_tns1_CLogLanguage * CLogLanguage;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateOfIssue;
@property (retain) NSString * DegreeName;
@property (retain) NSNumber * EffectiveTime;
@property (retain) NSNumber * EmpProfileForeignLanguageId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * LanguageId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * OrgDegreeRankId;
@property (retain) NSString * PlaceOfIssue;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * TrainingCenter;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileForeignLanguage : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileForeignLanguage;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileForeignLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileForeignLanguage:(time_tns1_EmpProfileForeignLanguage *)toAdd;
@property (readonly) NSMutableArray * EmpProfileForeignLanguage;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileHealthInsurance : NSObject {
	
/* elements */
	NSNumber * CLogHospitalId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * HICompanyRate;
	NSNumber * HIEmployeeRate;
	NSDate * HealthInsuranceDateEffect;
	NSDate * HealthInsuranceDateExpire;
	NSNumber * HealthInsuranceId;
	NSString * HealthInsuranceNumber;
	NSDate * ImplementDate;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	USBoolean * IsDifference;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileHealthInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogHospitalId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * HICompanyRate;
@property (retain) NSNumber * HIEmployeeRate;
@property (retain) NSDate * HealthInsuranceDateEffect;
@property (retain) NSDate * HealthInsuranceDateExpire;
@property (retain) NSNumber * HealthInsuranceId;
@property (retain) NSString * HealthInsuranceNumber;
@property (retain) NSDate * ImplementDate;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDifference;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileHealthInsurance : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileHealthInsurance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileHealthInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileHealthInsurance:(time_tns1_EmpProfileHealthInsurance *)toAdd;
@property (readonly) NSMutableArray * EmpProfileHealthInsurance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileHealthy : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * BloodGroup;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DiseaseAbsenceEndDay;
	NSDate * DiseaseAbsenceStartDay;
	NSNumber * DiseaseBenefit;
	NSString * DiseaseName;
	NSNumber * EmpProfileHealthyId;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	USBoolean * IsDiseaseAbsence;
	USBoolean * IsEnoughHealthToWork;
	USBoolean * IsGoodHealth;
	USBoolean * IsMaternityAbsence;
	USBoolean * IsMedicalHistory;
	USBoolean * IsPeopleWithDisability;
	NSDate * MaternityAbsenceEndDay;
	NSDate * MaternityAbsenceStartDay;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileHealthy *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * BloodGroup;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DiseaseAbsenceEndDay;
@property (retain) NSDate * DiseaseAbsenceStartDay;
@property (retain) NSNumber * DiseaseBenefit;
@property (retain) NSString * DiseaseName;
@property (retain) NSNumber * EmpProfileHealthyId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDiseaseAbsence;
@property (retain) USBoolean * IsEnoughHealthToWork;
@property (retain) USBoolean * IsGoodHealth;
@property (retain) USBoolean * IsMaternityAbsence;
@property (retain) USBoolean * IsMedicalHistory;
@property (retain) USBoolean * IsPeopleWithDisability;
@property (retain) NSDate * MaternityAbsenceEndDay;
@property (retain) NSDate * MaternityAbsenceStartDay;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileHealthy : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileHealthy;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileHealthy *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileHealthy:(time_tns1_EmpProfileHealthy *)toAdd;
@property (readonly) NSMutableArray * EmpProfileHealthy;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CLogHobby : NSObject {
	
/* elements */
	NSNumber * CLogHobbyId;
	NSString * Description;
	time_tns1_ArrayOfEmpProfileHobby * EmpProfileHobbies;
	USBoolean * IsDeleted;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CLogHobby *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogHobbyId;
@property (retain) NSString * Description;
@property (retain) time_tns1_ArrayOfEmpProfileHobby * EmpProfileHobbies;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileHobby : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	time_tns1_CLogHobby * CLogHobby;
	NSNumber * CLogHobbyId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfileHobbyId;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileHobby *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) time_tns1_CLogHobby * CLogHobby;
@property (retain) NSNumber * CLogHobbyId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfileHobbyId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileHobby : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileHobby;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileHobby *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileHobby:(time_tns1_EmpProfileHobby *)toAdd;
@property (readonly) NSMutableArray * EmpProfileHobby;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileInternalCourse : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpInternalCourseId;
	NSNumber * EmpProfileInternalCourseId;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Result;
	NSNumber * Status;
	NSString * Support;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileInternalCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpInternalCourseId;
@property (retain) NSNumber * EmpProfileInternalCourseId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Result;
@property (retain) NSNumber * Status;
@property (retain) NSString * Support;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileInternalCourse : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileInternalCourse;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileInternalCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileInternalCourse:(time_tns1_EmpProfileInternalCourse *)toAdd;
@property (readonly) NSMutableArray * EmpProfileInternalCourse;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileLeaveRegime : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileLeaveRegime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileLeaveRegime *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileLeaveRegime:(time_tns1_EmpProfileLeaveRegime *)toAdd;
@property (readonly) NSMutableArray * EmpProfileLeaveRegime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpOtherBenefit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	NSNumber * EmpOtherBenefitId;
	time_tns1_ArrayOfEmpProfileOtherBenefit * EmpProfileOtherBenefits;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpOtherBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) NSNumber * EmpOtherBenefitId;
@property (retain) time_tns1_ArrayOfEmpProfileOtherBenefit * EmpProfileOtherBenefits;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileOtherBenefit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	time_tns1_EmpOtherBenefit * EmpOtherBenefit;
	NSNumber * EmpOtherBenefitId;
	NSNumber * EmpProfileOtherBenefitId;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileOtherBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_EmpOtherBenefit * EmpOtherBenefit;
@property (retain) NSNumber * EmpOtherBenefitId;
@property (retain) NSNumber * EmpProfileOtherBenefitId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileOtherBenefit : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileOtherBenefit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileOtherBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileOtherBenefit:(time_tns1_EmpProfileOtherBenefit *)toAdd;
@property (readonly) NSMutableArray * EmpProfileOtherBenefit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileParticipation : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileParticipationId;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSDate * PartyFeeEndDate;
	NSNumber * PartyFeePercent;
	NSString * PartyFeeRole;
	NSDate * PartyFeeStartDate;
	NSNumber * RelateRowId;
	NSDate * UnionFeeEndDate;
	NSNumber * UnionFeePercent;
	NSString * UnionFeeRole;
	NSDate * UnionFeeStartDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileParticipation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileParticipationId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * PartyFeeEndDate;
@property (retain) NSNumber * PartyFeePercent;
@property (retain) NSString * PartyFeeRole;
@property (retain) NSDate * PartyFeeStartDate;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSDate * UnionFeeEndDate;
@property (retain) NSNumber * UnionFeePercent;
@property (retain) NSString * UnionFeeRole;
@property (retain) NSDate * UnionFeeStartDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileParticipation : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileParticipation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileParticipation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileParticipation:(time_tns1_EmpProfileParticipation *)toAdd;
@property (readonly) NSMutableArray * EmpProfileParticipation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_CLogPersonality : NSObject {
	
/* elements */
	NSNumber * CLogPersonalityId;
	NSString * Description;
	time_tns1_ArrayOfEmpProfilePersonality * EmpProfilePersonalities;
	USBoolean * IsDeleted;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_CLogPersonality *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogPersonalityId;
@property (retain) NSString * Description;
@property (retain) time_tns1_ArrayOfEmpProfilePersonality * EmpProfilePersonalities;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfilePersonality : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	time_tns1_CLogPersonality * CLogPersonality;
	NSNumber * CLogPersonalityId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfilePersonalityId;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfilePersonality *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) time_tns1_CLogPersonality * CLogPersonality;
@property (retain) NSNumber * CLogPersonalityId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfilePersonalityId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfilePersonality : NSObject {
	
/* elements */
	NSMutableArray *EmpProfilePersonality;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfilePersonality *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfilePersonality:(time_tns1_EmpProfilePersonality *)toAdd;
@property (readonly) NSMutableArray * EmpProfilePersonality;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileProcessOfWork : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * DecisionFile;
	NSString * DecisionName;
	NSString * DecisionNo;
	NSNumber * EmpProfileJobPositionId;
	NSNumber * EmpProfileProcessOfWorkId;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ExpirationDate;
	NSDate * ImplementationDate;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * OrgUnitAddress;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgUnitName;
	NSString * OrgUnitNameEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileProcessOfWork *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DecisionFile;
@property (retain) NSString * DecisionName;
@property (retain) NSString * DecisionNo;
@property (retain) NSNumber * EmpProfileJobPositionId;
@property (retain) NSNumber * EmpProfileProcessOfWorkId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OrgUnitAddress;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitName;
@property (retain) NSString * OrgUnitNameEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileProcessOfWork : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileProcessOfWork;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileProcessOfWork *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileProcessOfWork:(time_tns1_EmpProfileProcessOfWork *)toAdd;
@property (readonly) NSMutableArray * EmpProfileProcessOfWork;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_OrgRewardForUnit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgRewardForUnitId;
	NSNumber * OrgRewardId;
	time_tns1_OrgRewardMaster * OrgRewardMaster;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_OrgRewardForUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgRewardForUnitId;
@property (retain) NSNumber * OrgRewardId;
@property (retain) time_tns1_OrgRewardMaster * OrgRewardMaster;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfOrgRewardForUnit : NSObject {
	
/* elements */
	NSMutableArray *OrgRewardForUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfOrgRewardForUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgRewardForUnit:(time_tns1_OrgRewardForUnit *)toAdd;
@property (readonly) NSMutableArray * OrgRewardForUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_OrgRewardMaster : NSObject {
	
/* elements */
	NSNumber * ApproveId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSDate * DateOfIssue;
	NSString * DecisionNo;
	time_tns1_ArrayOfEmpProfileReward * EmpProfileRewards;
	USBoolean * IsAllCompany;
	USBoolean * IsDeleted;
	USBoolean * IsForEmployee;
	NSDate * ModifiedDate;
	NSNumber * MonetaryValue;
	time_tns1_ArrayOfOrgRewardForUnit * OrgRewardForUnits;
	NSNumber * OrgRewardMasterId;
	NSString * OrgRewardTypeCode;
	USBoolean * Status;
	NSString * Title;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_OrgRewardMaster *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproveId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSDate * DateOfIssue;
@property (retain) NSString * DecisionNo;
@property (retain) time_tns1_ArrayOfEmpProfileReward * EmpProfileRewards;
@property (retain) USBoolean * IsAllCompany;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsForEmployee;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * MonetaryValue;
@property (retain) time_tns1_ArrayOfOrgRewardForUnit * OrgRewardForUnits;
@property (retain) NSNumber * OrgRewardMasterId;
@property (retain) NSString * OrgRewardTypeCode;
@property (retain) USBoolean * Status;
@property (retain) NSString * Title;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileReward : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfileRewardId;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	time_tns1_OrgRewardMaster * OrgRewardMaster;
	NSNumber * OrgRewardMasterId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileReward *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfileRewardId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_OrgRewardMaster * OrgRewardMaster;
@property (retain) NSNumber * OrgRewardMasterId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileReward : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileReward;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileReward *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileReward:(time_tns1_EmpProfileReward *)toAdd;
@property (readonly) NSMutableArray * EmpProfileReward;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileTotalIncome : NSObject {
	
/* elements */
	NSDate * CompanyId;
	NSDate * CreatedDate;
	NSNumber * DependancyDeduction;
	NSNumber * EmpProfileTotalIncomeId;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSNumber * GrossPayment;
	NSDate * ModifiedDate;
	NSNumber * Month;
	NSNumber * NetPayment;
	NSNumber * PIT;
	NSNumber * TaxableAmount;
	NSNumber * TotalDeduction;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileTotalIncome *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * DependancyDeduction;
@property (retain) NSNumber * EmpProfileTotalIncomeId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * GrossPayment;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Month;
@property (retain) NSNumber * NetPayment;
@property (retain) NSNumber * PIT;
@property (retain) NSNumber * TaxableAmount;
@property (retain) NSNumber * TotalDeduction;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileTotalIncome : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileTotalIncome;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileTotalIncome *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileTotalIncome:(time_tns1_EmpProfileTotalIncome *)toAdd;
@property (readonly) NSMutableArray * EmpProfileTotalIncome;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileTraining : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * AttachedDocumentUrl;
	NSString * Certifications;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileTrainingId;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	NSDate * ExpiredDate;
	USBoolean * IsDeleted;
	USBoolean * IsInternal;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * RelateRowId;
	NSString * Result;
	NSDate * StartDate;
	NSNumber * Status;
	NSString * TrainingProgram;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileTraining *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * AttachedDocumentUrl;
@property (retain) NSString * Certifications;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileTrainingId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) NSDate * ExpiredDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInternal;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * Result;
@property (retain) NSDate * StartDate;
@property (retain) NSNumber * Status;
@property (retain) NSString * TrainingProgram;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileTraining : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileTraining;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileTraining *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileTraining:(time_tns1_EmpProfileTraining *)toAdd;
@property (readonly) NSMutableArray * EmpProfileTraining;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileWageType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileWageTypeId;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ImplementationDate;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * TimeWageTypeCode;
	NSNumber * TimeWageTypeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileWageType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileWageTypeId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * TimeWageTypeCode;
@property (retain) NSNumber * TimeWageTypeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileWageType : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileWageType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileWageType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileWageType:(time_tns1_EmpProfileWageType *)toAdd;
@property (readonly) NSMutableArray * EmpProfileWageType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileWorkingForm : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileWorkingFormId;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ImplementationDate;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * TimeWorkingFormCode;
	NSNumber * TimeWorkingFormId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileWorkingForm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileWorkingFormId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * TimeWorkingFormCode;
@property (retain) NSNumber * TimeWorkingFormId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpProfileWorkingForm : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileWorkingForm;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpProfileWorkingForm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileWorkingForm:(time_tns1_EmpProfileWorkingForm *)toAdd;
@property (readonly) NSMutableArray * EmpProfileWorkingForm;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpSocialInsuranceSalary : NSObject {
	
/* elements */
	NSNumber * CLogCurrencyId;
	NSNumber * CLogCurrencyRateId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * DecisionCode;
	NSString * DecisionName;
	NSNumber * EmpSocialInsuranceSalaryId;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSDate * ExpirationDate;
	NSString * FileDecisionUrl;
	NSDate * ImplementationDate;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	USBoolean * IsIncrease;
	USBoolean * IsRecall;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * Reason;
	NSDate * RecallMonth;
	NSDate * SocialInsuranceDate;
	NSString * cEmpSISalary;
	NSString * cEmpSISalaryCoeficient;
	NSString * cEmpSISalaryGrade;
	NSNumber * nEmpSISalary;
	NSNumber * nEmpSISalaryCoeficient;
	NSNumber * nEmpSISalaryGrade;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpSocialInsuranceSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogCurrencyId;
@property (retain) NSNumber * CLogCurrencyRateId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DecisionCode;
@property (retain) NSString * DecisionName;
@property (retain) NSNumber * EmpSocialInsuranceSalaryId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSString * FileDecisionUrl;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsIncrease;
@property (retain) USBoolean * IsRecall;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * Reason;
@property (retain) NSDate * RecallMonth;
@property (retain) NSDate * SocialInsuranceDate;
@property (retain) NSString * cEmpSISalary;
@property (retain) NSString * cEmpSISalaryCoeficient;
@property (retain) NSString * cEmpSISalaryGrade;
@property (retain) NSNumber * nEmpSISalary;
@property (retain) NSNumber * nEmpSISalaryCoeficient;
@property (retain) NSNumber * nEmpSISalaryGrade;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfEmpSocialInsuranceSalary : NSObject {
	
/* elements */
	NSMutableArray *EmpSocialInsuranceSalary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfEmpSocialInsuranceSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpSocialInsuranceSalary:(time_tns1_EmpSocialInsuranceSalary *)toAdd;
@property (readonly) NSMutableArray * EmpSocialInsuranceSalary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_NCClientConnection : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * ConnectionId;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSNumber * NCClientConnectionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_NCClientConnection *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ConnectionId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * NCClientConnectionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfNCClientConnection : NSObject {
	
/* elements */
	NSMutableArray *NCClientConnection;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfNCClientConnection *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addNCClientConnection:(time_tns1_NCClientConnection *)toAdd;
@property (readonly) NSMutableArray * NCClientConnection;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_NCMessageType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * NCMessageTypeId;
	time_tns1_ArrayOfNCMessage * NCMessages;
	NSString * Name;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_NCMessageType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NCMessageTypeId;
@property (retain) time_tns1_ArrayOfNCMessage * NCMessages;
@property (retain) NSString * Name;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_NCMessage : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_Employee * Employee;
	time_tns1_Employee * Employee1;
	NSString * FullMessage;
	USBoolean * IsDeleted;
	USBoolean * IsNew;
	NSDate * ModifiedDate;
	NSNumber * NCMessageId;
	time_tns1_NCMessageType * NCMessageType;
	NSNumber * NCMessageTypeId;
	NSNumber * ReceiverEmployeeId;
	NSNumber * SenderEmployeeId;
	NSString * ShortcutMessage;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_NCMessage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) time_tns1_Employee * Employee1;
@property (retain) NSString * FullMessage;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsNew;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NCMessageId;
@property (retain) time_tns1_NCMessageType * NCMessageType;
@property (retain) NSNumber * NCMessageTypeId;
@property (retain) NSNumber * ReceiverEmployeeId;
@property (retain) NSNumber * SenderEmployeeId;
@property (retain) NSString * ShortcutMessage;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfNCMessage : NSObject {
	
/* elements */
	NSMutableArray *NCMessage;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfNCMessage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addNCMessage:(time_tns1_NCMessage *)toAdd;
@property (readonly) NSMutableArray * NCMessage;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_Project : NSObject {
	
/* elements */
	NSString * Description;
	time_tns1_Employee * Employee;
	NSDate * EndDate;
	NSNumber * Id_;
	NSString * Name;
	NSNumber * OwnerId;
	NSNumber * Progress;
	time_tns1_ArrayOfProjectMember * ProjectMembers;
	NSDate * StartDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_Project *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Description;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSDate * EndDate;
@property (retain) NSNumber * Id_;
@property (retain) NSString * Name;
@property (retain) NSNumber * OwnerId;
@property (retain) NSNumber * Progress;
@property (retain) time_tns1_ArrayOfProjectMember * ProjectMembers;
@property (retain) NSDate * StartDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ProjectMember : NSObject {
	
/* elements */
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	NSNumber * Id_;
	time_tns1_Project * Project;
	NSNumber * ProjectId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ProjectMember *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * Id_;
@property (retain) time_tns1_Project * Project;
@property (retain) NSNumber * ProjectId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfProjectMember : NSObject {
	
/* elements */
	NSMutableArray *ProjectMember;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfProjectMember *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addProjectMember:(time_tns1_ProjectMember *)toAdd;
@property (readonly) NSMutableArray * ProjectMember;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfProject : NSObject {
	
/* elements */
	NSMutableArray *Project;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfProject *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addProject:(time_tns1_Project *)toAdd;
@property (readonly) NSMutableArray * Project;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ReportEmployeeRequestedBook : NSObject {
	
/* elements */
	NSString * Address1;
	NSString * Benefit;
	NSDate * BirthDay;
	NSString * CLogCityNoIns;
	NSString * CLogHospitalCode;
	time_tns1_Employee * Employee;
	NSString * Ethnicity;
	NSString * FullName;
	NSString * Gender;
	USBoolean * HadSIBook;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	NSString * Note;
	NSString * OrgUnitName;
	NSNumber * ReportEmployeeRequestedBookId;
	NSDate * RequestedDay;
	NSString * SICode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ReportEmployeeRequestedBook *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address1;
@property (retain) NSString * Benefit;
@property (retain) NSDate * BirthDay;
@property (retain) NSString * CLogCityNoIns;
@property (retain) NSString * CLogHospitalCode;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * Ethnicity;
@property (retain) NSString * FullName;
@property (retain) NSString * Gender;
@property (retain) USBoolean * HadSIBook;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) NSString * Note;
@property (retain) NSString * OrgUnitName;
@property (retain) NSNumber * ReportEmployeeRequestedBookId;
@property (retain) NSDate * RequestedDay;
@property (retain) NSString * SICode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_SelfLeaveRequest : NSObject {
	
/* elements */
	NSString * Address;
	NSNumber * ApprovalStatus;
	NSString * ApproverCode;
	NSNumber * ApproverId;
	NSDate * CreatedDate;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	USBoolean * IsCompensatoryLeave;
	USBoolean * IsDeleted;
	USBoolean * IsLeaveWithoutPay;
	USBoolean * IsPaidLeave;
	USBoolean * IsUnpaidLeave;
	NSDate * ModifiedDate;
	NSNumber * NumberDaysOff;
	NSNumber * OrganizationId;
	NSString * PhoneNumber;
	NSString * Reason;
	NSNumber * SalaryPenaltyValue;
	NSNumber * SelfLeaveRequestId;
	NSDate * StartDate;
	NSNumber * TimeAbsenceTypeId;
	NSString * WorkflowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_SelfLeaveRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSNumber * ApprovalStatus;
@property (retain) NSString * ApproverCode;
@property (retain) NSNumber * ApproverId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsCompensatoryLeave;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsLeaveWithoutPay;
@property (retain) USBoolean * IsPaidLeave;
@property (retain) USBoolean * IsUnpaidLeave;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NumberDaysOff;
@property (retain) NSNumber * OrganizationId;
@property (retain) NSString * PhoneNumber;
@property (retain) NSString * Reason;
@property (retain) NSNumber * SalaryPenaltyValue;
@property (retain) NSNumber * SelfLeaveRequestId;
@property (retain) NSDate * StartDate;
@property (retain) NSNumber * TimeAbsenceTypeId;
@property (retain) NSString * WorkflowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfSelfLeaveRequest : NSObject {
	
/* elements */
	NSMutableArray *SelfLeaveRequest;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfSelfLeaveRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSelfLeaveRequest:(time_tns1_SelfLeaveRequest *)toAdd;
@property (readonly) NSMutableArray * SelfLeaveRequest;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfSysRecPlanApprover : NSObject {
	
/* elements */
	NSMutableArray *SysRecPlanApprover;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfSysRecPlanApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysRecPlanApprover:(time_tns1_SysRecPlanApprover *)toAdd;
@property (readonly) NSMutableArray * SysRecPlanApprover;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfSysRecRequirementApprover : NSObject {
	
/* elements */
	NSMutableArray *SysRecRequirementApprover;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfSysRecRequirementApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysRecRequirementApprover:(time_tns1_SysRecRequirementApprover *)toAdd;
@property (readonly) NSMutableArray * SysRecRequirementApprover;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TrainingEmpPlan : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	time_tns1_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsSubmited;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * State;
	NSNumber * StrategyGoalId;
	NSString * Target;
	time_tns1_ArrayOfTrainingEmpPlanApproved * TrainingEmpPlanApproveds;
	NSNumber * TrainingEmpPlanId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TrainingEmpPlan *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsSubmited;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * State;
@property (retain) NSNumber * StrategyGoalId;
@property (retain) NSString * Target;
@property (retain) time_tns1_ArrayOfTrainingEmpPlanApproved * TrainingEmpPlanApproveds;
@property (retain) NSNumber * TrainingEmpPlanId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TrainingEmpPlanApproved : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	time_tns1_SysTrainingApprover * SysTrainingApprover;
	NSNumber * SysTrainingApproverId;
	time_tns1_TrainingEmpPlan * TrainingEmpPlan;
	NSNumber * TrainingEmpPlanApprovedId;
	NSNumber * TrainingEmpPlanId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TrainingEmpPlanApproved *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_SysTrainingApprover * SysTrainingApprover;
@property (retain) NSNumber * SysTrainingApproverId;
@property (retain) time_tns1_TrainingEmpPlan * TrainingEmpPlan;
@property (retain) NSNumber * TrainingEmpPlanApprovedId;
@property (retain) NSNumber * TrainingEmpPlanId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTrainingEmpPlanApproved : NSObject {
	
/* elements */
	NSMutableArray *TrainingEmpPlanApproved;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTrainingEmpPlanApproved *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingEmpPlanApproved:(time_tns1_TrainingEmpPlanApproved *)toAdd;
@property (readonly) NSMutableArray * TrainingEmpPlanApproved;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_SysTrainingApprover : NSObject {
	
/* elements */
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	time_tns1_Employee * Employee;
	USBoolean * IsDeleted;
	NSNumber * Number;
	NSNumber * SysTrainingApproverId;
	time_tns1_ArrayOfTrainingEmpPlanApproved * TrainingEmpPlanApproveds;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_SysTrainingApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Number;
@property (retain) NSNumber * SysTrainingApproverId;
@property (retain) time_tns1_ArrayOfTrainingEmpPlanApproved * TrainingEmpPlanApproveds;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfSysTrainingApprover : NSObject {
	
/* elements */
	NSMutableArray *SysTrainingApprover;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfSysTrainingApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysTrainingApprover:(time_tns1_SysTrainingApprover *)toAdd;
@property (readonly) NSMutableArray * SysTrainingApprover;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_Task : NSObject {
	
/* elements */
	NSString * Description;
	time_tns1_Employee * Employee;
	NSDate * FromDate;
	NSNumber * Id_;
	NSString * Name;
	NSNumber * Process;
	NSNumber * ProjectId;
	NSNumber * Status;
	NSNumber * TaskOwnerId;
	NSDate * ToDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_Task *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Description;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSDate * FromDate;
@property (retain) NSNumber * Id_;
@property (retain) NSString * Name;
@property (retain) NSNumber * Process;
@property (retain) NSNumber * ProjectId;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * TaskOwnerId;
@property (retain) NSDate * ToDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTask : NSObject {
	
/* elements */
	NSMutableArray *Task;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTask *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTask:(time_tns1_Task *)toAdd;
@property (readonly) NSMutableArray * Task;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTrainingEmpPlan : NSObject {
	
/* elements */
	NSMutableArray *TrainingEmpPlan;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTrainingEmpPlan *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingEmpPlan:(time_tns1_TrainingEmpPlan *)toAdd;
@property (readonly) NSMutableArray * TrainingEmpPlan;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_Employee : NSObject {
	
/* elements */
	NSDate * BirthDay;
	NSString * BirthPlace;
	NSNumber * BirthPlaceId;
	time_tns1_ArrayOfCBAccidentInsurance * CBAccidentInsurances;
	time_tns1_ArrayOfCBCompensationEmployeeGroupDetail * CBCompensationEmployeeGroupDetails;
	time_tns1_ArrayOfCBConvalescence * CBConvalescences;
	time_tns1_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
	time_tns1_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
	time_tns1_ArrayOfCBHealthInsuranceDetail * CBHealthInsuranceDetails;
	time_tns1_CLogCity * CLogCity;
	time_tns1_CLogCity * CLogCity1;
	time_tns1_CLogEmployeeType * CLogEmployeeType;
	time_tns1_ArrayOfCLogTrainer * CLogTrainers;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	time_tns1_ArrayOfEmpBasicProfile * EmpBasicProfiles;
	time_tns1_ArrayOfEmpCompetency * EmpCompetencies;
	time_tns1_ArrayOfEmpCompetencyRating * EmpCompetencyRatings;
	time_tns1_ArrayOfEmpContract * EmpContracts;
	time_tns1_ArrayOfEmpPerformanceAppraisal * EmpPerformanceAppraisals;
	time_tns1_ArrayOfEmpProfileAllowance * EmpProfileAllowances;
	time_tns1_ArrayOfEmpProfileBaseSalary * EmpProfileBaseSalaries;
	time_tns1_ArrayOfEmpProfileBenefit * EmpProfileBenefits;
	time_tns1_ArrayOfEmpProfileComment * EmpProfileComments;
	time_tns1_ArrayOfEmpProfileComment * EmpProfileComments1;
	time_tns1_ArrayOfEmpProfileComputingSkill * EmpProfileComputingSkills;
	time_tns1_ArrayOfEmpProfileContact * EmpProfileContacts;
	time_tns1_ArrayOfEmpProfileDegree * EmpProfileDegrees;
	time_tns1_ArrayOfEmpProfileDiscipline * EmpProfileDisciplines;
	time_tns1_ArrayOfEmpProfileEducation * EmpProfileEducations;
	time_tns1_ArrayOfEmpProfileEquipment * EmpProfileEquipments;
	time_tns1_ArrayOfEmpProfileExperience * EmpProfileExperiences;
	time_tns1_ArrayOfEmpProfileFamilyRelationship * EmpProfileFamilyRelationships;
	time_tns1_ArrayOfEmpProfileForeignLanguage * EmpProfileForeignLanguages;
	time_tns1_ArrayOfEmpProfileHealthInsurance * EmpProfileHealthInsurances;
	time_tns1_ArrayOfEmpProfileHealthy * EmpProfileHealthies;
	time_tns1_ArrayOfEmpProfileHobby * EmpProfileHobbies;
	time_tns1_ArrayOfEmpProfileInternalCourse * EmpProfileInternalCourses;
	time_tns1_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions;
	time_tns1_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions1;
	time_tns1_ArrayOfEmpProfileLeaveRegime * EmpProfileLeaveRegimes;
	time_tns1_ArrayOfEmpProfileOtherBenefit * EmpProfileOtherBenefits;
	time_tns1_ArrayOfEmpProfileParticipation * EmpProfileParticipations;
	time_tns1_ArrayOfEmpProfilePersonality * EmpProfilePersonalities;
	time_tns1_ArrayOfEmpProfileProcessOfWork * EmpProfileProcessOfWorks;
	time_tns1_ArrayOfEmpProfileQualification * EmpProfileQualifications;
	time_tns1_ArrayOfEmpProfileReward * EmpProfileRewards;
	time_tns1_ArrayOfEmpProfileSkill * EmpProfileSkills;
	time_tns1_ArrayOfEmpProfileTotalIncome * EmpProfileTotalIncomes;
	time_tns1_ArrayOfEmpProfileTraining * EmpProfileTrainings;
	time_tns1_ArrayOfEmpProfileWageType * EmpProfileWageTypes;
	time_tns1_ArrayOfEmpProfileWorkingExperience * EmpProfileWorkingExperiences;
	time_tns1_ArrayOfEmpProfileWorkingForm * EmpProfileWorkingForms;
	time_tns1_ArrayOfEmpSocialInsuranceSalary * EmpSocialInsuranceSalaries;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * EmployeeTKCode;
	NSNumber * EmployeeTypeId;
	NSDate * EndWorkingDate;
	NSDate * EntryDate;
	NSString * Ethnicity;
	NSNumber * EthnicityId;
	NSString * FirstName;
	NSString * FullName;
	time_tns1_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals;
	time_tns1_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals1;
	time_tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
	time_tns1_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
	time_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
	time_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
	time_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
	time_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives1;
	time_tns1_ArrayOfGPObjectiveInitiative * GPObjectiveInitiatives;
	time_tns1_ArrayOfGPPerformanceYearEndResult * GPPerformanceYearEndResults;
	USBoolean * Gender;
	NSString * HomeTown;
	NSNumber * HomeTownId;
	NSString * IdentityCardNo;
	NSString * ImageUrl;
	USBoolean * IsDeleted;
	USBoolean * IsOnlyYearOfBirthday;
	time_tns1_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
	time_tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
	time_tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts1;
	NSString * LastName;
	NSString * MainLanguage;
	NSNumber * MainLanguageId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	time_tns1_ArrayOfNCClientConnection * NCClientConnections;
	time_tns1_ArrayOfNCMessage * NCMessages;
	time_tns1_ArrayOfNCMessage * NCMessages1;
	NSDate * OfficialDate;
	NSString * OrgGroupCd;
	NSNumber * OrgGroupId;
	NSDate * ProbationaryDate;
	NSDate * ProbationaryEndDate;
	time_tns1_ArrayOfProjectMember * ProjectMembers;
	time_tns1_ArrayOfProject * Projects;
	time_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
	time_tns1_ArrayOfRecInterviewer * RecInterviewers;
	time_tns1_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
	time_tns1_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
	time_tns1_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
	NSString * Religion;
	NSNumber * ReligionId;
	time_tns1_ReportEmployeeRequestedBook * ReportEmployeeRequestedBook;
	time_tns1_ArrayOfSelfLeaveRequest * SelfLeaveRequests;
	time_tns1_ArrayOfSysRecPlanApprover * SysRecPlanApprovers;
	time_tns1_ArrayOfSysRecRequirementApprover * SysRecRequirementApprovers;
	time_tns1_ArrayOfSysTrainingApprover * SysTrainingApprovers;
	time_tns1_ArrayOfTask * Tasks;
	time_tns1_ArrayOfTrainingCourseEmployee * TrainingCourseEmployees;
	time_tns1_ArrayOfTrainingEmpPlan * TrainingEmpPlans;
	time_tns1_ArrayOfTrainingPlanEmployee * TrainingPlanEmployees;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_Employee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * BirthDay;
@property (retain) NSString * BirthPlace;
@property (retain) NSNumber * BirthPlaceId;
@property (retain) time_tns1_ArrayOfCBAccidentInsurance * CBAccidentInsurances;
@property (retain) time_tns1_ArrayOfCBCompensationEmployeeGroupDetail * CBCompensationEmployeeGroupDetails;
@property (retain) time_tns1_ArrayOfCBConvalescence * CBConvalescences;
@property (retain) time_tns1_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
@property (retain) time_tns1_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
@property (retain) time_tns1_ArrayOfCBHealthInsuranceDetail * CBHealthInsuranceDetails;
@property (retain) time_tns1_CLogCity * CLogCity;
@property (retain) time_tns1_CLogCity * CLogCity1;
@property (retain) time_tns1_CLogEmployeeType * CLogEmployeeType;
@property (retain) time_tns1_ArrayOfCLogTrainer * CLogTrainers;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) time_tns1_ArrayOfEmpBasicProfile * EmpBasicProfiles;
@property (retain) time_tns1_ArrayOfEmpCompetency * EmpCompetencies;
@property (retain) time_tns1_ArrayOfEmpCompetencyRating * EmpCompetencyRatings;
@property (retain) time_tns1_ArrayOfEmpContract * EmpContracts;
@property (retain) time_tns1_ArrayOfEmpPerformanceAppraisal * EmpPerformanceAppraisals;
@property (retain) time_tns1_ArrayOfEmpProfileAllowance * EmpProfileAllowances;
@property (retain) time_tns1_ArrayOfEmpProfileBaseSalary * EmpProfileBaseSalaries;
@property (retain) time_tns1_ArrayOfEmpProfileBenefit * EmpProfileBenefits;
@property (retain) time_tns1_ArrayOfEmpProfileComment * EmpProfileComments;
@property (retain) time_tns1_ArrayOfEmpProfileComment * EmpProfileComments1;
@property (retain) time_tns1_ArrayOfEmpProfileComputingSkill * EmpProfileComputingSkills;
@property (retain) time_tns1_ArrayOfEmpProfileContact * EmpProfileContacts;
@property (retain) time_tns1_ArrayOfEmpProfileDegree * EmpProfileDegrees;
@property (retain) time_tns1_ArrayOfEmpProfileDiscipline * EmpProfileDisciplines;
@property (retain) time_tns1_ArrayOfEmpProfileEducation * EmpProfileEducations;
@property (retain) time_tns1_ArrayOfEmpProfileEquipment * EmpProfileEquipments;
@property (retain) time_tns1_ArrayOfEmpProfileExperience * EmpProfileExperiences;
@property (retain) time_tns1_ArrayOfEmpProfileFamilyRelationship * EmpProfileFamilyRelationships;
@property (retain) time_tns1_ArrayOfEmpProfileForeignLanguage * EmpProfileForeignLanguages;
@property (retain) time_tns1_ArrayOfEmpProfileHealthInsurance * EmpProfileHealthInsurances;
@property (retain) time_tns1_ArrayOfEmpProfileHealthy * EmpProfileHealthies;
@property (retain) time_tns1_ArrayOfEmpProfileHobby * EmpProfileHobbies;
@property (retain) time_tns1_ArrayOfEmpProfileInternalCourse * EmpProfileInternalCourses;
@property (retain) time_tns1_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions;
@property (retain) time_tns1_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions1;
@property (retain) time_tns1_ArrayOfEmpProfileLeaveRegime * EmpProfileLeaveRegimes;
@property (retain) time_tns1_ArrayOfEmpProfileOtherBenefit * EmpProfileOtherBenefits;
@property (retain) time_tns1_ArrayOfEmpProfileParticipation * EmpProfileParticipations;
@property (retain) time_tns1_ArrayOfEmpProfilePersonality * EmpProfilePersonalities;
@property (retain) time_tns1_ArrayOfEmpProfileProcessOfWork * EmpProfileProcessOfWorks;
@property (retain) time_tns1_ArrayOfEmpProfileQualification * EmpProfileQualifications;
@property (retain) time_tns1_ArrayOfEmpProfileReward * EmpProfileRewards;
@property (retain) time_tns1_ArrayOfEmpProfileSkill * EmpProfileSkills;
@property (retain) time_tns1_ArrayOfEmpProfileTotalIncome * EmpProfileTotalIncomes;
@property (retain) time_tns1_ArrayOfEmpProfileTraining * EmpProfileTrainings;
@property (retain) time_tns1_ArrayOfEmpProfileWageType * EmpProfileWageTypes;
@property (retain) time_tns1_ArrayOfEmpProfileWorkingExperience * EmpProfileWorkingExperiences;
@property (retain) time_tns1_ArrayOfEmpProfileWorkingForm * EmpProfileWorkingForms;
@property (retain) time_tns1_ArrayOfEmpSocialInsuranceSalary * EmpSocialInsuranceSalaries;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * EmployeeTKCode;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSDate * EndWorkingDate;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * Ethnicity;
@property (retain) NSNumber * EthnicityId;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) time_tns1_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals;
@property (retain) time_tns1_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals1;
@property (retain) time_tns1_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
@property (retain) time_tns1_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
@property (retain) time_tns1_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
@property (retain) time_tns1_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
@property (retain) time_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
@property (retain) time_tns1_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives1;
@property (retain) time_tns1_ArrayOfGPObjectiveInitiative * GPObjectiveInitiatives;
@property (retain) time_tns1_ArrayOfGPPerformanceYearEndResult * GPPerformanceYearEndResults;
@property (retain) USBoolean * Gender;
@property (retain) NSString * HomeTown;
@property (retain) NSNumber * HomeTownId;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) time_tns1_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
@property (retain) time_tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
@property (retain) time_tns1_ArrayOfLMSCourseTranscript * LMSCourseTranscripts1;
@property (retain) NSString * LastName;
@property (retain) NSString * MainLanguage;
@property (retain) NSNumber * MainLanguageId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) time_tns1_ArrayOfNCClientConnection * NCClientConnections;
@property (retain) time_tns1_ArrayOfNCMessage * NCMessages;
@property (retain) time_tns1_ArrayOfNCMessage * NCMessages1;
@property (retain) NSDate * OfficialDate;
@property (retain) NSString * OrgGroupCd;
@property (retain) NSNumber * OrgGroupId;
@property (retain) NSDate * ProbationaryDate;
@property (retain) NSDate * ProbationaryEndDate;
@property (retain) time_tns1_ArrayOfProjectMember * ProjectMembers;
@property (retain) time_tns1_ArrayOfProject * Projects;
@property (retain) time_tns1_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
@property (retain) time_tns1_ArrayOfRecInterviewer * RecInterviewers;
@property (retain) time_tns1_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
@property (retain) time_tns1_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
@property (retain) time_tns1_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
@property (retain) NSString * Religion;
@property (retain) NSNumber * ReligionId;
@property (retain) time_tns1_ReportEmployeeRequestedBook * ReportEmployeeRequestedBook;
@property (retain) time_tns1_ArrayOfSelfLeaveRequest * SelfLeaveRequests;
@property (retain) time_tns1_ArrayOfSysRecPlanApprover * SysRecPlanApprovers;
@property (retain) time_tns1_ArrayOfSysRecRequirementApprover * SysRecRequirementApprovers;
@property (retain) time_tns1_ArrayOfSysTrainingApprover * SysTrainingApprovers;
@property (retain) time_tns1_ArrayOfTask * Tasks;
@property (retain) time_tns1_ArrayOfTrainingCourseEmployee * TrainingCourseEmployees;
@property (retain) time_tns1_ArrayOfTrainingEmpPlan * TrainingEmpPlans;
@property (retain) time_tns1_ArrayOfTrainingPlanEmployee * TrainingPlanEmployees;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_EmpProfileLeaveRegime : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CompensatoryLeave;
	NSNumber * CompensatoryLeaveOfYear;
	NSNumber * CompensatoryRemainedLeave;
	NSNumber * CompensatoryUsedLeave;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileLeaveRegimeId;
	time_tns1_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * MonthlyLeave;
	NSNumber * PlusMinusLeave;
	NSNumber * PreviousLeave;
	NSNumber * RemainedLeave;
	NSNumber * SeniorityLeave;
	NSNumber * TotalUsedLeave;
	NSNumber * Year;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_EmpProfileLeaveRegime *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CompensatoryLeave;
@property (retain) NSNumber * CompensatoryLeaveOfYear;
@property (retain) NSNumber * CompensatoryRemainedLeave;
@property (retain) NSNumber * CompensatoryUsedLeave;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileLeaveRegimeId;
@property (retain) time_tns1_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * MonthlyLeave;
@property (retain) NSNumber * PlusMinusLeave;
@property (retain) NSNumber * PreviousLeave;
@property (retain) NSNumber * RemainedLeave;
@property (retain) NSNumber * SeniorityLeave;
@property (retain) NSNumber * TotalUsedLeave;
@property (retain) NSNumber * Year;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TimeRequestMasTer : NSObject {
	
/* elements */
	NSDate * ApprovalDate;
	NSString * ApproverCode;
	NSNumber * ApproverId;
	NSString * Code;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * Flex1;
	NSNumber * Flex10;
	NSNumber * Flex11;
	NSNumber * Flex12;
	NSNumber * Flex13;
	USBoolean * Flex14;
	USBoolean * Flex15;
	USBoolean * Flex16;
	USBoolean * Flex17;
	NSDate * Flex18;
	NSDate * Flex19;
	NSString * Flex2;
	NSDate * Flex20;
	NSDate * Flex21;
	NSNumber * Flex22;
	NSNumber * Flex23;
	NSString * Flex24;
	NSString * Flex25;
	NSString * Flex26;
	NSString * Flex27;
	NSDate * Flex28;
	NSDate * Flex29;
	NSString * Flex3;
	NSString * Flex30;
	NSString * Flex31;
	NSString * Flex32;
	NSString * Flex33;
	NSString * Flex34;
	NSString * Flex35;
	NSString * Flex36;
	NSString * Flex37;
	NSString * Flex38;
	NSString * Flex39;
	NSDate * Flex4;
	NSNumber * Flex40;
	NSNumber * Flex41;
	NSNumber * Flex42;
	NSNumber * Flex43;
	NSNumber * Flex44;
	NSNumber * Flex5;
	NSNumber * Flex6;
	NSNumber * Flex7;
	NSNumber * Flex8;
	NSNumber * Flex9;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * Status;
	NSNumber * TimeRequestMasTerId;
	NSString * Type;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TimeRequestMasTer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ApprovalDate;
@property (retain) NSString * ApproverCode;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * Code;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * Flex1;
@property (retain) NSNumber * Flex10;
@property (retain) NSNumber * Flex11;
@property (retain) NSNumber * Flex12;
@property (retain) NSNumber * Flex13;
@property (retain) USBoolean * Flex14;
@property (retain) USBoolean * Flex15;
@property (retain) USBoolean * Flex16;
@property (retain) USBoolean * Flex17;
@property (retain) NSDate * Flex18;
@property (retain) NSDate * Flex19;
@property (retain) NSString * Flex2;
@property (retain) NSDate * Flex20;
@property (retain) NSDate * Flex21;
@property (retain) NSNumber * Flex22;
@property (retain) NSNumber * Flex23;
@property (retain) NSString * Flex24;
@property (retain) NSString * Flex25;
@property (retain) NSString * Flex26;
@property (retain) NSString * Flex27;
@property (retain) NSDate * Flex28;
@property (retain) NSDate * Flex29;
@property (retain) NSString * Flex3;
@property (retain) NSString * Flex30;
@property (retain) NSString * Flex31;
@property (retain) NSString * Flex32;
@property (retain) NSString * Flex33;
@property (retain) NSString * Flex34;
@property (retain) NSString * Flex35;
@property (retain) NSString * Flex36;
@property (retain) NSString * Flex37;
@property (retain) NSString * Flex38;
@property (retain) NSString * Flex39;
@property (retain) NSDate * Flex4;
@property (retain) NSNumber * Flex40;
@property (retain) NSNumber * Flex41;
@property (retain) NSNumber * Flex42;
@property (retain) NSNumber * Flex43;
@property (retain) NSNumber * Flex44;
@property (retain) NSNumber * Flex5;
@property (retain) NSNumber * Flex6;
@property (retain) NSNumber * Flex7;
@property (retain) NSNumber * Flex8;
@property (retain) NSNumber * Flex9;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * TimeRequestMasTerId;
@property (retain) NSString * Type;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTimeRequestMasTer : NSObject {
	
/* elements */
	NSMutableArray *TimeRequestMasTer;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTimeRequestMasTer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeRequestMasTer:(time_tns1_TimeRequestMasTer *)toAdd;
@property (readonly) NSMutableArray * TimeRequestMasTer;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TB_ESS_TIME_REQ_APP_MAPPING : NSObject {
	
/* elements */
	NSDate * APPROVAL_DATE;
	NSString * APPROVER_CODE;
	NSNumber * APPROVER_ID;
	NSString * APPROVER_NAME;
	NSNumber * APPROVE_STATUS;
	NSNumber * COMPANY_ID;
	NSDate * CREATED_DATE;
	USBoolean * IS_DELETED;
	NSDate * MODIFIED_DATE;
	NSNumber * ORDER_BY;
	NSString * REMARK;
	NSNumber * TIM_REQ_APP_MAPPING_ID;
	NSNumber * TIM_REQ_MASTER_ID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TB_ESS_TIME_REQ_APP_MAPPING *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * APPROVAL_DATE;
@property (retain) NSString * APPROVER_CODE;
@property (retain) NSNumber * APPROVER_ID;
@property (retain) NSString * APPROVER_NAME;
@property (retain) NSNumber * APPROVE_STATUS;
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSDate * CREATED_DATE;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSDate * MODIFIED_DATE;
@property (retain) NSNumber * ORDER_BY;
@property (retain) NSString * REMARK;
@property (retain) NSNumber * TIM_REQ_APP_MAPPING_ID;
@property (retain) NSNumber * TIM_REQ_MASTER_ID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTB_ESS_TIME_REQ_APP_MAPPING : NSObject {
	
/* elements */
	NSMutableArray *TB_ESS_TIME_REQ_APP_MAPPING;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTB_ESS_TIME_REQ_APP_MAPPING *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTB_ESS_TIME_REQ_APP_MAPPING:(time_tns1_TB_ESS_TIME_REQ_APP_MAPPING *)toAdd;
@property (readonly) NSMutableArray * TB_ESS_TIME_REQ_APP_MAPPING;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_SP_ESS_USER_REQ_EQUIP_Result : NSObject {
	
/* elements */
	NSDate * ApprovalDate;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * EQUIPMENT_BY_EMP_CODE;
	NSNumber * EQUIPMENT_BY_EMP_ID;
	NSString * EQUIPMENT_BY_EMP_NAME;
	NSString * EQUIPMENT_BY_ORG_NAME;
	NSString * EQUIPMENT_CODE;
	NSString * EQUIPMENT_FOR_EMP_CODE;
	NSNumber * EQUIPMENT_FOR_EMP_ID;
	NSString * EQUIPMENT_FOR_EMP_NAME;
	NSNumber * EQUIPMENT_FOR_ORG_ID;
	NSNumber * EQUIPMENT_IN_PLAN;
	NSNumber * EQUIPMENT_TYPE;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * Status;
	NSNumber * TimeRequestMasTerId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_SP_ESS_USER_REQ_EQUIP_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ApprovalDate;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * EQUIPMENT_BY_EMP_CODE;
@property (retain) NSNumber * EQUIPMENT_BY_EMP_ID;
@property (retain) NSString * EQUIPMENT_BY_EMP_NAME;
@property (retain) NSString * EQUIPMENT_BY_ORG_NAME;
@property (retain) NSString * EQUIPMENT_CODE;
@property (retain) NSString * EQUIPMENT_FOR_EMP_CODE;
@property (retain) NSNumber * EQUIPMENT_FOR_EMP_ID;
@property (retain) NSString * EQUIPMENT_FOR_EMP_NAME;
@property (retain) NSNumber * EQUIPMENT_FOR_ORG_ID;
@property (retain) NSNumber * EQUIPMENT_IN_PLAN;
@property (retain) NSNumber * EQUIPMENT_TYPE;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * TimeRequestMasTerId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfSP_ESS_USER_REQ_EQUIP_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_ESS_USER_REQ_EQUIP_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfSP_ESS_USER_REQ_EQUIP_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_ESS_USER_REQ_EQUIP_Result:(time_tns1_SP_ESS_USER_REQ_EQUIP_Result *)toAdd;
@property (readonly) NSMutableArray * SP_ESS_USER_REQ_EQUIP_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_SP_ESS_USER_APP_REQ_EQUIP_Result : NSObject {
	
/* elements */
	NSDate * APPROVAL_DATE;
	NSString * APPROVER_CODE;
	NSNumber * APPROVER_ID;
	NSString * APPROVER_NAME;
	NSNumber * APPROVE_STATUS;
	NSDate * ApprovalDate;
	NSNumber * COMPANY_ID;
	NSDate * CREATED_DATE;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * EQUIPMENT_BY_EMP_CODE;
	NSNumber * EQUIPMENT_BY_EMP_ID;
	NSString * EQUIPMENT_BY_EMP_NAME;
	NSString * EQUIPMENT_BY_ORG_NAME;
	NSString * EQUIPMENT_CODE;
	NSString * EQUIPMENT_FOR_EMP_CODE;
	NSNumber * EQUIPMENT_FOR_EMP_ID;
	NSString * EQUIPMENT_FOR_EMP_NAME;
	NSNumber * EQUIPMENT_FOR_ORG_ID;
	NSNumber * EQUIPMENT_IN_PLAN;
	NSNumber * EQUIPMENT_TYPE;
	USBoolean * IS_DELETED;
	USBoolean * IsDeleted;
	NSDate * MODIFIED_DATE;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * ORDER_BY;
	NSString * REMARK;
	NSNumber * Status;
	NSNumber * TIM_REQ_APP_MAPPING_ID;
	NSNumber * TIM_REQ_MASTER_ID;
	NSNumber * TimeRequestMasTerId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_SP_ESS_USER_APP_REQ_EQUIP_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * APPROVAL_DATE;
@property (retain) NSString * APPROVER_CODE;
@property (retain) NSNumber * APPROVER_ID;
@property (retain) NSString * APPROVER_NAME;
@property (retain) NSNumber * APPROVE_STATUS;
@property (retain) NSDate * ApprovalDate;
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSDate * CREATED_DATE;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * EQUIPMENT_BY_EMP_CODE;
@property (retain) NSNumber * EQUIPMENT_BY_EMP_ID;
@property (retain) NSString * EQUIPMENT_BY_EMP_NAME;
@property (retain) NSString * EQUIPMENT_BY_ORG_NAME;
@property (retain) NSString * EQUIPMENT_CODE;
@property (retain) NSString * EQUIPMENT_FOR_EMP_CODE;
@property (retain) NSNumber * EQUIPMENT_FOR_EMP_ID;
@property (retain) NSString * EQUIPMENT_FOR_EMP_NAME;
@property (retain) NSNumber * EQUIPMENT_FOR_ORG_ID;
@property (retain) NSNumber * EQUIPMENT_IN_PLAN;
@property (retain) NSNumber * EQUIPMENT_TYPE;
@property (retain) USBoolean * IS_DELETED;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * MODIFIED_DATE;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * ORDER_BY;
@property (retain) NSString * REMARK;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * TIM_REQ_APP_MAPPING_ID;
@property (retain) NSNumber * TIM_REQ_MASTER_ID;
@property (retain) NSNumber * TimeRequestMasTerId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfSP_ESS_USER_APP_REQ_EQUIP_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_ESS_USER_APP_REQ_EQUIP_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfSP_ESS_USER_APP_REQ_EQUIP_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_ESS_USER_APP_REQ_EQUIP_Result:(time_tns1_SP_ESS_USER_APP_REQ_EQUIP_Result *)toAdd;
@property (readonly) NSMutableArray * SP_ESS_USER_APP_REQ_EQUIP_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTimeSysConfigParam : NSObject {
	
/* elements */
	NSMutableArray *TimeSysConfigParam;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTimeSysConfigParam *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeSysConfigParam:(time_tns1_TimeSysConfigParam *)toAdd;
@property (readonly) NSMutableArray * TimeSysConfigParam;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_V_OrgUnit : NSObject {
	
/* elements */
	NSString * Address;
	NSNumber * CompanyId;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	USBoolean * IsLeaderActive;
	NSString * Name;
	NSNumber * OrgJobTitleId;
	NSString * OrgJobTitleLevelCode;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgUnitLeaderCode;
	NSString * OrgUnitLeaderFirstName;
	NSString * OrgUnitLeaderFullName;
	NSNumber * OrgUnitLeaderId;
	NSString * OrgUnitLeaderImageURL;
	NSString * OrgUnitLeaderJobPositionName;
	NSString * OrgUnitLeaderLastName;
	NSString * ParentCode;
	NSNumber * ParentId;
	NSNumber * TotalEmployees;
	NSString * WorkLocation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_V_OrgUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsLeaderActive;
@property (retain) NSString * Name;
@property (retain) NSNumber * OrgJobTitleId;
@property (retain) NSString * OrgJobTitleLevelCode;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitLeaderCode;
@property (retain) NSString * OrgUnitLeaderFirstName;
@property (retain) NSString * OrgUnitLeaderFullName;
@property (retain) NSNumber * OrgUnitLeaderId;
@property (retain) NSString * OrgUnitLeaderImageURL;
@property (retain) NSString * OrgUnitLeaderJobPositionName;
@property (retain) NSString * OrgUnitLeaderLastName;
@property (retain) NSString * ParentCode;
@property (retain) NSNumber * ParentId;
@property (retain) NSNumber * TotalEmployees;
@property (retain) NSString * WorkLocation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfV_OrgUnit : NSObject {
	
/* elements */
	NSMutableArray *V_OrgUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfV_OrgUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgUnit:(time_tns1_V_OrgUnit *)toAdd;
@property (readonly) NSMutableArray * V_OrgUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_SP_ESS_GET_EMP_HOLIDAY_Result : NSObject {
	
/* elements */
	NSNumber * NGAY_DA_NGHI;
	NSNumber * NGAY_NGHI_BU_CON_LAI;
	NSNumber * NGAY_NGHI_BU_DA_DUNG;
	NSNumber * NGAY_NGHI_BU_TRONG_NAM;
	NSNumber * NGAY_PHEP_CONG_TRU;
	NSNumber * NGAY_PHEP_CON_LAI;
	NSNumber * NGAY_PHEP_THAM_NIEN;
	NSNumber * NGAY_PHEP_THANG;
	NSNumber * TONG_NGAY_NGHI_BU;
	NSNumber * TONG_SO_NGAY_PHEP;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_SP_ESS_GET_EMP_HOLIDAY_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * NGAY_DA_NGHI;
@property (retain) NSNumber * NGAY_NGHI_BU_CON_LAI;
@property (retain) NSNumber * NGAY_NGHI_BU_DA_DUNG;
@property (retain) NSNumber * NGAY_NGHI_BU_TRONG_NAM;
@property (retain) NSNumber * NGAY_PHEP_CONG_TRU;
@property (retain) NSNumber * NGAY_PHEP_CON_LAI;
@property (retain) NSNumber * NGAY_PHEP_THAM_NIEN;
@property (retain) NSNumber * NGAY_PHEP_THANG;
@property (retain) NSNumber * TONG_NGAY_NGHI_BU;
@property (retain) NSNumber * TONG_SO_NGAY_PHEP;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_V_EMP_CONTRACT_WOKING_HOURS : NSObject {
	
/* elements */
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndTime;
	NSString * FullName;
	NSNumber * HoursOfOfficialWork;
	NSDate * StartTime;
	NSString * TimeWageTypeCode;
	NSString * TimeWageType_NameEN;
	NSString * TimeWageType_NameVN;
	NSString * TimeWorkingFormCode;
	NSString * TimeWorkingForm_NameEN;
	NSString * TimeWorkingForm_NameVN;
	NSNumber * WorkingDay;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_V_EMP_CONTRACT_WOKING_HOURS *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndTime;
@property (retain) NSString * FullName;
@property (retain) NSNumber * HoursOfOfficialWork;
@property (retain) NSDate * StartTime;
@property (retain) NSString * TimeWageTypeCode;
@property (retain) NSString * TimeWageType_NameEN;
@property (retain) NSString * TimeWageType_NameVN;
@property (retain) NSString * TimeWorkingFormCode;
@property (retain) NSString * TimeWorkingForm_NameEN;
@property (retain) NSString * TimeWorkingForm_NameVN;
@property (retain) NSNumber * WorkingDay;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfV_EMP_CONTRACT_WOKING_HOURS : NSObject {
	
/* elements */
	NSMutableArray *V_EMP_CONTRACT_WOKING_HOURS;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfV_EMP_CONTRACT_WOKING_HOURS *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_EMP_CONTRACT_WOKING_HOURS:(time_tns1_V_EMP_CONTRACT_WOKING_HOURS *)toAdd;
@property (readonly) NSMutableArray * V_EMP_CONTRACT_WOKING_HOURS;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TimeDailyWorkSheet : NSObject {
	
/* elements */
	NSNumber * BonusHoursShift1;
	NSNumber * BonusHoursShift2;
	NSNumber * BonusHoursShiftNight;
	NSNumber * CompanyId;
	NSNumber * CompensativeDayOff;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * Date;
	NSNumber * EarlyOutTime;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * EmployeeName;
	NSDate * EndHoursByReality;
	NSString * FLex1;
	NSString * FLex2;
	NSNumber * Flex10;
	NSDate * Flex11;
	NSDate * Flex12;
	NSDate * Flex13;
	USBoolean * Flex14;
	USBoolean * Flex15;
	USBoolean * Flex16;
	NSString * Flex3;
	NSString * Flex4;
	NSString * Flex5;
	NSNumber * Flex6;
	NSNumber * Flex7;
	NSNumber * Flex8;
	NSNumber * Flex9;
	NSNumber * HoursOfAllowanceWorkShiftNight;
	NSNumber * HoursOfDisabilityRegime;
	NSNumber * HoursOfOvertimeByReality;
	NSNumber * HoursOfOvertimeForSalaryCalculation;
	NSNumber * HoursOfOvertimeShiftNight;
	NSNumber * HoursOfPregnantRegime;
	NSNumber * HoursOfRasingChildrenRegime;
	NSNumber * HoursOfSubstitutedOvertime;
	NSNumber * HoursOfWorkByReality;
	NSDate * InLateTime;
	USBoolean * IsAbsenceForLeaving;
	USBoolean * IsAssignmentDay;
	USBoolean * IsClockWorkSheet;
	USBoolean * IsDeleted;
	USBoolean * IsLearningDay;
	USBoolean * IsNotWorkingWage;
	USBoolean * IsScanCard;
	USBoolean * IsWorkSheetChecked;
	NSNumber * LateInTime;
	NSNumber * LunchTime;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * Month;
	NSString * Note;
	NSNumber * NumberOfAbsenceDaysDeductedLeave;
	NSNumber * NumberOfAbsenceDaysDeductedSalary;
	NSNumber * NumberOfAbsenceDaysNotDeductSalaryLeave;
	NSNumber * NumberOfAllowanceWorkDayShift1;
	NSNumber * NumberOfAllowanceWorkDayShift2;
	NSNumber * NumberOfAllowanceWorkShiftNight;
	NSNumber * NumberOfDaysCompensativeOffDiligence;
	NSNumber * NumberOfDaysCompensativeOffUnpaid;
	NSString * OrgUnitCode;
	NSString * OrgUnitName;
	NSDate * OutEarlyTime;
	NSDate * StartHoursByReality;
	NSNumber * TimeDailyWorkSheetId;
	NSNumber * TimeWorkingFormId;
	NSNumber * WorkingDay;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TimeDailyWorkSheet *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * BonusHoursShift1;
@property (retain) NSNumber * BonusHoursShift2;
@property (retain) NSNumber * BonusHoursShiftNight;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CompensativeDayOff;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * Date;
@property (retain) NSNumber * EarlyOutTime;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * EmployeeName;
@property (retain) NSDate * EndHoursByReality;
@property (retain) NSString * FLex1;
@property (retain) NSString * FLex2;
@property (retain) NSNumber * Flex10;
@property (retain) NSDate * Flex11;
@property (retain) NSDate * Flex12;
@property (retain) NSDate * Flex13;
@property (retain) USBoolean * Flex14;
@property (retain) USBoolean * Flex15;
@property (retain) USBoolean * Flex16;
@property (retain) NSString * Flex3;
@property (retain) NSString * Flex4;
@property (retain) NSString * Flex5;
@property (retain) NSNumber * Flex6;
@property (retain) NSNumber * Flex7;
@property (retain) NSNumber * Flex8;
@property (retain) NSNumber * Flex9;
@property (retain) NSNumber * HoursOfAllowanceWorkShiftNight;
@property (retain) NSNumber * HoursOfDisabilityRegime;
@property (retain) NSNumber * HoursOfOvertimeByReality;
@property (retain) NSNumber * HoursOfOvertimeForSalaryCalculation;
@property (retain) NSNumber * HoursOfOvertimeShiftNight;
@property (retain) NSNumber * HoursOfPregnantRegime;
@property (retain) NSNumber * HoursOfRasingChildrenRegime;
@property (retain) NSNumber * HoursOfSubstitutedOvertime;
@property (retain) NSNumber * HoursOfWorkByReality;
@property (retain) NSDate * InLateTime;
@property (retain) USBoolean * IsAbsenceForLeaving;
@property (retain) USBoolean * IsAssignmentDay;
@property (retain) USBoolean * IsClockWorkSheet;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsLearningDay;
@property (retain) USBoolean * IsNotWorkingWage;
@property (retain) USBoolean * IsScanCard;
@property (retain) USBoolean * IsWorkSheetChecked;
@property (retain) NSNumber * LateInTime;
@property (retain) NSNumber * LunchTime;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Month;
@property (retain) NSString * Note;
@property (retain) NSNumber * NumberOfAbsenceDaysDeductedLeave;
@property (retain) NSNumber * NumberOfAbsenceDaysDeductedSalary;
@property (retain) NSNumber * NumberOfAbsenceDaysNotDeductSalaryLeave;
@property (retain) NSNumber * NumberOfAllowanceWorkDayShift1;
@property (retain) NSNumber * NumberOfAllowanceWorkDayShift2;
@property (retain) NSNumber * NumberOfAllowanceWorkShiftNight;
@property (retain) NSNumber * NumberOfDaysCompensativeOffDiligence;
@property (retain) NSNumber * NumberOfDaysCompensativeOffUnpaid;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSString * OrgUnitName;
@property (retain) NSDate * OutEarlyTime;
@property (retain) NSDate * StartHoursByReality;
@property (retain) NSNumber * TimeDailyWorkSheetId;
@property (retain) NSNumber * TimeWorkingFormId;
@property (retain) NSNumber * WorkingDay;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTimeDailyWorkSheet : NSObject {
	
/* elements */
	NSMutableArray *TimeDailyWorkSheet;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTimeDailyWorkSheet *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeDailyWorkSheet:(time_tns1_TimeDailyWorkSheet *)toAdd;
@property (readonly) NSMutableArray * TimeDailyWorkSheet;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TimeLeaveCalculationForm : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * FLex1;
	NSNumber * FLex10;
	NSDate * FLex11;
	NSDate * FLex12;
	NSDate * FLex13;
	USBoolean * FLex14;
	USBoolean * FLex15;
	USBoolean * FLex16;
	NSString * FLex2;
	NSString * FLex3;
	NSString * FLex4;
	NSString * FLex5;
	NSNumber * FLex6;
	NSNumber * FLex7;
	NSNumber * FLex8;
	NSNumber * FLex9;
	NSNumber * FirstFullLeaveDay;
	NSNumber * FirstHalfLeaveDay;
	NSString * ImplementationDate;
	USBoolean * IsDeleted;
	USBoolean * IsProbationAppliedLeave;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * MonthOfDeletingLeftLeave;
	NSNumber * StartDateOfLeave;
	NSNumber * TimeLeaveCalculationFormId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TimeLeaveCalculationForm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * FLex1;
@property (retain) NSNumber * FLex10;
@property (retain) NSDate * FLex11;
@property (retain) NSDate * FLex12;
@property (retain) NSDate * FLex13;
@property (retain) USBoolean * FLex14;
@property (retain) USBoolean * FLex15;
@property (retain) USBoolean * FLex16;
@property (retain) NSString * FLex2;
@property (retain) NSString * FLex3;
@property (retain) NSString * FLex4;
@property (retain) NSString * FLex5;
@property (retain) NSNumber * FLex6;
@property (retain) NSNumber * FLex7;
@property (retain) NSNumber * FLex8;
@property (retain) NSNumber * FLex9;
@property (retain) NSNumber * FirstFullLeaveDay;
@property (retain) NSNumber * FirstHalfLeaveDay;
@property (retain) NSString * ImplementationDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsProbationAppliedLeave;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * MonthOfDeletingLeftLeave;
@property (retain) NSNumber * StartDateOfLeave;
@property (retain) NSNumber * TimeLeaveCalculationFormId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTimeLeaveCalculationForm : NSObject {
	
/* elements */
	NSMutableArray *TimeLeaveCalculationForm;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTimeLeaveCalculationForm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeLeaveCalculationForm:(time_tns1_TimeLeaveCalculationForm *)toAdd;
@property (readonly) NSMutableArray * TimeLeaveCalculationForm;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TimeEmployeeNotCheckWorkingSheet : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * FLex1;
	NSNumber * FLex10;
	NSDate * FLex11;
	NSDate * FLex12;
	NSDate * FLex13;
	USBoolean * FLex14;
	USBoolean * FLex15;
	USBoolean * FLex16;
	NSString * FLex2;
	NSString * FLex3;
	NSString * FLex4;
	NSString * FLex5;
	NSNumber * FLex6;
	NSNumber * FLex7;
	NSNumber * FLex8;
	NSNumber * FLex9;
	USBoolean * IsCheckWorkingSheet;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * TimeEmployeeNotCheckWorkingSheetId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TimeEmployeeNotCheckWorkingSheet *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * FLex1;
@property (retain) NSNumber * FLex10;
@property (retain) NSDate * FLex11;
@property (retain) NSDate * FLex12;
@property (retain) NSDate * FLex13;
@property (retain) USBoolean * FLex14;
@property (retain) USBoolean * FLex15;
@property (retain) USBoolean * FLex16;
@property (retain) NSString * FLex2;
@property (retain) NSString * FLex3;
@property (retain) NSString * FLex4;
@property (retain) NSString * FLex5;
@property (retain) NSNumber * FLex6;
@property (retain) NSNumber * FLex7;
@property (retain) NSNumber * FLex8;
@property (retain) NSNumber * FLex9;
@property (retain) USBoolean * IsCheckWorkingSheet;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * TimeEmployeeNotCheckWorkingSheetId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTimeEmployeeNotCheckWorkingSheet : NSObject {
	
/* elements */
	NSMutableArray *TimeEmployeeNotCheckWorkingSheet;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTimeEmployeeNotCheckWorkingSheet *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeEmployeeNotCheckWorkingSheet:(time_tns1_TimeEmployeeNotCheckWorkingSheet *)toAdd;
@property (readonly) NSMutableArray * TimeEmployeeNotCheckWorkingSheet;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_TimeEmployeeWorkingDifferentTime : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * FLex1;
	NSNumber * FLex10;
	NSDate * FLex11;
	NSDate * FLex12;
	NSDate * FLex13;
	USBoolean * FLex14;
	USBoolean * FLex15;
	USBoolean * FLex16;
	NSString * FLex2;
	NSString * FLex3;
	NSString * FLex4;
	NSString * FLex5;
	NSNumber * FLex6;
	NSNumber * FLex7;
	NSNumber * FLex8;
	NSNumber * FLex9;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * TimeEmployeeWorkingDifferentTimeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_TimeEmployeeWorkingDifferentTime *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * FLex1;
@property (retain) NSNumber * FLex10;
@property (retain) NSDate * FLex11;
@property (retain) NSDate * FLex12;
@property (retain) NSDate * FLex13;
@property (retain) USBoolean * FLex14;
@property (retain) USBoolean * FLex15;
@property (retain) USBoolean * FLex16;
@property (retain) NSString * FLex2;
@property (retain) NSString * FLex3;
@property (retain) NSString * FLex4;
@property (retain) NSString * FLex5;
@property (retain) NSNumber * FLex6;
@property (retain) NSNumber * FLex7;
@property (retain) NSNumber * FLex8;
@property (retain) NSNumber * FLex9;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * TimeEmployeeWorkingDifferentTimeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface time_tns1_ArrayOfTimeEmployeeWorkingDifferentTime : NSObject {
	
/* elements */
	NSMutableArray *TimeEmployeeWorkingDifferentTime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (time_tns1_ArrayOfTimeEmployeeWorkingDifferentTime *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTimeEmployeeWorkingDifferentTime:(time_tns1_TimeEmployeeWorkingDifferentTime *)toAdd;
@property (readonly) NSMutableArray * TimeEmployeeWorkingDifferentTime;
/* attributes */
- (NSDictionary *)attributes;
@end
