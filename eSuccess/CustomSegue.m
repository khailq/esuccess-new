//
//  CustomSegue.m
//  eSuccess
//
//  Created by Scott on 5/2/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "CustomSegue.h"
#import "AppDelegate.h"

@implementation CustomSegue

-(void)perform
{
    UINavigationController *source = (UINavigationController *)self.sourceViewController;
    
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    NSMutableDictionary *dict = app.masterControllerDictionary;
    
    NSString *className = NSStringFromClass([self.destinationViewController class]);
    
    source.viewControllers = [NSArray array];
    if ([dict valueForKey:className] == nil)
    {
        [dict setValue:self.destinationViewController forKey:className];
        [source pushViewController:self.destinationViewController animated:YES];
    }
    else
    {
        [source pushViewController:[dict valueForKey:className] animated:YES];
    }
}


@end
