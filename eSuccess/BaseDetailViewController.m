//
//  BaseDetailViewController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 5/4/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailViewController.h"
#import "AppDelegate.h"
#import "CustomSplitViewController.h"


@interface BaseDetailViewController ()
{
    UIBarButtonItem *menuButton;
}



@end

@implementation BaseDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidDisappear:(BOOL)animated
{
    self.viewDidDisappear = YES;
    [super viewDidDisappear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(onMenuButtonClicked:)];
        self.navigationItem.leftBarButtonItem = menuButton;
    }
    else
        self.navigationItem.leftBarButtonItem = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationController.navigationBar.translucent = NO;
    
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsPortrait(orientation))
    {
        menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(onMenuButtonClicked:)];
        self.navigationItem.leftBarButtonItem = menuButton;
    }
}

-(void)onMenuButtonClicked:(id)sender
{
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    CustomSplitViewController *splitVc = (CustomSplitViewController*)app.window.rootViewController;
    [splitVc toggleMasterView:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation))
    {
        if (menuButton == nil)
        {
            menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(onMenuButtonClicked:)];
        }
        self.navigationItem.leftBarButtonItem = menuButton;
    }
    else
        self.navigationItem.leftBarButtonItem = nil;
}

@end
