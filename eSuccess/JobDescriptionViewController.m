////
////  JobInformationViewController.m
////  eSuccess
////
////  Created by admin on 5/11/13.
////  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
////
//
//#import "JobDescriptionViewController.h"
//#import "MBProgressHUD.h"
//#import "AppDelegate.h"
//#import "DayLeaveDetailCell.h"
//@interface JobDescriptionViewController ()
//
//@end
//
//@implementation JobDescriptionViewController
//@synthesize jobPostionArr;
//@synthesize jobNameArr;
//static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";
//
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}
//
//- (void)viewDidLoad
//{
//    [super viewDidLoad];
//    // Do any additional setup after loading the view.
//    [self getData];
//    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
//    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
//}
//
//- (void)didReceiveMemoryWarning
//{
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
//
//#pragma mark - Table view data source
////-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
////{
////    return 130;
////}
//
//-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    if ([jobNameArr count] > 0) {
//        return [jobNameArr objectAtIndex:section];
//    }
//    return nil;
//}
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    // Return the number of sections.
//    return [jobPostionArr count] > 0 ? jobPostionArr.count : 1;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    // Return the number of rows in the section.
//    //return [profileArray count];
//    if([jobPostionArr count] > 0){
//        return 4;
//    }
//    return 1;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (jobPostionArr.count == 0) {
//        return [self getDefaultEmptyCell];
//    }
//    
//    DayLeaveDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
//    if (cell == nil) {
//        cell = [[DayLeaveDetailCell alloc]init];
//    }
//    cell.lbLeft.font = [UIFont systemFontOfSize:14];
//    cell.lbLeft.backgroundColor = [UIColor clearColor];
//    cell.lbRight.font = [UIFont boldSystemFontOfSize:16];
//    cell.lbRight.backgroundColor = [UIColor clearColor];
//    int section = indexPath.section;
//    
//    tns2_V_EmpProfileJobPosition *job = [jobPostionArr objectAtIndex:section];
//    if (indexPath.row == 0) {
//        cell.lbLeft.text = @"Vị trí";
//        cell.lbRight.text = job.OrgJobPositionName;
//    }
//    if (indexPath.row == 1) {
//        cell.lbLeft.text = @"Công tác tại";
//        cell.lbRight.text = job.OrgUnitAddress;
//    }
//    if (indexPath.row == 2) {
//        cell.lbLeft.text = @"Đơn vị";
//        cell.lbRight.text = job.OrgUnitName;
//    }
//    if (indexPath.row == 3) {
//        cell.lbLeft.text = @"Địa điểm";
//        cell.lbRight.text = job.OrgUnitWorkLocation;
//    }
//    return cell;
//}
//
//#pragma get Job Positon
//-(void)getData
//{
//    jobPostionArr = [[NSMutableArray alloc]init];
//    jobNameArr = [[NSMutableArray alloc]init];
//    
//    [self getJobPostionById];
//}
//
//- (void)getJobPostionById
//{
//    BasicHttpBinding_IEmpProfileLayerServiceBinding * binding = [EmpProfileLayerService BasicHttpBinding_IEmpProfileLayerServiceBinding];
//    EmpProfileLayerService_GetViewJobPositionDescriptionById *request = [[EmpProfileLayerService_GetViewJobPositionDescriptionById alloc] init];
//    AppDelegate *app = [UIApplication sharedApplication].delegate;
//    if (app.otherEmpId != nil) {
//        request.EmployeeId = app.otherEmpId;
//    } else{
//        request.employeeId = app.sysUser.EmployeeId;
//    }
//    [binding GetViewJobPositionDescriptionByIdAsyncUsingParameters:request delegate:self];
//}
//
//- (void)operation:(BasicHttpBinding_IEmpProfileLayerServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)response
//{
//    if ([response.bodyParts count] == 0) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi kết nối" message:@"Không thể kết nối đến server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [alert show];
//    }
//    
//    for (id mine in response.bodyParts) {
//        if ([mine isKindOfClass:[EmpProfileLayerService_GetViewJobPositionDescriptionByIdResponse class]]) {
//            tns2_ArrayOfV_EmpProfileJobPosition *result = [mine GetViewJobPositionConcurrentByIdResult];
//            jobPostionArr = result.V_EmpProfileJobPosition;
//            for (tns2_V_EmpProfileJobPosition *job in jobPostionArr)  {
//                [jobNameArr addObject:job.OrgJobName];
//            }
//        }
//        
//        //        if ([mine isKindOfClass:[SOAPFault class]])
//        //        {
//        //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Server error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        //            [alert show];
//        //        }
//    }
//    [self.tableView reloadData];
//    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//    
//}
//
//@end
