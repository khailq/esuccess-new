//
//  ContractInforViewController.h
//  eSuccess
//
//  Created by admin on 5/20/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"
#import "EmpProfileLayerServiceSvc.h"
@interface ContractInforViewController : BaseDetailTableViewController<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>
@property(nonatomic, strong) NSMutableArray *contractArr;
@property(nonatomic, strong) NSMutableArray *contractTypeArr;
@end
