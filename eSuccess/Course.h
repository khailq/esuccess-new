//
//  Course.h
//  eSuccess
//
//  Created by HPTVIETNAM on 6/7/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Course : NSObject

@property (strong, nonatomic) NSNumber *employeeId;
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSNumber *warningReceived;
@property (strong, nonatomic) NSNumber *courseId;
@property (strong, nonatomic) NSNumber *participantStatus;
@property (strong, nonatomic) NSNumber *progressStatus;
@property (strong, nonatomic) NSString *courseName;
@property (strong, nonatomic) NSString *courseDescription;
@property (strong, nonatomic) NSString *keywords;
@property (strong, nonatomic) NSDate *startDate;
@property (strong, nonatomic) NSDate *endDate;
@property (strong, nonatomic) NSNumber *isRequired;
@property (strong, nonatomic) NSNumber *cost;
@property (strong, nonatomic) NSNumber *courseStatusId;
@property (strong, nonatomic) NSString *courseStatus;
@property (strong, nonatomic) NSNumber *courseTypeId;
@property (strong, nonatomic) NSString *courseTypeName;
@property (strong, nonatomic) NSNumber *topicGroupId;
@property (strong, nonatomic) NSString *topicGroupName;
@property (strong, nonatomic) NSString *topicGroupDescription;

@property (strong, nonatomic, readonly) NSString* participantStatusTitle;

+(Course*)parseCourseFromJSON:(NSDictionary*)jsonDict;

@end
