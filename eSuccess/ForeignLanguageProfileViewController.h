//
//  ForeignLanguageProfileViewController.h
//  eSuccess
//
//  Created by HPTVIETNAM on 6/26/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"
#import "EmpProfileLayerServiceSvc.h"
@interface ForeignLanguageProfileViewController : BaseDetailTableViewController<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>
@property(nonatomic, strong) NSMutableArray * foreginLanguageArr;
@property(nonatomic, strong) NSMutableArray * degreeNameArr;
@end
