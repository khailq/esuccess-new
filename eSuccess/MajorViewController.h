//
//  MajorViewController.h
//  eSuccess
//
//  Created by admin on 5/7/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailViewController.h"
#import "BaseDetailTableViewController.h"
@interface MajorViewController : BaseDetailTableViewController
{
    IBOutlet UILabel *lblEducationName;
    IBOutlet UILabel *lblMajor;
}
@property(nonatomic, retain) IBOutlet UILabel *lblEducationName;
@property(nonatomic, retain) IBOutlet UILabel *lblMajor; 
@end
