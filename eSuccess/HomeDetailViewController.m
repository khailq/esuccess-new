//
//  HomeDetailViewController.m
//  Presidents
//
//  Created by HPTVIETNAM on 4/13/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//
#import "HomeDetailViewController.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "tns2.h"
#import "UIImageView+AFNetworking.h"
#import "Employee Layer Profile Service/EmpProfileLayerServiceSvc.h"
#import "MBProgressHUD.h"

@interface HomeDetailViewController ()<BasicHttpBinding_IEmpProfileLayerServiceBindingResponseDelegate>
{
    NSDateFormatter *dateFormatter;
    int jobCount;
    BOOL semaphore;
    int finishedCount;
}

@property (weak, nonatomic) IBOutlet UILabel *lb_fullName;
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *lb_jobPosition;
@property (weak, nonatomic) IBOutlet UILabel *lb_orgUnitName;
@property (weak, nonatomic) IBOutlet UILabel *lb_phoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *lb_email;
@property (weak, nonatomic) IBOutlet UILabel *lb_empCode;
@property (weak, nonatomic) IBOutlet UILabel *lb_birthday;
@property (weak, nonatomic) IBOutlet UILabel *lb_gender;
@property (weak, nonatomic) IBOutlet UILabel *lb_birthPlace;
@property (weak, nonatomic) IBOutlet UILabel *lb_hometown;
@property (weak, nonatomic) IBOutlet UILabel *lb_address;
@property (weak, nonatomic) IBOutlet UILabel *lb_permanentAddress;
@property (weak, nonatomic) IBOutlet UILabel *lb_ethnicity;
@property (weak, nonatomic) IBOutlet UILabel *lb_nationality;
@property (weak, nonatomic) IBOutlet UILabel *lb_region;
@property (weak, nonatomic) IBOutlet UILabel *lb_identityNumber;
@property (weak, nonatomic) IBOutlet UILabel *lb_identityIssuedDate;
@property (weak, nonatomic) IBOutlet UILabel *lb_identityIssuedPlace;
@property (weak, nonatomic) IBOutlet UILabel *lb_passportNo;
@property (weak, nonatomic) IBOutlet UILabel *lb_passportIssuedPlace;
@property (weak, nonatomic) IBOutlet UILabel *lb_passportIssuedDate;
@property (weak, nonatomic) IBOutlet UILabel *lb_passportExpiredDate;
@property (weak, nonatomic) IBOutlet UILabel *lb_PITCode;
@property (weak, nonatomic) IBOutlet UILabel *lb_PITIssuedBy;
@property (weak, nonatomic) IBOutlet UILabel *lb_bankAccount;
@property (weak, nonatomic) IBOutlet UILabel *lb_bankName;
@property (weak, nonatomic) IBOutlet UILabel *lb_branch;

@end

@implementation HomeDetailViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    app.otherEmpId = nil;
    app.otherEmpName = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    jobCount = 0;
    
    self.title = @"Thông tin chung";
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onEmployeeIdChanged:) name:BeginViewProfileNotification object:nil];
    
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    if (app.otherEmpId != nil)
    {
        [self getDataForEmpId:app.otherEmpId];
        self.navigationItem.leftBarButtonItem = nil;
    }
    else
    {
        [self getDataForEmpId:app.sysUser.EmployeeId];
    }
}

-(void)onEmployeeIdChanged:(NSNotification*)notification
{
    finishedCount = 0;
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    if (app.otherEmpId != nil)
    {
        [self getDataForEmpId:app.otherEmpId];
    }
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:BeginViewProfileNotification object:nil];
}


- (void)getDataForEmpId:(NSNumber *)empId
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [self getEmpBasicProfile:empId];
    jobCount += 1;

    [self getEmpProfileJobPosition:empId];
    jobCount += 1;
    
    [self getEmpProfileContact:empId];
    jobCount += 1;
}

#pragma mark- Background handling

-(void)doWorkInBackground:(void (^)(void))backgroundWork
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), backgroundWork);
}

-(void)handleResponse:(id)response action:(void (^)(id))action
{
    dispatch_async(dispatch_get_main_queue(), ^{
        finishedCount++;
        if ([[response bodyParts] count] == 0)
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Không thể lấy được dữ liệu, vui lòng kiểm tra lại kết nối mạng" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alertView show];
            return;
        }
        for (id mine in [response bodyParts]) {
            if ([mine isKindOfClass:[SOAPFault class]]){
                NSString *faultMessage = [mine message];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:faultMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
                return;
            }
            
            action(mine);
            jobCount -= 1;
        }
        
        if ((finishedCount == 3 || jobCount == -10) && self != nil)
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    });
}


#pragma mark - GetEmpBasicProfileById

-(void)getEmpBasicProfile:(NSNumber *)empId
{
    semaphore = YES;
    BasicHttpBinding_IEmpProfileLayerServiceBinding *binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
    EmpProfileLayerServiceSvc_GetViewEmpBasicProfileById *request = [[EmpProfileLayerServiceSvc_GetViewEmpBasicProfileById alloc] init];
    request.EmployeeId = empId;
    
    [self doWorkInBackground:^{
        BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *response = [binding GetViewEmpBasicProfileByIdUsingParameters:request];
        [self handleResponse:response action:^(id result) {
            empl_tns1_V_EmpBasicProfile *info = [result GetViewEmpBasicProfileByIdResult];
            AppDelegate *app = [UIApplication sharedApplication].delegate;
            app.basicEmpProfile = info;
            [self getEmpBasicProfileCompleted:info];
        }];
    }];
    [binding GetViewEmpBasicProfileByIdAsyncUsingParameters:request delegate:self];
    semaphore = NO;
}

-(void)getEmpBasicProfileCompleted:(empl_tns1_V_EmpBasicProfile *)profile
{
    if (self == nil || self.viewDidDisappear) {
        return;
    }
    semaphore = YES;
    self.lb_bankAccount.text = profile.BankAccountNo;
    self.lb_branch.text = profile.BankBranch;
    self.lb_bankName.text = profile.BankName;
    self.lb_birthPlace.text = profile.BirthPlace;
    self.lb_fullName.text = profile.FullName;
    self.lb_gender.text = profile.Gender.boolValue ? @"Nam" : @"Nữ";
    self.lb_hometown.text = profile.HomeTown;
    self.lb_identityNumber.text = profile.IdentityCardNo;
    self.lb_identityIssuedPlace.text = profile.IdentityCardPlaceOfIssue;
    self.lb_identityIssuedDate.text = [dateFormatter stringFromDate:profile.IdentityCardDateOfIssue];
    
    if (!profile.IsOnlyYearOfBirthday.boolValue)
        self.lb_birthday.text = [dateFormatter stringFromDate:profile.BirthDay];
    else
    {
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:[NSDate date]];
        self.lb_birthday.text = [NSString stringWithFormat:@"%d", components.year];
    }
    
    if (profile.ImageUrl != nil){
        NSString *baseAvatarURL = AvatarUrl;
        baseAvatarURL = [baseAvatarURL stringByAppendingString:profile.ImageUrl];
        [self.avatar setImageWithURL:[NSURL URLWithString:baseAvatarURL]];
    }
    else
        [self.avatar setImage:nil];
    
    self.lb_nationality.text = profile.Nationality;
    self.lb_PITCode.text = profile.PITCode;
    self.lb_PITIssuedBy.text = profile.PITIssuedBy;
    self.lb_passportNo.text = profile.PassportNo;
    self.lb_passportIssuedDate.text = [dateFormatter stringFromDate:profile.PassportDateOfIssue];
    self.lb_passportIssuedPlace.text = profile.PassportPlaceOfIssue;
    self.lb_passportExpiredDate.text = [dateFormatter stringFromDate:profile.PassportDateOfExpire];
    self.lb_empCode.text = profile.EmployeeCode;
    self.lb_region.text = profile.Religion;
    self.lb_ethnicity.text = profile.Ethnicity;
    semaphore = NO;
}

#pragma mark - GetViewEmpProfileJobPosition


-(void)getEmpProfileJobPosition:(NSNumber *)empId
{
    semaphore = YES;
    
    BasicHttpBinding_IEmpProfileLayerServiceBinding *binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
    EmpProfileLayerServiceSvc_GetViewEmpProfileJobPositions *request = [[EmpProfileLayerServiceSvc_GetViewEmpProfileJobPositions alloc] init];
    request.EmployeeId = empId;
    [self doWorkInBackground:^{
        BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *response = [binding GetViewEmpProfileJobPositionsUsingParameters:request];
        [self handleResponse:response action:^(id result) {
            EmpProfileLayerServiceSvc_GetViewEmpProfileJobPositionsResponse *jobResponse = (EmpProfileLayerServiceSvc_GetViewEmpProfileJobPositionsResponse*)result;
            
            empl_tns1_ArrayOfV_EmpProfileJobPosition *array = [jobResponse GetViewEmpProfileJobPositionsResult];
            if (array.V_EmpProfileJobPosition.count > 0)
            {
                empl_tns1_V_EmpProfileJobPosition *job = array.V_EmpProfileJobPosition[0];
                [self getEmpProfileJobPositionCompleted:job];
            }
            
        }];
    }];
    
    
    semaphore = NO;
}

-(void)getEmpProfileJobPositionCompleted:(empl_tns1_V_EmpProfileJobPosition*)job
{
    if (self == nil || self.viewDidDisappear)
        return;
    semaphore = YES;
    self.lb_orgUnitName.text = job.OrgUnitName;
    self.lb_jobPosition.text = job.OrgJobPositionName;
    semaphore = NO;
}

#pragma mark - GetViewEmpProfileContact
-(void)getEmpProfileContact:(NSNumber *)empId
{
   
    BasicHttpBinding_IEmpProfileLayerServiceBinding *binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
    EmpProfileLayerServiceSvc_GetViewEmpProfileContactsByEmployeeId *request = [EmpProfileLayerServiceSvc_GetViewEmpProfileContactsByEmployeeId new];
    
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    request.employeeId = app.sysUser.EmployeeId;
    
        [self doWorkInBackground:^{
        BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *response = [binding GetViewEmpProfileContactsByEmployeeIdUsingParameters:request];
        [self handleResponse:response action:^(id result) {
            EmpProfileLayerServiceSvc_GetViewEmpProfileContactsByEmployeeIdResponse *contactResponse = (EmpProfileLayerServiceSvc_GetViewEmpProfileContactsByEmployeeIdResponse*)result;
            empl_tns1_ArrayOfV_EmpProfileContact *empContacts = [contactResponse GetViewEmpProfileContactsByEmployeeIdResult];
            for(empl_tns1_EmpProfileContact *empContact in empContacts.V_EmpProfileContact)
            {
                if (empContact.IsCurrent.boolValue)
                {
                    [self updateView:empContact];
                    break;
                }
            }
        }];
        
    }];
    [binding GetViewEmpProfileContactsByEmployeeIdAsyncUsingParameters:request delegate:self];
}

-(void)updateView:(empl_tns1_EmpProfileContact *)empContact
{
    if (self == nil || self.viewDidDisappear)
        return;
    self.lb_address.text = empContact.ContactAddress;
    self.lb_email.text = empContact.Email;
    self.lb_phoneNumber.text = empContact.MobileNumber;
    self.lb_permanentAddress.text = empContact.PermanentAddress;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)operation:(id)operation completedWithResponse:(id)response
{
    if ([response isKindOfClass:[BasicHttpBinding_IEmpProfileLayerServiceBindingResponse class]])
    {
        BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *profileLayerResponse = (BasicHttpBinding_IEmpProfileLayerServiceBindingResponse*)response;
        for (id mine in profileLayerResponse.bodyParts) {
            
            if ([mine isKindOfClass:[EmpProfileLayerServiceSvc_GetViewEmpBasicProfileByIdResponse class]])
            {
                empl_tns1_V_EmpBasicProfile *info = [mine GetViewEmpBasicProfileByIdResult];
                AppDelegate *app = [UIApplication sharedApplication].delegate;
                app.basicEmpProfile = info;
                [self getEmpBasicProfileCompleted:info];
                jobCount -= 1;
                finishedCount += 1;
            }
            
            if ([mine isKindOfClass:[EmpProfileLayerServiceSvc_GetViewEmpProfileJobPositionsResponse class]])
            {
                
            }
            
            
            if ([mine isKindOfClass:[SOAPFault class]])
                finishedCount++;
        }
    }
    
    if ((finishedCount == 3 || jobCount == -10) && self != nil)
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

#pragma mark - Table view delegate

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    // Navigation logic may go here. Create and push another view controller.
//    /*
//     DetailViewController *detailViewController = [[DetailViewController alloc] initWithNibName:@"Nib name" bundle:nil];
//     // ...
//     // Pass the selected object to the new view controller.
//     [self.navigationController pushViewController:detailViewController animated:YES];
//     */
//}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation) && app.otherEmpId != nil)
        self.navigationItem.leftBarButtonItem = nil;
}
@end
