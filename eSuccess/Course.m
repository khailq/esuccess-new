//
//  Course.m
//  eSuccess
//
//  Created by HPTVIETNAM on 6/7/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "Course.h"

@implementation Course

-(NSString*)participantStatusTitle
{
    if (_participantStatus == nil)
        return @"";
    
    switch (_participantStatus.intValue) {
        case 1:
            return @"Đã đăng ký";
        case 2:
            return @"Chưa đăng ký";
        case 3:
            return @"Đang diễn ra";
            
        default:
            return @"Đã hoàn thành";
    }
}

+(Course*)parseCourseFromJSON:(NSDictionary *)jsonDict
{
    if (jsonDict == nil)
        return nil;
    
    Course *course = [[Course alloc] init];
    course.employeeId = jsonDict[@"EmployeeId"];
    course.firstName = jsonDict[@"FirstName"];
    course.lastName = jsonDict[@"LastName"];
    course.warningReceived = jsonDict[@"NumberOfWarning"];
    course.courseId = jsonDict[@"LMSCourseId"];
    course.participantStatus = jsonDict[@"ParticipantStatus"];
    course.progressStatus = jsonDict[@"ProgressStatus"];
    course.courseName = jsonDict[@"CourseName"];
    course.courseDescription = jsonDict[@"CourseDescription"];
    course.keywords = jsonDict[@"Keyword"];
    course.startDate = jsonDict[@"StartDate"];
    course.endDate = jsonDict[@"EndDate"];
    course.courseTypeId = jsonDict[@"CourseTypeId"];
    course.courseTypeName = jsonDict[@"LMSCourseTypeName"];
    course.cost = jsonDict[@"Cost"];
    course.courseStatusId = jsonDict[@"CourseStatusId"];
    course.courseStatus = jsonDict[@"CourseStatus"];
    course.topicGroupId = jsonDict[@"TopicGroupId"];
    course.topicGroupName = jsonDict[@"TopicGroupName"];
    course.topicGroupDescription = jsonDict[@"TopicGroupDescription"];
    course.isRequired = jsonDict[@"IsRequired"];
    
    return course;
}
@end
