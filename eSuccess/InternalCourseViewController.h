//
//  InternalCourseViewController.h
//  eSuccess
//
//  Created by admin on 5/10/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"

@interface InternalCourseViewController : BaseDetailTableViewController
@property(nonatomic, strong)NSMutableArray *courseArray;
@property(nonatomic, strong)NSMutableArray *courseInforArray;
@end
