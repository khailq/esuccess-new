//
//  QualificationProfileViewController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 6/26/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "QualificationProfileViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "DayLeaveDetailCell.h"
@interface QualificationProfileViewController ()

@end

@implementation QualificationProfileViewController
@synthesize qualificationArr;
@synthesize qualiNameArr;
static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Trình độ bằng cấp";
    [self getData];
    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 130;
//}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([qualiNameArr count] > 0) {
        return [qualiNameArr objectAtIndex:section];
    }
    return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [qualificationArr count] > 0 ? qualificationArr.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [qualificationArr count] > 0 ? 8 : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (qualificationArr.count == 0)
        return [self getDefaultEmptyCell];
    
    DayLeaveDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
    if (cell == nil) {
        cell = [[DayLeaveDetailCell alloc]init];
    }
    cell.lbLeft.font = [UIFont systemFontOfSize:14];
    cell.lbLeft.backgroundColor = [UIColor clearColor];
    cell.lbRight.font = [UIFont boldSystemFontOfSize:16];
    cell.lbRight.backgroundColor = [UIColor clearColor];
    
    int section = indexPath.section;
    empl_tns1_V_EmpProfileQualification *qualification = [qualificationArr objectAtIndex:section];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    if (indexPath.row == 0) {
        cell.lbLeft.text = @"Chuyên ngành";
        cell.lbRight.text = qualification.CLogMajorName;
    }
    if (indexPath.row == 1) {
        cell.lbLeft.text = @"Xếp loại";
        cell.lbRight.text = qualification.OrgDegreeRankName;
    }
    if (indexPath.row == 2) {
        cell.lbLeft.text = @"Ngày cấp";
        cell.lbRight.text = [formatter stringFromDate:qualification.DateIssue];
    }
    if (indexPath.row == 3) {
        cell.lbLeft.text = @"Nơi cấp";
        cell.lbRight.text = qualification.PlaceIssue;
    }
    if (indexPath.row == 4) {
        cell.lbLeft.text = @"Ngày hết hạn";
        cell.lbRight.text = [formatter stringFromDate:qualification.DateExpire];
    }
    if (indexPath.row == 5) {
        cell.lbLeft.text = @"Hợp lệ";
        if ([qualification.IsValId boolValue] == true) {
            cell.lbRight.text = @"Có";
        } else
            cell.lbRight.text = @"Không";
    }
    if (indexPath.row == 6) {
        cell.lbLeft.text = @"Mô tả";
        cell.lbRight.text = qualification.OrgQualificationDescription;
    }
    if (indexPath.row == 7) {
        cell.lbLeft.text = @"Ghi chú";
        cell.lbRight.text = qualification.Other;
    }
    return cell;
}

#pragma get Data Emp Profile Qualification
-(void)getData
{
    qualificationArr = [[NSMutableArray alloc]init];
    qualiNameArr = [[NSMutableArray alloc]init];
    
    [self getEmpProfileQualificationById];
}
-(void)getEmpProfileQualificationById
{
    BasicHttpBinding_IEmpProfileLayerServiceBinding * binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
    EmpProfileLayerServiceSvc_GetViewEmpProfileQualificationById *request = [[EmpProfileLayerServiceSvc_GetViewEmpProfileQualificationById alloc] init];
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    if (app.otherEmpId != nil) {
        request.EmployeeId = app.otherEmpId;
    } else{
        request.employeeId = app.sysUser.EmployeeId ;
    }
    [binding GetViewEmpProfileQualificationByIdAsyncUsingParameters:request delegate:self];
}


- (void)operation:(BasicHttpBinding_IEmpProfileLayerServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)response
{
    if ([response.bodyParts count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi kết nối" message:@"Không thể kết nối đến server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    for (id mine in response.bodyParts) {
        if ([mine isKindOfClass:[EmpProfileLayerServiceSvc_GetViewEmpProfileQualificationByIdResponse class]]) {
            empl_tns1_ArrayOfV_EmpProfileQualification *result = [mine GetViewEmpProfileQualificationByIdResult];
            qualificationArr = result.V_EmpProfileQualification;
            for (empl_tns1_V_EmpProfileQualification *qualification in qualificationArr) {
                [qualiNameArr addObject:qualification.OrgQualificationName];
            }
            
            if (!self.viewDidDisappear)
                [self.tableView reloadData];
        }
        
        //        if ([mine isKindOfClass:[SOAPFault class]])
        //        {
        //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Server error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //            [alert show];
        //        }
    }
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

@end
