//
//  PerformanceAppraisalViewController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 6/21/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "PerformanceAppraisalViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "DayLeaveDetailCell.h"
@interface PerformanceAppraisalViewController ()

@end

@implementation PerformanceAppraisalViewController
@synthesize performAppraisalArr;
@synthesize timeArr;
static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Xếp loại hằng năm";
    [self getData];
    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 130;
//}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([timeArr count] > 0) {
        return [timeArr objectAtIndex:section];
    }
    return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [performAppraisalArr count] > 0 ? performAppraisalArr.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [performAppraisalArr count] > 0 ? 7 : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (performAppraisalArr.count == 0)
        return [self getDefaultEmptyCell];
    
    DayLeaveDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
    if (cell == nil) {
        cell = [[DayLeaveDetailCell alloc]init];
    }
    cell.lbLeft.font = [UIFont systemFontOfSize:14];
    cell.lbLeft.backgroundColor = [UIColor clearColor];
    cell.lbRight.font = [UIFont boldSystemFontOfSize:16];
    cell.lbRight.backgroundColor = [UIColor clearColor];
    
    int section = indexPath.section;
    empl_tns1_EmpPerformanceAppraisal *appraisal = [performAppraisalArr objectAtIndex:section];
    if (indexPath.row == 0) {
        cell.lbLeft.text = @"Kỳ đánh giá";
        if ([appraisal.IsHalfYear boolValue] == true && [appraisal.IsEndYear boolValue] == false ) {
            cell.lbRight.text = @"Giữa năm";
        } else
            cell.lbRight.text = @"Cuối năm";
    }
    if (indexPath.row == 1) {
        cell.lbLeft.text = @"Tự đánh giá";
        cell.lbRight.text = @"";
    }
    if (indexPath.row == 2) {
        cell.lbLeft.text = @"Điểm tự đánh giá";
        cell.lbRight.text = [NSString stringWithFormat:@"%d",[appraisal.SelfScore integerValue] ];
    }
    if (indexPath.row == 3) {
        cell.lbLeft.text = @"Ghi chú";
        cell.lbRight.text = appraisal.SelfComment;
    }
    if (indexPath.row == 4) {
        cell.lbLeft.text = @"Lãnh đạo đánh giá";
        cell.lbRight.text = @"";
    }
    if (indexPath.row == 5) {
        cell.lbLeft.text = @"Điểm";
        cell.lbRight.text = [NSString stringWithFormat:@"%d",[appraisal.Score integerValue] ];;
    }
    if (indexPath.row == 6) {
        cell.lbLeft.text = @"Nhận xét";
        cell.lbRight.text = appraisal.Comment;
    }
    return cell;
}


#pragma get Emp Performance Appraisal
- (void)getData
{
    performAppraisalArr = [[NSMutableArray alloc]init];
    timeArr = [[NSMutableArray alloc]init];
    
//    [self getEmpPerformanceAppraisalById];
}

//- (void)getEmpPerformanceAppraisalById
//{
//    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    hud.labelText = @"Đang cập nhật";
//    
//    BasicHttpBinding_IEmpProfileLayerServiceBinding *binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
//    EmpProfileLayerServiceSvc_GetViewEmpPerformanceAppraisalById *request = [[EmpProfileLayerServiceSvc_GetViewEmpPerformanceAppraisalById alloc]init];
//    AppDelegate *app = [UIApplication sharedApplication].delegate;
//    if (app.otherEmpId != nil)
//        request.employeeId = app.otherEmpId;
//    else
//        request.employeeId = app.sysUser.EmployeeId ;
//    //request.employeeId = [NSNumber numberWithInt:2];
//    [binding GetViewEmpPerformanceAppraisalByIdAsyncUsingParameters:request delegate:self];
//}

//- (void)operation:(BasicHttpBinding_IEmpProfileLayerServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)response
//{
//    if ([response.bodyParts count] == 0) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi kết nối" message:@"Không thể kết nối đến server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [alert show];
//    }
//    
//    for (id mine in response.bodyParts) {
//        if ([mine isKindOfClass:[EmpProfileLayerService_GetViewEmpPerformanceAppraisalByIdResponse class]]) {
//            empl_tns1_ArrayOfEmpPerformanceAppraisal *result = [mine GetViewEmpPerformanceAppraisalByIdResult];
//            performAppraisalArr = result.EmpPerformanceAppraisal;
//            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
//            [formatter setDateFormat:@"dd/MM/yyyy"];
//            for (empl_tns1_EmpPerformanceAppraisal *appraisal in performAppraisalArr) {
//                if (appraisal.AppraisalDate == nil) {
//                    [timeArr addObject:@""];
//                } else
//                    [timeArr addObject:[NSString stringWithFormat:@"%@",[formatter stringFromDate:appraisal.AppraisalDate]]];
//            }
//            
//            if (!self.viewDidDisappear)
//                [self.tableView reloadData];
//        }
//        
//        //        if ([mine isKindOfClass:[SOAPFault class]])
//        //        {
//        //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Server error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        //            [alert show];
//        //        }
//    }
//    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//}

@end
