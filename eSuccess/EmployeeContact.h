//
//  EmployeeContact.h
//  eSuccess
//
//  Created by HPTVIETNAM on 8/2/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EmployeeContact : NSObject
@property (strong, nonatomic) NSNumber * companyId;
@property (strong, nonatomic) NSNumber * contactAddressId;
@property (strong, nonatomic) NSNumber * emailProfileContactId;
@property (strong, nonatomic) NSNumber * employeeId;
@property (strong, nonatomic) NSNumber * permanentAddressId;
@property (strong, nonatomic) NSNumber * temporaryAddressId;

@property (strong, nonatomic) NSString * contactAddress;
@property (strong, nonatomic) NSString * email;
@property (strong, nonatomic) NSString * fax;
@property (strong, nonatomic) NSString * homephone;
@property (strong, nonatomic) NSString * mobilephone;
@property (strong, nonatomic) NSString * officephone;
@property (strong, nonatomic) NSString * permanentAddress;
@property (strong, nonatomic) NSString * temporaryAddress;

@property (strong, nonatomic) NSDate * createdDate;
@property (strong, nonatomic) NSDate * modifiedDate;
@property (strong, nonatomic) NSNumber * isCurrent;
@property (strong, nonatomic) NSNumber * isDelete;

+(EmployeeContact*)parseEmployeeContactByDict:(NSDictionary*)dict;

@end
