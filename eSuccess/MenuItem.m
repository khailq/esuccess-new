//
//  MenuItem.m
//  eSuccess
//
//  Created by HPTVIETNAM on 6/15/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "MenuItem.h"

@implementation MenuItem

+(MenuItem *)parseMenuItem:(NSDictionary *)dict
{
    MenuItem *menu = [[MenuItem alloc] init];
    menu.order = dict[@"order"];
    menu.title = dict[@"title"];
    menu.items = dict[@"items"];
    
    return menu;
}

- (NSComparisonResult)compare:(MenuItem *)otherObject {
    return [self.order compare:otherObject.order];
}
@end
