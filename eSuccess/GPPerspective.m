//
//  GPPerspective.m
//  eSuccess
//
//  Created by admin on 6/6/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "GPPerspective.h"

@implementation GPPerspective
+(GPPerspective *)gpPerspectiveWithId:(NSInteger)ID name:(NSString *)name coYearArr:(NSMutableArray *)gpCoYearArr
{
    GPPerspective *perSpec = [[GPPerspective alloc]init];
    perSpec.gpPerspecId = ID;
    perSpec.name = name;
    perSpec.coYearArr = gpCoYearArr;
    
    return perSpec;
}
@end
