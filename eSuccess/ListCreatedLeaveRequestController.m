//
//  ListCreatedLeaveRequestController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 5/4/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "ListCreatedLeaveRequestController.h"
#import "LeaveRequestCell.h"
#import "MBProgressHUD.h"
#import "LeaverRequestViewController.h"
#import "ViewLeaveRequestController.h"

#define ItemPerPage 20
#define PendingString @"Đang chờ duyệt"
#define Approvedstring @"Đã chấp nhận"
#define DeniedString @"Không chấp nhận"

static NSString *toLeaveFormSegueId = @"ToLeaveRequestForm";
//static NSString *leaveRequestIdentifier = @"LeaveRequestCell";

@interface ListCreatedLeaveRequestController ()
@property (strong, nonatomic) UIPopoverController *popover;
@end

@implementation ListCreatedLeaveRequestController
{
    NSArray *leaveRequests;
    NSMutableArray *acceptedRequests;
    NSMutableArray *deniedRequests;
    NSMutableArray *pendingRequests;
    
    int currentPage;
    UIBarButtonItem *nextPage, *previousPage;
    
    NSMutableDictionary *sectionNames;
    
    NSDateFormatter *dateFormatter;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    currentPage = 1;
    sectionNames = [NSMutableDictionary dictionary];
    
    nextPage = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFastForward target:self action:@selector(onNextPageClicked:)];
    previousPage = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRewind target:self action:@selector(onPreviousPageClicked:)];
    
    if (currentPage <= 1)
        previousPage.enabled = NO;
    
    UIBarButtonItem *addItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addRequest:)];
    
    self.navigationItem.rightBarButtonItems = @[addItem, nextPage, previousPage];
    
   // [self getLeaveRequestsByPage:currentPage];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}


-(void)onNextPageClicked:(id)sender
{
    currentPage = currentPage + 1;
    previousPage.enabled = YES;
    
   // [self getLeaveRequestsByPage:currentPage];
}

-(void)onPreviousPageClicked:(id)sender
{
    currentPage -= 1;
    if (currentPage <= 1)
        previousPage.enabled = NO;
    nextPage.enabled = YES;
   // [self getLeaveRequestsByPage:currentPage];
}

-(void)categoryRequestFromAllRequests:(NSArray*)allRequests
{
    acceptedRequests = [NSMutableArray array];
    deniedRequests = [NSMutableArray array];
    pendingRequests = [NSMutableArray array];
    
    for (tns1_SelfLeaveRequest *request in allRequests) {
        switch ([request.ApprovalStatus intValue]) {
            case 1:
                [pendingRequests addObject:request];
                break;
            case 2:
                [acceptedRequests addObject:request];
                break;
            default:
                [deniedRequests addObject:request];
                break;
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//#pragma mark - Leave request service
//
//-(void)getLeaveRequestsByPage:(int)pageNumber
//{
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    
//    if (pageNumber <= 0)
//        pageNumber = 1;
//    
//    AppDelegate *app = [UIApplication sharedApplication].delegate;
//    
//    BasicHttpBinding_ILeaveRequestServiceBinding *binding = [LeaveRequestServiceSvc BasicHttpBinding_ILeaveRequestServiceBinding];
//    
//    LeaveRequestServiceSvc_GetLeaveRequests *request = [[LeaveRequestServiceSvc_GetLeaveRequests alloc] init];
//    request.employeeCode = app.loginEmployeeInfo.EmployeeCode;
//    int skip = (pageNumber - 1) * ItemPerPage;
//    request.skip = [NSNumber numberWithInt:skip];
//    request.take = [NSNumber numberWithInt:ItemPerPage];
//    
//    [binding GetLeaveRequestsAsyncUsingParameters:request delegate:self];
//}
//
//-(void)operation:(BasicHttpBinding_IEmployeeServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmployeeServiceBindingResponse *)response
//{
//    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//    
//    if (response.bodyParts.count == 0)
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi kết nối" message:@"Không thể kết nối đến server" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [alert show];
//    }
//    for (id mine in response.bodyParts) {
//        if ([mine isKindOfClass:[LeaveRequestServiceSvc_GetLeaveRequestsResponse class]])
//        {
//            tns1_ArrayOfV_SelfLeaveRequest *result = [mine GetLeaveRequestsResult];
//            leaveRequests = [result V_SelfLeaveRequest];
//            
//            [self categoryRequestFromAllRequests:leaveRequests];
//            if (leaveRequests.count < 20)
//                nextPage.enabled = NO;
//            [self.tableView reloadData];
//        }
//    }
//}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (leaveRequests.count == 0)
        return 1;
    
    int sectionCount = 0;
    if (pendingRequests.count > 0)
    {
        sectionCount += 1;
        [sectionNames setValue:pendingRequests forKey:PendingString];
    }
    if (acceptedRequests.count > 0)
    {
        sectionCount += 1;
        [sectionNames setValue:acceptedRequests forKey:Approvedstring];
    }
    if (deniedRequests.count > 0)
    {
        sectionCount += 1;
        [sectionNames setValue:deniedRequests forKey:DeniedString];
    }
    
    return sectionCount;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (leaveRequests.count == 0)
        return nil;
    
    NSArray *keys = [sectionNames allKeys];
    return keys[section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (leaveRequests.count == 0)
        return 1;
    
    NSString *key = [sectionNames allKeys][section];
    NSArray *array = (NSArray*) sectionNames[key];
    return array.count;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (leaveRequests.count == 0)
        return [super tableView:tableView viewForHeaderInSection:section];
    
    NSString *key = sectionNames.allKeys[section];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 3, tableView.bounds.size.width - 10, 18)];
    label.text = key;
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont boldSystemFontOfSize:16];
    label.backgroundColor = [UIColor clearColor];
    [headerView addSubview:label];
    

    if ([key isEqualToString:Approvedstring])
    {
        headerView.backgroundColor = [UIColor colorWithRed:120.0/255.0 green:186.0/255.0 blue:0 alpha:1];
    }
    else if ([key isEqualToString:DeniedString])
        headerView.backgroundColor = [UIColor colorWithRed:176.0/255.0 green:30.0/2550.0 blue:0 alpha:1];
    else
        headerView.backgroundColor = [UIColor colorWithRed:244.0/255.0 green:179.0/255.0 blue:0 alpha:1];
    
    headerView.layer.borderColor = [UIColor whiteColor].CGColor;
    headerView.layer.borderWidth = 0.8f;
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (leaveRequests.count == 0)
        return [self getDefaultEmptyCell];
    
//    NSString *key = sectionNames.allKeys[indexPath.section];
//    NSArray *values = (NSArray *)sectionNames[key];
//    
//    LeaveRequestCell *cell = [tableView dequeueReusableCellWithIdentifier:leaveRequestIdentifier];
//    if (cell == nil)
//        cell = [[LeaveRequestCell alloc] init];
    
//    tns1_V_SelfLeaveRequest *leaveRequest = values[indexPath.row];
//    cell.lb_name.text = leaveRequest.EmployeeName;
//    cell.lb_reason.text = leaveRequest.Reason;
//    cell.lb_absenceType.text = leaveRequest.TimeAbsenceTypeName;
//    cell.lb_fromDate.text = [dateFormatter stringFromDate:leaveRequest.StartDate];
//    cell.lb_toDate.text = [dateFormatter stringFromDate:leaveRequest.EndDate];
//    cell.lb_unitName.text = leaveRequest.EmpUnitName;
//    
//    UIView *view = [[UIView alloc] initWithFrame:cell.frame];
//    view.backgroundColor = [self colorForViewAtIndexPath:indexPath status:[[leaveRequest ApprovalStatus] intValue]];
//    [cell setBackgroundView:view];
//    return cell;
}

-(UIColor*)colorForViewAtIndexPath:(NSIndexPath*)indexPath status:(int)approvalStatus
{
    BOOL even = indexPath.row % 2 == 0;
    switch (approvalStatus) {
        case 1:
            return even ? [UIColor colorWithRed:244.0/255.0 green:170.0/255.0 blue:0 alpha:0.4] : [UIColor colorWithRed:244.0/255.0 green:179.0/255.0 blue:0 alpha:0.2];
        case 2:
            return even ? [UIColor colorWithRed:120.0/255.0 green:186.0/255.0 blue:0 alpha:0.4] : [UIColor colorWithRed:120.0/255.0 green:186.0/255.0 blue:0 alpha:0.2];
        default:
            return even ? [UIColor colorWithRed:176.0/255.0 green:30.0/255.0 blue:0 alpha:0.4] : [UIColor colorWithRed:176.0/255.0 green:30.0/255.0 blue:0 alpha:0.2];
    }
}

-(void)addRequest:(id)sender
{
    [self performSegueWithIdentifier:toLeaveFormSegueId sender:self];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    UIStoryboard *storyboard = self.storyboard;
//    
//    NSString *key = sectionNames.allKeys[indexPath.section];
//    NSArray *values = (NSArray *)sectionNames[key];
    
//    tns1_V_SelfLeaveRequest *leaveRequest = values[indexPath.row];
//    if (leaveRequest.ApprovalStatus.intValue != 1)
//    {
//        ViewLeaveRequestController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ViewLeaveRequestId"];
//        viewController.leaveRequestId = leaveRequest.SelfLeaveRequestId;
//        [self.navigationController pushViewController:viewController animated:YES];
//    }
//    else
//    {
//        LeaverRequestViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"LeaveRequestView"];
//        controller.leaveRequest = leaveRequest;
//        [self.navigationController pushViewController:controller animated:YES];
//    }
}

@end
