//
//  CourseDetailCell.m
//  eSuccess
//
//  Created by HPTVIETNAM on 6/7/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "CourseDetailCell.h"

@implementation CourseDetailCell
{
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setCourse:(Course *)course
{
    if (_course != course)
    {
        _course = course;
        _lb_courseName.text = _course.courseName;
        _lb_courseType.text = _course.courseTypeName;
        _lb_courseTopic.text = _course.topicGroupName;
        _lb_courseStatus.text = _course.courseStatus;
        
        if (self.progressBar == nil)
        {
            ADVPercentProgressBar * progressBar = [[ADVPercentProgressBar alloc] initWithFrame:CGRectMake(0, 0, _v_progressBarContainer.frame.size.width, _v_progressBarContainer.frame.size.height) andProgressBarColor:ADVProgressBarRed];
            self.progressBar = progressBar;
            [_v_progressBarContainer addSubview:self.progressBar];
            self.progressBar.minProgressValue = 0;
            self.progressBar.maxProgressValue = 100;
            self.progressBar.showPercent = YES;
        }
        
        if (course.progressStatus.intValue <= 30)
            self.progressBar.progressBarColor = ADVProgressBarRed;
        else if (course.progressStatus.intValue <= 70)
            self.progressBar.progressBarColor = ADVProgressBarBlue;
        else
            self.progressBar.progressBarColor = ADVProgressBarGreen;
        
        self.progressBar.progress = course.progressStatus.intValue;
        
        @try {
            _tv_courseDescription.text = course.courseDescription;
        }
        @catch (NSException *exception) {
            NSLog(@"course description is null");
        }

    }
}
@end
