//
//  NetworkResolver.h
//  eSuccess
//
//  Created by HPTVIETNAM on 12/20/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkResolver : NSObject
+ (NSString *)getIPAddress;
@end
