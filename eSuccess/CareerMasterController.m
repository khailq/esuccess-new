//
//  CareerMasterController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 5/3/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "CareerMasterController.h"

@interface CareerMasterController ()

@end

@implementation CareerMasterController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.clearsSelectionOnViewWillAppear = NO;
    self.title = @"Career";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
}
@end
