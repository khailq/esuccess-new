//
//  CreateOrViewLeaveRequestViewController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 9/25/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "RequestServiceSvc.h"
#import "SystemServiceSvc.h"
#import "CreateOrViewLeaveRequestViewController.h"
#import "EmployeeServiceSvc.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
@interface CreateOrViewLeaveRequestViewController ()
{
    __weak IBOutlet UITextField *tb_SupplementDaysOffRemaining; // Tổng số ngày nghỉ bù còn lại
    __weak IBOutlet UITextField *tb_SupplementDaysOffPerYear;   // Tổng số ngày nghỉ bù trong năm
    __weak IBOutlet UITextField *tb_SupplemenetDaysOffUsed;     // Tổng số ngày nghỉ bù đã sử dụng
    __weak IBOutlet UITextField *tb_TotalDaysRequestToLeave;    // Tổng số ngày đã xin vắng mặt
    __weak IBOutlet UITextField *tb_TotalDaysLeft;          // Tổng số ngày đã nghỉ
    __weak IBOutlet UITextField *tb_DaysOffAddOrRemove;
    __weak IBOutlet UITextField *tb_SupplementalDaysOff;    // Tổng số ngày nghỉ bù    
    __weak IBOutlet UITextField *tb_TotalDaysOffRemaining;  // Số ngày nghỉ còn lại
    __weak IBOutlet UITextField *tb_TotalDaysOffPerMonth;   // Số ngày phép tháng
    __weak IBOutlet UITextField *tb_totalDaysOff;           // Tổng số ngày phép
    __weak IBOutlet UITextField *tb_TotalDaysOffPerYear;    // Tổng số ngày phép thâm niên
    __weak IBOutlet UITextField *tb_PhoneNumber;
    __weak IBOutlet UITextField *tb_Address;
    __weak IBOutlet UITextView *tb_reason;
    __weak IBOutlet UILabel *lb_absenceCost;
    __weak IBOutlet UILabel *lb_AbsenceType;
    __weak IBOutlet UILabel *lb_approverName;
    __weak IBOutlet UILabel *lb_ToDate;
    __weak IBOutlet UILabel *lb_FromDate;
    
    emp_tns1_SP_GetAllParentEmployee_Result * selectedLeader;
    sys_tns1_TB_ESS_SYSTEM_PARAMETER *selectedAbsenceType;
    
    NSDateFormatter *dateFormatter;
}
@end

@implementation CreateOrViewLeaveRequestViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.isViewMode = YES;
    if (self.isViewMode)
    {
        UIBarButtonItem *submitItem = [[UIBarButtonItem alloc] initWithTitle:@"Gởi đơn" style:UIBarButtonItemStyleDone target:self action:@selector(onSubmitRequestClicked:)];
        UIBarButtonItem *clearItem = [[UIBarButtonItem alloc] initWithTitle:@"Làm lại" style:UIBarButtonItemStyleBordered target:self action:@selector(onClearFormClicked:)];
        self.navigationItem.rightBarButtonItems = @[submitItem, clearItem];
    }
    
    tb_reason.layer.cornerRadius = 3;
    
    dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"dd/MM/yyyy";
}

#pragma mark- BarButtonItem actions

-(void)onClearFormClicked:(id)sender
{
        
}

-(void)onSubmitRequestClicked:(id)sender
{
    
}

#pragma mark-

-(void)createSubmitModelFromForm
{
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    
    
    req_tns1_LeaveRequestSubmitModel *model = [req_tns1_LeaveRequestSubmitModel new];
    model.Address = tb_Address.text;
    model.Phone = tb_PhoneNumber.text;
    model.Reason = tb_reason.text;
    model.ApproverId = selectedLeader.ParentId;
    model.CreatedDate = [dateFormatter stringFromDate:[NSDate date]];
    model.StartDate = lb_FromDate.text;
    model.EndDate = lb_ToDate.text;
    model.TimeAbsenceTypeId = [NSString stringWithFormat:@"%@|%@", selectedAbsenceType.SysParamCode, selectedAbsenceType.SysParamType];
    model.EmpId = app.sysUser.EmployeeId;
}

-(void)setIsViewMode:(BOOL)isViewMode
{
    if (_isViewMode != isViewMode)
    {
        _isViewMode = isViewMode;
        if (isViewMode)
        {
            tb_reason.editable = NO;
            tb_Address.enabled = NO;
            tb_PhoneNumber.enabled = NO;
        }
        else
        {
            tb_reason.editable = YES;
            tb_Address.enabled = YES;
            tb_PhoneNumber.enabled = YES;
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
