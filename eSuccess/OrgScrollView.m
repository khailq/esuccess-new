//
//  OrgScrollView.m
//  eSuccess
//
//  Created by HPTVIETNAM on 8/12/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "OrgScrollView.h"
#import "UIImageView+AFNetworking.h"
#import "Org Service/OrganizationServiceSvc.h"
#import "AppDelegate.h"

#define TopMargin 50
#define RightMargin 30


@implementation OrgScrollView
{
    NSNumber *targetId;
    NSArray *directReports;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id)initWithDirectReportList:(NSArray *)directReport frame:(CGRect)frame parent:(id<ProfilePanelProtocol>)parent
{
    self = [super initWithFrame:frame];
    if (self)
    {
        directReports = directReport;
        self.parentDataContext = parent;
    }
    
    return self;
}



// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{    
    CGFloat midX = 768 / 2.0;
    
    CGFloat topX = midX - H_PanelWidth / 2.0;
    CGFloat topY = 100;
    
    CGRect topRect = CGRectMake(topX, topY, H_PanelWidth, H_PanelHeight);
    
    if (directReports.count == 0)
        return;
    
    ProfilePanelView *topPanel = [[ProfilePanelView alloc] initWithFrame:topRect];
    org_tns2_V_ESS_ORG_DIRECT_REPORT *reporter = directReports[0];
    topPanel.delegate = self.parentDataContext;
    topPanel.dataContext = reporter;
    [topPanel setChildName:reporter.ChildName];
    [topPanel setUnitName:reporter.ChildUnitName];
    [topPanel setPositionname:reporter.ChildJobPositionName];
    [self setImageUrl:reporter.ImageUrl forImageView:topPanel.avatar];
    
    [self addSubview:topPanel];
    
    if (directReports.count <= 1)
        return;
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGFloat black[4] = {0, 0,
        0, 1};
    CGContextSetStrokeColor(ctx, black);
    
    CGContextBeginPath(ctx);

    CGFloat startX = topX + H_PanelWidth / 2.0;
    CGFloat startY = topY + H_PanelHeight;
    CGContextMoveToPoint(ctx, startX, startY);
        
    CGFloat endY = startY + TopMargin / 2.0;
    CGContextAddLineToPoint(ctx, startX, endY);
    
    CGContextClosePath(ctx);
    CGContextStrokePath(ctx);
    
    
    CGFloat leftX = 50;
    if (directReports.count == 3)
        leftX = topX - H_PanelWidth;
    CGFloat y = startY + TopMargin;
    CGFloat contentWidth = leftX;
    
    NSMutableArray *firstLastArray = [NSMutableArray arrayWithCapacity:2];
    
    for (int  i=1; i<directReports.count; ++i)
    {
        CGRect rect = CGRectMake(leftX, y, H_PanelWidth, H_PanelHeight);
        ProfilePanelView *panel = [[ProfilePanelView alloc] initWithFrame:rect];
        panel.delegate = self.parentDataContext;
        org_tns2_V_ESS_ORG_DIRECT_REPORT *dataContext = directReports[i];
        panel.dataContext = dataContext;
        [panel setChildName:dataContext.ChildName];
        [panel setUnitName:dataContext.ChildUnitName];
        [panel setPositionname:dataContext.ChildJobPositionName];
        [self setImageUrl:dataContext.ImageUrl forImageView:panel.avatar];
    
        
        
        [self addSubview:panel];
        contentWidth += H_PanelWidth + RightMargin;
        
        CGContextBeginPath(ctx);
        CGContextMoveToPoint(ctx, leftX + H_PanelWidth / 2.0, y);
        CGContextAddLineToPoint(ctx, leftX + H_PanelWidth / 2.0, endY);
        
        CGContextClosePath(ctx);
        CGContextStrokePath(ctx);
        
        leftX += H_PanelWidth + RightMargin;
        
        if (directReports.count == 3)
            leftX = midX + H_PanelWidth / 2.0;
        
        if (i == 1 || i == directReports.count - 1)
            [firstLastArray addObject:panel];
    }
    
    CGContextBeginPath(ctx);
    ProfilePanelView *first = firstLastArray[0];
    CGContextMoveToPoint(ctx, first.frame.origin.x + H_PanelWidth / 2.0, first.frame.origin.y - TopMargin / 2.0);
    ProfilePanelView *last = firstLastArray[1];
    CGContextAddLineToPoint(ctx, last.frame.origin.x + H_PanelWidth / 2.0, last.frame.origin.y - TopMargin / 2.0);
    CGContextClosePath(ctx);
    
    CGContextStrokePath(ctx);
    self.contentWidth = contentWidth;
}

-(void)setImageUrl:(NSString *)url forImageView:(UIImageView *)imgView
{
    if (url != nil){
        NSString *baseAvatarURL = AvatarUrl;
        baseAvatarURL = [baseAvatarURL stringByAppendingString:url];
        [imgView setImageWithURL:[NSURL URLWithString:baseAvatarURL]];
    }
}

-(void)drawUserPanel:(CGFloat)midX midY:(CGFloat)midY
{
    NSMutableArray* panelArray = [NSMutableArray arrayWithCapacity:directReports.count];   
    
    
    CGFloat totalHeight = H_PanelHeight + TopMargin + directReports.count > 0 ? H_PanelHeight : 0;
    
    CGFloat x = midX - H_PanelWidth / 2.0;
    CGFloat y = midY - totalHeight / 2.0;
    
    CGRect topRect = CGRectMake(x, y, H_PanelWidth, H_PanelHeight);
    
    ProfilePanelView *topPanel = [[ProfilePanelView alloc] initWithFrame:topRect];
    [self addSubview:topPanel];
    
    [panelArray addObject:topPanel];
    
    CGFloat totalWidth = (directReports.count - 1) * H_PanelWidth + (directReports.count - 2) * RightMargin ;
    CGFloat firstX = midX - totalWidth / 2.0;
    CGFloat childY = H_PanelHeight + TopMargin;
    
    for(org_tns2_V_ESS_ORG_DIRECT_REPORT *reporter in directReports)
    {
        if (![reporter.ChildId isEqualToNumber:targetId])
        {
            CGRect rect = CGRectMake(firstX, childY, H_PanelWidth, H_PanelHeight);
            ProfilePanelView *panel = [[ProfilePanelView alloc] initWithFrame:rect];
            [self addSubview:panel];
            
            [panelArray addObject:panel];
        }
    }
    
    
}

@end
