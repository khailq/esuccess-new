//
//  BasicDegreeViewController.h
//  eSuccess
//
//  Created by admin on 5/10/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "BaseDetailTableViewController.h"
@interface BasicDegreeViewController : BaseDetailTableViewController
@property(nonatomic, retain) NSMutableArray *degreeArray;
@property(nonatomic, retain) NSMutableArray *degreeNameArray;

@end
