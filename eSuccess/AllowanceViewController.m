//
//  AllowanceViewController.m
//  eSuccess
//
//  Created by admin on 5/24/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "AllowanceViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "DayLeaveDetailCell.h"
@interface AllowanceViewController ()

@end

@implementation AllowanceViewController
@synthesize allowanceArr;
@synthesize allowanceNameArr;
static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Phụ cấp các loại";
    [self getData];
    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 130;
//}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([allowanceNameArr count] > 0) {
        return [allowanceNameArr objectAtIndex:section];
    }
    return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [allowanceArr count] > 0 ? allowanceArr.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [allowanceArr count] > 0 ? 4 : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (allowanceArr.count == 0)
        return [self getDefaultEmptyCell];
    
    DayLeaveDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
    if (cell == nil) {
        cell = [[DayLeaveDetailCell alloc]init];
    }
    cell.lbLeft.font = [UIFont systemFontOfSize:14];
    cell.lbLeft.backgroundColor = [UIColor clearColor];
    cell.lbRight.font = [UIFont boldSystemFontOfSize:16];
    cell.lbRight.backgroundColor = [UIColor clearColor];
    
    int section = indexPath.section;
    empl_tns1_V_EmpProfileAllowance *allowance = [allowanceArr objectAtIndex:section];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy"];

    switch (indexPath.row) {
        case 0:
            cell.lbRight.text = @"Tên gọi";
            cell.lbLeft.text = allowance.AllowanceName;
            break;
        case 1:
            cell.lbRight.text = @"Ngày áp dụng";
            cell.lbLeft.text = [formatter stringFromDate:allowance.ActivedDate];
            break;
        case 2:
            cell.lbLeft.text = @"Giá trị";
            cell.lbLeft.text = allowance.AllowanceValue.stringValue;
            break;
        default:
            cell.lbLeft.text = @"Lý do";
            cell.lbRight.text = allowance.Note;
            break;
    }

    return cell;
}

#pragma get Data Allowance
-(void)getData
{
    allowanceArr = [[NSMutableArray alloc]init];
    allowanceNameArr = [[NSMutableArray alloc]init];
    
    [self getAllowanceById];
}
-(void)getAllowanceById
{
    BasicHttpBinding_IEmpProfileLayerServiceBinding * binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
    EmpProfileLayerServiceSvc_GetViewEmpProfileAllowanceByView *request = [[EmpProfileLayerServiceSvc_GetViewEmpProfileAllowanceByView alloc] init];
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    if (app.otherEmpId != nil) {
        request.EmployeeId = app.otherEmpId;
    } else{
        request.employeeId = app.sysUser.EmployeeId ;
    }
    [binding GetViewEmpProfileAllowanceByViewAsyncUsingParameters:request delegate:self];
}


- (void)operation:(BasicHttpBinding_IEmpProfileLayerServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)response
{
    if ([response.bodyParts count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi kết nối" message:@"Không thể kết nối đến server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    for (id mine in response.bodyParts) {
        if ([mine isKindOfClass:[EmpProfileLayerServiceSvc_GetViewEmpProfileAllowanceByViewResponse class]]) {
            empl_tns1_ArrayOfV_EmpProfileAllowance *result = [mine GetViewEmpProfileAllowanceByViewResult];
            allowanceArr = result.V_EmpProfileAllowance;
            for (empl_tns1_V_EmpProfileAllowance *allowance in allowanceArr) {
                [allowanceNameArr addObject:allowance.AllowanceCode];
            }
            
            if (!self.viewDidDisappear)
                [self.tableView reloadData];
        }
        
        //        if ([mine isKindOfClass:[SOAPFault class]])
        //        {
        //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Server error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //            [alert show];
        //        }
    }
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
}

@end
