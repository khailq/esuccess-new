#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class OrganizationServiceSvc_GetEmployeeByListOrgUnitId;
@class OrganizationServiceSvc_GetEmployeeByListOrgUnitIdResponse;
@class OrganizationServiceSvc_SearchOrgJob;
@class OrganizationServiceSvc_SearchOrgJobResponse;
@class OrganizationServiceSvc_SearchOrgJobPosition;
@class OrganizationServiceSvc_SearchOrgJobPositionResponse;
@class OrganizationServiceSvc_SearchOrgUnit;
@class OrganizationServiceSvc_SearchOrgUnitResponse;
@class OrganizationServiceSvc_GetAllCLogRatings;
@class OrganizationServiceSvc_GetAllCLogRatingsResponse;
@class OrganizationServiceSvc_GetOrgProficencyTypes;
@class OrganizationServiceSvc_GetOrgProficencyTypesResponse;
@class OrganizationServiceSvc_GetAllSpecificCompetencyByJobPositionId;
@class OrganizationServiceSvc_GetAllSpecificCompetencyByJobPositionIdResponse;
@class OrganizationServiceSvc_GetAllOrgGroupCompetency;
@class OrganizationServiceSvc_GetAllOrgGroupCompetencyResponse;
@class OrganizationServiceSvc_GetAllCompetencyByJobId;
@class OrganizationServiceSvc_GetAllCompetencyByJobIdResponse;
@class OrganizationServiceSvc_GetAllJobSpecificCompetencyByCompId;
@class OrganizationServiceSvc_GetAllJobSpecificCompetencyByCompIdResponse;
@class OrganizationServiceSvc_GetAllJobPosSpecificCompetencyByJobIdCompId;
@class OrganizationServiceSvc_GetAllJobPosSpecificCompetencyByJobIdCompIdResponse;
@class OrganizationServiceSvc_GetAllCompetencyLevelByCompetencyId;
@class OrganizationServiceSvc_GetAllCompetencyLevelByCompetencyIdResponse;
@class OrganizationServiceSvc_GetAllCompetencyLevelDetailByLevelId;
@class OrganizationServiceSvc_GetAllCompetencyLevelDetailByLevelIdResponse;
@class OrganizationServiceSvc_GetAllV_OrgSkillCompetencyBy;
@class OrganizationServiceSvc_GetAllV_OrgSkillCompetencyByResponse;
@class OrganizationServiceSvc_GetAllV_OrgQualificationCompetencyBy;
@class OrganizationServiceSvc_GetAllV_OrgQualificationCompetencyByResponse;
@class OrganizationServiceSvc_GetAllV_OrgExperienceCompetencyBy;
@class OrganizationServiceSvc_GetAllV_OrgExperienceCompetencyByResponse;
@class OrganizationServiceSvc_GetAllV_OrgDegreeCompetencyBy;
@class OrganizationServiceSvc_GetAllV_OrgDegreeCompetencyByResponse;
@class OrganizationServiceSvc_GetAllV_OrgAttitudeCompetencyBy;
@class OrganizationServiceSvc_GetAllV_OrgAttitudeCompetencyByResponse;
@class OrganizationServiceSvc_GetAllV_OrgKnowledgeCompetencyBy;
@class OrganizationServiceSvc_GetAllV_OrgKnowledgeCompetencyByResponse;
@class OrganizationServiceSvc_GetAllJobPosCompetency;
@class OrganizationServiceSvc_GetAllJobPosCompetencyResponse;
@class OrganizationServiceSvc_GetAllCoreCompetency;
@class OrganizationServiceSvc_GetAllCoreCompetencyResponse;
@class OrganizationServiceSvc_GetAllJobPosCompetencyByGroupId;
@class OrganizationServiceSvc_GetAllJobPosCompetencyByGroupIdResponse;
@class OrganizationServiceSvc_GetAllCoreCompetencyByGroupId;
@class OrganizationServiceSvc_GetAllCoreCompetencyByGroupIdResponse;
@class OrganizationServiceSvc_GetJobPosCompetencyById;
@class OrganizationServiceSvc_GetJobPosCompetencyByIdResponse;
@class OrganizationServiceSvc_GetAllJobPositionByJobId;
@class OrganizationServiceSvc_GetAllJobPositionByJobIdResponse;
@class OrganizationServiceSvc_EditJobPosCompetency;
@class OrganizationServiceSvc_EditJobPosCompetencyResponse;
@class OrganizationServiceSvc_GetDirecReportById;
@class OrganizationServiceSvc_GetDirecReportByIdResponse;
@class OrganizationServiceSvc_FindLevelOfGenOfPSCd;
@class OrganizationServiceSvc_FindLevelOfGenOfPSCdResponse;
@class OrganizationServiceSvc_IsAscendant;
@class OrganizationServiceSvc_IsAscendantResponse;
@class OrganizationServiceSvc_IsDescendant;
@class OrganizationServiceSvc_IsDescendantResponse;
@class OrganizationServiceSvc_IsXthAscendant;
@class OrganizationServiceSvc_IsXthAscendantResponse;
@class OrganizationServiceSvc_IsXthDescendant;
@class OrganizationServiceSvc_IsXthDescendantResponse;
@class OrganizationServiceSvc_GetXAscendantFromGenPSCd;
@class OrganizationServiceSvc_GetXAscendantFromGenPSCdResponse;
@class OrganizationServiceSvc_GetXDescendanceFromGenPSCd;
@class OrganizationServiceSvc_GetXDescendanceFromGenPSCdResponse;
@class OrganizationServiceSvc_GetAllGenPSHasTheSameLevel;
@class OrganizationServiceSvc_GetAllGenPSHasTheSameLevelResponse;
@class OrganizationServiceSvc_GetAllGenPSHasTheSameLevelAndParentNode;
@class OrganizationServiceSvc_GetAllGenPSHasTheSameLevelAndParentNodeResponse;
@class OrganizationServiceSvc_GetListOfPersonelStruct;
@class OrganizationServiceSvc_GetListOfPersonelStructResponse;
@class OrganizationServiceSvc_GetListOfEmployeeMappingPersonelStruct;
@class OrganizationServiceSvc_GetListOfEmployeeMappingPersonelStructResponse;
@class OrganizationServiceSvc_GetAllTreeOrgJobTitle;
@class OrganizationServiceSvc_GetAllTreeOrgJobTitleResponse;
@class OrganizationServiceSvc_GetViewOrgJobTitleByCode;
@class OrganizationServiceSvc_GetViewOrgJobTitleByCodeResponse;
@class OrganizationServiceSvc_GetListOrgJobTitleGroups;
@class OrganizationServiceSvc_GetListOrgJobTitleGroupsResponse;
@class OrganizationServiceSvc_GetListOrgJobTitleLevels;
@class OrganizationServiceSvc_GetListOrgJobTitleLevelsResponse;
@class OrganizationServiceSvc_AddOrgJobTitle;
@class OrganizationServiceSvc_AddOrgJobTitleResponse;
@class OrganizationServiceSvc_UpdateOrgJobTitle;
@class OrganizationServiceSvc_UpdateOrgJobTitleResponse;
@class OrganizationServiceSvc_DeleteOrgJobTitle;
@class OrganizationServiceSvc_DeleteOrgJobTitleResponse;
@class OrganizationServiceSvc_GetListAllOrgJobTitles;
@class OrganizationServiceSvc_GetListAllOrgJobTitlesResponse;
@class OrganizationServiceSvc_SearchOrgJobTitle;
@class OrganizationServiceSvc_SearchOrgJobTitleResponse;
@class OrganizationServiceSvc_GetListViewEmployeeByOrgJobTitleCode;
@class OrganizationServiceSvc_GetListViewEmployeeByOrgJobTitleCodeResponse;
@class OrganizationServiceSvc_GetAllClassificationEmpTree;
@class OrganizationServiceSvc_GetAllClassificationEmpTreeResponse;
@class OrganizationServiceSvc_GetAllClassificationEmpMapping;
@class OrganizationServiceSvc_GetAllClassificationEmpMappingResponse;
@class OrganizationServiceSvc_GetOrgChangeInfos;
@class OrganizationServiceSvc_GetOrgChangeInfosResponse;
@class OrganizationServiceSvc_GetAllChildrenReportEmployeeByParentId;
@class OrganizationServiceSvc_GetAllChildrenReportEmployeeByParentIdResponse;
@class OrganizationServiceSvc_GetAllDescendantsReportEmployeeByParentId;
@class OrganizationServiceSvc_GetAllDescendantsReportEmployeeByParentIdResponse;
@class OrganizationServiceSvc_GetReportEmployeeRoot;
@class OrganizationServiceSvc_GetReportEmployeeRootResponse;
@class OrganizationServiceSvc_GetAllUnits;
@class OrganizationServiceSvc_GetAllUnitsResponse;
@class OrganizationServiceSvc_GetAllChildrenOrgUnitByParentId;
@class OrganizationServiceSvc_GetAllChildrenOrgUnitByParentIdResponse;
@class OrganizationServiceSvc_GetAllDescendantsOrgUnitByParentId;
@class OrganizationServiceSvc_GetAllDescendantsOrgUnitByParentIdResponse;
@class OrganizationServiceSvc_GetOrgUnitRoot;
@class OrganizationServiceSvc_GetOrgUnitRootResponse;
@class OrganizationServiceSvc_GetUnitLeader;
@class OrganizationServiceSvc_GetUnitLeaderResponse;
@class OrganizationServiceSvc_GetAllUnitsPosition;
@class OrganizationServiceSvc_GetAllUnitsPositionResponse;
@class OrganizationServiceSvc_GetViewOrgUnitByOrgUnitId;
@class OrganizationServiceSvc_GetViewOrgUnitByOrgUnitIdResponse;
@class OrganizationServiceSvc_GetAllOrgUnitJobsByOrgUnitId;
@class OrganizationServiceSvc_GetAllOrgUnitJobsByOrgUnitIdResponse;
@class OrganizationServiceSvc_GetEmployeeKendoFilterByJobId;
@class OrganizationServiceSvc_GetEmployeeKendoFilterByJobIdResponse;
@class OrganizationServiceSvc_GetAllOrgJobByPage;
@class OrganizationServiceSvc_GetAllOrgJobByPageResponse;
@class OrganizationServiceSvc_GetViewOrgUnitJobByOrgUnitId;
@class OrganizationServiceSvc_GetViewOrgUnitJobByOrgUnitIdResponse;
@class OrganizationServiceSvc_CreateOrgUnitJob;
@class OrganizationServiceSvc_CreateOrgUnitJobResponse;
@class OrganizationServiceSvc_GetAllOrgUnitJobs;
@class OrganizationServiceSvc_GetAllOrgUnitJobsResponse;
@class OrganizationServiceSvc_GetOrgUnitsByPage;
@class OrganizationServiceSvc_GetOrgUnitsByPageResponse;
@class OrganizationServiceSvc_GetAllOrgUnitJobByOrgUnitId;
@class OrganizationServiceSvc_GetAllOrgUnitJobByOrgUnitIdResponse;
@class OrganizationServiceSvc_GetOrgUnitTreeByOrgUnitId;
@class OrganizationServiceSvc_GetOrgUnitTreeByOrgUnitIdResponse;
@class OrganizationServiceSvc_GetOrgUnitByOrgUnitId;
@class OrganizationServiceSvc_GetOrgUnitByOrgUnitIdResponse;
@class OrganizationServiceSvc_GetAllOrgUnitTree;
@class OrganizationServiceSvc_GetAllOrgUnitTreeResponse;
@class OrganizationServiceSvc_GetAllOrgUnitTreeByPermission;
@class OrganizationServiceSvc_GetAllOrgUnitTreeByPermissionResponse;
@class OrganizationServiceSvc_EditOrgUnit;
@class OrganizationServiceSvc_EditOrgUnitResponse;
@class OrganizationServiceSvc_TransferOrgUnit;
@class OrganizationServiceSvc_TransferOrgUnitResponse;
@class OrganizationServiceSvc_DeleteOrgUnit;
@class OrganizationServiceSvc_DeleteOrgUnitResponse;
@class OrganizationServiceSvc_DeleteOrgUnitJob;
@class OrganizationServiceSvc_DeleteOrgUnitJobResponse;
@class OrganizationServiceSvc_GetOrgSkillByOrgSkillId;
@class OrganizationServiceSvc_GetOrgSkillByOrgSkillIdResponse;
@class OrganizationServiceSvc_GetAllOrgSkillsBySkillType;
@class OrganizationServiceSvc_GetAllOrgSkillsBySkillTypeResponse;
@class OrganizationServiceSvc_GetAllOrgSkill;
@class OrganizationServiceSvc_GetAllOrgSkillResponse;
@class OrganizationServiceSvc_GetOrgSkillByPage;
@class OrganizationServiceSvc_GetOrgSkillByPageResponse;
@class OrganizationServiceSvc_GetOrgSkillKendoPageData;
@class OrganizationServiceSvc_GetOrgSkillKendoPageDataResponse;
@class OrganizationServiceSvc_GetOrgSkillPageNumber;
@class OrganizationServiceSvc_GetOrgSkillPageNumberResponse;
@class OrganizationServiceSvc_DeleteOrgSkill;
@class OrganizationServiceSvc_DeleteOrgSkillResponse;
@class OrganizationServiceSvc_GetOrgSkillById;
@class OrganizationServiceSvc_GetOrgSkillByIdResponse;
@class OrganizationServiceSvc_UpdateOrgSkill;
@class OrganizationServiceSvc_UpdateOrgSkillResponse;
@class OrganizationServiceSvc_CreateOrgSkill;
@class OrganizationServiceSvc_CreateOrgSkillResponse;
@class OrganizationServiceSvc_GetAllOrgTimeInCharge;
@class OrganizationServiceSvc_GetAllOrgTimeInChargeResponse;
@class OrganizationServiceSvc_GetAllOrgSkillType;
@class OrganizationServiceSvc_GetAllOrgSkillTypeResponse;
@class OrganizationServiceSvc_GetOrgSkillTypeByOrgSkillTypeId;
@class OrganizationServiceSvc_GetOrgSkillTypeByOrgSkillTypeIdResponse;
@class OrganizationServiceSvc_GetAllOrgProjectType;
@class OrganizationServiceSvc_GetAllOrgProjectTypeResponse;
@class OrganizationServiceSvc_GetOrgProjectTypeByOrgProjectTypeId;
@class OrganizationServiceSvc_GetOrgProjectTypeByOrgProjectTypeIdResponse;
@class OrganizationServiceSvc_GetAllCLogCareer;
@class OrganizationServiceSvc_GetAllCLogCareerResponse;
@class OrganizationServiceSvc_GetAllEmployeeByCareerCode;
@class OrganizationServiceSvc_GetAllEmployeeByCareerCodeResponse;
@class OrganizationServiceSvc_GetAllTimeInCharge;
@class OrganizationServiceSvc_GetAllTimeInChargeResponse;
@class OrganizationServiceSvc_GetAllOrgDegree;
@class OrganizationServiceSvc_GetAllOrgDegreeResponse;
@class OrganizationServiceSvc_GetOrgDegreeByOrgDegreeId;
@class OrganizationServiceSvc_GetOrgDegreeByOrgDegreeIdResponse;
@class OrganizationServiceSvc_GetAllOrgDegreeRank;
@class OrganizationServiceSvc_GetAllOrgDegreeRankResponse;
@class OrganizationServiceSvc_GetOrgDegreeRankByOrgDegreeRankId;
@class OrganizationServiceSvc_GetOrgDegreeRankByOrgDegreeRankIdResponse;
@class OrganizationServiceSvc_GetAllOrgJobs;
@class OrganizationServiceSvc_GetAllOrgJobsResponse;
@class OrganizationServiceSvc_GetViewOrgJobs;
@class OrganizationServiceSvc_GetViewOrgJobsResponse;
@class OrganizationServiceSvc_GetViewOrgJobById;
@class OrganizationServiceSvc_GetViewOrgJobByIdResponse;
@class OrganizationServiceSvc_GetAllViewOrgJobByCareerId;
@class OrganizationServiceSvc_GetAllViewOrgJobByCareerIdResponse;
@class OrganizationServiceSvc_GetAllOrgJobByCareer;
@class OrganizationServiceSvc_GetAllOrgJobByCareerResponse;
@class OrganizationServiceSvc_GetOrgJobById;
@class OrganizationServiceSvc_GetOrgJobByIdResponse;
@class OrganizationServiceSvc_GetOrgJobByCode;
@class OrganizationServiceSvc_GetOrgJobByCodeResponse;
@class OrganizationServiceSvc_GetOrgJobDescriptionDetailForJob;
@class OrganizationServiceSvc_GetOrgJobDescriptionDetailForJobResponse;
@class OrganizationServiceSvc_GetOrgJobDescriptionDetailForJobPosition;
@class OrganizationServiceSvc_GetOrgJobDescriptionDetailForJobPositionResponse;
@class OrganizationServiceSvc_CreateNewOrgJob;
@class OrganizationServiceSvc_CreateNewOrgJobResponse;
@class OrganizationServiceSvc_UpdateOrgJob;
@class OrganizationServiceSvc_UpdateOrgJobResponse;
@class OrganizationServiceSvc_DeleteOrgJob;
@class OrganizationServiceSvc_DeleteOrgJobResponse;
@class OrganizationServiceSvc_GetAllTreeOrgCareerJobAndWorkLevel;
@class OrganizationServiceSvc_GetAllTreeOrgCareerJobAndWorkLevelResponse;
@class OrganizationServiceSvc_GetAllEmployeeByOrgJobId;
@class OrganizationServiceSvc_GetAllEmployeeByOrgJobIdResponse;
@class OrganizationServiceSvc_GetAllOrgJobPositions;
@class OrganizationServiceSvc_GetAllOrgJobPositionsResponse;
@class OrganizationServiceSvc_GetListOrgJobPosByOrgJobId;
@class OrganizationServiceSvc_GetListOrgJobPosByOrgJobIdResponse;
@class OrganizationServiceSvc_GetAllOrgJobPosByOrgUnitId;
@class OrganizationServiceSvc_GetAllOrgJobPosByOrgUnitIdResponse;
@class OrganizationServiceSvc_GetAllJobPositionByOrgUnitId;
@class OrganizationServiceSvc_GetAllJobPositionByOrgUnitIdResponse;
@class OrganizationServiceSvc_GetAllJobPositionByOrgJobId;
@class OrganizationServiceSvc_GetAllJobPositionByOrgJobIdResponse;
@class OrganizationServiceSvc_GetAllJobPositionByWorkLevelId;
@class OrganizationServiceSvc_GetAllJobPositionByWorkLevelIdResponse;
@class OrganizationServiceSvc_GetAllJobPositionByJobTitleId;
@class OrganizationServiceSvc_GetAllJobPositionByJobTitleIdResponse;
@class OrganizationServiceSvc_GetAllViewOrgJobPosition;
@class OrganizationServiceSvc_GetAllViewOrgJobPositionResponse;
@class OrganizationServiceSvc_GetAllEmployeeByPositionId;
@class OrganizationServiceSvc_GetAllEmployeeByPositionIdResponse;
@class OrganizationServiceSvc_AddOrgJobPosition;
@class OrganizationServiceSvc_AddOrgJobPositionResponse;
@class OrganizationServiceSvc_UpdateOrgJobPosition;
@class OrganizationServiceSvc_UpdateOrgJobPositionResponse;
@class OrganizationServiceSvc_DeleteOrgJobPosition;
@class OrganizationServiceSvc_DeleteOrgJobPositionResponse;
@class OrganizationServiceSvc_GetAllOrganizationJobPosition;
@class OrganizationServiceSvc_GetAllOrganizationJobPositionResponse;
@class OrganizationServiceSvc_GetOrgJobPositionById;
@class OrganizationServiceSvc_GetOrgJobPositionByIdResponse;
@class OrganizationServiceSvc_GetViewOrgJobPositionDetailById;
@class OrganizationServiceSvc_GetViewOrgJobPositionDetailByIdResponse;
@class OrganizationServiceSvc_GetViewOrgJobPositionById;
@class OrganizationServiceSvc_GetViewOrgJobPositionByIdResponse;
@class OrganizationServiceSvc_GetAllOrgExperiences;
@class OrganizationServiceSvc_GetAllOrgExperiencesResponse;
@class OrganizationServiceSvc_CreateOrgExperience;
@class OrganizationServiceSvc_CreateOrgExperienceResponse;
@class OrganizationServiceSvc_GetAllOrgExperience;
@class OrganizationServiceSvc_GetAllOrgExperienceResponse;
@class OrganizationServiceSvc_GetOrgExperienceByOrgExperienceId;
@class OrganizationServiceSvc_GetOrgExperienceByOrgExperienceIdResponse;
@class OrganizationServiceSvc_GetOrgExperienceByProjectTypeAndTimeInCharge;
@class OrganizationServiceSvc_GetOrgExperienceByProjectTypeAndTimeInChargeResponse;
@class OrganizationServiceSvc_GetAllOrgQualifications;
@class OrganizationServiceSvc_GetAllOrgQualificationsResponse;
@class OrganizationServiceSvc_CreateOrgQualification;
@class OrganizationServiceSvc_CreateOrgQualificationResponse;
@class OrganizationServiceSvc_GetQualificationByMajorAndEducationLevel;
@class OrganizationServiceSvc_GetQualificationByMajorAndEducationLevelResponse;
@class OrganizationServiceSvc_GetAllOrgKnowledge;
@class OrganizationServiceSvc_GetAllOrgKnowledgeResponse;
@class OrganizationServiceSvc_GetAllOrgKnowledgeByMajor;
@class OrganizationServiceSvc_GetAllOrgKnowledgeByMajorResponse;
@class OrganizationServiceSvc_GetOrgKnowledgeByPage;
@class OrganizationServiceSvc_GetOrgKnowledgeByPageResponse;
@class OrganizationServiceSvc_GetOrgKnowledgePageNumber;
@class OrganizationServiceSvc_GetOrgKnowledgePageNumberResponse;
@class OrganizationServiceSvc_DeleteOrgKnowledge;
@class OrganizationServiceSvc_DeleteOrgKnowledgeResponse;
@class OrganizationServiceSvc_GetOrgKnowledgeById;
@class OrganizationServiceSvc_GetOrgKnowledgeByIdResponse;
@class OrganizationServiceSvc_UpdateOrgKnowledge;
@class OrganizationServiceSvc_UpdateOrgKnowledgeResponse;
@class OrganizationServiceSvc_CreateOrgKnowledge;
@class OrganizationServiceSvc_CreateOrgKnowledgeResponse;
@class OrganizationServiceSvc_GetAllRating;
@class OrganizationServiceSvc_GetAllRatingResponse;
@class OrganizationServiceSvc_DeleteOrgJobPositioRequiredProficency;
@class OrganizationServiceSvc_DeleteOrgJobPositioRequiredProficencyResponse;
@class OrganizationServiceSvc_GetAllOrgJobPositionKnowledgeProficency;
@class OrganizationServiceSvc_GetAllOrgJobPositionKnowledgeProficencyResponse;
@class OrganizationServiceSvc_AddOrgJobPositionKnowledgeProficencyByView;
@class OrganizationServiceSvc_AddOrgJobPositionKnowledgeProficencyByViewResponse;
@class OrganizationServiceSvc_UpdateOrgJobPositionKnowledgeProficencyByView;
@class OrganizationServiceSvc_UpdateOrgJobPositionKnowledgeProficencyByViewResponse;
@class OrganizationServiceSvc_GetAllOrgJobPositionExperienceProficency;
@class OrganizationServiceSvc_GetAllOrgJobPositionExperienceProficencyResponse;
@class OrganizationServiceSvc_AddOrgJobPositionExperienceProficencyByView;
@class OrganizationServiceSvc_AddOrgJobPositionExperienceProficencyByViewResponse;
@class OrganizationServiceSvc_UpdateOrgJobPositionExperienceProficencyByView;
@class OrganizationServiceSvc_UpdateOrgJobPositionExperienceProficencyByViewResponse;
@class OrganizationServiceSvc_GetAllOrgJobPositionQualificationProficency;
@class OrganizationServiceSvc_GetAllOrgJobPositionQualificationProficencyResponse;
@class OrganizationServiceSvc_AddOrgJobPositionQualificationProficencyByView;
@class OrganizationServiceSvc_AddOrgJobPositionQualificationProficencyByViewResponse;
@class OrganizationServiceSvc_UpdateOrgJobPositionQualificationProficencyByView;
@class OrganizationServiceSvc_UpdateOrgJobPositionQualificationProficencyByViewResponse;
@class OrganizationServiceSvc_GetAllOrgJobPositionSkillProficency;
@class OrganizationServiceSvc_GetAllOrgJobPositionSkillProficencyResponse;
@class OrganizationServiceSvc_AddOrgJobPositionSkillProficencyByView;
@class OrganizationServiceSvc_AddOrgJobPositionSkillProficencyByViewResponse;
@class OrganizationServiceSvc_UpdateOrgJobPositionSkillProficencyByView;
@class OrganizationServiceSvc_UpdateOrgJobPositionSkillProficencyByViewResponse;
@class OrganizationServiceSvc_GetAllTreeOrgJobTitlePosition;
@class OrganizationServiceSvc_GetAllTreeOrgJobTitlePositionResponse;
@class OrganizationServiceSvc_GetAllTreeOrgJobAndWorkLevel;
@class OrganizationServiceSvc_GetAllTreeOrgJobAndWorkLevelResponse;
@class OrganizationServiceSvc_GetAllTreeOrgUnitPosition;
@class OrganizationServiceSvc_GetAllTreeOrgUnitPositionResponse;
@class OrganizationServiceSvc_GetAllTreeOrgJobAndPosition;
@class OrganizationServiceSvc_GetAllTreeOrgJobAndPositionResponse;
@class OrganizationServiceSvc_GetAllOrgGroup;
@class OrganizationServiceSvc_GetAllOrgGroupResponse;
@class OrganizationServiceSvc_GetOrgGroupByPage;
@class OrganizationServiceSvc_GetOrgGroupByPageResponse;
@class OrganizationServiceSvc_GetOrgGroupKendoPageData;
@class OrganizationServiceSvc_GetOrgGroupKendoPageDataResponse;
@class OrganizationServiceSvc_GetOrgGroupPageNumber;
@class OrganizationServiceSvc_GetOrgGroupPageNumberResponse;
@class OrganizationServiceSvc_DeleteOrgGroup;
@class OrganizationServiceSvc_DeleteOrgGroupResponse;
@class OrganizationServiceSvc_GetOrgGroupById;
@class OrganizationServiceSvc_GetOrgGroupByIdResponse;
@class OrganizationServiceSvc_UpdateOrgGroup;
@class OrganizationServiceSvc_UpdateOrgGroupResponse;
@class OrganizationServiceSvc_CreateOrgGroup;
@class OrganizationServiceSvc_CreateOrgGroupResponse;
@class OrganizationServiceSvc_GetAllOrgUnits;
@class OrganizationServiceSvc_GetAllOrgUnitsResponse;
@class OrganizationServiceSvc_DeleteOrgWorkLevel;
@class OrganizationServiceSvc_DeleteOrgWorkLevelResponse;
@class OrganizationServiceSvc_GetOrgWorkLevelById;
@class OrganizationServiceSvc_GetOrgWorkLevelByIdResponse;
@class OrganizationServiceSvc_CreateOrgWorkLevel;
@class OrganizationServiceSvc_CreateOrgWorkLevelResponse;
@class OrganizationServiceSvc_GetAllWorkLevelByOrgJobId;
@class OrganizationServiceSvc_GetAllWorkLevelByOrgJobIdResponse;
@class OrganizationServiceSvc_GetAllEmployeeByOrgWorkLevelId;
@class OrganizationServiceSvc_GetAllEmployeeByOrgWorkLevelIdResponse;
@class OrganizationServiceSvc_GetViewEmployeeByJobPositionId;
@class OrganizationServiceSvc_GetViewEmployeeByJobPositionIdResponse;
#import "org_tns1.h"
#import "org_tns2.h"
#import "org_tns3.h"
#import "org_tns4.h"
@interface OrganizationServiceSvc_GetEmployeeByListOrgUnitId : NSObject {
	
/* elements */
	org_tns1_ArrayOfint * orgUnitIds;
	NSNumber * CompanyId;
	NSNumber * skip;
	NSNumber * rows;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetEmployeeByListOrgUnitId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns1_ArrayOfint * orgUnitIds;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * rows;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetEmployeeByListOrgUnitIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_Employee * GetEmployeeByListOrgUnitIdResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetEmployeeByListOrgUnitIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_Employee * GetEmployeeByListOrgUnitIdResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_SearchOrgJob : NSObject {
	
/* elements */
	NSString * input;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_SearchOrgJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * input;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_SearchOrgJobResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgJob * SearchOrgJobResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_SearchOrgJobResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgJob * SearchOrgJobResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_SearchOrgJobPosition : NSObject {
	
/* elements */
	NSString * input;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_SearchOrgJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * input;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_SearchOrgJobPositionResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgJobPosition * SearchOrgJobPositionResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_SearchOrgJobPositionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgJobPosition * SearchOrgJobPositionResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_SearchOrgUnit : NSObject {
	
/* elements */
	NSString * input;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_SearchOrgUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * input;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_SearchOrgUnitResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgUnitTree * SearchOrgUnitResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_SearchOrgUnitResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgUnitTree * SearchOrgUnitResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllCLogRatings : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllCLogRatings *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllCLogRatingsResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfCLogRating * GetAllCLogRatingsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllCLogRatingsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfCLogRating * GetAllCLogRatingsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgProficencyTypes : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgProficencyTypes *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgProficencyTypesResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgProficencyType * GetOrgProficencyTypesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgProficencyTypesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgProficencyType * GetOrgProficencyTypesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllSpecificCompetencyByJobPositionId : NSObject {
	
/* elements */
	NSNumber * JobPositionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllSpecificCompetencyByJobPositionId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * JobPositionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllSpecificCompetencyByJobPositionIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgJobPositionSpecificCompetency * GetAllSpecificCompetencyByJobPositionIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllSpecificCompetencyByJobPositionIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgJobPositionSpecificCompetency * GetAllSpecificCompetencyByJobPositionIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgGroupCompetency : NSObject {
	
/* elements */
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgGroupCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgGroupCompetencyResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgCompetencyGroup * GetAllOrgGroupCompetencyResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgGroupCompetencyResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgCompetencyGroup * GetAllOrgGroupCompetencyResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllCompetencyByJobId : NSObject {
	
/* elements */
	NSNumber * OrgJobId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllCompetencyByJobId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgJobId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllCompetencyByJobIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgJobSpecificCompetency * GetAllCompetencyByJobIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllCompetencyByJobIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgJobSpecificCompetency * GetAllCompetencyByJobIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllJobSpecificCompetencyByCompId : NSObject {
	
/* elements */
	NSNumber * OrgCompetencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllJobSpecificCompetencyByCompId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgCompetencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllJobSpecificCompetencyByCompIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgJobSpecificCompetency * GetAllJobSpecificCompetencyByCompIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllJobSpecificCompetencyByCompIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgJobSpecificCompetency * GetAllJobSpecificCompetencyByCompIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllJobPosSpecificCompetencyByJobIdCompId : NSObject {
	
/* elements */
	NSNumber * OrgJobId;
	NSNumber * OrgCompetencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllJobPosSpecificCompetencyByJobIdCompId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgJobId;
@property (retain) NSNumber * OrgCompetencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllJobPosSpecificCompetencyByJobIdCompIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgJobPositionSpecificCompetency * GetAllJobPosSpecificCompetencyByJobIdCompIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllJobPosSpecificCompetencyByJobIdCompIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgJobPositionSpecificCompetency * GetAllJobPosSpecificCompetencyByJobIdCompIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllCompetencyLevelByCompetencyId : NSObject {
	
/* elements */
	NSNumber * OrgCompetencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllCompetencyLevelByCompetencyId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgCompetencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllCompetencyLevelByCompetencyIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgProficencyLevel * GetAllCompetencyLevelByCompetencyIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllCompetencyLevelByCompetencyIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgProficencyLevel * GetAllCompetencyLevelByCompetencyIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllCompetencyLevelDetailByLevelId : NSObject {
	
/* elements */
	NSNumber * OrgProficencyLevelId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllCompetencyLevelDetailByLevelId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgProficencyLevelId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllCompetencyLevelDetailByLevelIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgProficencyLevelDetail * GetAllCompetencyLevelDetailByLevelIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllCompetencyLevelDetailByLevelIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgProficencyLevelDetail * GetAllCompetencyLevelDetailByLevelIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllV_OrgSkillCompetencyBy : NSObject {
	
/* elements */
	NSNumber * OrgProficencyLevelId;
	NSNumber * OrgProficencyLevelDetailId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllV_OrgSkillCompetencyBy *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllV_OrgSkillCompetencyByResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgSkillCompetency * GetAllV_OrgSkillCompetencyByResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllV_OrgSkillCompetencyByResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgSkillCompetency * GetAllV_OrgSkillCompetencyByResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllV_OrgQualificationCompetencyBy : NSObject {
	
/* elements */
	NSNumber * OrgProficencyLevelId;
	NSNumber * OrgProficencyLevelDetailId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllV_OrgQualificationCompetencyBy *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllV_OrgQualificationCompetencyByResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgQualificationCompetency * GetAllV_OrgQualificationCompetencyByResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllV_OrgQualificationCompetencyByResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgQualificationCompetency * GetAllV_OrgQualificationCompetencyByResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllV_OrgExperienceCompetencyBy : NSObject {
	
/* elements */
	NSNumber * OrgProficencyLevelId;
	NSNumber * OrgProficencyLevelDetailId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllV_OrgExperienceCompetencyBy *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllV_OrgExperienceCompetencyByResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgExperienceCompetency * GetAllV_OrgExperienceCompetencyByResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllV_OrgExperienceCompetencyByResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgExperienceCompetency * GetAllV_OrgExperienceCompetencyByResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllV_OrgDegreeCompetencyBy : NSObject {
	
/* elements */
	NSNumber * OrgProficencyLevelId;
	NSNumber * OrgProficencyLevelDetailId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllV_OrgDegreeCompetencyBy *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllV_OrgDegreeCompetencyByResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgDegreeCompetency * GetAllV_OrgDegreeCompetencyByResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllV_OrgDegreeCompetencyByResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgDegreeCompetency * GetAllV_OrgDegreeCompetencyByResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllV_OrgAttitudeCompetencyBy : NSObject {
	
/* elements */
	NSNumber * OrgProficencyLevelId;
	NSNumber * OrgProficencyLevelDetailId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllV_OrgAttitudeCompetencyBy *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllV_OrgAttitudeCompetencyByResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgAttitudeCompetency * GetAllV_OrgAttitudeCompetencyByResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllV_OrgAttitudeCompetencyByResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgAttitudeCompetency * GetAllV_OrgAttitudeCompetencyByResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllV_OrgKnowledgeCompetencyBy : NSObject {
	
/* elements */
	NSNumber * OrgProficencyLevelId;
	NSNumber * OrgProficencyLevelDetailId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllV_OrgKnowledgeCompetencyBy *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllV_OrgKnowledgeCompetencyByResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgKnowledgeCompetency * GetAllV_OrgKnowledgeCompetencyByResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllV_OrgKnowledgeCompetencyByResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgKnowledgeCompetency * GetAllV_OrgKnowledgeCompetencyByResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllJobPosCompetency : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllJobPosCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllJobPosCompetencyResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgCompetency * GetAllJobPosCompetencyResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllJobPosCompetencyResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgCompetency * GetAllJobPosCompetencyResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllCoreCompetency : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllCoreCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllCoreCompetencyResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgCompetency * GetAllCoreCompetencyResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllCoreCompetencyResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgCompetency * GetAllCoreCompetencyResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllJobPosCompetencyByGroupId : NSObject {
	
/* elements */
	NSNumber * OrgCompetencyGroupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllJobPosCompetencyByGroupId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgCompetencyGroupId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllJobPosCompetencyByGroupIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgCompetency * GetAllJobPosCompetencyByGroupIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllJobPosCompetencyByGroupIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgCompetency * GetAllJobPosCompetencyByGroupIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllCoreCompetencyByGroupId : NSObject {
	
/* elements */
	NSNumber * OrgCompetencyGroupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllCoreCompetencyByGroupId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgCompetencyGroupId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllCoreCompetencyByGroupIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgCompetency * GetAllCoreCompetencyByGroupIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllCoreCompetencyByGroupIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgCompetency * GetAllCoreCompetencyByGroupIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetJobPosCompetencyById : NSObject {
	
/* elements */
	NSNumber * OrgCompetencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetJobPosCompetencyById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgCompetencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetJobPosCompetencyByIdResponse : NSObject {
	
/* elements */
	org_tns2_OrgCompetency * GetJobPosCompetencyByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetJobPosCompetencyByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgCompetency * GetJobPosCompetencyByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllJobPositionByJobId : NSObject {
	
/* elements */
	NSNumber * CLogCareerId;
	NSNumber * OrgJobId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllJobPositionByJobId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogCareerId;
@property (retain) NSNumber * OrgJobId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllJobPositionByJobIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgJobPosition * GetAllJobPositionByJobIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllJobPositionByJobIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgJobPosition * GetAllJobPositionByJobIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_EditJobPosCompetency : NSObject {
	
/* elements */
	org_tns2_OrgCompetency * competency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_EditJobPosCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgCompetency * competency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_EditJobPosCompetencyResponse : NSObject {
	
/* elements */
	NSNumber * EditJobPosCompetencyResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_EditJobPosCompetencyResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EditJobPosCompetencyResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetDirecReportById : NSObject {
	
/* elements */
	NSNumber * parentId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetDirecReportById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * parentId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetDirecReportByIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_ESS_ORG_DIRECT_REPORT * GetDirecReportByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetDirecReportByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_ESS_ORG_DIRECT_REPORT * GetDirecReportByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_FindLevelOfGenOfPSCd : NSObject {
	
/* elements */
	NSString * strGenPersonelStructCd;
	NSNumber * intUserId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_FindLevelOfGenOfPSCd *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * strGenPersonelStructCd;
@property (retain) NSNumber * intUserId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_FindLevelOfGenOfPSCdResponse : NSObject {
	
/* elements */
	NSNumber * FindLevelOfGenOfPSCdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_FindLevelOfGenOfPSCdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * FindLevelOfGenOfPSCdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_IsAscendant : NSObject {
	
/* elements */
	NSString * strCurrentGenPSCd;
	NSString * strTestingGenPSCd;
	NSNumber * intUserId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_IsAscendant *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * strCurrentGenPSCd;
@property (retain) NSString * strTestingGenPSCd;
@property (retain) NSNumber * intUserId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_IsAscendantResponse : NSObject {
	
/* elements */
	USBoolean * IsAscendantResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_IsAscendantResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * IsAscendantResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_IsDescendant : NSObject {
	
/* elements */
	NSString * strCurrentGenPSCd;
	NSString * strTestingGenPSCd;
	NSNumber * intUserId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_IsDescendant *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * strCurrentGenPSCd;
@property (retain) NSString * strTestingGenPSCd;
@property (retain) NSNumber * intUserId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_IsDescendantResponse : NSObject {
	
/* elements */
	USBoolean * IsDescendantResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_IsDescendantResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * IsDescendantResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_IsXthAscendant : NSObject {
	
/* elements */
	NSString * strCurrentGenPSCd;
	NSString * strTestingGenPSCd;
	NSNumber * intXDistance;
	NSNumber * intUserId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_IsXthAscendant *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * strCurrentGenPSCd;
@property (retain) NSString * strTestingGenPSCd;
@property (retain) NSNumber * intXDistance;
@property (retain) NSNumber * intUserId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_IsXthAscendantResponse : NSObject {
	
/* elements */
	USBoolean * IsXthAscendantResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_IsXthAscendantResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * IsXthAscendantResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_IsXthDescendant : NSObject {
	
/* elements */
	NSString * strCurrentGenPSCd;
	NSString * strTestingGenPSCd;
	NSNumber * intXDistance;
	NSNumber * intUserId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_IsXthDescendant *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * strCurrentGenPSCd;
@property (retain) NSString * strTestingGenPSCd;
@property (retain) NSNumber * intXDistance;
@property (retain) NSNumber * intUserId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_IsXthDescendantResponse : NSObject {
	
/* elements */
	USBoolean * IsXthDescendantResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_IsXthDescendantResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * IsXthDescendantResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetXAscendantFromGenPSCd : NSObject {
	
/* elements */
	NSString * strCurrentGenPSCd;
	NSNumber * intXDistance;
	NSNumber * intUserId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetXAscendantFromGenPSCd *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * strCurrentGenPSCd;
@property (retain) NSNumber * intXDistance;
@property (retain) NSNumber * intUserId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetXAscendantFromGenPSCdResponse : NSObject {
	
/* elements */
	org_tns2_TB_ESS_PERSONEL_STRUCT * GetXAscendantFromGenPSCdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetXAscendantFromGenPSCdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_TB_ESS_PERSONEL_STRUCT * GetXAscendantFromGenPSCdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetXDescendanceFromGenPSCd : NSObject {
	
/* elements */
	NSString * strCurrentGenPSCd;
	NSNumber * intXDistance;
	NSNumber * intUserId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetXDescendanceFromGenPSCd *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * strCurrentGenPSCd;
@property (retain) NSNumber * intXDistance;
@property (retain) NSNumber * intUserId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetXDescendanceFromGenPSCdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfTB_ESS_PERSONEL_STRUCT * GetXDescendanceFromGenPSCdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetXDescendanceFromGenPSCdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfTB_ESS_PERSONEL_STRUCT * GetXDescendanceFromGenPSCdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllGenPSHasTheSameLevel : NSObject {
	
/* elements */
	NSString * strCurrentGenPSCd;
	NSNumber * intUserId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllGenPSHasTheSameLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * strCurrentGenPSCd;
@property (retain) NSNumber * intUserId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllGenPSHasTheSameLevelResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfTB_ESS_PERSONEL_STRUCT * GetAllGenPSHasTheSameLevelResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllGenPSHasTheSameLevelResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfTB_ESS_PERSONEL_STRUCT * GetAllGenPSHasTheSameLevelResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllGenPSHasTheSameLevelAndParentNode : NSObject {
	
/* elements */
	NSString * strCurrentGenPSCd;
	NSNumber * intUserId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllGenPSHasTheSameLevelAndParentNode *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * strCurrentGenPSCd;
@property (retain) NSNumber * intUserId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllGenPSHasTheSameLevelAndParentNodeResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfTB_ESS_PERSONEL_STRUCT * GetAllGenPSHasTheSameLevelAndParentNodeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllGenPSHasTheSameLevelAndParentNodeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfTB_ESS_PERSONEL_STRUCT * GetAllGenPSHasTheSameLevelAndParentNodeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetListOfPersonelStruct : NSObject {
	
/* elements */
	NSNumber * intUserId;
	USBoolean * isLimited;
	NSNumber * intMaxDepth;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetListOfPersonelStruct *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * intUserId;
@property (retain) USBoolean * isLimited;
@property (retain) NSNumber * intMaxDepth;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetListOfPersonelStructResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfTB_ESS_PERSONEL_STRUCT * GetListOfPersonelStructResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetListOfPersonelStructResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfTB_ESS_PERSONEL_STRUCT * GetListOfPersonelStructResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetListOfEmployeeMappingPersonelStruct : NSObject {
	
/* elements */
	org_tns1_ArrayOfint * PersonelStructIds;
	NSNumber * intUserId;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetListOfEmployeeMappingPersonelStruct *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns1_ArrayOfint * PersonelStructIds;
@property (retain) NSNumber * intUserId;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetListOfEmployeeMappingPersonelStructResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_ESS_PER_STRUCT_EMP_MAPPING * GetListOfEmployeeMappingPersonelStructResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetListOfEmployeeMappingPersonelStructResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_ESS_PER_STRUCT_EMP_MAPPING * GetListOfEmployeeMappingPersonelStructResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllTreeOrgJobTitle : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * UserId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllTreeOrgJobTitle *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * UserId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllTreeOrgJobTitleResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_ORG_JOB_TITLE_TREE * GetAllTreeOrgJobTitleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllTreeOrgJobTitleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_ORG_JOB_TITLE_TREE * GetAllTreeOrgJobTitleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetViewOrgJobTitleByCode : NSObject {
	
/* elements */
	NSString * code;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetViewOrgJobTitleByCode *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * code;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetViewOrgJobTitleByCodeResponse : NSObject {
	
/* elements */
	org_tns2_V_ORG_JOB_TITLE_AND_GROUP * GetViewOrgJobTitleByCodeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetViewOrgJobTitleByCodeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_V_ORG_JOB_TITLE_AND_GROUP * GetViewOrgJobTitleByCodeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetListOrgJobTitleGroups : NSObject {
	
/* elements */
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetListOrgJobTitleGroups *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetListOrgJobTitleGroupsResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_ORG_JOB_TITLE_GROUP * GetListOrgJobTitleGroupsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetListOrgJobTitleGroupsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_ORG_JOB_TITLE_GROUP * GetListOrgJobTitleGroupsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetListOrgJobTitleLevels : NSObject {
	
/* elements */
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetListOrgJobTitleLevels *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetListOrgJobTitleLevelsResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_ORG_JOB_TITLE_LEVEL * GetListOrgJobTitleLevelsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetListOrgJobTitleLevelsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_ORG_JOB_TITLE_LEVEL * GetListOrgJobTitleLevelsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_AddOrgJobTitle : NSObject {
	
/* elements */
	org_tns2_V_ORG_JOB_TITLE_AND_GROUP * viewOrgJobTitle;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_AddOrgJobTitle *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_V_ORG_JOB_TITLE_AND_GROUP * viewOrgJobTitle;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_AddOrgJobTitleResponse : NSObject {
	
/* elements */
	org_tns3_MsgNotifyModel * AddOrgJobTitleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_AddOrgJobTitleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns3_MsgNotifyModel * AddOrgJobTitleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_UpdateOrgJobTitle : NSObject {
	
/* elements */
	org_tns2_V_ORG_JOB_TITLE_AND_GROUP * viewOrgJobTitle;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_UpdateOrgJobTitle *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_V_ORG_JOB_TITLE_AND_GROUP * viewOrgJobTitle;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_UpdateOrgJobTitleResponse : NSObject {
	
/* elements */
	USBoolean * UpdateOrgJobTitleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_UpdateOrgJobTitleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateOrgJobTitleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_DeleteOrgJobTitle : NSObject {
	
/* elements */
	NSString * orgJobTitleCode;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_DeleteOrgJobTitle *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * orgJobTitleCode;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_DeleteOrgJobTitleResponse : NSObject {
	
/* elements */
	USBoolean * DeleteOrgJobTitleResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_DeleteOrgJobTitleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteOrgJobTitleResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetListAllOrgJobTitles : NSObject {
	
/* elements */
	NSNumber * companyId;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetListAllOrgJobTitles *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetListAllOrgJobTitlesResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_ORG_JOB_TITLE_AND_GROUP * GetListAllOrgJobTitlesResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetListAllOrgJobTitlesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_ORG_JOB_TITLE_AND_GROUP * GetListAllOrgJobTitlesResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_SearchOrgJobTitle : NSObject {
	
/* elements */
	NSString * input;
	NSNumber * companyId;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_SearchOrgJobTitle *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * input;
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_SearchOrgJobTitleResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_ORG_JOB_TITLE_AND_GROUP * SearchOrgJobTitleResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_SearchOrgJobTitleResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_ORG_JOB_TITLE_AND_GROUP * SearchOrgJobTitleResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetListViewEmployeeByOrgJobTitleCode : NSObject {
	
/* elements */
	org_tns1_ArrayOfstring * orgJobTitleCode;
	NSNumber * companyId;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetListViewEmployeeByOrgJobTitleCode *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns1_ArrayOfstring * orgJobTitleCode;
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetListViewEmployeeByOrgJobTitleCodeResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_Employee * GetListViewEmployeeByOrgJobTitleCodeResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetListViewEmployeeByOrgJobTitleCodeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_Employee * GetListViewEmployeeByOrgJobTitleCodeResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllClassificationEmpTree : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * intUserId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllClassificationEmpTree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * intUserId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllClassificationEmpTreeResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_ESS_TREE_OF_EMP_TYPE * GetAllClassificationEmpTreeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllClassificationEmpTreeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_ESS_TREE_OF_EMP_TYPE * GetAllClassificationEmpTreeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllClassificationEmpMapping : NSObject {
	
/* elements */
	NSString * uniqueId;
	NSString * Code;
	NSNumber * CompanyId;
	NSNumber * intUserId;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllClassificationEmpMapping *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * uniqueId;
@property (retain) NSString * Code;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * intUserId;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllClassificationEmpMappingResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_ESS_EMP_GROUP_MAPPING * GetAllClassificationEmpMappingResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllClassificationEmpMappingResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_ESS_EMP_GROUP_MAPPING * GetAllClassificationEmpMappingResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgChangeInfos : NSObject {
	
/* elements */
	NSNumber * top;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgChangeInfos *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * top;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgChangeInfosResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_ORG_CHANGE_INFO * GetOrgChangeInfosResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgChangeInfosResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_ORG_CHANGE_INFO * GetOrgChangeInfosResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllChildrenReportEmployeeByParentId : NSObject {
	
/* elements */
	NSNumber * parentId;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllChildrenReportEmployeeByParentId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * parentId;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllChildrenReportEmployeeByParentIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_ESS_ORG_DIRECT_REPORT * GetAllChildrenReportEmployeeByParentIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllChildrenReportEmployeeByParentIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_ESS_ORG_DIRECT_REPORT * GetAllChildrenReportEmployeeByParentIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllDescendantsReportEmployeeByParentId : NSObject {
	
/* elements */
	NSNumber * parentId;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllDescendantsReportEmployeeByParentId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * parentId;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllDescendantsReportEmployeeByParentIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_ESS_ORG_DIRECT_REPORT * GetAllDescendantsReportEmployeeByParentIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllDescendantsReportEmployeeByParentIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_ESS_ORG_DIRECT_REPORT * GetAllDescendantsReportEmployeeByParentIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetReportEmployeeRoot : NSObject {
	
/* elements */
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetReportEmployeeRoot *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetReportEmployeeRootResponse : NSObject {
	
/* elements */
	org_tns2_V_ESS_ORG_DIRECT_REPORT * GetReportEmployeeRootResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetReportEmployeeRootResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_V_ESS_ORG_DIRECT_REPORT * GetReportEmployeeRootResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllUnits : NSObject {
	
/* elements */
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllUnits *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllUnitsResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgUnit * GetAllUnitsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllUnitsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgUnit * GetAllUnitsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllChildrenOrgUnitByParentId : NSObject {
	
/* elements */
	NSNumber * parentId;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllChildrenOrgUnitByParentId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * parentId;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllChildrenOrgUnitByParentIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgUnitTree * GetAllChildrenOrgUnitByParentIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllChildrenOrgUnitByParentIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgUnitTree * GetAllChildrenOrgUnitByParentIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllDescendantsOrgUnitByParentId : NSObject {
	
/* elements */
	NSNumber * parentId;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllDescendantsOrgUnitByParentId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * parentId;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllDescendantsOrgUnitByParentIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgUnitTree * GetAllDescendantsOrgUnitByParentIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllDescendantsOrgUnitByParentIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgUnitTree * GetAllDescendantsOrgUnitByParentIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgUnitRoot : NSObject {
	
/* elements */
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgUnitRoot *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgUnitRootResponse : NSObject {
	
/* elements */
	org_tns2_V_OrgUnitTree * GetOrgUnitRootResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgUnitRootResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_V_OrgUnitTree * GetOrgUnitRootResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetUnitLeader : NSObject {
	
/* elements */
	NSNumber * UnitId;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetUnitLeader *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * UnitId;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetUnitLeaderResponse : NSObject {
	
/* elements */
	org_tns2_V_EmpProfileJobPosition * GetUnitLeaderResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetUnitLeaderResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_V_EmpProfileJobPosition * GetUnitLeaderResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllUnitsPosition : NSObject {
	
/* elements */
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllUnitsPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllUnitsPositionResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgUnitJobPosition * GetAllUnitsPositionResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllUnitsPositionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgUnitJobPosition * GetAllUnitsPositionResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetViewOrgUnitByOrgUnitId : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetViewOrgUnitByOrgUnitId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetViewOrgUnitByOrgUnitIdResponse : NSObject {
	
/* elements */
	org_tns2_V_OrgUnit * GetViewOrgUnitByOrgUnitIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetViewOrgUnitByOrgUnitIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_V_OrgUnit * GetViewOrgUnitByOrgUnitIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgUnitJobsByOrgUnitId : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgUnitJobsByOrgUnitId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgUnitJobsByOrgUnitIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgUnitJob * GetAllOrgUnitJobsByOrgUnitIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgUnitJobsByOrgUnitIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgUnitJob * GetAllOrgUnitJobsByOrgUnitIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetEmployeeKendoFilterByJobId : NSObject {
	
/* elements */
	NSNumber * jobId;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetEmployeeKendoFilterByJobId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * jobId;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetEmployeeKendoFilterByJobIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_Employee * GetEmployeeKendoFilterByJobIdResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetEmployeeKendoFilterByJobIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_Employee * GetEmployeeKendoFilterByJobIdResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgJobByPage : NSObject {
	
/* elements */
	NSNumber * skip;
	NSNumber * take;
	NSNumber * companyId;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgJobByPage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgJobByPageResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgJob * GetAllOrgJobByPageResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgJobByPageResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgJob * GetAllOrgJobByPageResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetViewOrgUnitJobByOrgUnitId : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetViewOrgUnitJobByOrgUnitId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetViewOrgUnitJobByOrgUnitIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgUnitJob * GetViewOrgUnitJobByOrgUnitIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetViewOrgUnitJobByOrgUnitIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgUnitJob * GetViewOrgUnitJobByOrgUnitIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_CreateOrgUnitJob : NSObject {
	
/* elements */
	org_tns2_OrgUnitJob * orgUnitJob;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_CreateOrgUnitJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgUnitJob * orgUnitJob;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_CreateOrgUnitJobResponse : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_CreateOrgUnitJobResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgUnitJobs : NSObject {
	
/* elements */
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgUnitJobs *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgUnitJobsResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgUnitJob * GetAllOrgUnitJobsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgUnitJobsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgUnitJob * GetAllOrgUnitJobsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgUnitsByPage : NSObject {
	
/* elements */
	NSNumber * _companyId;
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgUnitsByPage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * _companyId;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgUnitsByPageResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgUnitTree * GetOrgUnitsByPageResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgUnitsByPageResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgUnitTree * GetOrgUnitsByPageResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgUnitJobByOrgUnitId : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgUnitJobByOrgUnitId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgUnitJobByOrgUnitIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgUnitJob * GetAllOrgUnitJobByOrgUnitIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgUnitJobByOrgUnitIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgUnitJob * GetAllOrgUnitJobByOrgUnitIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgUnitTreeByOrgUnitId : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgUnitTreeByOrgUnitId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgUnitTreeByOrgUnitIdResponse : NSObject {
	
/* elements */
	org_tns2_V_OrgUnitTree * GetOrgUnitTreeByOrgUnitIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgUnitTreeByOrgUnitIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_V_OrgUnitTree * GetOrgUnitTreeByOrgUnitIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgUnitByOrgUnitId : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgUnitByOrgUnitId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgUnitByOrgUnitIdResponse : NSObject {
	
/* elements */
	org_tns2_OrgUnit * GetOrgUnitByOrgUnitIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgUnitByOrgUnitIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgUnit * GetOrgUnitByOrgUnitIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgUnitTree : NSObject {
	
/* elements */
	NSNumber * companyId;
	NSNumber * userId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgUnitTree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * userId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgUnitTreeResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgUnitTree * GetAllOrgUnitTreeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgUnitTreeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgUnitTree * GetAllOrgUnitTreeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgUnitTreeByPermission : NSObject {
	
/* elements */
	NSNumber * companyId;
	NSNumber * userId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgUnitTreeByPermission *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * companyId;
@property (retain) NSNumber * userId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgUnitTreeByPermissionResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfSP_ORG_UNIT_TREE_BY_USER_Result * GetAllOrgUnitTreeByPermissionResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgUnitTreeByPermissionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfSP_ORG_UNIT_TREE_BY_USER_Result * GetAllOrgUnitTreeByPermissionResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_EditOrgUnit : NSObject {
	
/* elements */
	org_tns2_OrgUnit * unit;
	org_tns2_ArrayOfV_OrgUnitJob * unitJobs;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_EditOrgUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgUnit * unit;
@property (retain) org_tns2_ArrayOfV_OrgUnitJob * unitJobs;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_EditOrgUnitResponse : NSObject {
	
/* elements */
	NSNumber * EditOrgUnitResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_EditOrgUnitResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * EditOrgUnitResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_TransferOrgUnit : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
	NSNumber * newOrgUnitParentId;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_TransferOrgUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
@property (retain) NSNumber * newOrgUnitParentId;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_TransferOrgUnitResponse : NSObject {
	
/* elements */
	NSNumber * TransferOrgUnitResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_TransferOrgUnitResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * TransferOrgUnitResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_DeleteOrgUnit : NSObject {
	
/* elements */
	NSNumber * OrgUnitId;
	NSString * OrgUnitCode;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_DeleteOrgUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_DeleteOrgUnitResponse : NSObject {
	
/* elements */
	NSNumber * DeleteOrgUnitResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_DeleteOrgUnitResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * DeleteOrgUnitResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_DeleteOrgUnitJob : NSObject {
	
/* elements */
	NSNumber * OrgUnitJobId;
	NSNumber * CompanyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_DeleteOrgUnitJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgUnitJobId;
@property (retain) NSNumber * CompanyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_DeleteOrgUnitJobResponse : NSObject {
	
/* elements */
	NSNumber * DeleteOrgUnitJobResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_DeleteOrgUnitJobResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * DeleteOrgUnitJobResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgSkillByOrgSkillId : NSObject {
	
/* elements */
	NSNumber * orgSkillId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgSkillByOrgSkillId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgSkillId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgSkillByOrgSkillIdResponse : NSObject {
	
/* elements */
	org_tns2_OrgSkill * GetOrgSkillByOrgSkillIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgSkillByOrgSkillIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgSkill * GetOrgSkillByOrgSkillIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgSkillsBySkillType : NSObject {
	
/* elements */
	NSNumber * OrgSkillTypeId;
	NSNumber * CompanyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgSkillsBySkillType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgSkillTypeId;
@property (retain) NSNumber * CompanyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgSkillsBySkillTypeResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgSkill * GetAllOrgSkillsBySkillTypeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgSkillsBySkillTypeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgSkill * GetAllOrgSkillsBySkillTypeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgSkill : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgSkillResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgSkill * GetAllOrgSkillResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgSkillResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgSkill * GetAllOrgSkillResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgSkillByPage : NSObject {
	
/* elements */
	NSNumber * pageIndex;
	NSNumber * pageSize;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgSkillByPage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * pageIndex;
@property (retain) NSNumber * pageSize;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgSkillByPageResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgSkill * GetOrgSkillByPageResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgSkillByPageResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgSkill * GetOrgSkillByPageResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgSkillKendoPageData : NSObject {
	
/* elements */
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgSkillKendoPageData *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgSkillKendoPageDataResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgSkill * GetOrgSkillKendoPageDataResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgSkillKendoPageDataResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgSkill * GetOrgSkillKendoPageDataResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgSkillPageNumber : NSObject {
	
/* elements */
	NSNumber * pageSize;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgSkillPageNumber *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * pageSize;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgSkillPageNumberResponse : NSObject {
	
/* elements */
	NSNumber * GetOrgSkillPageNumberResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgSkillPageNumberResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetOrgSkillPageNumberResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_DeleteOrgSkill : NSObject {
	
/* elements */
	NSNumber * orgSkillId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_DeleteOrgSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgSkillId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_DeleteOrgSkillResponse : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_DeleteOrgSkillResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgSkillById : NSObject {
	
/* elements */
	NSNumber * orgSkillId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgSkillById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgSkillId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgSkillByIdResponse : NSObject {
	
/* elements */
	org_tns2_OrgSkill * GetOrgSkillByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgSkillByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgSkill * GetOrgSkillByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_UpdateOrgSkill : NSObject {
	
/* elements */
	org_tns2_OrgSkill * orgSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_UpdateOrgSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgSkill * orgSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_UpdateOrgSkillResponse : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_UpdateOrgSkillResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_CreateOrgSkill : NSObject {
	
/* elements */
	org_tns2_OrgSkill * orgSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_CreateOrgSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgSkill * orgSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_CreateOrgSkillResponse : NSObject {
	
/* elements */
	org_tns2_OrgSkill * CreateOrgSkillResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_CreateOrgSkillResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgSkill * CreateOrgSkillResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgTimeInCharge : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgTimeInCharge *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgTimeInChargeResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgTimeInCharge * GetAllOrgTimeInChargeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgTimeInChargeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgTimeInCharge * GetAllOrgTimeInChargeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgSkillType : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgSkillType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgSkillTypeResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgSkillType * GetAllOrgSkillTypeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgSkillTypeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgSkillType * GetAllOrgSkillTypeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgSkillTypeByOrgSkillTypeId : NSObject {
	
/* elements */
	NSNumber * orgSkillTypeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgSkillTypeByOrgSkillTypeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgSkillTypeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgSkillTypeByOrgSkillTypeIdResponse : NSObject {
	
/* elements */
	org_tns2_OrgSkillType * GetOrgSkillTypeByOrgSkillTypeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgSkillTypeByOrgSkillTypeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgSkillType * GetOrgSkillTypeByOrgSkillTypeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgProjectType : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgProjectType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgProjectTypeResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgProjectType * GetAllOrgProjectTypeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgProjectTypeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgProjectType * GetAllOrgProjectTypeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgProjectTypeByOrgProjectTypeId : NSObject {
	
/* elements */
	NSNumber * orgProjectTypeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgProjectTypeByOrgProjectTypeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgProjectTypeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgProjectTypeByOrgProjectTypeIdResponse : NSObject {
	
/* elements */
	org_tns2_OrgProjectType * GetOrgProjectTypeByOrgProjectTypeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgProjectTypeByOrgProjectTypeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgProjectType * GetOrgProjectTypeByOrgProjectTypeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllCLogCareer : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllCLogCareer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllCLogCareerResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_ESS_CLOG_CAREER * GetAllCLogCareerResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllCLogCareerResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_ESS_CLOG_CAREER * GetAllCLogCareerResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllEmployeeByCareerCode : NSObject {
	
/* elements */
	NSString * CareerCode;
	NSNumber * take;
	NSNumber * skip;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllEmployeeByCareerCode *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CareerCode;
@property (retain) NSNumber * take;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllEmployeeByCareerCodeResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_Employee * GetAllEmployeeByCareerCodeResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllEmployeeByCareerCodeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_Employee * GetAllEmployeeByCareerCodeResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllTimeInCharge : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllTimeInCharge *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllTimeInChargeResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgTimeInCharge * GetAllTimeInChargeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllTimeInChargeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgTimeInCharge * GetAllTimeInChargeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgDegree : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgDegreeResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgDegree * GetAllOrgDegreeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgDegreeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgDegree * GetAllOrgDegreeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgDegreeByOrgDegreeId : NSObject {
	
/* elements */
	NSNumber * orgDegreeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgDegreeByOrgDegreeId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgDegreeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgDegreeByOrgDegreeIdResponse : NSObject {
	
/* elements */
	org_tns2_OrgDegree * GetOrgDegreeByOrgDegreeIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgDegreeByOrgDegreeIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgDegree * GetOrgDegreeByOrgDegreeIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgDegreeRank : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgDegreeRank *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgDegreeRankResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgDegreeRank * GetAllOrgDegreeRankResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgDegreeRankResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgDegreeRank * GetAllOrgDegreeRankResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgDegreeRankByOrgDegreeRankId : NSObject {
	
/* elements */
	NSNumber * orgDegreeRankId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgDegreeRankByOrgDegreeRankId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgDegreeRankId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgDegreeRankByOrgDegreeRankIdResponse : NSObject {
	
/* elements */
	org_tns2_OrgDegreeRank * GetOrgDegreeRankByOrgDegreeRankIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgDegreeRankByOrgDegreeRankIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgDegreeRank * GetOrgDegreeRankByOrgDegreeRankIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgJobs : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgJobs *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgJobsResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgJob * GetAllOrgJobsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgJobsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgJob * GetAllOrgJobsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetViewOrgJobs : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetViewOrgJobs *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetViewOrgJobsResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgJob * GetViewOrgJobsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetViewOrgJobsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgJob * GetViewOrgJobsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetViewOrgJobById : NSObject {
	
/* elements */
	NSNumber * orgJobId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetViewOrgJobById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgJobId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetViewOrgJobByIdResponse : NSObject {
	
/* elements */
	org_tns2_V_OrgJob * GetViewOrgJobByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetViewOrgJobByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_V_OrgJob * GetViewOrgJobByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllViewOrgJobByCareerId : NSObject {
	
/* elements */
	NSNumber * CLogCareerId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllViewOrgJobByCareerId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogCareerId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllViewOrgJobByCareerIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgJob * GetAllViewOrgJobByCareerIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllViewOrgJobByCareerIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgJob * GetAllViewOrgJobByCareerIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgJobByCareer : NSObject {
	
/* elements */
	NSString * CLogCareerCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgJobByCareer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogCareerCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgJobByCareerResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgJob * GetAllOrgJobByCareerResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgJobByCareerResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgJob * GetAllOrgJobByCareerResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgJobById : NSObject {
	
/* elements */
	NSNumber * orgJobId;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgJobById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgJobId;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgJobByIdResponse : NSObject {
	
/* elements */
	org_tns2_OrgJob * GetOrgJobByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgJobByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgJob * GetOrgJobByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgJobByCode : NSObject {
	
/* elements */
	NSString * code;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgJobByCode *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * code;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgJobByCodeResponse : NSObject {
	
/* elements */
	org_tns2_OrgJob * GetOrgJobByCodeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgJobByCodeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgJob * GetOrgJobByCodeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgJobDescriptionDetailForJob : NSObject {
	
/* elements */
	NSNumber * orgJobId;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgJobDescriptionDetailForJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgJobId;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgJobDescriptionDetailForJobResponse : NSObject {
	
/* elements */
	org_tns2_V_ESS_ORG_JOB_TASK_DESC * GetOrgJobDescriptionDetailForJobResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgJobDescriptionDetailForJobResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_V_ESS_ORG_JOB_TASK_DESC * GetOrgJobDescriptionDetailForJobResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgJobDescriptionDetailForJobPosition : NSObject {
	
/* elements */
	NSNumber * orgJobPositionId;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgJobDescriptionDetailForJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgJobPositionId;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgJobDescriptionDetailForJobPositionResponse : NSObject {
	
/* elements */
	org_tns2_V_ESS_ORG_POSITION_TASK_DESC * GetOrgJobDescriptionDetailForJobPositionResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgJobDescriptionDetailForJobPositionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_V_ESS_ORG_POSITION_TASK_DESC * GetOrgJobDescriptionDetailForJobPositionResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_CreateNewOrgJob : NSObject {
	
/* elements */
	org_tns2_OrgJob * orgJob;
	org_tns2_ArrayOfOrgWorkLevel * workLevels;
	NSString * jobDescriptions;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_CreateNewOrgJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgJob * orgJob;
@property (retain) org_tns2_ArrayOfOrgWorkLevel * workLevels;
@property (retain) NSString * jobDescriptions;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_CreateNewOrgJobResponse : NSObject {
	
/* elements */
	org_tns3_MsgNotifyModel * CreateNewOrgJobResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_CreateNewOrgJobResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns3_MsgNotifyModel * CreateNewOrgJobResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_UpdateOrgJob : NSObject {
	
/* elements */
	org_tns2_OrgJob * orgJob;
	org_tns2_ArrayOfOrgWorkLevel * workLevels;
	NSString * jobDescriptions;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_UpdateOrgJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgJob * orgJob;
@property (retain) org_tns2_ArrayOfOrgWorkLevel * workLevels;
@property (retain) NSString * jobDescriptions;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_UpdateOrgJobResponse : NSObject {
	
/* elements */
	org_tns3_MsgNotifyModel * UpdateOrgJobResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_UpdateOrgJobResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns3_MsgNotifyModel * UpdateOrgJobResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_DeleteOrgJob : NSObject {
	
/* elements */
	NSNumber * orgJobId;
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_DeleteOrgJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgJobId;
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_DeleteOrgJobResponse : NSObject {
	
/* elements */
	org_tns3_MsgNotifyModel * DeleteOrgJobResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_DeleteOrgJobResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns3_MsgNotifyModel * DeleteOrgJobResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllTreeOrgCareerJobAndWorkLevel : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * intUserId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllTreeOrgCareerJobAndWorkLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * intUserId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllTreeOrgCareerJobAndWorkLevelResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_ORG_CAREER_JOB_WLEVEL_TREE * GetAllTreeOrgCareerJobAndWorkLevelResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllTreeOrgCareerJobAndWorkLevelResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_ORG_CAREER_JOB_WLEVEL_TREE * GetAllTreeOrgCareerJobAndWorkLevelResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllEmployeeByOrgJobId : NSObject {
	
/* elements */
	NSNumber * OrgJobId;
	NSNumber * take;
	NSNumber * skip;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllEmployeeByOrgJobId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgJobId;
@property (retain) NSNumber * take;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllEmployeeByOrgJobIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_Employee * GetAllEmployeeByOrgJobIdResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllEmployeeByOrgJobIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_Employee * GetAllEmployeeByOrgJobIdResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgJobPositions : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgJobPositions *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgJobPositionsResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgJobPosition * GetAllOrgJobPositionsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgJobPositionsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgJobPosition * GetAllOrgJobPositionsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetListOrgJobPosByOrgJobId : NSObject {
	
/* elements */
	NSNumber * orgJobId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetListOrgJobPosByOrgJobId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgJobId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetListOrgJobPosByOrgJobIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgJobPosition * GetListOrgJobPosByOrgJobIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetListOrgJobPosByOrgJobIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgJobPosition * GetListOrgJobPosByOrgJobIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgJobPosByOrgUnitId : NSObject {
	
/* elements */
	NSNumber * orgUnitId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgJobPosByOrgUnitId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgUnitId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgJobPosByOrgUnitIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgJobPosition * GetAllOrgJobPosByOrgUnitIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgJobPosByOrgUnitIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgJobPosition * GetAllOrgJobPosByOrgUnitIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllJobPositionByOrgUnitId : NSObject {
	
/* elements */
	NSNumber * OrgUnitId;
	NSNumber * take;
	NSNumber * skip;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllJobPositionByOrgUnitId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * take;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllJobPositionByOrgUnitIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgJobPosition * GetAllJobPositionByOrgUnitIdResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllJobPositionByOrgUnitIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgJobPosition * GetAllJobPositionByOrgUnitIdResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllJobPositionByOrgJobId : NSObject {
	
/* elements */
	NSNumber * OrgJobId;
	NSNumber * take;
	NSNumber * skip;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllJobPositionByOrgJobId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgJobId;
@property (retain) NSNumber * take;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllJobPositionByOrgJobIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgJobPosition * GetAllJobPositionByOrgJobIdResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllJobPositionByOrgJobIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgJobPosition * GetAllJobPositionByOrgJobIdResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllJobPositionByWorkLevelId : NSObject {
	
/* elements */
	NSNumber * WorkLevelId;
	NSNumber * take;
	NSNumber * skip;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllJobPositionByWorkLevelId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * WorkLevelId;
@property (retain) NSNumber * take;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllJobPositionByWorkLevelIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgJobPosition * GetAllJobPositionByWorkLevelIdResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllJobPositionByWorkLevelIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgJobPosition * GetAllJobPositionByWorkLevelIdResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllJobPositionByJobTitleId : NSObject {
	
/* elements */
	NSNumber * JobTitleId;
	NSNumber * take;
	NSNumber * skip;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllJobPositionByJobTitleId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * JobTitleId;
@property (retain) NSNumber * take;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllJobPositionByJobTitleIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgJobPosition * GetAllJobPositionByJobTitleIdResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllJobPositionByJobTitleIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgJobPosition * GetAllJobPositionByJobTitleIdResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllViewOrgJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * take;
	NSNumber * skip;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllViewOrgJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * take;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllViewOrgJobPositionResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgJobPosition * GetAllViewOrgJobPositionResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllViewOrgJobPositionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgJobPosition * GetAllViewOrgJobPositionResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllEmployeeByPositionId : NSObject {
	
/* elements */
	NSNumber * OrgJobPositionId;
	NSNumber * take;
	NSNumber * skip;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllEmployeeByPositionId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * take;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllEmployeeByPositionIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_Employee * GetAllEmployeeByPositionIdResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllEmployeeByPositionIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_Employee * GetAllEmployeeByPositionIdResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_AddOrgJobPosition : NSObject {
	
/* elements */
	org_tns2_V_OrgJobPositionDetail * tableOrgJobPosition;
	NSString * jobDescriptions;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_AddOrgJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_V_OrgJobPositionDetail * tableOrgJobPosition;
@property (retain) NSString * jobDescriptions;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_AddOrgJobPositionResponse : NSObject {
	
/* elements */
	org_tns3_MsgNotifyModel * AddOrgJobPositionResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_AddOrgJobPositionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns3_MsgNotifyModel * AddOrgJobPositionResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_UpdateOrgJobPosition : NSObject {
	
/* elements */
	org_tns2_V_OrgJobPositionDetail * tableOrgJobPosition;
	NSString * jobDescriptions;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_UpdateOrgJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_V_OrgJobPositionDetail * tableOrgJobPosition;
@property (retain) NSString * jobDescriptions;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_UpdateOrgJobPositionResponse : NSObject {
	
/* elements */
	org_tns3_MsgNotifyModel * UpdateOrgJobPositionResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_UpdateOrgJobPositionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns3_MsgNotifyModel * UpdateOrgJobPositionResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_DeleteOrgJobPosition : NSObject {
	
/* elements */
	NSNumber * OrgJobPositionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_DeleteOrgJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgJobPositionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_DeleteOrgJobPositionResponse : NSObject {
	
/* elements */
	org_tns3_MsgNotifyModel * DeleteOrgJobPositionResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_DeleteOrgJobPositionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns3_MsgNotifyModel * DeleteOrgJobPositionResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrganizationJobPosition : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrganizationJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrganizationJobPositionResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgJobPosition * GetAllOrganizationJobPositionResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrganizationJobPositionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgJobPosition * GetAllOrganizationJobPositionResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgJobPositionById : NSObject {
	
/* elements */
	NSNumber * orgJobPositionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgJobPositionById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgJobPositionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgJobPositionByIdResponse : NSObject {
	
/* elements */
	org_tns2_OrgJobPosition * GetOrgJobPositionByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgJobPositionByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgJobPosition * GetOrgJobPositionByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetViewOrgJobPositionDetailById : NSObject {
	
/* elements */
	NSNumber * orgJobPositionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetViewOrgJobPositionDetailById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgJobPositionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetViewOrgJobPositionDetailByIdResponse : NSObject {
	
/* elements */
	org_tns2_V_OrgJobPositionDetail * GetViewOrgJobPositionDetailByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetViewOrgJobPositionDetailByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_V_OrgJobPositionDetail * GetViewOrgJobPositionDetailByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetViewOrgJobPositionById : NSObject {
	
/* elements */
	NSNumber * orgJobPositionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetViewOrgJobPositionById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgJobPositionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetViewOrgJobPositionByIdResponse : NSObject {
	
/* elements */
	org_tns2_V_OrgJobPosition * GetViewOrgJobPositionByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetViewOrgJobPositionByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_V_OrgJobPosition * GetViewOrgJobPositionByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgExperiences : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgExperiences *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgExperiencesResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgExperience * GetAllOrgExperiencesResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgExperiencesResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgExperience * GetAllOrgExperiencesResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_CreateOrgExperience : NSObject {
	
/* elements */
	org_tns2_OrgExperience * orgExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_CreateOrgExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgExperience * orgExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_CreateOrgExperienceResponse : NSObject {
	
/* elements */
	org_tns2_OrgExperience * CreateOrgExperienceResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_CreateOrgExperienceResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgExperience * CreateOrgExperienceResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgExperience : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgExperienceResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgExperience * GetAllOrgExperienceResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgExperienceResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgExperience * GetAllOrgExperienceResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgExperienceByOrgExperienceId : NSObject {
	
/* elements */
	NSNumber * orgExperienceId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgExperienceByOrgExperienceId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgExperienceId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgExperienceByOrgExperienceIdResponse : NSObject {
	
/* elements */
	org_tns2_OrgExperience * GetOrgExperienceByOrgExperienceIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgExperienceByOrgExperienceIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgExperience * GetOrgExperienceByOrgExperienceIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgExperienceByProjectTypeAndTimeInCharge : NSObject {
	
/* elements */
	NSNumber * OrgProjectTypeId;
	NSNumber * OrgTimeInChargeId;
	NSNumber * CompanyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgExperienceByProjectTypeAndTimeInCharge *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgProjectTypeId;
@property (retain) NSNumber * OrgTimeInChargeId;
@property (retain) NSNumber * CompanyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgExperienceByProjectTypeAndTimeInChargeResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgExperience * GetOrgExperienceByProjectTypeAndTimeInChargeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgExperienceByProjectTypeAndTimeInChargeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgExperience * GetOrgExperienceByProjectTypeAndTimeInChargeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgQualifications : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgQualifications *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgQualificationsResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgQualification * GetAllOrgQualificationsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgQualificationsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgQualification * GetAllOrgQualificationsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_CreateOrgQualification : NSObject {
	
/* elements */
	org_tns2_OrgQualification * orgQualification;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_CreateOrgQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgQualification * orgQualification;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_CreateOrgQualificationResponse : NSObject {
	
/* elements */
	org_tns2_OrgQualification * CreateOrgQualificationResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_CreateOrgQualificationResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgQualification * CreateOrgQualificationResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetQualificationByMajorAndEducationLevel : NSObject {
	
/* elements */
	NSNumber * CLogMajorId;
	NSNumber * CLogEducationLevelId;
	NSNumber * CompanyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetQualificationByMajorAndEducationLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSNumber * CLogEducationLevelId;
@property (retain) NSNumber * CompanyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetQualificationByMajorAndEducationLevelResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgQualification * GetQualificationByMajorAndEducationLevelResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetQualificationByMajorAndEducationLevelResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgQualification * GetQualificationByMajorAndEducationLevelResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgKnowledge : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgKnowledge *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgKnowledgeResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgKnowledge * GetAllOrgKnowledgeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgKnowledgeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgKnowledge * GetAllOrgKnowledgeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgKnowledgeByMajor : NSObject {
	
/* elements */
	NSNumber * CLogMajorId;
	NSNumber * CompanyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgKnowledgeByMajor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSNumber * CompanyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgKnowledgeByMajorResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgKnowledge * GetAllOrgKnowledgeByMajorResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgKnowledgeByMajorResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgKnowledge * GetAllOrgKnowledgeByMajorResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgKnowledgeByPage : NSObject {
	
/* elements */
	NSNumber * pageIndex;
	NSNumber * pageSize;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgKnowledgeByPage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * pageIndex;
@property (retain) NSNumber * pageSize;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgKnowledgeByPageResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgKnowledge * GetOrgKnowledgeByPageResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgKnowledgeByPageResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgKnowledge * GetOrgKnowledgeByPageResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgKnowledgePageNumber : NSObject {
	
/* elements */
	NSNumber * pageSize;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgKnowledgePageNumber *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * pageSize;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgKnowledgePageNumberResponse : NSObject {
	
/* elements */
	NSNumber * GetOrgKnowledgePageNumberResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgKnowledgePageNumberResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetOrgKnowledgePageNumberResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_DeleteOrgKnowledge : NSObject {
	
/* elements */
	NSNumber * orgKnowledgeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_DeleteOrgKnowledge *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgKnowledgeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_DeleteOrgKnowledgeResponse : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_DeleteOrgKnowledgeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgKnowledgeById : NSObject {
	
/* elements */
	NSNumber * orgKnowledgeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgKnowledgeById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgKnowledgeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgKnowledgeByIdResponse : NSObject {
	
/* elements */
	org_tns2_OrgKnowledge * GetOrgKnowledgeByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgKnowledgeByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgKnowledge * GetOrgKnowledgeByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_UpdateOrgKnowledge : NSObject {
	
/* elements */
	org_tns2_OrgKnowledge * orgKnowledge;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_UpdateOrgKnowledge *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgKnowledge * orgKnowledge;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_UpdateOrgKnowledgeResponse : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_UpdateOrgKnowledgeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_CreateOrgKnowledge : NSObject {
	
/* elements */
	org_tns2_OrgKnowledge * orgKnowledge;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_CreateOrgKnowledge *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgKnowledge * orgKnowledge;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_CreateOrgKnowledgeResponse : NSObject {
	
/* elements */
	org_tns2_OrgKnowledge * CreateOrgKnowledgeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_CreateOrgKnowledgeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgKnowledge * CreateOrgKnowledgeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllRating : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllRatingResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfCLogRating * GetAllRatingResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllRatingResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfCLogRating * GetAllRatingResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_DeleteOrgJobPositioRequiredProficency : NSObject {
	
/* elements */
	NSNumber * OrgJobPositionRequiredProficencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_DeleteOrgJobPositioRequiredProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgJobPositionRequiredProficencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_DeleteOrgJobPositioRequiredProficencyResponse : NSObject {
	
/* elements */
	USBoolean * DeleteOrgJobPositioRequiredProficencyResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_DeleteOrgJobPositioRequiredProficencyResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteOrgJobPositioRequiredProficencyResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgJobPositionKnowledgeProficency : NSObject {
	
/* elements */
	NSNumber * orgJobPositionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgJobPositionKnowledgeProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgJobPositionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgJobPositionKnowledgeProficencyResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgJobPositionKnowledgeProficency * GetAllOrgJobPositionKnowledgeProficencyResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgJobPositionKnowledgeProficencyResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgJobPositionKnowledgeProficency * GetAllOrgJobPositionKnowledgeProficencyResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_AddOrgJobPositionKnowledgeProficencyByView : NSObject {
	
/* elements */
	org_tns2_V_OrgJobPositionKnowledgeProficency * viewOrgJobPositionKnowledgeProficency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_AddOrgJobPositionKnowledgeProficencyByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_V_OrgJobPositionKnowledgeProficency * viewOrgJobPositionKnowledgeProficency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_AddOrgJobPositionKnowledgeProficencyByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddOrgJobPositionKnowledgeProficencyByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_AddOrgJobPositionKnowledgeProficencyByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddOrgJobPositionKnowledgeProficencyByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_UpdateOrgJobPositionKnowledgeProficencyByView : NSObject {
	
/* elements */
	org_tns2_V_OrgJobPositionKnowledgeProficency * viewOrgJobPositionKnowledgeProficency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_UpdateOrgJobPositionKnowledgeProficencyByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_V_OrgJobPositionKnowledgeProficency * viewOrgJobPositionKnowledgeProficency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_UpdateOrgJobPositionKnowledgeProficencyByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateOrgJobPositionKnowledgeProficencyByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_UpdateOrgJobPositionKnowledgeProficencyByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateOrgJobPositionKnowledgeProficencyByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgJobPositionExperienceProficency : NSObject {
	
/* elements */
	NSNumber * orgJobPositionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgJobPositionExperienceProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgJobPositionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgJobPositionExperienceProficencyResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgJobPositionExperienceProficency * GetAllOrgJobPositionExperienceProficencyResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgJobPositionExperienceProficencyResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgJobPositionExperienceProficency * GetAllOrgJobPositionExperienceProficencyResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_AddOrgJobPositionExperienceProficencyByView : NSObject {
	
/* elements */
	org_tns2_V_OrgJobPositionExperienceProficency * viewOrgJobPositionExperienceProficency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_AddOrgJobPositionExperienceProficencyByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_V_OrgJobPositionExperienceProficency * viewOrgJobPositionExperienceProficency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_AddOrgJobPositionExperienceProficencyByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddOrgJobPositionExperienceProficencyByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_AddOrgJobPositionExperienceProficencyByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddOrgJobPositionExperienceProficencyByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_UpdateOrgJobPositionExperienceProficencyByView : NSObject {
	
/* elements */
	org_tns2_V_OrgJobPositionExperienceProficency * viewOrgJobPositionExperienceProficency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_UpdateOrgJobPositionExperienceProficencyByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_V_OrgJobPositionExperienceProficency * viewOrgJobPositionExperienceProficency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_UpdateOrgJobPositionExperienceProficencyByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateOrgJobPositionExperienceProficencyByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_UpdateOrgJobPositionExperienceProficencyByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateOrgJobPositionExperienceProficencyByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgJobPositionQualificationProficency : NSObject {
	
/* elements */
	NSNumber * orgJobPositionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgJobPositionQualificationProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgJobPositionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgJobPositionQualificationProficencyResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgJobPositionQualificationProficency * GetAllOrgJobPositionQualificationProficencyResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgJobPositionQualificationProficencyResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgJobPositionQualificationProficency * GetAllOrgJobPositionQualificationProficencyResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_AddOrgJobPositionQualificationProficencyByView : NSObject {
	
/* elements */
	org_tns2_V_OrgJobPositionQualificationProficency * viewOrgJobPositionQualificationProficency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_AddOrgJobPositionQualificationProficencyByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_V_OrgJobPositionQualificationProficency * viewOrgJobPositionQualificationProficency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_AddOrgJobPositionQualificationProficencyByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddOrgJobPositionQualificationProficencyByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_AddOrgJobPositionQualificationProficencyByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddOrgJobPositionQualificationProficencyByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_UpdateOrgJobPositionQualificationProficencyByView : NSObject {
	
/* elements */
	org_tns2_V_OrgJobPositionQualificationProficency * viewOrgJobPositionQualificationProficency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_UpdateOrgJobPositionQualificationProficencyByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_V_OrgJobPositionQualificationProficency * viewOrgJobPositionQualificationProficency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_UpdateOrgJobPositionQualificationProficencyByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateOrgJobPositionQualificationProficencyByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_UpdateOrgJobPositionQualificationProficencyByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateOrgJobPositionQualificationProficencyByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgJobPositionSkillProficency : NSObject {
	
/* elements */
	NSNumber * orgJobPositionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgJobPositionSkillProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgJobPositionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgJobPositionSkillProficencyResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgJobPositionSkillProficency * GetAllOrgJobPositionSkillProficencyResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgJobPositionSkillProficencyResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgJobPositionSkillProficency * GetAllOrgJobPositionSkillProficencyResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_AddOrgJobPositionSkillProficencyByView : NSObject {
	
/* elements */
	org_tns2_V_OrgJobPositionSkillProficency * viewOrgJobPositionSkillProficency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_AddOrgJobPositionSkillProficencyByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_V_OrgJobPositionSkillProficency * viewOrgJobPositionSkillProficency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_AddOrgJobPositionSkillProficencyByViewResponse : NSObject {
	
/* elements */
	USBoolean * AddOrgJobPositionSkillProficencyByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_AddOrgJobPositionSkillProficencyByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * AddOrgJobPositionSkillProficencyByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_UpdateOrgJobPositionSkillProficencyByView : NSObject {
	
/* elements */
	org_tns2_V_OrgJobPositionSkillProficency * viewOrgJobPositionSkillProficency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_UpdateOrgJobPositionSkillProficencyByView *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_V_OrgJobPositionSkillProficency * viewOrgJobPositionSkillProficency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_UpdateOrgJobPositionSkillProficencyByViewResponse : NSObject {
	
/* elements */
	USBoolean * UpdateOrgJobPositionSkillProficencyByViewResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_UpdateOrgJobPositionSkillProficencyByViewResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * UpdateOrgJobPositionSkillProficencyByViewResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllTreeOrgJobTitlePosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * intUserId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllTreeOrgJobTitlePosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * intUserId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllTreeOrgJobTitlePositionResponse : NSObject {
	
/* elements */
	org_tns4_ArrayOfOrgJobTitlePositionTreeModel * GetAllTreeOrgJobTitlePositionResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllTreeOrgJobTitlePositionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns4_ArrayOfOrgJobTitlePositionTreeModel * GetAllTreeOrgJobTitlePositionResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllTreeOrgJobAndWorkLevel : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * intUserId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllTreeOrgJobAndWorkLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * intUserId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllTreeOrgJobAndWorkLevelResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_ORG_JOB_WORK_LEVEL_TREE * GetAllTreeOrgJobAndWorkLevelResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllTreeOrgJobAndWorkLevelResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_ORG_JOB_WORK_LEVEL_TREE * GetAllTreeOrgJobAndWorkLevelResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllTreeOrgUnitPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * intUserId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllTreeOrgUnitPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * intUserId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllTreeOrgUnitPositionResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_ORG_UNIT_POSITION_TREE * GetAllTreeOrgUnitPositionResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllTreeOrgUnitPositionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_ORG_UNIT_POSITION_TREE * GetAllTreeOrgUnitPositionResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllTreeOrgJobAndPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * intUserId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllTreeOrgJobAndPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * intUserId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllTreeOrgJobAndPositionResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_ORG_JOB_POSITION_TREE * GetAllTreeOrgJobAndPositionResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllTreeOrgJobAndPositionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_ORG_JOB_POSITION_TREE * GetAllTreeOrgJobAndPositionResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgGroup : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgGroupResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgGroup * GetAllOrgGroupResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgGroupResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgGroup * GetAllOrgGroupResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgGroupByPage : NSObject {
	
/* elements */
	NSNumber * pageIndex;
	NSNumber * pageSize;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgGroupByPage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * pageIndex;
@property (retain) NSNumber * pageSize;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgGroupByPageResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgGroup * GetOrgGroupByPageResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgGroupByPageResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgGroup * GetOrgGroupByPageResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgGroupKendoPageData : NSObject {
	
/* elements */
	NSNumber * skip;
	NSNumber * take;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgGroupKendoPageData *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * skip;
@property (retain) NSNumber * take;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgGroupKendoPageDataResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgGroup * GetOrgGroupKendoPageDataResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgGroupKendoPageDataResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgGroup * GetOrgGroupKendoPageDataResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgGroupPageNumber : NSObject {
	
/* elements */
	NSNumber * pageSize;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgGroupPageNumber *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * pageSize;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgGroupPageNumberResponse : NSObject {
	
/* elements */
	NSNumber * GetOrgGroupPageNumberResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgGroupPageNumberResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * GetOrgGroupPageNumberResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_DeleteOrgGroup : NSObject {
	
/* elements */
	NSNumber * orgGroupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_DeleteOrgGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgGroupId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_DeleteOrgGroupResponse : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_DeleteOrgGroupResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgGroupById : NSObject {
	
/* elements */
	NSNumber * orgGroupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgGroupById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgGroupId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgGroupByIdResponse : NSObject {
	
/* elements */
	org_tns2_OrgGroup * GetOrgGroupByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgGroupByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgGroup * GetOrgGroupByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_UpdateOrgGroup : NSObject {
	
/* elements */
	org_tns2_OrgGroup * orgGroup;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_UpdateOrgGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgGroup * orgGroup;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_UpdateOrgGroupResponse : NSObject {
	
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_UpdateOrgGroupResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_CreateOrgGroup : NSObject {
	
/* elements */
	org_tns2_OrgGroup * orgGroup;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_CreateOrgGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgGroup * orgGroup;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_CreateOrgGroupResponse : NSObject {
	
/* elements */
	org_tns2_OrgGroup * CreateOrgGroupResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_CreateOrgGroupResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgGroup * CreateOrgGroupResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgUnits : NSObject {
	
/* elements */
	NSNumber * companyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgUnits *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * companyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllOrgUnitsResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_OrgUnitTree * GetAllOrgUnitsResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllOrgUnitsResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_OrgUnitTree * GetAllOrgUnitsResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_DeleteOrgWorkLevel : NSObject {
	
/* elements */
	NSNumber * orgWorkLevelId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_DeleteOrgWorkLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgWorkLevelId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_DeleteOrgWorkLevelResponse : NSObject {
	
/* elements */
	USBoolean * DeleteOrgWorkLevelResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_DeleteOrgWorkLevelResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * DeleteOrgWorkLevelResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgWorkLevelById : NSObject {
	
/* elements */
	NSNumber * orgWorkLevelId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgWorkLevelById *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgWorkLevelId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetOrgWorkLevelByIdResponse : NSObject {
	
/* elements */
	org_tns2_OrgWorkLevel * GetOrgWorkLevelByIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetOrgWorkLevelByIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgWorkLevel * GetOrgWorkLevelByIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_CreateOrgWorkLevel : NSObject {
	
/* elements */
	org_tns2_OrgWorkLevel * orgWorkLevel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_CreateOrgWorkLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgWorkLevel * orgWorkLevel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_CreateOrgWorkLevelResponse : NSObject {
	
/* elements */
	org_tns2_OrgWorkLevel * CreateOrgWorkLevelResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_CreateOrgWorkLevelResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_OrgWorkLevel * CreateOrgWorkLevelResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllWorkLevelByOrgJobId : NSObject {
	
/* elements */
	NSNumber * orgJobId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllWorkLevelByOrgJobId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * orgJobId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllWorkLevelByOrgJobIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfOrgWorkLevel * GetAllWorkLevelByOrgJobIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllWorkLevelByOrgJobIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfOrgWorkLevel * GetAllWorkLevelByOrgJobIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllEmployeeByOrgWorkLevelId : NSObject {
	
/* elements */
	NSNumber * OrgWorkLevelId;
	NSNumber * take;
	NSNumber * skip;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllEmployeeByOrgWorkLevelId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSNumber * take;
@property (retain) NSNumber * skip;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetAllEmployeeByOrgWorkLevelIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_Employee * GetAllEmployeeByOrgWorkLevelIdResult;
	NSNumber * count;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetAllEmployeeByOrgWorkLevelIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_Employee * GetAllEmployeeByOrgWorkLevelIdResult;
@property (retain) NSNumber * count;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetViewEmployeeByJobPositionId : NSObject {
	
/* elements */
	NSNumber * jobPositionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetViewEmployeeByJobPositionId *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * jobPositionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface OrganizationServiceSvc_GetViewEmployeeByJobPositionIdResponse : NSObject {
	
/* elements */
	org_tns2_ArrayOfV_Employee * GetViewEmployeeByJobPositionIdResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (OrganizationServiceSvc_GetViewEmployeeByJobPositionIdResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfV_Employee * GetViewEmployeeByJobPositionIdResult;
/* attributes */
- (NSDictionary *)attributes;
@end
/* Cookies handling provided by http://en.wikibooks.org/wiki/Programming:WebObjects/Web_Services/Web_Service_Provider */
#import <libxml/parser.h>
#import "xs.h"
#import "OrganizationServiceSvc.h"
#import "ns1.h"
#import "org_tns1.h"
#import "org_tns2.h"
#import "org_tns5.h"
#import "org_tns3.h"
#import "org_tns4.h"
@class BasicHttpBinding_IOrganizationServiceBinding;
@interface OrganizationServiceSvc : NSObject {
	
}
+ (BasicHttpBinding_IOrganizationServiceBinding *)BasicHttpBinding_IOrganizationServiceBinding;
@end
@class BasicHttpBinding_IOrganizationServiceBindingResponse;
@class BasicHttpBinding_IOrganizationServiceBindingOperation;
@protocol BasicHttpBinding_IOrganizationServiceBindingResponseDelegate <NSObject>
- (void) operation:(BasicHttpBinding_IOrganizationServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IOrganizationServiceBindingResponse *)response;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding : NSObject <BasicHttpBinding_IOrganizationServiceBindingResponseDelegate> {
	NSURL *address;
	NSTimeInterval defaultTimeout;
	NSMutableArray *cookies;
	BOOL logXMLInOut;
	BOOL synchronousOperationComplete;
	NSString *authUsername;
	NSString *authPassword;
}
@property (copy) NSURL *address;
@property (assign) BOOL logXMLInOut;
@property (assign) NSTimeInterval defaultTimeout;
@property (nonatomic, retain) NSMutableArray *cookies;
@property (nonatomic, retain) NSString *authUsername;
@property (nonatomic, retain) NSString *authPassword;
- (id)initWithAddress:(NSString *)anAddress;
- (void)sendHTTPCallUsingBody:(NSString *)body soapAction:(NSString *)soapAction forOperation:(BasicHttpBinding_IOrganizationServiceBindingOperation *)operation;
- (void)addCookie:(NSHTTPCookie *)toAdd;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetEmployeeByListOrgUnitIdUsingParameters:(OrganizationServiceSvc_GetEmployeeByListOrgUnitId *)aParameters ;
- (void)GetEmployeeByListOrgUnitIdAsyncUsingParameters:(OrganizationServiceSvc_GetEmployeeByListOrgUnitId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)SearchOrgJobUsingParameters:(OrganizationServiceSvc_SearchOrgJob *)aParameters ;
- (void)SearchOrgJobAsyncUsingParameters:(OrganizationServiceSvc_SearchOrgJob *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)SearchOrgJobPositionUsingParameters:(OrganizationServiceSvc_SearchOrgJobPosition *)aParameters ;
- (void)SearchOrgJobPositionAsyncUsingParameters:(OrganizationServiceSvc_SearchOrgJobPosition *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)SearchOrgUnitUsingParameters:(OrganizationServiceSvc_SearchOrgUnit *)aParameters ;
- (void)SearchOrgUnitAsyncUsingParameters:(OrganizationServiceSvc_SearchOrgUnit *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllCLogRatingsUsingParameters:(OrganizationServiceSvc_GetAllCLogRatings *)aParameters ;
- (void)GetAllCLogRatingsAsyncUsingParameters:(OrganizationServiceSvc_GetAllCLogRatings *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgProficencyTypesUsingParameters:(OrganizationServiceSvc_GetOrgProficencyTypes *)aParameters ;
- (void)GetOrgProficencyTypesAsyncUsingParameters:(OrganizationServiceSvc_GetOrgProficencyTypes *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllSpecificCompetencyByJobPositionIdUsingParameters:(OrganizationServiceSvc_GetAllSpecificCompetencyByJobPositionId *)aParameters ;
- (void)GetAllSpecificCompetencyByJobPositionIdAsyncUsingParameters:(OrganizationServiceSvc_GetAllSpecificCompetencyByJobPositionId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgGroupCompetencyUsingParameters:(OrganizationServiceSvc_GetAllOrgGroupCompetency *)aParameters ;
- (void)GetAllOrgGroupCompetencyAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgGroupCompetency *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllCompetencyByJobIdUsingParameters:(OrganizationServiceSvc_GetAllCompetencyByJobId *)aParameters ;
- (void)GetAllCompetencyByJobIdAsyncUsingParameters:(OrganizationServiceSvc_GetAllCompetencyByJobId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllJobSpecificCompetencyByCompIdUsingParameters:(OrganizationServiceSvc_GetAllJobSpecificCompetencyByCompId *)aParameters ;
- (void)GetAllJobSpecificCompetencyByCompIdAsyncUsingParameters:(OrganizationServiceSvc_GetAllJobSpecificCompetencyByCompId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllJobPosSpecificCompetencyByJobIdCompIdUsingParameters:(OrganizationServiceSvc_GetAllJobPosSpecificCompetencyByJobIdCompId *)aParameters ;
- (void)GetAllJobPosSpecificCompetencyByJobIdCompIdAsyncUsingParameters:(OrganizationServiceSvc_GetAllJobPosSpecificCompetencyByJobIdCompId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllCompetencyLevelByCompetencyIdUsingParameters:(OrganizationServiceSvc_GetAllCompetencyLevelByCompetencyId *)aParameters ;
- (void)GetAllCompetencyLevelByCompetencyIdAsyncUsingParameters:(OrganizationServiceSvc_GetAllCompetencyLevelByCompetencyId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllCompetencyLevelDetailByLevelIdUsingParameters:(OrganizationServiceSvc_GetAllCompetencyLevelDetailByLevelId *)aParameters ;
- (void)GetAllCompetencyLevelDetailByLevelIdAsyncUsingParameters:(OrganizationServiceSvc_GetAllCompetencyLevelDetailByLevelId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllV_OrgSkillCompetencyByUsingParameters:(OrganizationServiceSvc_GetAllV_OrgSkillCompetencyBy *)aParameters ;
- (void)GetAllV_OrgSkillCompetencyByAsyncUsingParameters:(OrganizationServiceSvc_GetAllV_OrgSkillCompetencyBy *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllV_OrgQualificationCompetencyByUsingParameters:(OrganizationServiceSvc_GetAllV_OrgQualificationCompetencyBy *)aParameters ;
- (void)GetAllV_OrgQualificationCompetencyByAsyncUsingParameters:(OrganizationServiceSvc_GetAllV_OrgQualificationCompetencyBy *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllV_OrgExperienceCompetencyByUsingParameters:(OrganizationServiceSvc_GetAllV_OrgExperienceCompetencyBy *)aParameters ;
- (void)GetAllV_OrgExperienceCompetencyByAsyncUsingParameters:(OrganizationServiceSvc_GetAllV_OrgExperienceCompetencyBy *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllV_OrgDegreeCompetencyByUsingParameters:(OrganizationServiceSvc_GetAllV_OrgDegreeCompetencyBy *)aParameters ;
- (void)GetAllV_OrgDegreeCompetencyByAsyncUsingParameters:(OrganizationServiceSvc_GetAllV_OrgDegreeCompetencyBy *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllV_OrgAttitudeCompetencyByUsingParameters:(OrganizationServiceSvc_GetAllV_OrgAttitudeCompetencyBy *)aParameters ;
- (void)GetAllV_OrgAttitudeCompetencyByAsyncUsingParameters:(OrganizationServiceSvc_GetAllV_OrgAttitudeCompetencyBy *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllV_OrgKnowledgeCompetencyByUsingParameters:(OrganizationServiceSvc_GetAllV_OrgKnowledgeCompetencyBy *)aParameters ;
- (void)GetAllV_OrgKnowledgeCompetencyByAsyncUsingParameters:(OrganizationServiceSvc_GetAllV_OrgKnowledgeCompetencyBy *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllJobPosCompetencyUsingParameters:(OrganizationServiceSvc_GetAllJobPosCompetency *)aParameters ;
- (void)GetAllJobPosCompetencyAsyncUsingParameters:(OrganizationServiceSvc_GetAllJobPosCompetency *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllCoreCompetencyUsingParameters:(OrganizationServiceSvc_GetAllCoreCompetency *)aParameters ;
- (void)GetAllCoreCompetencyAsyncUsingParameters:(OrganizationServiceSvc_GetAllCoreCompetency *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllJobPosCompetencyByGroupIdUsingParameters:(OrganizationServiceSvc_GetAllJobPosCompetencyByGroupId *)aParameters ;
- (void)GetAllJobPosCompetencyByGroupIdAsyncUsingParameters:(OrganizationServiceSvc_GetAllJobPosCompetencyByGroupId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllCoreCompetencyByGroupIdUsingParameters:(OrganizationServiceSvc_GetAllCoreCompetencyByGroupId *)aParameters ;
- (void)GetAllCoreCompetencyByGroupIdAsyncUsingParameters:(OrganizationServiceSvc_GetAllCoreCompetencyByGroupId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetJobPosCompetencyByIdUsingParameters:(OrganizationServiceSvc_GetJobPosCompetencyById *)aParameters ;
- (void)GetJobPosCompetencyByIdAsyncUsingParameters:(OrganizationServiceSvc_GetJobPosCompetencyById *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllJobPositionByJobIdUsingParameters:(OrganizationServiceSvc_GetAllJobPositionByJobId *)aParameters ;
- (void)GetAllJobPositionByJobIdAsyncUsingParameters:(OrganizationServiceSvc_GetAllJobPositionByJobId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)EditJobPosCompetencyUsingParameters:(OrganizationServiceSvc_EditJobPosCompetency *)aParameters ;
- (void)EditJobPosCompetencyAsyncUsingParameters:(OrganizationServiceSvc_EditJobPosCompetency *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetDirecReportByIdUsingParameters:(OrganizationServiceSvc_GetDirecReportById *)aParameters ;
- (void)GetDirecReportByIdAsyncUsingParameters:(OrganizationServiceSvc_GetDirecReportById *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)FindLevelOfGenOfPSCdUsingParameters:(OrganizationServiceSvc_FindLevelOfGenOfPSCd *)aParameters ;
- (void)FindLevelOfGenOfPSCdAsyncUsingParameters:(OrganizationServiceSvc_FindLevelOfGenOfPSCd *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)IsAscendantUsingParameters:(OrganizationServiceSvc_IsAscendant *)aParameters ;
- (void)IsAscendantAsyncUsingParameters:(OrganizationServiceSvc_IsAscendant *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)IsDescendantUsingParameters:(OrganizationServiceSvc_IsDescendant *)aParameters ;
- (void)IsDescendantAsyncUsingParameters:(OrganizationServiceSvc_IsDescendant *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)IsXthAscendantUsingParameters:(OrganizationServiceSvc_IsXthAscendant *)aParameters ;
- (void)IsXthAscendantAsyncUsingParameters:(OrganizationServiceSvc_IsXthAscendant *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)IsXthDescendantUsingParameters:(OrganizationServiceSvc_IsXthDescendant *)aParameters ;
- (void)IsXthDescendantAsyncUsingParameters:(OrganizationServiceSvc_IsXthDescendant *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetXAscendantFromGenPSCdUsingParameters:(OrganizationServiceSvc_GetXAscendantFromGenPSCd *)aParameters ;
- (void)GetXAscendantFromGenPSCdAsyncUsingParameters:(OrganizationServiceSvc_GetXAscendantFromGenPSCd *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetXDescendanceFromGenPSCdUsingParameters:(OrganizationServiceSvc_GetXDescendanceFromGenPSCd *)aParameters ;
- (void)GetXDescendanceFromGenPSCdAsyncUsingParameters:(OrganizationServiceSvc_GetXDescendanceFromGenPSCd *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllGenPSHasTheSameLevelUsingParameters:(OrganizationServiceSvc_GetAllGenPSHasTheSameLevel *)aParameters ;
- (void)GetAllGenPSHasTheSameLevelAsyncUsingParameters:(OrganizationServiceSvc_GetAllGenPSHasTheSameLevel *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllGenPSHasTheSameLevelAndParentNodeUsingParameters:(OrganizationServiceSvc_GetAllGenPSHasTheSameLevelAndParentNode *)aParameters ;
- (void)GetAllGenPSHasTheSameLevelAndParentNodeAsyncUsingParameters:(OrganizationServiceSvc_GetAllGenPSHasTheSameLevelAndParentNode *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetListOfPersonelStructUsingParameters:(OrganizationServiceSvc_GetListOfPersonelStruct *)aParameters ;
- (void)GetListOfPersonelStructAsyncUsingParameters:(OrganizationServiceSvc_GetListOfPersonelStruct *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetListOfEmployeeMappingPersonelStructUsingParameters:(OrganizationServiceSvc_GetListOfEmployeeMappingPersonelStruct *)aParameters ;
- (void)GetListOfEmployeeMappingPersonelStructAsyncUsingParameters:(OrganizationServiceSvc_GetListOfEmployeeMappingPersonelStruct *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllTreeOrgJobTitleUsingParameters:(OrganizationServiceSvc_GetAllTreeOrgJobTitle *)aParameters ;
- (void)GetAllTreeOrgJobTitleAsyncUsingParameters:(OrganizationServiceSvc_GetAllTreeOrgJobTitle *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetViewOrgJobTitleByCodeUsingParameters:(OrganizationServiceSvc_GetViewOrgJobTitleByCode *)aParameters ;
- (void)GetViewOrgJobTitleByCodeAsyncUsingParameters:(OrganizationServiceSvc_GetViewOrgJobTitleByCode *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetListOrgJobTitleGroupsUsingParameters:(OrganizationServiceSvc_GetListOrgJobTitleGroups *)aParameters ;
- (void)GetListOrgJobTitleGroupsAsyncUsingParameters:(OrganizationServiceSvc_GetListOrgJobTitleGroups *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetListOrgJobTitleLevelsUsingParameters:(OrganizationServiceSvc_GetListOrgJobTitleLevels *)aParameters ;
- (void)GetListOrgJobTitleLevelsAsyncUsingParameters:(OrganizationServiceSvc_GetListOrgJobTitleLevels *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)AddOrgJobTitleUsingParameters:(OrganizationServiceSvc_AddOrgJobTitle *)aParameters ;
- (void)AddOrgJobTitleAsyncUsingParameters:(OrganizationServiceSvc_AddOrgJobTitle *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)UpdateOrgJobTitleUsingParameters:(OrganizationServiceSvc_UpdateOrgJobTitle *)aParameters ;
- (void)UpdateOrgJobTitleAsyncUsingParameters:(OrganizationServiceSvc_UpdateOrgJobTitle *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)DeleteOrgJobTitleUsingParameters:(OrganizationServiceSvc_DeleteOrgJobTitle *)aParameters ;
- (void)DeleteOrgJobTitleAsyncUsingParameters:(OrganizationServiceSvc_DeleteOrgJobTitle *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetListAllOrgJobTitlesUsingParameters:(OrganizationServiceSvc_GetListAllOrgJobTitles *)aParameters ;
- (void)GetListAllOrgJobTitlesAsyncUsingParameters:(OrganizationServiceSvc_GetListAllOrgJobTitles *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)SearchOrgJobTitleUsingParameters:(OrganizationServiceSvc_SearchOrgJobTitle *)aParameters ;
- (void)SearchOrgJobTitleAsyncUsingParameters:(OrganizationServiceSvc_SearchOrgJobTitle *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetListViewEmployeeByOrgJobTitleCodeUsingParameters:(OrganizationServiceSvc_GetListViewEmployeeByOrgJobTitleCode *)aParameters ;
- (void)GetListViewEmployeeByOrgJobTitleCodeAsyncUsingParameters:(OrganizationServiceSvc_GetListViewEmployeeByOrgJobTitleCode *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllClassificationEmpTreeUsingParameters:(OrganizationServiceSvc_GetAllClassificationEmpTree *)aParameters ;
- (void)GetAllClassificationEmpTreeAsyncUsingParameters:(OrganizationServiceSvc_GetAllClassificationEmpTree *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllClassificationEmpMappingUsingParameters:(OrganizationServiceSvc_GetAllClassificationEmpMapping *)aParameters ;
- (void)GetAllClassificationEmpMappingAsyncUsingParameters:(OrganizationServiceSvc_GetAllClassificationEmpMapping *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgChangeInfosUsingParameters:(OrganizationServiceSvc_GetOrgChangeInfos *)aParameters ;
- (void)GetOrgChangeInfosAsyncUsingParameters:(OrganizationServiceSvc_GetOrgChangeInfos *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllChildrenReportEmployeeByParentIdUsingParameters:(OrganizationServiceSvc_GetAllChildrenReportEmployeeByParentId *)aParameters ;
- (void)GetAllChildrenReportEmployeeByParentIdAsyncUsingParameters:(OrganizationServiceSvc_GetAllChildrenReportEmployeeByParentId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllDescendantsReportEmployeeByParentIdUsingParameters:(OrganizationServiceSvc_GetAllDescendantsReportEmployeeByParentId *)aParameters ;
- (void)GetAllDescendantsReportEmployeeByParentIdAsyncUsingParameters:(OrganizationServiceSvc_GetAllDescendantsReportEmployeeByParentId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetReportEmployeeRootUsingParameters:(OrganizationServiceSvc_GetReportEmployeeRoot *)aParameters ;
- (void)GetReportEmployeeRootAsyncUsingParameters:(OrganizationServiceSvc_GetReportEmployeeRoot *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllUnitsUsingParameters:(OrganizationServiceSvc_GetAllUnits *)aParameters ;
- (void)GetAllUnitsAsyncUsingParameters:(OrganizationServiceSvc_GetAllUnits *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllChildrenOrgUnitByParentIdUsingParameters:(OrganizationServiceSvc_GetAllChildrenOrgUnitByParentId *)aParameters ;
- (void)GetAllChildrenOrgUnitByParentIdAsyncUsingParameters:(OrganizationServiceSvc_GetAllChildrenOrgUnitByParentId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllDescendantsOrgUnitByParentIdUsingParameters:(OrganizationServiceSvc_GetAllDescendantsOrgUnitByParentId *)aParameters ;
- (void)GetAllDescendantsOrgUnitByParentIdAsyncUsingParameters:(OrganizationServiceSvc_GetAllDescendantsOrgUnitByParentId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgUnitRootUsingParameters:(OrganizationServiceSvc_GetOrgUnitRoot *)aParameters ;
- (void)GetOrgUnitRootAsyncUsingParameters:(OrganizationServiceSvc_GetOrgUnitRoot *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetUnitLeaderUsingParameters:(OrganizationServiceSvc_GetUnitLeader *)aParameters ;
- (void)GetUnitLeaderAsyncUsingParameters:(OrganizationServiceSvc_GetUnitLeader *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllUnitsPositionUsingParameters:(OrganizationServiceSvc_GetAllUnitsPosition *)aParameters ;
- (void)GetAllUnitsPositionAsyncUsingParameters:(OrganizationServiceSvc_GetAllUnitsPosition *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetViewOrgUnitByOrgUnitIdUsingParameters:(OrganizationServiceSvc_GetViewOrgUnitByOrgUnitId *)aParameters ;
- (void)GetViewOrgUnitByOrgUnitIdAsyncUsingParameters:(OrganizationServiceSvc_GetViewOrgUnitByOrgUnitId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgUnitJobsByOrgUnitIdUsingParameters:(OrganizationServiceSvc_GetAllOrgUnitJobsByOrgUnitId *)aParameters ;
- (void)GetAllOrgUnitJobsByOrgUnitIdAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgUnitJobsByOrgUnitId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetEmployeeKendoFilterByJobIdUsingParameters:(OrganizationServiceSvc_GetEmployeeKendoFilterByJobId *)aParameters ;
- (void)GetEmployeeKendoFilterByJobIdAsyncUsingParameters:(OrganizationServiceSvc_GetEmployeeKendoFilterByJobId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgJobByPageUsingParameters:(OrganizationServiceSvc_GetAllOrgJobByPage *)aParameters ;
- (void)GetAllOrgJobByPageAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgJobByPage *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetViewOrgUnitJobByOrgUnitIdUsingParameters:(OrganizationServiceSvc_GetViewOrgUnitJobByOrgUnitId *)aParameters ;
- (void)GetViewOrgUnitJobByOrgUnitIdAsyncUsingParameters:(OrganizationServiceSvc_GetViewOrgUnitJobByOrgUnitId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)CreateOrgUnitJobUsingParameters:(OrganizationServiceSvc_CreateOrgUnitJob *)aParameters ;
- (void)CreateOrgUnitJobAsyncUsingParameters:(OrganizationServiceSvc_CreateOrgUnitJob *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgUnitJobsUsingParameters:(OrganizationServiceSvc_GetAllOrgUnitJobs *)aParameters ;
- (void)GetAllOrgUnitJobsAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgUnitJobs *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgUnitsByPageUsingParameters:(OrganizationServiceSvc_GetOrgUnitsByPage *)aParameters ;
- (void)GetOrgUnitsByPageAsyncUsingParameters:(OrganizationServiceSvc_GetOrgUnitsByPage *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgUnitJobByOrgUnitIdUsingParameters:(OrganizationServiceSvc_GetAllOrgUnitJobByOrgUnitId *)aParameters ;
- (void)GetAllOrgUnitJobByOrgUnitIdAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgUnitJobByOrgUnitId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgUnitTreeByOrgUnitIdUsingParameters:(OrganizationServiceSvc_GetOrgUnitTreeByOrgUnitId *)aParameters ;
- (void)GetOrgUnitTreeByOrgUnitIdAsyncUsingParameters:(OrganizationServiceSvc_GetOrgUnitTreeByOrgUnitId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgUnitByOrgUnitIdUsingParameters:(OrganizationServiceSvc_GetOrgUnitByOrgUnitId *)aParameters ;
- (void)GetOrgUnitByOrgUnitIdAsyncUsingParameters:(OrganizationServiceSvc_GetOrgUnitByOrgUnitId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgUnitTreeUsingParameters:(OrganizationServiceSvc_GetAllOrgUnitTree *)aParameters ;
- (void)GetAllOrgUnitTreeAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgUnitTree *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgUnitTreeByPermissionUsingParameters:(OrganizationServiceSvc_GetAllOrgUnitTreeByPermission *)aParameters ;
- (void)GetAllOrgUnitTreeByPermissionAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgUnitTreeByPermission *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)EditOrgUnitUsingParameters:(OrganizationServiceSvc_EditOrgUnit *)aParameters ;
- (void)EditOrgUnitAsyncUsingParameters:(OrganizationServiceSvc_EditOrgUnit *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)TransferOrgUnitUsingParameters:(OrganizationServiceSvc_TransferOrgUnit *)aParameters ;
- (void)TransferOrgUnitAsyncUsingParameters:(OrganizationServiceSvc_TransferOrgUnit *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)DeleteOrgUnitUsingParameters:(OrganizationServiceSvc_DeleteOrgUnit *)aParameters ;
- (void)DeleteOrgUnitAsyncUsingParameters:(OrganizationServiceSvc_DeleteOrgUnit *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)DeleteOrgUnitJobUsingParameters:(OrganizationServiceSvc_DeleteOrgUnitJob *)aParameters ;
- (void)DeleteOrgUnitJobAsyncUsingParameters:(OrganizationServiceSvc_DeleteOrgUnitJob *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgSkillByOrgSkillIdUsingParameters:(OrganizationServiceSvc_GetOrgSkillByOrgSkillId *)aParameters ;
- (void)GetOrgSkillByOrgSkillIdAsyncUsingParameters:(OrganizationServiceSvc_GetOrgSkillByOrgSkillId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgSkillsBySkillTypeUsingParameters:(OrganizationServiceSvc_GetAllOrgSkillsBySkillType *)aParameters ;
- (void)GetAllOrgSkillsBySkillTypeAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgSkillsBySkillType *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgSkillUsingParameters:(OrganizationServiceSvc_GetAllOrgSkill *)aParameters ;
- (void)GetAllOrgSkillAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgSkill *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgSkillByPageUsingParameters:(OrganizationServiceSvc_GetOrgSkillByPage *)aParameters ;
- (void)GetOrgSkillByPageAsyncUsingParameters:(OrganizationServiceSvc_GetOrgSkillByPage *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgSkillKendoPageDataUsingParameters:(OrganizationServiceSvc_GetOrgSkillKendoPageData *)aParameters ;
- (void)GetOrgSkillKendoPageDataAsyncUsingParameters:(OrganizationServiceSvc_GetOrgSkillKendoPageData *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgSkillPageNumberUsingParameters:(OrganizationServiceSvc_GetOrgSkillPageNumber *)aParameters ;
- (void)GetOrgSkillPageNumberAsyncUsingParameters:(OrganizationServiceSvc_GetOrgSkillPageNumber *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)DeleteOrgSkillUsingParameters:(OrganizationServiceSvc_DeleteOrgSkill *)aParameters ;
- (void)DeleteOrgSkillAsyncUsingParameters:(OrganizationServiceSvc_DeleteOrgSkill *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgSkillByIdUsingParameters:(OrganizationServiceSvc_GetOrgSkillById *)aParameters ;
- (void)GetOrgSkillByIdAsyncUsingParameters:(OrganizationServiceSvc_GetOrgSkillById *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)UpdateOrgSkillUsingParameters:(OrganizationServiceSvc_UpdateOrgSkill *)aParameters ;
- (void)UpdateOrgSkillAsyncUsingParameters:(OrganizationServiceSvc_UpdateOrgSkill *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)CreateOrgSkillUsingParameters:(OrganizationServiceSvc_CreateOrgSkill *)aParameters ;
- (void)CreateOrgSkillAsyncUsingParameters:(OrganizationServiceSvc_CreateOrgSkill *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgTimeInChargeUsingParameters:(OrganizationServiceSvc_GetAllOrgTimeInCharge *)aParameters ;
- (void)GetAllOrgTimeInChargeAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgTimeInCharge *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgSkillTypeUsingParameters:(OrganizationServiceSvc_GetAllOrgSkillType *)aParameters ;
- (void)GetAllOrgSkillTypeAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgSkillType *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgSkillTypeByOrgSkillTypeIdUsingParameters:(OrganizationServiceSvc_GetOrgSkillTypeByOrgSkillTypeId *)aParameters ;
- (void)GetOrgSkillTypeByOrgSkillTypeIdAsyncUsingParameters:(OrganizationServiceSvc_GetOrgSkillTypeByOrgSkillTypeId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgProjectTypeUsingParameters:(OrganizationServiceSvc_GetAllOrgProjectType *)aParameters ;
- (void)GetAllOrgProjectTypeAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgProjectType *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgProjectTypeByOrgProjectTypeIdUsingParameters:(OrganizationServiceSvc_GetOrgProjectTypeByOrgProjectTypeId *)aParameters ;
- (void)GetOrgProjectTypeByOrgProjectTypeIdAsyncUsingParameters:(OrganizationServiceSvc_GetOrgProjectTypeByOrgProjectTypeId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllCLogCareerUsingParameters:(OrganizationServiceSvc_GetAllCLogCareer *)aParameters ;
- (void)GetAllCLogCareerAsyncUsingParameters:(OrganizationServiceSvc_GetAllCLogCareer *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllEmployeeByCareerCodeUsingParameters:(OrganizationServiceSvc_GetAllEmployeeByCareerCode *)aParameters ;
- (void)GetAllEmployeeByCareerCodeAsyncUsingParameters:(OrganizationServiceSvc_GetAllEmployeeByCareerCode *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllTimeInChargeUsingParameters:(OrganizationServiceSvc_GetAllTimeInCharge *)aParameters ;
- (void)GetAllTimeInChargeAsyncUsingParameters:(OrganizationServiceSvc_GetAllTimeInCharge *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgDegreeUsingParameters:(OrganizationServiceSvc_GetAllOrgDegree *)aParameters ;
- (void)GetAllOrgDegreeAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgDegree *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgDegreeByOrgDegreeIdUsingParameters:(OrganizationServiceSvc_GetOrgDegreeByOrgDegreeId *)aParameters ;
- (void)GetOrgDegreeByOrgDegreeIdAsyncUsingParameters:(OrganizationServiceSvc_GetOrgDegreeByOrgDegreeId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgDegreeRankUsingParameters:(OrganizationServiceSvc_GetAllOrgDegreeRank *)aParameters ;
- (void)GetAllOrgDegreeRankAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgDegreeRank *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgDegreeRankByOrgDegreeRankIdUsingParameters:(OrganizationServiceSvc_GetOrgDegreeRankByOrgDegreeRankId *)aParameters ;
- (void)GetOrgDegreeRankByOrgDegreeRankIdAsyncUsingParameters:(OrganizationServiceSvc_GetOrgDegreeRankByOrgDegreeRankId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgJobsUsingParameters:(OrganizationServiceSvc_GetAllOrgJobs *)aParameters ;
- (void)GetAllOrgJobsAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgJobs *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetViewOrgJobsUsingParameters:(OrganizationServiceSvc_GetViewOrgJobs *)aParameters ;
- (void)GetViewOrgJobsAsyncUsingParameters:(OrganizationServiceSvc_GetViewOrgJobs *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetViewOrgJobByIdUsingParameters:(OrganizationServiceSvc_GetViewOrgJobById *)aParameters ;
- (void)GetViewOrgJobByIdAsyncUsingParameters:(OrganizationServiceSvc_GetViewOrgJobById *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllViewOrgJobByCareerIdUsingParameters:(OrganizationServiceSvc_GetAllViewOrgJobByCareerId *)aParameters ;
- (void)GetAllViewOrgJobByCareerIdAsyncUsingParameters:(OrganizationServiceSvc_GetAllViewOrgJobByCareerId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgJobByCareerUsingParameters:(OrganizationServiceSvc_GetAllOrgJobByCareer *)aParameters ;
- (void)GetAllOrgJobByCareerAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgJobByCareer *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgJobByIdUsingParameters:(OrganizationServiceSvc_GetOrgJobById *)aParameters ;
- (void)GetOrgJobByIdAsyncUsingParameters:(OrganizationServiceSvc_GetOrgJobById *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgJobByCodeUsingParameters:(OrganizationServiceSvc_GetOrgJobByCode *)aParameters ;
- (void)GetOrgJobByCodeAsyncUsingParameters:(OrganizationServiceSvc_GetOrgJobByCode *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgJobDescriptionDetailForJobUsingParameters:(OrganizationServiceSvc_GetOrgJobDescriptionDetailForJob *)aParameters ;
- (void)GetOrgJobDescriptionDetailForJobAsyncUsingParameters:(OrganizationServiceSvc_GetOrgJobDescriptionDetailForJob *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgJobDescriptionDetailForJobPositionUsingParameters:(OrganizationServiceSvc_GetOrgJobDescriptionDetailForJobPosition *)aParameters ;
- (void)GetOrgJobDescriptionDetailForJobPositionAsyncUsingParameters:(OrganizationServiceSvc_GetOrgJobDescriptionDetailForJobPosition *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)CreateNewOrgJobUsingParameters:(OrganizationServiceSvc_CreateNewOrgJob *)aParameters ;
- (void)CreateNewOrgJobAsyncUsingParameters:(OrganizationServiceSvc_CreateNewOrgJob *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)UpdateOrgJobUsingParameters:(OrganizationServiceSvc_UpdateOrgJob *)aParameters ;
- (void)UpdateOrgJobAsyncUsingParameters:(OrganizationServiceSvc_UpdateOrgJob *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)DeleteOrgJobUsingParameters:(OrganizationServiceSvc_DeleteOrgJob *)aParameters ;
- (void)DeleteOrgJobAsyncUsingParameters:(OrganizationServiceSvc_DeleteOrgJob *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllTreeOrgCareerJobAndWorkLevelUsingParameters:(OrganizationServiceSvc_GetAllTreeOrgCareerJobAndWorkLevel *)aParameters ;
- (void)GetAllTreeOrgCareerJobAndWorkLevelAsyncUsingParameters:(OrganizationServiceSvc_GetAllTreeOrgCareerJobAndWorkLevel *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllEmployeeByOrgJobIdUsingParameters:(OrganizationServiceSvc_GetAllEmployeeByOrgJobId *)aParameters ;
- (void)GetAllEmployeeByOrgJobIdAsyncUsingParameters:(OrganizationServiceSvc_GetAllEmployeeByOrgJobId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgJobPositionsUsingParameters:(OrganizationServiceSvc_GetAllOrgJobPositions *)aParameters ;
- (void)GetAllOrgJobPositionsAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgJobPositions *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetListOrgJobPosByOrgJobIdUsingParameters:(OrganizationServiceSvc_GetListOrgJobPosByOrgJobId *)aParameters ;
- (void)GetListOrgJobPosByOrgJobIdAsyncUsingParameters:(OrganizationServiceSvc_GetListOrgJobPosByOrgJobId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgJobPosByOrgUnitIdUsingParameters:(OrganizationServiceSvc_GetAllOrgJobPosByOrgUnitId *)aParameters ;
- (void)GetAllOrgJobPosByOrgUnitIdAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgJobPosByOrgUnitId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllJobPositionByOrgUnitIdUsingParameters:(OrganizationServiceSvc_GetAllJobPositionByOrgUnitId *)aParameters ;
- (void)GetAllJobPositionByOrgUnitIdAsyncUsingParameters:(OrganizationServiceSvc_GetAllJobPositionByOrgUnitId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllJobPositionByOrgJobIdUsingParameters:(OrganizationServiceSvc_GetAllJobPositionByOrgJobId *)aParameters ;
- (void)GetAllJobPositionByOrgJobIdAsyncUsingParameters:(OrganizationServiceSvc_GetAllJobPositionByOrgJobId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllJobPositionByWorkLevelIdUsingParameters:(OrganizationServiceSvc_GetAllJobPositionByWorkLevelId *)aParameters ;
- (void)GetAllJobPositionByWorkLevelIdAsyncUsingParameters:(OrganizationServiceSvc_GetAllJobPositionByWorkLevelId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllJobPositionByJobTitleIdUsingParameters:(OrganizationServiceSvc_GetAllJobPositionByJobTitleId *)aParameters ;
- (void)GetAllJobPositionByJobTitleIdAsyncUsingParameters:(OrganizationServiceSvc_GetAllJobPositionByJobTitleId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllViewOrgJobPositionUsingParameters:(OrganizationServiceSvc_GetAllViewOrgJobPosition *)aParameters ;
- (void)GetAllViewOrgJobPositionAsyncUsingParameters:(OrganizationServiceSvc_GetAllViewOrgJobPosition *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllEmployeeByPositionIdUsingParameters:(OrganizationServiceSvc_GetAllEmployeeByPositionId *)aParameters ;
- (void)GetAllEmployeeByPositionIdAsyncUsingParameters:(OrganizationServiceSvc_GetAllEmployeeByPositionId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)AddOrgJobPositionUsingParameters:(OrganizationServiceSvc_AddOrgJobPosition *)aParameters ;
- (void)AddOrgJobPositionAsyncUsingParameters:(OrganizationServiceSvc_AddOrgJobPosition *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)UpdateOrgJobPositionUsingParameters:(OrganizationServiceSvc_UpdateOrgJobPosition *)aParameters ;
- (void)UpdateOrgJobPositionAsyncUsingParameters:(OrganizationServiceSvc_UpdateOrgJobPosition *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)DeleteOrgJobPositionUsingParameters:(OrganizationServiceSvc_DeleteOrgJobPosition *)aParameters ;
- (void)DeleteOrgJobPositionAsyncUsingParameters:(OrganizationServiceSvc_DeleteOrgJobPosition *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrganizationJobPositionUsingParameters:(OrganizationServiceSvc_GetAllOrganizationJobPosition *)aParameters ;
- (void)GetAllOrganizationJobPositionAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrganizationJobPosition *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgJobPositionByIdUsingParameters:(OrganizationServiceSvc_GetOrgJobPositionById *)aParameters ;
- (void)GetOrgJobPositionByIdAsyncUsingParameters:(OrganizationServiceSvc_GetOrgJobPositionById *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetViewOrgJobPositionDetailByIdUsingParameters:(OrganizationServiceSvc_GetViewOrgJobPositionDetailById *)aParameters ;
- (void)GetViewOrgJobPositionDetailByIdAsyncUsingParameters:(OrganizationServiceSvc_GetViewOrgJobPositionDetailById *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetViewOrgJobPositionByIdUsingParameters:(OrganizationServiceSvc_GetViewOrgJobPositionById *)aParameters ;
- (void)GetViewOrgJobPositionByIdAsyncUsingParameters:(OrganizationServiceSvc_GetViewOrgJobPositionById *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgExperiencesUsingParameters:(OrganizationServiceSvc_GetAllOrgExperiences *)aParameters ;
- (void)GetAllOrgExperiencesAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgExperiences *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)CreateOrgExperienceUsingParameters:(OrganizationServiceSvc_CreateOrgExperience *)aParameters ;
- (void)CreateOrgExperienceAsyncUsingParameters:(OrganizationServiceSvc_CreateOrgExperience *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgExperienceUsingParameters:(OrganizationServiceSvc_GetAllOrgExperience *)aParameters ;
- (void)GetAllOrgExperienceAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgExperience *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgExperienceByOrgExperienceIdUsingParameters:(OrganizationServiceSvc_GetOrgExperienceByOrgExperienceId *)aParameters ;
- (void)GetOrgExperienceByOrgExperienceIdAsyncUsingParameters:(OrganizationServiceSvc_GetOrgExperienceByOrgExperienceId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgExperienceByProjectTypeAndTimeInChargeUsingParameters:(OrganizationServiceSvc_GetOrgExperienceByProjectTypeAndTimeInCharge *)aParameters ;
- (void)GetOrgExperienceByProjectTypeAndTimeInChargeAsyncUsingParameters:(OrganizationServiceSvc_GetOrgExperienceByProjectTypeAndTimeInCharge *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgQualificationsUsingParameters:(OrganizationServiceSvc_GetAllOrgQualifications *)aParameters ;
- (void)GetAllOrgQualificationsAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgQualifications *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)CreateOrgQualificationUsingParameters:(OrganizationServiceSvc_CreateOrgQualification *)aParameters ;
- (void)CreateOrgQualificationAsyncUsingParameters:(OrganizationServiceSvc_CreateOrgQualification *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetQualificationByMajorAndEducationLevelUsingParameters:(OrganizationServiceSvc_GetQualificationByMajorAndEducationLevel *)aParameters ;
- (void)GetQualificationByMajorAndEducationLevelAsyncUsingParameters:(OrganizationServiceSvc_GetQualificationByMajorAndEducationLevel *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgKnowledgeUsingParameters:(OrganizationServiceSvc_GetAllOrgKnowledge *)aParameters ;
- (void)GetAllOrgKnowledgeAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgKnowledge *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgKnowledgeByMajorUsingParameters:(OrganizationServiceSvc_GetAllOrgKnowledgeByMajor *)aParameters ;
- (void)GetAllOrgKnowledgeByMajorAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgKnowledgeByMajor *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgKnowledgeByPageUsingParameters:(OrganizationServiceSvc_GetOrgKnowledgeByPage *)aParameters ;
- (void)GetOrgKnowledgeByPageAsyncUsingParameters:(OrganizationServiceSvc_GetOrgKnowledgeByPage *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgKnowledgePageNumberUsingParameters:(OrganizationServiceSvc_GetOrgKnowledgePageNumber *)aParameters ;
- (void)GetOrgKnowledgePageNumberAsyncUsingParameters:(OrganizationServiceSvc_GetOrgKnowledgePageNumber *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)DeleteOrgKnowledgeUsingParameters:(OrganizationServiceSvc_DeleteOrgKnowledge *)aParameters ;
- (void)DeleteOrgKnowledgeAsyncUsingParameters:(OrganizationServiceSvc_DeleteOrgKnowledge *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgKnowledgeByIdUsingParameters:(OrganizationServiceSvc_GetOrgKnowledgeById *)aParameters ;
- (void)GetOrgKnowledgeByIdAsyncUsingParameters:(OrganizationServiceSvc_GetOrgKnowledgeById *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)UpdateOrgKnowledgeUsingParameters:(OrganizationServiceSvc_UpdateOrgKnowledge *)aParameters ;
- (void)UpdateOrgKnowledgeAsyncUsingParameters:(OrganizationServiceSvc_UpdateOrgKnowledge *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)CreateOrgKnowledgeUsingParameters:(OrganizationServiceSvc_CreateOrgKnowledge *)aParameters ;
- (void)CreateOrgKnowledgeAsyncUsingParameters:(OrganizationServiceSvc_CreateOrgKnowledge *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllRatingUsingParameters:(OrganizationServiceSvc_GetAllRating *)aParameters ;
- (void)GetAllRatingAsyncUsingParameters:(OrganizationServiceSvc_GetAllRating *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)DeleteOrgJobPositioRequiredProficencyUsingParameters:(OrganizationServiceSvc_DeleteOrgJobPositioRequiredProficency *)aParameters ;
- (void)DeleteOrgJobPositioRequiredProficencyAsyncUsingParameters:(OrganizationServiceSvc_DeleteOrgJobPositioRequiredProficency *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgJobPositionKnowledgeProficencyUsingParameters:(OrganizationServiceSvc_GetAllOrgJobPositionKnowledgeProficency *)aParameters ;
- (void)GetAllOrgJobPositionKnowledgeProficencyAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgJobPositionKnowledgeProficency *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)AddOrgJobPositionKnowledgeProficencyByViewUsingParameters:(OrganizationServiceSvc_AddOrgJobPositionKnowledgeProficencyByView *)aParameters ;
- (void)AddOrgJobPositionKnowledgeProficencyByViewAsyncUsingParameters:(OrganizationServiceSvc_AddOrgJobPositionKnowledgeProficencyByView *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)UpdateOrgJobPositionKnowledgeProficencyByViewUsingParameters:(OrganizationServiceSvc_UpdateOrgJobPositionKnowledgeProficencyByView *)aParameters ;
- (void)UpdateOrgJobPositionKnowledgeProficencyByViewAsyncUsingParameters:(OrganizationServiceSvc_UpdateOrgJobPositionKnowledgeProficencyByView *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgJobPositionExperienceProficencyUsingParameters:(OrganizationServiceSvc_GetAllOrgJobPositionExperienceProficency *)aParameters ;
- (void)GetAllOrgJobPositionExperienceProficencyAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgJobPositionExperienceProficency *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)AddOrgJobPositionExperienceProficencyByViewUsingParameters:(OrganizationServiceSvc_AddOrgJobPositionExperienceProficencyByView *)aParameters ;
- (void)AddOrgJobPositionExperienceProficencyByViewAsyncUsingParameters:(OrganizationServiceSvc_AddOrgJobPositionExperienceProficencyByView *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)UpdateOrgJobPositionExperienceProficencyByViewUsingParameters:(OrganizationServiceSvc_UpdateOrgJobPositionExperienceProficencyByView *)aParameters ;
- (void)UpdateOrgJobPositionExperienceProficencyByViewAsyncUsingParameters:(OrganizationServiceSvc_UpdateOrgJobPositionExperienceProficencyByView *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgJobPositionQualificationProficencyUsingParameters:(OrganizationServiceSvc_GetAllOrgJobPositionQualificationProficency *)aParameters ;
- (void)GetAllOrgJobPositionQualificationProficencyAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgJobPositionQualificationProficency *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)AddOrgJobPositionQualificationProficencyByViewUsingParameters:(OrganizationServiceSvc_AddOrgJobPositionQualificationProficencyByView *)aParameters ;
- (void)AddOrgJobPositionQualificationProficencyByViewAsyncUsingParameters:(OrganizationServiceSvc_AddOrgJobPositionQualificationProficencyByView *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)UpdateOrgJobPositionQualificationProficencyByViewUsingParameters:(OrganizationServiceSvc_UpdateOrgJobPositionQualificationProficencyByView *)aParameters ;
- (void)UpdateOrgJobPositionQualificationProficencyByViewAsyncUsingParameters:(OrganizationServiceSvc_UpdateOrgJobPositionQualificationProficencyByView *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgJobPositionSkillProficencyUsingParameters:(OrganizationServiceSvc_GetAllOrgJobPositionSkillProficency *)aParameters ;
- (void)GetAllOrgJobPositionSkillProficencyAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgJobPositionSkillProficency *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)AddOrgJobPositionSkillProficencyByViewUsingParameters:(OrganizationServiceSvc_AddOrgJobPositionSkillProficencyByView *)aParameters ;
- (void)AddOrgJobPositionSkillProficencyByViewAsyncUsingParameters:(OrganizationServiceSvc_AddOrgJobPositionSkillProficencyByView *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)UpdateOrgJobPositionSkillProficencyByViewUsingParameters:(OrganizationServiceSvc_UpdateOrgJobPositionSkillProficencyByView *)aParameters ;
- (void)UpdateOrgJobPositionSkillProficencyByViewAsyncUsingParameters:(OrganizationServiceSvc_UpdateOrgJobPositionSkillProficencyByView *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllTreeOrgJobTitlePositionUsingParameters:(OrganizationServiceSvc_GetAllTreeOrgJobTitlePosition *)aParameters ;
- (void)GetAllTreeOrgJobTitlePositionAsyncUsingParameters:(OrganizationServiceSvc_GetAllTreeOrgJobTitlePosition *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllTreeOrgJobAndWorkLevelUsingParameters:(OrganizationServiceSvc_GetAllTreeOrgJobAndWorkLevel *)aParameters ;
- (void)GetAllTreeOrgJobAndWorkLevelAsyncUsingParameters:(OrganizationServiceSvc_GetAllTreeOrgJobAndWorkLevel *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllTreeOrgUnitPositionUsingParameters:(OrganizationServiceSvc_GetAllTreeOrgUnitPosition *)aParameters ;
- (void)GetAllTreeOrgUnitPositionAsyncUsingParameters:(OrganizationServiceSvc_GetAllTreeOrgUnitPosition *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllTreeOrgJobAndPositionUsingParameters:(OrganizationServiceSvc_GetAllTreeOrgJobAndPosition *)aParameters ;
- (void)GetAllTreeOrgJobAndPositionAsyncUsingParameters:(OrganizationServiceSvc_GetAllTreeOrgJobAndPosition *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgGroupUsingParameters:(OrganizationServiceSvc_GetAllOrgGroup *)aParameters ;
- (void)GetAllOrgGroupAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgGroup *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgGroupByPageUsingParameters:(OrganizationServiceSvc_GetOrgGroupByPage *)aParameters ;
- (void)GetOrgGroupByPageAsyncUsingParameters:(OrganizationServiceSvc_GetOrgGroupByPage *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgGroupKendoPageDataUsingParameters:(OrganizationServiceSvc_GetOrgGroupKendoPageData *)aParameters ;
- (void)GetOrgGroupKendoPageDataAsyncUsingParameters:(OrganizationServiceSvc_GetOrgGroupKendoPageData *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgGroupPageNumberUsingParameters:(OrganizationServiceSvc_GetOrgGroupPageNumber *)aParameters ;
- (void)GetOrgGroupPageNumberAsyncUsingParameters:(OrganizationServiceSvc_GetOrgGroupPageNumber *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)DeleteOrgGroupUsingParameters:(OrganizationServiceSvc_DeleteOrgGroup *)aParameters ;
- (void)DeleteOrgGroupAsyncUsingParameters:(OrganizationServiceSvc_DeleteOrgGroup *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgGroupByIdUsingParameters:(OrganizationServiceSvc_GetOrgGroupById *)aParameters ;
- (void)GetOrgGroupByIdAsyncUsingParameters:(OrganizationServiceSvc_GetOrgGroupById *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)UpdateOrgGroupUsingParameters:(OrganizationServiceSvc_UpdateOrgGroup *)aParameters ;
- (void)UpdateOrgGroupAsyncUsingParameters:(OrganizationServiceSvc_UpdateOrgGroup *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)CreateOrgGroupUsingParameters:(OrganizationServiceSvc_CreateOrgGroup *)aParameters ;
- (void)CreateOrgGroupAsyncUsingParameters:(OrganizationServiceSvc_CreateOrgGroup *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllOrgUnitsUsingParameters:(OrganizationServiceSvc_GetAllOrgUnits *)aParameters ;
- (void)GetAllOrgUnitsAsyncUsingParameters:(OrganizationServiceSvc_GetAllOrgUnits *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)DeleteOrgWorkLevelUsingParameters:(OrganizationServiceSvc_DeleteOrgWorkLevel *)aParameters ;
- (void)DeleteOrgWorkLevelAsyncUsingParameters:(OrganizationServiceSvc_DeleteOrgWorkLevel *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetOrgWorkLevelByIdUsingParameters:(OrganizationServiceSvc_GetOrgWorkLevelById *)aParameters ;
- (void)GetOrgWorkLevelByIdAsyncUsingParameters:(OrganizationServiceSvc_GetOrgWorkLevelById *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)CreateOrgWorkLevelUsingParameters:(OrganizationServiceSvc_CreateOrgWorkLevel *)aParameters ;
- (void)CreateOrgWorkLevelAsyncUsingParameters:(OrganizationServiceSvc_CreateOrgWorkLevel *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllWorkLevelByOrgJobIdUsingParameters:(OrganizationServiceSvc_GetAllWorkLevelByOrgJobId *)aParameters ;
- (void)GetAllWorkLevelByOrgJobIdAsyncUsingParameters:(OrganizationServiceSvc_GetAllWorkLevelByOrgJobId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetAllEmployeeByOrgWorkLevelIdUsingParameters:(OrganizationServiceSvc_GetAllEmployeeByOrgWorkLevelId *)aParameters ;
- (void)GetAllEmployeeByOrgWorkLevelIdAsyncUsingParameters:(OrganizationServiceSvc_GetAllEmployeeByOrgWorkLevelId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
- (BasicHttpBinding_IOrganizationServiceBindingResponse *)GetViewEmployeeByJobPositionIdUsingParameters:(OrganizationServiceSvc_GetViewEmployeeByJobPositionId *)aParameters ;
- (void)GetViewEmployeeByJobPositionIdAsyncUsingParameters:(OrganizationServiceSvc_GetViewEmployeeByJobPositionId *)aParameters  delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)responseDelegate;
@end
@interface BasicHttpBinding_IOrganizationServiceBindingOperation : NSOperation {
	BasicHttpBinding_IOrganizationServiceBinding *binding;
	BasicHttpBinding_IOrganizationServiceBindingResponse *response;
	id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate> delegate;
	NSMutableData *responseData;
	NSURLConnection *urlConnection;
}
@property (retain) BasicHttpBinding_IOrganizationServiceBinding *binding;
@property (readonly) BasicHttpBinding_IOrganizationServiceBindingResponse *response;
@property (nonatomic, assign) id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate> delegate;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, retain) NSURLConnection *urlConnection;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetEmployeeByListOrgUnitId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetEmployeeByListOrgUnitId * parameters;
}
@property (retain) OrganizationServiceSvc_GetEmployeeByListOrgUnitId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetEmployeeByListOrgUnitId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_SearchOrgJob : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_SearchOrgJob * parameters;
}
@property (retain) OrganizationServiceSvc_SearchOrgJob * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_SearchOrgJob *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_SearchOrgJobPosition : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_SearchOrgJobPosition * parameters;
}
@property (retain) OrganizationServiceSvc_SearchOrgJobPosition * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_SearchOrgJobPosition *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_SearchOrgUnit : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_SearchOrgUnit * parameters;
}
@property (retain) OrganizationServiceSvc_SearchOrgUnit * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_SearchOrgUnit *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllCLogRatings : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllCLogRatings * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllCLogRatings * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllCLogRatings *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgProficencyTypes : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgProficencyTypes * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgProficencyTypes * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgProficencyTypes *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllSpecificCompetencyByJobPositionId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllSpecificCompetencyByJobPositionId * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllSpecificCompetencyByJobPositionId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllSpecificCompetencyByJobPositionId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgGroupCompetency : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgGroupCompetency * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgGroupCompetency * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgGroupCompetency *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllCompetencyByJobId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllCompetencyByJobId * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllCompetencyByJobId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllCompetencyByJobId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllJobSpecificCompetencyByCompId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllJobSpecificCompetencyByCompId * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllJobSpecificCompetencyByCompId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllJobSpecificCompetencyByCompId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllJobPosSpecificCompetencyByJobIdCompId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllJobPosSpecificCompetencyByJobIdCompId * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllJobPosSpecificCompetencyByJobIdCompId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllJobPosSpecificCompetencyByJobIdCompId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllCompetencyLevelByCompetencyId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllCompetencyLevelByCompetencyId * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllCompetencyLevelByCompetencyId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllCompetencyLevelByCompetencyId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllCompetencyLevelDetailByLevelId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllCompetencyLevelDetailByLevelId * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllCompetencyLevelDetailByLevelId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllCompetencyLevelDetailByLevelId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllV_OrgSkillCompetencyBy : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllV_OrgSkillCompetencyBy * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllV_OrgSkillCompetencyBy * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllV_OrgSkillCompetencyBy *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllV_OrgQualificationCompetencyBy : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllV_OrgQualificationCompetencyBy * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllV_OrgQualificationCompetencyBy * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllV_OrgQualificationCompetencyBy *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllV_OrgExperienceCompetencyBy : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllV_OrgExperienceCompetencyBy * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllV_OrgExperienceCompetencyBy * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllV_OrgExperienceCompetencyBy *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllV_OrgDegreeCompetencyBy : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllV_OrgDegreeCompetencyBy * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllV_OrgDegreeCompetencyBy * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllV_OrgDegreeCompetencyBy *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllV_OrgAttitudeCompetencyBy : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllV_OrgAttitudeCompetencyBy * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllV_OrgAttitudeCompetencyBy * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllV_OrgAttitudeCompetencyBy *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllV_OrgKnowledgeCompetencyBy : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllV_OrgKnowledgeCompetencyBy * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllV_OrgKnowledgeCompetencyBy * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllV_OrgKnowledgeCompetencyBy *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllJobPosCompetency : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllJobPosCompetency * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllJobPosCompetency * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllJobPosCompetency *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllCoreCompetency : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllCoreCompetency * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllCoreCompetency * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllCoreCompetency *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllJobPosCompetencyByGroupId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllJobPosCompetencyByGroupId * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllJobPosCompetencyByGroupId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllJobPosCompetencyByGroupId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllCoreCompetencyByGroupId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllCoreCompetencyByGroupId * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllCoreCompetencyByGroupId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllCoreCompetencyByGroupId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetJobPosCompetencyById : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetJobPosCompetencyById * parameters;
}
@property (retain) OrganizationServiceSvc_GetJobPosCompetencyById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetJobPosCompetencyById *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllJobPositionByJobId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllJobPositionByJobId * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllJobPositionByJobId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllJobPositionByJobId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_EditJobPosCompetency : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_EditJobPosCompetency * parameters;
}
@property (retain) OrganizationServiceSvc_EditJobPosCompetency * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_EditJobPosCompetency *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetDirecReportById : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetDirecReportById * parameters;
}
@property (retain) OrganizationServiceSvc_GetDirecReportById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetDirecReportById *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_FindLevelOfGenOfPSCd : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_FindLevelOfGenOfPSCd * parameters;
}
@property (retain) OrganizationServiceSvc_FindLevelOfGenOfPSCd * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_FindLevelOfGenOfPSCd *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_IsAscendant : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_IsAscendant * parameters;
}
@property (retain) OrganizationServiceSvc_IsAscendant * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_IsAscendant *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_IsDescendant : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_IsDescendant * parameters;
}
@property (retain) OrganizationServiceSvc_IsDescendant * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_IsDescendant *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_IsXthAscendant : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_IsXthAscendant * parameters;
}
@property (retain) OrganizationServiceSvc_IsXthAscendant * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_IsXthAscendant *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_IsXthDescendant : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_IsXthDescendant * parameters;
}
@property (retain) OrganizationServiceSvc_IsXthDescendant * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_IsXthDescendant *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetXAscendantFromGenPSCd : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetXAscendantFromGenPSCd * parameters;
}
@property (retain) OrganizationServiceSvc_GetXAscendantFromGenPSCd * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetXAscendantFromGenPSCd *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetXDescendanceFromGenPSCd : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetXDescendanceFromGenPSCd * parameters;
}
@property (retain) OrganizationServiceSvc_GetXDescendanceFromGenPSCd * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetXDescendanceFromGenPSCd *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllGenPSHasTheSameLevel : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllGenPSHasTheSameLevel * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllGenPSHasTheSameLevel * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllGenPSHasTheSameLevel *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllGenPSHasTheSameLevelAndParentNode : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllGenPSHasTheSameLevelAndParentNode * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllGenPSHasTheSameLevelAndParentNode * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllGenPSHasTheSameLevelAndParentNode *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetListOfPersonelStruct : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetListOfPersonelStruct * parameters;
}
@property (retain) OrganizationServiceSvc_GetListOfPersonelStruct * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetListOfPersonelStruct *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetListOfEmployeeMappingPersonelStruct : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetListOfEmployeeMappingPersonelStruct * parameters;
}
@property (retain) OrganizationServiceSvc_GetListOfEmployeeMappingPersonelStruct * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetListOfEmployeeMappingPersonelStruct *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllTreeOrgJobTitle : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllTreeOrgJobTitle * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllTreeOrgJobTitle * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllTreeOrgJobTitle *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetViewOrgJobTitleByCode : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetViewOrgJobTitleByCode * parameters;
}
@property (retain) OrganizationServiceSvc_GetViewOrgJobTitleByCode * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetViewOrgJobTitleByCode *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetListOrgJobTitleGroups : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetListOrgJobTitleGroups * parameters;
}
@property (retain) OrganizationServiceSvc_GetListOrgJobTitleGroups * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetListOrgJobTitleGroups *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetListOrgJobTitleLevels : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetListOrgJobTitleLevels * parameters;
}
@property (retain) OrganizationServiceSvc_GetListOrgJobTitleLevels * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetListOrgJobTitleLevels *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_AddOrgJobTitle : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_AddOrgJobTitle * parameters;
}
@property (retain) OrganizationServiceSvc_AddOrgJobTitle * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_AddOrgJobTitle *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_UpdateOrgJobTitle : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_UpdateOrgJobTitle * parameters;
}
@property (retain) OrganizationServiceSvc_UpdateOrgJobTitle * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_UpdateOrgJobTitle *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_DeleteOrgJobTitle : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_DeleteOrgJobTitle * parameters;
}
@property (retain) OrganizationServiceSvc_DeleteOrgJobTitle * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_DeleteOrgJobTitle *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetListAllOrgJobTitles : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetListAllOrgJobTitles * parameters;
}
@property (retain) OrganizationServiceSvc_GetListAllOrgJobTitles * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetListAllOrgJobTitles *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_SearchOrgJobTitle : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_SearchOrgJobTitle * parameters;
}
@property (retain) OrganizationServiceSvc_SearchOrgJobTitle * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_SearchOrgJobTitle *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetListViewEmployeeByOrgJobTitleCode : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetListViewEmployeeByOrgJobTitleCode * parameters;
}
@property (retain) OrganizationServiceSvc_GetListViewEmployeeByOrgJobTitleCode * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetListViewEmployeeByOrgJobTitleCode *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllClassificationEmpTree : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllClassificationEmpTree * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllClassificationEmpTree * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllClassificationEmpTree *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllClassificationEmpMapping : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllClassificationEmpMapping * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllClassificationEmpMapping * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllClassificationEmpMapping *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgChangeInfos : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgChangeInfos * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgChangeInfos * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgChangeInfos *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllChildrenReportEmployeeByParentId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllChildrenReportEmployeeByParentId * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllChildrenReportEmployeeByParentId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllChildrenReportEmployeeByParentId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllDescendantsReportEmployeeByParentId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllDescendantsReportEmployeeByParentId * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllDescendantsReportEmployeeByParentId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllDescendantsReportEmployeeByParentId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetReportEmployeeRoot : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetReportEmployeeRoot * parameters;
}
@property (retain) OrganizationServiceSvc_GetReportEmployeeRoot * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetReportEmployeeRoot *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllUnits : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllUnits * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllUnits * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllUnits *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllChildrenOrgUnitByParentId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllChildrenOrgUnitByParentId * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllChildrenOrgUnitByParentId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllChildrenOrgUnitByParentId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllDescendantsOrgUnitByParentId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllDescendantsOrgUnitByParentId * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllDescendantsOrgUnitByParentId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllDescendantsOrgUnitByParentId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgUnitRoot : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgUnitRoot * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgUnitRoot * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgUnitRoot *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetUnitLeader : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetUnitLeader * parameters;
}
@property (retain) OrganizationServiceSvc_GetUnitLeader * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetUnitLeader *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllUnitsPosition : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllUnitsPosition * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllUnitsPosition * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllUnitsPosition *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetViewOrgUnitByOrgUnitId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetViewOrgUnitByOrgUnitId * parameters;
}
@property (retain) OrganizationServiceSvc_GetViewOrgUnitByOrgUnitId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetViewOrgUnitByOrgUnitId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgUnitJobsByOrgUnitId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgUnitJobsByOrgUnitId * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgUnitJobsByOrgUnitId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgUnitJobsByOrgUnitId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetEmployeeKendoFilterByJobId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetEmployeeKendoFilterByJobId * parameters;
}
@property (retain) OrganizationServiceSvc_GetEmployeeKendoFilterByJobId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetEmployeeKendoFilterByJobId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgJobByPage : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgJobByPage * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgJobByPage * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgJobByPage *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetViewOrgUnitJobByOrgUnitId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetViewOrgUnitJobByOrgUnitId * parameters;
}
@property (retain) OrganizationServiceSvc_GetViewOrgUnitJobByOrgUnitId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetViewOrgUnitJobByOrgUnitId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_CreateOrgUnitJob : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_CreateOrgUnitJob * parameters;
}
@property (retain) OrganizationServiceSvc_CreateOrgUnitJob * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_CreateOrgUnitJob *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgUnitJobs : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgUnitJobs * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgUnitJobs * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgUnitJobs *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgUnitsByPage : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgUnitsByPage * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgUnitsByPage * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgUnitsByPage *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgUnitJobByOrgUnitId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgUnitJobByOrgUnitId * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgUnitJobByOrgUnitId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgUnitJobByOrgUnitId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgUnitTreeByOrgUnitId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgUnitTreeByOrgUnitId * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgUnitTreeByOrgUnitId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgUnitTreeByOrgUnitId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgUnitByOrgUnitId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgUnitByOrgUnitId * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgUnitByOrgUnitId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgUnitByOrgUnitId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgUnitTree : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgUnitTree * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgUnitTree * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgUnitTree *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgUnitTreeByPermission : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgUnitTreeByPermission * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgUnitTreeByPermission * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgUnitTreeByPermission *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_EditOrgUnit : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_EditOrgUnit * parameters;
}
@property (retain) OrganizationServiceSvc_EditOrgUnit * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_EditOrgUnit *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_TransferOrgUnit : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_TransferOrgUnit * parameters;
}
@property (retain) OrganizationServiceSvc_TransferOrgUnit * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_TransferOrgUnit *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_DeleteOrgUnit : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_DeleteOrgUnit * parameters;
}
@property (retain) OrganizationServiceSvc_DeleteOrgUnit * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_DeleteOrgUnit *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_DeleteOrgUnitJob : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_DeleteOrgUnitJob * parameters;
}
@property (retain) OrganizationServiceSvc_DeleteOrgUnitJob * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_DeleteOrgUnitJob *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgSkillByOrgSkillId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgSkillByOrgSkillId * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgSkillByOrgSkillId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgSkillByOrgSkillId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgSkillsBySkillType : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgSkillsBySkillType * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgSkillsBySkillType * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgSkillsBySkillType *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgSkill : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgSkill * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgSkill * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgSkill *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgSkillByPage : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgSkillByPage * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgSkillByPage * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgSkillByPage *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgSkillKendoPageData : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgSkillKendoPageData * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgSkillKendoPageData * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgSkillKendoPageData *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgSkillPageNumber : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgSkillPageNumber * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgSkillPageNumber * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgSkillPageNumber *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_DeleteOrgSkill : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_DeleteOrgSkill * parameters;
}
@property (retain) OrganizationServiceSvc_DeleteOrgSkill * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_DeleteOrgSkill *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgSkillById : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgSkillById * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgSkillById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgSkillById *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_UpdateOrgSkill : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_UpdateOrgSkill * parameters;
}
@property (retain) OrganizationServiceSvc_UpdateOrgSkill * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_UpdateOrgSkill *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_CreateOrgSkill : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_CreateOrgSkill * parameters;
}
@property (retain) OrganizationServiceSvc_CreateOrgSkill * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_CreateOrgSkill *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgTimeInCharge : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgTimeInCharge * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgTimeInCharge * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgTimeInCharge *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgSkillType : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgSkillType * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgSkillType * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgSkillType *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgSkillTypeByOrgSkillTypeId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgSkillTypeByOrgSkillTypeId * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgSkillTypeByOrgSkillTypeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgSkillTypeByOrgSkillTypeId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgProjectType : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgProjectType * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgProjectType * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgProjectType *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgProjectTypeByOrgProjectTypeId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgProjectTypeByOrgProjectTypeId * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgProjectTypeByOrgProjectTypeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgProjectTypeByOrgProjectTypeId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllCLogCareer : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllCLogCareer * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllCLogCareer * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllCLogCareer *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllEmployeeByCareerCode : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllEmployeeByCareerCode * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllEmployeeByCareerCode * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllEmployeeByCareerCode *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllTimeInCharge : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllTimeInCharge * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllTimeInCharge * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllTimeInCharge *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgDegree : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgDegree * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgDegree * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgDegree *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgDegreeByOrgDegreeId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgDegreeByOrgDegreeId * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgDegreeByOrgDegreeId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgDegreeByOrgDegreeId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgDegreeRank : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgDegreeRank * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgDegreeRank * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgDegreeRank *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgDegreeRankByOrgDegreeRankId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgDegreeRankByOrgDegreeRankId * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgDegreeRankByOrgDegreeRankId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgDegreeRankByOrgDegreeRankId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgJobs : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgJobs * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgJobs * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgJobs *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetViewOrgJobs : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetViewOrgJobs * parameters;
}
@property (retain) OrganizationServiceSvc_GetViewOrgJobs * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetViewOrgJobs *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetViewOrgJobById : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetViewOrgJobById * parameters;
}
@property (retain) OrganizationServiceSvc_GetViewOrgJobById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetViewOrgJobById *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllViewOrgJobByCareerId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllViewOrgJobByCareerId * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllViewOrgJobByCareerId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllViewOrgJobByCareerId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgJobByCareer : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgJobByCareer * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgJobByCareer * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgJobByCareer *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgJobById : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgJobById * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgJobById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgJobById *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgJobByCode : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgJobByCode * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgJobByCode * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgJobByCode *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgJobDescriptionDetailForJob : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgJobDescriptionDetailForJob * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgJobDescriptionDetailForJob * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgJobDescriptionDetailForJob *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgJobDescriptionDetailForJobPosition : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgJobDescriptionDetailForJobPosition * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgJobDescriptionDetailForJobPosition * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgJobDescriptionDetailForJobPosition *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_CreateNewOrgJob : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_CreateNewOrgJob * parameters;
}
@property (retain) OrganizationServiceSvc_CreateNewOrgJob * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_CreateNewOrgJob *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_UpdateOrgJob : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_UpdateOrgJob * parameters;
}
@property (retain) OrganizationServiceSvc_UpdateOrgJob * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_UpdateOrgJob *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_DeleteOrgJob : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_DeleteOrgJob * parameters;
}
@property (retain) OrganizationServiceSvc_DeleteOrgJob * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_DeleteOrgJob *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllTreeOrgCareerJobAndWorkLevel : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllTreeOrgCareerJobAndWorkLevel * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllTreeOrgCareerJobAndWorkLevel * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllTreeOrgCareerJobAndWorkLevel *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllEmployeeByOrgJobId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllEmployeeByOrgJobId * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllEmployeeByOrgJobId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllEmployeeByOrgJobId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgJobPositions : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgJobPositions * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgJobPositions * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgJobPositions *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetListOrgJobPosByOrgJobId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetListOrgJobPosByOrgJobId * parameters;
}
@property (retain) OrganizationServiceSvc_GetListOrgJobPosByOrgJobId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetListOrgJobPosByOrgJobId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgJobPosByOrgUnitId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgJobPosByOrgUnitId * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgJobPosByOrgUnitId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgJobPosByOrgUnitId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllJobPositionByOrgUnitId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllJobPositionByOrgUnitId * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllJobPositionByOrgUnitId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllJobPositionByOrgUnitId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllJobPositionByOrgJobId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllJobPositionByOrgJobId * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllJobPositionByOrgJobId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllJobPositionByOrgJobId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllJobPositionByWorkLevelId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllJobPositionByWorkLevelId * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllJobPositionByWorkLevelId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllJobPositionByWorkLevelId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllJobPositionByJobTitleId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllJobPositionByJobTitleId * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllJobPositionByJobTitleId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllJobPositionByJobTitleId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllViewOrgJobPosition : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllViewOrgJobPosition * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllViewOrgJobPosition * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllViewOrgJobPosition *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllEmployeeByPositionId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllEmployeeByPositionId * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllEmployeeByPositionId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllEmployeeByPositionId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_AddOrgJobPosition : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_AddOrgJobPosition * parameters;
}
@property (retain) OrganizationServiceSvc_AddOrgJobPosition * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_AddOrgJobPosition *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_UpdateOrgJobPosition : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_UpdateOrgJobPosition * parameters;
}
@property (retain) OrganizationServiceSvc_UpdateOrgJobPosition * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_UpdateOrgJobPosition *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_DeleteOrgJobPosition : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_DeleteOrgJobPosition * parameters;
}
@property (retain) OrganizationServiceSvc_DeleteOrgJobPosition * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_DeleteOrgJobPosition *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrganizationJobPosition : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrganizationJobPosition * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrganizationJobPosition * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrganizationJobPosition *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgJobPositionById : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgJobPositionById * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgJobPositionById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgJobPositionById *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetViewOrgJobPositionDetailById : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetViewOrgJobPositionDetailById * parameters;
}
@property (retain) OrganizationServiceSvc_GetViewOrgJobPositionDetailById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetViewOrgJobPositionDetailById *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetViewOrgJobPositionById : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetViewOrgJobPositionById * parameters;
}
@property (retain) OrganizationServiceSvc_GetViewOrgJobPositionById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetViewOrgJobPositionById *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgExperiences : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgExperiences * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgExperiences * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgExperiences *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_CreateOrgExperience : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_CreateOrgExperience * parameters;
}
@property (retain) OrganizationServiceSvc_CreateOrgExperience * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_CreateOrgExperience *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgExperience : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgExperience * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgExperience * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgExperience *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgExperienceByOrgExperienceId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgExperienceByOrgExperienceId * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgExperienceByOrgExperienceId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgExperienceByOrgExperienceId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgExperienceByProjectTypeAndTimeInCharge : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgExperienceByProjectTypeAndTimeInCharge * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgExperienceByProjectTypeAndTimeInCharge * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgExperienceByProjectTypeAndTimeInCharge *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgQualifications : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgQualifications * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgQualifications * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgQualifications *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_CreateOrgQualification : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_CreateOrgQualification * parameters;
}
@property (retain) OrganizationServiceSvc_CreateOrgQualification * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_CreateOrgQualification *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetQualificationByMajorAndEducationLevel : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetQualificationByMajorAndEducationLevel * parameters;
}
@property (retain) OrganizationServiceSvc_GetQualificationByMajorAndEducationLevel * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetQualificationByMajorAndEducationLevel *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgKnowledge : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgKnowledge * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgKnowledge * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgKnowledge *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgKnowledgeByMajor : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgKnowledgeByMajor * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgKnowledgeByMajor * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgKnowledgeByMajor *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgKnowledgeByPage : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgKnowledgeByPage * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgKnowledgeByPage * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgKnowledgeByPage *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgKnowledgePageNumber : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgKnowledgePageNumber * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgKnowledgePageNumber * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgKnowledgePageNumber *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_DeleteOrgKnowledge : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_DeleteOrgKnowledge * parameters;
}
@property (retain) OrganizationServiceSvc_DeleteOrgKnowledge * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_DeleteOrgKnowledge *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgKnowledgeById : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgKnowledgeById * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgKnowledgeById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgKnowledgeById *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_UpdateOrgKnowledge : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_UpdateOrgKnowledge * parameters;
}
@property (retain) OrganizationServiceSvc_UpdateOrgKnowledge * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_UpdateOrgKnowledge *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_CreateOrgKnowledge : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_CreateOrgKnowledge * parameters;
}
@property (retain) OrganizationServiceSvc_CreateOrgKnowledge * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_CreateOrgKnowledge *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllRating : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllRating * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllRating * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllRating *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_DeleteOrgJobPositioRequiredProficency : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_DeleteOrgJobPositioRequiredProficency * parameters;
}
@property (retain) OrganizationServiceSvc_DeleteOrgJobPositioRequiredProficency * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_DeleteOrgJobPositioRequiredProficency *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgJobPositionKnowledgeProficency : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgJobPositionKnowledgeProficency * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgJobPositionKnowledgeProficency * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgJobPositionKnowledgeProficency *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_AddOrgJobPositionKnowledgeProficencyByView : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_AddOrgJobPositionKnowledgeProficencyByView * parameters;
}
@property (retain) OrganizationServiceSvc_AddOrgJobPositionKnowledgeProficencyByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_AddOrgJobPositionKnowledgeProficencyByView *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_UpdateOrgJobPositionKnowledgeProficencyByView : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_UpdateOrgJobPositionKnowledgeProficencyByView * parameters;
}
@property (retain) OrganizationServiceSvc_UpdateOrgJobPositionKnowledgeProficencyByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_UpdateOrgJobPositionKnowledgeProficencyByView *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgJobPositionExperienceProficency : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgJobPositionExperienceProficency * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgJobPositionExperienceProficency * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgJobPositionExperienceProficency *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_AddOrgJobPositionExperienceProficencyByView : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_AddOrgJobPositionExperienceProficencyByView * parameters;
}
@property (retain) OrganizationServiceSvc_AddOrgJobPositionExperienceProficencyByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_AddOrgJobPositionExperienceProficencyByView *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_UpdateOrgJobPositionExperienceProficencyByView : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_UpdateOrgJobPositionExperienceProficencyByView * parameters;
}
@property (retain) OrganizationServiceSvc_UpdateOrgJobPositionExperienceProficencyByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_UpdateOrgJobPositionExperienceProficencyByView *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgJobPositionQualificationProficency : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgJobPositionQualificationProficency * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgJobPositionQualificationProficency * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgJobPositionQualificationProficency *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_AddOrgJobPositionQualificationProficencyByView : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_AddOrgJobPositionQualificationProficencyByView * parameters;
}
@property (retain) OrganizationServiceSvc_AddOrgJobPositionQualificationProficencyByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_AddOrgJobPositionQualificationProficencyByView *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_UpdateOrgJobPositionQualificationProficencyByView : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_UpdateOrgJobPositionQualificationProficencyByView * parameters;
}
@property (retain) OrganizationServiceSvc_UpdateOrgJobPositionQualificationProficencyByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_UpdateOrgJobPositionQualificationProficencyByView *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgJobPositionSkillProficency : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgJobPositionSkillProficency * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgJobPositionSkillProficency * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgJobPositionSkillProficency *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_AddOrgJobPositionSkillProficencyByView : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_AddOrgJobPositionSkillProficencyByView * parameters;
}
@property (retain) OrganizationServiceSvc_AddOrgJobPositionSkillProficencyByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_AddOrgJobPositionSkillProficencyByView *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_UpdateOrgJobPositionSkillProficencyByView : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_UpdateOrgJobPositionSkillProficencyByView * parameters;
}
@property (retain) OrganizationServiceSvc_UpdateOrgJobPositionSkillProficencyByView * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_UpdateOrgJobPositionSkillProficencyByView *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllTreeOrgJobTitlePosition : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllTreeOrgJobTitlePosition * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllTreeOrgJobTitlePosition * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllTreeOrgJobTitlePosition *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllTreeOrgJobAndWorkLevel : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllTreeOrgJobAndWorkLevel * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllTreeOrgJobAndWorkLevel * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllTreeOrgJobAndWorkLevel *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllTreeOrgUnitPosition : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllTreeOrgUnitPosition * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllTreeOrgUnitPosition * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllTreeOrgUnitPosition *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllTreeOrgJobAndPosition : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllTreeOrgJobAndPosition * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllTreeOrgJobAndPosition * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllTreeOrgJobAndPosition *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgGroup : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgGroup * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgGroup * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgGroup *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgGroupByPage : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgGroupByPage * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgGroupByPage * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgGroupByPage *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgGroupKendoPageData : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgGroupKendoPageData * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgGroupKendoPageData * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgGroupKendoPageData *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgGroupPageNumber : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgGroupPageNumber * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgGroupPageNumber * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgGroupPageNumber *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_DeleteOrgGroup : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_DeleteOrgGroup * parameters;
}
@property (retain) OrganizationServiceSvc_DeleteOrgGroup * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_DeleteOrgGroup *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgGroupById : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgGroupById * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgGroupById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgGroupById *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_UpdateOrgGroup : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_UpdateOrgGroup * parameters;
}
@property (retain) OrganizationServiceSvc_UpdateOrgGroup * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_UpdateOrgGroup *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_CreateOrgGroup : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_CreateOrgGroup * parameters;
}
@property (retain) OrganizationServiceSvc_CreateOrgGroup * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_CreateOrgGroup *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllOrgUnits : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllOrgUnits * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllOrgUnits * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllOrgUnits *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_DeleteOrgWorkLevel : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_DeleteOrgWorkLevel * parameters;
}
@property (retain) OrganizationServiceSvc_DeleteOrgWorkLevel * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_DeleteOrgWorkLevel *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetOrgWorkLevelById : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetOrgWorkLevelById * parameters;
}
@property (retain) OrganizationServiceSvc_GetOrgWorkLevelById * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetOrgWorkLevelById *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_CreateOrgWorkLevel : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_CreateOrgWorkLevel * parameters;
}
@property (retain) OrganizationServiceSvc_CreateOrgWorkLevel * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_CreateOrgWorkLevel *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllWorkLevelByOrgJobId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllWorkLevelByOrgJobId * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllWorkLevelByOrgJobId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllWorkLevelByOrgJobId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetAllEmployeeByOrgWorkLevelId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetAllEmployeeByOrgWorkLevelId * parameters;
}
@property (retain) OrganizationServiceSvc_GetAllEmployeeByOrgWorkLevelId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetAllEmployeeByOrgWorkLevelId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_GetViewEmployeeByJobPositionId : BasicHttpBinding_IOrganizationServiceBindingOperation {
	OrganizationServiceSvc_GetViewEmployeeByJobPositionId * parameters;
}
@property (retain) OrganizationServiceSvc_GetViewEmployeeByJobPositionId * parameters;
- (id)initWithBinding:(BasicHttpBinding_IOrganizationServiceBinding *)aBinding delegate:(id<BasicHttpBinding_IOrganizationServiceBindingResponseDelegate>)aDelegate
	parameters:(OrganizationServiceSvc_GetViewEmployeeByJobPositionId *)aParameters
;
@end
@interface BasicHttpBinding_IOrganizationServiceBinding_envelope : NSObject {
}
+ (BasicHttpBinding_IOrganizationServiceBinding_envelope *)sharedInstance;
- (NSString *)serializedFormUsingHeaderElements:(NSDictionary *)headerElements bodyElements:(NSDictionary *)bodyElements;
@end
@interface BasicHttpBinding_IOrganizationServiceBindingResponse : NSObject {
	NSArray *headers;
	NSArray *bodyParts;
	NSError *error;
}
@property (retain) NSArray *headers;
@property (retain) NSArray *bodyParts;
@property (retain) NSError *error;
@end
