#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class org_tns4_ArrayOfOrgJobTitlePositionTreeModel;
@class org_tns4_OrgJobTitlePositionTreeModel;
@class org_tns4_ArrayOfOrgJobTitleModel;
@class org_tns4_OrgJobTitleModel;
#import "org_tns2.h"
@interface org_tns4_OrgJobTitleModel : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * OrgJobTitleCode;
	NSString * OrgJobTitleDesc;
	NSString * OrgJobTitleGroupCode;
	NSNumber * OrgJobTitleId;
	NSString * OrgJobTitleLevelCode;
	NSString * OrgJobTitleNameEN;
	NSString * OrgJobTitleNameVN;
	org_tns2_ArrayOfOrgJobPosition * items;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns4_OrgJobTitleModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OrgJobTitleCode;
@property (retain) NSString * OrgJobTitleDesc;
@property (retain) NSString * OrgJobTitleGroupCode;
@property (retain) NSNumber * OrgJobTitleId;
@property (retain) NSString * OrgJobTitleLevelCode;
@property (retain) NSString * OrgJobTitleNameEN;
@property (retain) NSString * OrgJobTitleNameVN;
@property (retain) org_tns2_ArrayOfOrgJobPosition * items;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns4_ArrayOfOrgJobTitleModel : NSObject {
	
/* elements */
	NSMutableArray *OrgJobTitleModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns4_ArrayOfOrgJobTitleModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgJobTitleModel:(org_tns4_OrgJobTitleModel *)toAdd;
@property (readonly) NSMutableArray * OrgJobTitleModel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns4_OrgJobTitlePositionTreeModel : NSObject {
	
/* elements */
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * GROUP_CODE;
	NSString * GROUP_NAME_EN;
	NSString * GROUP_NAME_VN;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * SysParamCode;
	NSNumber * SysParamCompanyId;
	NSString * SysParamDesc;
	NSNumber * SysParamId;
	NSString * SysParamKey;
	NSNumber * SysParamOrder;
	org_tns4_ArrayOfOrgJobTitleModel * items;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns4_OrgJobTitlePositionTreeModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * GROUP_CODE;
@property (retain) NSString * GROUP_NAME_EN;
@property (retain) NSString * GROUP_NAME_VN;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * SysParamCode;
@property (retain) NSNumber * SysParamCompanyId;
@property (retain) NSString * SysParamDesc;
@property (retain) NSNumber * SysParamId;
@property (retain) NSString * SysParamKey;
@property (retain) NSNumber * SysParamOrder;
@property (retain) org_tns4_ArrayOfOrgJobTitleModel * items;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns4_ArrayOfOrgJobTitlePositionTreeModel : NSObject {
	
/* elements */
	NSMutableArray *OrgJobTitlePositionTreeModel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns4_ArrayOfOrgJobTitlePositionTreeModel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgJobTitlePositionTreeModel:(org_tns4_OrgJobTitlePositionTreeModel *)toAdd;
@property (readonly) NSMutableArray * OrgJobTitlePositionTreeModel;
/* attributes */
- (NSDictionary *)attributes;
@end
