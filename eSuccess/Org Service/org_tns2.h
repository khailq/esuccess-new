#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class org_tns2_ArrayOfV_Employee;
@class org_tns2_V_Employee;
@class org_tns2_ArrayOfV_OrgJob;
@class org_tns2_V_OrgJob;
@class org_tns2_ArrayOfV_OrgJobPosition;
@class org_tns2_V_OrgJobPosition;
@class org_tns2_ArrayOfV_OrgUnitTree;
@class org_tns2_V_OrgUnitTree;
@class org_tns2_ArrayOfCLogRating;
@class org_tns2_CLogRating;
@class org_tns2_ArrayOfEmpCompetency;
@class org_tns2_ArrayOfEmpProficencyDetailRating;
@class org_tns2_ArrayOfEmpProficencyLevelDetailRating;
@class org_tns2_ArrayOfEmpProficencyLevelRating;
@class org_tns2_ArrayOfEmpProfileSkill;
@class org_tns2_ArrayOfEmpProfileWorkingExperience;
@class org_tns2_ArrayOfLMSCourseAttendee;
@class org_tns2_ArrayOfLMSCourseCompetency;
@class org_tns2_ArrayOfLMSCourseRequiredCompetency;
@class org_tns2_ArrayOfLMSCourseTranscript;
@class org_tns2_ArrayOfOrgCoreCompetency;
@class org_tns2_ArrayOfOrgProficencyDetail;
@class org_tns2_ArrayOfOrgProficencyLevelDetail;
@class org_tns2_ArrayOfOrgProficencyLevel;
@class org_tns2_ArrayOfRecCanProficencyDetailRating;
@class org_tns2_ArrayOfRecCanProficencyLevelDetailRating;
@class org_tns2_ArrayOfRecCandidateCompetencyRating;
@class org_tns2_ArrayOfRecCandidateExperience;
@class org_tns2_ArrayOfRecCandidateProficencyLevelRating;
@class org_tns2_ArrayOfRecCandidateQualification;
@class org_tns2_ArrayOfRecCandidateSkill;
@class org_tns2_ArrayOfTrainingEmpProficency;
@class org_tns2_ArrayOfTrainingProficencyExpected;
@class org_tns2_ArrayOfTrainingProficencyRequire;
@class org_tns2_EmpCompetency;
@class org_tns2_Employee;
@class org_tns2_OrgCoreCompetency;
@class org_tns2_OrgProficencyLevel;
@class org_tns2_ArrayOfCBAccidentInsurance;
@class org_tns2_ArrayOfCBCompensationEmployeeGroupDetail;
@class org_tns2_ArrayOfCBConvalescence;
@class org_tns2_ArrayOfCBDayOffSocialInsurance;
@class org_tns2_ArrayOfCBFactorEmployeeMetaData;
@class org_tns2_ArrayOfCBHealthInsuranceDetail;
@class org_tns2_CLogCity;
@class org_tns2_CLogEmployeeType;
@class org_tns2_ArrayOfCLogTrainer;
@class org_tns2_ArrayOfEmpBasicProfile;
@class org_tns2_ArrayOfEmpCompetencyRating;
@class org_tns2_ArrayOfEmpContract;
@class org_tns2_ArrayOfEmpPerformanceAppraisal;
@class org_tns2_ArrayOfEmpProfileAllowance;
@class org_tns2_ArrayOfEmpProfileBaseSalary;
@class org_tns2_ArrayOfEmpProfileBenefit;
@class org_tns2_ArrayOfEmpProfileComment;
@class org_tns2_ArrayOfEmpProfileComputingSkill;
@class org_tns2_ArrayOfEmpProfileContact;
@class org_tns2_ArrayOfEmpProfileDegree;
@class org_tns2_ArrayOfEmpProfileDiscipline;
@class org_tns2_ArrayOfEmpProfileEducation;
@class org_tns2_ArrayOfEmpProfileEquipment;
@class org_tns2_ArrayOfEmpProfileExperience;
@class org_tns2_ArrayOfEmpProfileFamilyRelationship;
@class org_tns2_ArrayOfEmpProfileForeignLanguage;
@class org_tns2_ArrayOfEmpProfileHealthInsurance;
@class org_tns2_ArrayOfEmpProfileHealthy;
@class org_tns2_ArrayOfEmpProfileHobby;
@class org_tns2_ArrayOfEmpProfileInternalCourse;
@class org_tns2_ArrayOfEmpProfileJobPosition;
@class org_tns2_ArrayOfEmpProfileLeaveRegime;
@class org_tns2_ArrayOfEmpProfileOtherBenefit;
@class org_tns2_ArrayOfEmpProfileParticipation;
@class org_tns2_ArrayOfEmpProfilePersonality;
@class org_tns2_ArrayOfEmpProfileProcessOfWork;
@class org_tns2_ArrayOfEmpProfileQualification;
@class org_tns2_ArrayOfEmpProfileReward;
@class org_tns2_ArrayOfEmpProfileSocialInsurance;
@class org_tns2_ArrayOfEmpProfileTotalIncome;
@class org_tns2_ArrayOfEmpProfileTraining;
@class org_tns2_ArrayOfEmpProfileWageType;
@class org_tns2_ArrayOfEmpProfileWorkingForm;
@class org_tns2_ArrayOfEmpSocialInsuranceSalary;
@class org_tns2_ArrayOfGPAdditionAppraisal;
@class org_tns2_ArrayOfGPCompanyScoreCard;
@class org_tns2_ArrayOfGPCompanyStrategicGoal;
@class org_tns2_ArrayOfGPCompanyYearlyObjective;
@class org_tns2_ArrayOfGPEmployeeScoreCard;
@class org_tns2_ArrayOfGPIndividualYearlyObjective;
@class org_tns2_ArrayOfGPObjectiveInitiative;
@class org_tns2_ArrayOfGPPerformanceYearEndResult;
@class org_tns2_ArrayOfNCClientConnection;
@class org_tns2_ArrayOfNCMessage;
@class org_tns2_ArrayOfProjectMember;
@class org_tns2_ArrayOfProject;
@class org_tns2_ArrayOfRecInterviewSchedule;
@class org_tns2_ArrayOfRecInterviewer;
@class org_tns2_ArrayOfRecPhaseEmpDisplaced;
@class org_tns2_ArrayOfRecRecruitmentRequirement;
@class org_tns2_ArrayOfRecRequirementEmpDisplaced;
@class org_tns2_ReportEmployeeRequestedBook;
@class org_tns2_ArrayOfSelfLeaveRequest;
@class org_tns2_ArrayOfSysRecPlanApprover;
@class org_tns2_ArrayOfSysRecRequirementApprover;
@class org_tns2_ArrayOfSysTrainingApprover;
@class org_tns2_ArrayOfTask;
@class org_tns2_ArrayOfTrainingCourseEmployee;
@class org_tns2_ArrayOfTrainingEmpPlan;
@class org_tns2_ArrayOfTrainingPlanEmployee;
@class org_tns2_CBAccidentInsurance;
@class org_tns2_CBCompensationEmployeeGroupDetail;
@class org_tns2_CBCompensationEmployeeGroup;
@class org_tns2_CBFactorEmployeeMetaData;
@class org_tns2_CLogCBFactor;
@class org_tns2_ArrayOfCBCompensationTypeFactor;
@class org_tns2_CLogCBCompensationCategory;
@class org_tns2_CBCompensationTypeFactor;
@class org_tns2_ArrayOfCLogCBFactor;
@class org_tns2_CBConvalescence;
@class org_tns2_CBDayOffSocialInsurance;
@class org_tns2_CLogCBDisease;
@class org_tns2_CLogHospital;
@class org_tns2_ArrayOfCBHealthInsurance;
@class org_tns2_CBHealthInsurance;
@class org_tns2_CLogCountry;
@class org_tns2_ArrayOfCLogDistrict;
@class org_tns2_ArrayOfCLogHospital;
@class org_tns2_ArrayOfEmployee;
@class org_tns2_ArrayOfCLogCity;
@class org_tns2_CLogDistrict;
@class org_tns2_CBHealthInsuranceDetail;
@class org_tns2_CLogTrainer;
@class org_tns2_Address;
@class org_tns2_ArrayOfTrainingCourse;
@class org_tns2_ArrayOfOrgUnit;
@class org_tns2_EmpContract;
@class org_tns2_Employer;
@class org_tns2_OrgJob;
@class org_tns2_OrgJobPosition;
@class org_tns2_ArrayOfOrgJobPosition;
@class org_tns2_ArrayOfOrgJobSpecificCompetency;
@class org_tns2_ArrayOfOrgUnitJob;
@class org_tns2_ArrayOfOrgJobPositionRequiredProficency;
@class org_tns2_ArrayOfOrgJobPositionSpecificCompetency;
@class org_tns2_ArrayOfRecPlanJobPosition;
@class org_tns2_ArrayOfRecProcessApplyForJobPosition;
@class org_tns2_ArrayOfRecRecruitmentPhaseJobPosition;
@class org_tns2_ArrayOfRecRequirementJobPosition;
@class org_tns2_EmpProfileJobPosition;
@class org_tns2_OrgJobPositionRequiredProficency;
@class org_tns2_OrgJobPositionSpecificCompetency;
@class org_tns2_OrgCompetency;
@class org_tns2_EmpProficencyLevelRating;
@class org_tns2_EmpCompetencyRating;
@class org_tns2_EmpProficencyLevelDetailRating;
@class org_tns2_OrgProficencyLevelDetail;
@class org_tns2_EmpProficencyDetailRating;
@class org_tns2_OrgProficencyDetail;
@class org_tns2_OrgProficencyType;
@class org_tns2_ArrayOfTrainingPlanProfiency;
@class org_tns2_TrainingPlanProfiency;
@class org_tns2_RecCanProficencyLevelDetailRating;
@class org_tns2_RecCandidateProficencyLevelRating;
@class org_tns2_RecCanProficencyDetailRating;
@class org_tns2_RecCandidateCompetencyRating;
@class org_tns2_RecCandidate;
@class org_tns2_ArrayOfRecCandidateApplication;
@class org_tns2_ArrayOfRecCandidateDegree;
@class org_tns2_ArrayOfRecCandidateFamilyRelationship;
@class org_tns2_ArrayOfRecCandidateForeignLanguage;
@class org_tns2_RecCandidateStatu;
@class org_tns2_RecCandidateSupplier;
@class org_tns2_ArrayOfRecProcessPhaseResult;
@class org_tns2_RecCandidateApplication;
@class org_tns2_RecCandidateProfileStatu;
@class org_tns2_RecRecruitmentPhaseJobPosition;
@class org_tns2_RecInterviewSchedule;
@class org_tns2_RecGroupInterviewer;
@class org_tns2_RecInterviewPhase;
@class org_tns2_RecInterviewScheduleStatu;
@class org_tns2_ArrayOfRecScheduleInterviewerResult;
@class org_tns2_RecInterviewer;
@class org_tns2_RecScheduleInterviewerResult;
@class org_tns2_RecInterviewPhaseEvaluation;
@class org_tns2_RecEvaluationCriterion;
@class org_tns2_RecGroupEvaluationCriterion;
@class org_tns2_ArrayOfRecInterviewPhaseEvaluation;
@class org_tns2_ArrayOfRecEvaluationCriterion;
@class org_tns2_RecRecruitmentProcessPhase;
@class org_tns2_ArrayOfRecInterviewPhase;
@class org_tns2_RecRecruitmentProcess;
@class org_tns2_RecProcessPhaseResult;
@class org_tns2_ArrayOfRecRecruitmentProcessPhase;
@class org_tns2_RecProcessApplyForJobPosition;
@class org_tns2_RecRecruitmentPhase;
@class org_tns2_RecPhaseStatu;
@class org_tns2_RecRecruitmentRequirement;
@class org_tns2_RecPhaseEmpDisplaced;
@class org_tns2_ArrayOfRecRecruitmentPhase;
@class org_tns2_RecRecruitmentPlan;
@class org_tns2_ArrayOfRecRequirementApproveHistory;
@class org_tns2_RecRequirementStatu;
@class org_tns2_ArrayOfRecPlanApproveHistory;
@class org_tns2_RecPlanStatu;
@class org_tns2_RecPlanApproveHistory;
@class org_tns2_SysRecPlanApprover;
@class org_tns2_RecPlanJobPosition;
@class org_tns2_ArrayOfRecRecruitmentPlan;
@class org_tns2_RecRequirementApproveHistory;
@class org_tns2_SysRecRequirementApprover;
@class org_tns2_RecRequirementEmpDisplaced;
@class org_tns2_RecRequirementJobPosition;
@class org_tns2_RecCandidateDegree;
@class org_tns2_CLogDegree;
@class org_tns2_RecCandidateExperience;
@class org_tns2_OrgExperience;
@class org_tns2_OrgProjectType;
@class org_tns2_OrgTimeInCharge;
@class org_tns2_ArrayOfOrgExperience;
@class org_tns2_RecCandidateFamilyRelationship;
@class org_tns2_CLogFamilyRelationship;
@class org_tns2_EmpProfileFamilyRelationship;
@class org_tns2_ArrayOfCBPITDependent;
@class org_tns2_ArrayOfEmpBeneficiary;
@class org_tns2_CBPITDependent;
@class org_tns2_EmpBeneficiary;
@class org_tns2_RecCandidateForeignLanguage;
@class org_tns2_RecCandidateQualification;
@class org_tns2_OrgQualification;
@class org_tns2_EmpProfileQualification;
@class org_tns2_RecCandidateSkill;
@class org_tns2_OrgSkill;
@class org_tns2_OrgSkillType;
@class org_tns2_EmpProfileSkill;
@class org_tns2_ArrayOfOrgSkill;
@class org_tns2_TrainingEmpProficency;
@class org_tns2_TrainingCourseEmployee;
@class org_tns2_TrainingCourseSchedule;
@class org_tns2_CLogCourseSchedule;
@class org_tns2_TrainingCourse;
@class org_tns2_ArrayOfTrainingCourseSchedule;
@class org_tns2_CLogTrainingCenter;
@class org_tns2_TrainingCategory;
@class org_tns2_ArrayOfTrainingCourseChapter;
@class org_tns2_TrainingCoursePeriod;
@class org_tns2_TrainingCourseType;
@class org_tns2_ArrayOfTrainingCourseUnit;
@class org_tns2_ArrayOfTrainingPlanDegree;
@class org_tns2_TrainingPlanRequest;
@class org_tns2_TrainingCourseChapter;
@class org_tns2_TrainingCourseUnit;
@class org_tns2_TrainingPlanDegree;
@class org_tns2_CLogMajor;
@class org_tns2_ArrayOfTrainingPlanRequest;
@class org_tns2_EmpProfileEducation;
@class org_tns2_TrainingPlanEmployee;
@class org_tns2_TrainingProficencyExpected;
@class org_tns2_TrainingProficencyRequire;
@class org_tns2_ArrayOfRecCandidate;
@class org_tns2_RecCandidateTypeSupplier;
@class org_tns2_ArrayOfRecCandidateSupplier;
@class org_tns2_ArrayOfLMSTopicCompetency;
@class org_tns2_OrgCompetencyGroup;
@class org_tns2_LMSCourseCompetency;
@class org_tns2_LMSCourse;
@class org_tns2_CLogCourseStatu;
@class org_tns2_GPCompanyYearlyObjective;
@class org_tns2_GPObjectiveInitiative;
@class org_tns2_LMSContentTopic;
@class org_tns2_ArrayOfLMSCourseContent;
@class org_tns2_ArrayOfLMSCourseDetail;
@class org_tns2_ArrayOfLMSCourseProgressUnit;
@class org_tns2_LMSCourseType;
@class org_tns2_ArrayOfLMSCourse;
@class org_tns2_GPCompanyStrategicGoal;
@class org_tns2_GPPerspective;
@class org_tns2_GPStrategy;
@class org_tns2_GPCompanyScoreCard;
@class org_tns2_GPPerspectiveMeasure;
@class org_tns2_GPEmployeeScoreCard;
@class org_tns2_ArrayOfGPEmployeeScoreCardAppraisal;
@class org_tns2_ArrayOfGPEmployeeScoreCardProgress;
@class org_tns2_GPPerformanceExecutionReport;
@class org_tns2_GPEmployeeScoreCardAppraisal;
@class org_tns2_GPEmployeeScoreCardProgress;
@class org_tns2_GPIndividualYearlyObjective;
@class org_tns2_ArrayOfGPExecutionPlanDetail;
@class org_tns2_ArrayOfGPIndividualYearlyObjectiveAppraisal;
@class org_tns2_GPExecutionPlanDetail;
@class org_tns2_GPExecutionPlan;
@class org_tns2_ArrayOfGPExecutionPlanActivity;
@class org_tns2_ArrayOfGPEmployeeWholePlanAppraisal;
@class org_tns2_GPAdditionAppraisal;
@class org_tns2_GPEmployeeWholePlanAppraisal;
@class org_tns2_GPPerformanceYearEndResult;
@class org_tns2_GPExecutionPlanActivity;
@class org_tns2_ArrayOfGPExecutionPlanActivityAppraisal;
@class org_tns2_ArrayOfGPPeriodicReport;
@class org_tns2_GPExecutionPlanActivityAppraisal;
@class org_tns2_GPPeriodicReport;
@class org_tns2_GPIndividualYearlyObjectiveAppraisal;
@class org_tns2_ArrayOfGPPerspectiveMeasure;
@class org_tns2_ArrayOfGPPerspectiveValue;
@class org_tns2_ArrayOfGPCompanyStrategicGoalDetail;
@class org_tns2_GPStrategyMapObject;
@class org_tns2_GPCompanyStrategicGoalDetail;
@class org_tns2_GPPerspectiveValue;
@class org_tns2_ArrayOfGPPerspectiveValueDetail;
@class org_tns2_GPPerspectiveValueDetail;
@class org_tns2_ArrayOfGPStrategyMapObject;
@class org_tns2_LMSTopicGroup;
@class org_tns2_LMSTopicCompetency;
@class org_tns2_ArrayOfLMSContentTopic;
@class org_tns2_LMSCourseAttendee;
@class org_tns2_LMSCourseContent;
@class org_tns2_CLogLMSContentType;
@class org_tns2_LMSCourseDetail;
@class org_tns2_LMSCourseProgressUnit;
@class org_tns2_OrgUnit;
@class org_tns2_LMSCourseRequiredCompetency;
@class org_tns2_LMSCourseTranscript;
@class org_tns2_ArrayOfOrgCompetency;
@class org_tns2_OrgJobSpecificCompetency;
@class org_tns2_OrgUnitJob;
@class org_tns2_EmpBasicProfile;
@class org_tns2_CLogEmpWorkingStatu;
@class org_tns2_EmpPerformanceAppraisal;
@class org_tns2_EmpProfileAllowance;
@class org_tns2_EmpProfileBaseSalary;
@class org_tns2_EmpProfileBenefit;
@class org_tns2_EmpProfileComment;
@class org_tns2_EmpProfileComputingSkill;
@class org_tns2_EmpProfileContact;
@class org_tns2_EmpProfileDegree;
@class org_tns2_OrgDegree;
@class org_tns2_EmpProfileDiscipline;
@class org_tns2_OrgDisciplineMaster;
@class org_tns2_ArrayOfOrgDisciplineForUnit;
@class org_tns2_OrgDisciplineForUnit;
@class org_tns2_EmpProfileEquipment;
@class org_tns2_CLogEquipment;
@class org_tns2_EmpProfileExperience;
@class org_tns2_EmpProfileForeignLanguage;
@class org_tns2_CLogLanguage;
@class org_tns2_EmpProfileHealthInsurance;
@class org_tns2_EmpProfileHealthy;
@class org_tns2_EmpProfileHobby;
@class org_tns2_CLogHobby;
@class org_tns2_EmpProfileInternalCourse;
@class org_tns2_EmpProfileLeaveRegime;
@class org_tns2_EmpProfileOtherBenefit;
@class org_tns2_EmpOtherBenefit;
@class org_tns2_EmpProfileParticipation;
@class org_tns2_EmpProfilePersonality;
@class org_tns2_CLogPersonality;
@class org_tns2_EmpProfileProcessOfWork;
@class org_tns2_EmpProfileReward;
@class org_tns2_OrgRewardMaster;
@class org_tns2_ArrayOfOrgRewardForUnit;
@class org_tns2_OrgRewardForUnit;
@class org_tns2_EmpProfileSocialInsurance;
@class org_tns2_EmpProfileTotalIncome;
@class org_tns2_EmpProfileTraining;
@class org_tns2_EmpProfileWageType;
@class org_tns2_EmpProfileWorkingExperience;
@class org_tns2_EmpProfileWorkingForm;
@class org_tns2_EmpSocialInsuranceSalary;
@class org_tns2_NCClientConnection;
@class org_tns2_NCMessage;
@class org_tns2_NCMessageType;
@class org_tns2_ProjectMember;
@class org_tns2_Project;
@class org_tns2_SelfLeaveRequest;
@class org_tns2_SysTrainingApprover;
@class org_tns2_ArrayOfTrainingEmpPlanApproved;
@class org_tns2_TrainingEmpPlanApproved;
@class org_tns2_TrainingEmpPlan;
@class org_tns2_Task;
@class org_tns2_ArrayOfOrgProficencyType;
@class org_tns2_ArrayOfV_OrgJobPositionSpecificCompetency;
@class org_tns2_V_OrgJobPositionSpecificCompetency;
@class org_tns2_ArrayOfOrgCompetencyGroup;
@class org_tns2_ArrayOfV_OrgJobSpecificCompetency;
@class org_tns2_V_OrgJobSpecificCompetency;
@class org_tns2_ArrayOfV_OrgProficencyLevel;
@class org_tns2_V_OrgProficencyLevel;
@class org_tns2_ArrayOfV_OrgProficencyLevelDetail;
@class org_tns2_V_OrgProficencyLevelDetail;
@class org_tns2_ArrayOfV_OrgSkillCompetency;
@class org_tns2_V_OrgSkillCompetency;
@class org_tns2_ArrayOfV_OrgQualificationCompetency;
@class org_tns2_V_OrgQualificationCompetency;
@class org_tns2_ArrayOfV_OrgExperienceCompetency;
@class org_tns2_V_OrgExperienceCompetency;
@class org_tns2_ArrayOfV_OrgDegreeCompetency;
@class org_tns2_V_OrgDegreeCompetency;
@class org_tns2_ArrayOfV_OrgAttitudeCompetency;
@class org_tns2_V_OrgAttitudeCompetency;
@class org_tns2_ArrayOfV_OrgKnowledgeCompetency;
@class org_tns2_V_OrgKnowledgeCompetency;
@class org_tns2_ArrayOfV_ESS_ORG_DIRECT_REPORT;
@class org_tns2_V_ESS_ORG_DIRECT_REPORT;
@class org_tns2_TB_ESS_PERSONEL_STRUCT;
@class org_tns2_ArrayOfTB_ESS_PERSONEL_STRUCT;
@class org_tns2_ArrayOfV_ESS_PER_STRUCT_EMP_MAPPING;
@class org_tns2_V_ESS_PER_STRUCT_EMP_MAPPING;
@class org_tns2_ArrayOfV_ORG_JOB_TITLE_TREE;
@class org_tns2_V_ORG_JOB_TITLE_TREE;
@class org_tns2_V_ORG_JOB_TITLE_AND_GROUP;
@class org_tns2_ArrayOfV_ORG_JOB_TITLE_GROUP;
@class org_tns2_V_ORG_JOB_TITLE_GROUP;
@class org_tns2_ArrayOfV_ORG_JOB_TITLE_LEVEL;
@class org_tns2_V_ORG_JOB_TITLE_LEVEL;
@class org_tns2_ArrayOfV_ORG_JOB_TITLE_AND_GROUP;
@class org_tns2_ArrayOfV_ESS_TREE_OF_EMP_TYPE;
@class org_tns2_V_ESS_TREE_OF_EMP_TYPE;
@class org_tns2_ArrayOfV_ESS_EMP_GROUP_MAPPING;
@class org_tns2_V_ESS_EMP_GROUP_MAPPING;
@class org_tns2_ArrayOfV_ORG_CHANGE_INFO;
@class org_tns2_V_ORG_CHANGE_INFO;
@class org_tns2_ArrayOfV_OrgUnit;
@class org_tns2_V_OrgUnit;
@class org_tns2_V_EmpProfileJobPosition;
@class org_tns2_ArrayOfV_OrgUnitJobPosition;
@class org_tns2_V_OrgUnitJobPosition;
@class org_tns2_ArrayOfV_OrgUnitJob;
@class org_tns2_V_OrgUnitJob;
@class org_tns2_ArrayOfOrgJob;
@class org_tns2_ArrayOfSP_ORG_UNIT_TREE_BY_USER_Result;
@class org_tns2_SP_ORG_UNIT_TREE_BY_USER_Result;
@class org_tns2_ArrayOfOrgTimeInCharge;
@class org_tns2_ArrayOfOrgSkillType;
@class org_tns2_ArrayOfOrgProjectType;
@class org_tns2_ArrayOfV_ESS_CLOG_CAREER;
@class org_tns2_V_ESS_CLOG_CAREER;
@class org_tns2_ArrayOfOrgDegree;
@class org_tns2_ArrayOfOrgDegreeRank;
@class org_tns2_OrgDegreeRank;
@class org_tns2_V_ESS_ORG_JOB_TASK_DESC;
@class org_tns2_V_ESS_ORG_POSITION_TASK_DESC;
@class org_tns2_ArrayOfOrgWorkLevel;
@class org_tns2_OrgWorkLevel;
@class org_tns2_ArrayOfV_ORG_CAREER_JOB_WLEVEL_TREE;
@class org_tns2_V_ORG_CAREER_JOB_WLEVEL_TREE;
@class org_tns2_V_OrgJobPositionDetail;
@class org_tns2_ArrayOfV_OrgExperience;
@class org_tns2_V_OrgExperience;
@class org_tns2_ArrayOfOrgQualification;
@class org_tns2_ArrayOfOrgKnowledge;
@class org_tns2_OrgKnowledge;
@class org_tns2_ArrayOfV_OrgJobPositionKnowledgeProficency;
@class org_tns2_V_OrgJobPositionKnowledgeProficency;
@class org_tns2_ArrayOfV_OrgJobPositionExperienceProficency;
@class org_tns2_V_OrgJobPositionExperienceProficency;
@class org_tns2_ArrayOfV_OrgJobPositionQualificationProficency;
@class org_tns2_V_OrgJobPositionQualificationProficency;
@class org_tns2_ArrayOfV_OrgJobPositionSkillProficency;
@class org_tns2_V_OrgJobPositionSkillProficency;
@class org_tns2_ArrayOfV_ORG_JOB_WORK_LEVEL_TREE;
@class org_tns2_V_ORG_JOB_WORK_LEVEL_TREE;
@class org_tns2_ArrayOfV_ORG_UNIT_POSITION_TREE;
@class org_tns2_V_ORG_UNIT_POSITION_TREE;
@class org_tns2_ArrayOfV_ORG_JOB_POSITION_TREE;
@class org_tns2_V_ORG_JOB_POSITION_TREE;
@class org_tns2_ArrayOfOrgGroup;
@class org_tns2_OrgGroup;
#import "org_tns5.h"
@interface org_tns2_V_Employee : NSObject {
	
/* elements */
	NSDate * BirthDay;
	NSString * CAREER_NAME_EN;
	NSString * CAREER_NAME_VN;
	NSString * CLOG_CAREER_CODE;
	NSNumber * CompanyId;
	NSString * Email;
	NSString * EmpWorkingStatusCode;
	NSNumber * EmpWorkingStatusId;
	NSString * EmpWorkingStatusName;
	NSString * EmpWorkingStatusNameEN;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EmployeeTypeId;
	NSString * EmployeeTypeName;
	NSString * EmployeeTypeNameEN;
	NSDate * EndWorkingDate;
	NSDate * EntryDate;
	NSString * FullName;
	USBoolean * Gender;
	NSString * IdentityCardNo;
	NSString * ImageUrl;
	USBoolean * IsDeleted;
	USBoolean * IsOnlyYearOfBirthday;
	NSString * MobileNumber;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobNameEN;
	NSNumber * OrgJobPositionId;
	NSString * OrgJobPositionName;
	NSString * OrgJobPositionNameEN;
	NSString * OrgJobTitleCode;
	NSNumber * OrgJobTitleId;
	NSString * OrgJobTitleLevelCode;
	NSString * OrgJobTitleLevelNameEN;
	NSString * OrgJobTitleLevelNameVN;
	NSString * OrgJobTitleNameEN;
	NSString * OrgJobTitleNameVN;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSNumber * OrgWorkLevelId;
	NSString * UnitName;
	NSString * UnitNameEN;
	NSString * WorkLevelName;
	NSString * WorkLevelNameEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_Employee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * BirthDay;
@property (retain) NSString * CAREER_NAME_EN;
@property (retain) NSString * CAREER_NAME_VN;
@property (retain) NSString * CLOG_CAREER_CODE;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Email;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSNumber * EmpWorkingStatusId;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmpWorkingStatusNameEN;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSString * EmployeeTypeName;
@property (retain) NSString * EmployeeTypeNameEN;
@property (retain) NSDate * EndWorkingDate;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) NSString * MobileNumber;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobNameEN;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSString * OrgJobPositionNameEN;
@property (retain) NSString * OrgJobTitleCode;
@property (retain) NSNumber * OrgJobTitleId;
@property (retain) NSString * OrgJobTitleLevelCode;
@property (retain) NSString * OrgJobTitleLevelNameEN;
@property (retain) NSString * OrgJobTitleLevelNameVN;
@property (retain) NSString * OrgJobTitleNameEN;
@property (retain) NSString * OrgJobTitleNameVN;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * UnitName;
@property (retain) NSString * UnitNameEN;
@property (retain) NSString * WorkLevelName;
@property (retain) NSString * WorkLevelNameEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_Employee : NSObject {
	
/* elements */
	NSMutableArray *V_Employee;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_Employee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_Employee:(org_tns2_V_Employee *)toAdd;
@property (readonly) NSMutableArray * V_Employee;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_OrgJob : NSObject {
	
/* elements */
	NSNumber * CLogCareerId;
	NSString * CareerName;
	NSString * CareerNameEN;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * NameEN;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_OrgJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogCareerId;
@property (retain) NSString * CareerName;
@property (retain) NSString * CareerNameEN;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * NameEN;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_OrgJob : NSObject {
	
/* elements */
	NSMutableArray *V_OrgJob;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_OrgJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgJob:(org_tns2_V_OrgJob *)toAdd;
@property (readonly) NSMutableArray * V_OrgJob;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_OrgJobPosition : NSObject {
	
/* elements */
	NSNumber * CLogCareerId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSString * JobTitleLevelCode;
	NSString * JobTitleLevelNameEN;
	NSString * JobTitleLevelNameVN;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobNameEN;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgJobTitleCode;
	NSNumber * OrgJobTitleId;
	NSString * OrgJobTitleNameEN;
	NSString * OrgJobTitleNameVN;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgWorkLevelCode;
	NSNumber * OrgWorkLevelId;
	NSString * UnitName;
	NSString * UnitNameEN;
	NSString * WorkLevelNameEN;
	NSString * WorkLevelNameVN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_OrgJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogCareerId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * JobTitleLevelCode;
@property (retain) NSString * JobTitleLevelNameEN;
@property (retain) NSString * JobTitleLevelNameVN;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobNameEN;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgJobTitleCode;
@property (retain) NSNumber * OrgJobTitleId;
@property (retain) NSString * OrgJobTitleNameEN;
@property (retain) NSString * OrgJobTitleNameVN;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * UnitName;
@property (retain) NSString * UnitNameEN;
@property (retain) NSString * WorkLevelNameEN;
@property (retain) NSString * WorkLevelNameVN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_OrgJobPosition : NSObject {
	
/* elements */
	NSMutableArray *V_OrgJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_OrgJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgJobPosition:(org_tns2_V_OrgJobPosition *)toAdd;
@property (readonly) NSMutableArray * V_OrgJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_OrgUnitTree : NSObject {
	
/* elements */
	NSString * Address;
	NSNumber * AddressId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSNumber * OrgUnitLevelId;
	NSString * ParentCode;
	NSNumber * ParentId;
	NSNumber * TotalEmployees;
	NSString * WorkLocation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_OrgUnitTree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSNumber * AddressId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * OrgUnitLevelId;
@property (retain) NSString * ParentCode;
@property (retain) NSNumber * ParentId;
@property (retain) NSNumber * TotalEmployees;
@property (retain) NSString * WorkLocation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_OrgUnitTree : NSObject {
	
/* elements */
	NSMutableArray *V_OrgUnitTree;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_OrgUnitTree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgUnitTree:(org_tns2_V_OrgUnitTree *)toAdd;
@property (readonly) NSMutableArray * V_OrgUnitTree;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CBAccidentInsurance : NSObject {
	
/* elements */
	NSString * AccidentInsuranceContractCode;
	NSString * AccidentInsuranceContractNumber;
	NSString * AccidentName;
	NSNumber * CBAccidentInsuranceId;
	NSNumber * Charge;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * EffectDate;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	NSDate * ExpireDate;
	NSString * InsuranceCompanyName;
	NSString * InsuranceKind;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CBAccidentInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AccidentInsuranceContractCode;
@property (retain) NSString * AccidentInsuranceContractNumber;
@property (retain) NSString * AccidentName;
@property (retain) NSNumber * CBAccidentInsuranceId;
@property (retain) NSNumber * Charge;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EffectDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpireDate;
@property (retain) NSString * InsuranceCompanyName;
@property (retain) NSString * InsuranceKind;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfCBAccidentInsurance : NSObject {
	
/* elements */
	NSMutableArray *CBAccidentInsurance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfCBAccidentInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBAccidentInsurance:(org_tns2_CBAccidentInsurance *)toAdd;
@property (readonly) NSMutableArray * CBAccidentInsurance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CBCompensationTypeFactor : NSObject {
	
/* elements */
	NSNumber * CBCompensationTypeFactorId;
	NSNumber * CLogCBCompensationTypeId;
	org_tns2_CLogCBFactor * CLogCBFactor;
	NSNumber * CLogCBFactorId;
	USBoolean * IsDeleted;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CBCompensationTypeFactor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBCompensationTypeFactorId;
@property (retain) NSNumber * CLogCBCompensationTypeId;
@property (retain) org_tns2_CLogCBFactor * CLogCBFactor;
@property (retain) NSNumber * CLogCBFactorId;
@property (retain) USBoolean * IsDeleted;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfCBCompensationTypeFactor : NSObject {
	
/* elements */
	NSMutableArray *CBCompensationTypeFactor;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfCBCompensationTypeFactor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBCompensationTypeFactor:(org_tns2_CBCompensationTypeFactor *)toAdd;
@property (readonly) NSMutableArray * CBCompensationTypeFactor;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfCLogCBFactor : NSObject {
	
/* elements */
	NSMutableArray *CLogCBFactor;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfCLogCBFactor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogCBFactor:(org_tns2_CLogCBFactor *)toAdd;
@property (readonly) NSMutableArray * CLogCBFactor;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CLogCBCompensationCategory : NSObject {
	
/* elements */
	NSNumber * CLogCBCompensationCategoryId;
	org_tns2_ArrayOfCLogCBFactor * CLogCBFactors;
	NSString * CLogCompensationCategoryCode;
	NSString * Description;
	USBoolean * IsDeleted;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CLogCBCompensationCategory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogCBCompensationCategoryId;
@property (retain) org_tns2_ArrayOfCLogCBFactor * CLogCBFactors;
@property (retain) NSString * CLogCompensationCategoryCode;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CLogCBFactor : NSObject {
	
/* elements */
	org_tns2_ArrayOfCBCompensationTypeFactor * CBCompensationTypeFactors;
	org_tns2_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
	org_tns2_CLogCBCompensationCategory * CLogCBCompensationCategory;
	NSNumber * CLogCBCompensationCategoryId;
	NSString * CLogCBFactorCode;
	NSNumber * CLogCBFactorGroupId;
	NSNumber * CLogCBFactorId;
	NSString * Description;
	NSString * FormulaExpression;
	USBoolean * IsActived;
	USBoolean * IsDeactived;
	USBoolean * IsDeleted;
	USBoolean * IsInitial;
	NSString * Name;
	NSNumber * Priority;
	NSString * ServiceReferenceEndPointAddress;
	NSString * ServiceReferenceEndPointOperationContract;
	NSString * TableReference;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CLogCBFactor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfCBCompensationTypeFactor * CBCompensationTypeFactors;
@property (retain) org_tns2_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
@property (retain) org_tns2_CLogCBCompensationCategory * CLogCBCompensationCategory;
@property (retain) NSNumber * CLogCBCompensationCategoryId;
@property (retain) NSString * CLogCBFactorCode;
@property (retain) NSNumber * CLogCBFactorGroupId;
@property (retain) NSNumber * CLogCBFactorId;
@property (retain) NSString * Description;
@property (retain) NSString * FormulaExpression;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeactived;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInitial;
@property (retain) NSString * Name;
@property (retain) NSNumber * Priority;
@property (retain) NSString * ServiceReferenceEndPointAddress;
@property (retain) NSString * ServiceReferenceEndPointOperationContract;
@property (retain) NSString * TableReference;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CBFactorEmployeeMetaData : NSObject {
	
/* elements */
	NSDate * BeginDate;
	org_tns2_CBCompensationEmployeeGroup * CBCompensationEmployeeGroup;
	NSNumber * CBCompensationEmployeeGroupId;
	NSString * CBFactorEmployeeMetaDataDescription;
	NSNumber * CBFactorEmployeeMetaDataId;
	org_tns2_CLogCBFactor * CLogCBFactor;
	NSNumber * CLogCBFactorId;
	NSString * ChangeReason;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	NSNumber * FactorValue;
	NSString * GroupGuid;
	USBoolean * IsActivated;
	USBoolean * IsAppliedForCompany;
	USBoolean * IsChanged;
	USBoolean * IsCurrent;
	USBoolean * IsDeactivated;
	USBoolean * IsDeleted;
	USBoolean * IsInitialed;
	NSDate * ModifiedDate;
	NSNumber * OldFactorValue;
	NSString * ReferenceColunmName;
	NSString * ReferenceRowGuid;
	NSString * ReferenceTableName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CBFactorEmployeeMetaData *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * BeginDate;
@property (retain) org_tns2_CBCompensationEmployeeGroup * CBCompensationEmployeeGroup;
@property (retain) NSNumber * CBCompensationEmployeeGroupId;
@property (retain) NSString * CBFactorEmployeeMetaDataDescription;
@property (retain) NSNumber * CBFactorEmployeeMetaDataId;
@property (retain) org_tns2_CLogCBFactor * CLogCBFactor;
@property (retain) NSNumber * CLogCBFactorId;
@property (retain) NSString * ChangeReason;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) NSNumber * FactorValue;
@property (retain) NSString * GroupGuid;
@property (retain) USBoolean * IsActivated;
@property (retain) USBoolean * IsAppliedForCompany;
@property (retain) USBoolean * IsChanged;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeactivated;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInitialed;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OldFactorValue;
@property (retain) NSString * ReferenceColunmName;
@property (retain) NSString * ReferenceRowGuid;
@property (retain) NSString * ReferenceTableName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfCBFactorEmployeeMetaData : NSObject {
	
/* elements */
	NSMutableArray *CBFactorEmployeeMetaData;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfCBFactorEmployeeMetaData *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBFactorEmployeeMetaData:(org_tns2_CBFactorEmployeeMetaData *)toAdd;
@property (readonly) NSMutableArray * CBFactorEmployeeMetaData;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CBCompensationEmployeeGroup : NSObject {
	
/* elements */
	org_tns2_ArrayOfCBCompensationEmployeeGroupDetail * CBCompensationEmployeeGroupDetails;
	NSNumber * CBCompensationEmployeeGroupId;
	org_tns2_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
	NSNumber * CompanyId;
	NSString * Description;
	USBoolean * IsActived;
	USBoolean * IsDeactived;
	USBoolean * IsDeleted;
	USBoolean * IsInitial;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CBCompensationEmployeeGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfCBCompensationEmployeeGroupDetail * CBCompensationEmployeeGroupDetails;
@property (retain) NSNumber * CBCompensationEmployeeGroupId;
@property (retain) org_tns2_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeactived;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInitial;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CBCompensationEmployeeGroupDetail : NSObject {
	
/* elements */
	org_tns2_CBCompensationEmployeeGroup * CBCompensationEmployeeGroup;
	NSNumber * CBCompensationEmployeeGroupDetailId;
	NSNumber * CBCompensationEmployeeGroupId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CBCompensationEmployeeGroupDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_CBCompensationEmployeeGroup * CBCompensationEmployeeGroup;
@property (retain) NSNumber * CBCompensationEmployeeGroupDetailId;
@property (retain) NSNumber * CBCompensationEmployeeGroupId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfCBCompensationEmployeeGroupDetail : NSObject {
	
/* elements */
	NSMutableArray *CBCompensationEmployeeGroupDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfCBCompensationEmployeeGroupDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBCompensationEmployeeGroupDetail:(org_tns2_CBCompensationEmployeeGroupDetail *)toAdd;
@property (readonly) NSMutableArray * CBCompensationEmployeeGroupDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CBConvalescence : NSObject {
	
/* elements */
	NSNumber * AllowanceAmount;
	NSNumber * CBConvalescenceId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	NSDate * FromDate;
	NSNumber * HealthLessLevel;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * NumberDayOff;
	NSDate * NumberOfMounthApplied;
	NSDate * ToDate;
	NSNumber * TypeId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CBConvalescence *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AllowanceAmount;
@property (retain) NSNumber * CBConvalescenceId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * FromDate;
@property (retain) NSNumber * HealthLessLevel;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * NumberDayOff;
@property (retain) NSDate * NumberOfMounthApplied;
@property (retain) NSDate * ToDate;
@property (retain) NSNumber * TypeId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfCBConvalescence : NSObject {
	
/* elements */
	NSMutableArray *CBConvalescence;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfCBConvalescence *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBConvalescence:(org_tns2_CBConvalescence *)toAdd;
@property (readonly) NSMutableArray * CBConvalescence;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CLogCBDisease : NSObject {
	
/* elements */
	org_tns2_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
	NSNumber * CLogCBDiseaseId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	NSString * DiseaseCode;
	USBoolean * IsChronic;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CLogCBDisease *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
@property (retain) NSNumber * CLogCBDiseaseId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) NSString * DiseaseCode;
@property (retain) USBoolean * IsChronic;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CBHealthInsurance : NSObject {
	
/* elements */
	NSNumber * CBHealthInsuranceId;
	org_tns2_CLogHospital * CLogHospital;
	NSNumber * CLogHospitalId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * EffectDate;
	NSDate * ExpireDate;
	NSNumber * GrossCharge;
	NSString * HealthInsuranceContractCode;
	NSString * HealthInsuranceContractNumber;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Remark;
	NSNumber * SignerId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CBHealthInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBHealthInsuranceId;
@property (retain) org_tns2_CLogHospital * CLogHospital;
@property (retain) NSNumber * CLogHospitalId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EffectDate;
@property (retain) NSDate * ExpireDate;
@property (retain) NSNumber * GrossCharge;
@property (retain) NSString * HealthInsuranceContractCode;
@property (retain) NSString * HealthInsuranceContractNumber;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Remark;
@property (retain) NSNumber * SignerId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfCBHealthInsurance : NSObject {
	
/* elements */
	NSMutableArray *CBHealthInsurance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfCBHealthInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBHealthInsurance:(org_tns2_CBHealthInsurance *)toAdd;
@property (readonly) NSMutableArray * CBHealthInsurance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfCLogCity : NSObject {
	
/* elements */
	NSMutableArray *CLogCity;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfCLogCity *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogCity:(org_tns2_CLogCity *)toAdd;
@property (readonly) NSMutableArray * CLogCity;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CLogCountry : NSObject {
	
/* elements */
	org_tns2_ArrayOfCLogCity * CLogCities;
	NSString * CLogCountryCode;
	NSNumber * CLogCountryId;
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CLogCountry *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfCLogCity * CLogCities;
@property (retain) NSString * CLogCountryCode;
@property (retain) NSNumber * CLogCountryId;
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CLogDistrict : NSObject {
	
/* elements */
	org_tns2_CLogCity * CLogCity;
	NSString * CLogCityCode;
	NSNumber * CLogCityId;
	NSString * CLogDistrictCode;
	NSNumber * CLogDistrictId;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CLogDistrict *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_CLogCity * CLogCity;
@property (retain) NSString * CLogCityCode;
@property (retain) NSNumber * CLogCityId;
@property (retain) NSString * CLogDistrictCode;
@property (retain) NSNumber * CLogDistrictId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfCLogDistrict : NSObject {
	
/* elements */
	NSMutableArray *CLogDistrict;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfCLogDistrict *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogDistrict:(org_tns2_CLogDistrict *)toAdd;
@property (readonly) NSMutableArray * CLogDistrict;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfCLogHospital : NSObject {
	
/* elements */
	NSMutableArray *CLogHospital;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfCLogHospital *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogHospital:(org_tns2_CLogHospital *)toAdd;
@property (readonly) NSMutableArray * CLogHospital;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmployee : NSObject {
	
/* elements */
	NSMutableArray *Employee;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmployee:(org_tns2_Employee *)toAdd;
@property (readonly) NSMutableArray * Employee;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CLogCity : NSObject {
	
/* elements */
	NSString * CLogCityCode;
	NSNumber * CLogCityId;
	NSString * CLogCityNoIns;
	org_tns2_CLogCountry * CLogCountry;
	NSString * CLogCountryCode;
	NSNumber * CLogCountryId;
	org_tns2_ArrayOfCLogDistrict * CLogDistricts;
	org_tns2_ArrayOfCLogHospital * CLogHospitals;
	NSNumber * CompanyId;
	org_tns2_ArrayOfEmployee * Employees;
	org_tns2_ArrayOfEmployee * Employees1;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
	NSString * SysNo;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CLogCity *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogCityCode;
@property (retain) NSNumber * CLogCityId;
@property (retain) NSString * CLogCityNoIns;
@property (retain) org_tns2_CLogCountry * CLogCountry;
@property (retain) NSString * CLogCountryCode;
@property (retain) NSNumber * CLogCountryId;
@property (retain) org_tns2_ArrayOfCLogDistrict * CLogDistricts;
@property (retain) org_tns2_ArrayOfCLogHospital * CLogHospitals;
@property (retain) NSNumber * CompanyId;
@property (retain) org_tns2_ArrayOfEmployee * Employees;
@property (retain) org_tns2_ArrayOfEmployee * Employees1;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
@property (retain) NSString * SysNo;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CLogHospital : NSObject {
	
/* elements */
	org_tns2_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
	org_tns2_ArrayOfCBHealthInsurance * CBHealthInsurances;
	org_tns2_CLogCity * CLogCity;
	NSNumber * CLogCityId;
	NSString * CLogCityNoIns;
	NSString * CLogHospitalCode;
	NSNumber * CLogHospitalId;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CLogHospital *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
@property (retain) org_tns2_ArrayOfCBHealthInsurance * CBHealthInsurances;
@property (retain) org_tns2_CLogCity * CLogCity;
@property (retain) NSNumber * CLogCityId;
@property (retain) NSString * CLogCityNoIns;
@property (retain) NSString * CLogHospitalCode;
@property (retain) NSNumber * CLogHospitalId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CBDayOffSocialInsurance : NSObject {
	
/* elements */
	NSNumber * CBDayOffSocialInsuranceId;
	org_tns2_CLogCBDisease * CLogCBDisease;
	NSNumber * CLogCBDiseaseId;
	org_tns2_CLogHospital * CLogHospital;
	NSNumber * CLogHospitalId;
	NSNumber * ClogCBDayOffTypeId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * DoctorName;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	NSDate * FromDate;
	USBoolean * IsDeleted;
	USBoolean * IsPaid;
	USBoolean * IsWorkAccident;
	NSDate * ModifiedDate;
	NSNumber * NumberDayOff;
	NSNumber * NumberDayOffByDoctor;
	NSNumber * NumberPaidDay;
	NSString * Reason;
	NSDate * ToDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CBDayOffSocialInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBDayOffSocialInsuranceId;
@property (retain) org_tns2_CLogCBDisease * CLogCBDisease;
@property (retain) NSNumber * CLogCBDiseaseId;
@property (retain) org_tns2_CLogHospital * CLogHospital;
@property (retain) NSNumber * CLogHospitalId;
@property (retain) NSNumber * ClogCBDayOffTypeId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DoctorName;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * FromDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsPaid;
@property (retain) USBoolean * IsWorkAccident;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NumberDayOff;
@property (retain) NSNumber * NumberDayOffByDoctor;
@property (retain) NSNumber * NumberPaidDay;
@property (retain) NSString * Reason;
@property (retain) NSDate * ToDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfCBDayOffSocialInsurance : NSObject {
	
/* elements */
	NSMutableArray *CBDayOffSocialInsurance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfCBDayOffSocialInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBDayOffSocialInsurance:(org_tns2_CBDayOffSocialInsurance *)toAdd;
@property (readonly) NSMutableArray * CBDayOffSocialInsurance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CBHealthInsuranceDetail : NSObject {
	
/* elements */
	NSNumber * CBHealthInsuranceDetailId;
	NSNumber * CBHealthInsuranceId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CBHealthInsuranceDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBHealthInsuranceDetailId;
@property (retain) NSNumber * CBHealthInsuranceId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfCBHealthInsuranceDetail : NSObject {
	
/* elements */
	NSMutableArray *CBHealthInsuranceDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfCBHealthInsuranceDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBHealthInsuranceDetail:(org_tns2_CBHealthInsuranceDetail *)toAdd;
@property (readonly) NSMutableArray * CBHealthInsuranceDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CLogEmployeeType : NSObject {
	
/* elements */
	NSString * CLogEmployeeTypeCode;
	NSNumber * CLogEmployeeTypeId;
	org_tns2_ArrayOfEmployee * Employees;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CLogEmployeeType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogEmployeeTypeCode;
@property (retain) NSNumber * CLogEmployeeTypeId;
@property (retain) org_tns2_ArrayOfEmployee * Employees;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_Employer : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_ArrayOfEmpContract * EmpContracts;
	NSString * EmployerCode;
	NSNumber * EmployerId;
	NSString * FullName;
	USBoolean * IsDeleted;
	NSString * JobPositionName;
	NSDate * ModifiedDate;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_Employer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_ArrayOfEmpContract * EmpContracts;
@property (retain) NSString * EmployerCode;
@property (retain) NSNumber * EmployerId;
@property (retain) NSString * FullName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * JobPositionName;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * DecisionFile;
	NSString * DecisionName;
	NSString * DecisionNo;
	NSString * DirectReportToEmployeeCode;
	NSNumber * DirectReportToEmployeeId;
	NSString * EmpJobPositionCode;
	NSNumber * EmpProfileJobPositionId;
	org_tns2_Employee * Employee;
	org_tns2_Employee * Employee1;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ExpirationDate;
	NSDate * ImplementationDate;
	USBoolean * IsActive;
	USBoolean * IsConcurrentPost;
	USBoolean * IsDeleted;
	USBoolean * IsMainPost;
	USBoolean * IsPromoted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * OrgJobExternalTitleCode;
	org_tns2_OrgJobPosition * OrgJobPosition;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSNumber * PercentParticipation;
	NSString * Reason;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DecisionFile;
@property (retain) NSString * DecisionName;
@property (retain) NSString * DecisionNo;
@property (retain) NSString * DirectReportToEmployeeCode;
@property (retain) NSNumber * DirectReportToEmployeeId;
@property (retain) NSString * EmpJobPositionCode;
@property (retain) NSNumber * EmpProfileJobPositionId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) org_tns2_Employee * Employee1;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsConcurrentPost;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainPost;
@property (retain) USBoolean * IsPromoted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * OrgJobExternalTitleCode;
@property (retain) org_tns2_OrgJobPosition * OrgJobPosition;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * PercentParticipation;
@property (retain) NSString * Reason;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileJobPosition : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileJobPosition:(org_tns2_EmpProfileJobPosition *)toAdd;
@property (readonly) NSMutableArray * EmpProfileJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgJobPositionRequiredProficency : NSObject {
	
/* elements */
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	org_tns2_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSNumber * OrgJobPositionRequiredProficencyId;
	NSString * ProficiencyTypeCode;
	NSNumber * RecordId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgJobPositionRequiredProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * OrgJobPositionRequiredProficencyId;
@property (retain) NSString * ProficiencyTypeCode;
@property (retain) NSNumber * RecordId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgJobPositionRequiredProficency : NSObject {
	
/* elements */
	NSMutableArray *OrgJobPositionRequiredProficency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgJobPositionRequiredProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgJobPositionRequiredProficency:(org_tns2_OrgJobPositionRequiredProficency *)toAdd;
@property (readonly) NSMutableArray * OrgJobPositionRequiredProficency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpCompetencyRating : NSObject {
	
/* elements */
	NSDate * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpCompetencyRatingId;
	org_tns2_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpCompetencyRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpCompetencyRatingId;
@property (retain) org_tns2_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgProficencyDetail : NSObject {
	
/* elements */
	NSMutableArray *OrgProficencyDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgProficencyDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgProficencyDetail:(org_tns2_OrgProficencyDetail *)toAdd;
@property (readonly) NSMutableArray * OrgProficencyDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgProficencyLevelDetail : NSObject {
	
/* elements */
	NSMutableArray *OrgProficencyLevelDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgProficencyLevelDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgProficencyLevelDetail:(org_tns2_OrgProficencyLevelDetail *)toAdd;
@property (readonly) NSMutableArray * OrgProficencyLevelDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_TrainingPlanProfiency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	org_tns2_OrgProficencyType * OrgProficencyType;
	NSNumber * OrgProficencyTypeId;
	NSNumber * ProficencyId;
	NSNumber * TrainingPlanProfiencyId;
	NSNumber * TrainingPlanRequestId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_TrainingPlanProfiency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_OrgProficencyType * OrgProficencyType;
@property (retain) NSNumber * OrgProficencyTypeId;
@property (retain) NSNumber * ProficencyId;
@property (retain) NSNumber * TrainingPlanProfiencyId;
@property (retain) NSNumber * TrainingPlanRequestId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfTrainingPlanProfiency : NSObject {
	
/* elements */
	NSMutableArray *TrainingPlanProfiency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfTrainingPlanProfiency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingPlanProfiency:(org_tns2_TrainingPlanProfiency *)toAdd;
@property (readonly) NSMutableArray * TrainingPlanProfiency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgProficencyType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	org_tns2_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
	NSNumber * OrgProficencyTypeId;
	NSString * ProficencyTypeCode;
	NSString * ProficencyTypeName;
	org_tns2_ArrayOfTrainingPlanProfiency * TrainingPlanProfiencies;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgProficencyType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
@property (retain) NSNumber * OrgProficencyTypeId;
@property (retain) NSString * ProficencyTypeCode;
@property (retain) NSString * ProficencyTypeName;
@property (retain) org_tns2_ArrayOfTrainingPlanProfiency * TrainingPlanProfiencies;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecCanProficencyDetailRating : NSObject {
	
/* elements */
	org_tns2_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * Mark;
	NSDate * ModifiedDate;
	NSString * Note;
	org_tns2_OrgProficencyDetail * OrgProficencyDetail;
	NSNumber * OrgProficencyDetailId;
	NSNumber * RecCanProficencyDetailRatingId;
	org_tns2_RecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRating;
	NSNumber * RecCanProficencyLevelDetailRatingId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecCanProficencyDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Mark;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) org_tns2_OrgProficencyDetail * OrgProficencyDetail;
@property (retain) NSNumber * OrgProficencyDetailId;
@property (retain) NSNumber * RecCanProficencyDetailRatingId;
@property (retain) org_tns2_RecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRating;
@property (retain) NSNumber * RecCanProficencyLevelDetailRatingId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecCanProficencyDetailRating : NSObject {
	
/* elements */
	NSMutableArray *RecCanProficencyDetailRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecCanProficencyDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCanProficencyDetailRating:(org_tns2_RecCanProficencyDetailRating *)toAdd;
@property (readonly) NSMutableArray * RecCanProficencyDetailRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecCandidateProfileStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * Priority;
	NSString * ProfileStatusCode;
	NSString * ProfileStatusName;
	org_tns2_ArrayOfRecCandidateApplication * RecCandidateApplications;
	NSNumber * RecCandidateProfileStatusId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecCandidateProfileStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Priority;
@property (retain) NSString * ProfileStatusCode;
@property (retain) NSString * ProfileStatusName;
@property (retain) org_tns2_ArrayOfRecCandidateApplication * RecCandidateApplications;
@property (retain) NSNumber * RecCandidateProfileStatusId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecEvaluationCriterion : NSObject {
	
/* elements */
	NSMutableArray *RecEvaluationCriterion;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecEvaluationCriterion *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecEvaluationCriterion:(org_tns2_RecEvaluationCriterion *)toAdd;
@property (readonly) NSMutableArray * RecEvaluationCriterion;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecGroupEvaluationCriterion : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * Priority;
	org_tns2_ArrayOfRecEvaluationCriterion * RecEvaluationCriterions;
	NSString * RecGroupEvalDesc;
	NSString * RecGroupEvalName;
	NSNumber * RecGroupEvaluationCriterionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecGroupEvaluationCriterion *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Priority;
@property (retain) org_tns2_ArrayOfRecEvaluationCriterion * RecEvaluationCriterions;
@property (retain) NSString * RecGroupEvalDesc;
@property (retain) NSString * RecGroupEvalName;
@property (retain) NSNumber * RecGroupEvaluationCriterionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecInterviewPhaseEvaluation : NSObject {
	
/* elements */
	NSMutableArray *RecInterviewPhaseEvaluation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecInterviewPhaseEvaluation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecInterviewPhaseEvaluation:(org_tns2_RecInterviewPhaseEvaluation *)toAdd;
@property (readonly) NSMutableArray * RecInterviewPhaseEvaluation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecEvaluationCriterion : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * MaxPoint;
	NSNumber * Priority;
	NSString * RecEvalDesc;
	NSString * RecEvalName;
	NSNumber * RecEvaluationCriterionId;
	org_tns2_RecGroupEvaluationCriterion * RecGroupEvaluationCriterion;
	NSNumber * RecGroupEvaluationCriterionId;
	org_tns2_ArrayOfRecInterviewPhaseEvaluation * RecInterviewPhaseEvaluations;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecEvaluationCriterion *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * MaxPoint;
@property (retain) NSNumber * Priority;
@property (retain) NSString * RecEvalDesc;
@property (retain) NSString * RecEvalName;
@property (retain) NSNumber * RecEvaluationCriterionId;
@property (retain) org_tns2_RecGroupEvaluationCriterion * RecGroupEvaluationCriterion;
@property (retain) NSNumber * RecGroupEvaluationCriterionId;
@property (retain) org_tns2_ArrayOfRecInterviewPhaseEvaluation * RecInterviewPhaseEvaluations;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecInterviewPhase : NSObject {
	
/* elements */
	NSMutableArray *RecInterviewPhase;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecInterviewPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecInterviewPhase:(org_tns2_RecInterviewPhase *)toAdd;
@property (readonly) NSMutableArray * RecInterviewPhase;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecProcessPhaseResult : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Point_;
	org_tns2_RecCandidate * RecCandidate;
	org_tns2_RecCandidateApplication * RecCandidateApplication;
	NSNumber * RecCandidateApplicationId;
	NSNumber * RecCandidateId;
	NSNumber * RecProcessPhaseResultId;
	org_tns2_RecRecruitmentProcessPhase * RecRecruitmentProcessPhase;
	NSNumber * RecRecruitmentProcessPhaseId;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecProcessPhaseResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Point_;
@property (retain) org_tns2_RecCandidate * RecCandidate;
@property (retain) org_tns2_RecCandidateApplication * RecCandidateApplication;
@property (retain) NSNumber * RecCandidateApplicationId;
@property (retain) NSNumber * RecCandidateId;
@property (retain) NSNumber * RecProcessPhaseResultId;
@property (retain) org_tns2_RecRecruitmentProcessPhase * RecRecruitmentProcessPhase;
@property (retain) NSNumber * RecRecruitmentProcessPhaseId;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecProcessPhaseResult : NSObject {
	
/* elements */
	NSMutableArray *RecProcessPhaseResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecProcessPhaseResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecProcessPhaseResult:(org_tns2_RecProcessPhaseResult *)toAdd;
@property (readonly) NSMutableArray * RecProcessPhaseResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecProcessApplyForJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	org_tns2_OrgJobPosition * OrgJobPosition;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSNumber * RecProcessApplyForJobPositionId;
	org_tns2_RecRecruitmentProcess * RecRecruitmentProcess;
	NSNumber * RecRecruitmentProcessId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecProcessApplyForJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_OrgJobPosition * OrgJobPosition;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * RecProcessApplyForJobPositionId;
@property (retain) org_tns2_RecRecruitmentProcess * RecRecruitmentProcess;
@property (retain) NSNumber * RecRecruitmentProcessId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecProcessApplyForJobPosition : NSObject {
	
/* elements */
	NSMutableArray *RecProcessApplyForJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecProcessApplyForJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecProcessApplyForJobPosition:(org_tns2_RecProcessApplyForJobPosition *)toAdd;
@property (readonly) NSMutableArray * RecProcessApplyForJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecRecruitmentProcessPhase : NSObject {
	
/* elements */
	NSMutableArray *RecRecruitmentProcessPhase;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecRecruitmentProcessPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRecruitmentProcessPhase:(org_tns2_RecRecruitmentProcessPhase *)toAdd;
@property (readonly) NSMutableArray * RecRecruitmentProcessPhase;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecRecruitmentProcess : NSObject {
	
/* elements */
	NSDate * ApplyDate;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	org_tns2_ArrayOfRecProcessApplyForJobPosition * RecProcessApplyForJobPositions;
	NSString * RecProcessDescription;
	NSString * RecProcessName;
	NSNumber * RecRecruitmentProcessId;
	org_tns2_ArrayOfRecRecruitmentProcessPhase * RecRecruitmentProcessPhases;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecRecruitmentProcess *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ApplyDate;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_ArrayOfRecProcessApplyForJobPosition * RecProcessApplyForJobPositions;
@property (retain) NSString * RecProcessDescription;
@property (retain) NSString * RecProcessName;
@property (retain) NSNumber * RecRecruitmentProcessId;
@property (retain) org_tns2_ArrayOfRecRecruitmentProcessPhase * RecRecruitmentProcessPhases;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecRecruitmentProcessPhase : NSObject {
	
/* elements */
	NSNumber * CoefficientPoint;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Priority;
	org_tns2_ArrayOfRecInterviewPhase * RecInterviewPhases;
	NSString * RecProcessPhaseName;
	org_tns2_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
	org_tns2_RecRecruitmentProcess * RecRecruitmentProcess;
	NSNumber * RecRecruitmentProcessId;
	NSNumber * RecRecruitmentProcessPhaseId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecRecruitmentProcessPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CoefficientPoint;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Priority;
@property (retain) org_tns2_ArrayOfRecInterviewPhase * RecInterviewPhases;
@property (retain) NSString * RecProcessPhaseName;
@property (retain) org_tns2_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
@property (retain) org_tns2_RecRecruitmentProcess * RecRecruitmentProcess;
@property (retain) NSNumber * RecRecruitmentProcessId;
@property (retain) NSNumber * RecRecruitmentProcessPhaseId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecInterviewPhase : NSObject {
	
/* elements */
	NSNumber * CoefficientPoint;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * InterviewPhaseName;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Priority;
	org_tns2_ArrayOfRecInterviewPhaseEvaluation * RecInterviewPhaseEvaluations;
	NSNumber * RecInterviewPhaseId;
	org_tns2_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
	NSNumber * RecProcessPhaseId;
	org_tns2_RecRecruitmentProcessPhase * RecRecruitmentProcessPhase;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecInterviewPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CoefficientPoint;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * InterviewPhaseName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Priority;
@property (retain) org_tns2_ArrayOfRecInterviewPhaseEvaluation * RecInterviewPhaseEvaluations;
@property (retain) NSNumber * RecInterviewPhaseId;
@property (retain) org_tns2_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
@property (retain) NSNumber * RecProcessPhaseId;
@property (retain) org_tns2_RecRecruitmentProcessPhase * RecRecruitmentProcessPhase;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecInterviewPhaseEvaluation : NSObject {
	
/* elements */
	NSNumber * ComanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	org_tns2_RecEvaluationCriterion * RecEvaluationCriterion;
	NSNumber * RecEvaluationCriterionId;
	org_tns2_RecInterviewPhase * RecInterviewPhase;
	NSNumber * RecInterviewPhaseEvaluationId;
	NSNumber * RecInterviewPhaseId;
	org_tns2_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecInterviewPhaseEvaluation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ComanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_RecEvaluationCriterion * RecEvaluationCriterion;
@property (retain) NSNumber * RecEvaluationCriterionId;
@property (retain) org_tns2_RecInterviewPhase * RecInterviewPhase;
@property (retain) NSNumber * RecInterviewPhaseEvaluationId;
@property (retain) NSNumber * RecInterviewPhaseId;
@property (retain) org_tns2_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecScheduleInterviewerResult : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Point_;
	org_tns2_RecInterviewPhaseEvaluation * RecInterviewPhaseEvaluation;
	NSNumber * RecInterviewPhaseEvaluationId;
	org_tns2_RecInterviewSchedule * RecInterviewSchedule;
	NSNumber * RecInterviewScheduleId;
	org_tns2_RecInterviewer * RecInterviewer;
	NSNumber * RecInterviewerId;
	NSNumber * RecScheduleInterviewerResultId;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecScheduleInterviewerResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Point_;
@property (retain) org_tns2_RecInterviewPhaseEvaluation * RecInterviewPhaseEvaluation;
@property (retain) NSNumber * RecInterviewPhaseEvaluationId;
@property (retain) org_tns2_RecInterviewSchedule * RecInterviewSchedule;
@property (retain) NSNumber * RecInterviewScheduleId;
@property (retain) org_tns2_RecInterviewer * RecInterviewer;
@property (retain) NSNumber * RecInterviewerId;
@property (retain) NSNumber * RecScheduleInterviewerResultId;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecScheduleInterviewerResult : NSObject {
	
/* elements */
	NSMutableArray *RecScheduleInterviewerResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecScheduleInterviewerResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecScheduleInterviewerResult:(org_tns2_RecScheduleInterviewerResult *)toAdd;
@property (readonly) NSMutableArray * RecScheduleInterviewerResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecInterviewer : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsMainInterviewer;
	NSDate * ModifiedDate;
	org_tns2_RecGroupInterviewer * RecGroupInterviewer;
	NSNumber * RecGroupInterviewerId;
	NSNumber * RecInterviewerId;
	org_tns2_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecInterviewer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainInterviewer;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_RecGroupInterviewer * RecGroupInterviewer;
@property (retain) NSNumber * RecGroupInterviewerId;
@property (retain) NSNumber * RecInterviewerId;
@property (retain) org_tns2_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecInterviewer : NSObject {
	
/* elements */
	NSMutableArray *RecInterviewer;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecInterviewer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecInterviewer:(org_tns2_RecInterviewer *)toAdd;
@property (readonly) NSMutableArray * RecInterviewer;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecGroupInterviewer : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * GroupName;
	USBoolean * IsDeleted;
	NSNumber * RecGroupInterviewerId;
	org_tns2_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
	org_tns2_ArrayOfRecInterviewer * RecInterviewers;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecGroupInterviewer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * GroupName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecGroupInterviewerId;
@property (retain) org_tns2_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
@property (retain) org_tns2_ArrayOfRecInterviewer * RecInterviewers;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecInterviewScheduleStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * Priority;
	NSNumber * RecInterviewScheduleStatusId;
	org_tns2_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
	NSString * StatusCode;
	NSString * StatusName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecInterviewScheduleStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Priority;
@property (retain) NSNumber * RecInterviewScheduleStatusId;
@property (retain) org_tns2_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
@property (retain) NSString * StatusCode;
@property (retain) NSString * StatusName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecInterviewSchedule : NSObject {
	
/* elements */
	NSNumber * AveragePoint;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeCreateId;
	NSDate * InterviewDate;
	NSString * InterviewPlace;
	NSDate * InterviewTime;
	USBoolean * IsDeleted;
	USBoolean * IsIntervieweeNotified;
	USBoolean * IsInterviewerNotified;
	NSDate * ModifiedDate;
	org_tns2_RecCandidateApplication * RecCandidateApplication;
	NSNumber * RecCandidateApplicationId;
	org_tns2_RecGroupInterviewer * RecGroupInterviewer;
	NSNumber * RecGroupInterviewerId;
	org_tns2_RecInterviewPhase * RecInterviewPhase;
	NSNumber * RecInterviewPhaseId;
	NSNumber * RecInterviewScheduleId;
	org_tns2_RecInterviewScheduleStatu * RecInterviewScheduleStatu;
	NSNumber * RecInterviewScheduleStatusId;
	org_tns2_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecInterviewSchedule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AveragePoint;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeCreateId;
@property (retain) NSDate * InterviewDate;
@property (retain) NSString * InterviewPlace;
@property (retain) NSDate * InterviewTime;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsIntervieweeNotified;
@property (retain) USBoolean * IsInterviewerNotified;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_RecCandidateApplication * RecCandidateApplication;
@property (retain) NSNumber * RecCandidateApplicationId;
@property (retain) org_tns2_RecGroupInterviewer * RecGroupInterviewer;
@property (retain) NSNumber * RecGroupInterviewerId;
@property (retain) org_tns2_RecInterviewPhase * RecInterviewPhase;
@property (retain) NSNumber * RecInterviewPhaseId;
@property (retain) NSNumber * RecInterviewScheduleId;
@property (retain) org_tns2_RecInterviewScheduleStatu * RecInterviewScheduleStatu;
@property (retain) NSNumber * RecInterviewScheduleStatusId;
@property (retain) org_tns2_ArrayOfRecScheduleInterviewerResult * RecScheduleInterviewerResults;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecInterviewSchedule : NSObject {
	
/* elements */
	NSMutableArray *RecInterviewSchedule;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecInterviewSchedule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecInterviewSchedule:(org_tns2_RecInterviewSchedule *)toAdd;
@property (readonly) NSMutableArray * RecInterviewSchedule;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecPhaseEmpDisplaced : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	org_tns2_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSString * Reason;
	NSNumber * RecPhaseEmpDisplaced1;
	org_tns2_RecRecruitmentPhase * RecRecruitmentPhase;
	NSNumber * RecRecruitmentPhaseId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecPhaseEmpDisplaced *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * Reason;
@property (retain) NSNumber * RecPhaseEmpDisplaced1;
@property (retain) org_tns2_RecRecruitmentPhase * RecRecruitmentPhase;
@property (retain) NSNumber * RecRecruitmentPhaseId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecPhaseEmpDisplaced : NSObject {
	
/* elements */
	NSMutableArray *RecPhaseEmpDisplaced;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecPhaseEmpDisplaced *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecPhaseEmpDisplaced:(org_tns2_RecPhaseEmpDisplaced *)toAdd;
@property (readonly) NSMutableArray * RecPhaseEmpDisplaced;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecRecruitmentPhase : NSObject {
	
/* elements */
	NSMutableArray *RecRecruitmentPhase;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecRecruitmentPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRecruitmentPhase:(org_tns2_RecRecruitmentPhase *)toAdd;
@property (readonly) NSMutableArray * RecRecruitmentPhase;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecPhaseStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSString * PhaseStatusName;
	NSNumber * RecPhaseStatusId;
	org_tns2_ArrayOfRecRecruitmentPhase * RecRecruitmentPhases;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecPhaseStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * PhaseStatusName;
@property (retain) NSNumber * RecPhaseStatusId;
@property (retain) org_tns2_ArrayOfRecRecruitmentPhase * RecRecruitmentPhases;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecRecruitmentPhaseJobPosition : NSObject {
	
/* elements */
	NSMutableArray *RecRecruitmentPhaseJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecRecruitmentPhaseJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRecruitmentPhaseJobPosition:(org_tns2_RecRecruitmentPhaseJobPosition *)toAdd;
@property (readonly) NSMutableArray * RecRecruitmentPhaseJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_SysRecPlanApprover : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsMainApprover;
	NSNumber * Priority;
	org_tns2_ArrayOfRecPlanApproveHistory * RecPlanApproveHistories;
	NSNumber * SysRecPlanApproverId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_SysRecPlanApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainApprover;
@property (retain) NSNumber * Priority;
@property (retain) org_tns2_ArrayOfRecPlanApproveHistory * RecPlanApproveHistories;
@property (retain) NSNumber * SysRecPlanApproverId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecPlanApproveHistory : NSObject {
	
/* elements */
	NSDate * Date;
	USBoolean * IsApproved;
	USBoolean * IsDeleted;
	NSNumber * RecPlanApproveHistoryId;
	org_tns2_RecRecruitmentPlan * RecRecruitmentPlan;
	NSNumber * RecRecruitmentPlanId;
	NSString * Remark;
	org_tns2_SysRecPlanApprover * SysRecPlanApprover;
	NSNumber * SysRecPlanApproverId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecPlanApproveHistory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * Date;
@property (retain) USBoolean * IsApproved;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecPlanApproveHistoryId;
@property (retain) org_tns2_RecRecruitmentPlan * RecRecruitmentPlan;
@property (retain) NSNumber * RecRecruitmentPlanId;
@property (retain) NSString * Remark;
@property (retain) org_tns2_SysRecPlanApprover * SysRecPlanApprover;
@property (retain) NSNumber * SysRecPlanApproverId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecPlanApproveHistory : NSObject {
	
/* elements */
	NSMutableArray *RecPlanApproveHistory;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecPlanApproveHistory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecPlanApproveHistory:(org_tns2_RecPlanApproveHistory *)toAdd;
@property (readonly) NSMutableArray * RecPlanApproveHistory;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecPlanJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	org_tns2_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSNumber * RecPlanJobPositionId;
	NSString * RecReason;
	org_tns2_RecRecruitmentPlan * RecRecruitmentPlan;
	NSNumber * RecRecruitmentPlanId;
	NSNumber * RecruitedQuantity;
	NSNumber * RequirementQuantity;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecPlanJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * RecPlanJobPositionId;
@property (retain) NSString * RecReason;
@property (retain) org_tns2_RecRecruitmentPlan * RecRecruitmentPlan;
@property (retain) NSNumber * RecRecruitmentPlanId;
@property (retain) NSNumber * RecruitedQuantity;
@property (retain) NSNumber * RequirementQuantity;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecPlanJobPosition : NSObject {
	
/* elements */
	NSMutableArray *RecPlanJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecPlanJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecPlanJobPosition:(org_tns2_RecPlanJobPosition *)toAdd;
@property (readonly) NSMutableArray * RecPlanJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecRecruitmentPlan : NSObject {
	
/* elements */
	NSMutableArray *RecRecruitmentPlan;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecRecruitmentPlan *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRecruitmentPlan:(org_tns2_RecRecruitmentPlan *)toAdd;
@property (readonly) NSMutableArray * RecRecruitmentPlan;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecPlanStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * RecPlanStatusId;
	org_tns2_ArrayOfRecRecruitmentPlan * RecRecruitmentPlans;
	NSString * StatusName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecPlanStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecPlanStatusId;
@property (retain) org_tns2_ArrayOfRecRecruitmentPlan * RecRecruitmentPlans;
@property (retain) NSString * StatusName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecRecruitmentRequirement : NSObject {
	
/* elements */
	NSMutableArray *RecRecruitmentRequirement;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecRecruitmentRequirement *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRecruitmentRequirement:(org_tns2_RecRecruitmentRequirement *)toAdd;
@property (readonly) NSMutableArray * RecRecruitmentRequirement;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecRecruitmentPlan : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * PlanDescription;
	org_tns2_ArrayOfRecPlanApproveHistory * RecPlanApproveHistories;
	NSNumber * RecPlanApproveHistoryId;
	org_tns2_ArrayOfRecPlanJobPosition * RecPlanJobPositions;
	org_tns2_RecPlanStatu * RecPlanStatu;
	NSNumber * RecPlanStatusId;
	NSString * RecRecruitmentPlanCode;
	NSNumber * RecRecruitmentPlanId;
	org_tns2_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
	NSDate * StartDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecRecruitmentPlan *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * PlanDescription;
@property (retain) org_tns2_ArrayOfRecPlanApproveHistory * RecPlanApproveHistories;
@property (retain) NSNumber * RecPlanApproveHistoryId;
@property (retain) org_tns2_ArrayOfRecPlanJobPosition * RecPlanJobPositions;
@property (retain) org_tns2_RecPlanStatu * RecPlanStatu;
@property (retain) NSNumber * RecPlanStatusId;
@property (retain) NSString * RecRecruitmentPlanCode;
@property (retain) NSNumber * RecRecruitmentPlanId;
@property (retain) org_tns2_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
@property (retain) NSDate * StartDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_SysRecRequirementApprover : NSObject {
	
/* elements */
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsMainApprover;
	NSNumber * Priority;
	org_tns2_ArrayOfRecRequirementApproveHistory * RecRequirementApproveHistories;
	NSNumber * SysRecRecruitmentApproverId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_SysRecRequirementApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainApprover;
@property (retain) NSNumber * Priority;
@property (retain) org_tns2_ArrayOfRecRequirementApproveHistory * RecRequirementApproveHistories;
@property (retain) NSNumber * SysRecRecruitmentApproverId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecRequirementApproveHistory : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * Date;
	USBoolean * IsApproved;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * RecRecruitmentApproveHistoryId;
	org_tns2_RecRecruitmentRequirement * RecRecruitmentRequirement;
	NSNumber * RecRequirementId;
	NSString * Remark;
	NSNumber * SysRecRecruitmentApproverId;
	org_tns2_SysRecRequirementApprover * SysRecRequirementApprover;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecRequirementApproveHistory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * Date;
@property (retain) USBoolean * IsApproved;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * RecRecruitmentApproveHistoryId;
@property (retain) org_tns2_RecRecruitmentRequirement * RecRecruitmentRequirement;
@property (retain) NSNumber * RecRequirementId;
@property (retain) NSString * Remark;
@property (retain) NSNumber * SysRecRecruitmentApproverId;
@property (retain) org_tns2_SysRecRequirementApprover * SysRecRequirementApprover;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecRequirementApproveHistory : NSObject {
	
/* elements */
	NSMutableArray *RecRequirementApproveHistory;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecRequirementApproveHistory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRequirementApproveHistory:(org_tns2_RecRequirementApproveHistory *)toAdd;
@property (readonly) NSMutableArray * RecRequirementApproveHistory;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecRequirementEmpDisplaced : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	org_tns2_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSString * Reason;
	org_tns2_RecRecruitmentRequirement * RecRecruitmentRequirement;
	NSNumber * RecRecruitmentRequirementId;
	NSNumber * RecRequirementEmpDisplaced1;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecRequirementEmpDisplaced *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * Reason;
@property (retain) org_tns2_RecRecruitmentRequirement * RecRecruitmentRequirement;
@property (retain) NSNumber * RecRecruitmentRequirementId;
@property (retain) NSNumber * RecRequirementEmpDisplaced1;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecRequirementEmpDisplaced : NSObject {
	
/* elements */
	NSMutableArray *RecRequirementEmpDisplaced;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecRequirementEmpDisplaced *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRequirementEmpDisplaced:(org_tns2_RecRequirementEmpDisplaced *)toAdd;
@property (readonly) NSMutableArray * RecRequirementEmpDisplaced;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecRequirementJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	USBoolean * IsDisplacement;
	NSString * JobDescription;
	NSNumber * JobDescriptionId;
	NSDate * ModifiedDate;
	org_tns2_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSString * RecReason;
	org_tns2_RecRecruitmentRequirement * RecRecruitmentRequirement;
	NSNumber * RecRecruitmentRequirementId;
	NSNumber * RecRequirementJobPositionId;
	NSNumber * RecruitedQuantity;
	NSNumber * RequirementQuantity;
	NSString * WorkingLocation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecRequirementJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDisplacement;
@property (retain) NSString * JobDescription;
@property (retain) NSNumber * JobDescriptionId;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * RecReason;
@property (retain) org_tns2_RecRecruitmentRequirement * RecRecruitmentRequirement;
@property (retain) NSNumber * RecRecruitmentRequirementId;
@property (retain) NSNumber * RecRequirementJobPositionId;
@property (retain) NSNumber * RecruitedQuantity;
@property (retain) NSNumber * RequirementQuantity;
@property (retain) NSString * WorkingLocation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecRequirementJobPosition : NSObject {
	
/* elements */
	NSMutableArray *RecRequirementJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecRequirementJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecRequirementJobPosition:(org_tns2_RecRequirementJobPosition *)toAdd;
@property (readonly) NSMutableArray * RecRequirementJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecRequirementStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	org_tns2_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
	NSNumber * RecRequirementStatusId;
	NSString * StatusName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecRequirementStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) org_tns2_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
@property (retain) NSNumber * RecRequirementStatusId;
@property (retain) NSString * StatusName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecRecruitmentRequirement : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgUnitId;
	NSDate * RecBeginDate;
	NSDate * RecEndDate;
	NSNumber * RecRecruitmentApproveHistoryId;
	org_tns2_ArrayOfRecRecruitmentPhase * RecRecruitmentPhases;
	org_tns2_RecRecruitmentPlan * RecRecruitmentPlan;
	NSNumber * RecRecruitmentPlanId;
	NSNumber * RecRecruitmentRequirementId;
	org_tns2_ArrayOfRecRequirementApproveHistory * RecRequirementApproveHistories;
	org_tns2_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
	org_tns2_ArrayOfRecRequirementJobPosition * RecRequirementJobPositions;
	org_tns2_RecRequirementStatu * RecRequirementStatu;
	NSNumber * RecRequirementStatusId;
	NSNumber * RecruitedQuantity;
	NSString * RecruitmentPurpose;
	NSString * Remark;
	NSString * RequirementCode;
	NSDate * RequirementDate;
	NSString * RequirementName;
	NSNumber * TotalQuantity;
	NSNumber * UrgentLevel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecRecruitmentRequirement *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSDate * RecBeginDate;
@property (retain) NSDate * RecEndDate;
@property (retain) NSNumber * RecRecruitmentApproveHistoryId;
@property (retain) org_tns2_ArrayOfRecRecruitmentPhase * RecRecruitmentPhases;
@property (retain) org_tns2_RecRecruitmentPlan * RecRecruitmentPlan;
@property (retain) NSNumber * RecRecruitmentPlanId;
@property (retain) NSNumber * RecRecruitmentRequirementId;
@property (retain) org_tns2_ArrayOfRecRequirementApproveHistory * RecRequirementApproveHistories;
@property (retain) org_tns2_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
@property (retain) org_tns2_ArrayOfRecRequirementJobPosition * RecRequirementJobPositions;
@property (retain) org_tns2_RecRequirementStatu * RecRequirementStatu;
@property (retain) NSNumber * RecRequirementStatusId;
@property (retain) NSNumber * RecruitedQuantity;
@property (retain) NSString * RecruitmentPurpose;
@property (retain) NSString * Remark;
@property (retain) NSString * RequirementCode;
@property (retain) NSDate * RequirementDate;
@property (retain) NSString * RequirementName;
@property (retain) NSNumber * TotalQuantity;
@property (retain) NSNumber * UrgentLevel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecRecruitmentPhase : NSObject {
	
/* elements */
	NSDate * ApplicationDate;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	USBoolean * IsNotified;
	NSDate * ModifiedDate;
	NSString * PhaseName;
	org_tns2_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
	org_tns2_RecPhaseStatu * RecPhaseStatu;
	NSNumber * RecPhaseStatusId;
	NSString * RecRecruitmentPhaseCode;
	NSNumber * RecRecruitmentPhaseId;
	org_tns2_ArrayOfRecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPositions;
	org_tns2_RecRecruitmentRequirement * RecRecruitmentRequirement;
	NSNumber * RecRecruitmentRequirementId;
	NSString * Remark;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecRecruitmentPhase *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ApplicationDate;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsNotified;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * PhaseName;
@property (retain) org_tns2_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
@property (retain) org_tns2_RecPhaseStatu * RecPhaseStatu;
@property (retain) NSNumber * RecPhaseStatusId;
@property (retain) NSString * RecRecruitmentPhaseCode;
@property (retain) NSNumber * RecRecruitmentPhaseId;
@property (retain) org_tns2_ArrayOfRecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPositions;
@property (retain) org_tns2_RecRecruitmentRequirement * RecRecruitmentRequirement;
@property (retain) NSNumber * RecRecruitmentRequirementId;
@property (retain) NSString * Remark;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecRecruitmentPhaseJobPosition : NSObject {
	
/* elements */
	NSString * AgeLimitation;
	NSString * BeginSalaryLevel;
	NSString * BenefitAllowance;
	NSNumber * CompanyId;
	NSString * ContractTerm;
	NSDate * CreatedDate;
	NSNumber * DirectLeader;
	NSDate * FromDate;
	NSString * GenderLimitation;
	USBoolean * HasProbation;
	USBoolean * IsDeleted;
	USBoolean * IsDisplacement;
	NSString * JobDescription;
	NSDate * ModifiedDate;
	org_tns2_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSString * ProbationTime;
	org_tns2_ArrayOfRecCandidateApplication * RecCandidateApplications;
	org_tns2_RecRecruitmentPhase * RecRecruitmentPhase;
	NSNumber * RecRecruitmentPhaseId;
	NSNumber * RecRecruitmentPhaseJobPositionId;
	NSNumber * RecruitedQuantity;
	NSNumber * RequirementQuantity;
	NSDate * StartWorkDate;
	NSDate * ToDate;
	NSString * WorkingLocation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecRecruitmentPhaseJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AgeLimitation;
@property (retain) NSString * BeginSalaryLevel;
@property (retain) NSString * BenefitAllowance;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ContractTerm;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * DirectLeader;
@property (retain) NSDate * FromDate;
@property (retain) NSString * GenderLimitation;
@property (retain) USBoolean * HasProbation;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDisplacement;
@property (retain) NSString * JobDescription;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * ProbationTime;
@property (retain) org_tns2_ArrayOfRecCandidateApplication * RecCandidateApplications;
@property (retain) org_tns2_RecRecruitmentPhase * RecRecruitmentPhase;
@property (retain) NSNumber * RecRecruitmentPhaseId;
@property (retain) NSNumber * RecRecruitmentPhaseJobPositionId;
@property (retain) NSNumber * RecruitedQuantity;
@property (retain) NSNumber * RequirementQuantity;
@property (retain) NSDate * StartWorkDate;
@property (retain) NSDate * ToDate;
@property (retain) NSString * WorkingLocation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecCandidateApplication : NSObject {
	
/* elements */
	NSNumber * AveragePoint;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * CurrentInterviewPhaseId;
	NSNumber * CurrentProcessPhaseId;
	NSString * HRRemark;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSDate * NegotiateDate;
	org_tns2_RecCandidate * RecCandidate;
	NSNumber * RecCandidateApplicationId;
	NSNumber * RecCandidateId;
	org_tns2_RecCandidateProfileStatu * RecCandidateProfileStatu;
	NSNumber * RecCandidateProfileStatusId;
	org_tns2_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
	org_tns2_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
	org_tns2_RecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPosition;
	NSNumber * RecRecruitmentPhaseJobPositionId;
	NSDate * StartWorkingDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecCandidateApplication *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AveragePoint;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * CurrentInterviewPhaseId;
@property (retain) NSNumber * CurrentProcessPhaseId;
@property (retain) NSString * HRRemark;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * NegotiateDate;
@property (retain) org_tns2_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateApplicationId;
@property (retain) NSNumber * RecCandidateId;
@property (retain) org_tns2_RecCandidateProfileStatu * RecCandidateProfileStatu;
@property (retain) NSNumber * RecCandidateProfileStatusId;
@property (retain) org_tns2_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
@property (retain) org_tns2_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
@property (retain) org_tns2_RecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPosition;
@property (retain) NSNumber * RecRecruitmentPhaseJobPositionId;
@property (retain) NSDate * StartWorkingDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecCandidateApplication : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateApplication;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecCandidateApplication *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateApplication:(org_tns2_RecCandidateApplication *)toAdd;
@property (readonly) NSMutableArray * RecCandidateApplication;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecCandidateCompetencyRating : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateCompetencyRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecCandidateCompetencyRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateCompetencyRating:(org_tns2_RecCandidateCompetencyRating *)toAdd;
@property (readonly) NSMutableArray * RecCandidateCompetencyRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CLogDegree : NSObject {
	
/* elements */
	NSNumber * CLogDegreeId;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * Note;
	org_tns2_ArrayOfRecCandidateDegree * RecCandidateDegrees;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CLogDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogDegreeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) org_tns2_ArrayOfRecCandidateDegree * RecCandidateDegrees;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecCandidateDegree : NSObject {
	
/* elements */
	org_tns2_CLogDegree * CLogDegree;
	NSNumber * CLogDegreeId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	USBoolean * IsDeleted;
	USBoolean * IsValId;
	NSDate * ModifiedDate;
	NSString * Other;
	NSString * PlaceIssue;
	org_tns2_RecCandidate * RecCandidate;
	NSNumber * RecCandidateDegreeId;
	NSNumber * RecCandidateId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecCandidateDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_CLogDegree * CLogDegree;
@property (retain) NSNumber * CLogDegreeId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) org_tns2_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateDegreeId;
@property (retain) NSNumber * RecCandidateId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecCandidateDegree : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateDegree;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecCandidateDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateDegree:(org_tns2_RecCandidateDegree *)toAdd;
@property (readonly) NSMutableArray * RecCandidateDegree;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgExperience : NSObject {
	
/* elements */
	NSMutableArray *OrgExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgExperience:(org_tns2_OrgExperience *)toAdd;
@property (readonly) NSMutableArray * OrgExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgProjectType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	org_tns2_ArrayOfOrgExperience * OrgExperiences;
	NSNumber * OrgProjectTypeId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgProjectType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) org_tns2_ArrayOfOrgExperience * OrgExperiences;
@property (retain) NSNumber * OrgProjectTypeId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgTimeInCharge : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	org_tns2_ArrayOfOrgExperience * OrgExperiences;
	NSNumber * OrgTimeInChargeId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgTimeInCharge *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_ArrayOfOrgExperience * OrgExperiences;
@property (retain) NSNumber * OrgTimeInChargeId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgExperience : NSObject {
	
/* elements */
	NSString * Achievement;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgExperienceId;
	org_tns2_OrgProjectType * OrgProjectType;
	NSNumber * OrgProjectTypeId;
	org_tns2_OrgTimeInCharge * OrgTimeInCharge;
	NSNumber * OrgTimeInChargeId;
	org_tns2_ArrayOfRecCandidateExperience * RecCandidateExperiences;
	NSString * RoleDescription;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Achievement;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgExperienceId;
@property (retain) org_tns2_OrgProjectType * OrgProjectType;
@property (retain) NSNumber * OrgProjectTypeId;
@property (retain) org_tns2_OrgTimeInCharge * OrgTimeInCharge;
@property (retain) NSNumber * OrgTimeInChargeId;
@property (retain) org_tns2_ArrayOfRecCandidateExperience * RecCandidateExperiences;
@property (retain) NSString * RoleDescription;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecCandidateExperience : NSObject {
	
/* elements */
	org_tns2_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompanyName;
	NSDate * CreatedDate;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	USBoolean * IsInternal;
	NSString * JobPosition;
	NSDate * ModifiedDate;
	NSString * NotableAchievements;
	NSString * Note;
	org_tns2_OrgExperience * OrgExperience;
	NSNumber * OrgExperienceId;
	org_tns2_RecCandidate * RecCandidate;
	NSNumber * RecCandidateExperienceId;
	NSNumber * RecCandidateId;
	NSString * Responsibilities;
	NSDate * StartDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecCandidateExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompanyName;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInternal;
@property (retain) NSString * JobPosition;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * NotableAchievements;
@property (retain) NSString * Note;
@property (retain) org_tns2_OrgExperience * OrgExperience;
@property (retain) NSNumber * OrgExperienceId;
@property (retain) org_tns2_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateExperienceId;
@property (retain) NSNumber * RecCandidateId;
@property (retain) NSString * Responsibilities;
@property (retain) NSDate * StartDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecCandidateExperience : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecCandidateExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateExperience:(org_tns2_RecCandidateExperience *)toAdd;
@property (readonly) NSMutableArray * RecCandidateExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CBPITDependent : NSObject {
	
/* elements */
	NSNumber * CBPITDependentId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DependentBeginDate;
	NSString * DependentDemonstrative;
	NSDate * DependentEndDate;
	org_tns2_EmpProfileFamilyRelationship * EmpProfileFamilyRelationship;
	NSNumber * EmpProfileFamilyRelationshipId;
	USBoolean * IsDeleted;
	USBoolean * IsDependent;
	NSDate * ModifiedDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CBPITDependent *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CBPITDependentId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DependentBeginDate;
@property (retain) NSString * DependentDemonstrative;
@property (retain) NSDate * DependentEndDate;
@property (retain) org_tns2_EmpProfileFamilyRelationship * EmpProfileFamilyRelationship;
@property (retain) NSNumber * EmpProfileFamilyRelationshipId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDependent;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfCBPITDependent : NSObject {
	
/* elements */
	NSMutableArray *CBPITDependent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfCBPITDependent *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCBPITDependent:(org_tns2_CBPITDependent *)toAdd;
@property (readonly) NSMutableArray * CBPITDependent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpBeneficiary : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpBeneficiaryId;
	NSNumber * EmpFamilyRelationshipId;
	org_tns2_EmpProfileFamilyRelationship * EmpProfileFamilyRelationship;
	NSDate * ImplementationDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpBeneficiary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpBeneficiaryId;
@property (retain) NSNumber * EmpFamilyRelationshipId;
@property (retain) org_tns2_EmpProfileFamilyRelationship * EmpProfileFamilyRelationship;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpBeneficiary : NSObject {
	
/* elements */
	NSMutableArray *EmpBeneficiary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpBeneficiary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpBeneficiary:(org_tns2_EmpBeneficiary *)toAdd;
@property (readonly) NSMutableArray * EmpBeneficiary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileFamilyRelationship : NSObject {
	
/* elements */
	NSString * Address;
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSDate * BirthDay;
	org_tns2_ArrayOfCBPITDependent * CBPITDependents;
	org_tns2_CLogFamilyRelationship * CLogFamilyRelationship;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DeathDate;
	org_tns2_ArrayOfEmpBeneficiary * EmpBeneficiaries;
	NSNumber * EmpProfileFamilyRelationshipId;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * HomeTown;
	USBoolean * IsDeleted;
	USBoolean * IsEmergencyContact;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	USBoolean * Nourish;
	NSString * Occupation;
	NSString * OccupationPlace;
	NSString * PersonFirstName;
	NSString * PersonFullName;
	NSString * PersonLastName;
	NSNumber * PersonSameOccupationId;
	NSString * Phone;
	NSNumber * RelateRowId;
	NSNumber * RelationshipId;
	NSString * RelationshipName;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSDate * BirthDay;
@property (retain) org_tns2_ArrayOfCBPITDependent * CBPITDependents;
@property (retain) org_tns2_CLogFamilyRelationship * CLogFamilyRelationship;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DeathDate;
@property (retain) org_tns2_ArrayOfEmpBeneficiary * EmpBeneficiaries;
@property (retain) NSNumber * EmpProfileFamilyRelationshipId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * HomeTown;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEmergencyContact;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) USBoolean * Nourish;
@property (retain) NSString * Occupation;
@property (retain) NSString * OccupationPlace;
@property (retain) NSString * PersonFirstName;
@property (retain) NSString * PersonFullName;
@property (retain) NSString * PersonLastName;
@property (retain) NSNumber * PersonSameOccupationId;
@property (retain) NSString * Phone;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSNumber * RelationshipId;
@property (retain) NSString * RelationshipName;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileFamilyRelationship : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileFamilyRelationship;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileFamilyRelationship:(org_tns2_EmpProfileFamilyRelationship *)toAdd;
@property (readonly) NSMutableArray * EmpProfileFamilyRelationship;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CLogFamilyRelationship : NSObject {
	
/* elements */
	NSNumber * CLogFamilyRelationshipId;
	NSNumber * CompanyId;
	org_tns2_ArrayOfEmpProfileFamilyRelationship * EmpProfileFamilyRelationships;
	NSString * FamilyRelationshipCode;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
	org_tns2_ArrayOfRecCandidateFamilyRelationship * RecCandidateFamilyRelationships;
	USBoolean * Sex;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CLogFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogFamilyRelationshipId;
@property (retain) NSNumber * CompanyId;
@property (retain) org_tns2_ArrayOfEmpProfileFamilyRelationship * EmpProfileFamilyRelationships;
@property (retain) NSString * FamilyRelationshipCode;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
@property (retain) org_tns2_ArrayOfRecCandidateFamilyRelationship * RecCandidateFamilyRelationships;
@property (retain) USBoolean * Sex;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecCandidateFamilyRelationship : NSObject {
	
/* elements */
	NSString * Address;
	NSDate * BirthDay;
	org_tns2_CLogFamilyRelationship * CLogFamilyRelationship;
	NSNumber * CLogFamilyRelationshipId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DeathDate;
	NSString * HomeTown;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	USBoolean * Nourish;
	NSString * Occupation;
	NSString * OccupationPlace;
	NSString * PersonFirstName;
	NSString * PersonFullName;
	NSString * PersonLastName;
	NSNumber * PersonSameOccupationId;
	NSString * Phone;
	org_tns2_RecCandidate * RecCandidate;
	NSNumber * RecCandidateFamilyRelationshipId;
	NSNumber * RecCandidateId;
	NSString * RelationshipName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecCandidateFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSDate * BirthDay;
@property (retain) org_tns2_CLogFamilyRelationship * CLogFamilyRelationship;
@property (retain) NSNumber * CLogFamilyRelationshipId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DeathDate;
@property (retain) NSString * HomeTown;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) USBoolean * Nourish;
@property (retain) NSString * Occupation;
@property (retain) NSString * OccupationPlace;
@property (retain) NSString * PersonFirstName;
@property (retain) NSString * PersonFullName;
@property (retain) NSString * PersonLastName;
@property (retain) NSNumber * PersonSameOccupationId;
@property (retain) NSString * Phone;
@property (retain) org_tns2_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateFamilyRelationshipId;
@property (retain) NSNumber * RecCandidateId;
@property (retain) NSString * RelationshipName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecCandidateFamilyRelationship : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateFamilyRelationship;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecCandidateFamilyRelationship *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateFamilyRelationship:(org_tns2_RecCandidateFamilyRelationship *)toAdd;
@property (readonly) NSMutableArray * RecCandidateFamilyRelationship;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecCandidateForeignLanguage : NSObject {
	
/* elements */
	NSString * AttachmenttUrl;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Degree;
	NSDate * Duration_;
	NSDate * EffectiveDate;
	USBoolean * IsDeleted;
	NSString * Language;
	NSString * LanguageSkill;
	NSDate * ModifiedDate;
	NSString * Note;
	org_tns2_RecCandidate * RecCandidate;
	NSNumber * RecCandidateForeignLanguageId;
	NSNumber * RecCandidateId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecCandidateForeignLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AttachmenttUrl;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Degree;
@property (retain) NSDate * Duration_;
@property (retain) NSDate * EffectiveDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Language;
@property (retain) NSString * LanguageSkill;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) org_tns2_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateForeignLanguageId;
@property (retain) NSNumber * RecCandidateId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecCandidateForeignLanguage : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateForeignLanguage;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecCandidateForeignLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateForeignLanguage:(org_tns2_RecCandidateForeignLanguage *)toAdd;
@property (readonly) NSMutableArray * RecCandidateForeignLanguage;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileQualification : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	NSNumber * EmpProfileQualificationId;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsValId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * OrgDegreeRankId;
	org_tns2_OrgQualification * OrgQualification;
	NSNumber * OrgQualificationId;
	NSString * Other;
	NSString * PlaceIssue;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) NSNumber * EmpProfileQualificationId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgDegreeRankId;
@property (retain) org_tns2_OrgQualification * OrgQualification;
@property (retain) NSNumber * OrgQualificationId;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileQualification : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileQualification;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileQualification:(org_tns2_EmpProfileQualification *)toAdd;
@property (readonly) NSMutableArray * EmpProfileQualification;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgQualification : NSObject {
	
/* elements */
	NSNumber * CLogEducationLevelId;
	NSNumber * CLogMajorId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * Description;
	org_tns2_ArrayOfEmpProfileQualification * EmpProfileQualifications;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * OrgQualificationCode;
	NSNumber * OrgQualificationId;
	NSString * OrgQualificationName;
	org_tns2_ArrayOfRecCandidateQualification * RecCandidateQualifications;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogEducationLevelId;
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) org_tns2_ArrayOfEmpProfileQualification * EmpProfileQualifications;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OrgQualificationCode;
@property (retain) NSNumber * OrgQualificationId;
@property (retain) NSString * OrgQualificationName;
@property (retain) org_tns2_ArrayOfRecCandidateQualification * RecCandidateQualifications;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecCandidateQualification : NSObject {
	
/* elements */
	org_tns2_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	USBoolean * IsDeleted;
	USBoolean * IsValId;
	NSDate * ModifiedDate;
	org_tns2_OrgQualification * OrgQualification;
	NSNumber * OrgQualificationId;
	NSString * Other;
	NSString * PlaceIssue;
	org_tns2_RecCandidate * RecCandidate;
	NSNumber * RecCandidateId;
	NSNumber * RecCandidateQualificationId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecCandidateQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValId;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_OrgQualification * OrgQualification;
@property (retain) NSNumber * OrgQualificationId;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) org_tns2_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateId;
@property (retain) NSNumber * RecCandidateQualificationId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecCandidateQualification : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateQualification;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecCandidateQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateQualification:(org_tns2_RecCandidateQualification *)toAdd;
@property (readonly) NSMutableArray * RecCandidateQualification;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileSkill : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	org_tns2_CLogRating * CLogRating;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * DocumentRef;
	NSNumber * EmpProfileSkillId;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	org_tns2_OrgSkill * OrgSkill;
	NSNumber * OrgSkillId;
	NSNumber * RatingId;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) org_tns2_CLogRating * CLogRating;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DocumentRef;
@property (retain) NSNumber * EmpProfileSkillId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_OrgSkill * OrgSkill;
@property (retain) NSNumber * OrgSkillId;
@property (retain) NSNumber * RatingId;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileSkill : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileSkill:(org_tns2_EmpProfileSkill *)toAdd;
@property (readonly) NSMutableArray * EmpProfileSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgSkill : NSObject {
	
/* elements */
	NSMutableArray *OrgSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgSkill:(org_tns2_OrgSkill *)toAdd;
@property (readonly) NSMutableArray * OrgSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgSkillType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * OrgSkillTypeId;
	org_tns2_ArrayOfOrgSkill * OrgSkills;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgSkillType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * OrgSkillTypeId;
@property (retain) org_tns2_ArrayOfOrgSkill * OrgSkills;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfTrainingCourseSchedule : NSObject {
	
/* elements */
	NSMutableArray *TrainingCourseSchedule;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfTrainingCourseSchedule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingCourseSchedule:(org_tns2_TrainingCourseSchedule *)toAdd;
@property (readonly) NSMutableArray * TrainingCourseSchedule;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CLogCourseSchedule : NSObject {
	
/* elements */
	NSNumber * ClogCourseScheduleId;
	NSNumber * DayOfWeek;
	NSDate * EndTime;
	USBoolean * IsDeleted;
	NSNumber * Shift;
	NSDate * StartTime;
	org_tns2_ArrayOfTrainingCourseSchedule * TrainingCourseSchedules;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CLogCourseSchedule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ClogCourseScheduleId;
@property (retain) NSNumber * DayOfWeek;
@property (retain) NSDate * EndTime;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Shift;
@property (retain) NSDate * StartTime;
@property (retain) org_tns2_ArrayOfTrainingCourseSchedule * TrainingCourseSchedules;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfTrainingCourse : NSObject {
	
/* elements */
	NSMutableArray *TrainingCourse;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfTrainingCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingCourse:(org_tns2_TrainingCourse *)toAdd;
@property (readonly) NSMutableArray * TrainingCourse;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CLogTrainingCenter : NSObject {
	
/* elements */
	NSNumber * CLogTrainingCenterId;
	USBoolean * IsDeleted;
	NSString * Name;
	org_tns2_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CLogTrainingCenter *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogTrainingCenterId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) org_tns2_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_TrainingCategory : NSObject {
	
/* elements */
	NSDate * CreateDate;
	NSString * Description;
	NSString * ImgUrl;
	USBoolean * IsDeleted;
	NSString * Name;
	NSNumber * TrainingCategoryId;
	org_tns2_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_TrainingCategory *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) NSString * ImgUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSNumber * TrainingCategoryId;
@property (retain) org_tns2_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_TrainingCourseChapter : NSObject {
	
/* elements */
	NSNumber * Chapter;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * Session;
	org_tns2_TrainingCourse * TrainingCourse;
	NSNumber * TrainingCourseChapterId;
	NSNumber * TrainingCourseId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_TrainingCourseChapter *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Chapter;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * Session;
@property (retain) org_tns2_TrainingCourse * TrainingCourse;
@property (retain) NSNumber * TrainingCourseChapterId;
@property (retain) NSNumber * TrainingCourseId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfTrainingCourseChapter : NSObject {
	
/* elements */
	NSMutableArray *TrainingCourseChapter;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfTrainingCourseChapter *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingCourseChapter:(org_tns2_TrainingCourseChapter *)toAdd;
@property (readonly) NSMutableArray * TrainingCourseChapter;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_TrainingCoursePeriod : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
	NSNumber * Session;
	NSString * Target;
	NSNumber * TrainingCoursePeriodId;
	org_tns2_ArrayOfTrainingCourse * TrainingCourses;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_TrainingCoursePeriod *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) NSNumber * Session;
@property (retain) NSString * Target;
@property (retain) NSNumber * TrainingCoursePeriodId;
@property (retain) org_tns2_ArrayOfTrainingCourse * TrainingCourses;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_TrainingCourseType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * Note;
	NSNumber * TrainingCourseTypeId;
	org_tns2_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_TrainingCourseType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) NSNumber * TrainingCourseTypeId;
@property (retain) org_tns2_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_TrainingCourseUnit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * OrgUnitId;
	org_tns2_TrainingCourse * TrainingCourse;
	NSNumber * TrainingCourseId;
	NSNumber * TrainingCourseUnitId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_TrainingCourseUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * OrgUnitId;
@property (retain) org_tns2_TrainingCourse * TrainingCourse;
@property (retain) NSNumber * TrainingCourseId;
@property (retain) NSNumber * TrainingCourseUnitId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfTrainingCourseUnit : NSObject {
	
/* elements */
	NSMutableArray *TrainingCourseUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfTrainingCourseUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingCourseUnit:(org_tns2_TrainingCourseUnit *)toAdd;
@property (readonly) NSMutableArray * TrainingCourseUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_TrainingPlanDegree : NSObject {
	
/* elements */
	NSNumber * CLogDegreeId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	org_tns2_TrainingCourse * TrainingCourse;
	NSNumber * TrainingPlanDegreeId;
	NSNumber * TraningCourseId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_TrainingPlanDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogDegreeId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_TrainingCourse * TrainingCourse;
@property (retain) NSNumber * TrainingPlanDegreeId;
@property (retain) NSNumber * TraningCourseId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfTrainingPlanDegree : NSObject {
	
/* elements */
	NSMutableArray *TrainingPlanDegree;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfTrainingPlanDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingPlanDegree:(org_tns2_TrainingPlanDegree *)toAdd;
@property (readonly) NSMutableArray * TrainingPlanDegree;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileEducation : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	org_tns2_CLogMajor * CLogMajor;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileEducationId;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EndYear;
	USBoolean * IsDeleted;
	NSString * Major;
	NSNumber * MajorId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * RelateRowId;
	NSNumber * StartYear;
	NSString * University;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileEducation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) org_tns2_CLogMajor * CLogMajor;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileEducationId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EndYear;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Major;
@property (retain) NSNumber * MajorId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSNumber * StartYear;
@property (retain) NSString * University;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileEducation : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileEducation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileEducation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileEducation:(org_tns2_EmpProfileEducation *)toAdd;
@property (readonly) NSMutableArray * EmpProfileEducation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfTrainingPlanRequest : NSObject {
	
/* elements */
	NSMutableArray *TrainingPlanRequest;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfTrainingPlanRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingPlanRequest:(org_tns2_TrainingPlanRequest *)toAdd;
@property (readonly) NSMutableArray * TrainingPlanRequest;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CLogMajor : NSObject {
	
/* elements */
	NSNumber * CLogCareerId;
	NSString * CLogMajorCode;
	NSNumber * CLogMajorId;
	org_tns2_ArrayOfEmpProfileEducation * EmpProfileEducations;
	USBoolean * IsDeleted;
	NSString * Name;
	NSString * NameEN;
	org_tns2_ArrayOfTrainingPlanRequest * TrainingPlanRequests;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CLogMajor *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogCareerId;
@property (retain) NSString * CLogMajorCode;
@property (retain) NSNumber * CLogMajorId;
@property (retain) org_tns2_ArrayOfEmpProfileEducation * EmpProfileEducations;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) org_tns2_ArrayOfTrainingPlanRequest * TrainingPlanRequests;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_TrainingPlanEmployee : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * TrainingPlanEmployeeId;
	org_tns2_TrainingPlanRequest * TrainingPlanRequest;
	NSNumber * TrainingPlanRequestId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_TrainingPlanEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * TrainingPlanEmployeeId;
@property (retain) org_tns2_TrainingPlanRequest * TrainingPlanRequest;
@property (retain) NSNumber * TrainingPlanRequestId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfTrainingPlanEmployee : NSObject {
	
/* elements */
	NSMutableArray *TrainingPlanEmployee;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfTrainingPlanEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingPlanEmployee:(org_tns2_TrainingPlanEmployee *)toAdd;
@property (readonly) NSMutableArray * TrainingPlanEmployee;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_TrainingPlanRequest : NSObject {
	
/* elements */
	NSNumber * ApproverId;
	org_tns2_CLogMajor * CLogMajor;
	NSNumber * CLogMajorId;
	NSNumber * CompanyId;
	NSString * Content;
	NSNumber * Cost;
	NSDate * CreatedDate;
	NSDate * EndDate;
	USBoolean * IsApproved;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSDate * StartDate;
	NSNumber * StrategyGoalId;
	NSString * Target;
	NSString * Title;
	org_tns2_ArrayOfTrainingCourse * TrainingCourses;
	org_tns2_ArrayOfTrainingPlanEmployee * TrainingPlanEmployees;
	NSNumber * TrainingPlanRequestId;
	NSNumber * TrainingPlanRequestTypeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_TrainingPlanRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproverId;
@property (retain) org_tns2_CLogMajor * CLogMajor;
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Content;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsApproved;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * StartDate;
@property (retain) NSNumber * StrategyGoalId;
@property (retain) NSString * Target;
@property (retain) NSString * Title;
@property (retain) org_tns2_ArrayOfTrainingCourse * TrainingCourses;
@property (retain) org_tns2_ArrayOfTrainingPlanEmployee * TrainingPlanEmployees;
@property (retain) NSNumber * TrainingPlanRequestId;
@property (retain) NSNumber * TrainingPlanRequestTypeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_TrainingProficencyExpected : NSObject {
	
/* elements */
	org_tns2_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	USBoolean * IsDeleted;
	NSNumber * ProficencyId;
	NSNumber * ProficencyType;
	org_tns2_TrainingCourse * TrainingCourse;
	NSNumber * TrainingCourseId;
	NSNumber * TrainingProficencyExpectedId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_TrainingProficencyExpected *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ProficencyId;
@property (retain) NSNumber * ProficencyType;
@property (retain) org_tns2_TrainingCourse * TrainingCourse;
@property (retain) NSNumber * TrainingCourseId;
@property (retain) NSNumber * TrainingProficencyExpectedId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfTrainingProficencyExpected : NSObject {
	
/* elements */
	NSMutableArray *TrainingProficencyExpected;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfTrainingProficencyExpected *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingProficencyExpected:(org_tns2_TrainingProficencyExpected *)toAdd;
@property (readonly) NSMutableArray * TrainingProficencyExpected;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_TrainingProficencyRequire : NSObject {
	
/* elements */
	org_tns2_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	USBoolean * IsDeleted;
	NSNumber * ProficencyId;
	NSNumber * ProficencyType;
	org_tns2_TrainingCourse * TrainingCourse;
	NSNumber * TrainingCourseId;
	NSNumber * TrainingProficencyRequireId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_TrainingProficencyRequire *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ProficencyId;
@property (retain) NSNumber * ProficencyType;
@property (retain) org_tns2_TrainingCourse * TrainingCourse;
@property (retain) NSNumber * TrainingCourseId;
@property (retain) NSNumber * TrainingProficencyRequireId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfTrainingProficencyRequire : NSObject {
	
/* elements */
	NSMutableArray *TrainingProficencyRequire;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfTrainingProficencyRequire *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingProficencyRequire:(org_tns2_TrainingProficencyRequire *)toAdd;
@property (readonly) NSMutableArray * TrainingProficencyRequire;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_TrainingCourse : NSObject {
	
/* elements */
	org_tns2_CLogTrainer * CLogTrainer;
	NSNumber * CLogTrainerId;
	org_tns2_CLogTrainingCenter * CLogTrainingCenter;
	NSNumber * CLogTrainingCenterId;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSDate * CreatedDate;
	NSString * Email;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	USBoolean * IsExpired;
	USBoolean * IsLongTrainingCourse;
	USBoolean * IsNecessitated;
	USBoolean * IsPublish;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * Number;
	NSString * PhoneNumber;
	NSString * Result;
	NSDate * StartDate;
	NSNumber * StrategyGoalId;
	org_tns2_TrainingCategory * TrainingCategory;
	NSNumber * TrainingCategoryId;
	org_tns2_ArrayOfTrainingCourseChapter * TrainingCourseChapters;
	NSNumber * TrainingCourseId;
	org_tns2_TrainingCoursePeriod * TrainingCoursePeriod;
	NSNumber * TrainingCoursePeriodId;
	org_tns2_ArrayOfTrainingCourseSchedule * TrainingCourseSchedules;
	org_tns2_TrainingCourseType * TrainingCourseType;
	NSNumber * TrainingCourseTypeId;
	org_tns2_ArrayOfTrainingCourseUnit * TrainingCourseUnits;
	org_tns2_ArrayOfTrainingPlanDegree * TrainingPlanDegrees;
	org_tns2_TrainingPlanRequest * TrainingPlanRequest;
	NSNumber * TrainingPlanRequestId;
	org_tns2_ArrayOfTrainingProficencyExpected * TrainingProficencyExpecteds;
	org_tns2_ArrayOfTrainingProficencyRequire * TrainingProficencyRequires;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_TrainingCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_CLogTrainer * CLogTrainer;
@property (retain) NSNumber * CLogTrainerId;
@property (retain) org_tns2_CLogTrainingCenter * CLogTrainingCenter;
@property (retain) NSNumber * CLogTrainingCenterId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Email;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsExpired;
@property (retain) USBoolean * IsLongTrainingCourse;
@property (retain) USBoolean * IsNecessitated;
@property (retain) USBoolean * IsPublish;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * Number;
@property (retain) NSString * PhoneNumber;
@property (retain) NSString * Result;
@property (retain) NSDate * StartDate;
@property (retain) NSNumber * StrategyGoalId;
@property (retain) org_tns2_TrainingCategory * TrainingCategory;
@property (retain) NSNumber * TrainingCategoryId;
@property (retain) org_tns2_ArrayOfTrainingCourseChapter * TrainingCourseChapters;
@property (retain) NSNumber * TrainingCourseId;
@property (retain) org_tns2_TrainingCoursePeriod * TrainingCoursePeriod;
@property (retain) NSNumber * TrainingCoursePeriodId;
@property (retain) org_tns2_ArrayOfTrainingCourseSchedule * TrainingCourseSchedules;
@property (retain) org_tns2_TrainingCourseType * TrainingCourseType;
@property (retain) NSNumber * TrainingCourseTypeId;
@property (retain) org_tns2_ArrayOfTrainingCourseUnit * TrainingCourseUnits;
@property (retain) org_tns2_ArrayOfTrainingPlanDegree * TrainingPlanDegrees;
@property (retain) org_tns2_TrainingPlanRequest * TrainingPlanRequest;
@property (retain) NSNumber * TrainingPlanRequestId;
@property (retain) org_tns2_ArrayOfTrainingProficencyExpected * TrainingProficencyExpecteds;
@property (retain) org_tns2_ArrayOfTrainingProficencyRequire * TrainingProficencyRequires;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfTrainingCourseEmployee : NSObject {
	
/* elements */
	NSMutableArray *TrainingCourseEmployee;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfTrainingCourseEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingCourseEmployee:(org_tns2_TrainingCourseEmployee *)toAdd;
@property (readonly) NSMutableArray * TrainingCourseEmployee;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_TrainingCourseSchedule : NSObject {
	
/* elements */
	org_tns2_CLogCourseSchedule * CLogCourseSchedule;
	NSNumber * ClogCourseScheduleId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	org_tns2_TrainingCourse * TrainingCourse;
	org_tns2_ArrayOfTrainingCourseEmployee * TrainingCourseEmployees;
	NSNumber * TrainingCourseId;
	NSNumber * TrainingCourseScheduleId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_TrainingCourseSchedule *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_CLogCourseSchedule * CLogCourseSchedule;
@property (retain) NSNumber * ClogCourseScheduleId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_TrainingCourse * TrainingCourse;
@property (retain) org_tns2_ArrayOfTrainingCourseEmployee * TrainingCourseEmployees;
@property (retain) NSNumber * TrainingCourseId;
@property (retain) NSNumber * TrainingCourseScheduleId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_TrainingCourseEmployee : NSObject {
	
/* elements */
	NSNumber * Absence;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsGraduated;
	NSNumber * Mark;
	NSDate * ModifiedDate;
	NSNumber * TrainingCourseEmployeeId;
	org_tns2_TrainingCourseSchedule * TrainingCourseSchedule;
	NSNumber * TrainingCourseScheduleId;
	org_tns2_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_TrainingCourseEmployee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Absence;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsGraduated;
@property (retain) NSNumber * Mark;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * TrainingCourseEmployeeId;
@property (retain) org_tns2_TrainingCourseSchedule * TrainingCourseSchedule;
@property (retain) NSNumber * TrainingCourseScheduleId;
@property (retain) org_tns2_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_TrainingEmpProficency : NSObject {
	
/* elements */
	NSNumber * CLogEmpRatingId;
	org_tns2_CLogRating * CLogRating;
	org_tns2_CLogRating * CLogRating1;
	NSNumber * ClogEmpManagerId;
	USBoolean * IsDeleted;
	NSNumber * ManagerId;
	org_tns2_OrgSkill * OrgSkill;
	NSNumber * ProficencyId;
	NSNumber * ProficencyType;
	org_tns2_TrainingCourseEmployee * TrainingCourseEmployee;
	NSNumber * TrainingCourseEmployeeId;
	NSNumber * TrainingEmpProficencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_TrainingEmpProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogEmpRatingId;
@property (retain) org_tns2_CLogRating * CLogRating;
@property (retain) org_tns2_CLogRating * CLogRating1;
@property (retain) NSNumber * ClogEmpManagerId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ManagerId;
@property (retain) org_tns2_OrgSkill * OrgSkill;
@property (retain) NSNumber * ProficencyId;
@property (retain) NSNumber * ProficencyType;
@property (retain) org_tns2_TrainingCourseEmployee * TrainingCourseEmployee;
@property (retain) NSNumber * TrainingCourseEmployeeId;
@property (retain) NSNumber * TrainingEmpProficencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfTrainingEmpProficency : NSObject {
	
/* elements */
	NSMutableArray *TrainingEmpProficency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfTrainingEmpProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingEmpProficency:(org_tns2_TrainingEmpProficency *)toAdd;
@property (readonly) NSMutableArray * TrainingEmpProficency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgSkill : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * Description;
	org_tns2_ArrayOfEmpProfileSkill * EmpProfileSkills;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * OrgSkillCode;
	NSNumber * OrgSkillId;
	org_tns2_OrgSkillType * OrgSkillType;
	NSNumber * OrgSkillTypeId;
	NSNumber * Priority;
	NSString * PriorityName;
	org_tns2_ArrayOfRecCandidateSkill * RecCandidateSkills;
	org_tns2_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) org_tns2_ArrayOfEmpProfileSkill * EmpProfileSkills;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * OrgSkillCode;
@property (retain) NSNumber * OrgSkillId;
@property (retain) org_tns2_OrgSkillType * OrgSkillType;
@property (retain) NSNumber * OrgSkillTypeId;
@property (retain) NSNumber * Priority;
@property (retain) NSString * PriorityName;
@property (retain) org_tns2_ArrayOfRecCandidateSkill * RecCandidateSkills;
@property (retain) org_tns2_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecCandidateSkill : NSObject {
	
/* elements */
	org_tns2_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * DocumentRef;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	org_tns2_OrgSkill * OrgSkill;
	NSNumber * OrgSkillId;
	org_tns2_RecCandidate * RecCandidate;
	NSNumber * RecCandidateId;
	NSNumber * RecCandidateSkillId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecCandidateSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DocumentRef;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_OrgSkill * OrgSkill;
@property (retain) NSNumber * OrgSkillId;
@property (retain) org_tns2_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateId;
@property (retain) NSNumber * RecCandidateSkillId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecCandidateSkill : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecCandidateSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateSkill:(org_tns2_RecCandidateSkill *)toAdd;
@property (readonly) NSMutableArray * RecCandidateSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecCandidate : NSObject {
	
/* elements */
	NSMutableArray *RecCandidate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecCandidate *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidate:(org_tns2_RecCandidate *)toAdd;
@property (readonly) NSMutableArray * RecCandidate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecCandidateStatu : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * RecCandidateStatusId;
	org_tns2_ArrayOfRecCandidate * RecCandidates;
	NSString * StatusName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecCandidateStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecCandidateStatusId;
@property (retain) org_tns2_ArrayOfRecCandidate * RecCandidates;
@property (retain) NSString * StatusName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecCandidateSupplier : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateSupplier;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecCandidateSupplier *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateSupplier:(org_tns2_RecCandidateSupplier *)toAdd;
@property (readonly) NSMutableArray * RecCandidateSupplier;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecCandidateTypeSupplier : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * RecCadidateTypeSupplierId;
	org_tns2_ArrayOfRecCandidateSupplier * RecCandidateSuppliers;
	NSString * TypeSupplierName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecCandidateTypeSupplier *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecCadidateTypeSupplierId;
@property (retain) org_tns2_ArrayOfRecCandidateSupplier * RecCandidateSuppliers;
@property (retain) NSString * TypeSupplierName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecCandidateSupplier : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	NSNumber * RecCadidateSupplierId;
	NSNumber * RecCadidateTypeSupplierId;
	org_tns2_RecCandidateTypeSupplier * RecCandidateTypeSupplier;
	org_tns2_ArrayOfRecCandidate * RecCandidates;
	NSString * SupplierAddress;
	NSString * SupplierEmail;
	NSString * SupplierName;
	NSString * SupplierPhone;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecCandidateSupplier *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * RecCadidateSupplierId;
@property (retain) NSNumber * RecCadidateTypeSupplierId;
@property (retain) org_tns2_RecCandidateTypeSupplier * RecCandidateTypeSupplier;
@property (retain) org_tns2_ArrayOfRecCandidate * RecCandidates;
@property (retain) NSString * SupplierAddress;
@property (retain) NSString * SupplierEmail;
@property (retain) NSString * SupplierName;
@property (retain) NSString * SupplierPhone;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecCandidate : NSObject {
	
/* elements */
	NSString * Address1;
	NSNumber * Address1Id;
	NSString * Address1Phone;
	NSString * Address2;
	NSNumber * Address2Id;
	NSString * Address2Phone;
	NSDate * ApplyDate;
	NSDate * BirthDay;
	NSString * BirthPlace;
	NSNumber * BirthPlaceId;
	NSString * CVFileType;
	NSString * CVUrl;
	USBoolean * CanBusinessTrip;
	USBoolean * CanOT;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * CulturalLevelId;
	NSString * CurrentSalary;
	NSNumber * EducationLevelId;
	NSString * Email;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * Ethnicity;
	NSNumber * EthnicityId;
	NSString * ExpectedSalary;
	NSString * FirstName;
	NSString * Forte;
	NSString * FullName;
	USBoolean * Gender;
	NSString * HomeTown;
	NSNumber * HomeTownId;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	NSString * ImageUrl;
	USBoolean * IsDeleted;
	USBoolean * IsManager;
	USBoolean * IsOnlyYearOfBirthday;
	NSString * LastName;
	NSString * MainLanguage;
	NSNumber * MainLanguageId;
	NSNumber * MajorId;
	NSNumber * MaritalStatusId;
	NSString * Mobile;
	NSDate * ModifiedDate;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSNumber * NumEmpManaged;
	NSString * OfficePhone;
	NSNumber * RecCadidateSupplierId;
	org_tns2_ArrayOfRecCandidateApplication * RecCandidateApplications;
	NSString * RecCandidateCode;
	org_tns2_ArrayOfRecCandidateCompetencyRating * RecCandidateCompetencyRatings;
	org_tns2_ArrayOfRecCandidateDegree * RecCandidateDegrees;
	org_tns2_ArrayOfRecCandidateExperience * RecCandidateExperiences;
	org_tns2_ArrayOfRecCandidateFamilyRelationship * RecCandidateFamilyRelationships;
	org_tns2_ArrayOfRecCandidateForeignLanguage * RecCandidateForeignLanguages;
	NSNumber * RecCandidateId;
	org_tns2_ArrayOfRecCandidateQualification * RecCandidateQualifications;
	org_tns2_ArrayOfRecCandidateSkill * RecCandidateSkills;
	org_tns2_RecCandidateStatu * RecCandidateStatu;
	NSNumber * RecCandidateStatusId;
	org_tns2_RecCandidateSupplier * RecCandidateSupplier;
	org_tns2_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
	NSString * Religion;
	NSNumber * ReligionId;
	NSString * Weaknesses;
	NSNumber * YearsExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecCandidate *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address1;
@property (retain) NSNumber * Address1Id;
@property (retain) NSString * Address1Phone;
@property (retain) NSString * Address2;
@property (retain) NSNumber * Address2Id;
@property (retain) NSString * Address2Phone;
@property (retain) NSDate * ApplyDate;
@property (retain) NSDate * BirthDay;
@property (retain) NSString * BirthPlace;
@property (retain) NSNumber * BirthPlaceId;
@property (retain) NSString * CVFileType;
@property (retain) NSString * CVUrl;
@property (retain) USBoolean * CanBusinessTrip;
@property (retain) USBoolean * CanOT;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * CulturalLevelId;
@property (retain) NSString * CurrentSalary;
@property (retain) NSNumber * EducationLevelId;
@property (retain) NSString * Email;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * Ethnicity;
@property (retain) NSNumber * EthnicityId;
@property (retain) NSString * ExpectedSalary;
@property (retain) NSString * FirstName;
@property (retain) NSString * Forte;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) NSString * HomeTown;
@property (retain) NSNumber * HomeTownId;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsManager;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) NSString * LastName;
@property (retain) NSString * MainLanguage;
@property (retain) NSNumber * MainLanguageId;
@property (retain) NSNumber * MajorId;
@property (retain) NSNumber * MaritalStatusId;
@property (retain) NSString * Mobile;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSNumber * NumEmpManaged;
@property (retain) NSString * OfficePhone;
@property (retain) NSNumber * RecCadidateSupplierId;
@property (retain) org_tns2_ArrayOfRecCandidateApplication * RecCandidateApplications;
@property (retain) NSString * RecCandidateCode;
@property (retain) org_tns2_ArrayOfRecCandidateCompetencyRating * RecCandidateCompetencyRatings;
@property (retain) org_tns2_ArrayOfRecCandidateDegree * RecCandidateDegrees;
@property (retain) org_tns2_ArrayOfRecCandidateExperience * RecCandidateExperiences;
@property (retain) org_tns2_ArrayOfRecCandidateFamilyRelationship * RecCandidateFamilyRelationships;
@property (retain) org_tns2_ArrayOfRecCandidateForeignLanguage * RecCandidateForeignLanguages;
@property (retain) NSNumber * RecCandidateId;
@property (retain) org_tns2_ArrayOfRecCandidateQualification * RecCandidateQualifications;
@property (retain) org_tns2_ArrayOfRecCandidateSkill * RecCandidateSkills;
@property (retain) org_tns2_RecCandidateStatu * RecCandidateStatu;
@property (retain) NSNumber * RecCandidateStatusId;
@property (retain) org_tns2_RecCandidateSupplier * RecCandidateSupplier;
@property (retain) org_tns2_ArrayOfRecProcessPhaseResult * RecProcessPhaseResults;
@property (retain) NSString * Religion;
@property (retain) NSNumber * ReligionId;
@property (retain) NSString * Weaknesses;
@property (retain) NSNumber * YearsExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecCandidateProficencyLevelRating : NSObject {
	
/* elements */
	NSMutableArray *RecCandidateProficencyLevelRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecCandidateProficencyLevelRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCandidateProficencyLevelRating:(org_tns2_RecCandidateProficencyLevelRating *)toAdd;
@property (readonly) NSMutableArray * RecCandidateProficencyLevelRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecCandidateCompetencyRating : NSObject {
	
/* elements */
	org_tns2_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * Mark;
	NSDate * ModifiedDate;
	NSString * Note;
	org_tns2_RecCandidate * RecCandidate;
	NSNumber * RecCandidateCompetencyRatingId;
	NSNumber * RecCandidateId;
	org_tns2_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecCandidateCompetencyRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Mark;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) org_tns2_RecCandidate * RecCandidate;
@property (retain) NSNumber * RecCandidateCompetencyRatingId;
@property (retain) NSNumber * RecCandidateId;
@property (retain) org_tns2_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecCandidateProficencyLevelRating : NSObject {
	
/* elements */
	org_tns2_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * Mark;
	NSDate * ModifiedDate;
	NSString * Note;
	org_tns2_OrgProficencyLevel * OrgProficencyLevel;
	NSNumber * OrgProficencyLevelId;
	org_tns2_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
	org_tns2_RecCandidateCompetencyRating * RecCandidateCompetencyRating;
	NSNumber * RecCandidateCompetencyRatingId;
	NSNumber * RecCandidateProficencyLevelRatingId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecCandidateProficencyLevelRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Mark;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) org_tns2_OrgProficencyLevel * OrgProficencyLevel;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) org_tns2_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
@property (retain) org_tns2_RecCandidateCompetencyRating * RecCandidateCompetencyRating;
@property (retain) NSNumber * RecCandidateCompetencyRatingId;
@property (retain) NSNumber * RecCandidateProficencyLevelRatingId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_RecCanProficencyLevelDetailRating : NSObject {
	
/* elements */
	org_tns2_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * Mark;
	NSDate * ModifiedDate;
	NSString * Note;
	org_tns2_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
	NSNumber * OrgProficencyLevelDetailId;
	org_tns2_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
	NSNumber * RecCanProficencyLevelDetailRatingId;
	org_tns2_RecCandidateProficencyLevelRating * RecCandidateProficencyLevelRating;
	NSNumber * RecCandidateProficencyLevelRatingId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_RecCanProficencyLevelDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Mark;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) org_tns2_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) org_tns2_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
@property (retain) NSNumber * RecCanProficencyLevelDetailRatingId;
@property (retain) org_tns2_RecCandidateProficencyLevelRating * RecCandidateProficencyLevelRating;
@property (retain) NSNumber * RecCandidateProficencyLevelRatingId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfRecCanProficencyLevelDetailRating : NSObject {
	
/* elements */
	NSMutableArray *RecCanProficencyLevelDetailRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfRecCanProficencyLevelDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addRecCanProficencyLevelDetailRating:(org_tns2_RecCanProficencyLevelDetailRating *)toAdd;
@property (readonly) NSMutableArray * RecCanProficencyLevelDetailRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgProficencyLevelDetail : NSObject {
	
/* elements */
	org_tns2_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	org_tns2_ArrayOfOrgProficencyDetail * OrgProficencyDetails;
	org_tns2_OrgProficencyLevel * OrgProficencyLevel;
	NSNumber * OrgProficencyLevelDetailId;
	NSNumber * OrgProficencyLevelId;
	org_tns2_OrgProficencyType * OrgProficencyType;
	NSNumber * OrgProficencyTypeId;
	org_tns2_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
	NSString * Statement;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgProficencyLevelDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_ArrayOfOrgProficencyDetail * OrgProficencyDetails;
@property (retain) org_tns2_OrgProficencyLevel * OrgProficencyLevel;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) org_tns2_OrgProficencyType * OrgProficencyType;
@property (retain) NSNumber * OrgProficencyTypeId;
@property (retain) org_tns2_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
@property (retain) NSString * Statement;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgProficencyDetail : NSObject {
	
/* elements */
	org_tns2_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgCompetencyTypeId;
	NSNumber * OrgProficencyDetailId;
	org_tns2_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
	NSNumber * OrgProficencyLevelDetailId;
	org_tns2_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgProficencyDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgCompetencyTypeId;
@property (retain) NSNumber * OrgProficencyDetailId;
@property (retain) org_tns2_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) org_tns2_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProficencyDetailRating : NSObject {
	
/* elements */
	NSDate * AppraisalDate;
	org_tns2_CLogRating * CLogRating;
	NSDate * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpProficencyDetailRatingId;
	org_tns2_EmpProficencyLevelDetailRating * EmpProficencyLevelDetailRating;
	NSNumber * EmpProficencyLevelDetailRatingId;
	NSString * ManagerNote;
	NSNumber * ManagerRating;
	org_tns2_OrgProficencyDetail * OrgProficencyDetail;
	NSNumber * OrgProficencyDetailId;
	NSNumber * PeriodicallyAssessment;
	NSString * SelfNote;
	NSNumber * SelfRating;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProficencyDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AppraisalDate;
@property (retain) org_tns2_CLogRating * CLogRating;
@property (retain) NSDate * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProficencyDetailRatingId;
@property (retain) org_tns2_EmpProficencyLevelDetailRating * EmpProficencyLevelDetailRating;
@property (retain) NSNumber * EmpProficencyLevelDetailRatingId;
@property (retain) NSString * ManagerNote;
@property (retain) NSNumber * ManagerRating;
@property (retain) org_tns2_OrgProficencyDetail * OrgProficencyDetail;
@property (retain) NSNumber * OrgProficencyDetailId;
@property (retain) NSNumber * PeriodicallyAssessment;
@property (retain) NSString * SelfNote;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProficencyDetailRating : NSObject {
	
/* elements */
	NSMutableArray *EmpProficencyDetailRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProficencyDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProficencyDetailRating:(org_tns2_EmpProficencyDetailRating *)toAdd;
@property (readonly) NSMutableArray * EmpProficencyDetailRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProficencyLevelDetailRating : NSObject {
	
/* elements */
	NSDate * AppraisalDate;
	org_tns2_CLogRating * CLogRating;
	NSDate * CompanyId;
	NSDate * CreatedDate;
	org_tns2_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
	NSNumber * EmpProficencyLevelDetailRatingId;
	org_tns2_EmpProficencyLevelRating * EmpProficencyLevelRating;
	NSNumber * EmpProficencyLevelRatingId;
	NSString * ManagerNote;
	NSNumber * ManagerRating;
	org_tns2_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
	NSNumber * OrgProficencyLevelDetailId;
	NSNumber * PeriodicallyAssessment;
	NSNumber * Score;
	NSString * SelfNote;
	NSNumber * SelfRatingId;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProficencyLevelDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AppraisalDate;
@property (retain) org_tns2_CLogRating * CLogRating;
@property (retain) NSDate * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
@property (retain) NSNumber * EmpProficencyLevelDetailRatingId;
@property (retain) org_tns2_EmpProficencyLevelRating * EmpProficencyLevelRating;
@property (retain) NSNumber * EmpProficencyLevelRatingId;
@property (retain) NSString * ManagerNote;
@property (retain) NSNumber * ManagerRating;
@property (retain) org_tns2_OrgProficencyLevelDetail * OrgProficencyLevelDetail;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) NSNumber * PeriodicallyAssessment;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfNote;
@property (retain) NSNumber * SelfRatingId;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProficencyLevelDetailRating : NSObject {
	
/* elements */
	NSMutableArray *EmpProficencyLevelDetailRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProficencyLevelDetailRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProficencyLevelDetailRating:(org_tns2_EmpProficencyLevelDetailRating *)toAdd;
@property (readonly) NSMutableArray * EmpProficencyLevelDetailRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProficencyLevelRating : NSObject {
	
/* elements */
	NSDate * AppraisalDate;
	org_tns2_CLogRating * CLogRating;
	NSDate * CreatedDate;
	org_tns2_EmpCompetencyRating * EmpCompetencyRating;
	NSNumber * EmpCompetencyRatingId;
	org_tns2_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
	NSNumber * EmpProficencyLevelRatingId;
	NSString * ManagerNote;
	NSNumber * ManagerRating;
	org_tns2_OrgProficencyLevel * OrgProficencyLevel;
	NSNumber * OrgProficencyLevelId;
	NSNumber * PeriodicallyAssessment;
	NSNumber * Score;
	NSString * SelfNote;
	NSNumber * SelfRating;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProficencyLevelRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AppraisalDate;
@property (retain) org_tns2_CLogRating * CLogRating;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_EmpCompetencyRating * EmpCompetencyRating;
@property (retain) NSNumber * EmpCompetencyRatingId;
@property (retain) org_tns2_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
@property (retain) NSNumber * EmpProficencyLevelRatingId;
@property (retain) NSString * ManagerNote;
@property (retain) NSNumber * ManagerRating;
@property (retain) org_tns2_OrgProficencyLevel * OrgProficencyLevel;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) NSNumber * PeriodicallyAssessment;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfNote;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProficencyLevelRating : NSObject {
	
/* elements */
	NSMutableArray *EmpProficencyLevelRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProficencyLevelRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProficencyLevelRating:(org_tns2_EmpProficencyLevelRating *)toAdd;
@property (readonly) NSMutableArray * EmpProficencyLevelRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfLMSCourse : NSObject {
	
/* elements */
	NSMutableArray *LMSCourse;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfLMSCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourse:(org_tns2_LMSCourse *)toAdd;
@property (readonly) NSMutableArray * LMSCourse;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CLogCourseStatu : NSObject {
	
/* elements */
	NSNumber * CourseStatusId;
	USBoolean * IsDeleted;
	org_tns2_ArrayOfLMSCourse * LMSCourses;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CLogCourseStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CourseStatusId;
@property (retain) USBoolean * IsDeleted;
@property (retain) org_tns2_ArrayOfLMSCourse * LMSCourses;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_GPEmployeeScoreCardAppraisal : NSObject {
	
/* elements */
	NSNumber * AppraisalTime;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_GPEmployeeScoreCard * GPEmployeeScoreCard;
	NSNumber * GPEmployeeScoreCardAppraisalId;
	NSNumber * GPEmployeeScoreCardId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * Score;
	NSString * SelfComment;
	NSNumber * SelfRating;
	NSNumber * SelfScore;
	NSNumber * Status;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_GPEmployeeScoreCardAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AppraisalTime;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_GPEmployeeScoreCard * GPEmployeeScoreCard;
@property (retain) NSNumber * GPEmployeeScoreCardAppraisalId;
@property (retain) NSNumber * GPEmployeeScoreCardId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * SelfScore;
@property (retain) NSNumber * Status;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfGPEmployeeScoreCardAppraisal : NSObject {
	
/* elements */
	NSMutableArray *GPEmployeeScoreCardAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfGPEmployeeScoreCardAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPEmployeeScoreCardAppraisal:(org_tns2_GPEmployeeScoreCardAppraisal *)toAdd;
@property (readonly) NSMutableArray * GPEmployeeScoreCardAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_GPEmployeeScoreCardProgress : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_GPEmployeeScoreCard * GPEmployeeScoreCard;
	NSNumber * GPEmployeeScoreCardId;
	NSNumber * GPEmployeeScoreCardProgressId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * NewValue;
	NSNumber * OldValue;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_GPEmployeeScoreCardProgress *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_GPEmployeeScoreCard * GPEmployeeScoreCard;
@property (retain) NSNumber * GPEmployeeScoreCardId;
@property (retain) NSNumber * GPEmployeeScoreCardProgressId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NewValue;
@property (retain) NSNumber * OldValue;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfGPEmployeeScoreCardProgress : NSObject {
	
/* elements */
	NSMutableArray *GPEmployeeScoreCardProgress;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfGPEmployeeScoreCardProgress *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPEmployeeScoreCardProgress:(org_tns2_GPEmployeeScoreCardProgress *)toAdd;
@property (readonly) NSMutableArray * GPEmployeeScoreCardProgress;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_GPAdditionAppraisal : NSObject {
	
/* elements */
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	org_tns2_Employee * Employee1;
	NSNumber * EmployeeId;
	NSNumber * GPAdditionAppraisalId;
	org_tns2_GPExecutionPlan * GPExecutionPlan;
	NSNumber * GPExecutionPlanId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * SupervisorId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_GPAdditionAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) org_tns2_Employee * Employee1;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * GPAdditionAppraisalId;
@property (retain) org_tns2_GPExecutionPlan * GPExecutionPlan;
@property (retain) NSNumber * GPExecutionPlanId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * SupervisorId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfGPAdditionAppraisal : NSObject {
	
/* elements */
	NSMutableArray *GPAdditionAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfGPAdditionAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPAdditionAppraisal:(org_tns2_GPAdditionAppraisal *)toAdd;
@property (readonly) NSMutableArray * GPAdditionAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_GPEmployeeWholePlanAppraisal : NSObject {
	
/* elements */
	NSNumber * AppraisalTime;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * GPEmployeeWholePlanAppraisalId;
	org_tns2_GPExecutionPlan * GPExecutionPlan;
	NSNumber * GPExecutionPlanId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * Result;
	NSNumber * Score;
	NSString * SelfComment;
	NSNumber * SelfRating;
	NSNumber * SelfScore;
	NSNumber * Status;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_GPEmployeeWholePlanAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AppraisalTime;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * GPEmployeeWholePlanAppraisalId;
@property (retain) org_tns2_GPExecutionPlan * GPExecutionPlan;
@property (retain) NSNumber * GPExecutionPlanId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * Result;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * SelfScore;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfGPEmployeeWholePlanAppraisal : NSObject {
	
/* elements */
	NSMutableArray *GPEmployeeWholePlanAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfGPEmployeeWholePlanAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPEmployeeWholePlanAppraisal:(org_tns2_GPEmployeeWholePlanAppraisal *)toAdd;
@property (readonly) NSMutableArray * GPEmployeeWholePlanAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_GPPerformanceYearEndResult : NSObject {
	
/* elements */
	NSString * ActivityName;
	NSNumber * Actual;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	org_tns2_GPExecutionPlan * GPExecutionPlan;
	NSNumber * GPPerformancePlanId;
	NSNumber * GPPerformanceYearEndResultId;
	USBoolean * HalfYearResult;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Progress;
	NSNumber * Rate;
	NSString * Result;
	NSNumber * Target;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_GPPerformanceYearEndResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ActivityName;
@property (retain) NSNumber * Actual;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) org_tns2_GPExecutionPlan * GPExecutionPlan;
@property (retain) NSNumber * GPPerformancePlanId;
@property (retain) NSNumber * GPPerformanceYearEndResultId;
@property (retain) USBoolean * HalfYearResult;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Progress;
@property (retain) NSNumber * Rate;
@property (retain) NSString * Result;
@property (retain) NSNumber * Target;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfGPPerformanceYearEndResult : NSObject {
	
/* elements */
	NSMutableArray *GPPerformanceYearEndResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfGPPerformanceYearEndResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPPerformanceYearEndResult:(org_tns2_GPPerformanceYearEndResult *)toAdd;
@property (readonly) NSMutableArray * GPPerformanceYearEndResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_GPExecutionPlan : NSObject {
	
/* elements */
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmployeeId;
	org_tns2_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals;
	org_tns2_ArrayOfGPEmployeeWholePlanAppraisal * GPEmployeeWholePlanAppraisals;
	org_tns2_ArrayOfGPExecutionPlanDetail * GPExecutionPlanDetails;
	NSNumber * GPExecutionPlanId;
	org_tns2_ArrayOfGPPerformanceYearEndResult * GPPerformanceYearEndResults;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * Period;
	NSNumber * Status;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_GPExecutionPlan *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmployeeId;
@property (retain) org_tns2_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals;
@property (retain) org_tns2_ArrayOfGPEmployeeWholePlanAppraisal * GPEmployeeWholePlanAppraisals;
@property (retain) org_tns2_ArrayOfGPExecutionPlanDetail * GPExecutionPlanDetails;
@property (retain) NSNumber * GPExecutionPlanId;
@property (retain) org_tns2_ArrayOfGPPerformanceYearEndResult * GPPerformanceYearEndResults;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * Period;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_GPExecutionPlanActivityAppraisal : NSObject {
	
/* elements */
	NSNumber * AppraisalTime;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_GPExecutionPlanActivity * GPExecutionPlanActivity;
	NSNumber * GPExecutionPlanActivityAppraisalId;
	NSNumber * GPExecutionPlanActivityId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * Score;
	NSString * SelfComment;
	NSNumber * SelfRating;
	NSNumber * SelfScore;
	NSNumber * Status;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_GPExecutionPlanActivityAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AppraisalTime;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_GPExecutionPlanActivity * GPExecutionPlanActivity;
@property (retain) NSNumber * GPExecutionPlanActivityAppraisalId;
@property (retain) NSNumber * GPExecutionPlanActivityId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * SelfScore;
@property (retain) NSNumber * Status;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfGPExecutionPlanActivityAppraisal : NSObject {
	
/* elements */
	NSMutableArray *GPExecutionPlanActivityAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfGPExecutionPlanActivityAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPExecutionPlanActivityAppraisal:(org_tns2_GPExecutionPlanActivityAppraisal *)toAdd;
@property (readonly) NSMutableArray * GPExecutionPlanActivityAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_GPPeriodicReport : NSObject {
	
/* elements */
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_GPExecutionPlanActivity * GPExecutionPlanActivity;
	NSNumber * GPExecutionPlanActivityId;
	NSNumber * GPPeriodicReportId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSDate * ReportDateTime;
	NSNumber * ReportTimes;
	NSNumber * Status;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_GPPeriodicReport *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_GPExecutionPlanActivity * GPExecutionPlanActivity;
@property (retain) NSNumber * GPExecutionPlanActivityId;
@property (retain) NSNumber * GPPeriodicReportId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * ReportDateTime;
@property (retain) NSNumber * ReportTimes;
@property (retain) NSNumber * Status;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfGPPeriodicReport : NSObject {
	
/* elements */
	NSMutableArray *GPPeriodicReport;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfGPPeriodicReport *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPPeriodicReport:(org_tns2_GPPeriodicReport *)toAdd;
@property (readonly) NSMutableArray * GPPeriodicReport;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_GPExecutionPlanActivity : NSObject {
	
/* elements */
	NSString * AchievementLevel;
	NSString * ActivityGroup;
	NSString * ActivityName;
	NSNumber * Budget;
	NSString * BudgetUnit;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSDate * EndDate;
	org_tns2_ArrayOfGPExecutionPlanActivityAppraisal * GPExecutionPlanActivityAppraisals;
	NSNumber * GPExecutionPlanActivityId;
	org_tns2_GPExecutionPlanDetail * GPExecutionPlanDetail;
	NSNumber * GPExecutionPlanDetailId;
	org_tns2_ArrayOfGPPeriodicReport * GPPeriodicReports;
	USBoolean * IsDeleted;
	NSNumber * Measure;
	NSString * MeasureUnit;
	NSDate * ModifiedDate;
	NSDate * StartDate;
	NSString * Status;
	NSNumber * Target;
	NSString * TargetUnit;
	NSNumber * Variance;
	NSString * VarianceUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_GPExecutionPlanActivity *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AchievementLevel;
@property (retain) NSString * ActivityGroup;
@property (retain) NSString * ActivityName;
@property (retain) NSNumber * Budget;
@property (retain) NSString * BudgetUnit;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) org_tns2_ArrayOfGPExecutionPlanActivityAppraisal * GPExecutionPlanActivityAppraisals;
@property (retain) NSNumber * GPExecutionPlanActivityId;
@property (retain) org_tns2_GPExecutionPlanDetail * GPExecutionPlanDetail;
@property (retain) NSNumber * GPExecutionPlanDetailId;
@property (retain) org_tns2_ArrayOfGPPeriodicReport * GPPeriodicReports;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Measure;
@property (retain) NSString * MeasureUnit;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * StartDate;
@property (retain) NSString * Status;
@property (retain) NSNumber * Target;
@property (retain) NSString * TargetUnit;
@property (retain) NSNumber * Variance;
@property (retain) NSString * VarianceUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfGPExecutionPlanActivity : NSObject {
	
/* elements */
	NSMutableArray *GPExecutionPlanActivity;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfGPExecutionPlanActivity *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPExecutionPlanActivity:(org_tns2_GPExecutionPlanActivity *)toAdd;
@property (readonly) NSMutableArray * GPExecutionPlanActivity;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_GPExecutionPlanDetail : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_GPExecutionPlan * GPExecutionPlan;
	org_tns2_ArrayOfGPExecutionPlanActivity * GPExecutionPlanActivities;
	NSNumber * GPExecutionPlanDetailId;
	NSNumber * GPExecutionPlanId;
	org_tns2_GPIndividualYearlyObjective * GPIndividualYearlyObjective;
	NSNumber * GPIndividualYearlyObjectiveId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_GPExecutionPlanDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_GPExecutionPlan * GPExecutionPlan;
@property (retain) org_tns2_ArrayOfGPExecutionPlanActivity * GPExecutionPlanActivities;
@property (retain) NSNumber * GPExecutionPlanDetailId;
@property (retain) NSNumber * GPExecutionPlanId;
@property (retain) org_tns2_GPIndividualYearlyObjective * GPIndividualYearlyObjective;
@property (retain) NSNumber * GPIndividualYearlyObjectiveId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfGPExecutionPlanDetail : NSObject {
	
/* elements */
	NSMutableArray *GPExecutionPlanDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfGPExecutionPlanDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPExecutionPlanDetail:(org_tns2_GPExecutionPlanDetail *)toAdd;
@property (readonly) NSMutableArray * GPExecutionPlanDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_GPIndividualYearlyObjectiveAppraisal : NSObject {
	
/* elements */
	NSNumber * AppraisalTime;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_GPIndividualYearlyObjective * GPIndividualYearlyObjective;
	NSNumber * GPIndividualYearlyObjectiveAppraisalId;
	NSNumber * GPIndividualYearlyObjectiveId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * Score;
	NSString * SelfComment;
	NSNumber * SelfRating;
	NSNumber * SelfScore;
	NSNumber * Status;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_GPIndividualYearlyObjectiveAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AppraisalTime;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_GPIndividualYearlyObjective * GPIndividualYearlyObjective;
@property (retain) NSNumber * GPIndividualYearlyObjectiveAppraisalId;
@property (retain) NSNumber * GPIndividualYearlyObjectiveId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * SelfScore;
@property (retain) NSNumber * Status;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfGPIndividualYearlyObjectiveAppraisal : NSObject {
	
/* elements */
	NSMutableArray *GPIndividualYearlyObjectiveAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfGPIndividualYearlyObjectiveAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPIndividualYearlyObjectiveAppraisal:(org_tns2_GPIndividualYearlyObjectiveAppraisal *)toAdd;
@property (readonly) NSMutableArray * GPIndividualYearlyObjectiveAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_GPPerspectiveValueDetail : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_GPPerspectiveValue * GPPerspectiveValue;
	org_tns2_GPPerspectiveValue * GPPerspectiveValue1;
	NSNumber * GPPerspectiveValueDetailId;
	NSNumber * GPStrategyMapObjectId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * PerspectiveValueDestinationId;
	NSNumber * PerspectiveValueSourceId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_GPPerspectiveValueDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_GPPerspectiveValue * GPPerspectiveValue;
@property (retain) org_tns2_GPPerspectiveValue * GPPerspectiveValue1;
@property (retain) NSNumber * GPPerspectiveValueDetailId;
@property (retain) NSNumber * GPStrategyMapObjectId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * PerspectiveValueDestinationId;
@property (retain) NSNumber * PerspectiveValueSourceId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfGPPerspectiveValueDetail : NSObject {
	
/* elements */
	NSMutableArray *GPPerspectiveValueDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfGPPerspectiveValueDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPPerspectiveValueDetail:(org_tns2_GPPerspectiveValueDetail *)toAdd;
@property (readonly) NSMutableArray * GPPerspectiveValueDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_GPPerspectiveValue : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	org_tns2_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	org_tns2_ArrayOfGPPerspectiveValueDetail * GPPerspectiveValueDetails;
	org_tns2_ArrayOfGPPerspectiveValueDetail * GPPerspectiveValueDetails1;
	NSNumber * GPPerspectiveValueId;
	org_tns2_GPStrategyMapObject * GPStrategyMapObject;
	NSNumber * GPStrategyMapObjectId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * PositionX;
	NSNumber * PositionY;
	NSString * ValueType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_GPPerspectiveValue *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) org_tns2_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) org_tns2_ArrayOfGPPerspectiveValueDetail * GPPerspectiveValueDetails;
@property (retain) org_tns2_ArrayOfGPPerspectiveValueDetail * GPPerspectiveValueDetails1;
@property (retain) NSNumber * GPPerspectiveValueId;
@property (retain) org_tns2_GPStrategyMapObject * GPStrategyMapObject;
@property (retain) NSNumber * GPStrategyMapObjectId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * PositionX;
@property (retain) NSNumber * PositionY;
@property (retain) NSString * ValueType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfGPPerspectiveValue : NSObject {
	
/* elements */
	NSMutableArray *GPPerspectiveValue;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfGPPerspectiveValue *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPPerspectiveValue:(org_tns2_GPPerspectiveValue *)toAdd;
@property (readonly) NSMutableArray * GPPerspectiveValue;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfGPCompanyYearlyObjective : NSObject {
	
/* elements */
	NSMutableArray *GPCompanyYearlyObjective;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfGPCompanyYearlyObjective *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPCompanyYearlyObjective:(org_tns2_GPCompanyYearlyObjective *)toAdd;
@property (readonly) NSMutableArray * GPCompanyYearlyObjective;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfGPStrategyMapObject : NSObject {
	
/* elements */
	NSMutableArray *GPStrategyMapObject;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfGPStrategyMapObject *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPStrategyMapObject:(org_tns2_GPStrategyMapObject *)toAdd;
@property (readonly) NSMutableArray * GPStrategyMapObject;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_GPStrategy : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EndYear;
	org_tns2_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
	NSNumber * GPStrategyId;
	org_tns2_ArrayOfGPStrategyMapObject * GPStrategyMapObjects;
	USBoolean * IsActived;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * StartYear;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_GPStrategy *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EndYear;
@property (retain) org_tns2_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
@property (retain) NSNumber * GPStrategyId;
@property (retain) org_tns2_ArrayOfGPStrategyMapObject * GPStrategyMapObjects;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * StartYear;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_GPStrategyMapObject : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	org_tns2_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails;
	org_tns2_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
	org_tns2_ArrayOfGPPerspectiveValue * GPPerspectiveValues;
	org_tns2_GPStrategy * GPStrategy;
	NSNumber * GPStrategyId;
	NSNumber * GPStrategyMapObjectId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_GPStrategyMapObject *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) org_tns2_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails;
@property (retain) org_tns2_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
@property (retain) org_tns2_ArrayOfGPPerspectiveValue * GPPerspectiveValues;
@property (retain) org_tns2_GPStrategy * GPStrategy;
@property (retain) NSNumber * GPStrategyId;
@property (retain) NSNumber * GPStrategyMapObjectId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_GPCompanyStrategicGoalDetail : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_GPCompanyStrategicGoal * GPCompanyStrategicGoal;
	org_tns2_GPCompanyStrategicGoal * GPCompanyStrategicGoal1;
	NSNumber * GPCompanyStrategicGoalDestinationId;
	NSNumber * GPCompanyStrategicGoalDetailId;
	NSNumber * GPCompanyStrategicGoalSourceId;
	org_tns2_GPStrategyMapObject * GPStrategyMapObject;
	NSNumber * GPStrategyMapObjectId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_GPCompanyStrategicGoalDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_GPCompanyStrategicGoal * GPCompanyStrategicGoal;
@property (retain) org_tns2_GPCompanyStrategicGoal * GPCompanyStrategicGoal1;
@property (retain) NSNumber * GPCompanyStrategicGoalDestinationId;
@property (retain) NSNumber * GPCompanyStrategicGoalDetailId;
@property (retain) NSNumber * GPCompanyStrategicGoalSourceId;
@property (retain) org_tns2_GPStrategyMapObject * GPStrategyMapObject;
@property (retain) NSNumber * GPStrategyMapObjectId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfGPCompanyStrategicGoalDetail : NSObject {
	
/* elements */
	NSMutableArray *GPCompanyStrategicGoalDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfGPCompanyStrategicGoalDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPCompanyStrategicGoalDetail:(org_tns2_GPCompanyStrategicGoalDetail *)toAdd;
@property (readonly) NSMutableArray * GPCompanyStrategicGoalDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_GPCompanyStrategicGoal : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	org_tns2_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails;
	org_tns2_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails1;
	NSNumber * GPCompanyStrategicGoalId;
	org_tns2_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
	org_tns2_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	NSNumber * GPStrategyId;
	org_tns2_GPStrategyMapObject * GPStrategyMapObject;
	NSNumber * GPStrategyMapObjectId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OwnerId;
	NSString * PeriodStrategy;
	NSString * SmartAttainableGoal;
	NSString * SmartMeasureGoal;
	NSString * SmartRelevantGoal;
	NSString * SmartSpecificGoal;
	NSString * SmartTimeGoal;
	NSString * Statement;
	NSNumber * YearBegin;
	NSNumber * YearEnd;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_GPCompanyStrategicGoal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) org_tns2_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails;
@property (retain) org_tns2_ArrayOfGPCompanyStrategicGoalDetail * GPCompanyStrategicGoalDetails1;
@property (retain) NSNumber * GPCompanyStrategicGoalId;
@property (retain) org_tns2_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
@property (retain) org_tns2_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) NSNumber * GPStrategyId;
@property (retain) org_tns2_GPStrategyMapObject * GPStrategyMapObject;
@property (retain) NSNumber * GPStrategyMapObjectId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OwnerId;
@property (retain) NSString * PeriodStrategy;
@property (retain) NSString * SmartAttainableGoal;
@property (retain) NSString * SmartMeasureGoal;
@property (retain) NSString * SmartRelevantGoal;
@property (retain) NSString * SmartSpecificGoal;
@property (retain) NSString * SmartTimeGoal;
@property (retain) NSString * Statement;
@property (retain) NSNumber * YearBegin;
@property (retain) NSNumber * YearEnd;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfGPCompanyStrategicGoal : NSObject {
	
/* elements */
	NSMutableArray *GPCompanyStrategicGoal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfGPCompanyStrategicGoal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPCompanyStrategicGoal:(org_tns2_GPCompanyStrategicGoal *)toAdd;
@property (readonly) NSMutableArray * GPCompanyStrategicGoal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_GPObjectiveInitiative : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	org_tns2_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
	NSNumber * GPObjectiveInitiativeId;
	org_tns2_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	USBoolean * IsDeleted;
	org_tns2_ArrayOfLMSCourse * LMSCourses;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * OwnerId;
	NSNumber * Progress;
	NSString * Purpose;
	NSNumber * YearBegin;
	NSNumber * YearEnd;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_GPObjectiveInitiative *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) org_tns2_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
@property (retain) NSNumber * GPObjectiveInitiativeId;
@property (retain) org_tns2_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) USBoolean * IsDeleted;
@property (retain) org_tns2_ArrayOfLMSCourse * LMSCourses;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * OwnerId;
@property (retain) NSNumber * Progress;
@property (retain) NSString * Purpose;
@property (retain) NSNumber * YearBegin;
@property (retain) NSNumber * YearEnd;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfGPObjectiveInitiative : NSObject {
	
/* elements */
	NSMutableArray *GPObjectiveInitiative;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfGPObjectiveInitiative *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPObjectiveInitiative:(org_tns2_GPObjectiveInitiative *)toAdd;
@property (readonly) NSMutableArray * GPObjectiveInitiative;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_GPPerspectiveMeasure : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
	org_tns2_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
	org_tns2_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	NSNumber * GPPerspectiveMeasureId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_GPPerspectiveMeasure *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
@property (retain) org_tns2_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
@property (retain) org_tns2_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) NSNumber * GPPerspectiveMeasureId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfGPPerspectiveMeasure : NSObject {
	
/* elements */
	NSMutableArray *GPPerspectiveMeasure;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfGPPerspectiveMeasure *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPPerspectiveMeasure:(org_tns2_GPPerspectiveMeasure *)toAdd;
@property (readonly) NSMutableArray * GPPerspectiveMeasure;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_GPPerspective : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	org_tns2_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
	org_tns2_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
	org_tns2_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
	org_tns2_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
	org_tns2_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
	org_tns2_ArrayOfGPObjectiveInitiative * GPObjectiveInitiatives;
	NSNumber * GPPerspectiveId;
	org_tns2_ArrayOfGPPerspectiveMeasure * GPPerspectiveMeasures;
	org_tns2_ArrayOfGPPerspectiveValue * GPPerspectiveValues;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_GPPerspective *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) org_tns2_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
@property (retain) org_tns2_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
@property (retain) org_tns2_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
@property (retain) org_tns2_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
@property (retain) org_tns2_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
@property (retain) org_tns2_ArrayOfGPObjectiveInitiative * GPObjectiveInitiatives;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) org_tns2_ArrayOfGPPerspectiveMeasure * GPPerspectiveMeasures;
@property (retain) org_tns2_ArrayOfGPPerspectiveValue * GPPerspectiveValues;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_GPIndividualYearlyObjective : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	org_tns2_Employee * Employee1;
	NSNumber * EmployeeId;
	org_tns2_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
	NSNumber * GPCompanyYearlyObjectiveId;
	org_tns2_GPEmployeeScoreCard * GPEmployeeScoreCard;
	NSNumber * GPEmployeeScoreCardId;
	org_tns2_ArrayOfGPExecutionPlanDetail * GPExecutionPlanDetails;
	org_tns2_ArrayOfGPIndividualYearlyObjectiveAppraisal * GPIndividualYearlyObjectiveAppraisals;
	NSNumber * GPIndividualYearlyObjectiveId;
	org_tns2_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * ObjectiveType;
	NSNumber * Owner;
	NSString * SmartAttainableGoal;
	NSString * SmartMeasureGoal;
	NSString * SmartRelevantGoal;
	NSString * SmartSpecificGoal;
	NSString * SmartTimeGoal;
	NSString * Statement;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_GPIndividualYearlyObjective *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) org_tns2_Employee * Employee1;
@property (retain) NSNumber * EmployeeId;
@property (retain) org_tns2_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
@property (retain) NSNumber * GPCompanyYearlyObjectiveId;
@property (retain) org_tns2_GPEmployeeScoreCard * GPEmployeeScoreCard;
@property (retain) NSNumber * GPEmployeeScoreCardId;
@property (retain) org_tns2_ArrayOfGPExecutionPlanDetail * GPExecutionPlanDetails;
@property (retain) org_tns2_ArrayOfGPIndividualYearlyObjectiveAppraisal * GPIndividualYearlyObjectiveAppraisals;
@property (retain) NSNumber * GPIndividualYearlyObjectiveId;
@property (retain) org_tns2_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * ObjectiveType;
@property (retain) NSNumber * Owner;
@property (retain) NSString * SmartAttainableGoal;
@property (retain) NSString * SmartMeasureGoal;
@property (retain) NSString * SmartRelevantGoal;
@property (retain) NSString * SmartSpecificGoal;
@property (retain) NSString * SmartTimeGoal;
@property (retain) NSString * Statement;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfGPIndividualYearlyObjective : NSObject {
	
/* elements */
	NSMutableArray *GPIndividualYearlyObjective;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfGPIndividualYearlyObjective *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPIndividualYearlyObjective:(org_tns2_GPIndividualYearlyObjective *)toAdd;
@property (readonly) NSMutableArray * GPIndividualYearlyObjective;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_GPPerformanceExecutionReport : NSObject {
	
/* elements */
	NSString * ActivityName;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_GPEmployeeScoreCard * GPEmployeeScoreCard;
	NSNumber * GPEmployeeScoreCardId;
	NSNumber * GPPerformanceExecutionReportId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * Progress;
	NSString * ReportNumber;
	NSDate * ReportTime;
	NSNumber * Reporter;
	NSNumber * Status;
	NSNumber * Target;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_GPPerformanceExecutionReport *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ActivityName;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_GPEmployeeScoreCard * GPEmployeeScoreCard;
@property (retain) NSNumber * GPEmployeeScoreCardId;
@property (retain) NSNumber * GPPerformanceExecutionReportId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Progress;
@property (retain) NSString * ReportNumber;
@property (retain) NSDate * ReportTime;
@property (retain) NSNumber * Reporter;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * Target;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_GPEmployeeScoreCard : NSObject {
	
/* elements */
	NSNumber * Actual;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	org_tns2_GPCompanyScoreCard * GPCompanyScoreCard;
	NSNumber * GPCompanyScoreCardId;
	org_tns2_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCard1;
	org_tns2_GPEmployeeScoreCard * GPEmployeeScoreCard2;
	org_tns2_ArrayOfGPEmployeeScoreCardAppraisal * GPEmployeeScoreCardAppraisals;
	NSNumber * GPEmployeeScoreCardId;
	org_tns2_ArrayOfGPEmployeeScoreCardProgress * GPEmployeeScoreCardProgresses;
	org_tns2_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
	org_tns2_GPPerformanceExecutionReport * GPPerformanceExecutionReport;
	org_tns2_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	org_tns2_GPPerspectiveMeasure * GPPerspectiveMeasure;
	NSNumber * GPPerspectiveMeasureId;
	USBoolean * IsDeleted;
	NSString * Metric;
	NSDate * ModifiedDate;
	NSNumber * ParentId;
	NSNumber * PercentageOwnership;
	NSNumber * Progress;
	NSString * ScoreCardGroup;
	NSString * ScoreCardSubGroup;
	NSString * Statement;
	NSNumber * Status;
	NSNumber * Target;
	NSNumber * Weight;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_GPEmployeeScoreCard *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Actual;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) org_tns2_GPCompanyScoreCard * GPCompanyScoreCard;
@property (retain) NSNumber * GPCompanyScoreCardId;
@property (retain) org_tns2_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCard1;
@property (retain) org_tns2_GPEmployeeScoreCard * GPEmployeeScoreCard2;
@property (retain) org_tns2_ArrayOfGPEmployeeScoreCardAppraisal * GPEmployeeScoreCardAppraisals;
@property (retain) NSNumber * GPEmployeeScoreCardId;
@property (retain) org_tns2_ArrayOfGPEmployeeScoreCardProgress * GPEmployeeScoreCardProgresses;
@property (retain) org_tns2_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
@property (retain) org_tns2_GPPerformanceExecutionReport * GPPerformanceExecutionReport;
@property (retain) org_tns2_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) org_tns2_GPPerspectiveMeasure * GPPerspectiveMeasure;
@property (retain) NSNumber * GPPerspectiveMeasureId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Metric;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * ParentId;
@property (retain) NSNumber * PercentageOwnership;
@property (retain) NSNumber * Progress;
@property (retain) NSString * ScoreCardGroup;
@property (retain) NSString * ScoreCardSubGroup;
@property (retain) NSString * Statement;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * Target;
@property (retain) NSNumber * Weight;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfGPEmployeeScoreCard : NSObject {
	
/* elements */
	NSMutableArray *GPEmployeeScoreCard;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfGPEmployeeScoreCard *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPEmployeeScoreCard:(org_tns2_GPEmployeeScoreCard *)toAdd;
@property (readonly) NSMutableArray * GPEmployeeScoreCard;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_GPCompanyScoreCard : NSObject {
	
/* elements */
	NSNumber * Actual;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	NSNumber * GPCompanyScoreCardId;
	org_tns2_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
	NSNumber * GPCompanyYearlyObjectiveId;
	org_tns2_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
	org_tns2_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	org_tns2_GPPerspectiveMeasure * GPPerspectiveMeasure;
	NSNumber * GPPerspectiveMeasureId;
	USBoolean * IsDeleted;
	NSString * Metric;
	NSDate * ModifiedDate;
	NSNumber * Progress;
	NSString * ReportingRythm;
	NSNumber * ScoreCardCreator;
	NSString * Statement;
	NSNumber * Status;
	NSNumber * Target;
	NSNumber * Weight;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_GPCompanyScoreCard *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Actual;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * GPCompanyScoreCardId;
@property (retain) org_tns2_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
@property (retain) NSNumber * GPCompanyYearlyObjectiveId;
@property (retain) org_tns2_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
@property (retain) org_tns2_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) org_tns2_GPPerspectiveMeasure * GPPerspectiveMeasure;
@property (retain) NSNumber * GPPerspectiveMeasureId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Metric;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Progress;
@property (retain) NSString * ReportingRythm;
@property (retain) NSNumber * ScoreCardCreator;
@property (retain) NSString * Statement;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * Target;
@property (retain) NSNumber * Weight;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfGPCompanyScoreCard : NSObject {
	
/* elements */
	NSMutableArray *GPCompanyScoreCard;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfGPCompanyScoreCard *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addGPCompanyScoreCard:(org_tns2_GPCompanyScoreCard *)toAdd;
@property (readonly) NSMutableArray * GPCompanyScoreCard;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_GPCompanyYearlyObjective : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	org_tns2_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
	org_tns2_GPCompanyStrategicGoal * GPCompanyStrategicGoal;
	NSNumber * GPCompanyStrategicGoalId;
	NSNumber * GPCompanyYearlyObjectiveId;
	org_tns2_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
	org_tns2_GPObjectiveInitiative * GPObjectiveInitiative;
	NSNumber * GPObjectiveInitiativeId;
	org_tns2_GPPerspective * GPPerspective;
	NSNumber * GPPerspectiveId;
	org_tns2_GPStrategy * GPStrategy;
	NSNumber * GPStrategyId;
	USBoolean * IsDeleted;
	org_tns2_ArrayOfLMSCourse * LMSCourses;
	NSDate * ModifiedDate;
	NSNumber * Owner;
	NSString * SmartAttainableGoal;
	NSString * SmartMeasureGoal;
	NSString * SmartRelevantGoal;
	NSString * SmartSpecificGoal;
	NSString * SmartTimeGoal;
	NSString * Statement;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_GPCompanyYearlyObjective *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) org_tns2_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
@property (retain) org_tns2_GPCompanyStrategicGoal * GPCompanyStrategicGoal;
@property (retain) NSNumber * GPCompanyStrategicGoalId;
@property (retain) NSNumber * GPCompanyYearlyObjectiveId;
@property (retain) org_tns2_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
@property (retain) org_tns2_GPObjectiveInitiative * GPObjectiveInitiative;
@property (retain) NSNumber * GPObjectiveInitiativeId;
@property (retain) org_tns2_GPPerspective * GPPerspective;
@property (retain) NSNumber * GPPerspectiveId;
@property (retain) org_tns2_GPStrategy * GPStrategy;
@property (retain) NSNumber * GPStrategyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) org_tns2_ArrayOfLMSCourse * LMSCourses;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Owner;
@property (retain) NSString * SmartAttainableGoal;
@property (retain) NSString * SmartMeasureGoal;
@property (retain) NSString * SmartRelevantGoal;
@property (retain) NSString * SmartSpecificGoal;
@property (retain) NSString * SmartTimeGoal;
@property (retain) NSString * Statement;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_LMSTopicCompetency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	org_tns2_LMSContentTopic * LMSContentTopic;
	NSNumber * LMSContentTopicId;
	NSNumber * LMSTopicCompetencyId;
	NSDate * ModifiedDate;
	org_tns2_OrgCompetency * OrgCompetency;
	NSNumber * OrgCompetencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_LMSTopicCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) org_tns2_LMSContentTopic * LMSContentTopic;
@property (retain) NSNumber * LMSContentTopicId;
@property (retain) NSNumber * LMSTopicCompetencyId;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_OrgCompetency * OrgCompetency;
@property (retain) NSNumber * OrgCompetencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfLMSTopicCompetency : NSObject {
	
/* elements */
	NSMutableArray *LMSTopicCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfLMSTopicCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSTopicCompetency:(org_tns2_LMSTopicCompetency *)toAdd;
@property (readonly) NSMutableArray * LMSTopicCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfLMSContentTopic : NSObject {
	
/* elements */
	NSMutableArray *LMSContentTopic;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfLMSContentTopic *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSContentTopic:(org_tns2_LMSContentTopic *)toAdd;
@property (readonly) NSMutableArray * LMSContentTopic;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_LMSTopicGroup : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	org_tns2_ArrayOfLMSContentTopic * LMSContentTopics;
	NSNumber * LMSTopicGroupId;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_LMSTopicGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) org_tns2_ArrayOfLMSContentTopic * LMSContentTopics;
@property (retain) NSNumber * LMSTopicGroupId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_LMSContentTopic : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * LMSContentTopicId;
	org_tns2_ArrayOfLMSCourse * LMSCourses;
	org_tns2_ArrayOfLMSTopicCompetency * LMSTopicCompetencies;
	org_tns2_LMSTopicGroup * LMSTopicGroup;
	NSNumber * LMSTopicGroupId;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_LMSContentTopic *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * LMSContentTopicId;
@property (retain) org_tns2_ArrayOfLMSCourse * LMSCourses;
@property (retain) org_tns2_ArrayOfLMSTopicCompetency * LMSTopicCompetencies;
@property (retain) org_tns2_LMSTopicGroup * LMSTopicGroup;
@property (retain) NSNumber * LMSTopicGroupId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_LMSCourseAttendee : NSObject {
	
/* elements */
	org_tns2_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSString * Comment;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsApproved;
	USBoolean * IsDeleted;
	USBoolean * IsPassed;
	org_tns2_LMSCourse * LMSCourse;
	NSNumber * LMSCourseAttendeeId;
	NSNumber * LMSCourseId;
	NSNumber * LMSDevelopmentPlanId;
	NSDate * ModifiedDate;
	NSNumber * NumberOfWarning;
	NSNumber * OverallScore;
	NSNumber * ParticipationStatus;
	NSNumber * ProgressStatus;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_LMSCourseAttendee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsApproved;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsPassed;
@property (retain) org_tns2_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseAttendeeId;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSNumber * LMSDevelopmentPlanId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NumberOfWarning;
@property (retain) NSNumber * OverallScore;
@property (retain) NSNumber * ParticipationStatus;
@property (retain) NSNumber * ProgressStatus;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfLMSCourseAttendee : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseAttendee;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfLMSCourseAttendee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseAttendee:(org_tns2_LMSCourseAttendee *)toAdd;
@property (readonly) NSMutableArray * LMSCourseAttendee;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CLogLMSContentType : NSObject {
	
/* elements */
	NSNumber * CLogLMSContentTypeId;
	NSNumber * CompanyId;
	USBoolean * IsDeleted;
	org_tns2_ArrayOfLMSCourseContent * LMSCourseContents;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CLogLMSContentType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogLMSContentTypeId;
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsDeleted;
@property (retain) org_tns2_ArrayOfLMSCourseContent * LMSCourseContents;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_LMSCourseContent : NSObject {
	
/* elements */
	org_tns2_CLogLMSContentType * CLogLMSContentType;
	NSNumber * CLogLMSContentType_CLogLMSContentTypeId;
	org_tns2_LMSCourse * LMSCourse;
	org_tns2_LMSCourseContent * LMSCourseContent1;
	NSNumber * LMSCourseContent1_LMSCourseContentId;
	NSNumber * LMSCourseContentId;
	org_tns2_LMSCourseContent * LMSCourseContents1;
	NSNumber * LMSCourse_LMSCourseId;
	NSString * Name;
	NSString * Statement;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_LMSCourseContent *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_CLogLMSContentType * CLogLMSContentType;
@property (retain) NSNumber * CLogLMSContentType_CLogLMSContentTypeId;
@property (retain) org_tns2_LMSCourse * LMSCourse;
@property (retain) org_tns2_LMSCourseContent * LMSCourseContent1;
@property (retain) NSNumber * LMSCourseContent1_LMSCourseContentId;
@property (retain) NSNumber * LMSCourseContentId;
@property (retain) org_tns2_LMSCourseContent * LMSCourseContents1;
@property (retain) NSNumber * LMSCourse_LMSCourseId;
@property (retain) NSString * Name;
@property (retain) NSString * Statement;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfLMSCourseContent : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseContent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfLMSCourseContent *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseContent:(org_tns2_LMSCourseContent *)toAdd;
@property (readonly) NSMutableArray * LMSCourseContent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_LMSCourseDetail : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CourseContentTypeId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	org_tns2_LMSCourse * LMSCourse;
	NSNumber * LMSCourseDetailId;
	NSNumber * LMSCourseId;
	NSNumber * Level;
	NSDate * ModifiedDate;
	NSString * Resource;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_LMSCourseDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CourseContentTypeId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) org_tns2_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseDetailId;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSNumber * Level;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Resource;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfLMSCourseDetail : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfLMSCourseDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseDetail:(org_tns2_LMSCourseDetail *)toAdd;
@property (readonly) NSMutableArray * LMSCourseDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgUnit : NSObject {
	
/* elements */
	NSMutableArray *OrgUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgUnit:(org_tns2_OrgUnit *)toAdd;
@property (readonly) NSMutableArray * OrgUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgUnit : NSObject {
	
/* elements */
	NSString * Address;
	org_tns2_Address * Address1;
	NSNumber * AddressId;
	NSString * CommissionDesc;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	NSString * FunctionDesc;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	org_tns2_ArrayOfLMSCourseProgressUnit * LMSCourseProgressUnits;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
	org_tns2_ArrayOfOrgUnit * OrgUnit1;
	org_tns2_OrgUnit * OrgUnit2;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSNumber * OrgUnitLevelId;
	NSString * ParentCode;
	NSNumber * ParentId;
	NSNumber * TotalEmployees;
	NSString * WorkLocation;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) org_tns2_Address * Address1;
@property (retain) NSNumber * AddressId;
@property (retain) NSString * CommissionDesc;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) NSString * FunctionDesc;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) org_tns2_ArrayOfLMSCourseProgressUnit * LMSCourseProgressUnits;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) org_tns2_ArrayOfOrgUnit * OrgUnit1;
@property (retain) org_tns2_OrgUnit * OrgUnit2;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * OrgUnitLevelId;
@property (retain) NSString * ParentCode;
@property (retain) NSNumber * ParentId;
@property (retain) NSNumber * TotalEmployees;
@property (retain) NSString * WorkLocation;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_LMSCourseProgressUnit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	org_tns2_LMSCourse * LMSCourse;
	NSNumber * LMSCourseId;
	NSNumber * LMSCourseProgressUnitId;
	NSDate * ModifiedDate;
	NSNumber * NumOfEmpFinish;
	org_tns2_OrgUnit * OrgUnit;
	NSNumber * OrgUnitId;
	USBoolean * Status;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_LMSCourseProgressUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) org_tns2_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSNumber * LMSCourseProgressUnitId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NumOfEmpFinish;
@property (retain) org_tns2_OrgUnit * OrgUnit;
@property (retain) NSNumber * OrgUnitId;
@property (retain) USBoolean * Status;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfLMSCourseProgressUnit : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseProgressUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfLMSCourseProgressUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseProgressUnit:(org_tns2_LMSCourseProgressUnit *)toAdd;
@property (readonly) NSMutableArray * LMSCourseProgressUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_LMSCourseRequiredCompetency : NSObject {
	
/* elements */
	org_tns2_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompetencyName;
	NSString * CompetencyType;
	NSNumber * CourseRequiredCompetencyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	org_tns2_LMSCourse * LMSCourse;
	NSNumber * LMSCourseId;
	NSDate * ModifiedDate;
	org_tns2_OrgCompetency * OrgCompetency;
	NSNumber * OrgCompetencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_LMSCourseRequiredCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompetencyName;
@property (retain) NSString * CompetencyType;
@property (retain) NSNumber * CourseRequiredCompetencyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) org_tns2_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_OrgCompetency * OrgCompetency;
@property (retain) NSNumber * OrgCompetencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfLMSCourseRequiredCompetency : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseRequiredCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfLMSCourseRequiredCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseRequiredCompetency:(org_tns2_LMSCourseRequiredCompetency *)toAdd;
@property (readonly) NSMutableArray * LMSCourseRequiredCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_LMSCourseTranscript : NSObject {
	
/* elements */
	org_tns2_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSString * Comment;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	org_tns2_Employee * Employee1;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsPass;
	org_tns2_LMSCourse * LMSCourse;
	NSNumber * LMSCourseId;
	NSNumber * LMSCourseTranscriptId;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_LMSCourseTranscript *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) org_tns2_Employee * Employee1;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsPass;
@property (retain) org_tns2_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSNumber * LMSCourseTranscriptId;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfLMSCourseTranscript : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseTranscript;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfLMSCourseTranscript *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseTranscript:(org_tns2_LMSCourseTranscript *)toAdd;
@property (readonly) NSMutableArray * LMSCourseTranscript;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_LMSCourseType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSNumber * LMSCourseTypeId;
	NSString * LMSCourseTypeName;
	org_tns2_ArrayOfLMSCourse * LMSCourses;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_LMSCourseType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * LMSCourseTypeId;
@property (retain) NSString * LMSCourseTypeName;
@property (retain) org_tns2_ArrayOfLMSCourse * LMSCourses;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_LMSCourse : NSObject {
	
/* elements */
	NSNumber * ApprovedBy;
	org_tns2_CLogCourseStatu * CLogCourseStatu;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSString * CourseIcon;
	NSNumber * CourseStatusId;
	NSNumber * CourseTypeId;
	NSDate * CreatedDate;
	NSDate * EndDate;
	org_tns2_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
	org_tns2_GPObjectiveInitiative * GPObjectiveInitiative;
	USBoolean * IsApproved;
	USBoolean * IsDeleted;
	USBoolean * IsRequired;
	NSString * Keyword;
	org_tns2_LMSContentTopic * LMSContentTopic;
	org_tns2_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
	NSString * LMSCourseCode;
	org_tns2_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
	org_tns2_ArrayOfLMSCourseContent * LMSCourseContents;
	org_tns2_ArrayOfLMSCourseDetail * LMSCourseDetails;
	NSNumber * LMSCourseId;
	org_tns2_ArrayOfLMSCourseProgressUnit * LMSCourseProgressUnits;
	org_tns2_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
	org_tns2_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
	org_tns2_LMSCourseType * LMSCourseType;
	NSNumber * LMSInitiativeObjectiveId;
	NSNumber * LMSTopicGroupId;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
	NSNumber * Requestedby;
	NSDate * StartDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_LMSCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApprovedBy;
@property (retain) org_tns2_CLogCourseStatu * CLogCourseStatu;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSString * CourseIcon;
@property (retain) NSNumber * CourseStatusId;
@property (retain) NSNumber * CourseTypeId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * EndDate;
@property (retain) org_tns2_GPCompanyYearlyObjective * GPCompanyYearlyObjective;
@property (retain) org_tns2_GPObjectiveInitiative * GPObjectiveInitiative;
@property (retain) USBoolean * IsApproved;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsRequired;
@property (retain) NSString * Keyword;
@property (retain) org_tns2_LMSContentTopic * LMSContentTopic;
@property (retain) org_tns2_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
@property (retain) NSString * LMSCourseCode;
@property (retain) org_tns2_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
@property (retain) org_tns2_ArrayOfLMSCourseContent * LMSCourseContents;
@property (retain) org_tns2_ArrayOfLMSCourseDetail * LMSCourseDetails;
@property (retain) NSNumber * LMSCourseId;
@property (retain) org_tns2_ArrayOfLMSCourseProgressUnit * LMSCourseProgressUnits;
@property (retain) org_tns2_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
@property (retain) org_tns2_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
@property (retain) org_tns2_LMSCourseType * LMSCourseType;
@property (retain) NSNumber * LMSInitiativeObjectiveId;
@property (retain) NSNumber * LMSTopicGroupId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) NSNumber * Requestedby;
@property (retain) NSDate * StartDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_LMSCourseCompetency : NSObject {
	
/* elements */
	org_tns2_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompetencyName;
	NSNumber * CompetencyTypeId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	org_tns2_LMSCourse * LMSCourse;
	NSNumber * LMSCourseCompetencyId;
	NSNumber * LMSCourseId;
	NSDate * ModifiedDate;
	org_tns2_OrgCompetency * OrgCompetency;
	NSNumber * OrgCompetencyId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_LMSCourseCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompetencyName;
@property (retain) NSNumber * CompetencyTypeId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) org_tns2_LMSCourse * LMSCourse;
@property (retain) NSNumber * LMSCourseCompetencyId;
@property (retain) NSNumber * LMSCourseId;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_OrgCompetency * OrgCompetency;
@property (retain) NSNumber * OrgCompetencyId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfLMSCourseCompetency : NSObject {
	
/* elements */
	NSMutableArray *LMSCourseCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfLMSCourseCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLMSCourseCompetency:(org_tns2_LMSCourseCompetency *)toAdd;
@property (readonly) NSMutableArray * LMSCourseCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgCompetency : NSObject {
	
/* elements */
	NSMutableArray *OrgCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgCompetency:(org_tns2_OrgCompetency *)toAdd;
@property (readonly) NSMutableArray * OrgCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgCompetencyGroup : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * GroupName;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	org_tns2_ArrayOfOrgCompetency * OrgCompetencies;
	NSNumber * OrgCompetencyGroupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgCompetencyGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * GroupName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_ArrayOfOrgCompetency * OrgCompetencies;
@property (retain) NSNumber * OrgCompetencyGroupId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgJobSpecificCompetency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	org_tns2_OrgCompetency * OrgCompetency;
	NSNumber * OrgCompetencyId;
	org_tns2_OrgJob * OrgJob;
	NSNumber * OrgJobId;
	NSNumber * OrgJobSpecificCompetencyId;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgJobSpecificCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_OrgCompetency * OrgCompetency;
@property (retain) NSNumber * OrgCompetencyId;
@property (retain) org_tns2_OrgJob * OrgJob;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSNumber * OrgJobSpecificCompetencyId;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgJobSpecificCompetency : NSObject {
	
/* elements */
	NSMutableArray *OrgJobSpecificCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgJobSpecificCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgJobSpecificCompetency:(org_tns2_OrgJobSpecificCompetency *)toAdd;
@property (readonly) NSMutableArray * OrgJobSpecificCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgProficencyLevel : NSObject {
	
/* elements */
	NSMutableArray *OrgProficencyLevel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgProficencyLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgProficencyLevel:(org_tns2_OrgProficencyLevel *)toAdd;
@property (readonly) NSMutableArray * OrgProficencyLevel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgCompetency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Definition;
	USBoolean * IsCore;
	USBoolean * IsDeleted;
	org_tns2_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
	org_tns2_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
	org_tns2_ArrayOfLMSTopicCompetency * LMSTopicCompetencies;
	NSDate * ModifiedDate;
	NSString * Name;
	org_tns2_OrgCompetencyGroup * OrgCompetencyGroup;
	NSNumber * OrgCompetencyGroupId;
	NSNumber * OrgCompetencyId;
	org_tns2_ArrayOfOrgJobSpecificCompetency * OrgJobSpecificCompetencies;
	org_tns2_ArrayOfOrgProficencyLevel * OrgProficencyLevels;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Definition;
@property (retain) USBoolean * IsCore;
@property (retain) USBoolean * IsDeleted;
@property (retain) org_tns2_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
@property (retain) org_tns2_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
@property (retain) org_tns2_ArrayOfLMSTopicCompetency * LMSTopicCompetencies;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) org_tns2_OrgCompetencyGroup * OrgCompetencyGroup;
@property (retain) NSNumber * OrgCompetencyGroupId;
@property (retain) NSNumber * OrgCompetencyId;
@property (retain) org_tns2_ArrayOfOrgJobSpecificCompetency * OrgJobSpecificCompetencies;
@property (retain) org_tns2_ArrayOfOrgProficencyLevel * OrgProficencyLevels;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgProficencyLevel : NSObject {
	
/* elements */
	org_tns2_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompetencyLevelName;
	NSDate * CreatedDate;
	org_tns2_ArrayOfEmpCompetency * EmpCompetencies;
	org_tns2_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	org_tns2_OrgCompetency * OrgCompetency;
	NSNumber * OrgCompetencyId;
	org_tns2_ArrayOfOrgJobPositionSpecificCompetency * OrgJobPositionSpecificCompetencies;
	org_tns2_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
	NSNumber * OrgProficencyLevelId;
	org_tns2_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
	NSString * Statement;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgProficencyLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompetencyLevelName;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_ArrayOfEmpCompetency * EmpCompetencies;
@property (retain) org_tns2_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_OrgCompetency * OrgCompetency;
@property (retain) NSNumber * OrgCompetencyId;
@property (retain) org_tns2_ArrayOfOrgJobPositionSpecificCompetency * OrgJobPositionSpecificCompetencies;
@property (retain) org_tns2_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) org_tns2_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
@property (retain) NSString * Statement;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgJobPositionSpecificCompetency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	org_tns2_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSNumber * OrgJobPositionSpecificCompetencyId;
	org_tns2_OrgProficencyLevel * OrgProficencyLevel;
	NSNumber * OrgProficencyLevelId;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgJobPositionSpecificCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * OrgJobPositionSpecificCompetencyId;
@property (retain) org_tns2_OrgProficencyLevel * OrgProficencyLevel;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgJobPositionSpecificCompetency : NSObject {
	
/* elements */
	NSMutableArray *OrgJobPositionSpecificCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgJobPositionSpecificCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgJobPositionSpecificCompetency:(org_tns2_OrgJobPositionSpecificCompetency *)toAdd;
@property (readonly) NSMutableArray * OrgJobPositionSpecificCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	org_tns2_ArrayOfEmpContract * EmpContracts;
	org_tns2_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
	org_tns2_OrgJob * OrgJob;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	org_tns2_ArrayOfOrgJobPositionRequiredProficency * OrgJobPositionRequiredProficencies;
	org_tns2_ArrayOfOrgJobPositionSpecificCompetency * OrgJobPositionSpecificCompetencies;
	NSString * OrgJobPositionTitle;
	NSString * OrgJobTitleCode;
	NSNumber * OrgJobTitleId;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgWorkLevelCode;
	NSNumber * OrgWorkLevelId;
	org_tns2_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
	org_tns2_ArrayOfRecPlanJobPosition * RecPlanJobPositions;
	org_tns2_ArrayOfRecProcessApplyForJobPosition * RecProcessApplyForJobPositions;
	org_tns2_ArrayOfRecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPositions;
	org_tns2_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
	org_tns2_ArrayOfRecRequirementJobPosition * RecRequirementJobPositions;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_ArrayOfEmpContract * EmpContracts;
@property (retain) org_tns2_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) org_tns2_OrgJob * OrgJob;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) org_tns2_ArrayOfOrgJobPositionRequiredProficency * OrgJobPositionRequiredProficencies;
@property (retain) org_tns2_ArrayOfOrgJobPositionSpecificCompetency * OrgJobPositionSpecificCompetencies;
@property (retain) NSString * OrgJobPositionTitle;
@property (retain) NSString * OrgJobTitleCode;
@property (retain) NSNumber * OrgJobTitleId;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) org_tns2_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
@property (retain) org_tns2_ArrayOfRecPlanJobPosition * RecPlanJobPositions;
@property (retain) org_tns2_ArrayOfRecProcessApplyForJobPosition * RecProcessApplyForJobPositions;
@property (retain) org_tns2_ArrayOfRecRecruitmentPhaseJobPosition * RecRecruitmentPhaseJobPositions;
@property (retain) org_tns2_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
@property (retain) org_tns2_ArrayOfRecRequirementJobPosition * RecRequirementJobPositions;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgJobPosition : NSObject {
	
/* elements */
	NSMutableArray *OrgJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgJobPosition:(org_tns2_OrgJobPosition *)toAdd;
@property (readonly) NSMutableArray * OrgJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgUnitJob : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	org_tns2_OrgJob * OrgJob;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSNumber * OrgUnitJobId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgUnitJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_OrgJob * OrgJob;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * OrgUnitJobId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgUnitJob : NSObject {
	
/* elements */
	NSMutableArray *OrgUnitJob;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgUnitJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgUnitJob:(org_tns2_OrgUnitJob *)toAdd;
@property (readonly) NSMutableArray * OrgUnitJob;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgJob : NSObject {
	
/* elements */
	NSString * CLogCareerCode;
	NSNumber * CLogCareerId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_ArrayOfEmpContract * EmpContracts;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	org_tns2_ArrayOfOrgJobPosition * OrgJobPositions;
	org_tns2_ArrayOfOrgJobSpecificCompetency * OrgJobSpecificCompetencies;
	org_tns2_ArrayOfOrgUnitJob * OrgUnitJobs;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogCareerCode;
@property (retain) NSNumber * CLogCareerId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_ArrayOfEmpContract * EmpContracts;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) org_tns2_ArrayOfOrgJobPosition * OrgJobPositions;
@property (retain) org_tns2_ArrayOfOrgJobSpecificCompetency * OrgJobSpecificCompetencies;
@property (retain) org_tns2_ArrayOfOrgUnitJob * OrgUnitJobs;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpContract : NSObject {
	
/* elements */
	org_tns2_Address * Address;
	NSString * AllocationEquipmentDescription;
	NSString * Allowances;
	NSString * AttachFile;
	NSNumber * BaseWage;
	NSString * Bonuses;
	NSString * CLogContractTermCode;
	NSString * CLogContractTypeCode;
	NSNumber * CompanyId;
	NSString * CompensationDescription;
	NSString * ContractCode;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DateSigned;
	NSDate * EmpAppraiseDate;
	NSString * EmpAppraiseNote;
	NSNumber * EmpAppraiseRating;
	NSNumber * EmpContractId;
	org_tns2_Employee * Employee;
	NSString * EmployeeRepresentativeAddress;
	NSNumber * EmployeeRepresentativeAddressId;
	NSDate * EmployeeRepresentativeBirthDay;
	NSString * EmployeeRepresentativeBirthPlace;
	NSString * EmployeeRepresentativeCode;
	NSString * EmployeeRepresentativeFullName;
	NSNumber * EmployeeRepresentativeId;
	NSString * EmployeeRepresentativeMajor;
	NSString * EmployeeRepresentativeNationality;
	org_tns2_Employer * Employer;
	NSNumber * EmployerRepresentativeId;
	NSDate * EndDate;
	NSDate * ExpirationDate;
	NSDate * HRAppraiseDate;
	NSString * HRAppraiseNote;
	NSNumber * HRAppraiseRating;
	NSString * HolIdayPolicy;
	NSNumber * HoursOfOfficialWork;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	USBoolean * IsDeleted;
	USBoolean * IsEnd;
	USBoolean * IsProbation;
	USBoolean * IsSigned;
	NSString * JobResponsibilitiesDescription;
	NSString * LaborSupportEquipments;
	NSDate * LeaderAppraiseDate;
	NSString * LeaderAppraiseNote;
	NSNumber * LeaderAppraiseRating;
	NSString * MedicalInsurance;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSDate * OfficialWorkEndTime;
	NSDate * OfficialWorkStartTime;
	org_tns2_OrgJob * OrgJob;
	NSNumber * OrgJobId;
	org_tns2_OrgJobPosition * OrgJobPosition;
	NSNumber * OrgJobPositionId;
	NSNumber * OrgUnitId;
	NSString * OrganizationAddress;
	NSNumber * OrganizationAddressId;
	NSString * OrganizationName;
	NSString * OrganizationPhone;
	NSString * OtherPolicy;
	NSString * PayDay;
	NSString * PayingMethod;
	NSString * PlaceSigned;
	NSDate * ProbationaryEndDate;
	NSDate * ProbationaryStartDate;
	NSNumber * ProbationaryTime;
	NSString * ProvidedEquipment;
	NSNumber * RelationEmpContractId;
	NSString * SalaryIncreasePolicy;
	NSString * SocialInsurance;
	NSDate * StartDate;
	NSString * TrainingPolicy;
	NSString * Transportation;
	NSString * WorkingAddress;
	NSNumber * WorkingAddressId;
	NSNumber * WorkingDay;
	NSString * WorkingTime;
	NSString * WorkingTimePolicy;
	NSString * cAllowance;
	NSString * cBaseSalary;
	NSString * cSocialInsuranceSalary;
	NSNumber * nAllowance;
	NSNumber * nSocialInsuranceSalary;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_Address * Address;
@property (retain) NSString * AllocationEquipmentDescription;
@property (retain) NSString * Allowances;
@property (retain) NSString * AttachFile;
@property (retain) NSNumber * BaseWage;
@property (retain) NSString * Bonuses;
@property (retain) NSString * CLogContractTermCode;
@property (retain) NSString * CLogContractTypeCode;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompensationDescription;
@property (retain) NSString * ContractCode;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateSigned;
@property (retain) NSDate * EmpAppraiseDate;
@property (retain) NSString * EmpAppraiseNote;
@property (retain) NSNumber * EmpAppraiseRating;
@property (retain) NSNumber * EmpContractId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeRepresentativeAddress;
@property (retain) NSNumber * EmployeeRepresentativeAddressId;
@property (retain) NSDate * EmployeeRepresentativeBirthDay;
@property (retain) NSString * EmployeeRepresentativeBirthPlace;
@property (retain) NSString * EmployeeRepresentativeCode;
@property (retain) NSString * EmployeeRepresentativeFullName;
@property (retain) NSNumber * EmployeeRepresentativeId;
@property (retain) NSString * EmployeeRepresentativeMajor;
@property (retain) NSString * EmployeeRepresentativeNationality;
@property (retain) org_tns2_Employer * Employer;
@property (retain) NSNumber * EmployerRepresentativeId;
@property (retain) NSDate * EndDate;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSDate * HRAppraiseDate;
@property (retain) NSString * HRAppraiseNote;
@property (retain) NSNumber * HRAppraiseRating;
@property (retain) NSString * HolIdayPolicy;
@property (retain) NSNumber * HoursOfOfficialWork;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEnd;
@property (retain) USBoolean * IsProbation;
@property (retain) USBoolean * IsSigned;
@property (retain) NSString * JobResponsibilitiesDescription;
@property (retain) NSString * LaborSupportEquipments;
@property (retain) NSDate * LeaderAppraiseDate;
@property (retain) NSString * LeaderAppraiseNote;
@property (retain) NSNumber * LeaderAppraiseRating;
@property (retain) NSString * MedicalInsurance;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * OfficialWorkEndTime;
@property (retain) NSDate * OfficialWorkStartTime;
@property (retain) org_tns2_OrgJob * OrgJob;
@property (retain) NSNumber * OrgJobId;
@property (retain) org_tns2_OrgJobPosition * OrgJobPosition;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrganizationAddress;
@property (retain) NSNumber * OrganizationAddressId;
@property (retain) NSString * OrganizationName;
@property (retain) NSString * OrganizationPhone;
@property (retain) NSString * OtherPolicy;
@property (retain) NSString * PayDay;
@property (retain) NSString * PayingMethod;
@property (retain) NSString * PlaceSigned;
@property (retain) NSDate * ProbationaryEndDate;
@property (retain) NSDate * ProbationaryStartDate;
@property (retain) NSNumber * ProbationaryTime;
@property (retain) NSString * ProvidedEquipment;
@property (retain) NSNumber * RelationEmpContractId;
@property (retain) NSString * SalaryIncreasePolicy;
@property (retain) NSString * SocialInsurance;
@property (retain) NSDate * StartDate;
@property (retain) NSString * TrainingPolicy;
@property (retain) NSString * Transportation;
@property (retain) NSString * WorkingAddress;
@property (retain) NSNumber * WorkingAddressId;
@property (retain) NSNumber * WorkingDay;
@property (retain) NSString * WorkingTime;
@property (retain) NSString * WorkingTimePolicy;
@property (retain) NSString * cAllowance;
@property (retain) NSString * cBaseSalary;
@property (retain) NSString * cSocialInsuranceSalary;
@property (retain) NSNumber * nAllowance;
@property (retain) NSNumber * nSocialInsuranceSalary;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpContract : NSObject {
	
/* elements */
	NSMutableArray *EmpContract;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpContract *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpContract:(org_tns2_EmpContract *)toAdd;
@property (readonly) NSMutableArray * EmpContract;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_Address : NSObject {
	
/* elements */
	NSNumber * AddressId;
	NSString * AddressNumber;
	NSNumber * CLogCityId;
	NSNumber * CLogCountryId;
	NSNumber * CLogDistrictId;
	org_tns2_ArrayOfCLogTrainer * CLogTrainers;
	org_tns2_ArrayOfEmpContract * EmpContracts;
	NSString * FullAddress;
	org_tns2_ArrayOfOrgUnit * OrgUnits;
	NSString * PostalCode;
	NSString * Street;
	NSString * Ward;
	NSString * ZipCode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_Address *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * AddressId;
@property (retain) NSString * AddressNumber;
@property (retain) NSNumber * CLogCityId;
@property (retain) NSNumber * CLogCountryId;
@property (retain) NSNumber * CLogDistrictId;
@property (retain) org_tns2_ArrayOfCLogTrainer * CLogTrainers;
@property (retain) org_tns2_ArrayOfEmpContract * EmpContracts;
@property (retain) NSString * FullAddress;
@property (retain) org_tns2_ArrayOfOrgUnit * OrgUnits;
@property (retain) NSString * PostalCode;
@property (retain) NSString * Street;
@property (retain) NSString * Ward;
@property (retain) NSString * ZipCode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CLogTrainer : NSObject {
	
/* elements */
	NSString * Address;
	org_tns2_Address * Address1;
	NSNumber * AddressId;
	NSNumber * CLogTrainerId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * EducationUnit;
	NSString * Email;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	NSString * FirstName;
	NSString * FullName;
	USBoolean * IsDeleted;
	USBoolean * IsEmployee;
	NSString * LastName;
	NSDate * ModifiedDate;
	NSString * PhoneNumber;
	org_tns2_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CLogTrainer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) org_tns2_Address * Address1;
@property (retain) NSNumber * AddressId;
@property (retain) NSNumber * CLogTrainerId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * EducationUnit;
@property (retain) NSString * Email;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEmployee;
@property (retain) NSString * LastName;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * PhoneNumber;
@property (retain) org_tns2_ArrayOfTrainingCourse * TrainingCourses;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfCLogTrainer : NSObject {
	
/* elements */
	NSMutableArray *CLogTrainer;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfCLogTrainer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogTrainer:(org_tns2_CLogTrainer *)toAdd;
@property (readonly) NSMutableArray * CLogTrainer;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CLogEmpWorkingStatu : NSObject {
	
/* elements */
	NSNumber * CLogEmpWorkingStatusId;
	NSNumber * CompanyId;
	org_tns2_ArrayOfEmpBasicProfile * EmpBasicProfiles;
	NSString * EmpWorkingStatusCode;
	NSString * EmpWorkingStatusName;
	NSString * EmpWorkingStatusNameEN;
	USBoolean * IsDeleted;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CLogEmpWorkingStatu *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogEmpWorkingStatusId;
@property (retain) NSNumber * CompanyId;
@property (retain) org_tns2_ArrayOfEmpBasicProfile * EmpBasicProfiles;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmpWorkingStatusNameEN;
@property (retain) USBoolean * IsDeleted;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpBasicProfile : NSObject {
	
/* elements */
	NSString * BankAccountNo;
	NSString * BankBranch;
	NSString * BankName;
	NSNumber * CLogBankBranchId;
	org_tns2_CLogEmpWorkingStatu * CLogEmpWorkingStatu;
	NSString * CVUrl;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * CulturalLevel;
	NSNumber * CulturalLevelId;
	NSString * EducationLevel;
	NSNumber * EducationLevelId;
	NSNumber * EmpBasicProfileId;
	NSNumber * EmpWorkingStatusId;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardPlaceOfIssue;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	NSString * Major;
	NSNumber * MajorId;
	NSString * MaritalStatus;
	NSNumber * MaritalStatusId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Nationality;
	NSNumber * NationalityId;
	NSString * PITCode;
	NSString * PITIssuedBy;
	NSDate * PassportDateOfExpire;
	NSDate * PassportDateOfIssue;
	NSString * PassportNo;
	NSString * PassportPlaceOfIssue;
	NSNumber * ProbationarySalary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpBasicProfile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * BankAccountNo;
@property (retain) NSString * BankBranch;
@property (retain) NSString * BankName;
@property (retain) NSNumber * CLogBankBranchId;
@property (retain) org_tns2_CLogEmpWorkingStatu * CLogEmpWorkingStatu;
@property (retain) NSString * CVUrl;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * CulturalLevel;
@property (retain) NSNumber * CulturalLevelId;
@property (retain) NSString * EducationLevel;
@property (retain) NSNumber * EducationLevelId;
@property (retain) NSNumber * EmpBasicProfileId;
@property (retain) NSNumber * EmpWorkingStatusId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Major;
@property (retain) NSNumber * MajorId;
@property (retain) NSString * MaritalStatus;
@property (retain) NSNumber * MaritalStatusId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Nationality;
@property (retain) NSNumber * NationalityId;
@property (retain) NSString * PITCode;
@property (retain) NSString * PITIssuedBy;
@property (retain) NSDate * PassportDateOfExpire;
@property (retain) NSDate * PassportDateOfIssue;
@property (retain) NSString * PassportNo;
@property (retain) NSString * PassportPlaceOfIssue;
@property (retain) NSNumber * ProbationarySalary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpBasicProfile : NSObject {
	
/* elements */
	NSMutableArray *EmpBasicProfile;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpBasicProfile *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpBasicProfile:(org_tns2_EmpBasicProfile *)toAdd;
@property (readonly) NSMutableArray * EmpBasicProfile;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpCompetencyRating : NSObject {
	
/* elements */
	NSMutableArray *EmpCompetencyRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpCompetencyRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpCompetencyRating:(org_tns2_EmpCompetencyRating *)toAdd;
@property (readonly) NSMutableArray * EmpCompetencyRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpPerformanceAppraisal : NSObject {
	
/* elements */
	NSDate * AppraisalDate;
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * Comment;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpPerformanceAppraisalId;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsEndYear;
	USBoolean * IsHalfYear;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * Rating;
	NSNumber * RelateRowId;
	NSNumber * Score;
	NSString * SelfComment;
	NSNumber * SelfRating;
	NSNumber * SelfScore;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpPerformanceAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AppraisalDate;
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * Comment;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpPerformanceAppraisalId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsEndYear;
@property (retain) USBoolean * IsHalfYear;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Rating;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSNumber * Score;
@property (retain) NSString * SelfComment;
@property (retain) NSNumber * SelfRating;
@property (retain) NSNumber * SelfScore;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpPerformanceAppraisal : NSObject {
	
/* elements */
	NSMutableArray *EmpPerformanceAppraisal;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpPerformanceAppraisal *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpPerformanceAppraisal:(org_tns2_EmpPerformanceAppraisal *)toAdd;
@property (readonly) NSMutableArray * EmpPerformanceAppraisal;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileAllowance : NSObject {
	
/* elements */
	NSDate * ActivedDate;
	NSString * AllowanceCode;
	NSString * AllowanceName;
	NSNumber * AllowanceValue;
	NSNumber * CLogCurrency;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpProfileAllowanceId;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsActived;
	USBoolean * IsDeactived;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileAllowance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ActivedDate;
@property (retain) NSString * AllowanceCode;
@property (retain) NSString * AllowanceName;
@property (retain) NSNumber * AllowanceValue;
@property (retain) NSNumber * CLogCurrency;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileAllowanceId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeactived;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileAllowance : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileAllowance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileAllowance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileAllowance:(org_tns2_EmpProfileAllowance *)toAdd;
@property (readonly) NSMutableArray * EmpProfileAllowance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileBaseSalary : NSObject {
	
/* elements */
	NSDate * ActivedDate;
	NSNumber * CBSalaryCofficientId;
	NSNumber * CBSalaryGradeId;
	NSNumber * CBSalaryLevelId;
	NSNumber * CBSalaryScaleId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpProfileBaseSalaryId;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsActived;
	USBoolean * IsDeactived;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * NonPayScaleSalaryValue;
	NSString * Note;
	NSNumber * PayScaleSalaryValue;
	NSString * SalaryCode;
	NSNumber * SalaryId;
	NSString * SalaryName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileBaseSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * ActivedDate;
@property (retain) NSNumber * CBSalaryCofficientId;
@property (retain) NSNumber * CBSalaryGradeId;
@property (retain) NSNumber * CBSalaryLevelId;
@property (retain) NSNumber * CBSalaryScaleId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileBaseSalaryId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsActived;
@property (retain) USBoolean * IsDeactived;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NonPayScaleSalaryValue;
@property (retain) NSString * Note;
@property (retain) NSNumber * PayScaleSalaryValue;
@property (retain) NSString * SalaryCode;
@property (retain) NSNumber * SalaryId;
@property (retain) NSString * SalaryName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileBaseSalary : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileBaseSalary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileBaseSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileBaseSalary:(org_tns2_EmpProfileBaseSalary *)toAdd;
@property (readonly) NSMutableArray * EmpProfileBaseSalary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileBenefit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileBenefitId;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * HealthInsuranceDateExpire;
	NSDate * HealthInsuranceDateIssue;
	NSNumber * HealthInsuranceId;
	NSString * HealthInsuranceNumber;
	NSString * HealthInsuranceRegPlace;
	USBoolean * IsDeleted;
	NSDate * LabourBookDateOfIssue;
	NSString * LabourBookNo;
	NSString * LabourBookPlaceOfIssue;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * OtherInsuranceCompanyName;
	NSDate * OtherInsuranceDateExpire;
	NSDate * OtherInsuranceDateIssue;
	NSString * OtherInsuranceNumber;
	NSDate * SocialInsuranceDateIssue;
	NSNumber * SocialInsuranceId;
	NSNumber * SocialInsuranceJoiningBefore;
	NSString * SocialInsuranceNumber;
	NSDate * UnemploymentInsuranceDateIssue;
	NSNumber * UnemploymentInsuranceId;
	NSString * UnemploymentInsuranceNumber;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileBenefitId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * HealthInsuranceDateExpire;
@property (retain) NSDate * HealthInsuranceDateIssue;
@property (retain) NSNumber * HealthInsuranceId;
@property (retain) NSString * HealthInsuranceNumber;
@property (retain) NSString * HealthInsuranceRegPlace;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * LabourBookDateOfIssue;
@property (retain) NSString * LabourBookNo;
@property (retain) NSString * LabourBookPlaceOfIssue;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OtherInsuranceCompanyName;
@property (retain) NSDate * OtherInsuranceDateExpire;
@property (retain) NSDate * OtherInsuranceDateIssue;
@property (retain) NSString * OtherInsuranceNumber;
@property (retain) NSDate * SocialInsuranceDateIssue;
@property (retain) NSNumber * SocialInsuranceId;
@property (retain) NSNumber * SocialInsuranceJoiningBefore;
@property (retain) NSString * SocialInsuranceNumber;
@property (retain) NSDate * UnemploymentInsuranceDateIssue;
@property (retain) NSNumber * UnemploymentInsuranceId;
@property (retain) NSString * UnemploymentInsuranceNumber;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileBenefit : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileBenefit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileBenefit:(org_tns2_EmpProfileBenefit *)toAdd;
@property (readonly) NSMutableArray * EmpProfileBenefit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileComment : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSString * Content;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfileCommentId;
	org_tns2_Employee * Employee;
	org_tns2_Employee * Employee1;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * RelateRowId;
	NSString * ReviewerCode;
	NSNumber * ReviewerId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileComment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Content;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfileCommentId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) org_tns2_Employee * Employee1;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * ReviewerCode;
@property (retain) NSNumber * ReviewerId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileComment : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileComment;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileComment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileComment:(org_tns2_EmpProfileComment *)toAdd;
@property (readonly) NSMutableArray * EmpProfileComment;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileComputingSkill : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSString * ComputingSkillName;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	NSNumber * EmpProfileComputingSkillId;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsValid;
	NSDate * ModifiedDate;
	NSString * Other;
	NSString * PlaceIssue;
	NSNumber * RatingId;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileComputingSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ComputingSkillName;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) NSNumber * EmpProfileComputingSkillId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValid;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) NSNumber * RatingId;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileComputingSkill : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileComputingSkill;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileComputingSkill *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileComputingSkill:(org_tns2_EmpProfileComputingSkill *)toAdd;
@property (readonly) NSMutableArray * EmpProfileComputingSkill;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileContact : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSString * ContactAddress;
	NSNumber * ContactAddressId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * Email;
	NSNumber * EmpProfileContactId;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	NSString * Fax;
	NSString * HomePhone;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	NSString * MobileNumber;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * OfficePhone;
	NSString * PermanentAddress;
	NSNumber * PermanentAddressId;
	NSNumber * RelateRowId;
	NSString * TemporaryAddress;
	NSNumber * TemporaryAddressId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileContact *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ContactAddress;
@property (retain) NSNumber * ContactAddressId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Email;
@property (retain) NSNumber * EmpProfileContactId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * Fax;
@property (retain) NSString * HomePhone;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * MobileNumber;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OfficePhone;
@property (retain) NSString * PermanentAddress;
@property (retain) NSNumber * PermanentAddressId;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * TemporaryAddress;
@property (retain) NSNumber * TemporaryAddressId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileContact : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileContact;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileContact *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileContact:(org_tns2_EmpProfileContact *)toAdd;
@property (readonly) NSMutableArray * EmpProfileContact;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgDegree : NSObject {
	
/* elements */
	NSNumber * CLogMajorId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * Description;
	org_tns2_ArrayOfEmpProfileDegree * EmpProfileDegrees;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * OrgDegreeId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) org_tns2_ArrayOfEmpProfileDegree * EmpProfileDegrees;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * OrgDegreeId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileDegree : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DateExpire;
	NSDate * DateIssue;
	NSNumber * EmpProfileDegreeId;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsValId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	org_tns2_OrgDegree * OrgDegree;
	NSNumber * OrgDegreeId;
	NSNumber * OrgDegreeRankId;
	NSString * Other;
	NSString * PlaceIssue;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateExpire;
@property (retain) NSDate * DateIssue;
@property (retain) NSNumber * EmpProfileDegreeId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsValId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_OrgDegree * OrgDegree;
@property (retain) NSNumber * OrgDegreeId;
@property (retain) NSNumber * OrgDegreeRankId;
@property (retain) NSString * Other;
@property (retain) NSString * PlaceIssue;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileDegree : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileDegree;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileDegree:(org_tns2_EmpProfileDegree *)toAdd;
@property (readonly) NSMutableArray * EmpProfileDegree;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgDisciplineForUnit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgDisciplineForUnitId;
	NSNumber * OrgDisciplineId;
	org_tns2_OrgDisciplineMaster * OrgDisciplineMaster;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgDisciplineForUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgDisciplineForUnitId;
@property (retain) NSNumber * OrgDisciplineId;
@property (retain) org_tns2_OrgDisciplineMaster * OrgDisciplineMaster;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgDisciplineForUnit : NSObject {
	
/* elements */
	NSMutableArray *OrgDisciplineForUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgDisciplineForUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgDisciplineForUnit:(org_tns2_OrgDisciplineForUnit *)toAdd;
@property (readonly) NSMutableArray * OrgDisciplineForUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgDisciplineMaster : NSObject {
	
/* elements */
	NSNumber * ApproveId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSDate * DateOfIssue;
	NSString * DecisionNo;
	org_tns2_ArrayOfEmpProfileDiscipline * EmpProfileDisciplines;
	USBoolean * IsDeleted;
	USBoolean * IsForEmployee;
	NSDate * ModifiedDate;
	NSNumber * MonetaryValue;
	org_tns2_ArrayOfOrgDisciplineForUnit * OrgDisciplineForUnits;
	NSNumber * OrgDisciplineMasterId;
	NSString * OrgDisciplineTypeCode;
	USBoolean * Status;
	NSString * Title;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgDisciplineMaster *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproveId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSDate * DateOfIssue;
@property (retain) NSString * DecisionNo;
@property (retain) org_tns2_ArrayOfEmpProfileDiscipline * EmpProfileDisciplines;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsForEmployee;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * MonetaryValue;
@property (retain) org_tns2_ArrayOfOrgDisciplineForUnit * OrgDisciplineForUnits;
@property (retain) NSNumber * OrgDisciplineMasterId;
@property (retain) NSString * OrgDisciplineTypeCode;
@property (retain) USBoolean * Status;
@property (retain) NSString * Title;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileDiscipline : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfileDisciplineId;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	org_tns2_OrgDisciplineMaster * OrgDisciplineMaster;
	NSNumber * OrgDisciplineMasterId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileDiscipline *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfileDisciplineId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_OrgDisciplineMaster * OrgDisciplineMaster;
@property (retain) NSNumber * OrgDisciplineMasterId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileDiscipline : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileDiscipline;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileDiscipline *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileDiscipline:(org_tns2_EmpProfileDiscipline *)toAdd;
@property (readonly) NSMutableArray * EmpProfileDiscipline;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CLogEquipment : NSObject {
	
/* elements */
	NSDate * AddDate;
	NSString * CLogAssetsTypeCode;
	NSNumber * CLogCurrencyId;
	NSNumber * CLogEquipmentId;
	NSString * CLogEquipmentTypeCode;
	NSString * Code;
	NSNumber * CompanyId;
	NSNumber * Cost;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * Depreciation;
	org_tns2_ArrayOfEmpProfileEquipment * EmpProfileEquipments;
	USBoolean * IsBigValue;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * Note;
	NSNumber * Number;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CLogEquipment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * AddDate;
@property (retain) NSString * CLogAssetsTypeCode;
@property (retain) NSNumber * CLogCurrencyId;
@property (retain) NSNumber * CLogEquipmentId;
@property (retain) NSString * CLogEquipmentTypeCode;
@property (retain) NSString * Code;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * Cost;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * Depreciation;
@property (retain) org_tns2_ArrayOfEmpProfileEquipment * EmpProfileEquipments;
@property (retain) USBoolean * IsBigValue;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * Note;
@property (retain) NSNumber * Number;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileEquipment : NSObject {
	
/* elements */
	org_tns2_CLogEquipment * CLogEquipment;
	NSNumber * CLogEquipmentId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DateHandover;
	NSDate * DateReceived;
	NSString * Description;
	NSNumber * EmpProfileEquipmentId;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * HandoverStatus;
	USBoolean * IsDeleted;
	USBoolean * IsHandover;
	USBoolean * IsReturnToComp;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	USBoolean * MustHandover;
	NSNumber * Number;
	NSString * ReceiverCode;
	NSNumber * ReceiverId;
	NSNumber * RequestDetailId;
	NSString * StateHandover;
	NSString * StateReceived;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileEquipment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_CLogEquipment * CLogEquipment;
@property (retain) NSNumber * CLogEquipmentId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateHandover;
@property (retain) NSDate * DateReceived;
@property (retain) NSString * Description;
@property (retain) NSNumber * EmpProfileEquipmentId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * HandoverStatus;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsHandover;
@property (retain) USBoolean * IsReturnToComp;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) USBoolean * MustHandover;
@property (retain) NSNumber * Number;
@property (retain) NSString * ReceiverCode;
@property (retain) NSNumber * ReceiverId;
@property (retain) NSNumber * RequestDetailId;
@property (retain) NSString * StateHandover;
@property (retain) NSString * StateReceived;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileEquipment : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileEquipment;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileEquipment *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileEquipment:(org_tns2_EmpProfileEquipment *)toAdd;
@property (readonly) NSMutableArray * EmpProfileEquipment;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileExperience : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * AttachedDocumentUrl;
	NSString * Company;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * Customer;
	NSNumber * EmpProfileExperienceId;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * ProjectName;
	NSString * ProjectResult;
	NSNumber * RelateRowId;
	NSString * Roles;
	NSDate * StartDate;
	NSString * TeamSize;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * AttachedDocumentUrl;
@property (retain) NSString * Company;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Customer;
@property (retain) NSNumber * EmpProfileExperienceId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * ProjectName;
@property (retain) NSString * ProjectResult;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * Roles;
@property (retain) NSDate * StartDate;
@property (retain) NSString * TeamSize;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileExperience : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileExperience:(org_tns2_EmpProfileExperience *)toAdd;
@property (readonly) NSMutableArray * EmpProfileExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CLogLanguage : NSObject {
	
/* elements */
	NSNumber * CLogLanguageId;
	org_tns2_ArrayOfEmpProfileForeignLanguage * EmpProfileForeignLanguages;
	USBoolean * IsDeleted;
	NSString * Language;
	NSString * Name;
	NSString * NameEN;
	NSString * Note;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CLogLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogLanguageId;
@property (retain) org_tns2_ArrayOfEmpProfileForeignLanguage * EmpProfileForeignLanguages;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Language;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * Note;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileForeignLanguage : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * AttachmenttUrl;
	org_tns2_CLogLanguage * CLogLanguage;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DateOfIssue;
	NSString * DegreeName;
	NSNumber * EffectiveTime;
	NSNumber * EmpProfileForeignLanguageId;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * LanguageId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * OrgDegreeRankId;
	NSString * PlaceOfIssue;
	NSNumber * RelateRowId;
	NSString * TrainingCenter;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileForeignLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * AttachmenttUrl;
@property (retain) org_tns2_CLogLanguage * CLogLanguage;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DateOfIssue;
@property (retain) NSString * DegreeName;
@property (retain) NSNumber * EffectiveTime;
@property (retain) NSNumber * EmpProfileForeignLanguageId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * LanguageId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * OrgDegreeRankId;
@property (retain) NSString * PlaceOfIssue;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * TrainingCenter;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileForeignLanguage : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileForeignLanguage;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileForeignLanguage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileForeignLanguage:(org_tns2_EmpProfileForeignLanguage *)toAdd;
@property (readonly) NSMutableArray * EmpProfileForeignLanguage;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileHealthInsurance : NSObject {
	
/* elements */
	NSNumber * CLogHospitalId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * HealthInsuranceDateEffect;
	NSDate * HealthInsuranceDateExpire;
	NSNumber * HealthInsuranceId;
	NSString * HealthInsuranceNumber;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileHealthInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogHospitalId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * HealthInsuranceDateEffect;
@property (retain) NSDate * HealthInsuranceDateExpire;
@property (retain) NSNumber * HealthInsuranceId;
@property (retain) NSString * HealthInsuranceNumber;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileHealthInsurance : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileHealthInsurance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileHealthInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileHealthInsurance:(org_tns2_EmpProfileHealthInsurance *)toAdd;
@property (readonly) NSMutableArray * EmpProfileHealthInsurance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileHealthy : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * BloodGroup;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSDate * DiseaseAbsenceEndDay;
	NSDate * DiseaseAbsenceStartDay;
	NSNumber * DiseaseBenefit;
	NSString * DiseaseName;
	NSNumber * EmpProfileHealthyId;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	USBoolean * IsDiseaseAbsence;
	USBoolean * IsEnoughHealthToWork;
	USBoolean * IsGoodHealth;
	USBoolean * IsMaternityAbsence;
	USBoolean * IsMedicalHistory;
	USBoolean * IsPeopleWithDisability;
	NSDate * MaternityAbsenceEndDay;
	NSDate * MaternityAbsenceStartDay;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileHealthy *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * BloodGroup;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSDate * DiseaseAbsenceEndDay;
@property (retain) NSDate * DiseaseAbsenceStartDay;
@property (retain) NSNumber * DiseaseBenefit;
@property (retain) NSString * DiseaseName;
@property (retain) NSNumber * EmpProfileHealthyId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsDiseaseAbsence;
@property (retain) USBoolean * IsEnoughHealthToWork;
@property (retain) USBoolean * IsGoodHealth;
@property (retain) USBoolean * IsMaternityAbsence;
@property (retain) USBoolean * IsMedicalHistory;
@property (retain) USBoolean * IsPeopleWithDisability;
@property (retain) NSDate * MaternityAbsenceEndDay;
@property (retain) NSDate * MaternityAbsenceStartDay;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileHealthy : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileHealthy;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileHealthy *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileHealthy:(org_tns2_EmpProfileHealthy *)toAdd;
@property (readonly) NSMutableArray * EmpProfileHealthy;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CLogHobby : NSObject {
	
/* elements */
	NSNumber * CLogHobbyId;
	NSString * Description;
	org_tns2_ArrayOfEmpProfileHobby * EmpProfileHobbies;
	USBoolean * IsDeleted;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CLogHobby *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogHobbyId;
@property (retain) NSString * Description;
@property (retain) org_tns2_ArrayOfEmpProfileHobby * EmpProfileHobbies;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileHobby : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	org_tns2_CLogHobby * CLogHobby;
	NSNumber * CLogHobbyId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfileHobbyId;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileHobby *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) org_tns2_CLogHobby * CLogHobby;
@property (retain) NSNumber * CLogHobbyId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfileHobbyId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileHobby : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileHobby;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileHobby *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileHobby:(org_tns2_EmpProfileHobby *)toAdd;
@property (readonly) NSMutableArray * EmpProfileHobby;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileInternalCourse : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpInternalCourseId;
	NSNumber * EmpProfileInternalCourseId;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Result;
	NSNumber * Status;
	NSString * Support;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileInternalCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpInternalCourseId;
@property (retain) NSNumber * EmpProfileInternalCourseId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Result;
@property (retain) NSNumber * Status;
@property (retain) NSString * Support;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileInternalCourse : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileInternalCourse;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileInternalCourse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileInternalCourse:(org_tns2_EmpProfileInternalCourse *)toAdd;
@property (readonly) NSMutableArray * EmpProfileInternalCourse;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileLeaveRegime : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CompensatoryLeave;
	NSNumber * CompensatoryLeaveOfYear;
	NSNumber * CompensatoryRemainedLeave;
	NSNumber * CompensatoryUsedLeave;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileLeaveRegimeId;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * MonthlyLeave;
	NSNumber * PlusMinusLeave;
	NSNumber * PreviousLeave;
	NSNumber * RemainedLeave;
	NSNumber * SeniorityLeave;
	NSNumber * TotalUsedLeave;
	NSNumber * Year;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileLeaveRegime *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CompensatoryLeave;
@property (retain) NSNumber * CompensatoryLeaveOfYear;
@property (retain) NSNumber * CompensatoryRemainedLeave;
@property (retain) NSNumber * CompensatoryUsedLeave;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileLeaveRegimeId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * MonthlyLeave;
@property (retain) NSNumber * PlusMinusLeave;
@property (retain) NSNumber * PreviousLeave;
@property (retain) NSNumber * RemainedLeave;
@property (retain) NSNumber * SeniorityLeave;
@property (retain) NSNumber * TotalUsedLeave;
@property (retain) NSNumber * Year;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileLeaveRegime : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileLeaveRegime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileLeaveRegime *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileLeaveRegime:(org_tns2_EmpProfileLeaveRegime *)toAdd;
@property (readonly) NSMutableArray * EmpProfileLeaveRegime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpOtherBenefit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	NSNumber * EmpOtherBenefitId;
	org_tns2_ArrayOfEmpProfileOtherBenefit * EmpProfileOtherBenefits;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpOtherBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) NSNumber * EmpOtherBenefitId;
@property (retain) org_tns2_ArrayOfEmpProfileOtherBenefit * EmpProfileOtherBenefits;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileOtherBenefit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	org_tns2_EmpOtherBenefit * EmpOtherBenefit;
	NSNumber * EmpOtherBenefitId;
	NSNumber * EmpProfileOtherBenefitId;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileOtherBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_EmpOtherBenefit * EmpOtherBenefit;
@property (retain) NSNumber * EmpOtherBenefitId;
@property (retain) NSNumber * EmpProfileOtherBenefitId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileOtherBenefit : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileOtherBenefit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileOtherBenefit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileOtherBenefit:(org_tns2_EmpProfileOtherBenefit *)toAdd;
@property (readonly) NSMutableArray * EmpProfileOtherBenefit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileParticipation : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileParticipationId;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSDate * PartyFeeEndDate;
	NSNumber * PartyFeePercent;
	NSString * PartyFeeRole;
	NSDate * PartyFeeStartDate;
	NSNumber * RelateRowId;
	NSDate * UnionFeeEndDate;
	NSNumber * UnionFeePercent;
	NSString * UnionFeeRole;
	NSDate * UnionFeeStartDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileParticipation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileParticipationId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSDate * PartyFeeEndDate;
@property (retain) NSNumber * PartyFeePercent;
@property (retain) NSString * PartyFeeRole;
@property (retain) NSDate * PartyFeeStartDate;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSDate * UnionFeeEndDate;
@property (retain) NSNumber * UnionFeePercent;
@property (retain) NSString * UnionFeeRole;
@property (retain) NSDate * UnionFeeStartDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileParticipation : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileParticipation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileParticipation *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileParticipation:(org_tns2_EmpProfileParticipation *)toAdd;
@property (readonly) NSMutableArray * EmpProfileParticipation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CLogPersonality : NSObject {
	
/* elements */
	NSNumber * CLogPersonalityId;
	NSString * Description;
	org_tns2_ArrayOfEmpProfilePersonality * EmpProfilePersonalities;
	USBoolean * IsDeleted;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CLogPersonality *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogPersonalityId;
@property (retain) NSString * Description;
@property (retain) org_tns2_ArrayOfEmpProfilePersonality * EmpProfilePersonalities;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfilePersonality : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	org_tns2_CLogPersonality * CLogPersonality;
	NSNumber * CLogPersonalityId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfilePersonalityId;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSNumber * RelateRowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfilePersonality *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) org_tns2_CLogPersonality * CLogPersonality;
@property (retain) NSNumber * CLogPersonalityId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfilePersonalityId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfilePersonality : NSObject {
	
/* elements */
	NSMutableArray *EmpProfilePersonality;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfilePersonality *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfilePersonality:(org_tns2_EmpProfilePersonality *)toAdd;
@property (readonly) NSMutableArray * EmpProfilePersonality;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileProcessOfWork : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * DecisionFile;
	NSString * DecisionName;
	NSString * DecisionNo;
	NSNumber * EmpProfileJobPositionId;
	NSNumber * EmpProfileProcessOfWorkId;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ExpirationDate;
	NSDate * ImplementationDate;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * OrgUnitAddress;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgUnitName;
	NSString * OrgUnitNameEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileProcessOfWork *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DecisionFile;
@property (retain) NSString * DecisionName;
@property (retain) NSString * DecisionNo;
@property (retain) NSNumber * EmpProfileJobPositionId;
@property (retain) NSNumber * EmpProfileProcessOfWorkId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OrgUnitAddress;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitName;
@property (retain) NSString * OrgUnitNameEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileProcessOfWork : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileProcessOfWork;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileProcessOfWork *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileProcessOfWork:(org_tns2_EmpProfileProcessOfWork *)toAdd;
@property (readonly) NSMutableArray * EmpProfileProcessOfWork;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgRewardForUnit : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgRewardForUnitId;
	NSNumber * OrgRewardId;
	org_tns2_OrgRewardMaster * OrgRewardMaster;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgRewardForUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgRewardForUnitId;
@property (retain) NSNumber * OrgRewardId;
@property (retain) org_tns2_OrgRewardMaster * OrgRewardMaster;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgRewardForUnit : NSObject {
	
/* elements */
	NSMutableArray *OrgRewardForUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgRewardForUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgRewardForUnit:(org_tns2_OrgRewardForUnit *)toAdd;
@property (readonly) NSMutableArray * OrgRewardForUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgRewardMaster : NSObject {
	
/* elements */
	NSNumber * ApproveId;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSDate * DateOfIssue;
	NSString * DecisionNo;
	org_tns2_ArrayOfEmpProfileReward * EmpProfileRewards;
	USBoolean * IsAllCompany;
	USBoolean * IsDeleted;
	USBoolean * IsForEmployee;
	NSDate * ModifiedDate;
	NSNumber * MonetaryValue;
	org_tns2_ArrayOfOrgRewardForUnit * OrgRewardForUnits;
	NSNumber * OrgRewardMasterId;
	NSString * OrgRewardTypeCode;
	USBoolean * Status;
	NSString * Title;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgRewardMaster *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproveId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSDate * DateOfIssue;
@property (retain) NSString * DecisionNo;
@property (retain) org_tns2_ArrayOfEmpProfileReward * EmpProfileRewards;
@property (retain) USBoolean * IsAllCompany;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsForEmployee;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * MonetaryValue;
@property (retain) org_tns2_ArrayOfOrgRewardForUnit * OrgRewardForUnits;
@property (retain) NSNumber * OrgRewardMasterId;
@property (retain) NSString * OrgRewardTypeCode;
@property (retain) USBoolean * Status;
@property (retain) NSString * Title;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileReward : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSNumber * CreatedBy;
	NSNumber * EmpProfileRewardId;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	org_tns2_OrgRewardMaster * OrgRewardMaster;
	NSNumber * OrgRewardMasterId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileReward *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSNumber * EmpProfileRewardId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_OrgRewardMaster * OrgRewardMaster;
@property (retain) NSNumber * OrgRewardMasterId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileReward : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileReward;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileReward *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileReward:(org_tns2_EmpProfileReward *)toAdd;
@property (readonly) NSMutableArray * EmpProfileReward;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileSocialInsurance : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsLostBook;
	NSDate * LabourBookDateOfIssue;
	NSString * LabourBookNo;
	NSString * LabourBookPlaceOfIssue;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * OtherInsuranceCompanyName;
	NSDate * OtherInsuranceDateEffect;
	NSDate * OtherInsuranceDateExpire;
	NSString * OtherInsuranceNumber;
	NSDate * SocialInsuranceDateIssue;
	NSNumber * SocialInsuranceId;
	NSNumber * SocialInsuranceJoiningBefore;
	NSString * SocialInsuranceNumber;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileSocialInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsLostBook;
@property (retain) NSDate * LabourBookDateOfIssue;
@property (retain) NSString * LabourBookNo;
@property (retain) NSString * LabourBookPlaceOfIssue;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OtherInsuranceCompanyName;
@property (retain) NSDate * OtherInsuranceDateEffect;
@property (retain) NSDate * OtherInsuranceDateExpire;
@property (retain) NSString * OtherInsuranceNumber;
@property (retain) NSDate * SocialInsuranceDateIssue;
@property (retain) NSNumber * SocialInsuranceId;
@property (retain) NSNumber * SocialInsuranceJoiningBefore;
@property (retain) NSString * SocialInsuranceNumber;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileSocialInsurance : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileSocialInsurance;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileSocialInsurance *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileSocialInsurance:(org_tns2_EmpProfileSocialInsurance *)toAdd;
@property (readonly) NSMutableArray * EmpProfileSocialInsurance;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileTotalIncome : NSObject {
	
/* elements */
	NSDate * CompanyId;
	NSDate * CreatedDate;
	NSNumber * DependancyDeduction;
	NSNumber * EmpProfileTotalIncomeId;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	NSNumber * GrossPayment;
	NSDate * ModifiedDate;
	NSNumber * Month;
	NSNumber * NetPayment;
	NSNumber * PIT;
	NSNumber * TaxableAmount;
	NSNumber * TotalDeduction;
	NSNumber * Year;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileTotalIncome *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * DependancyDeduction;
@property (retain) NSNumber * EmpProfileTotalIncomeId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * GrossPayment;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * Month;
@property (retain) NSNumber * NetPayment;
@property (retain) NSNumber * PIT;
@property (retain) NSNumber * TaxableAmount;
@property (retain) NSNumber * TotalDeduction;
@property (retain) NSNumber * Year;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileTotalIncome : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileTotalIncome;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileTotalIncome *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileTotalIncome:(org_tns2_EmpProfileTotalIncome *)toAdd;
@property (readonly) NSMutableArray * EmpProfileTotalIncome;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileTraining : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	NSString * AttachedDocumentUrl;
	NSString * Certifications;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileTrainingId;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	NSDate * ExpiredDate;
	USBoolean * IsDeleted;
	USBoolean * IsInternal;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSNumber * RelateRowId;
	NSString * Result;
	NSDate * StartDate;
	NSNumber * Status;
	NSString * TrainingProgram;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileTraining *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) NSString * AttachedDocumentUrl;
@property (retain) NSString * Certifications;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileTrainingId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) NSDate * ExpiredDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInternal;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * Result;
@property (retain) NSDate * StartDate;
@property (retain) NSNumber * Status;
@property (retain) NSString * TrainingProgram;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileTraining : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileTraining;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileTraining *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileTraining:(org_tns2_EmpProfileTraining *)toAdd;
@property (readonly) NSMutableArray * EmpProfileTraining;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileWageType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileWageTypeId;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ImplementationDate;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * TimeWageTypeCode;
	NSNumber * TimeWageTypeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileWageType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileWageTypeId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * TimeWageTypeCode;
@property (retain) NSNumber * TimeWageTypeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileWageType : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileWageType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileWageType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileWageType:(org_tns2_EmpProfileWageType *)toAdd;
@property (readonly) NSMutableArray * EmpProfileWageType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileWorkingExperience : NSObject {
	
/* elements */
	NSString * ApproveNote;
	NSNumber * ApproveStatus;
	NSNumber * ApproverId;
	org_tns2_CLogRating * CLogRating;
	NSNumber * CompanyId;
	NSString * CompanyName;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileWorkingExperienceId;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	USBoolean * IsDeleted;
	USBoolean * IsInternal;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * NotableAchievements;
	NSString * Note;
	NSNumber * OrgProjectTypeId;
	NSNumber * OrgTimeInChargeId;
	NSString * ProjectName;
	NSNumber * RatingId;
	NSNumber * RelateRowId;
	NSString * Responsibilities;
	NSString * Role;
	NSDate * StartDate;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileWorkingExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ApproveNote;
@property (retain) NSNumber * ApproveStatus;
@property (retain) NSNumber * ApproverId;
@property (retain) org_tns2_CLogRating * CLogRating;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompanyName;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileWorkingExperienceId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsInternal;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * NotableAchievements;
@property (retain) NSString * Note;
@property (retain) NSNumber * OrgProjectTypeId;
@property (retain) NSNumber * OrgTimeInChargeId;
@property (retain) NSString * ProjectName;
@property (retain) NSNumber * RatingId;
@property (retain) NSNumber * RelateRowId;
@property (retain) NSString * Responsibilities;
@property (retain) NSString * Role;
@property (retain) NSDate * StartDate;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileWorkingExperience : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileWorkingExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileWorkingExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileWorkingExperience:(org_tns2_EmpProfileWorkingExperience *)toAdd;
@property (readonly) NSMutableArray * EmpProfileWorkingExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpProfileWorkingForm : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSNumber * EmpProfileWorkingFormId;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ImplementationDate;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * TimeWorkingFormCode;
	NSNumber * TimeWorkingFormId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpProfileWorkingForm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpProfileWorkingFormId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * TimeWorkingFormCode;
@property (retain) NSNumber * TimeWorkingFormId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpProfileWorkingForm : NSObject {
	
/* elements */
	NSMutableArray *EmpProfileWorkingForm;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpProfileWorkingForm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpProfileWorkingForm:(org_tns2_EmpProfileWorkingForm *)toAdd;
@property (readonly) NSMutableArray * EmpProfileWorkingForm;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpSocialInsuranceSalary : NSObject {
	
/* elements */
	NSNumber * CLogCurrencyId;
	NSNumber * CLogCurrencyRateId;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * DecisionCode;
	NSString * DecisionName;
	NSNumber * EmpSocialInsuranceSalaryId;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	NSDate * ExpirationDate;
	NSString * FileDecisionUrl;
	NSDate * ImplementationDate;
	USBoolean * IsCurrent;
	USBoolean * IsDeleted;
	USBoolean * IsIncrease;
	USBoolean * IsRecall;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Note;
	NSString * Reason;
	NSDate * RecallMonth;
	NSDate * SocialInsuranceDate;
	NSString * cEmpSISalary;
	NSString * cEmpSISalaryCoeficient;
	NSString * cEmpSISalaryGrade;
	NSNumber * nEmpSISalary;
	NSNumber * nEmpSISalaryCoeficient;
	NSNumber * nEmpSISalaryGrade;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpSocialInsuranceSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogCurrencyId;
@property (retain) NSNumber * CLogCurrencyRateId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DecisionCode;
@property (retain) NSString * DecisionName;
@property (retain) NSNumber * EmpSocialInsuranceSalaryId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSString * FileDecisionUrl;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsCurrent;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsIncrease;
@property (retain) USBoolean * IsRecall;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Note;
@property (retain) NSString * Reason;
@property (retain) NSDate * RecallMonth;
@property (retain) NSDate * SocialInsuranceDate;
@property (retain) NSString * cEmpSISalary;
@property (retain) NSString * cEmpSISalaryCoeficient;
@property (retain) NSString * cEmpSISalaryGrade;
@property (retain) NSNumber * nEmpSISalary;
@property (retain) NSNumber * nEmpSISalaryCoeficient;
@property (retain) NSNumber * nEmpSISalaryGrade;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpSocialInsuranceSalary : NSObject {
	
/* elements */
	NSMutableArray *EmpSocialInsuranceSalary;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpSocialInsuranceSalary *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpSocialInsuranceSalary:(org_tns2_EmpSocialInsuranceSalary *)toAdd;
@property (readonly) NSMutableArray * EmpSocialInsuranceSalary;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_NCClientConnection : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * ConnectionId;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	NSNumber * NCClientConnectionId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_NCClientConnection *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * ConnectionId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * NCClientConnectionId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfNCClientConnection : NSObject {
	
/* elements */
	NSMutableArray *NCClientConnection;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfNCClientConnection *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addNCClientConnection:(org_tns2_NCClientConnection *)toAdd;
@property (readonly) NSMutableArray * NCClientConnection;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_NCMessageType : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * NCMessageTypeId;
	org_tns2_ArrayOfNCMessage * NCMessages;
	NSString * Name;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_NCMessageType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NCMessageTypeId;
@property (retain) org_tns2_ArrayOfNCMessage * NCMessages;
@property (retain) NSString * Name;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_NCMessage : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	org_tns2_Employee * Employee1;
	NSString * FullMessage;
	USBoolean * IsDeleted;
	USBoolean * IsNew;
	NSDate * ModifiedDate;
	NSNumber * NCMessageId;
	org_tns2_NCMessageType * NCMessageType;
	NSNumber * NCMessageTypeId;
	NSNumber * ReceiverEmployeeId;
	NSNumber * SenderEmployeeId;
	NSString * ShortcutMessage;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_NCMessage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) org_tns2_Employee * Employee1;
@property (retain) NSString * FullMessage;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsNew;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NCMessageId;
@property (retain) org_tns2_NCMessageType * NCMessageType;
@property (retain) NSNumber * NCMessageTypeId;
@property (retain) NSNumber * ReceiverEmployeeId;
@property (retain) NSNumber * SenderEmployeeId;
@property (retain) NSString * ShortcutMessage;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfNCMessage : NSObject {
	
/* elements */
	NSMutableArray *NCMessage;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfNCMessage *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addNCMessage:(org_tns2_NCMessage *)toAdd;
@property (readonly) NSMutableArray * NCMessage;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_Project : NSObject {
	
/* elements */
	NSString * Description;
	org_tns2_Employee * Employee;
	NSDate * EndDate;
	NSNumber * Id_;
	NSString * Name;
	NSNumber * OwnerId;
	NSNumber * Progress;
	org_tns2_ArrayOfProjectMember * ProjectMembers;
	NSDate * StartDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_Project *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Description;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSDate * EndDate;
@property (retain) NSNumber * Id_;
@property (retain) NSString * Name;
@property (retain) NSNumber * OwnerId;
@property (retain) NSNumber * Progress;
@property (retain) org_tns2_ArrayOfProjectMember * ProjectMembers;
@property (retain) NSDate * StartDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ProjectMember : NSObject {
	
/* elements */
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	NSNumber * Id_;
	org_tns2_Project * Project;
	NSNumber * ProjectId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ProjectMember *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * Id_;
@property (retain) org_tns2_Project * Project;
@property (retain) NSNumber * ProjectId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfProjectMember : NSObject {
	
/* elements */
	NSMutableArray *ProjectMember;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfProjectMember *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addProjectMember:(org_tns2_ProjectMember *)toAdd;
@property (readonly) NSMutableArray * ProjectMember;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfProject : NSObject {
	
/* elements */
	NSMutableArray *Project;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfProject *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addProject:(org_tns2_Project *)toAdd;
@property (readonly) NSMutableArray * Project;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ReportEmployeeRequestedBook : NSObject {
	
/* elements */
	NSString * Address1;
	NSString * Benefit;
	NSDate * BirthDay;
	NSString * CLogCityNoIns;
	NSString * CLogHospitalCode;
	org_tns2_Employee * Employee;
	NSString * Ethnicity;
	NSString * FullName;
	NSString * Gender;
	USBoolean * HadSIBook;
	NSDate * IdentityCardDateOfIssue;
	NSString * IdentityCardNo;
	NSString * IdentityCardPlaceOfIssue;
	NSString * Note;
	NSString * OrgUnitName;
	NSNumber * ReportEmployeeRequestedBookId;
	NSDate * RequestedDay;
	NSString * SICode;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ReportEmployeeRequestedBook *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address1;
@property (retain) NSString * Benefit;
@property (retain) NSDate * BirthDay;
@property (retain) NSString * CLogCityNoIns;
@property (retain) NSString * CLogHospitalCode;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * Ethnicity;
@property (retain) NSString * FullName;
@property (retain) NSString * Gender;
@property (retain) USBoolean * HadSIBook;
@property (retain) NSDate * IdentityCardDateOfIssue;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * IdentityCardPlaceOfIssue;
@property (retain) NSString * Note;
@property (retain) NSString * OrgUnitName;
@property (retain) NSNumber * ReportEmployeeRequestedBookId;
@property (retain) NSDate * RequestedDay;
@property (retain) NSString * SICode;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_SelfLeaveRequest : NSObject {
	
/* elements */
	NSString * Address;
	NSNumber * ApprovalStatus;
	NSString * ApproverCode;
	NSNumber * ApproverId;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * EndDate;
	USBoolean * IsCompensatoryLeave;
	USBoolean * IsDeleted;
	USBoolean * IsLeaveWithoutPay;
	USBoolean * IsPaidLeave;
	USBoolean * IsUnpaidLeave;
	NSDate * ModifiedDate;
	NSNumber * NumberDaysOff;
	NSNumber * OrganizationId;
	NSString * PhoneNumber;
	NSString * Reason;
	NSNumber * SalaryPenaltyValue;
	NSNumber * SelfLeaveRequestId;
	NSDate * StartDate;
	NSNumber * TimeAbsenceTypeId;
	NSString * WorkflowId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_SelfLeaveRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSNumber * ApprovalStatus;
@property (retain) NSString * ApproverCode;
@property (retain) NSNumber * ApproverId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * EndDate;
@property (retain) USBoolean * IsCompensatoryLeave;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsLeaveWithoutPay;
@property (retain) USBoolean * IsPaidLeave;
@property (retain) USBoolean * IsUnpaidLeave;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * NumberDaysOff;
@property (retain) NSNumber * OrganizationId;
@property (retain) NSString * PhoneNumber;
@property (retain) NSString * Reason;
@property (retain) NSNumber * SalaryPenaltyValue;
@property (retain) NSNumber * SelfLeaveRequestId;
@property (retain) NSDate * StartDate;
@property (retain) NSNumber * TimeAbsenceTypeId;
@property (retain) NSString * WorkflowId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfSelfLeaveRequest : NSObject {
	
/* elements */
	NSMutableArray *SelfLeaveRequest;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfSelfLeaveRequest *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSelfLeaveRequest:(org_tns2_SelfLeaveRequest *)toAdd;
@property (readonly) NSMutableArray * SelfLeaveRequest;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfSysRecPlanApprover : NSObject {
	
/* elements */
	NSMutableArray *SysRecPlanApprover;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfSysRecPlanApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysRecPlanApprover:(org_tns2_SysRecPlanApprover *)toAdd;
@property (readonly) NSMutableArray * SysRecPlanApprover;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfSysRecRequirementApprover : NSObject {
	
/* elements */
	NSMutableArray *SysRecRequirementApprover;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfSysRecRequirementApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysRecRequirementApprover:(org_tns2_SysRecRequirementApprover *)toAdd;
@property (readonly) NSMutableArray * SysRecRequirementApprover;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_TrainingEmpPlan : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	USBoolean * IsSubmited;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * State;
	NSNumber * StrategyGoalId;
	NSString * Target;
	org_tns2_ArrayOfTrainingEmpPlanApproved * TrainingEmpPlanApproveds;
	NSNumber * TrainingEmpPlanId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_TrainingEmpPlan *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsSubmited;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * State;
@property (retain) NSNumber * StrategyGoalId;
@property (retain) NSString * Target;
@property (retain) org_tns2_ArrayOfTrainingEmpPlanApproved * TrainingEmpPlanApproveds;
@property (retain) NSNumber * TrainingEmpPlanId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_TrainingEmpPlanApproved : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	org_tns2_SysTrainingApprover * SysTrainingApprover;
	NSNumber * SysTrainingApproverId;
	org_tns2_TrainingEmpPlan * TrainingEmpPlan;
	NSNumber * TrainingEmpPlanApprovedId;
	NSNumber * TrainingEmpPlanId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_TrainingEmpPlanApproved *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_SysTrainingApprover * SysTrainingApprover;
@property (retain) NSNumber * SysTrainingApproverId;
@property (retain) org_tns2_TrainingEmpPlan * TrainingEmpPlan;
@property (retain) NSNumber * TrainingEmpPlanApprovedId;
@property (retain) NSNumber * TrainingEmpPlanId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfTrainingEmpPlanApproved : NSObject {
	
/* elements */
	NSMutableArray *TrainingEmpPlanApproved;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfTrainingEmpPlanApproved *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingEmpPlanApproved:(org_tns2_TrainingEmpPlanApproved *)toAdd;
@property (readonly) NSMutableArray * TrainingEmpPlanApproved;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_SysTrainingApprover : NSObject {
	
/* elements */
	NSNumber * ApproverId;
	NSNumber * CompanyId;
	org_tns2_Employee * Employee;
	USBoolean * IsDeleted;
	NSNumber * Number;
	NSNumber * SysTrainingApproverId;
	org_tns2_ArrayOfTrainingEmpPlanApproved * TrainingEmpPlanApproveds;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_SysTrainingApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * ApproverId;
@property (retain) NSNumber * CompanyId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * Number;
@property (retain) NSNumber * SysTrainingApproverId;
@property (retain) org_tns2_ArrayOfTrainingEmpPlanApproved * TrainingEmpPlanApproveds;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfSysTrainingApprover : NSObject {
	
/* elements */
	NSMutableArray *SysTrainingApprover;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfSysTrainingApprover *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSysTrainingApprover:(org_tns2_SysTrainingApprover *)toAdd;
@property (readonly) NSMutableArray * SysTrainingApprover;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_Task : NSObject {
	
/* elements */
	NSString * Description;
	org_tns2_Employee * Employee;
	NSDate * FromDate;
	NSNumber * Id_;
	NSString * Name;
	NSNumber * Process;
	NSNumber * ProjectId;
	NSNumber * Status;
	NSNumber * TaskOwnerId;
	NSDate * ToDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_Task *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Description;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSDate * FromDate;
@property (retain) NSNumber * Id_;
@property (retain) NSString * Name;
@property (retain) NSNumber * Process;
@property (retain) NSNumber * ProjectId;
@property (retain) NSNumber * Status;
@property (retain) NSNumber * TaskOwnerId;
@property (retain) NSDate * ToDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfTask : NSObject {
	
/* elements */
	NSMutableArray *Task;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfTask *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTask:(org_tns2_Task *)toAdd;
@property (readonly) NSMutableArray * Task;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfTrainingEmpPlan : NSObject {
	
/* elements */
	NSMutableArray *TrainingEmpPlan;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfTrainingEmpPlan *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTrainingEmpPlan:(org_tns2_TrainingEmpPlan *)toAdd;
@property (readonly) NSMutableArray * TrainingEmpPlan;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_Employee : NSObject {
	
/* elements */
	NSDate * BirthDay;
	NSString * BirthPlace;
	NSNumber * BirthPlaceId;
	org_tns2_ArrayOfCBAccidentInsurance * CBAccidentInsurances;
	org_tns2_ArrayOfCBCompensationEmployeeGroupDetail * CBCompensationEmployeeGroupDetails;
	org_tns2_ArrayOfCBConvalescence * CBConvalescences;
	org_tns2_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
	org_tns2_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
	org_tns2_ArrayOfCBHealthInsuranceDetail * CBHealthInsuranceDetails;
	org_tns2_CLogCity * CLogCity;
	org_tns2_CLogCity * CLogCity1;
	org_tns2_CLogEmployeeType * CLogEmployeeType;
	org_tns2_ArrayOfCLogTrainer * CLogTrainers;
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	org_tns2_ArrayOfEmpBasicProfile * EmpBasicProfiles;
	org_tns2_ArrayOfEmpCompetency * EmpCompetencies;
	org_tns2_ArrayOfEmpCompetencyRating * EmpCompetencyRatings;
	org_tns2_ArrayOfEmpContract * EmpContracts;
	org_tns2_ArrayOfEmpPerformanceAppraisal * EmpPerformanceAppraisals;
	org_tns2_ArrayOfEmpProfileAllowance * EmpProfileAllowances;
	org_tns2_ArrayOfEmpProfileBaseSalary * EmpProfileBaseSalaries;
	org_tns2_ArrayOfEmpProfileBenefit * EmpProfileBenefits;
	org_tns2_ArrayOfEmpProfileComment * EmpProfileComments;
	org_tns2_ArrayOfEmpProfileComment * EmpProfileComments1;
	org_tns2_ArrayOfEmpProfileComputingSkill * EmpProfileComputingSkills;
	org_tns2_ArrayOfEmpProfileContact * EmpProfileContacts;
	org_tns2_ArrayOfEmpProfileDegree * EmpProfileDegrees;
	org_tns2_ArrayOfEmpProfileDiscipline * EmpProfileDisciplines;
	org_tns2_ArrayOfEmpProfileEducation * EmpProfileEducations;
	org_tns2_ArrayOfEmpProfileEquipment * EmpProfileEquipments;
	org_tns2_ArrayOfEmpProfileExperience * EmpProfileExperiences;
	org_tns2_ArrayOfEmpProfileFamilyRelationship * EmpProfileFamilyRelationships;
	org_tns2_ArrayOfEmpProfileForeignLanguage * EmpProfileForeignLanguages;
	org_tns2_ArrayOfEmpProfileHealthInsurance * EmpProfileHealthInsurances;
	org_tns2_ArrayOfEmpProfileHealthy * EmpProfileHealthies;
	org_tns2_ArrayOfEmpProfileHobby * EmpProfileHobbies;
	org_tns2_ArrayOfEmpProfileInternalCourse * EmpProfileInternalCourses;
	org_tns2_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions;
	org_tns2_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions1;
	org_tns2_ArrayOfEmpProfileLeaveRegime * EmpProfileLeaveRegimes;
	org_tns2_ArrayOfEmpProfileOtherBenefit * EmpProfileOtherBenefits;
	org_tns2_ArrayOfEmpProfileParticipation * EmpProfileParticipations;
	org_tns2_ArrayOfEmpProfilePersonality * EmpProfilePersonalities;
	org_tns2_ArrayOfEmpProfileProcessOfWork * EmpProfileProcessOfWorks;
	org_tns2_ArrayOfEmpProfileQualification * EmpProfileQualifications;
	org_tns2_ArrayOfEmpProfileReward * EmpProfileRewards;
	org_tns2_ArrayOfEmpProfileSkill * EmpProfileSkills;
	org_tns2_ArrayOfEmpProfileSocialInsurance * EmpProfileSocialInsurances;
	org_tns2_ArrayOfEmpProfileTotalIncome * EmpProfileTotalIncomes;
	org_tns2_ArrayOfEmpProfileTraining * EmpProfileTrainings;
	org_tns2_ArrayOfEmpProfileWageType * EmpProfileWageTypes;
	org_tns2_ArrayOfEmpProfileWorkingExperience * EmpProfileWorkingExperiences;
	org_tns2_ArrayOfEmpProfileWorkingForm * EmpProfileWorkingForms;
	org_tns2_ArrayOfEmpSocialInsuranceSalary * EmpSocialInsuranceSalaries;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSString * EmployeeTKCode;
	NSNumber * EmployeeTypeId;
	NSDate * EndWorkingDate;
	NSDate * EntryDate;
	NSString * Ethnicity;
	NSNumber * EthnicityId;
	NSString * FirstName;
	NSString * FullName;
	org_tns2_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals;
	org_tns2_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals1;
	org_tns2_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
	org_tns2_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
	org_tns2_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
	org_tns2_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
	org_tns2_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
	org_tns2_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives1;
	org_tns2_ArrayOfGPObjectiveInitiative * GPObjectiveInitiatives;
	org_tns2_ArrayOfGPPerformanceYearEndResult * GPPerformanceYearEndResults;
	USBoolean * Gender;
	NSString * HomeTown;
	NSNumber * HomeTownId;
	NSString * IdentityCardNo;
	NSString * ImageUrl;
	USBoolean * IsDeleted;
	USBoolean * IsOnlyYearOfBirthday;
	org_tns2_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
	org_tns2_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
	org_tns2_ArrayOfLMSCourseTranscript * LMSCourseTranscripts1;
	NSString * LastName;
	NSString * MainLanguage;
	NSNumber * MainLanguageId;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	org_tns2_ArrayOfNCClientConnection * NCClientConnections;
	org_tns2_ArrayOfNCMessage * NCMessages;
	org_tns2_ArrayOfNCMessage * NCMessages1;
	NSDate * OfficialDate;
	NSString * OrgGroupCd;
	NSNumber * OrgGroupId;
	NSDate * ProbationaryDate;
	NSDate * ProbationaryEndDate;
	org_tns2_ArrayOfProjectMember * ProjectMembers;
	org_tns2_ArrayOfProject * Projects;
	org_tns2_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
	org_tns2_ArrayOfRecInterviewer * RecInterviewers;
	org_tns2_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
	org_tns2_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
	org_tns2_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
	NSString * Religion;
	NSNumber * ReligionId;
	org_tns2_ReportEmployeeRequestedBook * ReportEmployeeRequestedBook;
	org_tns2_ArrayOfSelfLeaveRequest * SelfLeaveRequests;
	org_tns2_ArrayOfSysRecPlanApprover * SysRecPlanApprovers;
	org_tns2_ArrayOfSysRecRequirementApprover * SysRecRequirementApprovers;
	org_tns2_ArrayOfSysTrainingApprover * SysTrainingApprovers;
	org_tns2_ArrayOfTask * Tasks;
	org_tns2_ArrayOfTrainingCourseEmployee * TrainingCourseEmployees;
	org_tns2_ArrayOfTrainingEmpPlan * TrainingEmpPlans;
	org_tns2_ArrayOfTrainingPlanEmployee * TrainingPlanEmployees;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_Employee *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * BirthDay;
@property (retain) NSString * BirthPlace;
@property (retain) NSNumber * BirthPlaceId;
@property (retain) org_tns2_ArrayOfCBAccidentInsurance * CBAccidentInsurances;
@property (retain) org_tns2_ArrayOfCBCompensationEmployeeGroupDetail * CBCompensationEmployeeGroupDetails;
@property (retain) org_tns2_ArrayOfCBConvalescence * CBConvalescences;
@property (retain) org_tns2_ArrayOfCBDayOffSocialInsurance * CBDayOffSocialInsurances;
@property (retain) org_tns2_ArrayOfCBFactorEmployeeMetaData * CBFactorEmployeeMetaDatas;
@property (retain) org_tns2_ArrayOfCBHealthInsuranceDetail * CBHealthInsuranceDetails;
@property (retain) org_tns2_CLogCity * CLogCity;
@property (retain) org_tns2_CLogCity * CLogCity1;
@property (retain) org_tns2_CLogEmployeeType * CLogEmployeeType;
@property (retain) org_tns2_ArrayOfCLogTrainer * CLogTrainers;
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) org_tns2_ArrayOfEmpBasicProfile * EmpBasicProfiles;
@property (retain) org_tns2_ArrayOfEmpCompetency * EmpCompetencies;
@property (retain) org_tns2_ArrayOfEmpCompetencyRating * EmpCompetencyRatings;
@property (retain) org_tns2_ArrayOfEmpContract * EmpContracts;
@property (retain) org_tns2_ArrayOfEmpPerformanceAppraisal * EmpPerformanceAppraisals;
@property (retain) org_tns2_ArrayOfEmpProfileAllowance * EmpProfileAllowances;
@property (retain) org_tns2_ArrayOfEmpProfileBaseSalary * EmpProfileBaseSalaries;
@property (retain) org_tns2_ArrayOfEmpProfileBenefit * EmpProfileBenefits;
@property (retain) org_tns2_ArrayOfEmpProfileComment * EmpProfileComments;
@property (retain) org_tns2_ArrayOfEmpProfileComment * EmpProfileComments1;
@property (retain) org_tns2_ArrayOfEmpProfileComputingSkill * EmpProfileComputingSkills;
@property (retain) org_tns2_ArrayOfEmpProfileContact * EmpProfileContacts;
@property (retain) org_tns2_ArrayOfEmpProfileDegree * EmpProfileDegrees;
@property (retain) org_tns2_ArrayOfEmpProfileDiscipline * EmpProfileDisciplines;
@property (retain) org_tns2_ArrayOfEmpProfileEducation * EmpProfileEducations;
@property (retain) org_tns2_ArrayOfEmpProfileEquipment * EmpProfileEquipments;
@property (retain) org_tns2_ArrayOfEmpProfileExperience * EmpProfileExperiences;
@property (retain) org_tns2_ArrayOfEmpProfileFamilyRelationship * EmpProfileFamilyRelationships;
@property (retain) org_tns2_ArrayOfEmpProfileForeignLanguage * EmpProfileForeignLanguages;
@property (retain) org_tns2_ArrayOfEmpProfileHealthInsurance * EmpProfileHealthInsurances;
@property (retain) org_tns2_ArrayOfEmpProfileHealthy * EmpProfileHealthies;
@property (retain) org_tns2_ArrayOfEmpProfileHobby * EmpProfileHobbies;
@property (retain) org_tns2_ArrayOfEmpProfileInternalCourse * EmpProfileInternalCourses;
@property (retain) org_tns2_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions;
@property (retain) org_tns2_ArrayOfEmpProfileJobPosition * EmpProfileJobPositions1;
@property (retain) org_tns2_ArrayOfEmpProfileLeaveRegime * EmpProfileLeaveRegimes;
@property (retain) org_tns2_ArrayOfEmpProfileOtherBenefit * EmpProfileOtherBenefits;
@property (retain) org_tns2_ArrayOfEmpProfileParticipation * EmpProfileParticipations;
@property (retain) org_tns2_ArrayOfEmpProfilePersonality * EmpProfilePersonalities;
@property (retain) org_tns2_ArrayOfEmpProfileProcessOfWork * EmpProfileProcessOfWorks;
@property (retain) org_tns2_ArrayOfEmpProfileQualification * EmpProfileQualifications;
@property (retain) org_tns2_ArrayOfEmpProfileReward * EmpProfileRewards;
@property (retain) org_tns2_ArrayOfEmpProfileSkill * EmpProfileSkills;
@property (retain) org_tns2_ArrayOfEmpProfileSocialInsurance * EmpProfileSocialInsurances;
@property (retain) org_tns2_ArrayOfEmpProfileTotalIncome * EmpProfileTotalIncomes;
@property (retain) org_tns2_ArrayOfEmpProfileTraining * EmpProfileTrainings;
@property (retain) org_tns2_ArrayOfEmpProfileWageType * EmpProfileWageTypes;
@property (retain) org_tns2_ArrayOfEmpProfileWorkingExperience * EmpProfileWorkingExperiences;
@property (retain) org_tns2_ArrayOfEmpProfileWorkingForm * EmpProfileWorkingForms;
@property (retain) org_tns2_ArrayOfEmpSocialInsuranceSalary * EmpSocialInsuranceSalaries;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSString * EmployeeTKCode;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSDate * EndWorkingDate;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * Ethnicity;
@property (retain) NSNumber * EthnicityId;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) org_tns2_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals;
@property (retain) org_tns2_ArrayOfGPAdditionAppraisal * GPAdditionAppraisals1;
@property (retain) org_tns2_ArrayOfGPCompanyScoreCard * GPCompanyScoreCards;
@property (retain) org_tns2_ArrayOfGPCompanyStrategicGoal * GPCompanyStrategicGoals;
@property (retain) org_tns2_ArrayOfGPCompanyYearlyObjective * GPCompanyYearlyObjectives;
@property (retain) org_tns2_ArrayOfGPEmployeeScoreCard * GPEmployeeScoreCards;
@property (retain) org_tns2_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives;
@property (retain) org_tns2_ArrayOfGPIndividualYearlyObjective * GPIndividualYearlyObjectives1;
@property (retain) org_tns2_ArrayOfGPObjectiveInitiative * GPObjectiveInitiatives;
@property (retain) org_tns2_ArrayOfGPPerformanceYearEndResult * GPPerformanceYearEndResults;
@property (retain) USBoolean * Gender;
@property (retain) NSString * HomeTown;
@property (retain) NSNumber * HomeTownId;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) org_tns2_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
@property (retain) org_tns2_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
@property (retain) org_tns2_ArrayOfLMSCourseTranscript * LMSCourseTranscripts1;
@property (retain) NSString * LastName;
@property (retain) NSString * MainLanguage;
@property (retain) NSNumber * MainLanguageId;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_ArrayOfNCClientConnection * NCClientConnections;
@property (retain) org_tns2_ArrayOfNCMessage * NCMessages;
@property (retain) org_tns2_ArrayOfNCMessage * NCMessages1;
@property (retain) NSDate * OfficialDate;
@property (retain) NSString * OrgGroupCd;
@property (retain) NSNumber * OrgGroupId;
@property (retain) NSDate * ProbationaryDate;
@property (retain) NSDate * ProbationaryEndDate;
@property (retain) org_tns2_ArrayOfProjectMember * ProjectMembers;
@property (retain) org_tns2_ArrayOfProject * Projects;
@property (retain) org_tns2_ArrayOfRecInterviewSchedule * RecInterviewSchedules;
@property (retain) org_tns2_ArrayOfRecInterviewer * RecInterviewers;
@property (retain) org_tns2_ArrayOfRecPhaseEmpDisplaced * RecPhaseEmpDisplaceds;
@property (retain) org_tns2_ArrayOfRecRecruitmentRequirement * RecRecruitmentRequirements;
@property (retain) org_tns2_ArrayOfRecRequirementEmpDisplaced * RecRequirementEmpDisplaceds;
@property (retain) NSString * Religion;
@property (retain) NSNumber * ReligionId;
@property (retain) org_tns2_ReportEmployeeRequestedBook * ReportEmployeeRequestedBook;
@property (retain) org_tns2_ArrayOfSelfLeaveRequest * SelfLeaveRequests;
@property (retain) org_tns2_ArrayOfSysRecPlanApprover * SysRecPlanApprovers;
@property (retain) org_tns2_ArrayOfSysRecRequirementApprover * SysRecRequirementApprovers;
@property (retain) org_tns2_ArrayOfSysTrainingApprover * SysTrainingApprovers;
@property (retain) org_tns2_ArrayOfTask * Tasks;
@property (retain) org_tns2_ArrayOfTrainingCourseEmployee * TrainingCourseEmployees;
@property (retain) org_tns2_ArrayOfTrainingEmpPlan * TrainingEmpPlans;
@property (retain) org_tns2_ArrayOfTrainingPlanEmployee * TrainingPlanEmployees;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgCoreCompetency : NSObject {
	
/* elements */
	org_tns2_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompetencyLevel;
	NSDate * CreatedDate;
	NSString * Definition;
	org_tns2_ArrayOfEmpCompetency * EmpCompetencies;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * OrgCoreCompetencyId;
	NSString * Statement;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgCoreCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompetencyLevel;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Definition;
@property (retain) org_tns2_ArrayOfEmpCompetency * EmpCompetencies;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * OrgCoreCompetencyId;
@property (retain) NSString * Statement;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_EmpCompetency : NSObject {
	
/* elements */
	org_tns2_CLogRating * CLogRating;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSNumber * EmpCompetencyId;
	org_tns2_Employee * Employee;
	NSNumber * EmployeeId;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	org_tns2_OrgCoreCompetency * OrgCoreCompetency;
	NSNumber * OrgCoreCompetencyId;
	org_tns2_OrgProficencyLevel * OrgProficencyLevel;
	NSNumber * OrgProficencyLevelId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_EmpCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) org_tns2_CLogRating * CLogRating;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSNumber * EmpCompetencyId;
@property (retain) org_tns2_Employee * Employee;
@property (retain) NSNumber * EmployeeId;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) org_tns2_OrgCoreCompetency * OrgCoreCompetency;
@property (retain) NSNumber * OrgCoreCompetencyId;
@property (retain) org_tns2_OrgProficencyLevel * OrgProficencyLevel;
@property (retain) NSNumber * OrgProficencyLevelId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfEmpCompetency : NSObject {
	
/* elements */
	NSMutableArray *EmpCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfEmpCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addEmpCompetency:(org_tns2_EmpCompetency *)toAdd;
@property (readonly) NSMutableArray * EmpCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgCoreCompetency : NSObject {
	
/* elements */
	NSMutableArray *OrgCoreCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgCoreCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgCoreCompetency:(org_tns2_OrgCoreCompetency *)toAdd;
@property (readonly) NSMutableArray * OrgCoreCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_CLogRating : NSObject {
	
/* elements */
	NSNumber * CLogRatingId;
	NSString * Description;
	org_tns2_ArrayOfEmpCompetency * EmpCompetencies;
	org_tns2_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
	org_tns2_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
	org_tns2_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
	org_tns2_ArrayOfEmpProfileSkill * EmpProfileSkills;
	org_tns2_ArrayOfEmpProfileWorkingExperience * EmpProfileWorkingExperiences;
	USBoolean * IsDeleted;
	org_tns2_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
	org_tns2_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
	org_tns2_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
	org_tns2_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
	org_tns2_ArrayOfOrgCoreCompetency * OrgCoreCompetencies;
	org_tns2_ArrayOfOrgProficencyDetail * OrgProficencyDetails;
	org_tns2_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
	org_tns2_ArrayOfOrgProficencyLevel * OrgProficencyLevels;
	org_tns2_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
	org_tns2_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
	org_tns2_ArrayOfRecCandidateCompetencyRating * RecCandidateCompetencyRatings;
	org_tns2_ArrayOfRecCandidateExperience * RecCandidateExperiences;
	org_tns2_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
	org_tns2_ArrayOfRecCandidateQualification * RecCandidateQualifications;
	org_tns2_ArrayOfRecCandidateSkill * RecCandidateSkills;
	org_tns2_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
	org_tns2_ArrayOfTrainingEmpProficency * TrainingEmpProficencies1;
	org_tns2_ArrayOfTrainingProficencyExpected * TrainingProficencyExpecteds;
	org_tns2_ArrayOfTrainingProficencyRequire * TrainingProficencyRequires;
	NSNumber * Value;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_CLogRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSString * Description;
@property (retain) org_tns2_ArrayOfEmpCompetency * EmpCompetencies;
@property (retain) org_tns2_ArrayOfEmpProficencyDetailRating * EmpProficencyDetailRatings;
@property (retain) org_tns2_ArrayOfEmpProficencyLevelDetailRating * EmpProficencyLevelDetailRatings;
@property (retain) org_tns2_ArrayOfEmpProficencyLevelRating * EmpProficencyLevelRatings;
@property (retain) org_tns2_ArrayOfEmpProfileSkill * EmpProfileSkills;
@property (retain) org_tns2_ArrayOfEmpProfileWorkingExperience * EmpProfileWorkingExperiences;
@property (retain) USBoolean * IsDeleted;
@property (retain) org_tns2_ArrayOfLMSCourseAttendee * LMSCourseAttendees;
@property (retain) org_tns2_ArrayOfLMSCourseCompetency * LMSCourseCompetencies;
@property (retain) org_tns2_ArrayOfLMSCourseRequiredCompetency * LMSCourseRequiredCompetencies;
@property (retain) org_tns2_ArrayOfLMSCourseTranscript * LMSCourseTranscripts;
@property (retain) org_tns2_ArrayOfOrgCoreCompetency * OrgCoreCompetencies;
@property (retain) org_tns2_ArrayOfOrgProficencyDetail * OrgProficencyDetails;
@property (retain) org_tns2_ArrayOfOrgProficencyLevelDetail * OrgProficencyLevelDetails;
@property (retain) org_tns2_ArrayOfOrgProficencyLevel * OrgProficencyLevels;
@property (retain) org_tns2_ArrayOfRecCanProficencyDetailRating * RecCanProficencyDetailRatings;
@property (retain) org_tns2_ArrayOfRecCanProficencyLevelDetailRating * RecCanProficencyLevelDetailRatings;
@property (retain) org_tns2_ArrayOfRecCandidateCompetencyRating * RecCandidateCompetencyRatings;
@property (retain) org_tns2_ArrayOfRecCandidateExperience * RecCandidateExperiences;
@property (retain) org_tns2_ArrayOfRecCandidateProficencyLevelRating * RecCandidateProficencyLevelRatings;
@property (retain) org_tns2_ArrayOfRecCandidateQualification * RecCandidateQualifications;
@property (retain) org_tns2_ArrayOfRecCandidateSkill * RecCandidateSkills;
@property (retain) org_tns2_ArrayOfTrainingEmpProficency * TrainingEmpProficencies;
@property (retain) org_tns2_ArrayOfTrainingEmpProficency * TrainingEmpProficencies1;
@property (retain) org_tns2_ArrayOfTrainingProficencyExpected * TrainingProficencyExpecteds;
@property (retain) org_tns2_ArrayOfTrainingProficencyRequire * TrainingProficencyRequires;
@property (retain) NSNumber * Value;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfCLogRating : NSObject {
	
/* elements */
	NSMutableArray *CLogRating;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfCLogRating *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCLogRating:(org_tns2_CLogRating *)toAdd;
@property (readonly) NSMutableArray * CLogRating;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgProficencyType : NSObject {
	
/* elements */
	NSMutableArray *OrgProficencyType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgProficencyType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgProficencyType:(org_tns2_OrgProficencyType *)toAdd;
@property (readonly) NSMutableArray * OrgProficencyType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_OrgJobPositionSpecificCompetency : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSString * CompentencyDefinition;
	NSString * CompetencyLevelName;
	NSString * CompetencyName;
	NSDate * CreatedDate;
	USBoolean * IsCore;
	USBoolean * IsDeleted;
	USBoolean * JobPosSpecificCompetencyIsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgCompetencyGroupId;
	NSNumber * OrgCompetencyId;
	NSNumber * OrgJobId;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSNumber * OrgJobPositionSpecificCompetencyId;
	NSString * OrgJobPositionTitle;
	NSNumber * OrgProficencyLevelId;
	NSNumber * OrgUnitId;
	NSString * PostionName;
	NSNumber * ProficencyLevelRating;
	NSString * ProficencyStatement;
	NSString * RatingDescription;
	NSNumber * RatingValue;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_OrgJobPositionSpecificCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompentencyDefinition;
@property (retain) NSString * CompetencyLevelName;
@property (retain) NSString * CompetencyName;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsCore;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * JobPosSpecificCompetencyIsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgCompetencyGroupId;
@property (retain) NSNumber * OrgCompetencyId;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * OrgJobPositionSpecificCompetencyId;
@property (retain) NSString * OrgJobPositionTitle;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * PostionName;
@property (retain) NSNumber * ProficencyLevelRating;
@property (retain) NSString * ProficencyStatement;
@property (retain) NSString * RatingDescription;
@property (retain) NSNumber * RatingValue;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_OrgJobPositionSpecificCompetency : NSObject {
	
/* elements */
	NSMutableArray *V_OrgJobPositionSpecificCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_OrgJobPositionSpecificCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgJobPositionSpecificCompetency:(org_tns2_V_OrgJobPositionSpecificCompetency *)toAdd;
@property (readonly) NSMutableArray * V_OrgJobPositionSpecificCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgCompetencyGroup : NSObject {
	
/* elements */
	NSMutableArray *OrgCompetencyGroup;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgCompetencyGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgCompetencyGroup:(org_tns2_OrgCompetencyGroup *)toAdd;
@property (readonly) NSMutableArray * OrgCompetencyGroup;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_OrgJobSpecificCompetency : NSObject {
	
/* elements */
	NSNumber * CLogCareerId;
	NSNumber * CompanyId;
	NSString * Definition;
	USBoolean * IsCore;
	USBoolean * IsDeleted;
	NSNumber * OrgCompetencyGroupId;
	NSNumber * OrgCompetencyId;
	NSString * OrgCompetencyName;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSNumber * OrgJobSpecificCompetencyId;
	USBoolean * OrgJobSpecificCompetencyIsDeleted;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_OrgJobSpecificCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogCareerId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Definition;
@property (retain) USBoolean * IsCore;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * OrgCompetencyGroupId;
@property (retain) NSNumber * OrgCompetencyId;
@property (retain) NSString * OrgCompetencyName;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSNumber * OrgJobSpecificCompetencyId;
@property (retain) USBoolean * OrgJobSpecificCompetencyIsDeleted;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_OrgJobSpecificCompetency : NSObject {
	
/* elements */
	NSMutableArray *V_OrgJobSpecificCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_OrgJobSpecificCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgJobSpecificCompetency:(org_tns2_V_OrgJobSpecificCompetency *)toAdd;
@property (readonly) NSMutableArray * V_OrgJobSpecificCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_OrgProficencyLevel : NSObject {
	
/* elements */
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompetencyLevelName;
	NSDate * CreatedDate;
	USBoolean * IsCore;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgCompetencyGroupId;
	NSNumber * OrgCompetencyId;
	NSString * OrgCompetencyName;
	NSNumber * OrgProficencyLevelId;
	NSString * RatingDescription;
	NSNumber * RatingValue;
	NSString * Statement;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_OrgProficencyLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompetencyLevelName;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsCore;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgCompetencyGroupId;
@property (retain) NSNumber * OrgCompetencyId;
@property (retain) NSString * OrgCompetencyName;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) NSString * RatingDescription;
@property (retain) NSNumber * RatingValue;
@property (retain) NSString * Statement;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_OrgProficencyLevel : NSObject {
	
/* elements */
	NSMutableArray *V_OrgProficencyLevel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_OrgProficencyLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgProficencyLevel:(org_tns2_V_OrgProficencyLevel *)toAdd;
@property (readonly) NSMutableArray * V_OrgProficencyLevel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_OrgProficencyLevelDetail : NSObject {
	
/* elements */
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgProficencyLevelDetailId;
	NSNumber * OrgProficencyLevelId;
	NSNumber * OrgProficencyTypeId;
	NSString * ProficencyTypeCode;
	NSString * ProficencyTypeName;
	NSString * RatingDescription;
	NSNumber * RatingValue;
	NSString * Statement;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_OrgProficencyLevelDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) NSNumber * OrgProficencyTypeId;
@property (retain) NSString * ProficencyTypeCode;
@property (retain) NSString * ProficencyTypeName;
@property (retain) NSString * RatingDescription;
@property (retain) NSNumber * RatingValue;
@property (retain) NSString * Statement;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_OrgProficencyLevelDetail : NSObject {
	
/* elements */
	NSMutableArray *V_OrgProficencyLevelDetail;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_OrgProficencyLevelDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgProficencyLevelDetail:(org_tns2_V_OrgProficencyLevelDetail *)toAdd;
@property (readonly) NSMutableArray * V_OrgProficencyLevelDetail;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_OrgSkillCompetency : NSObject {
	
/* elements */
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompetencyLevelName;
	USBoolean * IsDeleted;
	NSNumber * OrgCompetencyId;
	NSNumber * OrgProficencyDetailId;
	NSNumber * OrgProficencyLevelDetailId;
	NSNumber * OrgProficencyLevelId;
	NSNumber * OrgProficencyTypeId;
	NSString * OrgSkillCode;
	NSNumber * OrgSkillTypeId;
	NSString * ProficencyTypeCode;
	NSString * ProficencyTypeName;
	NSString * RatingDescription;
	NSNumber * RatingValue;
	NSString * SkillDescription;
	NSString * SkillName;
	NSString * SkillTypeName;
	NSString * Statement;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_OrgSkillCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompetencyLevelName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * OrgCompetencyId;
@property (retain) NSNumber * OrgProficencyDetailId;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) NSNumber * OrgProficencyTypeId;
@property (retain) NSString * OrgSkillCode;
@property (retain) NSNumber * OrgSkillTypeId;
@property (retain) NSString * ProficencyTypeCode;
@property (retain) NSString * ProficencyTypeName;
@property (retain) NSString * RatingDescription;
@property (retain) NSNumber * RatingValue;
@property (retain) NSString * SkillDescription;
@property (retain) NSString * SkillName;
@property (retain) NSString * SkillTypeName;
@property (retain) NSString * Statement;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_OrgSkillCompetency : NSObject {
	
/* elements */
	NSMutableArray *V_OrgSkillCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_OrgSkillCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgSkillCompetency:(org_tns2_V_OrgSkillCompetency *)toAdd;
@property (readonly) NSMutableArray * V_OrgSkillCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_OrgQualificationCompetency : NSObject {
	
/* elements */
	NSString * CLogMajorCode;
	NSNumber * CLogMajorId;
	NSString * CLogMajorName;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompetencyLevelName;
	USBoolean * IsDeleted;
	NSNumber * OrgCompetencyId;
	NSNumber * OrgProficencyDetailId;
	NSNumber * OrgProficencyDetailRatingId;
	NSNumber * OrgProficencyLevelDetailId;
	NSNumber * OrgProficencyLevelId;
	NSNumber * OrgProficencyLevelRatingId;
	NSString * OrgProficencyLevelStatement;
	NSNumber * OrgProficencyTypeId;
	NSString * OrgQualificationCode;
	NSString * OrgQualificationDescription;
	NSNumber * OrgQualificationId;
	NSString * OrgQualificationName;
	NSString * ProficencyTypeCode;
	NSString * ProficencyTypeName;
	NSString * RatingDescription;
	NSNumber * RatingValue;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_OrgQualificationCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogMajorCode;
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSString * CLogMajorName;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompetencyLevelName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * OrgCompetencyId;
@property (retain) NSNumber * OrgProficencyDetailId;
@property (retain) NSNumber * OrgProficencyDetailRatingId;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) NSNumber * OrgProficencyLevelRatingId;
@property (retain) NSString * OrgProficencyLevelStatement;
@property (retain) NSNumber * OrgProficencyTypeId;
@property (retain) NSString * OrgQualificationCode;
@property (retain) NSString * OrgQualificationDescription;
@property (retain) NSNumber * OrgQualificationId;
@property (retain) NSString * OrgQualificationName;
@property (retain) NSString * ProficencyTypeCode;
@property (retain) NSString * ProficencyTypeName;
@property (retain) NSString * RatingDescription;
@property (retain) NSNumber * RatingValue;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_OrgQualificationCompetency : NSObject {
	
/* elements */
	NSMutableArray *V_OrgQualificationCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_OrgQualificationCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgQualificationCompetency:(org_tns2_V_OrgQualificationCompetency *)toAdd;
@property (readonly) NSMutableArray * V_OrgQualificationCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_OrgExperienceCompetency : NSObject {
	
/* elements */
	NSString * Achievement;
	NSNumber * CLogRatingId;
	NSNumber * CompanyId;
	NSString * CompetencyLevelName;
	USBoolean * IsDeleted;
	NSString * LevelDetailStatement;
	NSNumber * OrgCompetencyId;
	NSNumber * OrgExperienceId;
	NSNumber * OrgProficencyDetailId;
	NSNumber * OrgProficencyLevelDetailId;
	NSNumber * OrgProficencyLevelDetailRatingId;
	NSNumber * OrgProficencyLevelId;
	NSNumber * OrgProficencyTypeId;
	NSNumber * OrgProjectTypeId;
	NSString * OrgProjectTypeName;
	NSString * OrgTimeInChargeDescription;
	NSNumber * OrgTimeInChargeId;
	NSString * ProficencyTypeCode;
	NSString * ProficencyTypeName;
	NSString * RatingDescription;
	NSNumber * RatingValue;
	NSString * RoleDescription;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_OrgExperienceCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Achievement;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * CompetencyLevelName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * LevelDetailStatement;
@property (retain) NSNumber * OrgCompetencyId;
@property (retain) NSNumber * OrgExperienceId;
@property (retain) NSNumber * OrgProficencyDetailId;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) NSNumber * OrgProficencyLevelDetailRatingId;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) NSNumber * OrgProficencyTypeId;
@property (retain) NSNumber * OrgProjectTypeId;
@property (retain) NSString * OrgProjectTypeName;
@property (retain) NSString * OrgTimeInChargeDescription;
@property (retain) NSNumber * OrgTimeInChargeId;
@property (retain) NSString * ProficencyTypeCode;
@property (retain) NSString * ProficencyTypeName;
@property (retain) NSString * RatingDescription;
@property (retain) NSNumber * RatingValue;
@property (retain) NSString * RoleDescription;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_OrgExperienceCompetency : NSObject {
	
/* elements */
	NSMutableArray *V_OrgExperienceCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_OrgExperienceCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgExperienceCompetency:(org_tns2_V_OrgExperienceCompetency *)toAdd;
@property (readonly) NSMutableArray * V_OrgExperienceCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_OrgDegreeCompetency : NSObject {
	
/* elements */
	NSNumber * CLogRatingId;
	NSString * CompetencyLevelName;
	USBoolean * IsDeleted;
	NSString * LevelDetailStatement;
	NSNumber * OrgCompetencyId;
	NSString * OrgDegreeDescription;
	NSNumber * OrgDegreeId;
	NSString * OrgDegreeName;
	NSNumber * OrgProficencyDetailId;
	NSNumber * OrgProficencyLevelDetailId;
	NSNumber * OrgProficencyLevelDetailRatingId;
	NSNumber * OrgProficencyLevelId;
	NSNumber * OrgProficencyTypeId;
	NSString * ProficencyTypeCode;
	NSString * ProficencyTypeName;
	NSString * RatingDescription;
	NSNumber * RatingValue;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_OrgDegreeCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSString * CompetencyLevelName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * LevelDetailStatement;
@property (retain) NSNumber * OrgCompetencyId;
@property (retain) NSString * OrgDegreeDescription;
@property (retain) NSNumber * OrgDegreeId;
@property (retain) NSString * OrgDegreeName;
@property (retain) NSNumber * OrgProficencyDetailId;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) NSNumber * OrgProficencyLevelDetailRatingId;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) NSNumber * OrgProficencyTypeId;
@property (retain) NSString * ProficencyTypeCode;
@property (retain) NSString * ProficencyTypeName;
@property (retain) NSString * RatingDescription;
@property (retain) NSNumber * RatingValue;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_OrgDegreeCompetency : NSObject {
	
/* elements */
	NSMutableArray *V_OrgDegreeCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_OrgDegreeCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgDegreeCompetency:(org_tns2_V_OrgDegreeCompetency *)toAdd;
@property (readonly) NSMutableArray * V_OrgDegreeCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_OrgAttitudeCompetency : NSObject {
	
/* elements */
	NSNumber * CLogRatingId;
	NSString * CompetencyLevelName;
	USBoolean * IsDeleted;
	NSString * OrgAttitudeCode;
	NSNumber * OrgAttitudeId;
	NSString * OrgAttitudeName;
	NSString * OrgAttitudeValue;
	NSNumber * OrgCompetencyId;
	NSNumber * OrgProficencyDetailId;
	NSNumber * OrgProficencyLevelDetailId;
	NSNumber * OrgProficencyLevelId;
	NSNumber * OrgProficencyTypeId;
	NSString * ProficencyTypeCode;
	NSString * RatingDescription;
	NSNumber * RatingValue;
	NSString * Statement;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_OrgAttitudeCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSString * CompetencyLevelName;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * OrgAttitudeCode;
@property (retain) NSNumber * OrgAttitudeId;
@property (retain) NSString * OrgAttitudeName;
@property (retain) NSString * OrgAttitudeValue;
@property (retain) NSNumber * OrgCompetencyId;
@property (retain) NSNumber * OrgProficencyDetailId;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) NSNumber * OrgProficencyTypeId;
@property (retain) NSString * ProficencyTypeCode;
@property (retain) NSString * RatingDescription;
@property (retain) NSNumber * RatingValue;
@property (retain) NSString * Statement;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_OrgAttitudeCompetency : NSObject {
	
/* elements */
	NSMutableArray *V_OrgAttitudeCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_OrgAttitudeCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgAttitudeCompetency:(org_tns2_V_OrgAttitudeCompetency *)toAdd;
@property (readonly) NSMutableArray * V_OrgAttitudeCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_OrgKnowledgeCompetency : NSObject {
	
/* elements */
	NSNumber * CLogMajorId;
	NSNumber * CLogRatingId;
	NSString * CompetencyLevelName;
	NSString * Description;
	USBoolean * IsDeleted;
	NSNumber * OrgCompetencyId;
	NSString * OrgKnowledgeCode;
	NSNumber * OrgKnowledgeId;
	NSString * OrgKnowledgeName;
	NSNumber * OrgProficencyDetailId;
	NSNumber * OrgProficencyLevelDetailId;
	NSNumber * OrgProficencyLevelId;
	NSNumber * OrgProficencyTypeId;
	NSString * ProficencyTypeCode;
	NSString * RatingDescription;
	NSNumber * RatingValue;
	NSString * Statement;
	NSNumber * WeightPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_OrgKnowledgeCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSString * CompetencyLevelName;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * OrgCompetencyId;
@property (retain) NSString * OrgKnowledgeCode;
@property (retain) NSNumber * OrgKnowledgeId;
@property (retain) NSString * OrgKnowledgeName;
@property (retain) NSNumber * OrgProficencyDetailId;
@property (retain) NSNumber * OrgProficencyLevelDetailId;
@property (retain) NSNumber * OrgProficencyLevelId;
@property (retain) NSNumber * OrgProficencyTypeId;
@property (retain) NSString * ProficencyTypeCode;
@property (retain) NSString * RatingDescription;
@property (retain) NSNumber * RatingValue;
@property (retain) NSString * Statement;
@property (retain) NSNumber * WeightPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_OrgKnowledgeCompetency : NSObject {
	
/* elements */
	NSMutableArray *V_OrgKnowledgeCompetency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_OrgKnowledgeCompetency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgKnowledgeCompetency:(org_tns2_V_OrgKnowledgeCompetency *)toAdd;
@property (readonly) NSMutableArray * V_OrgKnowledgeCompetency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_ESS_ORG_DIRECT_REPORT : NSObject {
	
/* elements */
	NSString * ChildCode;
	NSString * ChildEmail;
	NSNumber * ChildEmpJobPositionId;
	NSNumber * ChildId;
	NSString * ChildJobPositionName;
	NSString * ChildMobileNumber;
	NSString * ChildName;
	NSString * ChildUnitName;
	NSNumber * CompanyId;
	NSString * DirectReportToEmployeeCode;
	NSString * ImageUrl;
	USBoolean * IsDeleted;
	NSString * ParentCode;
	NSNumber * ParentId;
	NSString * ParentName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_ESS_ORG_DIRECT_REPORT *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ChildCode;
@property (retain) NSString * ChildEmail;
@property (retain) NSNumber * ChildEmpJobPositionId;
@property (retain) NSNumber * ChildId;
@property (retain) NSString * ChildJobPositionName;
@property (retain) NSString * ChildMobileNumber;
@property (retain) NSString * ChildName;
@property (retain) NSString * ChildUnitName;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * DirectReportToEmployeeCode;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * ParentCode;
@property (retain) NSNumber * ParentId;
@property (retain) NSString * ParentName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_ESS_ORG_DIRECT_REPORT : NSObject {
	
/* elements */
	NSMutableArray *V_ESS_ORG_DIRECT_REPORT;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_ESS_ORG_DIRECT_REPORT *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ESS_ORG_DIRECT_REPORT:(org_tns2_V_ESS_ORG_DIRECT_REPORT *)toAdd;
@property (readonly) NSMutableArray * V_ESS_ORG_DIRECT_REPORT;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_TB_ESS_PERSONEL_STRUCT : NSObject {
	
/* elements */
	NSNumber * CREATED_BY;
	NSDate * CREATED_DATE;
	USBoolean * IS_DELETED;
	NSNumber * MODIFIED_BY;
	NSDate * MODIFIED_DATE;
	NSString * TB_ESS_PER_STRUCT_CD;
	NSString * TB_ESS_PER_STRUCT_DESC;
	NSDate * TB_ESS_PER_STRUCT_EXP_DATE;
	NSString * TB_ESS_PER_STRUCT_GEN_CD;
	NSNumber * TB_ESS_PER_STRUCT_ID;
	USBoolean * TB_ESS_PER_STRUCT_IS_INACTIVE;
	NSString * TB_ESS_PER_STRUCT_NM_EN;
	NSString * TB_ESS_PER_STRUCT_NM_VN;
	NSDate * TB_ESS_PER_STRUCT_VALID_DATE;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_TB_ESS_PERSONEL_STRUCT *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CREATED_BY;
@property (retain) NSDate * CREATED_DATE;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSNumber * MODIFIED_BY;
@property (retain) NSDate * MODIFIED_DATE;
@property (retain) NSString * TB_ESS_PER_STRUCT_CD;
@property (retain) NSString * TB_ESS_PER_STRUCT_DESC;
@property (retain) NSDate * TB_ESS_PER_STRUCT_EXP_DATE;
@property (retain) NSString * TB_ESS_PER_STRUCT_GEN_CD;
@property (retain) NSNumber * TB_ESS_PER_STRUCT_ID;
@property (retain) USBoolean * TB_ESS_PER_STRUCT_IS_INACTIVE;
@property (retain) NSString * TB_ESS_PER_STRUCT_NM_EN;
@property (retain) NSString * TB_ESS_PER_STRUCT_NM_VN;
@property (retain) NSDate * TB_ESS_PER_STRUCT_VALID_DATE;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfTB_ESS_PERSONEL_STRUCT : NSObject {
	
/* elements */
	NSMutableArray *TB_ESS_PERSONEL_STRUCT;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfTB_ESS_PERSONEL_STRUCT *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addTB_ESS_PERSONEL_STRUCT:(org_tns2_TB_ESS_PERSONEL_STRUCT *)toAdd;
@property (readonly) NSMutableArray * TB_ESS_PERSONEL_STRUCT;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_ESS_PER_STRUCT_EMP_MAPPING : NSObject {
	
/* elements */
	NSDate * BirthDay;
	NSNumber * CREATED_BY;
	NSDate * CREATED_DATE;
	NSNumber * CompanyId;
	NSString * Email;
	NSString * EmpWorkingStatusCode;
	NSNumber * EmpWorkingStatusId;
	NSString * EmpWorkingStatusName;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSNumber * EmployeeTypeId;
	NSString * EmployeeTypeName;
	NSDate * EndWorkingDate;
	NSDate * EntryDate;
	NSString * FullName;
	USBoolean * Gender;
	USBoolean * IS_DELETED;
	NSString * IdentityCardNo;
	NSString * ImageUrl;
	USBoolean * IsDeleted;
	USBoolean * IsOnlyYearOfBirthday;
	NSNumber * MODIFIED_BY;
	NSDate * MODIFIED_DATE;
	NSString * MobileNumber;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSNumber * OrgJobPositionId;
	NSString * OrgJobPositionName;
	NSNumber * OrgUnitId;
	NSString * TB_ESS_EMP_CD;
	NSNumber * TB_ESS_EMP_ID;
	NSString * TB_ESS_PER_STRUCT_CD;
	NSString * TB_ESS_PER_STRUCT_EMP_MAPPING_DESC;
	NSNumber * TB_ESS_PER_STRUCT_EMP_MAPPING_ID;
	NSDate * TB_ESS_PER_STRUCT_EMP_MAPPING_IN_DATE;
	NSDate * TB_ESS_PER_STRUCT_EMP_MAPPING_OUT_DATE;
	NSNumber * TB_ESS_PER_STRUCT_ID;
	NSString * UnitName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_ESS_PER_STRUCT_EMP_MAPPING *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * BirthDay;
@property (retain) NSNumber * CREATED_BY;
@property (retain) NSDate * CREATED_DATE;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Email;
@property (retain) NSString * EmpWorkingStatusCode;
@property (retain) NSNumber * EmpWorkingStatusId;
@property (retain) NSString * EmpWorkingStatusName;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSNumber * EmployeeTypeId;
@property (retain) NSString * EmployeeTypeName;
@property (retain) NSDate * EndWorkingDate;
@property (retain) NSDate * EntryDate;
@property (retain) NSString * FullName;
@property (retain) USBoolean * Gender;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSString * IdentityCardNo;
@property (retain) NSString * ImageUrl;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsOnlyYearOfBirthday;
@property (retain) NSNumber * MODIFIED_BY;
@property (retain) NSDate * MODIFIED_DATE;
@property (retain) NSString * MobileNumber;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * TB_ESS_EMP_CD;
@property (retain) NSNumber * TB_ESS_EMP_ID;
@property (retain) NSString * TB_ESS_PER_STRUCT_CD;
@property (retain) NSString * TB_ESS_PER_STRUCT_EMP_MAPPING_DESC;
@property (retain) NSNumber * TB_ESS_PER_STRUCT_EMP_MAPPING_ID;
@property (retain) NSDate * TB_ESS_PER_STRUCT_EMP_MAPPING_IN_DATE;
@property (retain) NSDate * TB_ESS_PER_STRUCT_EMP_MAPPING_OUT_DATE;
@property (retain) NSNumber * TB_ESS_PER_STRUCT_ID;
@property (retain) NSString * UnitName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_ESS_PER_STRUCT_EMP_MAPPING : NSObject {
	
/* elements */
	NSMutableArray *V_ESS_PER_STRUCT_EMP_MAPPING;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_ESS_PER_STRUCT_EMP_MAPPING *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ESS_PER_STRUCT_EMP_MAPPING:(org_tns2_V_ESS_PER_STRUCT_EMP_MAPPING *)toAdd;
@property (readonly) NSMutableArray * V_ESS_PER_STRUCT_EMP_MAPPING;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_ORG_JOB_TITLE_TREE : NSObject {
	
/* elements */
	NSString * CODE;
	NSNumber * COMPANY_ID;
	NSNumber * ID_;
	NSString * LEVEL_CODE;
	NSString * LEVEL_NAME_EN;
	NSString * LEVEL_NAME_VN;
	NSString * NAME;
	NSString * NAME_EN;
	NSString * PARENT_CODE;
	NSString * PARENT_ID;
	NSString * TYPE;
	NSString * UNIQUE_ID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_ORG_JOB_TITLE_TREE *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CODE;
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSNumber * ID_;
@property (retain) NSString * LEVEL_CODE;
@property (retain) NSString * LEVEL_NAME_EN;
@property (retain) NSString * LEVEL_NAME_VN;
@property (retain) NSString * NAME;
@property (retain) NSString * NAME_EN;
@property (retain) NSString * PARENT_CODE;
@property (retain) NSString * PARENT_ID;
@property (retain) NSString * TYPE;
@property (retain) NSString * UNIQUE_ID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_ORG_JOB_TITLE_TREE : NSObject {
	
/* elements */
	NSMutableArray *V_ORG_JOB_TITLE_TREE;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_ORG_JOB_TITLE_TREE *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ORG_JOB_TITLE_TREE:(org_tns2_V_ORG_JOB_TITLE_TREE *)toAdd;
@property (readonly) NSMutableArray * V_ORG_JOB_TITLE_TREE;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_ORG_JOB_TITLE_AND_GROUP : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * GROUP_CODE;
	NSNumber * GROUP_ID;
	NSString * GROUP_NAME_EN;
	NSString * GROUP_NAME_VN;
	NSNumber * GROUP_ORDER;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSString * JOB_TITLE_CODE;
	NSString * JOB_TITLE_DESC;
	NSNumber * JOB_TITLE_ID;
	NSString * JOB_TITLE_NAME_EN;
	NSString * JOB_TITLE_NAME_VN;
	NSString * LEVEL_CODE;
	NSNumber * LEVEL_ID;
	NSString * LEVEL_NAME_EN;
	NSString * LEVEL_NAME_VN;
	NSDate * ModifiedDate;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_ORG_JOB_TITLE_AND_GROUP *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * GROUP_CODE;
@property (retain) NSNumber * GROUP_ID;
@property (retain) NSString * GROUP_NAME_EN;
@property (retain) NSString * GROUP_NAME_VN;
@property (retain) NSNumber * GROUP_ORDER;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * JOB_TITLE_CODE;
@property (retain) NSString * JOB_TITLE_DESC;
@property (retain) NSNumber * JOB_TITLE_ID;
@property (retain) NSString * JOB_TITLE_NAME_EN;
@property (retain) NSString * JOB_TITLE_NAME_VN;
@property (retain) NSString * LEVEL_CODE;
@property (retain) NSNumber * LEVEL_ID;
@property (retain) NSString * LEVEL_NAME_EN;
@property (retain) NSString * LEVEL_NAME_VN;
@property (retain) NSDate * ModifiedDate;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_ORG_JOB_TITLE_GROUP : NSObject {
	
/* elements */
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	NSString * GROUP_CODE;
	NSString * GROUP_NAME_EN;
	NSString * GROUP_NAME_VN;
	USBoolean * IsDeleted;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * SysParamCode;
	NSNumber * SysParamCompanyId;
	NSString * SysParamDesc;
	NSNumber * SysParamId;
	NSString * SysParamKey;
	NSNumber * SysParamOrder;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_ORG_JOB_TITLE_GROUP *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * GROUP_CODE;
@property (retain) NSString * GROUP_NAME_EN;
@property (retain) NSString * GROUP_NAME_VN;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * SysParamCode;
@property (retain) NSNumber * SysParamCompanyId;
@property (retain) NSString * SysParamDesc;
@property (retain) NSNumber * SysParamId;
@property (retain) NSString * SysParamKey;
@property (retain) NSNumber * SysParamOrder;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_ORG_JOB_TITLE_GROUP : NSObject {
	
/* elements */
	NSMutableArray *V_ORG_JOB_TITLE_GROUP;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_ORG_JOB_TITLE_GROUP *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ORG_JOB_TITLE_GROUP:(org_tns2_V_ORG_JOB_TITLE_GROUP *)toAdd;
@property (readonly) NSMutableArray * V_ORG_JOB_TITLE_GROUP;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_ORG_JOB_TITLE_LEVEL : NSObject {
	
/* elements */
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	USBoolean * IsDeleted;
	NSString * LEVEL_CODE;
	NSString * LEVEL_NAME_EN;
	NSString * LEVEL_NAME_VN;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * SysParamCode;
	NSNumber * SysParamCompanyId;
	NSString * SysParamDesc;
	NSNumber * SysParamId;
	NSString * SysParamKey;
	NSNumber * SysParamOrder;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_ORG_JOB_TITLE_LEVEL *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * LEVEL_CODE;
@property (retain) NSString * LEVEL_NAME_EN;
@property (retain) NSString * LEVEL_NAME_VN;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * SysParamCode;
@property (retain) NSNumber * SysParamCompanyId;
@property (retain) NSString * SysParamDesc;
@property (retain) NSNumber * SysParamId;
@property (retain) NSString * SysParamKey;
@property (retain) NSNumber * SysParamOrder;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_ORG_JOB_TITLE_LEVEL : NSObject {
	
/* elements */
	NSMutableArray *V_ORG_JOB_TITLE_LEVEL;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_ORG_JOB_TITLE_LEVEL *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ORG_JOB_TITLE_LEVEL:(org_tns2_V_ORG_JOB_TITLE_LEVEL *)toAdd;
@property (readonly) NSMutableArray * V_ORG_JOB_TITLE_LEVEL;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_ORG_JOB_TITLE_AND_GROUP : NSObject {
	
/* elements */
	NSMutableArray *V_ORG_JOB_TITLE_AND_GROUP;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_ORG_JOB_TITLE_AND_GROUP *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ORG_JOB_TITLE_AND_GROUP:(org_tns2_V_ORG_JOB_TITLE_AND_GROUP *)toAdd;
@property (readonly) NSMutableArray * V_ORG_JOB_TITLE_AND_GROUP;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_ESS_TREE_OF_EMP_TYPE : NSObject {
	
/* elements */
	NSString * CODE;
	NSNumber * COMPANY_ID;
	NSString * NAME;
	NSString * NAME_EN;
	NSString * PARENT_CODE;
	NSNumber * PARENT_ID;
	NSNumber * ROW_ID;
	NSString * TYPE;
	NSString * UNIQUE_ID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_ESS_TREE_OF_EMP_TYPE *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CODE;
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSString * NAME;
@property (retain) NSString * NAME_EN;
@property (retain) NSString * PARENT_CODE;
@property (retain) NSNumber * PARENT_ID;
@property (retain) NSNumber * ROW_ID;
@property (retain) NSString * TYPE;
@property (retain) NSString * UNIQUE_ID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_ESS_TREE_OF_EMP_TYPE : NSObject {
	
/* elements */
	NSMutableArray *V_ESS_TREE_OF_EMP_TYPE;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_ESS_TREE_OF_EMP_TYPE *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ESS_TREE_OF_EMP_TYPE:(org_tns2_V_ESS_TREE_OF_EMP_TYPE *)toAdd;
@property (readonly) NSMutableArray * V_ESS_TREE_OF_EMP_TYPE;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_ESS_EMP_GROUP_MAPPING : NSObject {
	
/* elements */
	NSString * CODE;
	NSDate * EMP_BRITHDAY;
	NSString * EMP_CODE;
	NSNumber * EMP_COMPANY_ID;
	NSString * EMP_EMAIL;
	NSDate * EMP_ENTRY_DATE;
	NSString * EMP_FULL_NAME;
	USBoolean * EMP_GENDER;
	NSNumber * EMP_ID;
	NSString * EMP_IMG_URL;
	NSString * EMP_MOBILE;
	NSNumber * EMP_ORD_ID;
	NSString * EMP_TYPE_NAME;
	NSString * EMP_UNIT_NAME;
	NSString * ICON_GROUP;
	NSNumber * ROW_ID;
	NSString * UNIQUE_ID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_ESS_EMP_GROUP_MAPPING *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CODE;
@property (retain) NSDate * EMP_BRITHDAY;
@property (retain) NSString * EMP_CODE;
@property (retain) NSNumber * EMP_COMPANY_ID;
@property (retain) NSString * EMP_EMAIL;
@property (retain) NSDate * EMP_ENTRY_DATE;
@property (retain) NSString * EMP_FULL_NAME;
@property (retain) USBoolean * EMP_GENDER;
@property (retain) NSNumber * EMP_ID;
@property (retain) NSString * EMP_IMG_URL;
@property (retain) NSString * EMP_MOBILE;
@property (retain) NSNumber * EMP_ORD_ID;
@property (retain) NSString * EMP_TYPE_NAME;
@property (retain) NSString * EMP_UNIT_NAME;
@property (retain) NSString * ICON_GROUP;
@property (retain) NSNumber * ROW_ID;
@property (retain) NSString * UNIQUE_ID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_ESS_EMP_GROUP_MAPPING : NSObject {
	
/* elements */
	NSMutableArray *V_ESS_EMP_GROUP_MAPPING;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_ESS_EMP_GROUP_MAPPING *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ESS_EMP_GROUP_MAPPING:(org_tns2_V_ESS_EMP_GROUP_MAPPING *)toAdd;
@property (readonly) NSMutableArray * V_ESS_EMP_GROUP_MAPPING;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_ORG_CHANGE_INFO : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * EventDate;
	NSString * EventType;
	NSString * EventXmlData;
	NSNumber * RecordId;
	NSString * TableName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_ORG_CHANGE_INFO *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * EventDate;
@property (retain) NSString * EventType;
@property (retain) NSString * EventXmlData;
@property (retain) NSNumber * RecordId;
@property (retain) NSString * TableName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_ORG_CHANGE_INFO : NSObject {
	
/* elements */
	NSMutableArray *V_ORG_CHANGE_INFO;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_ORG_CHANGE_INFO *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ORG_CHANGE_INFO:(org_tns2_V_ORG_CHANGE_INFO *)toAdd;
@property (readonly) NSMutableArray * V_ORG_CHANGE_INFO;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_OrgUnit : NSObject {
	
/* elements */
	NSString * Address;
	NSNumber * CompanyId;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	USBoolean * IsLeaderActive;
	NSString * Name;
	NSNumber * OrgJobTitleId;
	NSString * OrgJobTitleLevelCode;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgUnitLeaderCode;
	NSString * OrgUnitLeaderFirstName;
	NSString * OrgUnitLeaderFullName;
	NSNumber * OrgUnitLeaderId;
	NSString * OrgUnitLeaderImageURL;
	NSString * OrgUnitLeaderJobPositionName;
	NSString * OrgUnitLeaderLastName;
	NSString * ParentCode;
	NSNumber * ParentId;
	NSNumber * TotalEmployees;
	NSString * WorkLocation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_OrgUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsLeaderActive;
@property (retain) NSString * Name;
@property (retain) NSNumber * OrgJobTitleId;
@property (retain) NSString * OrgJobTitleLevelCode;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitLeaderCode;
@property (retain) NSString * OrgUnitLeaderFirstName;
@property (retain) NSString * OrgUnitLeaderFullName;
@property (retain) NSNumber * OrgUnitLeaderId;
@property (retain) NSString * OrgUnitLeaderImageURL;
@property (retain) NSString * OrgUnitLeaderJobPositionName;
@property (retain) NSString * OrgUnitLeaderLastName;
@property (retain) NSString * ParentCode;
@property (retain) NSNumber * ParentId;
@property (retain) NSNumber * TotalEmployees;
@property (retain) NSString * WorkLocation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_OrgUnit : NSObject {
	
/* elements */
	NSMutableArray *V_OrgUnit;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_OrgUnit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgUnit:(org_tns2_V_OrgUnit *)toAdd;
@property (readonly) NSMutableArray * V_OrgUnit;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_EmpProfileJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * DecisionFile;
	NSString * DecisionName;
	NSString * DecisionNo;
	NSString * DirectReportToEmployeeCode;
	NSString * DirectReportToEmployeeFullName;
	NSNumber * DirectReportToEmployeeId;
	NSNumber * EmpProfileJobPositionId;
	NSString * EmployeeCode;
	NSNumber * EmployeeId;
	NSDate * ExpirationDate;
	NSString * FirstName;
	NSString * FullName;
	NSDate * ImplementationDate;
	USBoolean * IsActive;
	USBoolean * IsConcurrentPost;
	USBoolean * IsDeleted;
	USBoolean * IsMainPost;
	USBoolean * IsPromoted;
	NSString * JOB_TITLE_LEVEL_NAME_EN;
	NSString * JOB_TITLE_LEVEL_NAME_VN;
	NSString * LastName;
	NSDate * ModifiedDate;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobNameEN;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgJobPositionName;
	NSString * OrgJobPositionNameEN;
	NSString * OrgJobTitleCode;
	NSString * OrgJobTitleGroupCode;
	NSNumber * OrgJobTitleId;
	NSString * OrgJobTitleLevelCode;
	NSString * OrgJobTitleNameEN;
	NSString * OrgJobTitleNameVN;
	NSString * OrgUnitAddress;
	NSString * OrgUnitCode;
	NSString * OrgUnitDescription;
	NSNumber * OrgUnitId;
	NSString * OrgUnitName;
	NSString * OrgUnitNameEN;
	NSString * OrgUnitWorkLocation;
	NSString * OrgWorkLevelCode;
	NSString * OrgWorkLevelDescription;
	NSNumber * OrgWorkLevelId;
	NSString * OrgWorkLevelName;
	NSString * OrgWorkLevelNameEN;
	NSNumber * PercentParticipation;
	NSString * Reason;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_EmpProfileJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * DecisionFile;
@property (retain) NSString * DecisionName;
@property (retain) NSString * DecisionNo;
@property (retain) NSString * DirectReportToEmployeeCode;
@property (retain) NSString * DirectReportToEmployeeFullName;
@property (retain) NSNumber * DirectReportToEmployeeId;
@property (retain) NSNumber * EmpProfileJobPositionId;
@property (retain) NSString * EmployeeCode;
@property (retain) NSNumber * EmployeeId;
@property (retain) NSDate * ExpirationDate;
@property (retain) NSString * FirstName;
@property (retain) NSString * FullName;
@property (retain) NSDate * ImplementationDate;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsConcurrentPost;
@property (retain) USBoolean * IsDeleted;
@property (retain) USBoolean * IsMainPost;
@property (retain) USBoolean * IsPromoted;
@property (retain) NSString * JOB_TITLE_LEVEL_NAME_EN;
@property (retain) NSString * JOB_TITLE_LEVEL_NAME_VN;
@property (retain) NSString * LastName;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobNameEN;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSString * OrgJobPositionNameEN;
@property (retain) NSString * OrgJobTitleCode;
@property (retain) NSString * OrgJobTitleGroupCode;
@property (retain) NSNumber * OrgJobTitleId;
@property (retain) NSString * OrgJobTitleLevelCode;
@property (retain) NSString * OrgJobTitleNameEN;
@property (retain) NSString * OrgJobTitleNameVN;
@property (retain) NSString * OrgUnitAddress;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSString * OrgUnitDescription;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitName;
@property (retain) NSString * OrgUnitNameEN;
@property (retain) NSString * OrgUnitWorkLocation;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSString * OrgWorkLevelDescription;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * OrgWorkLevelName;
@property (retain) NSString * OrgWorkLevelNameEN;
@property (retain) NSNumber * PercentParticipation;
@property (retain) NSString * Reason;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_OrgUnitJobPosition : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * NumOfEmp;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgJobPositionName;
	NSString * OrgJobPositionTitle;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgUnitName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_OrgUnitJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * NumOfEmp;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgJobPositionName;
@property (retain) NSString * OrgJobPositionTitle;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgUnitName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_OrgUnitJobPosition : NSObject {
	
/* elements */
	NSMutableArray *V_OrgUnitJobPosition;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_OrgUnitJobPosition *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgUnitJobPosition:(org_tns2_V_OrgUnitJobPosition *)toAdd;
@property (readonly) NSMutableArray * V_OrgUnitJobPosition;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_OrgUnitJob : NSObject {
	
/* elements */
	NSString * CAREER_NAME_EN;
	NSString * CAREER_NAME_VN;
	NSString * CLOG_CAREER_CODE;
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobName;
	NSString * OrgJobNameEN;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSNumber * OrgUnitJobId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_OrgUnitJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CAREER_NAME_EN;
@property (retain) NSString * CAREER_NAME_VN;
@property (retain) NSString * CLOG_CAREER_CODE;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobName;
@property (retain) NSString * OrgJobNameEN;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * OrgUnitJobId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_OrgUnitJob : NSObject {
	
/* elements */
	NSMutableArray *V_OrgUnitJob;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_OrgUnitJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgUnitJob:(org_tns2_V_OrgUnitJob *)toAdd;
@property (readonly) NSMutableArray * V_OrgUnitJob;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgJob : NSObject {
	
/* elements */
	NSMutableArray *OrgJob;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgJob *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgJob:(org_tns2_OrgJob *)toAdd;
@property (readonly) NSMutableArray * OrgJob;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_SP_ORG_UNIT_TREE_BY_USER_Result : NSObject {
	
/* elements */
	NSString * Address;
	NSNumber * AddressId;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsActive;
	NSNumber * MasterId;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSNumber * OrgUnitLevelId;
	NSString * ParentCode;
	NSNumber * ParentId;
	NSNumber * TotalEmployees;
	NSString * WorkLocation;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_SP_ORG_UNIT_TREE_BY_USER_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Address;
@property (retain) NSNumber * AddressId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsActive;
@property (retain) NSNumber * MasterId;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSNumber * OrgUnitLevelId;
@property (retain) NSString * ParentCode;
@property (retain) NSNumber * ParentId;
@property (retain) NSNumber * TotalEmployees;
@property (retain) NSString * WorkLocation;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfSP_ORG_UNIT_TREE_BY_USER_Result : NSObject {
	
/* elements */
	NSMutableArray *SP_ORG_UNIT_TREE_BY_USER_Result;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfSP_ORG_UNIT_TREE_BY_USER_Result *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addSP_ORG_UNIT_TREE_BY_USER_Result:(org_tns2_SP_ORG_UNIT_TREE_BY_USER_Result *)toAdd;
@property (readonly) NSMutableArray * SP_ORG_UNIT_TREE_BY_USER_Result;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgTimeInCharge : NSObject {
	
/* elements */
	NSMutableArray *OrgTimeInCharge;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgTimeInCharge *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgTimeInCharge:(org_tns2_OrgTimeInCharge *)toAdd;
@property (readonly) NSMutableArray * OrgTimeInCharge;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgSkillType : NSObject {
	
/* elements */
	NSMutableArray *OrgSkillType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgSkillType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgSkillType:(org_tns2_OrgSkillType *)toAdd;
@property (readonly) NSMutableArray * OrgSkillType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgProjectType : NSObject {
	
/* elements */
	NSMutableArray *OrgProjectType;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgProjectType *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgProjectType:(org_tns2_OrgProjectType *)toAdd;
@property (readonly) NSMutableArray * OrgProjectType;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_ESS_CLOG_CAREER : NSObject {
	
/* elements */
	NSString * CAREER_DESC;
	NSString * CAREER_NAME_EN;
	NSString * CAREER_NAME_VN;
	NSString * CLOG_CAREER_CODE;
	NSNumber * CLOG_CAREER_ID;
	NSNumber * COMPANY_ID;
	NSNumber * CREATED_BY;
	NSDate * CREATED_DATE;
	NSString * GROUP_CAREER_CODE;
	USBoolean * IS_DELETED;
	NSNumber * MODIFIED_BY;
	NSDate * MODIFIED_DATE;
	NSNumber * ORDER_BY;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_ESS_CLOG_CAREER *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CAREER_DESC;
@property (retain) NSString * CAREER_NAME_EN;
@property (retain) NSString * CAREER_NAME_VN;
@property (retain) NSString * CLOG_CAREER_CODE;
@property (retain) NSNumber * CLOG_CAREER_ID;
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSNumber * CREATED_BY;
@property (retain) NSDate * CREATED_DATE;
@property (retain) NSString * GROUP_CAREER_CODE;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSNumber * MODIFIED_BY;
@property (retain) NSDate * MODIFIED_DATE;
@property (retain) NSNumber * ORDER_BY;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_ESS_CLOG_CAREER : NSObject {
	
/* elements */
	NSMutableArray *V_ESS_CLOG_CAREER;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_ESS_CLOG_CAREER *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ESS_CLOG_CAREER:(org_tns2_V_ESS_CLOG_CAREER *)toAdd;
@property (readonly) NSMutableArray * V_ESS_CLOG_CAREER;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgDegree : NSObject {
	
/* elements */
	NSMutableArray *OrgDegree;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgDegree *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgDegree:(org_tns2_OrgDegree *)toAdd;
@property (readonly) NSMutableArray * OrgDegree;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgDegreeRank : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSNumber * OrgDegreeRankId;
	NSNumber * Priority;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgDegreeRank *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSNumber * OrgDegreeRankId;
@property (retain) NSNumber * Priority;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgDegreeRank : NSObject {
	
/* elements */
	NSMutableArray *OrgDegreeRank;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgDegreeRank *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgDegreeRank:(org_tns2_OrgDegreeRank *)toAdd;
@property (readonly) NSMutableArray * OrgDegreeRank;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_ESS_ORG_JOB_TASK_DESC : NSObject {
	
/* elements */
	NSNumber * COMPANY_ID;
	NSDate * CREATED_DATE;
	USBoolean * IS_DELETED;
	NSString * JOB_CODE;
	NSNumber * JOB_ID;
	NSDate * MODIFIED_DATE;
	NSNumber * ORG_JOB_TASK_DESC_ID;
	NSNumber * PRIORITY;
	NSString * TYPE_CODE;
	NSString * VALUE;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_ESS_ORG_JOB_TASK_DESC *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSDate * CREATED_DATE;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSString * JOB_CODE;
@property (retain) NSNumber * JOB_ID;
@property (retain) NSDate * MODIFIED_DATE;
@property (retain) NSNumber * ORG_JOB_TASK_DESC_ID;
@property (retain) NSNumber * PRIORITY;
@property (retain) NSString * TYPE_CODE;
@property (retain) NSString * VALUE;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_ESS_ORG_POSITION_TASK_DESC : NSObject {
	
/* elements */
	NSNumber * COMPANY_ID;
	NSDate * CREATED_DATE;
	USBoolean * IS_DELETED;
	NSDate * MODIFIED_DATE;
	NSNumber * ORG_JOB_TASK_DESC_ID;
	NSString * POSITION_CODE;
	NSNumber * POSITION_ID;
	NSNumber * PRIORITY;
	NSString * TYPE_CODE;
	NSString * VALUE;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_ESS_ORG_POSITION_TASK_DESC *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSDate * CREATED_DATE;
@property (retain) USBoolean * IS_DELETED;
@property (retain) NSDate * MODIFIED_DATE;
@property (retain) NSNumber * ORG_JOB_TASK_DESC_ID;
@property (retain) NSString * POSITION_CODE;
@property (retain) NSNumber * POSITION_ID;
@property (retain) NSNumber * PRIORITY;
@property (retain) NSString * TYPE_CODE;
@property (retain) NSString * VALUE;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgWorkLevel : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreateDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgWorkLevelCode;
	NSNumber * OrgWorkLevelId;
	NSString * rowguid;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgWorkLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreateDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * rowguid;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgWorkLevel : NSObject {
	
/* elements */
	NSMutableArray *OrgWorkLevel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgWorkLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgWorkLevel:(org_tns2_OrgWorkLevel *)toAdd;
@property (readonly) NSMutableArray * OrgWorkLevel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_ORG_CAREER_JOB_WLEVEL_TREE : NSObject {
	
/* elements */
	NSString * CODE;
	NSNumber * COMPANY_ID;
	NSNumber * ID_;
	NSString * NAME;
	NSString * NAME_EN;
	NSString * PARENT_CODE;
	NSString * PARENT_ID;
	NSString * TYPE;
	NSString * UNIQUE_ID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_ORG_CAREER_JOB_WLEVEL_TREE *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CODE;
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSNumber * ID_;
@property (retain) NSString * NAME;
@property (retain) NSString * NAME_EN;
@property (retain) NSString * PARENT_CODE;
@property (retain) NSString * PARENT_ID;
@property (retain) NSString * TYPE;
@property (retain) NSString * UNIQUE_ID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_ORG_CAREER_JOB_WLEVEL_TREE : NSObject {
	
/* elements */
	NSMutableArray *V_ORG_CAREER_JOB_WLEVEL_TREE;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_ORG_CAREER_JOB_WLEVEL_TREE *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ORG_CAREER_JOB_WLEVEL_TREE:(org_tns2_V_ORG_CAREER_JOB_WLEVEL_TREE *)toAdd;
@property (readonly) NSMutableArray * V_ORG_CAREER_JOB_WLEVEL_TREE;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_OrgJobPositionDetail : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSNumber * CreatedBy;
	NSDate * CreatedDate;
	USBoolean * IsActive;
	USBoolean * IsDeleted;
	NSString * JobName;
	NSString * JobNameEN;
	NSNumber * ModifiedBy;
	NSDate * ModifiedDate;
	NSString * Name;
	NSString * NameEN;
	NSString * OrgJobCode;
	NSNumber * OrgJobId;
	NSString * OrgJobPositionCode;
	NSNumber * OrgJobPositionId;
	NSString * OrgJobPositionTitle;
	NSString * OrgJobTitleCode;
	NSNumber * OrgJobTitleId;
	NSString * OrgJobTitleNameEN;
	NSString * OrgJobTitleNameVN;
	NSString * OrgUnitCode;
	NSNumber * OrgUnitId;
	NSString * OrgWorkLevelCode;
	NSNumber * OrgWorkLevelId;
	NSString * TITLE_LEVEL_CODE;
	NSString * TITLE_LEVEL_NAME_EN;
	NSString * TITLE_LEVEL_NAME_VN;
	NSString * UnitName;
	NSString * UnitNameEN;
	NSString * WorkLevelName;
	NSString * WorkLevelNameEN;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_OrgJobPositionDetail *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSNumber * CreatedBy;
@property (retain) NSDate * CreatedDate;
@property (retain) USBoolean * IsActive;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * JobName;
@property (retain) NSString * JobNameEN;
@property (retain) NSNumber * ModifiedBy;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * Name;
@property (retain) NSString * NameEN;
@property (retain) NSString * OrgJobCode;
@property (retain) NSNumber * OrgJobId;
@property (retain) NSString * OrgJobPositionCode;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSString * OrgJobPositionTitle;
@property (retain) NSString * OrgJobTitleCode;
@property (retain) NSNumber * OrgJobTitleId;
@property (retain) NSString * OrgJobTitleNameEN;
@property (retain) NSString * OrgJobTitleNameVN;
@property (retain) NSString * OrgUnitCode;
@property (retain) NSNumber * OrgUnitId;
@property (retain) NSString * OrgWorkLevelCode;
@property (retain) NSNumber * OrgWorkLevelId;
@property (retain) NSString * TITLE_LEVEL_CODE;
@property (retain) NSString * TITLE_LEVEL_NAME_EN;
@property (retain) NSString * TITLE_LEVEL_NAME_VN;
@property (retain) NSString * UnitName;
@property (retain) NSString * UnitNameEN;
@property (retain) NSString * WorkLevelName;
@property (retain) NSString * WorkLevelNameEN;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_OrgExperience : NSObject {
	
/* elements */
	NSString * Achievement;
	NSNumber * CompanyId;
	USBoolean * IsExperienceDeleted;
	NSNumber * OrgExperienceId;
	NSNumber * OrgProjectTypeId;
	NSString * OrgProjectTypeName;
	NSString * OrgTimeInChargeDescription;
	NSNumber * OrgTimeInChargeId;
	NSString * RoleDescription;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_OrgExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Achievement;
@property (retain) NSNumber * CompanyId;
@property (retain) USBoolean * IsExperienceDeleted;
@property (retain) NSNumber * OrgExperienceId;
@property (retain) NSNumber * OrgProjectTypeId;
@property (retain) NSString * OrgProjectTypeName;
@property (retain) NSString * OrgTimeInChargeDescription;
@property (retain) NSNumber * OrgTimeInChargeId;
@property (retain) NSString * RoleDescription;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_OrgExperience : NSObject {
	
/* elements */
	NSMutableArray *V_OrgExperience;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_OrgExperience *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgExperience:(org_tns2_V_OrgExperience *)toAdd;
@property (readonly) NSMutableArray * V_OrgExperience;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgQualification : NSObject {
	
/* elements */
	NSMutableArray *OrgQualification;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgQualification *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgQualification:(org_tns2_OrgQualification *)toAdd;
@property (readonly) NSMutableArray * OrgQualification;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgKnowledge : NSObject {
	
/* elements */
	NSNumber * CLogMajorId;
	NSNumber * CompanyId;
	NSString * Description;
	USBoolean * IsDeleted;
	NSString * OrgKnowledgeCode;
	NSNumber * OrgKnowledgeId;
	NSString * OrgKnowledgeName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgKnowledge *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSNumber * CompanyId;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSString * OrgKnowledgeCode;
@property (retain) NSNumber * OrgKnowledgeId;
@property (retain) NSString * OrgKnowledgeName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgKnowledge : NSObject {
	
/* elements */
	NSMutableArray *OrgKnowledge;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgKnowledge *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgKnowledge:(org_tns2_OrgKnowledge *)toAdd;
@property (readonly) NSMutableArray * OrgKnowledge;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_OrgJobPositionKnowledgeProficency : NSObject {
	
/* elements */
	NSString * CLogMajorCode;
	NSNumber * CLogMajorId;
	NSString * CLogMajorName;
	NSString * CLogRatingDescription;
	NSNumber * CLogRatingId;
	NSNumber * CLogRatingValue;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgJobPositionId;
	NSNumber * OrgJobPositionRequiredProficencyId;
	NSString * OrgKnowledgeCode;
	NSString * OrgKnowledgeDescription;
	NSString * OrgKnowledgeName;
	NSString * ProficiencyTypeCode;
	NSNumber * RecordId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_OrgJobPositionKnowledgeProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogMajorCode;
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSString * CLogMajorName;
@property (retain) NSString * CLogRatingDescription;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CLogRatingValue;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * OrgJobPositionRequiredProficencyId;
@property (retain) NSString * OrgKnowledgeCode;
@property (retain) NSString * OrgKnowledgeDescription;
@property (retain) NSString * OrgKnowledgeName;
@property (retain) NSString * ProficiencyTypeCode;
@property (retain) NSNumber * RecordId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_OrgJobPositionKnowledgeProficency : NSObject {
	
/* elements */
	NSMutableArray *V_OrgJobPositionKnowledgeProficency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_OrgJobPositionKnowledgeProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgJobPositionKnowledgeProficency:(org_tns2_V_OrgJobPositionKnowledgeProficency *)toAdd;
@property (readonly) NSMutableArray * V_OrgJobPositionKnowledgeProficency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_OrgJobPositionExperienceProficency : NSObject {
	
/* elements */
	NSString * Achievement;
	NSString * CLogRatingDescription;
	NSNumber * CLogRatingId;
	NSNumber * CLogRatingValue;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgExperienceId;
	NSNumber * OrgJobPositionId;
	NSNumber * OrgJobPositionRequiredProficencyId;
	NSNumber * OrgProjectTypeId;
	NSString * OrgProjectTypeName;
	NSString * OrgTimeInChargeDescription;
	NSNumber * OrgTimeInChargeId;
	NSString * ProficiencyTypeCode;
	NSNumber * RecordId;
	NSString * RoleDescription;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_OrgJobPositionExperienceProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Achievement;
@property (retain) NSString * CLogRatingDescription;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CLogRatingValue;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgExperienceId;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * OrgJobPositionRequiredProficencyId;
@property (retain) NSNumber * OrgProjectTypeId;
@property (retain) NSString * OrgProjectTypeName;
@property (retain) NSString * OrgTimeInChargeDescription;
@property (retain) NSNumber * OrgTimeInChargeId;
@property (retain) NSString * ProficiencyTypeCode;
@property (retain) NSNumber * RecordId;
@property (retain) NSString * RoleDescription;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_OrgJobPositionExperienceProficency : NSObject {
	
/* elements */
	NSMutableArray *V_OrgJobPositionExperienceProficency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_OrgJobPositionExperienceProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgJobPositionExperienceProficency:(org_tns2_V_OrgJobPositionExperienceProficency *)toAdd;
@property (readonly) NSMutableArray * V_OrgJobPositionExperienceProficency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_OrgJobPositionQualificationProficency : NSObject {
	
/* elements */
	NSNumber * CLogEducationLevelId;
	NSString * CLogEducationLevelName;
	NSString * CLogMajorCode;
	NSNumber * CLogMajorId;
	NSString * CLogRatingDescription;
	NSNumber * CLogRatingId;
	NSNumber * CLogRatingValue;
	NSString * ClogMajorName;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgJobPositionId;
	NSNumber * OrgJobPositionRequiredProficencyId;
	NSString * OrgQualificationCode;
	NSString * OrgQualificationDescription;
	NSString * OrgQualificationName;
	NSString * ProficiencyTypeCode;
	NSNumber * RecordId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_OrgJobPositionQualificationProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CLogEducationLevelId;
@property (retain) NSString * CLogEducationLevelName;
@property (retain) NSString * CLogMajorCode;
@property (retain) NSNumber * CLogMajorId;
@property (retain) NSString * CLogRatingDescription;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CLogRatingValue;
@property (retain) NSString * ClogMajorName;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * OrgJobPositionRequiredProficencyId;
@property (retain) NSString * OrgQualificationCode;
@property (retain) NSString * OrgQualificationDescription;
@property (retain) NSString * OrgQualificationName;
@property (retain) NSString * ProficiencyTypeCode;
@property (retain) NSNumber * RecordId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_OrgJobPositionQualificationProficency : NSObject {
	
/* elements */
	NSMutableArray *V_OrgJobPositionQualificationProficency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_OrgJobPositionQualificationProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgJobPositionQualificationProficency:(org_tns2_V_OrgJobPositionQualificationProficency *)toAdd;
@property (readonly) NSMutableArray * V_OrgJobPositionQualificationProficency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_OrgJobPositionSkillProficency : NSObject {
	
/* elements */
	NSString * CLogRatingDescription;
	NSNumber * CLogRatingId;
	NSNumber * CLogRatingValue;
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSNumber * OrgJobPositionId;
	NSNumber * OrgJobPositionRequiredProficencyId;
	NSString * OrgSkillCode;
	NSString * OrgSkillName;
	NSNumber * OrgSkillTypeId;
	NSNumber * Priority;
	NSString * PriorityName;
	NSString * ProficiencyTypeCode;
	NSNumber * RecordId;
	NSString * SKillDescription;
	NSString * SkillTypeName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_OrgJobPositionSkillProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CLogRatingDescription;
@property (retain) NSNumber * CLogRatingId;
@property (retain) NSNumber * CLogRatingValue;
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSNumber * OrgJobPositionId;
@property (retain) NSNumber * OrgJobPositionRequiredProficencyId;
@property (retain) NSString * OrgSkillCode;
@property (retain) NSString * OrgSkillName;
@property (retain) NSNumber * OrgSkillTypeId;
@property (retain) NSNumber * Priority;
@property (retain) NSString * PriorityName;
@property (retain) NSString * ProficiencyTypeCode;
@property (retain) NSNumber * RecordId;
@property (retain) NSString * SKillDescription;
@property (retain) NSString * SkillTypeName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_OrgJobPositionSkillProficency : NSObject {
	
/* elements */
	NSMutableArray *V_OrgJobPositionSkillProficency;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_OrgJobPositionSkillProficency *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_OrgJobPositionSkillProficency:(org_tns2_V_OrgJobPositionSkillProficency *)toAdd;
@property (readonly) NSMutableArray * V_OrgJobPositionSkillProficency;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_ORG_JOB_WORK_LEVEL_TREE : NSObject {
	
/* elements */
	NSString * CODE;
	NSNumber * COMPANY_ID;
	NSNumber * ID_;
	NSString * NAME;
	NSString * NAME_EN;
	NSString * PARENT_CODE;
	NSString * PARENT_ID;
	NSString * TYPE;
	NSString * UNIQUE_ID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_ORG_JOB_WORK_LEVEL_TREE *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CODE;
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSNumber * ID_;
@property (retain) NSString * NAME;
@property (retain) NSString * NAME_EN;
@property (retain) NSString * PARENT_CODE;
@property (retain) NSString * PARENT_ID;
@property (retain) NSString * TYPE;
@property (retain) NSString * UNIQUE_ID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_ORG_JOB_WORK_LEVEL_TREE : NSObject {
	
/* elements */
	NSMutableArray *V_ORG_JOB_WORK_LEVEL_TREE;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_ORG_JOB_WORK_LEVEL_TREE *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ORG_JOB_WORK_LEVEL_TREE:(org_tns2_V_ORG_JOB_WORK_LEVEL_TREE *)toAdd;
@property (readonly) NSMutableArray * V_ORG_JOB_WORK_LEVEL_TREE;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_ORG_UNIT_POSITION_TREE : NSObject {
	
/* elements */
	NSString * CODE;
	NSNumber * COMPANY_ID;
	NSNumber * ID_;
	NSString * NAME;
	NSString * NAME_EN;
	NSString * PARENT_CODE;
	NSString * PARENT_ID;
	NSString * TYPE;
	NSString * UNIQUE_ID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_ORG_UNIT_POSITION_TREE *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CODE;
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSNumber * ID_;
@property (retain) NSString * NAME;
@property (retain) NSString * NAME_EN;
@property (retain) NSString * PARENT_CODE;
@property (retain) NSString * PARENT_ID;
@property (retain) NSString * TYPE;
@property (retain) NSString * UNIQUE_ID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_ORG_UNIT_POSITION_TREE : NSObject {
	
/* elements */
	NSMutableArray *V_ORG_UNIT_POSITION_TREE;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_ORG_UNIT_POSITION_TREE *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ORG_UNIT_POSITION_TREE:(org_tns2_V_ORG_UNIT_POSITION_TREE *)toAdd;
@property (readonly) NSMutableArray * V_ORG_UNIT_POSITION_TREE;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_V_ORG_JOB_POSITION_TREE : NSObject {
	
/* elements */
	NSString * CODE;
	NSNumber * COMPANY_ID;
	NSNumber * ID_;
	NSString * NAME;
	NSString * NAME_EN;
	NSString * PARENT_CODE;
	NSString * PARENT_ID;
	NSString * TYPE;
	NSString * UNIQUE_ID;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_V_ORG_JOB_POSITION_TREE *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CODE;
@property (retain) NSNumber * COMPANY_ID;
@property (retain) NSNumber * ID_;
@property (retain) NSString * NAME;
@property (retain) NSString * NAME_EN;
@property (retain) NSString * PARENT_CODE;
@property (retain) NSString * PARENT_ID;
@property (retain) NSString * TYPE;
@property (retain) NSString * UNIQUE_ID;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfV_ORG_JOB_POSITION_TREE : NSObject {
	
/* elements */
	NSMutableArray *V_ORG_JOB_POSITION_TREE;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfV_ORG_JOB_POSITION_TREE *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addV_ORG_JOB_POSITION_TREE:(org_tns2_V_ORG_JOB_POSITION_TREE *)toAdd;
@property (readonly) NSMutableArray * V_ORG_JOB_POSITION_TREE;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_OrgGroup : NSObject {
	
/* elements */
	NSNumber * CompanyId;
	NSDate * CreatedDate;
	NSString * Description;
	USBoolean * IsDeleted;
	NSDate * ModifiedDate;
	NSString * OrgGroupCode;
	NSNumber * OrgGroupId;
	NSString * OrgGroupName_FL;
	NSString * OrgGroupName_NL;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_OrgGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CompanyId;
@property (retain) NSDate * CreatedDate;
@property (retain) NSString * Description;
@property (retain) USBoolean * IsDeleted;
@property (retain) NSDate * ModifiedDate;
@property (retain) NSString * OrgGroupCode;
@property (retain) NSNumber * OrgGroupId;
@property (retain) NSString * OrgGroupName_FL;
@property (retain) NSString * OrgGroupName_NL;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface org_tns2_ArrayOfOrgGroup : NSObject {
	
/* elements */
	NSMutableArray *OrgGroup;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (org_tns2_ArrayOfOrgGroup *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOrgGroup:(org_tns2_OrgGroup *)toAdd;
@property (readonly) NSMutableArray * OrgGroup;
/* attributes */
- (NSDictionary *)attributes;
@end
