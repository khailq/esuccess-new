//
//  SocialInsuranceSalaryViewController.m
//  eSuccess
//
//  Created by HPTVIETNAM on 6/25/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "SocialInsuranceSalaryViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "DayLeaveDetailCell.h"
@interface SocialInsuranceSalaryViewController ()

@end

@implementation SocialInsuranceSalaryViewController
@synthesize socialInsSalaryArr;
@synthesize gradeArr;
static NSString *dayLeaveCellIdentifier = @"DayLeaveCell";
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"Biến động lương BHXH";
    [self getData];
    UINib *nib = [UINib nibWithNibName:@"DayLeaveDetailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:dayLeaveCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 130;
//}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([gradeArr count] > 0) {
        return [gradeArr objectAtIndex:section];
    }
    return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [socialInsSalaryArr count] > 0 ? socialInsSalaryArr.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [socialInsSalaryArr count] > 0 ? 9 : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (socialInsSalaryArr.count == 0)
        return [self getDefaultEmptyCell];
    
    DayLeaveDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:dayLeaveCellIdentifier];
    if (cell == nil) {
        cell = [[DayLeaveDetailCell alloc]init];
    }
    cell.lbLeft.font = [UIFont systemFontOfSize:14];
    cell.lbLeft.backgroundColor = [UIColor clearColor];
    cell.lbRight.font = [UIFont boldSystemFontOfSize:16];
    cell.lbRight.backgroundColor = [UIColor clearColor];
    
    int section = indexPath.section;
    empl_tns1_V_EmpSocialInsuranceSalary *salary = [socialInsSalaryArr objectAtIndex:section];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    if (indexPath.row == 0) {
        cell.lbLeft.text = @"Quyết định";
        cell.lbRight.text = salary.DecisionName;
    }
    if (indexPath.row == 1) {
        cell.lbLeft.text = @"Hệ số";
        cell.lbRight.text = [NSString stringWithFormat:@"%.2f",[salary.nEmpSISalaryCoeficient floatValue]];
    }
    if (indexPath.row == 2) {
        cell.lbLeft.text = @"Lương cơ bản";
        cell.lbRight.text = [NSString stringWithFormat:@"%8.2f",[salary.nEmpSISalary floatValue]];
    }
    if (indexPath.row == 3) {
        cell.lbLeft.text = @"Ngày áp dụng";
        cell.lbRight.text = [formatter stringFromDate:salary.ImplementationDate];
    }
    if (indexPath.row == 4) {
        cell.lbLeft.text = @"Đến ngày";
        cell.lbRight.text = [formatter stringFromDate:salary.ExpirationDate];
    }
    if (indexPath.row == 5) {
        cell.lbLeft.text = @"Ngày báo BHXH";
        cell.lbRight.text = [formatter stringFromDate:salary.SocialInsuranceDate];
    }
    if (indexPath.row == 6) {
        cell.lbLeft.text = @"Tiền tệ";
        cell.lbRight.text = salary.CLogCurrencyName;
    }
    if (indexPath.row == 7) {
        cell.lbLeft.text = @"Tháng truy thu";
        [formatter setDateFormat:@"MM/yyyy"];
        cell.lbRight.text = [formatter stringFromDate:salary.RecallMonth];
    }
    if (indexPath.row == 8) {
        cell.lbLeft.text = @"Lý do";
        cell.lbRight.text = salary.Reason;
    }
    return cell;
}

#pragma getData
- (void)getData
{
    socialInsSalaryArr = [[NSMutableArray alloc]init];
    gradeArr = [[NSMutableArray alloc]init];
    
    [self getEmpProfileSocialInsSalaryById];
}

- (void) getEmpProfileSocialInsSalaryById
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Đang cập nhật";
    
    BasicHttpBinding_IEmpProfileLayerServiceBinding *binding = [EmpProfileLayerServiceSvc BasicHttpBinding_IEmpProfileLayerServiceBinding];
    EmpProfileLayerServiceSvc_GetViewEmpSocialInsuranceSalaryById *request = [[EmpProfileLayerServiceSvc_GetViewEmpSocialInsuranceSalaryById alloc] init];
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    if (app.otherEmpId != nil)
        request.employeeId = app.otherEmpId;
    else
        request.employeeId = app.sysUser.EmployeeId ;
    //request.EmployeeId = [NSNumber numberWithInt:2];
    [binding GetViewEmpSocialInsuranceSalaryByIdAsyncUsingParameters:request delegate:self];
}

- (void)operation:(BasicHttpBinding_IEmpProfileLayerServiceBindingOperation *)operation completedWithResponse:(BasicHttpBinding_IEmpProfileLayerServiceBindingResponse *)response
{
    if ([response.bodyParts count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi kết nối" message:@"Không thể kết nối đến server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    for (id mine in response.bodyParts){
        if ([mine isKindOfClass:[EmpProfileLayerServiceSvc_GetViewEmpSocialInsuranceSalaryByIdResponse class]]) {
            empl_tns1_ArrayOfV_EmpSocialInsuranceSalary *result = [mine GetViewEmpSocialInsuranceSalaryByIdResult];
            socialInsSalaryArr = result.V_EmpSocialInsuranceSalary;
            for (empl_tns1_V_EmpSocialInsuranceSalary *salary in socialInsSalaryArr) {
                if (salary.nEmpSISalaryGrade == nil) {
                    [gradeArr addObject:@""];
                } else
                    [gradeArr addObject:[NSString stringWithFormat:@"Bậc %1.2f",[salary.nEmpSISalaryGrade floatValue]]];
            }
            
            if (!self.viewDidDisappear)
                [self.tableView reloadData];
        }
        //        if ([mine isKindOfClass:[SOAPFault class]])
        //        {
        //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Server error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //            [alert show];
        //        }
    }
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

@end
