//
//  MenuItem.h
//  eSuccess
//
//  Created by HPTVIETNAM on 6/15/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuItem : NSObject

@property (strong, nonatomic) NSNumber * order;
@property (strong, nonatomic) NSString * title;
@property (strong, nonatomic) NSArray * items;

+(MenuItem *)parseMenuItem:(NSDictionary *)dict;

@end
