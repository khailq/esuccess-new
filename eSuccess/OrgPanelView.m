//
//  OrgPanelView.m
//  eSuccess
//
//  Created by HPTVIETNAM on 5/11/13.
//  Copyright (c) 2013 HPTVIETNAM. All rights reserved.
//

#import "OrgPanelView.h"

@implementation OrgPanelView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void) clipCornersToOvalWidth:(float)ovalWidth height:(float)ovalHeight
{
    float fw, fh;
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect rect = CGRectMake(-4.0f, -4.0f, self.frame.size.width + 8, self.frame.size.height + 8);
    
    if (ovalWidth == 0 || ovalHeight == 0) {
        CGContextAddRect(context, rect);
        return;
    }
    CGContextSaveGState(context);
    CGContextTranslateCTM (context, CGRectGetMinX(rect), CGRectGetMinY(rect));
    CGContextScaleCTM (context, ovalWidth, ovalHeight);
    fw = CGRectGetWidth (rect) / ovalWidth;
    fh = CGRectGetHeight (rect) / ovalHeight;
    CGContextMoveToPoint(context, fw, fh/2);
    CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);
    CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1);
    CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1);
    CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1);
    CGContextClosePath(context);
    CGContextRestoreGState(context);
}

-(void)drawRect:(CGRect)rect {
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextBeginPath(ctx);
    
    
    CGFloat minY = CGRectGetMinY(rect);
    CGFloat minX = CGRectGetMinX(rect);
    CGFloat maxX = CGRectGetMaxX(rect);
    CGFloat midX = CGRectGetMidX(rect);
    CGContextMoveToPoint(ctx, 0, minY);
    CGContextAddLineToPoint(ctx, minX, 15);
    CGContextAddLineToPoint(ctx, midX - 20, 15);
    CGContextAddLineToPoint(ctx, midX, 0);
    CGContextAddLineToPoint(ctx, midX + 20, 15);
    CGContextAddLineToPoint(ctx, maxX, 15);
    CGContextAddLineToPoint(ctx, maxX, minY);
    CGContextClosePath(ctx);
    
    CGContextSetFillColorWithColor(ctx, [UIColor colorWithRed:225.0f/255.0f green:225.0f/255.0f blue:225.0f/255.0f alpha:1].CGColor);
    
    CGContextFillPath(ctx);
}



@end
